/**
 * rsr 
 *
 *Aug 6, 2016
 */
package com.cucumber.framework.helper.Button;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Generic.GenericHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.interfaces.IwebComponent;
import com.cucumber.framework.settings.ObjectRepo;
/**
 * @author vishal bhut
 */
public class ButtonHelper implements IwebComponent 
{
	
	private WebDriver driver;
	private Logger oLog = LoggerHelper.getLogger(ButtonHelper.class);
	private GenericHelper geneHelpObj; 
	private WaitHelper waitObj;
	WebDriverWait wait =new WebDriverWait (ObjectRepo.driver,60);
	
	public ButtonHelper(WebDriver driver) {
		this.driver = driver;
		oLog.debug("Button Helper : " + this.driver.hashCode());
		geneHelpObj = new GenericHelper(driver); 
		waitObj = new WaitHelper(driver, ObjectRepo.reader);
	}
	
	public void click(By locator) {
		click(driver.findElement(locator));
		oLog.info(locator);
	}
	
	public void click(WebElement element){
		waitObj.waitForElementVisible(element);
		new Helper().javascriptExecutorClick(element);
		oLog.info(element);
	}
	
	public String BtnColor(WebElement element) 
	{
		waitObj.waitForElementVisible(element);
		String color = element.getCssValue("background-color");
		System.out.println(color);
		String hex = Color.fromString(color).asHex();
		System.out.println(hex);
		return hex;
	}
	public String FontColorCodeHex(WebElement element) {
		waitObj.waitForElementVisible(element);
		String color = element.getCssValue("color");
		String hex = Color.fromString(color).asHex();
		System.out.println(hex);

		return hex;
	}
}
