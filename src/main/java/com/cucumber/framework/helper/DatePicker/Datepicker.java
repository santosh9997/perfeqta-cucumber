package com.cucumber.framework.helper.DatePicker;

import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import com.cucumber.framework.settings.ObjectRepo;
/**
 * @author vishal bhut
 */
public class Datepicker {
	static List<String> month = Arrays.asList("January", "February", "March", "April", "May", "June", "July", "August",
			"September", "October", "November", "December");
	static int expMonth;
	static int expYear;
	static String expDate = null;
	static String calMonth = null;
	static String calYear = null;
	static boolean dateFound;

	public static void Dateselection(String Exp_Date, int Exp_Month, int Exp_Year, WebElement CalPath) {
		try {
			System.out.println("in date selection \n Expected Date is: " + Exp_Date + "-" + Exp_Month + "-" + Exp_Year);
			Thread.sleep(2000);
			JavascriptExecutor jse = (JavascriptExecutor) ObjectRepo.driver;
			jse.executeScript("window.scrollTo(0, -document.body.scrollHeight);");
			Thread.sleep(2000);
			CalPath.click();
			Thread.sleep(2000);
			expDate = Exp_Date;
			expMonth = Exp_Month;
			expYear = Exp_Year;

			WebElement ele_monthyear = ObjectRepo.driver.findElement(By.xpath("//strong[@class='ng-binding']"));
			dateFound = true;
			while (dateFound) {
				String[] my = ele_monthyear.getText().split(" ");
				String mm = my[0];
				String year = my[1];
				calMonth = mm;
				calYear = year;

				Thread.sleep(2000);
				if (month.lastIndexOf(calMonth) + 1 == expMonth && (expYear == Integer.parseInt(calYear))) {
					selectDate(expDate);
					dateFound = false;
				} else if (month.lastIndexOf(calMonth) + 1 < expMonth && (expYear == Integer.parseInt(calYear))
						|| expYear > Integer.parseInt(calYear)) {
					ObjectRepo.driver
							.findElement(By
									.xpath("//button[contains(@class,'btn btn-default btn-sm pull-right uib-right')]"))
							.click();
				} else if (month.lastIndexOf(calMonth) + 1 > expMonth && (expYear == Integer.parseInt(calYear))
						|| expYear < Integer.parseInt(calYear)) {
					ObjectRepo.driver
							.findElement(
									By.xpath("//button[contains(@class,'btn btn-default btn-sm pull-left uib-left')]"))
							.click();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void selectDate(String date) {
		WebElement Ele_Table = ObjectRepo.driver.findElement(By.xpath("//*[@class='uib-daypicker']"));
		List<WebElement> tablerow = Ele_Table.findElements(By.tagName("tr"));
//		List<WebElement> tablecol = Ele_Table.findElements(By.tagName("td"));
//		WebElement celval = null;
		outerloop: for (int i = 2; i <= tablerow.size(); i++) {
			List<WebElement> tablecol1 = tablerow.get(i).findElements(By.tagName("td"));
			int colsize = tablecol1.size();
			for (int j = 1; j < colsize; j++) {
				String cell = tablecol1.get(j).getText();
				String app_Date = date;
				if (cell.equals(app_Date)) {
					try {
						String str = "//span[text()='" + app_Date + "']";
						ObjectRepo.driver.findElement(By.xpath(str.trim())).click();
						break outerloop;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
}
