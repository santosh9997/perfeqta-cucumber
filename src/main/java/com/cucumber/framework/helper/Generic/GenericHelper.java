/**
 * @author rahul.rathore
 *	
 *	06-Aug-2016
 */
package com.cucumber.framework.helper.Generic;

import java.io.File;
import java.io.IOException;
import java.nio.Buffer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
//import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.testng.Reporter;

import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.interfaces.IwebComponent;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.DateTimeHelper;
import com.cucumber.framework.utility.ResourceHelper;
/**
 * @author vishal bhut
 */
public class GenericHelper implements IwebComponent {

	private WebDriver driver;
	private Logger oLog = LoggerHelper.getLogger(GenericHelper.class);
	//private WaitHelper waitObj;
	
	public GenericHelper(WebDriver driver) {
		this.driver = driver;
		oLog.debug("GenericHelper : " + this.driver.hashCode());
		//waitObj = new WaitHelper(driver, ObjectRepo.reader);
	}
	public WebElement getElement(By locator) {
		oLog.info(locator);
		if (IsElementPresentQuick(locator))
			return driver.findElement(locator);
		
		try {
			throw new NoSuchElementException("Element Not Found : " + locator);
		} catch (RuntimeException re) {
			oLog.error(re);
			throw re;
		}
	}
	
	/**
	 * Check for element is present based on locator
	 * If the element is present return the web element otherwise null
	 * @param locator
	 * @return WebElement or null
	 */
	
	public WebElement getElementWithNull(By locator) {
		oLog.info(locator);
		try {
			return driver.findElement(locator);
		} catch (NoSuchElementException e) {
			// Ignore
		}
		return null;
	}

	public boolean IsElementPresentQuick(By locator) {
		boolean flag = driver.findElements(locator).size() >= 1;
		
		//driver.findElements(By.xpath(""));
		oLog.info(flag);
		return flag;
	}

	public String takeScreenShot(String name) throws IOException {

		/*if (driver instanceof HtmlUnitDriver) {
			oLog.fatal("HtmlUnitDriver Cannot take the ScreenShot");
			return "";
		}*/

		File destDir = new File(ResourceHelper.getResourcePath("screenshots/")
				+ DateTimeHelper.getCurrentDate());
		if (!destDir.exists())
			destDir.mkdir();

		File destPath = new File(destDir.getAbsolutePath()
				+ System.getProperty("file.separator") + name + ".png");
		try 
		{
			//File sourcePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE); 
			//File destinationPath = new File(System.getProperty("user.dir") + "/target/test-report/extent-report/screenshots/" + screenshotName + ".png"); 
			//copyFile(sourcePath, destinationPath); Reporter.addScreenCaptureFromPath("./screenshots/"+screenshotName+".png");
			
			
			FileUtils
					.copyFile(((TakesScreenshot) driver)
							.getScreenshotAs(OutputType.FILE), destPath);
			
			
			//Reporter.addScreenCaptureFromPath("./screenshots/"+name+".png");
		} catch (IOException e) {
			oLog.error(e);
			throw e;
		}
		oLog.info(destPath.getAbsolutePath());
		
		
		return destPath.getAbsolutePath();
	}

	public byte[] takeScreenShotBytes() {
		oLog.info("");
		 byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
		
		 
//		 byte[] screenShot = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
         
		return screenshot;
		
	}
	/*
	public byte[] takeScreenShot(OutputType<byte[]> bytes) {
		// TODO Auto-generated method stub
		oLog.info("");
		 byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
	
		
		return screenshot;
	}*/

	
	//*
	public String getLocatorFromWebElement(WebElement element) {

		int a=0;
		String locator = element.toString().split("->")[1].replaceFirst("(?s)(.*)\\]", "$1" + "");
		return locator;
	}
	public By locatorParser(String locator) {

		By loc = By.id(locator);
//		System.err.println("locator: " + locator);
		if (locator.contains("id:")) {
			System.err.println("This is ID: " + locator);
			locator = locator.replace("id: ", "");
			loc = By.id(locator);
			System.err.println("ID: " + loc);

		} else if (locator.contains("name:")) {
			System.err.println("This is NAME: " + locator);
			locator = locator.replace("name: ", "");
			loc = By.name(locator);
			System.err.println("Name: " + loc);

		} else if (locator.contains("xpath:")) {

//			System.err.println("This is Xpath: " + locator);
			locator = locator.replace("xpath: ", "");
			loc = By.xpath(locator);
//			System.err.println("xpath: " + loc);
		} else if (locator.contains("css:")) {

			System.err.println("This is CSS: " + locator);
			locator = locator.replace("css: ", "");
			loc = By.cssSelector(locator);
			System.err.println("xpath: " + loc);

		} else if (locator.contains("link text:")) {
			System.err.println("This is link text: " + locator);
			locator = locator.replace("link text: ", "");
			loc = By.linkText(locator.trim());
			System.err.println("xpath: " + loc);
		}

		return loc;

	}
	
	//Siddharth
	
	public boolean isDisplayed(WebElement locator) {
		oLog.info("Locator : " + locator);
		//waitObj.waitForElementVisible(locator);
		return locator.isDisplayed();
	}
	
	public String getCurrentTimeAndDate() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm");
	    LocalDateTime now = LocalDateTime.now();
	    System.out.println(dtf.format(now));
		return dtf.format(now).toString();
	}
	
	public String getTodayDate() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
	    LocalDateTime now = LocalDateTime.now();
	    System.out.println(dtf.format(now));
		return dtf.format(now).toString();
	}
	
	public int getCellingValue(int upperValue, int lowerValue) {
		int n = (int) Math.ceil((double) upperValue / lowerValue)	;;
		return n;
	}

	public String getCurrentSystemDate() {
		/*
		 * Function : This function is used to display current system date : Created By
		 * : Santosh Shinde Created Date : 23th oct 2018
		 */
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		LocalDateTime now = LocalDateTime.now();
		System.out.println(dtf.format(now));
		return dtf.format(now).toString();
	}
}
