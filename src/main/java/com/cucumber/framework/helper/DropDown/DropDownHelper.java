/**
 * @author rahul.rathore
 *	
 *	07-Aug-2016
 */
package com.cucumber.framework.helper.DropDown;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.framework.helper.Generic.GenericHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;

/**
 * @author vishal bhut
 */
public class DropDownHelper extends GenericHelper {
	
	private WebDriver driver;
	private Logger oLog = LoggerHelper.getLogger(DropDownHelper.class);
	private GenericHelper geneHelpObj; 
	private WaitHelper waitObj;

	public DropDownHelper(WebDriver driver) {
		super(driver);
		this.driver = driver;
		oLog.debug("DropDownHelper : " + this.driver.hashCode());
		geneHelpObj = new GenericHelper(driver); 
		waitObj = new WaitHelper(driver, ObjectRepo.reader);
	}
	
	public void SelectUsingVisibleValue(WebElement locator,String visibleValue) {
		waitObj.waitForElementVisible(locator);
		SelectUsingVisibleValue(getElement(geneHelpObj.locatorParser(geneHelpObj.getLocatorFromWebElement(locator))),visibleValue);
	}
	
	public void SelectUsingValue(WebElement locator,String value) {
		waitObj.waitForElementVisible(locator);
		Select select = new Select(getElement(geneHelpObj.locatorParser(geneHelpObj.getLocatorFromWebElement(locator))));
		select.selectByValue(value);
		oLog.info("Locator : " + locator + " Value : " + value);
	}
	
	public void SelectUsingIndex(WebElement locator,int index) {
		waitObj.waitForElementVisible(locator);
		Select select = new Select(getElement(geneHelpObj.locatorParser(geneHelpObj.getLocatorFromWebElement(locator))));
		select.selectByIndex(index);
		oLog.info("Locator : " + locator + " Index : " + index);
	}
	
	public String getFirstSelectedOption(WebElement locator)
	{
		String defaultItem=null;
		if(locator.isDisplayed())
		{
		waitObj.waitForElementVisible(locator);
		Select select = new Select(locator);
		WebElement firstElement = select.getFirstSelectedOption();
		defaultItem = firstElement.getText();
		return defaultItem;
		}
		else
		{
			System.out.println();
			return defaultItem;
		}
	}
	public String getSelectedValue(WebElement locator) {
		oLog.info(locator);
		waitObj.waitForElementVisible(locator);
		String abc =  getSelectedValue(getElement(geneHelpObj.locatorParser(geneHelpObj.getLocatorFromWebElement(locator)))).toString();
		return abc;
	}
	
	/*
	 * public String getSelectedValue(WebElement element) { String value = new
	 * Select(element).getFirstSelectedOption().getText(); oLog.info("WebELement : "
	 * + element + " Value : "+ value); return value; }
	 */
	
	
	
	public List<String> getAllDropDownValues(WebElement locator) {
		waitObj.waitForElementVisible(locator);
		Select select = new Select(getElement(geneHelpObj.locatorParser(geneHelpObj.getLocatorFromWebElement(locator))));
		List<WebElement> elementList = select.getOptions();
		List<String> valueList = new LinkedList<String>();
		
		for (WebElement element : elementList) {
			oLog.info(element.getText());
			valueList.add(element.getText());
		}
		return valueList;
	}
	
	public void selectDropDownText(WebElement element, String value) {
		Select fruits = new Select(element);
		fruits.selectByVisibleText(value);
	}
	
	public void selectDropDownIndex(WebElement element, int index) {
		Select fruits = new Select(element);
		fruits.selectByIndex(index);
	}
}
