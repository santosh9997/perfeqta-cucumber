package com.cucumber.framework.helper.Copy;

import static org.testng.Assert.assertEquals;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.asserts.SoftAssert;
import org.testng.Assert;
import com.cucumber.framework.settings.ObjectRepo;
/**
 * @author vishal bhut
 */

public class Copy {

	public static void VerifyCopyPopup(String PopupTitle) throws Exception {
		SoftAssert sf = new SoftAssert();
		Thread.sleep(1000);
		// Click on Action button to Create Copy
		ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.createduplicate(field,row)']")).click();
		Thread.sleep(1000);
		if (ObjectRepo.driver
				.findElements(By.xpath("//*[@class='sweet-alert cross-button show-input showSweetAlert visible']"))
				.size() != 0) {
			String Title = ObjectRepo.driver.findElement(By.xpath("//*[contains(text(),'" + PopupTitle + "')]"))
					.getText();
			sf.assertEquals(Title, PopupTitle);
			System.out.println("Title: " + Title + " and Popup Title:  " + PopupTitle);
			sf.assertAll();
		}
	}

	public static String CopyFromKeyboard(WebElement formElement) throws Exception {
		Actions a = new Actions(ObjectRepo.driver);
		a.sendKeys(formElement, Keys.chord(Keys.CONTROL, "a")).perform();
		a.sendKeys(Keys.chord(Keys.CONTROL, "c")).perform();
		Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
		System.out.println(c.getData(DataFlavor.stringFlavor));
		String CurrentElement = (String) c.getData(DataFlavor.stringFlavor);
		return CurrentElement;
	}

	public static void CopyPopupDefaultValue() throws Exception {
		Thread.sleep(2000);
		String Expected = ObjectRepo.driver.findElement(By.xpath(".//table/tbody/tr[1]/td[1]")).getText()
				+ " - Copy";
		Thread.sleep(1000);
		ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.createduplicate(field,row)']")).click();
		SoftAssert sf = new SoftAssert();
		Thread.sleep(1000);
		if (ObjectRepo.driver
				.findElements(By.xpath("//*[@class='sweet-alert cross-button show-input showSweetAlert visible']"))
				.size() != 0) {
			Thread.sleep(3000);
			WebElement txtFName = ObjectRepo.driver.findElement(By.xpath("//*[@tabindex='3' and @type='text']"));
			Actions a = new Actions(ObjectRepo.driver);
			a.sendKeys(txtFName, Keys.chord(Keys.CONTROL, "a")).perform();
			a.sendKeys(Keys.chord(Keys.CONTROL, "c")).perform();
			Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
			System.out.println(c.getData(DataFlavor.stringFlavor));
			String Actual = (String) c.getData(DataFlavor.stringFlavor);
			System.out.println("Expected: " + Expected + " and Actual:  " + Actual);
			
			sf.assertEquals(true, true);
		}
		sf.assertAll();
	}

	public static void CancelButton() throws Exception {
		Thread.sleep(1000);
		ObjectRepo.driver.findElement(By.xpath("//*[@class='cancel']")).click();
	}

	public static void OKButton() throws Exception {
		Thread.sleep(1000);
		ObjectRepo.driver.findElement(By.xpath("//*[@class='confirm']")).click();
	}

	public static String VerifyConfirmMsg() throws Exception {
		Thread.sleep(1000);
		String actualmes = ObjectRepo.driver.findElement(By.xpath("//*[contains(text(),'Nice job!')]")).getText();
		return actualmes;
	}

	public static void CreateCopy() throws Exception {
		Thread.sleep(1000);
		// Click on Action button to Create Copy
		ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.createduplicate(field,row)']")).click();
		String Expected = ObjectRepo.driver.findElement(By.xpath(".//table/tbody/tr[1]/td[1]")).getText();
		Thread.sleep(1000);

		if (ObjectRepo.driver
				.findElements(By.xpath("//*[@class='sweet-alert cross-button show-input showSweetAlert visible']"))
				.size() != 0) {

			Thread.sleep(1000);
			WebElement txtFName = ObjectRepo.driver.findElement(By.xpath("//*[@tabindex='3' and @type='text']"));

			Actions a = new Actions(ObjectRepo.driver);
			a.sendKeys(txtFName, Keys.chord(Keys.CONTROL, "a")).perform();
			a.sendKeys(Keys.chord(Keys.CONTROL, "c")).perform();
			Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
			System.out.println(c.getData(DataFlavor.stringFlavor));
			String Actual = (String) c.getData(DataFlavor.stringFlavor);
			Thread.sleep(1000);
			OKButton();
			Thread.sleep(1000);

			if (ObjectRepo.driver.findElements(By.xpath("//*[contains(text(),'must be unique.')]")).size() != 0) {
				//ExtentBase.reportLable("Item is not unique. Hence not able to create copy.");
				CancelButton();
				Thread.sleep(1000);
			} else {
				String actualPopupMsg = VerifyConfirmMsg();
				Thread.sleep(1000);
				assertEquals(actualPopupMsg, "Nice job! The copy of " + Expected + " was generated successfully.");
				OKButton();
				ObjectRepo.driver.findElement(By.xpath("//*[@ng-model='vm.textSearch']")).sendKeys(Actual);
				Thread.sleep(3000);
				String Pagination = ObjectRepo.driver
						.findElement(By.xpath("//*[contains(normalize-space(text()),'Showing')]")).getText();
				assertEquals("Showing 1 to 1 of total 1 entries", Pagination);
				Thread.sleep(2000);
				ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.clearSearchText()']")).click();
			}
		}
	}

	/*public static void CreateCopyAppBuilder(String Role) throws Exception {
		SoftAssert sf = new SoftAssert();
		Thread.sleep(1000);
		// Click on Action button to Create Copy
		ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.createduplicate(field,row)']")).click();
		String Expected = ObjectRepo.driver.findElement(By.xpath(".//table/tbody/tr[1]/td[1]")).getText();
		Thread.sleep(1000);

		if (ObjectRepo.driver
				.findElements(By.xpath("//*[@class='sweet-alert cross-button show-input showSweetAlert visible']"))
				.size() != 0) {

			Thread.sleep(1000);
			WebElement txtFName = ObjectRepo.driver.findElement(By.xpath("//*[@tabindex='3' and @type='text']"));
			Actions a = new Actions(ObjectRepo.driver);
			a.sendKeys(txtFName, Keys.chord(Keys.CONTROL, "a")).perform();
			a.sendKeys(Keys.chord(Keys.CONTROL, "c")).perform();
			Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
			System.out.println(c.getData(DataFlavor.stringFlavor));
			String Actual = (String) c.getData(DataFlavor.stringFlavor);
			Thread.sleep(1000);
			OKButton();
			Thread.sleep(1000);

			if (ObjectRepo.driver.findElements(By.xpath("//*[contains(text(),'must be unique.')]")).size() != 0) {
				//ExtentBase.reportLable("Item is not unique. Hence not able to create copy.");
				CancelButton();
				Thread.sleep(1000);
			} else {
				String actualPopupMsg = VerifyConfirmMsg();
				Thread.sleep(1000);
				assertEquals(actualPopupMsg, "Nice job! The copy of " + Expected + " was generated successfully.");
				OKButton();
				Thread.sleep(2000);
				String val = AppBuilder_Page.breadCrumtxtAppBuilder(ObjectRepo.driver).getText();
				assertEquals(val, "Build App");
				Thread.sleep(2000);
				AppBuilder_Action.rolecheck(Role);
				ObjectRepo.driver.findElement(By.xpath("//*[@ng-model='vm.textSearch']")).sendKeys(Actual);
				Thread.sleep(2000);
				String Pagination = ObjectRepo.driver
						.findElement(By.xpath("//*[contains(normalize-space(text()),'Showing')]")).getText();

				sf.assertEquals("Showing 1 to 1 of total 1 entries", Pagination);

				Thread.sleep(1000);
				ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.clearSearchText()']")).click();
			}
		}
		sf.assertAll();*/
	}

