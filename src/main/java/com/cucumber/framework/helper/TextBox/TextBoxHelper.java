/**
 * @author rahul.rathore
 *	
 *	07-Aug-2016
 */
package com.cucumber.framework.helper.TextBox;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.framework.helper.Generic.GenericHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;

/**
 * @author vishal bhut
 */
public class TextBoxHelper extends GenericHelper {

	private WebDriver driver;
	private Logger oLog = LoggerHelper.getLogger(TextBoxHelper.class);
	private GenericHelper geneHelpObj;
	private WaitHelper waitObj;
	WebDriverWait wait =new WebDriverWait (ObjectRepo.driver,60);
	public TextBoxHelper(WebDriver driver) {
		super(driver);
		this.driver = driver;
		oLog.debug("TextBoxHelper : " + this.driver.hashCode());
		geneHelpObj = new GenericHelper(driver); 
		waitObj = new WaitHelper(driver, ObjectRepo.reader);
	}


	// -----------------------
	// public void sendKeys(By locator,String value) {
	// oLog.info("Locator : " + locator + " Value : " + value);
	// getElement(locator).sendKeys(value);
	// }


	public void sendKeys(WebElement locator, String value) {
		oLog.info("Locator : " + locator.getText() + " Value : " + value);
		waitObj.waitForElementVisible(locator);
		getElement(geneHelpObj.locatorParser(geneHelpObj.getLocatorFromWebElement(locator))).sendKeys(value);
	}

	public void clear(WebElement locator) {
		oLog.info("Locator : " + locator);
		waitObj.waitForElementVisible(locator);
		getElement(geneHelpObj.locatorParser(geneHelpObj.getLocatorFromWebElement(locator))).clear();
	}

	public String getText(WebElement locator) {
		oLog.info("Locator : " + locator);
		waitObj.waitForElementVisible(locator);
		return getElement(geneHelpObj.locatorParser(geneHelpObj.getLocatorFromWebElement(locator))).getText();
	}

	public void clearAndSendKeys(WebElement locator, String value) {
		waitObj.waitForElementVisible(locator);
		WebElement element = getElement(geneHelpObj.locatorParser(geneHelpObj.getLocatorFromWebElement(locator)));
		element.clear();
		element.sendKeys(value);
		oLog.info("Locator : " + locator + " Value : " + value);
	}
	
	public boolean isEnabled(WebElement locator) {
		oLog.info("Locator : " + locator);
		waitObj.waitForElementVisible(locator);
		return getElement(geneHelpObj.locatorParser(geneHelpObj.getLocatorFromWebElement(locator))).isEnabled();
	}

}
