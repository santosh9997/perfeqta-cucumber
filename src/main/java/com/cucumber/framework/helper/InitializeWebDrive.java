/**
 * rsr 
 *
 *Aug 5, 2016
 */
package com.cucumber.framework.helper;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.cucumber.framework.configreader.PropertyFileReader;
import com.cucumber.framework.configuration.browser.BrowserType;
import com.cucumber.framework.configuration.browser.ChromeBrowser;
import com.cucumber.framework.configuration.browser.FirefoxBrowser;
import com.cucumber.framework.configuration.browser.IExploreBrowser;
import com.cucumber.framework.configuration.browser.PhantomJsBrowser;
import com.cucumber.framework.exception.NoSutiableDriverFoundException;
import com.cucumber.framework.helper.Generic.GenericHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.settings.ObjectRepo;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

/**
 * @author vishal bhut
 */

public class InitializeWebDrive {

	private Logger oLog = LoggerHelper.getLogger(InitializeWebDrive.class);

	public InitializeWebDrive(PropertyFileReader reader) {
		ObjectRepo.reader = reader;
	}

	WebDriver driverIncong;

	public WebDriver standAloneStepUp(BrowserType bType) throws Exception {
		try {
			oLog.info(bType);

			switch (bType) {

			case Chrome:
				ChromeBrowser chrome = ChromeBrowser.class.newInstance();
				return chrome.getChromeDriver(chrome.getChromeCapabilities());

			case Firefox:
				FirefoxBrowser firefox = FirefoxBrowser.class.newInstance();
				return firefox.getFirefoxDriver(firefox.getFirefoxCapabilities());

			/*
			 * case HtmlUnitDriver: HtmlUnitBrowser htmlUnit =
			 * HtmlUnitBrowser.class.newInstance(); return
			 * htmlUnit.getHtmlUnitDriver(htmlUnit .getHtmlUnitDriverCapabilities());
			 */

			case Iexplorer:
				IExploreBrowser iExplore = IExploreBrowser.class.newInstance();
				return iExplore.getIExplorerDriver(iExplore.getIExplorerCapabilities());

			case PhantomJs:
				PhantomJsBrowser jsBrowser = PhantomJsBrowser.class.newInstance();
				return jsBrowser.getPhantomJsDriver(jsBrowser.getPhantomJsService(),
						jsBrowser.getPhantomJsCapability());

			default:
				throw new NoSutiableDriverFoundException(" Driver Not Found : " + ObjectRepo.reader.getBrowser());
			}
		} catch (Exception e) {
			oLog.equals(e);
			throw e;
		}
	}

	@Before({ "~@firefox", "~@chrome", "~@phantomjs", "~@iexplorer" })
	public void before() throws Exception {
		setUpDriver(ObjectRepo.reader.getBrowser());
		oLog.info(ObjectRepo.reader.getBrowser());
	}

	@After({ "~@firefox", "~@chrome", "~@phantomjs", "~@iexplorer" })
	public void after(Scenario scenario) throws Exception {
		tearDownDriver(scenario);
		oLog.info("");
	}

	@Before(order = 4, value = { "@iexplorer" })
	public void beforeExplorer() throws Exception {
		setUpDriver(BrowserType.Iexplorer);
		oLog.info(BrowserType.Iexplorer);
	}

	@After(order = 4, value = { "@iexplorer" })
	public void afterExplorer(Scenario scenario) throws Exception {
		tearDownDriver(scenario);
		oLog.info("");
	}

	@Before(order = 3, value = { "@firefox" })
	public void beforeFirefox() throws Exception {
		setUpDriver(BrowserType.Firefox);
		oLog.info(BrowserType.Firefox);
	}

	@After(order = 3, value = { "@firefox" })
	public void afterFirefox(Scenario scenario) throws Exception {
		tearDownDriver(scenario);
		oLog.info("");
	}

	@Before(order = 2, value = { "@chrome" })
	public void beforeChrome() throws Exception {
		setUpDriver(BrowserType.Chrome);
		oLog.info(BrowserType.Chrome);
	}

	@After(order = 2, value = { "@chrome" })
	public void afterChrome(Scenario scenario) throws Exception {
		tearDownDriver(scenario);
		oLog.info("");
	}

	@Before(order = 1, value = { "@phantomjs" })
	public void beforePhantomjs() throws Exception {
		setUpDriver(BrowserType.PhantomJs);
		oLog.info(BrowserType.PhantomJs);
	}

	@After(order = 1, value = { "@phantomjs" })
	public void afterPhantomjs(Scenario scenario) throws Exception {
		tearDownDriver(scenario);
		oLog.info("");
	}

	public void setUpDriver(BrowserType bType) throws Exception {
		ObjectRepo.driver = standAloneStepUp(bType);
		oLog.debug("InitializeWebDrive : " + ObjectRepo.driver.hashCode());
		ObjectRepo.driver.manage().timeouts().pageLoadTimeout(ObjectRepo.reader.getPageLoadTimeOut(), TimeUnit.SECONDS);
		ObjectRepo.driver.manage().timeouts().implicitlyWait(ObjectRepo.reader.getImplicitWait(), TimeUnit.SECONDS);
		ObjectRepo.driver.manage().window().maximize();

	}

	public void setUpDriverInconginto() throws Exception {

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--incognito");
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		driverIncong = new ChromeDriver(capabilities);
	}

	public void tearDownDriver(Scenario scenario) throws Exception {

		try {
			if (ObjectRepo.driver != null) {

				if (scenario.isFailed())

					// below line will display the path of screenshot file.
					scenario.write(new GenericHelper(ObjectRepo.driver).takeScreenShot(scenario.getName()));

				// below line will display the screenshot in HTML report.
				scenario.embed(new GenericHelper(ObjectRepo.driver).takeScreenShotBytes(), "image/png");

				tearDownLogout();

				tearDownBrowser();

				oLog.info("Shutting Down the driver");
			}
		} catch (Exception e) {
			tearDownBrowser();
			oLog.error(e);
			throw e;
		}
	}

	public void tearDownLogout() throws InterruptedException {

		if (ObjectRepo.driver.findElements(By.xpath("//*[@class='header-username-bx ng-binding']")).size() == 1) {

			// this code will perform logout 
			System.out.println("loging out from site");
			ObjectRepo.driver.findElement(By.xpath("//*[@class='header-username-bx ng-binding']")).click();
			Thread.sleep(1000);
			ObjectRepo.driver.findElement(By.xpath("//*[@ui-sref='logout']")).click();
			Thread.sleep(2000);
		} 

		System.out.println("this is logout function");
	}

	public void tearDownBrowser() {
		ObjectRepo.driver.quit();
		ObjectRepo.reader = null;
		ObjectRepo.driver = null;
	}

}
