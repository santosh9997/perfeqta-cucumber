package com.cucumber.framework.helper.Search;

import static org.testng.Assert.assertEquals;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;

/**
 * @author vishal bhut
 */
public class Search {

//	public Search sObj;
//	
//	public Search() {
//			sObj = new Search();
//	}
	private WaitHelper waitObj;
	public void SearchVerification(String PaginationText, String PaginationSelectedVal, WebElement Grid,
			String SearchVal, WebElement seachElement, String searchPagination) throws Exception {
		
		waitObj = new WaitHelper(ObjectRepo.driver, ObjectRepo.reader);
		
		Thread.sleep(500);
		System.out.println("Inside Pagination for Sorting");

		String Pagination = PaginationText;

		System.out.println("Pagination Text:" + Pagination);

		String PaginationDrp = PaginationSelectedVal;

		String[] splited = Pagination.split("\\s+");
		if (PaginationDrp.equalsIgnoreCase("")) {
			PaginationDrp = "1";
			System.out.println("Pagination Dropdown not available");
		}

		int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
				TotalEntry = Integer.parseInt(splited[6]), PageSizeDrp = Integer.parseInt(PaginationDrp);

		System.out.println("FromEntry:" + FromEntry + "ToEntry:" + ToEntry + "TotalEntry" + TotalEntry + "PaginationDrp"
				+ PageSizeDrp);

		ArrayList<String> obtainedList = new ArrayList<>();
		ArrayList<String> sortedList = new ArrayList<>();

		Thread.sleep(500);
		if (TotalEntry <= 10) {
			PageSizeDrp = 1;
		}
		for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {
			Thread.sleep(500);
			WebElement GridElement = Grid; // Replace TableID with Actual Table ID or Xpath
			WebElement Webtable = GridElement;

			List<WebElement> TotalColumnCount = Webtable.findElements(
					By.xpath("/html/body/div[1]/div/ui-view/ui-view/div/div[2]/grid/div[2]/div/table/thead/tr/th"));
			System.out.println("No. of Columns in the WebTable: " + TotalColumnCount.size());
			List<WebElement> TotalRowForLoop = Webtable.findElements(
					By.xpath("//*[@class='table table-hover table-striped table-bordered table-condensed']/tbody/tr"));

			// Now we will Iterate the Table and print the Values
			int ColumnIndex = 1;
			for (WebElement colElement : TotalRowForLoop) {
				List<WebElement> TotalRowCount = colElement.findElements(
						By.xpath("/html/body/div[1]/div/ui-view/ui-view/div/div[2]/grid/div[2]/div/table/tbody/tr["
								+ ColumnIndex + "]/td[1]"));
				System.out.println("No. of rows in the WebTable: " + TotalRowCount.size());
				int RowIndex = 1;
				for (WebElement rowElement : TotalRowCount) {
					System.out.println(
							"Row " + RowIndex + " Column " + ColumnIndex + "Cell Data :  " + rowElement.getText());
					ColumnIndex = ColumnIndex + 1;
					obtainedList.add(rowElement.getText());
					sortedList.add(rowElement.getText());
					ActualRowCount = ActualRowCount + TotalRowCount.size();
				}

				RowIndex = RowIndex + 1;
			}

			// Click Next button
			if (TotalEntry > 10) {
				WebElement linkElement = ObjectRepo.driver
						.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
				System.out.println(linkElement.getText());
				((JavascriptExecutor) ObjectRepo.driver).executeScript("arguments[0].click();", linkElement);

			}

		}

		Iterator<String> itr = obtainedList.iterator();
		int TotalMatch = 0;
		while (itr.hasNext()) {
			String CurrentVal = (String) itr.next();

			if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(CurrentVal, SearchVal)) {
				System.out.println("Match record found:" + CurrentVal);

				TotalMatch++;
			}
		}
		System.out.println("TotalMatch: " + TotalMatch);

		//Thread.sleep(3000);
		//WebDriverWait wait = new WebDriverWait(ObjectRepo.driver, 10);
		//wait.until(ExpectedConditions.visibilityOf(seachElement));
		//waitObj.waitForElementVisible(seachElement);
		seachElement.sendKeys(SearchVal);
		Thread.sleep(3000);
		if (ObjectRepo.driver.findElements(By.xpath("//*[contains(text(),'No Records Found.')]")).size() != 0) {

			System.out.println("---------------No reocrds found------------------------------");

		} else {

			// check page value
			//Thread.sleep(3000);
			waitObj.waitForElementVisible(ObjectRepo.driver.findElement(By.xpath("//*[@class='paddingLeft-15px ng-binding']")));
			String aftersrch = ObjectRepo.driver.findElement(By.xpath("//*[@class='paddingLeft-15px ng-binding']"))
					.getText();
			System.out.println("Pagination Text:" + aftersrch);
			String[] splited1 = aftersrch.split("\\s+");

			int foundResult = Integer.parseInt(splited1[6]);

			System.out.println("found Result after search: " + foundResult);
			Assert.assertEquals(TotalMatch, foundResult);
			System.out.println("---------------------------------------------");
			System.out.println("Successfully: Verify Search result");
			System.out.println("---------------------------------------------");
//			VerifyClearSearch(Pagination);
		}
	}

	public void SiteSearchVerification(String PaginationText, String PaginationSelectedVal, WebElement Grid,
			String SearchVal, WebElement seachElement, String searchPagination) throws Exception {
		Thread.sleep(1000);
		System.out.println("Inside Pagination for Sorting");

		String Pagination = PaginationText;
		System.out.println("Pagination Text:" + Pagination);
		String PaginationDrp = PaginationSelectedVal;
		String[] splited = Pagination.split("\\s+");
		if (PaginationDrp.equalsIgnoreCase("")) {
			PaginationDrp = "1";
			System.out.println("Pagination Dropdown not available");
		}
		int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
				TotalEntry = Integer.parseInt(splited[6]), PageSizeDrp = Integer.parseInt(PaginationDrp);

		System.out.println("FromEntry:" + FromEntry + "ToEntry:" + ToEntry + "TotalEntry" + TotalEntry + "PaginationDrp"
				+ PageSizeDrp);

		ArrayList<String> obtainedList = new ArrayList<>();
		ArrayList<String> sortedList = new ArrayList<>();
		Thread.sleep(2000);
		if (TotalEntry <= 10) {
			PageSizeDrp = 1;
		}
		for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {
			Thread.sleep(1000);
			WebElement GridElement = Grid; // Replace TableID with Actual Table ID or Xpath
			WebElement Webtable = GridElement;

			List<WebElement> TotalColumnCount = Webtable.findElements(By.xpath(
					"/html/body/div[1]/div/ui-view/ui-view/div/div[2]/div[2]/grid/div[2]/div/table/thead/tr/th"));
			System.out.println("No. of Columns in the WebTable: " + TotalColumnCount.size());
			List<WebElement> TotalRowForLoop = Webtable.findElements(
					By.xpath("//*[@class='table table-hover table-striped table-bordered table-condensed']/tbody/tr"));

			// Now we will Iterate the Table and print the Values
			int ColumnIndex = 1;
			for (WebElement colElement : TotalRowForLoop) {
				List<WebElement> TotalRowCount = colElement.findElements(By
						.xpath("/html/body/div[1]/div/ui-view/ui-view/div/div[2]/div[2]/grid/div[2]/div/table/tbody/tr["
								+ +ColumnIndex + "]/td[1]"));
				System.out.println("No. of rows in the WebTable: " + TotalRowCount.size());
				int RowIndex = 1;
				for (WebElement rowElement : TotalRowCount) {
					System.out.println(
							"Row " + RowIndex + " Column " + ColumnIndex + "Cell Data :  " + rowElement.getText());
					ColumnIndex = ColumnIndex + 1;
					obtainedList.add(rowElement.getText());
					sortedList.add(rowElement.getText());
					ActualRowCount = ActualRowCount + TotalRowCount.size();
				}

				RowIndex = RowIndex + 1;
			}

			// Click Next button
			if (TotalEntry > 10) {
				WebElement linkElement = ObjectRepo.driver
						.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
				System.out.println(linkElement.getText());
				((JavascriptExecutor) ObjectRepo.driver).executeScript("arguments[0].click();", linkElement);
			}
			Thread.sleep(1000);
		}

		Iterator<String> itr = obtainedList.iterator();
		int TotalMatch = 0;
		while (itr.hasNext()) {
			String CurrentVal = (String) itr.next();

			if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(CurrentVal, SearchVal)) {
				System.out.println("Match record found:" + CurrentVal);

				TotalMatch++;
			}
		}
		System.out.println("TotalMatch: " + TotalMatch);
		// ObjectRepo.driver.navigate().refresh();

		Thread.sleep(3000);
		// search value

		WebDriverWait wait = new WebDriverWait(ObjectRepo.driver, 10);
		wait.until(ExpectedConditions.visibilityOf(seachElement));

		seachElement.sendKeys(SearchVal);
		Thread.sleep(3000);
		if (ObjectRepo.driver.findElements(By.xpath("//*[contains(text(),'No Records Found.')]")).size() != 0) {

			System.out.println("---------------No reocrds found------------------------------");
		} else {
			// check page value
			Thread.sleep(3000);
			String aftersrch = ObjectRepo.driver.findElement(By.xpath("//*[@class='paddingLeft-15px ng-binding']"))
					.getText();
			System.out.println("Pagination Text:" + aftersrch);
			String[] splited1 = aftersrch.split("\\s+");

			int foundResult = Integer.parseInt(splited1[6]);

			System.out.println("found Result after search: " + foundResult);
			Assert.assertEquals(TotalMatch, foundResult);
			System.out.println("---------------------------------------------");
			System.out.println("Successfully: Verify Search result");
			System.out.println("---------------------------------------------");

			VerifyClearSearch(Pagination);
		}
	}

	public void ImportDataSearchVerification(String PaginationText, String PaginationSelectedVal,
			WebElement Grid, String SearchVal, WebElement seachElement, String searchPagination) throws Exception {

		Thread.sleep(1000);
		System.out.println("Inside Pagination for Sorting");

		String Pagination = PaginationText;

		System.out.println("Pagination Text:" + Pagination);

		String PaginationDrp = PaginationSelectedVal;

		String[] splited = Pagination.split("\\s+");
		if (PaginationDrp.equalsIgnoreCase("")) {
			PaginationDrp = "1";
			System.out.println("Pagination Dropdown not available");
		}

		int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
				TotalEntry = Integer.parseInt(splited[6]), PageSizeDrp = Integer.parseInt(PaginationDrp);

		System.out.println("FromEntry:" + FromEntry + "ToEntry:" + ToEntry + "TotalEntry" + TotalEntry + "PaginationDrp"
				+ PageSizeDrp);

		ArrayList<String> obtainedList = new ArrayList<>();
		ArrayList<String> sortedList = new ArrayList<>();

		// ObjectRepo.driver.findElement((By.xpath("//*[@ng-click='vm.sort(field)'
		// and text()='"+ColumnName+"']"))).click();

		Thread.sleep(2000);
		if (TotalEntry <= 10) {
			PageSizeDrp = 1;
		}
		for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {
			Thread.sleep(1000);
			WebElement GridElement = Grid; // Replace TableID with Actual Table ID or Xpath
			WebElement Webtable = GridElement;

			List<WebElement> TotalColumnCount = Webtable.findElements(
					By.xpath("/html/body/div/div/ui-view/ui-view/div/div/div[2]/div/grid/div[2]/div/table/thead/tr"));
			System.out.println("No. of Columns in the WebTable: " + TotalColumnCount.size());
			List<WebElement> TotalRowForLoop = Webtable.findElements(
					By.xpath("//*[@class='table table-hover table-striped table-bordered table-condensed']/tbody/tr"));

			// Now we will Iterate the Table and print the Values
			int ColumnIndex = 1;
			for (WebElement colElement : TotalRowForLoop) {
				List<WebElement> TotalRowCount = colElement.findElements(
						By.xpath("/html/body/div/div/ui-view/ui-view/div/div/div[2]/div/grid/div[2]/div/table/tbody/tr["
								+ ColumnIndex + "]/td[1]"));
				System.out.println("No. of rows in the WebTable: " + TotalRowCount.size());
				int RowIndex = 1;
				for (WebElement rowElement : TotalRowCount) {
					System.out.println(
							"Row " + RowIndex + " Column " + ColumnIndex + "Cell Data :  " + rowElement.getText());
					ColumnIndex = ColumnIndex + 1;
					obtainedList.add(rowElement.getText());
					sortedList.add(rowElement.getText());
					ActualRowCount = ActualRowCount + TotalRowCount.size();
				}

				RowIndex = RowIndex + 1;
			}

			// Click Next button
			if (TotalEntry > 10) {
				WebElement linkElement = ObjectRepo.driver
						.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
				System.out.println(linkElement.getText());
				((JavascriptExecutor) ObjectRepo.driver).executeScript("arguments[0].click();", linkElement);
			}
			Thread.sleep(1000);
		}
		Iterator<String> itr = obtainedList.iterator();
		int TotalMatch = 0;
		while (itr.hasNext()) {
			String CurrentVal = (String) itr.next();

			if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(CurrentVal, SearchVal)) {
				System.out.println("Match record found:" + CurrentVal);

				TotalMatch++;
			}
		}
		System.out.println("TotalMatch: " + TotalMatch);

		Thread.sleep(3000);
		// search value

		WebDriverWait wait = new WebDriverWait(ObjectRepo.driver, 10);
		wait.until(ExpectedConditions.visibilityOf(seachElement));

		seachElement.sendKeys(SearchVal);
		Thread.sleep(3000);
		if (ObjectRepo.driver.findElements(By.xpath("//*[contains(text(),'No Records Found.')]")).size() != 0) {

			System.out.println("---------------No reocrds found------------------------------");
		} else {
			// check page value
			Thread.sleep(3000);
			String aftersrch = ObjectRepo.driver.findElement(By.xpath("//*[@class='paddingLeft-15px ng-binding']"))
					.getText();
			System.out.println("Pagination Text:" + aftersrch);
			String[] splited1 = aftersrch.split("\\s+");

			int foundResult = Integer.parseInt(splited1[6]);

			System.out.println("found Result after search: " + foundResult);

			Assert.assertEquals(TotalMatch, foundResult);

			System.out.println("---------------------------------------------");
			System.out.println("Successfully: Verify Search result");
			System.out.println("---------------------------------------------");
			VerifyClearSearch(Pagination);
		}
	}

	public void VerifyClearSearch(String PaginationText) throws Exception {

		Thread.sleep(1000);

		ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.clearSearchText()']")).click();

		Thread.sleep(1000);

		String CurrentPagination = ObjectRepo.driver
				.findElement(By.xpath("//*[contains(normalize-space(text()),'Showing')]")).getText();
		assertEquals(PaginationText, CurrentPagination);

		Thread.sleep(1000);
	}
}