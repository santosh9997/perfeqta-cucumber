package com.cucumber.framework.helper.Sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;

import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;
/**
 * @author vishal bhut
 */

public class SortingOnColumn
{	
	public boolean sorting=false;
	private WaitHelper waitObj;

	public void verifySorting(String ColumnName, int ColNumberToSort, String PaginationText,
			String PaginationSelectedVal, WebElement Grid) throws Exception {
		
		waitObj = new WaitHelper(ObjectRepo.driver, ObjectRepo.reader);

		Thread.sleep(1000);
		System.out.println("Inside Pagination for Sorting");

		String Pagination = PaginationText;

		System.out.println("Pagination Text:" + Pagination);

		String PaginationDrp = PaginationSelectedVal;

		String[] splited = Pagination.split("\\s+");

		if (PaginationDrp.equalsIgnoreCase("")) {
			PaginationDrp = "1";
			System.out.println("Pagination Dropdown not available");
		}
		int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
				TotalEntry = Integer.parseInt(splited[6]), PageSizeDrp = Integer.parseInt(PaginationDrp);

		System.out.println("FromEntry:" + FromEntry + "ToEntry:" + ToEntry + "TotalEntry" + TotalEntry + "PaginationDrp"
				+ PageSizeDrp);

		ArrayList<String> obtainedList = new ArrayList<>();
		ArrayList<String> sortedList = new ArrayList<>();

		waitObj.waitForElementVisible(ObjectRepo.driver.findElement((By.xpath("//*[@ng-click='vm.sort(field)' and text()='" + ColumnName + "']"))));
		ObjectRepo.driver.findElement((By.xpath("//*[@ng-click='vm.sort(field)' and text()='" + ColumnName + "']")))
		.click();

		Thread.sleep(2000);
		if (TotalEntry <= 10) {
			PageSizeDrp = 1;
		}
		for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {
			Thread.sleep(1000);
			WebElement GridElement = Grid; // Replace TableID with Actual Table ID or Xpath
			WebElement Webtable = GridElement;

			List<WebElement> TotalColumnCount = Webtable.findElements(
					By.xpath("/html/body/div[1]/div/ui-view/ui-view/div/div[2]/grid/div[2]/div/table/thead/tr/th"));
			System.out.println("No. of Columns in the WebTable: " + TotalColumnCount.size());
			List<WebElement> TotalRowForLoop = Webtable.findElements(
					By.xpath("//*[@class='table table-hover table-striped table-bordered table-condensed']/tbody/tr"));

			// Now we will Iterate the Table and print the Values
			int ColumnIndex = 1;
			for (WebElement colElement : TotalRowForLoop) {
				List<WebElement> TotalRowCount = colElement.findElements(
						By.xpath("/html/body/div[1]/div/ui-view/ui-view/div/div[2]/grid/div[2]/div/table/tbody/tr["
								+ ColumnIndex + "]/td[" + ColNumberToSort + "]"));
				System.out.println("No. of rows in the WebTable: " + TotalRowCount.size());
				int RowIndex = 1;
				for (WebElement rowElement : TotalRowCount) {
					System.out.println(
							"Row " + RowIndex + " Column " + ColumnIndex + "Cell Data :  " + rowElement.getText());
					ColumnIndex = ColumnIndex + 1;
					obtainedList.add(rowElement.getText());
					sortedList.add(rowElement.getText());
					ActualRowCount = ActualRowCount + TotalRowCount.size();
				}

				RowIndex = RowIndex + 1;
			}

			// Click Next button
			if (TotalEntry > 10) {
				WebElement linkElement = ObjectRepo.driver
						.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
				System.out.println(linkElement.getText());
				((JavascriptExecutor) ObjectRepo.driver).executeScript("arguments[0].click();", linkElement);

			}
			Thread.sleep(1000);
		}
		Collections.sort(sortedList);

		//ObjectRepo.driver.navigate().refresh();
		if (obtainedList.equals(sortedList))
		{
			sorting=true;
			System.out.println("Sorting is Proper");
		} 
		else
		{
			sorting=false;
			System.out.println("Sorting is not Proper");
		}
		
		Assert.assertEquals(sorting, true);
	}

	public void SiteSorting(String ColumnName, int ColNumberToSort, String PaginationText,
			String PaginationSelectedVal, WebElement Grid) throws Exception {
		Thread.sleep(1000);
		System.out.println("Inside Pagination for Sorting");

		String Pagination = PaginationText;

		System.out.println("Pagination Text:" + Pagination);

		String PaginationDrp = PaginationSelectedVal;

		String[] splited = Pagination.split("\\s+");

		if (PaginationDrp.equalsIgnoreCase("")) {
			PaginationDrp = "1";
			System.out.println("Pagination Dropdown not available");
		}
		int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
				TotalEntry = Integer.parseInt(splited[6]), PageSizeDrp = Integer.parseInt(PaginationDrp);

		System.out.println("FromEntry:" + FromEntry + "ToEntry:" + ToEntry + "TotalEntry" + TotalEntry + "PaginationDrp"
				+ PageSizeDrp);

		ArrayList<String> obtainedList = new ArrayList<>();
		ArrayList<String> sortedList = new ArrayList<>();

		ObjectRepo.driver.findElement((By.xpath("//*[@ng-click='vm.sort(field)' and text()='" + ColumnName + "']")))
		.click();

		Thread.sleep(2000);
		if (TotalEntry <= 10) {
			PageSizeDrp = 1;
		}
		for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {
			Thread.sleep(1000);
			WebElement GridElement = Grid; // Replace TableID with Actual Table ID or Xpath
			WebElement Webtable = GridElement;

			List<WebElement> TotalColumnCount = Webtable.findElements(By.xpath(
					"/html/body/div[1]/div/ui-view/ui-view/div/div[2]/div[2]/grid/div[2]/div/table/thead/tr/th"));
			System.out.println("No. of Columns in the WebTable: " + TotalColumnCount.size());
			List<WebElement> TotalRowForLoop = Webtable.findElements(
					By.xpath("//*[@class='table table-hover table-striped table-bordered table-condensed']/tbody/tr"));

			// Now we will Iterate the Table and print the Values
			int ColumnIndex = 1;
			for (WebElement colElement : TotalRowForLoop) {
				List<WebElement> TotalRowCount = colElement.findElements(By
						.xpath("/html/body/div[1]/div/ui-view/ui-view/div/div[2]/div[2]/grid/div[2]/div/table/tbody/tr["
								+ ColumnIndex + "]/td[" + ColNumberToSort + "]"));
				System.out.println("No. of rows in the WebTable: " + TotalRowCount.size());
				int RowIndex = 1;
				for (WebElement rowElement : TotalRowCount) {
					System.out.println(
							"Row " + RowIndex + " Column " + ColumnIndex + "Cell Data :  " + rowElement.getText());
					ColumnIndex = ColumnIndex + 1;
					obtainedList.add(rowElement.getText());
					sortedList.add(rowElement.getText());
					ActualRowCount = ActualRowCount + TotalRowCount.size();
				}

				RowIndex = RowIndex + 1;
			}

			// Click Next button
			if (TotalEntry > 10) {
				WebElement linkElement = ObjectRepo.driver
						.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
				System.out.println(linkElement.getText());
				((JavascriptExecutor) ObjectRepo.driver).executeScript("arguments[0].click();", linkElement);

			}
			Thread.sleep(1000);
		}
		Collections.sort(sortedList);

		//ObjectRepo.driver.navigate().refresh();
		if (obtainedList.equals(sortedList)) {
			System.out.println("Sorting is Proper");
		} else {
			System.out.println("Sorting is not Proper");
		}
	}

	public void ImportDataSorting(String ColumnName, int ColNumberToSort, String PaginationText,
			String PaginationSelectedVal, WebElement Grid) throws Exception {

		Thread.sleep(1000);
		System.out.println("Inside Pagination for Sorting");

		String Pagination = PaginationText;

		System.out.println("Pagination Text:" + Pagination);

		String PaginationDrp = PaginationSelectedVal;

		String[] splited = Pagination.split("\\s+");

		if (PaginationDrp.equalsIgnoreCase("")) {
			PaginationDrp = "1";
			System.out.println("Pagination Dropdown not available");
		}
		int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
				TotalEntry = Integer.parseInt(splited[6]), PageSizeDrp = Integer.parseInt(PaginationDrp);

		System.out.println("FromEntry:" + FromEntry + "ToEntry:" + ToEntry + "TotalEntry" + TotalEntry + "PaginationDrp"
				+ PageSizeDrp);

		ArrayList<String> obtainedList = new ArrayList<>();
		ArrayList<String> sortedList = new ArrayList<>();

		ObjectRepo.driver.findElement((By.xpath("//*[@ng-click='vm.sort(field)' and text()='" + ColumnName + "']")))
		.click();

		Thread.sleep(2000);
		if (TotalEntry <= 10) {
			PageSizeDrp = 1;
		}
		for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {
			Thread.sleep(1000);
			WebElement GridElement = Grid; // Replace TableID with Actual Table ID or Xpath
			WebElement Webtable = GridElement;

			List<WebElement> TotalColumnCount = Webtable.findElements(
					By.xpath("/html/body/div/div/ui-view/ui-view/div/div/div[2]/div/grid/div[2]/div/table/thead/tr"));
			System.out.println("No. of Columns in the WebTable: " + TotalColumnCount.size());
			List<WebElement> TotalRowForLoop = Webtable.findElements(
					By.xpath("//*[@class='table table-hover table-striped table-bordered table-condensed']/tbody/tr"));

			// Now we will Iterate the Table and print the Values
			int ColumnIndex = 1;
			for (WebElement colElement : TotalRowForLoop) {

				List<WebElement> TotalRowCount = colElement.findElements(
						By.xpath("/html/body/div/div/ui-view/ui-view/div/div/div[2]/div/grid/div[2]/div/table/tbody/tr["
								+ ColumnIndex + "]/td[" + ColNumberToSort + "]"));
				System.out.println("No. of rows in the WebTable: " + TotalRowCount.size());
				int RowIndex = 1;
				for (WebElement rowElement : TotalRowCount) {
					System.out.println(
							"Row " + RowIndex + " Column " + ColumnIndex + "Cell Data :  " + rowElement.getText());
					ColumnIndex = ColumnIndex + 1;
					obtainedList.add(rowElement.getText());
					sortedList.add(rowElement.getText());
					ActualRowCount = ActualRowCount + TotalRowCount.size();
				}
				RowIndex = RowIndex + 1;
			}
			// Click Next button
			if (TotalEntry > 10) {
				WebElement linkElement = ObjectRepo.driver
						.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
				System.out.println(linkElement.getText());
				((JavascriptExecutor) ObjectRepo.driver).executeScript("arguments[0].click();", linkElement);
			}
			Thread.sleep(1000);
		}
		Collections.sort(sortedList);

		//ObjectRepo.driver.navigate().refresh();
		if (obtainedList.equals(sortedList)) {
			System.out.println("Sorting is Proper");
		} else {
			System.out.println("Sorting is not Proper");
		}
	}

	public void ListOrderSorting(ArrayList<String> ListItem, String Order) {

		if (Order.equalsIgnoreCase("ascending")) {
			Collections.sort(ListItem);
			for (int i = 0; i < ListItem.size(); i++) {
				System.out.println("---------------Sorted List:----------------");
				System.out.println(ListItem.get(i));
				System.out.println("------------------------------");
			}
		} else {
			Collections.reverse(ListItem);
			for (int i = 0; i < ListItem.size(); i++) {
				System.out.println("---------------Sorted List:----------------");
				System.out.println(ListItem.get(i));
				System.out.println("------------------------------");
			}
		}
	}
}