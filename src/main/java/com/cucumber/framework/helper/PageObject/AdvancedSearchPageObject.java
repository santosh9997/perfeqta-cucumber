package com.cucumber.framework.helper.PageObject;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.CheckBox.CheckBoxOrRadioButtonHelper;	
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Generic.GenericHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.stepdefinition.AdvancedSearch;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.DateTimeHelper;
import com.cucumber.framework.utility.ExcelUtils;

public class AdvancedSearchPageObject extends PageBase {
	private WebDriver driver;
	
	public ButtonHelper btnHelper;
	public CheckBoxOrRadioButtonHelper checkBoxAndBtnHelper;
	public TextBoxHelper textBoxHelper;
	public DropDownHelper drpdwnHelper;
	public GenericHelper genricHelper;
	private Helper helper = new Helper();
	private final Logger log = LoggerHelper.getLogger(AdvancedSearchPageObject.class);
	public String allModuleExpected;
	public String expectedAppName, myFavSaveTime; //= new AppBuilderPageObject(driver).getAppTitle;
	//public AdvancedSearch advSrchObj = new AdvancedSearch();

	public AdvancedSearchPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		checkBoxAndBtnHelper = new CheckBoxOrRadioButtonHelper(driver);
		textBoxHelper = new TextBoxHelper(driver);
		drpdwnHelper = new DropDownHelper(driver);
		genricHelper = new GenericHelper(driver);
//		advSrchObj = new AdvancedSearch();
//		commanfun = new CommonFunctions();
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.advancesearch.searchByApps']")
	public WebElement clickAdvSearchTile;

	@FindBy(how = How.XPATH, using = "//span[@class='currentStep ng-binding']")
	public WebElement advSearchModuleName;

	@FindBy(how = How.XPATH, using = "//span[@ng-bind-html='step.ncyBreadcrumbLabel']")
	public WebElement advSearchBreadcrumbs;

	@FindBy(how = How.XPATH, using = "//div[1]/form/div[1]/ul/li[1]/a")
	public WebElement advSearchLabel;

	@FindBy(how = How.XPATH, using = "//fieldset[@ng-disabled='vm.loadapp']")
	public WebElement displayModule;

	@FindBy(how = How.XPATH, using = "//input[@ng-click='vm.onModuleSelectChange()']")
	public WebElement uncheckAllModule;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='$select.search']")
	public WebElement searchAppTextfield;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.clearApps()']")
	public WebElement clearLink;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Enter one or multiple App names to search']")
	public WebElement clearFunctionality;

	@FindBy(how = How.XPATH, using = "//ul[@class='ui-select-no-choice dropdown-menu']")
	public WebElement NoAppsFoundDrpdwn;

	@FindBy(how = How.XPATH, using = "//span[@class='ui-select-choices-row-inner']")
	public WebElement selectApps;

	@FindBy(how = How.XPATH, using = "//label[@class='ng-binding']")
	public WebElement appNameVerify;
	
	//according v34010
	@FindBy(how = How.XPATH, using = "//*[@ng-if='app.id']")
	public WebElement appNameVerify1;

	@FindBy(how = How.XPATH, using = "//button[@class='btn btn-success ng-binding']")
	public WebElement goBtn;

	@FindBy(how = How.XPATH, using = "/html/body/div/div/ui-view/div/div[3]/div/div[1]/div/div[1]/div[1]/b")
	public WebElement clickAppToCollapse;

	//on v34010
	@FindBy(how = How.XPATH, using = "//div[3]/div/div/div[1]/div/div[1]/div[2]")
	public WebElement clickAppToCollapse1;
	
	@FindBy(how = How.XPATH, using = "//div[@class='col-md-10 col-md-offset-1 alert alert-warning no-records ng-binding']")
	public WebElement noRecordFoundListings;
	
	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'Records Pending Your Review')])[1]")
	public WebElement recordsPendingYourReviewLink;
	
	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'Records You Created Today')])[1]")
	public WebElement recordsYouCreatedTodayLink;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'Records Assigned To You')])[1]")
	public WebElement recordsAssignedToYouLink;
	
	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'Records You Accessed Today')])[1]")
	public WebElement recordsYouAccessedTodayLink;
	
	@FindBy(how = How.XPATH, using = "//div[1]/div/div/div/quick-search/div/div/table/tbody/tr[4]/td/div/span/a")
	public WebElement recordsPendingYourReview;
	
	@FindBy(how = How.XPATH, using = "//div[1]/div/div/div/quick-search/div/div/table/tbody/tr[1]/td/div/span/a")
	public WebElement recordsYouCreatedToday;
	
	@FindBy(how = How.XPATH, using = "//div[1]/div/div/div/quick-search/div/div/table/tbody/tr[3]/td/div/span/a")
	public WebElement recordsAssignedToYou;
	
	@FindBy(how = How.XPATH, using = "//div[1]/div/div/div/quick-search/div/div/table/tbody/tr[2]/td/div/span/a")
	public WebElement recordsYouAccessedToday;
	
	@FindBy(how = How.XPATH, using = "//div[4]/div/div[1]/div[1]/h4")
	public WebElement expandcollapseFilterBy;

	@FindBy(how = How.XPATH, using = "//label[@for='date']")
	public WebElement dateLabelFilterBy;

	@FindBy(how = How.XPATH, using = "//*[contains(text(), 'Batch')]")
	public WebElement verifyBatchLink;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.clear();advanceSearchForm.$setPristine();advanceSearchForm.$setUntouched();']")
	public WebElement clickClearBtn;

	@FindBy(how = How.XPATH, using = "//img[@title='Export as CSV']")
	public WebElement csvIconDisplay;

	@FindBy(how = How.XPATH, using = "//img[@title='Export as Excel']")
	public WebElement excelIconDisplay;

	@FindBy(how = How.XPATH, using = "//img[@title='Export as PDF']")
	public WebElement pdfIconDisplay;

	@FindBy(how = How.XPATH, using = "//button[@title='Click to save your search']")
	public WebElement addToFavoriteBtn;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.saveData(favoriteSearchForm)']")
	public WebElement addToFavoriteSaveBtn;

	@FindBy(how = How.XPATH, using = "//p[@translate='text.FAVORITENAMEISREQUIRED']")
	public WebElement addToFavoriteRequiredMsg;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.closePopup()']")
	public WebElement cancelBtnAddToFavorite;

	@FindBy(how = How.XPATH, using = "//input[@name='searchNameTextbox']")
	public WebElement addToFavoriteTextfield;

	@FindBy(how = How.XPATH, using = "//p[@translate='text.FAVORITENAMESHOULDBEAT2']")
	public WebElement addToFavoriteMinimumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='text.FAVORITENAMESHOULD	TMORE50']")
	public WebElement addToFavoriteMaximumMsg;

	@FindBy(how = How.XPATH, using = "//h4[@class='col-sm-11 ng-binding']")
	public WebElement addToFavoriteLabel;

	@FindBy(how = How.XPATH, using = "//i[@class='glyphicon glyphicon-remove pull-right critearia-01 ng-click-active']")
	public WebElement closeIconAddToFavorite;

	@FindBy(how = How.XPATH, using = "//input[@ng-value='false']")
	public WebElement addToFavoriteRadioBtn;

	@FindBy(how = How.XPATH, using = "//div/div[2]/div/div/div/table/tbody/tr/td[1]/div/span/a")
	public WebElement favoriteSavedSuccess;

	@FindBy(how = How.XPATH, using = "//p[@translate='text.FAVORITENAMEMUSTBEUNIQUE']")
	public WebElement addToFavoriteUniqueMsg;
	
	@FindBy(how = How.XPATH, using = "//label[@ng-click='vm.defaultActiveTab=tab.id']//em[contains(text(),'My Favorites')]")
	public WebElement clickMyFavoriteTable;

	@FindBy(how = How.XPATH, using = "(//tr[@ng-repeat='data in vm.myFavoriteData']//following::tr/td[3])[1]")
	public WebElement removeFromMyFavorite;

	@FindBy(how = How.XPATH, using = "//div[2]/div/div/div/div/div/t")
	public WebElement totlEntyNoMyFavorite;

	@FindBy(how = How.XPATH, using = "//p[@style='display: block;']")
	public WebElement removeMsgMyFavorite;

	@FindBy(how = How.XPATH, using = "//button[@class='confirm']")
	public WebElement removeYesMsgMyFavorite;

	@FindBy(how = How.XPATH, using = "//button[@class='cancel']")
	public WebElement removeNoMsgMyFavorite;

	@FindBy(how = How.XPATH, using = "//label[@ng-click='vm.defaultActiveTab=tab.id']//em[contains(text(),'My Favorites')]")
	public WebElement myFavoritesLabel;

	@FindBy(how = How.XPATH, using = "//strong[@style='font-size: 15px;']")
	public WebElement filterByLabel;

	@FindBy(how = How.XPATH, using = "//div[@class='advance-search-main-icons-bx ng-scope']")
	public WebElement filterByIcons;

	@FindBy(how = How.XPATH, using = "//div[1]/ul/li[1]/a/em")
	public WebElement dateRangeSpelling;

	@FindBy(how = How.XPATH, using = "//div[1]/ul/li[2]/a/em")
	public WebElement attributes_EntitiesSpelling;

	@FindBy(how = How.XPATH, using = "//div[1]/ul/li[3]/a/em")
	public WebElement sitesSpelling;

	@FindBy(how = How.XPATH, using = "//div[1]/ul/li[4]/a/em")
	public WebElement batchEntriesSpelling;

	@FindBy(how = How.XPATH, using = "//div[1]/ul/li[5]/a/em")
	public WebElement assigneeSpelling;

	@FindBy(how = How.XPATH, using = "//div[1]/ul/li[6]/a/em")
	public WebElement recordStatusSpelling;

	@FindBy(how = How.XPATH, using = "//select[@name='qcform']")
	public WebElement dateDrpDwnFilterBy;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.advancesearch.date']")
	public WebElement getDateFilterBy;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.advancesearch.startDate']")
	public WebElement getStartDateFilterBy;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.advancesearch.endDate']")
	public WebElement getEndDateFilterBy;

	@FindBy(how = How.XPATH, using = "//input[@ng-change=\"vm.changeRadioButtonValue('dateRange')\"]")
	public WebElement dateRangeByDefault;

	@FindBy(how = How.XPATH, using = "//a[@ng-click=\"vm.onClearFilters('date')\"]")
	public WebElement clearLinkFilterBy;

	@FindBy(how = How.XPATH, using = "//span[@title='Remove Date Range Filter']")
	public WebElement removeIconFilterBy;

	@FindBy(how = How.XPATH, using = "//*[@for='test1']")
	public WebElement monthYearDrpDwn;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.advancesearch.month']")
	public WebElement selectMonthDrpDwn;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.advancesearch.year']")
	public WebElement selectYearDrpDwn;

	@FindBy(how = How.XPATH, using = "//td[@data-th='Created Date']")
	public WebElement createdDateGrid;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='selectPage(page + 1, $event)']")
	public WebElement nextPagging;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.open1()']")
	public WebElement specificDateIcon;

	@FindBy(how = How.XPATH, using = "//button[@ng-click=\"select('today', $event)\"]")
	public WebElement selectSpecificDate;

	@FindBy(how = How.XPATH, using = "//p[@translate='text.monthisrequired']")
	public WebElement monthDrpDwnRequiredMsg;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='text.yearisrequired']")
	public WebElement yearDrpDwnRequiredMsg;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.attribEtyFilter[$index].attrb']")
	public WebElement attributeDrpDwn;

	@FindBy(how = How.XPATH, using = "//p[@class='err-message ng-binding ng-scope']")
	public WebElement valueRequiredMsg;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addAttribute($index)']")
	public WebElement addAttribute;

	@FindBy(how = How.XPATH, using = "//*[@name='attributeTextbox0']")
	public WebElement attributeValue;

	@FindBy(how = How.XPATH, using = "//span[@title='Remove Attributes/Entity Filter']")
	public WebElement attributesRemoveIcon;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.onClearFilters()']")
	public WebElement attributesClearLink;

	@FindBy(how = How.XPATH, using = "//a[@ng-click=\"vm.onClearFilters('sites')\"]")
	public WebElement sitesClearLink;

	@FindBy(how = How.XPATH, using = "//select[@title='Please select Region']")
	public WebElement sitesRegionDrpDwn;

	@FindBy(how = How.XPATH, using = "//span[@title='Remove Sites Filter']")
	public WebElement sitesRemoveIcon;

	@FindBy(how = How.XPATH, using = "//input[@name='batchEntryId']")
	public WebElement batchEntrytextfield;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.advancesearch.table.BATCHENTRYIDSHOULDNOT20']")
	public WebElement batchEntryMaximumMsg;

	@FindBy(how = How.XPATH, using = "//a[@ng-click=\"vm.onClearFilters('batchEntryId')\"]")
	public WebElement batchEntryClearLink;

	@FindBy(how = How.XPATH, using = "//span[@title='Remove Batch Entries Filter']")
	public WebElement batchEntryRemoveIcon;

	@FindBy(how = How.XPATH, using = "//label[@for='test11']")
	public WebElement assigneeRadioBtn;

	@FindBy(how = How.XPATH, using = "//label[@for='test12']")
	public WebElement assigneeToMeRadioBtn;

	@FindBy(how = How.XPATH, using = "//*[@id='Assignee-Clear-Roles']/div/button")
	public WebElement assigneeRoleDrpdwn;

	@FindBy(how = How.XPATH, using = "//*[@data-ng-click='selectAll()']")
	public WebElement checkAllRoleDrpdwn;

	@FindBy(how = How.XPATH, using = "//input[@checked='checked']")
	public WebElement checkAllFunctioning;

	@FindBy(how = How.XPATH, using = "//a[@data-ng-click='deselectAll();']")
	public WebElement unCheckAllRoleDrpdwn;

	@FindBy(how = How.XPATH, using = "//div[2]/div[2]/div[2]/div/ul/li[1]")
	public WebElement passRecordStatus;

	@FindBy(how = How.XPATH, using = "//div[2]/div[2]/div[2]/div/ul/li[2]")
	public WebElement failRecordStatus;

	@FindBy(how = How.XPATH, using = "//div[2]/div[2]/div[2]/div/ul/li[3]")
	public WebElement reviewPendingRecordStatus;

	@FindBy(how = How.XPATH, using = "//div[2]/div[2]/div[2]/div/ul/li[4]")
	public WebElement voidRecordStatus;

	@FindBy(how = How.XPATH, using = "//div[2]/div[2]/div[2]/div/ul/li[5]")
	public WebElement draftRecordStatus;

	@FindBy(how = How.XPATH, using = "//div[2]/div[2]/div[2]/div/ul/li[6]")
	public WebElement rejectedRecordStatus;

	@FindBy(how = How.XPATH, using = "//span[@title='Remove Record Status Filter']")
	public WebElement recordStatusRemoveIcon;

	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[2]/div/a")
	public WebElement viewLinkApp;

	@FindBy(how = How.XPATH, using = "(//*[@class='entryFormGroupOverwrite ng-binding'])[1]")
	public WebElement viewLinkAppTitle;

	@FindBy(how = How.XPATH, using = "(//*[@ng-if=\"!etry['masterQCSettings']\"])[1]")
	public WebElement viewLinkOfRecords;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.back()']")
	public WebElement BackBtnOfRecord;

	@FindBy(how = How.XPATH, using = "//*[@class='glyphicon glyphicon-edit text-primary']")
	public WebElement assigneeIcon;

	@FindBy(how = How.XPATH, using = "(//*[@ng-class='settings.buttonClasses'])[2]")
	public WebElement assigneeDrpDwn;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-model='searchFilter'])[2]")
	public WebElement assigneeDrpDwnSrchBox;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.assign(changeAssigneeForm)']")
	public WebElement assignBtn;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.removeAppAccordian(app.id)'])[1]")
	public WebElement closeBtnOfAppGrip;
	
	@FindBy(how = How.XPATH, using = "//*[@class='pull-right showing-text-adva-search ng-binding']")
	public WebElement recodsCount;
	
	@FindBy(how = How.XPATH, using = "(//table[@ng-if='!vm.loadMyFavouriteData && vm.myFavoriteData && vm.myFavoriteData.length']/following::th[1])[1]")
	public WebElement myFavSrchName;
	
	@FindBy(how = How.XPATH, using = "(//table[@ng-if='!vm.loadMyFavouriteData && vm.myFavoriteData && vm.myFavoriteData.length']/following::th[2])[1]")
	public WebElement myFavLstUpdate;
	
	@FindBy(how = How.XPATH, using = "(//table[@ng-if='!vm.loadMyFavouriteData && vm.myFavoriteData && vm.myFavoriteData.length']/following::th[3])[1]")
	public WebElement myFavAction;

	@FindBy(how = How.XPATH, using = "(//tr[@ng-repeat='data in vm.myFavoriteData']/following::td[2])[1]")
	public WebElement myFavLastUpdateValue;
	
	// --------------------public methods--------------------

	public WebDriver getDriver() {
		return this.driver;
	}

	public void clickAdvancedSearchTile() throws Exception {
		//log.info(clickAdvSearchTile);
		clickAdvSearchTile.click();
	}

	public void verifyAdvSearchModuleName() throws Exception {
		log.info(advSearchModuleName);
		Assert.assertEquals(textBoxHelper.getText(advSearchModuleName), ObjectRepo.reader.getAdvSearchLabel());
	}

	public void verifyAdvSearchBreadcrumbs() throws Exception {
		log.info(advSearchBreadcrumbs);
		Assert.assertEquals(textBoxHelper.getText(advSearchBreadcrumbs), ObjectRepo.reader.getAdvSearchLabel());
	}

	public void verifyAdvSearchLabel() throws Exception {
		log.info(advSearchLabel);
		Assert.assertEquals(textBoxHelper.getText(advSearchLabel), ObjectRepo.reader.getAdvSearchLabel());
	}
	
	public void verifyAdvSearchLabelColor() throws Exception {
		log.info(advSearchLabel);
		Assert.assertEquals(btnHelper.FontColorCodeHex(advSearchLabel), ObjectRepo.reader.getBrightBlueColor());
	}

	public void verifyAllModuleIsDisplayed() throws Exception {

		log.info(displayModule);
		String expected = "", count = null, actual="";
		String arry[] = allModuleExpected.split(",");
		List<WebElement> links = driver.findElements(By.xpath("//fieldset[@ng-disabled='vm.loadapp']"));
		String []linkText = new String[links.size()];

		for(int i=0; i < links.size(); i++)
		{
			linkText[i] = links.get(i).getText();
			actual = actual + linkText[i];		   
		}

		for (int j=0; j < arry.length; j++)
		{
			count = arry[j];
			count = count.trim();
			expected = expected + "\n" + count;
			expected = expected.trim();
		}

		System.err.println(actual);
		System.out.println("=====================");
		System.err.println(expected);
		Assert.assertEquals(actual, expected);
	}

	public void verifyAllModuleSelected() throws Exception {
		log.info(advSearchLabel);
		if(driver.findElements(By.xpath("//input[@aria-checked='true']")).size() !=0)
		{
			Assert.assertEquals(true, true);
		}
		else
		{
			Assert.assertEquals(true, false);
		}
	}

	public void clickUncheckAllModule() throws Exception {
		log.info(uncheckAllModule);

		int count = driver.findElements(By.xpath("//*[@class='check--label-box']")).size();
		for (int i = 1; i <= count; i++)
		{
			driver.findElement(By.xpath("//*[@class='list']/li["+i+"]/label/span[1]")).click();
		}
	}

	public void clickCheckAllModule() throws Exception {
		log.info(uncheckAllModule);
		int count = driver.findElements(By.xpath("//*[@class='check--label-box']")).size();

		for (int i = 1; i <= count; i++)
		{
			driver.findElement(By.xpath("//*[@class='list']/li["+i+"]/label/span[1]")).click();
		}
	}

	public void verifyCheckAllModule() throws Exception {
		log.info(uncheckAllModule);
		WebElement element;
		int selectedModule = 1;
		int count = driver.findElements(By.xpath("//*[@class='check--label-box']")).size();

		for (int i = 1; i < count; i++) {
			element = driver.findElement(By.xpath("//*[@id='module" + i + "']"));
			if (element.isSelected()) {
				selectedModule++;
			}
		}
		System.out.println("Total selected module: " + selectedModule);
		Assert.assertEquals(selectedModule, count);
	}

	public void verifyUncheckAllModule() throws Exception {
		log.info(uncheckAllModule);
		WebElement element;
		int selectedModule = 1;
		int actual = 1;
		int count = driver.findElements(By.xpath("//*[@class='check--label-box']")).size();

		for (int i = 1; i < count; i++) {
			element = driver.findElement(By.xpath("//*[@id='module" + i + "']"));
			if (element.isSelected()) {
				selectedModule++;
			}
			else
			{
				actual++;
			}
		}
		System.out.println("Total selected module Actual: " + actual);
		System.out.println("Total selected module Old: " + selectedModule);
		Assert.assertEquals(actual, count);
	}

	public void clickSearchAppTextField() throws Exception {
		log.info(searchAppTextfield);
		btnHelper.click(searchAppTextfield);
	}

	public void searchAppValidAppName(String validAppName) throws Exception {
		log.info(searchAppTextfield);
		textBoxHelper.sendKeys(searchAppTextfield,validAppName);
	}

	public void clickClearLink() throws Exception {
		log.info(clearLink);
		btnHelper.click(clearLink);
	}

	public void verifyClearFunctionality() throws Exception {
		log.info(clearLink);
		boolean isDisplayDrpDwn = driver.findElements(By.xpath("//*[@class='ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu']")).size() != 0;
		Assert.assertEquals(isDisplayDrpDwn, false);
	}

	public void verifyNoAppsFoundMsg() throws Exception {
		log.info(NoAppsFoundDrpdwn);
		Assert.assertEquals(textBoxHelper.getText(NoAppsFoundDrpdwn).trim(), ObjectRepo.reader.getNoAppsFoundDrpdwn());
		
	}

	public void searchAppInvalidAppName(String invalidAppName) throws Exception {
		log.info(searchAppTextfield);
		textBoxHelper.sendKeys(searchAppTextfield,invalidAppName);
	}

	public void selectAppsFromDrpDwn() throws Exception {
		log.info(selectApps);
		btnHelper.click(selectApps);
		btnHelper.click(goBtn);
		btnHelper.click(clickAppToCollapse);
		String nowSplit = textBoxHelper.getText(appNameVerify);
		String actual[] = nowSplit.split("\\(");
		System.out.println("Expected Result: "+ actual[0]);
		Assert.assertEquals(actual[0], new AppBuilderPageObject(driver).getAppTitle);
	}

	public void selectMultipleAppsFromDrpDwn(String mulApps1) throws Exception {
		log.info(selectApps);

		String[] splits = mulApps1.split(",");
		System.out.println("splits: "+ splits.toString()  + splits.length);
		for(int i=0; i<splits.length; i++)
		{
			textBoxHelper.sendKeys(searchAppTextfield,splits[i]);
			btnHelper.click(selectApps);
		}
		btnHelper.click(goBtn);
	}

	public void verifyMultipleApps(String multipleApps) throws Exception {
		btnHelper.click(clickAppToCollapse);
		SoftAssert sf = new SoftAssert();
		String[] splits = multipleApps.split(",");
		String actual;
		System.out.println("splits: "+ splits.toString()  + splits.length);
		for(int i=1; i<=splits.length; i++)
		{
			actual = driver.findElement(By.xpath("(//*[@ng-repeat='app in vm.accordianApps track by $index'])[" + i + "]")).getText();
				sf.assertEquals(actual.split("\\(")[0].trim(),splits[i-1]);
		}
		sf.assertAll();
	}

	public void enterAppsHavingNoRecord(String NoRecord) throws Exception {
		textBoxHelper.sendKeys(searchAppTextfield, NoRecord);
	}

	public void verifyNoRecordsFoundMsgInListings() throws Exception {
		log.info(searchAppTextfield);
		Assert.assertEquals(textBoxHelper.getText(noRecordFoundListings).trim(),ObjectRepo.reader.getNoRecordFoundMsg());
	}

	public void verifyRecordsPendingYourReview() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(recordsPendingYourReview),ObjectRepo.reader.getRecordsPendingYourReview());
	}

	public void verifyRecordsPendingYourReviewColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(recordsPendingYourReview), ObjectRepo.reader.getBlueColor());
	}

	public void verifyRecordsYouAccessedToday() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(recordsYouAccessedToday),ObjectRepo.reader.getRecordsYouAccessedToday());
	}

	public void verifyRecordsYouAccessedTodayColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(recordsYouAccessedToday), ObjectRepo.reader.getBlueColor());
	}

	public void verifyRecordsAssignedToYou() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(recordsAssignedToYou),ObjectRepo.reader.getRecordsAssignedToYou());
	}

	public void verifyRecordsAssignedToYouColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(recordsAssignedToYou), ObjectRepo.reader.getBlueColor());
	}

	public void verifyRecordsYouCreatedToday() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(recordsYouCreatedToday),ObjectRepo.reader.getRecordsYouCreatedToday());
	}

	public void verifyRecordsYouCreatedTodayColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(recordsYouCreatedToday), ObjectRepo.reader.getBlueColor());
	}

	public void clickFilterByExpand() throws Exception {
		btnHelper.click(expandcollapseFilterBy);
	}

	public void clickFilterByCollapse() throws Exception {
		btnHelper.click(expandcollapseFilterBy);
	}

	public void verifyCollapseFilterByOption() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//label[@for='date']")).size() != 0)
		{
			actual = false;
		}
		else
		{
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void searchForApp(String validApp) {
		textBoxHelper.sendKeys(searchAppTextfield,validApp);
	}

	public void verifyGoBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(goBtn), ObjectRepo.reader.getLightGreenColor());
	}

	public void clickGoBtn() throws Exception {
		btnHelper.click(selectApps);
		btnHelper.click(goBtn);
	}

	public void verifyBatchReviewLink() throws Exception {
		log.info(verifyBatchLink);
		if(driver.findElements(By.xpath("//*[contains(text(), 'Batch')]")).size() != 0)
		{
			Assert.assertEquals(true, true);
		}
		else
		{
			Assert.assertEquals(true, false);
		}
	}

	public void clickClearAllBtn() throws Exception {
		log.info(clickClearBtn);
		btnHelper.click(clickClearBtn);
	}

	public void verifyClearAllFunctionality() throws Exception {
		log.info(clickClearBtn);
		WebElement element;
		int selectedModule = 1;
		int actual = 1;
		int count = driver.findElements(By.xpath("//*[@class='check--label-box']")).size();

		for (int i = 1; i < count; i++) {
			element = driver.findElement(By.xpath("//*[@id='module" + i + "']"));
			if (element.isSelected()) {
				selectedModule++;
			}
			else
			{
				actual++;
			}
		}
		System.out.println("Total selected module Actual: " + actual);
		System.out.println("Total selected module Old: " + selectedModule);
		Assert.assertEquals(actual, count);
	}

	public void clickClearAllBtnColor() throws Exception {
		log.info(clickClearBtn);
		Assert.assertEquals(btnHelper.BtnColor(clickClearBtn), ObjectRepo.reader.getDarkGreyColor());
	}

	public void verifyAllFieldGetCleared() throws Exception {
		log.info(clickClearBtn);
		WebElement element;
		int selectedModule = 1;
		int actual = 1;
		int count = driver.findElements(By.xpath("//*[@class='check--label-box']")).size();

		for (int i = 1; i < count; i++) {
			element = driver.findElement(By.xpath("//*[@id='module" + i + "']"));
			if (element.isSelected()) {
				selectedModule++;
			}
			else
			{
				actual++;
			}
		}
		System.out.println("Total selected module Actual: " + actual);
		System.out.println("Total selected module Old: " + selectedModule);
		Assert.assertEquals(actual, count);
	}

	public void clickCollapseBtnGrid() throws Exception {
		log.info(clickAppToCollapse);
		btnHelper.click(clickAppToCollapse);
	}

	public void verifyCollapseBtnGrid() throws Exception {
		String nowSplit = textBoxHelper.getText(appNameVerify);
		if(nowSplit.contains(" ("))
		{
			nowSplit= nowSplit.substring(0, nowSplit.indexOf("\\(")); 
			System.out.println(nowSplit);
		}
		Assert.assertEquals(nowSplit, new AppBuilderPageObject(driver).getAppTitle);
	}

	public void verifyCSVIconIsVisible() throws Exception {
		boolean actual = genricHelper.isDisplayed(csvIconDisplay);
		Assert.assertEquals(actual, true);
	}

	public void verifyExcelIconIsVisible() throws Exception {
		boolean actual = genricHelper.isDisplayed(excelIconDisplay);
		Assert.assertEquals(actual, true);
	}

	public void verifyPDFIconIsVisible() throws Exception {
		boolean actual = genricHelper.isDisplayed(pdfIconDisplay);
		Assert.assertEquals(actual, true);
	}

	public void verifyAddToFavoriteBtn() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(addToFavoriteBtn), ObjectRepo.reader.getBlueColor());
	}

	public void clickAddToFavoriteBtn() throws Exception {
		btnHelper.click(addToFavoriteBtn);
	}

	public void clickAddToFavoriteSaveBtn() throws Exception {
		btnHelper.click(addToFavoriteSaveBtn);
	}

	public void verifyAddToFavoriteRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(addToFavoriteRequiredMsg), ObjectRepo.reader.getFavNameRequireValMsg());
		btnHelper.click(cancelBtnAddToFavorite);
	}

	public void verifyAddToFavoriteRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(addToFavoriteRequiredMsg), ObjectRepo.reader.getValMsgColor());
		btnHelper.click(cancelBtnAddToFavorite);
	}

	public void clickAddToFavoriteTextField() throws Exception {
		btnHelper.click(addToFavoriteTextfield);
	}

	public void enterAddToFavoriteMinimumChar(String minChar) throws Exception {
		textBoxHelper.clearAndSendKeys(addToFavoriteTextfield,minChar);
	}

	public void verifyAddToFavoriteMinimumMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(addToFavoriteMinimumMsg), ObjectRepo.reader.getFavNameMinValMsg());
		btnHelper.click(cancelBtnAddToFavorite);
	}

	public void verifyAddToFavoriteMinimumMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(addToFavoriteMinimumMsg), ObjectRepo.reader.getValMsgColor());
		btnHelper.click(cancelBtnAddToFavorite);
	}

	public void enterAddToFavoriteMaximumChar(String maxChar) throws Exception {
			textBoxHelper.clearAndSendKeys(addToFavoriteTextfield,maxChar);
	}

	public void verifyAddToFavoriteMaximumMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(addToFavoriteMaximumMsg), ObjectRepo.reader.getFavNameMaxValMsg());
		btnHelper.click(cancelBtnAddToFavorite);
	}

	public void verifyAddToFavoriteMaximumMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(addToFavoriteMaximumMsg), ObjectRepo.reader.getValMsgColor());
		btnHelper.click(cancelBtnAddToFavorite);
	}

	public void enterAddToFavoriteValidChar(String validChar) throws Exception {
		textBoxHelper.clearAndSendKeys(addToFavoriteTextfield,validChar+"- " + new Helper().randomNumberGeneration());
	}

	public void verifyAddToFavoriteNoValidationMsg() throws Exception {
		boolean required = driver.findElements(By.xpath("//p[@translate='text.FAVORITENAMEISREQUIRED']")).size() == 0;
		boolean minimum = driver.findElements(By.xpath("//p[@translate='text.FAVORITENAMESHOULDBEAT2']")).size() == 0;
		boolean maximum = driver.findElements(By.xpath("//p[@translate='text.FAVORITENAMESHOULDNOTMORE50']")).size() == 0;
		boolean actual;

		if(required && minimum && maximum)
		{
			actual = true;
		}
		else
		{
			actual = false;
		}
		Assert.assertEquals(actual, true);
		btnHelper.click(cancelBtnAddToFavorite);
	}

	public void verifyAddToFavoriteLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(addToFavoriteLabel), ObjectRepo.reader.getAddToFavoriteLabel());
		btnHelper.click(cancelBtnAddToFavorite);
	}

	public void clickCloseIconAddToFavorite() throws Exception {
		btnHelper.click(closeIconAddToFavorite);
	}

	public void verifyCloseIconFunctionalityOfAddToFavorite() throws Exception {
		boolean actual = textBoxHelper.isEnabled(addToFavoriteBtn);
		Assert.assertEquals(actual, true);
	}

	public void clickCancelBtnAddToFavorite() throws Exception {
		btnHelper.click(cancelBtnAddToFavorite);
	}

	public void verifyCancelBtnFunctionalityOfAddToFavorite() throws Exception {
		boolean actual = textBoxHelper.isEnabled(addToFavoriteBtn);
		Assert.assertEquals(actual, true);
	}

	public void verifyMyFavoriteRadioSelectedByDefault() throws Exception {
		boolean actual = checkBoxAndBtnHelper.isIselected(addToFavoriteRadioBtn);
		btnHelper.click(cancelBtnAddToFavorite);
		Assert.assertEquals(actual, true);
	}

	public void verifyMyFavoriteSaveBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(addToFavoriteSaveBtn), ObjectRepo.reader.getLightGreenColor());
		btnHelper.click(cancelBtnAddToFavorite);
	}

	public void verifyMyFavoriteCancelBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(cancelBtnAddToFavorite), ObjectRepo.reader.getDarkGreyColor());
		btnHelper.click(cancelBtnAddToFavorite);
	}

	public void clickMyFavoriteSaveBtn() throws Exception {
		btnHelper.click(addToFavoriteSaveBtn);
		myFavSaveTime = new DateTimeHelper().getCurrentDateTimeMyFavModule();
		Thread.sleep(2000);
	}

	public void verifySavedFavoriteDisplayInListings(String getVal) throws Exception {
		Assert.assertEquals(textBoxHelper.getText(favoriteSavedSuccess), getVal);
	}

	public void enterAddToFavoriteDuplicate(String duplicateChar) throws Exception {
		textBoxHelper.clearAndSendKeys(addToFavoriteTextfield,duplicateChar);
	}

	public void verifyMyFavoriteUniqueMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(addToFavoriteUniqueMsg), ObjectRepo.reader.getFavNameUniqueValMsg());
		btnHelper.click(cancelBtnAddToFavorite);
	}

	public void clickRemoveIconMyFavorites() throws Exception {
		btnHelper.click(clickMyFavoriteTable);
		btnHelper.click(removeFromMyFavorite);
	}

	public void verifyRemoveMsgMyFavorites(String getText) throws Exception {
		Assert.assertEquals(textBoxHelper.getText(removeMsgMyFavorite), ObjectRepo.reader.getRemoveMsgMyFavorite() + getText + ObjectRepo.reader.getQuestionMark());
		btnHelper.click(removeYesMsgMyFavorite);
	}

	public void verifyMyFavoriteRemoveYesBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(removeYesMsgMyFavorite), ObjectRepo.reader.getLightGreenColor());
		btnHelper.click(removeYesMsgMyFavorite);
	}

	public void verifyMyFavoriteRemoveNoBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(removeNoMsgMyFavorite), ObjectRepo.reader.getRemoveMsgColor());
		btnHelper.click(removeYesMsgMyFavorite);
	}

	public void clickMyFavoriteRemoveNoBtn() throws Exception {
		btnHelper.click(removeNoMsgMyFavorite);
	}

	public void verifyMyFavoriteRemoveNoBtnClick(String duplicate) throws Exception {
		Assert.assertEquals(textBoxHelper.getText(favoriteSavedSuccess), duplicate);
	}

	public void verifySavedDataInMyFavorite(String duplicate) throws Exception {
		Assert.assertEquals(textBoxHelper.getText(favoriteSavedSuccess), duplicate);		
		btnHelper.click(removeFromMyFavorite);
		btnHelper.click(removeYesMsgMyFavorite);
	}

	public void verifyFilterByLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(filterByLabel), ObjectRepo.reader.getFilterByLabel());
	}

	public void verifyFilterByIconsVisible() throws Exception {
				Assert.assertEquals(textBoxHelper.getText(filterByIcons), ObjectRepo.reader.getFilterByIcons());
	}

	public void verifyDateRangeSpelling() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(dateRangeSpelling), ObjectRepo.reader.getDateRangeSpelling());
	}

	public void verifyAttributesEntitiesSpelling() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(attributes_EntitiesSpelling), ObjectRepo.reader.getAttributesEntitiesSpelling());
	}

	public void verifySitesSpelling() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(sitesSpelling), ObjectRepo.reader.getSitesSpelling());
	}

	public void verifyBatchEntriesSpelling() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(batchEntriesSpelling), ObjectRepo.reader.getBatchEntriesSpelling());
	}

	public void verifyAssigneeSpelling() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(assigneeSpelling), ObjectRepo.reader.getAssigneeSpelling());
	}

	public void verifyRecordStatusSpelling() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(recordStatusSpelling), ObjectRepo.reader.getRecordStatusText());
	}

	public void selectTodaysDateFilterBy() throws Exception {
		drpdwnHelper.selectDropDownText(dateDrpDwnFilterBy, ObjectRepo.reader.getDateDrpDwnFilterBy());
		
	}

	public void verifyTodaysDateFilterBy() throws Exception {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		String systemDate = dateFormat.format(date);
		System.out.println("System Date: "+ systemDate);
		String currentDate = helper.getClipboardContents(getDateFilterBy);
		System.out.println("Current Date: "+ currentDate);
		Assert.assertEquals(systemDate, currentDate);
	}

	public void selectThisWeekDateFilterBy() throws Exception {
		drpdwnHelper.selectDropDownText(dateDrpDwnFilterBy, ObjectRepo.reader.getDateDrpDwnForWeek());
	}

	public void verifyThisWeekFilterBy() throws Exception {
		String expectedSunday = helper.weekDateOfSunday(); 
		String expectedSaturday = helper.weekDateOfSaturday();
		String actualSunday = helper.getClipboardContents(getStartDateFilterBy);
		String actualSaturday = helper.getClipboardContents(getEndDateFilterBy);
		Assert.assertEquals(actualSunday + " " + actualSaturday, expectedSunday + " " + expectedSaturday);
	}

	public void verifyThisWeekFilterByByDefault() throws Exception {
		boolean actual = checkBoxAndBtnHelper.isIselected(dateRangeByDefault);
		Assert.assertEquals(actual, true);
	}

	public void selectThisMonthFilterBy() throws Exception {
		drpdwnHelper.selectDropDownText(dateDrpDwnFilterBy, ObjectRepo.reader.getDateDrpDwnForMonth());
	}

	public void verifyThisMonthFilterBy() throws Exception {
		String expectedSunday = helper.thisMonthStartDate(); 
		String expectedSaturday = helper.thisMonthEndDate();
		String actualSunday = helper.getClipboardContents(getStartDateFilterBy);
		String actualSaturday = helper.getClipboardContents(getEndDateFilterBy);
		Assert.assertEquals(actualSunday + " " + actualSaturday, expectedSunday + " " + expectedSaturday);
	}

	public void verifyThisMonthFilterByByDefault() throws Exception {
		boolean actual = checkBoxAndBtnHelper.isIselected(dateRangeByDefault);
		Assert.assertEquals(actual, true);
	}

	public void verifyClearLinkVisibleInDateRange() throws Exception {
		boolean actual = genricHelper.isDisplayed(clearLinkFilterBy);
		Assert.assertEquals(actual, true);
	}

	public void verifyClearLinkVisibleInDateRangeColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(clearLinkFilterBy), ObjectRepo.reader.getBlueColor());
	}

	public void clickClearLinkFilterBy() throws Exception {
		btnHelper.click(clearLinkFilterBy);
	}

	public void verifyAllFieldIsCleared() throws Exception {
		boolean actual = checkBoxAndBtnHelper.isIselected(dateRangeByDefault);
		Assert.assertEquals(actual, false);
	}

	public void verifyRemoveIconIsVisible() throws Exception {
		boolean actual = genricHelper.isDisplayed(removeIconFilterBy);
		Assert.assertEquals(actual, true);
	}

	public void clickMonthYearDrpdwn() throws Exception {
		btnHelper.click(monthYearDrpDwn);
	}

	public void verifyMonthYearDrpdwnIsVisible() throws Exception {
		boolean monthDrpDwn = genricHelper.isDisplayed(selectMonthDrpDwn);
		boolean yearDrpDwn = genricHelper.isDisplayed(selectYearDrpDwn);
		boolean flag;
		if(monthDrpDwn && yearDrpDwn)
		{
			flag = true;
		}
		else {
			flag = false;
		}
		Assert.assertEquals(flag, true);
	}

	public void verifyRemoveIconColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(removeIconFilterBy), ObjectRepo.reader.getRemoveIconColor());
	}

	public void searchForThisWeekApp(String thisWeek) throws Exception {
		textBoxHelper.clearAndSendKeys(searchAppTextfield,thisWeek);
	}

	public void VerifySearchForThisWeekApp() throws Exception {
		btnHelper.click(goBtn);
		
		String nextBtnColor, startDate, endDate;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();

		startDate = helper.getClipboardContents(getStartDateFilterBy);
		endDate = helper.getClipboardContents(getEndDateFilterBy);

		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));
				try {
					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				btnHelper.click(nextPagging);
				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array

		int actualMatchedRecords = 0;
		int expectedMatchedRecords = temp.size();

		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);

			String Str = outArray[j];
			String[] arrOfStr = Str.split(" ");

			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date srtDate = sdf.parse(startDate);
			Date enDate = sdf.parse(endDate);
			Date foundDate = sdf.parse(arrOfStr[0]);

			if (foundDate.after(srtDate) && foundDate.before(enDate)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + "\nStart Date: " + startDate
						+ "\nEnd Date: " + endDate);
			} else if (foundDate.equals(srtDate) || foundDate.equals(enDate)) {
				{
					actualMatchedRecords++;
					System.out.println("--------------------------------");
					System.out.println("Record Matched                |");
					System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + "\nStart Date: " + startDate
							+ "\nEnd Date: " + endDate);
				}
			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + "\nStart Date: " + startDate
						+ "\nEnd Date: " + endDate);
			}
			System.out.println("--------------------------------");

		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);
		Assert.assertEquals(actualMatchedRecords, expectedMatchedRecords);
	}

	public void searchForTodayApp(String today) throws Exception {
		textBoxHelper.clearAndSendKeys(searchAppTextfield,today);
	}

	public void verifySearchForTodayApp() throws Exception {

		btnHelper.click(goBtn);

		String nextBtnColor;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();

		String todayDate = helper.getClipboardContents(getDateFilterBy);

		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				btnHelper.click(nextPagging);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array

		int actualMatchedRecords = 0;
		int expectedMatchedRecords = temp.size();

		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);

			String Str = outArray[j];
			String[] arrOfStr = Str.split(" ");

			if (arrOfStr[0].equalsIgnoreCase(todayDate)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + todayDate);

			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + todayDate);
			}
			System.out.println("--------------------------------");

		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);

		Assert.assertEquals(actualMatchedRecords, expectedMatchedRecords);
	}

	public void searchForThisMonthApp(String thisMonth) throws Exception {
		textBoxHelper.clearAndSendKeys(searchAppTextfield,thisMonth);
	}

	public void verifySearchForThisMonthApp() throws Exception {
		btnHelper.click(goBtn);
		
		String nextBtnColor, startDate, endDate;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();
		startDate = helper.getClipboardContents(getStartDateFilterBy);
		endDate = helper.getClipboardContents(getEndDateFilterBy);

		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {
				
				nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				btnHelper.click(nextPagging);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array
		int actualMatchedRecords = 0;
		int expectedMatchedRecords = temp.size();

		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);
			String Str = outArray[j];
			String[] arrOfStr = Str.split(" ");
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date srtDate = sdf.parse(startDate);
			Date enDate = sdf.parse(endDate);
			Date foundDate = sdf.parse(arrOfStr[0]);

			if (foundDate.after(srtDate) && foundDate.before(enDate)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + "\nStart Date: " + startDate
						+ "\nEnd Date: " + endDate);

			} else if (foundDate.equals(srtDate) || foundDate.equals(enDate)) {
				{
					actualMatchedRecords++;
					System.out.println("--------------------------------");
					System.out.println("Record Matched                |");
					System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + "\nStart Date: " + startDate
							+ "\nEnd Date: " + endDate);
				}
			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + "\nStart Date: " + startDate
						+ "\nEnd Date: " + endDate);
			}
			System.out.println("--------------------------------");
		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);

		Assert.assertEquals(actualMatchedRecords, expectedMatchedRecords);
	}

	public void selectSpecificDateFromDrpDwn(String specificDate) throws Exception {
		drpdwnHelper.selectDropDownText(dateDrpDwnFilterBy, specificDate);
	}

	public void selectSpecificDate() throws Exception {
		btnHelper.click(specificDateIcon);
		btnHelper.click(selectSpecificDate);
	}

	public void verifyBySelectSpecificDate() throws Exception {

		btnHelper.click(goBtn);
		String nextBtnColor;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();

		String todayDate = helper.getClipboardContents(getDateFilterBy);

		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);

				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				btnHelper.click(nextPagging);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array

		int actualMatchedRecords = 0;
		int expectedMatchedRecords = temp.size();

		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);

			String Str = outArray[j];
			String[] arrOfStr = Str.split(" ");

			if (arrOfStr[0].equalsIgnoreCase(todayDate)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + todayDate);

			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual: " + arrOfStr[0] + "\nexpected: " + todayDate);
			}
			System.out.println("--------------------------------");

		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);

		Assert.assertEquals(actualMatchedRecords, expectedMatchedRecords);
	}

	public void selectMonthFromDrpDwn(String selectMonth) throws Exception {
		drpdwnHelper.selectDropDownText(selectMonthDrpDwn, selectMonth);
	}

	public void verifyMonthRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(monthDrpDwnRequiredMsg), ObjectRepo.reader.getMonthRequireValMsg());
	}

	public void verifyMonthRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(monthDrpDwnRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}
	
	public void verifyYearRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(yearDrpDwnRequiredMsg), ObjectRepo.reader.getYearRequiredValMsg());
	}

	public void verifyYearRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(yearDrpDwnRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void selectYearFromDrpDwn(String selectYear) throws Exception {
		drpdwnHelper.selectDropDownText(selectYearDrpDwn, selectYear);
	}

	public void verifyMonthYearProperData(String selectMonth, String selectYear) throws Exception {
		btnHelper.click(goBtn);

		selectMonthFromDrpDwn(selectMonth);
		selectYearFromDrpDwn(selectYear);

		String monthInput = helper.getDrpDwnFirstSelectedOptn(selectMonthDrpDwn);
		String yearInput = helper.getDrpDwnFirstSelectedOptn(selectYearDrpDwn);

		String nextBtnColor;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();

		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));
				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}
					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				btnHelper.click(nextPagging);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Created Date']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array

		int actualMatchedRecords = 1;
		int expectedMatchedRecords = temp.size();
		System.out.println("Temvferfdfffffffffff: "+ temp.size());
		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);

			String Str = outArray[j];

			String[] arrOfStr = Str.split(" ");
			System.out.println("split1: " + Arrays.toString(arrOfStr));
			String[] dateToSplit = arrOfStr[j].split("/");

			System.out.println("split1: " + dateToSplit);
			System.out.println("month: " + dateToSplit[0]);
			System.out.println("year: " + dateToSplit[2]);

			String monthNo = getMonthNo(monthInput);

			if (dateToSplit[0].equalsIgnoreCase(monthNo) && dateToSplit[2].equalsIgnoreCase(yearInput)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual month and year: " + dateToSplit[0] + " / " + dateToSplit[2] + "\nexpected: "
						+ monthNo + " / " + yearInput);
			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual month and year: " + dateToSplit[0] + " / " + dateToSplit[2] + "\nexpected: "
						+ monthNo + " / " + yearInput);
			}
			System.out.println("--------------------------------");

		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);

		Assert.assertEquals(actualMatchedRecords, expectedMatchedRecords);
	}

	static public String getMonthNo(String monthName) {
		String monthNo = "0";

		switch (monthName) {
		case "January":
			monthNo = "01";
			break;
		case "February":
			monthNo = "02";
			break;
		case "March":
			monthNo = "03";
			break;
		case "April":
			monthNo = "04";
			break;
		case "May":
			monthNo = "05";
			break;
		case "June":
			monthNo = "06";
			break;
		case "July":
			monthNo = "07";
			break;
		case "August":
			monthNo = "08";
			break;
		case "September":
			monthNo = "09";
			break;
		case "October":
			monthNo = "10";
			break;
		case "November":
			monthNo = "11";
			break;
		case "December":
			monthNo = "12";
			break;
		default:
			System.out.println("Invalid input");
			break;
		}
		return monthNo;
	}

	public void attributesLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(attributes_EntitiesSpelling), ObjectRepo.reader.getAttributesEntitiesSpelling());
	}

	public void clickAttributesIcon() throws Exception {
		btnHelper.click(attributes_EntitiesSpelling);
	}

	public void selectKeyAttributesDrpDwn(String selectAttributes) throws Exception {
		drpdwnHelper.selectDropDownText(attributeDrpDwn, selectAttributes);
	}

	public void verifyValueRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(valueRequiredMsg), ObjectRepo.reader.getAttributeEntityRequiredMsg());
	}

	public void verifyValueRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(valueRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyAddAttributeIcon() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(addAttribute), true);
	}

	public void verifyAddAttributeIconColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(addAttribute), ObjectRepo.reader.getLightGreenColor());
	}

	public void selectAttributeValue(String value) throws Exception {
		textBoxHelper.clearAndSendKeys(attributeValue, value);
	}

	public void clickAddAttributes() throws Exception {
		btnHelper.click(addAttribute);
	}

	public void verifyAttributeFilterData() throws Exception {
		List<WebElement> count = ObjectRepo.driver.findElements(By.xpath("//button[@ng-click='vm.removeAttribute($index)']"));
		Assert.assertEquals(count.size(), 2);
	}

	public void verifyRemoveIconVisible() throws Exception {
		Assert.assertEquals(true, textBoxHelper.isDisplayed(attributesRemoveIcon));
	}

	public void removeIconColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(attributesRemoveIcon), ObjectRepo.reader.getRemoveIconColor());
	}

	public void clickRemoveIcon() throws Exception {
		btnHelper.click(attributesRemoveIcon);
	}

	public void verifyRemoveIconFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//select[@ng-model='vm.attribEtyFilter[$index].attrb']")).size() != 0)
		{
			actual = true;
		}
		else
		{
			actual = false;
		}
		Assert.assertEquals(actual, false);
	}

	public void verifyDataAccordingly(String attributeVal, String inputAttributeVal) throws Exception {

		String attributeValue = attributeVal;
		String inputValue = inputAttributeVal;

		String nextBtnColor;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();

		btnHelper.click(filterByLabel);

		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='" + attributeValue + "']"));

				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				btnHelper.click(filterByLabel);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='" + attributeValue + "']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array

		int actualMatchedRecords = 0;
		int expectedMatchedRecords = temp.size();

		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);

			if (outArray[0].equalsIgnoreCase(inputValue)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual: " + outArray[0] + "\nexpected: " + inputValue);

			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual: " + outArray[0] + "\nexpected: " + inputValue);

			}
			System.out.println("--------------------------------");

		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);

		Assert.assertEquals(actualMatchedRecords, expectedMatchedRecords);

	}

	public void verifyClearLinkVisible() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(attributesClearLink), true);
	}

	public void verifyClearLinkVisibleColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(attributesClearLink), ObjectRepo.reader.getRemoveIconColor());
	}

	public void clickAttributesClearLink() throws Exception {
		btnHelper.click(attributesClearLink);
	}

	public void verifyAttributesClearLinkFunctionality() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(attributeDrpDwn), ObjectRepo.reader.getAttributeDrpDwn());
	}

	public void verifyAttributeRemoveIconVisible() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(attributesRemoveIcon), true);
	}

	public void attributeRemoveIconColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(attributesRemoveIcon), ObjectRepo.reader.getRemoveIconColor());
	}

	public void verifySitesLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(sitesSpelling), ObjectRepo.reader.getSitesSpelling());
	}

	public void clickOnSites() throws Exception {
		btnHelper.click(sitesSpelling);
	}

	public void verifySitesClearLink() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(sitesClearLink), true);
	}

	public void verifySitesClearLinkColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(sitesClearLink), ObjectRepo.reader.getBlueColor());
	}

	public void selectSitesDrpDwn() throws Exception {
		drpdwnHelper.selectDropDownText(sitesRegionDrpDwn, ObjectRepo.reader.getSitesRegionDrpDwnForSelect());
	}

	public void clickSitesClearLink() throws Exception {
		btnHelper.click(sitesClearLink);
	}

	public void verifySitesClearLinkFunctionality() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(sitesRegionDrpDwn), ObjectRepo.reader.getSitesRegionDrpDwn());
	}

	public void verifySitesRemoveIconVisible() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(sitesRemoveIcon), true);
	}

	public void verifySitesRemoveIconColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(sitesRemoveIcon), ObjectRepo.reader.getRemoveIconColor());
	}

	public void clickSitesRemoveIcon() throws Exception {
		btnHelper.click(sitesRemoveIcon);
	}

	public void verifySitesRemoveIconFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//select[@title='Please select Region']")).size() != 0)
		{
			actual = false;
		}else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void verifyBatchEntryLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(batchEntriesSpelling), ObjectRepo.reader.getBatchEntriesSpelling());
	}

	public void clickBatchEntries() throws Exception {
		btnHelper.click(batchEntriesSpelling);
	}

	public void enterTwentyCharBatchEntriesField(String maxChar) throws Exception {
		textBoxHelper.clearAndSendKeys(batchEntrytextfield,maxChar);
	}

	public void verifyBatchEntiesMaximimMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(batchEntryMaximumMsg), ObjectRepo.reader.getBatchEntryMaximumMsg());
	}

	public void verifyBatchEntiesMaximimMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(batchEntryMaximumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void clickBatchEntiesField() throws Exception {
		btnHelper.click(batchEntrytextfield);
	}

	public void enterBatchEntiesValidChar(String validChar) throws Exception {
		textBoxHelper.clearAndSendKeys(batchEntrytextfield,validChar);
	}

	public void verifyBatchEntitiesSearching(String validBatchEntry) throws Exception {
		btnHelper.click(goBtn);
		String inputValue = validBatchEntry;
		String batchInputValue = inputValue;
		String nextBtnColor;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();

		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Batch Entry ID']"));

				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				btnHelper.click(nextPagging);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@data-th='Batch Entry ID']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array

		int actualMatchedRecords = 0;
		int expectedMatchedRecords = temp.size();

		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);

			if (outArray[0].equalsIgnoreCase(batchInputValue)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual: " + outArray[0] + "\nexpected: " + batchInputValue);

			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual: " + outArray[0] + "\nexpected: " + batchInputValue);

			}
			System.out.println("--------------------------------");

		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);

		Assert.assertEquals(actualMatchedRecords, expectedMatchedRecords);
	}

	public void verifyBatchEntryClearLinkVisible() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(batchEntryClearLink), true);
	}

	public void verifyBatchEntryClearLinkColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(batchEntryClearLink), ObjectRepo.reader.getBlueColor());
	}

	public void clickBatchEntryClearLink() throws Exception {
		btnHelper.click(batchEntryClearLink);
	}

	public void verifyBatchEntryClearLinkFunctionality() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(batchEntrytextfield), "");
	}

	public void verifyBatchEntryRemoveIconVisible() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(batchEntryRemoveIcon), true);
	}

	public void verifyBatchEntryRemoveIconColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(batchEntryRemoveIcon), ObjectRepo.reader.getRemoveIconColor());
	}

	public void clickBatchEntryRemoveIcon() throws Exception {
		btnHelper.click(batchEntryRemoveIcon);
	}

	public void verifyBatchEntryRemoveIconFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@name='batchEntryId']")).size() != 0)
		{
			actual = false;
		}
		else{
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickAssigneeIcon() throws Exception {
		btnHelper.click(assigneeSpelling);
	}

	public void verifyAssigneeRadioBtnVisible() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(assigneeRadioBtn), true);
	}

	public void verifyAssigneeToMeRadioBtnVisible() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(assigneeToMeRadioBtn), true);
	}

	public void verifyAssigneeRadioBtnByDefaultSelected() throws Exception {
		boolean actual = true;
		if(driver.findElements(By.xpath("//input[@id='test11' and @aria-checked='true']")).size() != 0)
		{
			actual = true;
		} else{
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickRoleDrpDwnVal() throws Exception {
		btnHelper.click(assigneeRoleDrpdwn);
	}

	public void clickCheckAllRoleDrpDwnVal() throws Exception {
		btnHelper.click(checkAllRoleDrpdwn);
	}

	public void verifyCheckAllRoleDrpDwnValFunctionality() throws Exception {
		List<WebElement> totlChkBox = new ArrayList<WebElement>();
		totlChkBox = driver.findElements(By.xpath("//*[@class='checkboxInput']"));
		System.out.println("Total checkbox value is: " + totlChkBox.size());
		int count = 0;
		for (int i = 0; i < totlChkBox.size(); i++) {
			if (totlChkBox.get(i).isSelected() == true) {
				count++;
			}
		}
		Assert.assertEquals(count, totlChkBox.size());
	}

	public void clickUnCheckAllRoleDrpDwnVal() throws Exception {
		btnHelper.click(unCheckAllRoleDrpdwn);
	}

	public void verifyUnCheckAllRoleDrpDwnValFunctionality() throws Exception {
		List<WebElement> totlChkBox = new ArrayList<WebElement>();
		totlChkBox = driver.findElements(By.xpath("//input[@checked='checked']"));
		System.out.println("Total checkbox value is: " + totlChkBox.size());
		int count = 0;
		for (int i = 0; i < totlChkBox.size(); i++) {
			if (totlChkBox.get(i).isSelected() == true) {
				count++;
			}
		}
		Assert.assertEquals(count, totlChkBox.size());
	}

	public void verifyRecordStatusLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(recordStatusSpelling), ObjectRepo.reader.getRecordStatusText());
	}

	public void clickRecordStatusLabel() throws Exception {
		btnHelper.click(recordStatusSpelling);
	}

	public void verifyRecordStatusCount(String statusCount) throws Exception {
		String[] count= statusCount.split(",");
		List<WebElement> xpath = driver.findElements(By.xpath("//*[@ng-change='vm.validateFields()']"));
		int xpathCount = xpath.size();
		Assert.assertEquals(xpathCount, count.length);
	}

	public void verifyPassedRecordStatus() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(passRecordStatus), true);
	}

	public void verifyFailedRecordStatus() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(failRecordStatus), true);
	}

	public void verifyReviewPendingRecordStatus() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(reviewPendingRecordStatus), true);
	}

	public void verifyVoidRecordStatus() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(voidRecordStatus), true);
	}

	public void verifyDraftRecordStatus() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(draftRecordStatus), true);
	}

	public void verifyRejectedRecordStatus() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(rejectedRecordStatus), true);
	}

	public void clickPassedRecordCheckbox() throws Exception {
		btnHelper.click(passRecordStatus);
	}

	public void verifyPassedRecordCheckboxFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@ng-change='vm.validateFields()' and @aria-checked='true']")).size() != 0)
		{
			actual = true;
		}else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickFailedRecordCheckbox() throws Exception {
		btnHelper.click(failRecordStatus);
	}

	public void verifyFailedRecordCheckboxFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@ng-change='vm.validateFields()' and @aria-checked='true']")).size() != 0)
		{
			actual = true;
		}else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickReviewPendingRecordCheckbox() throws Exception {
		btnHelper.click(reviewPendingRecordStatus);
	}

	public void verifyReviewPendingRecordCheckboxFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@ng-change='vm.validateFields()' and @aria-checked='true']")).size() != 0)
		{
			actual = true;
		}else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickVoidRecordCheckbox() throws Exception {
		btnHelper.click(voidRecordStatus);
	}

	public void verifyVoidRecordCheckboxFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@ng-change='vm.validateFields()' and @aria-checked='true']")).size() != 0)
		{
			actual = true;
		}else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickDraftRecordCheckbox() throws Exception {
		btnHelper.click(draftRecordStatus);
	}

	public void verifyDraftRecordCheckboxFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@ng-change='vm.validateFields()' and @aria-checked='true']")).size() != 0)
		{
			actual = true;
		}else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickRejectedRecordCheckbox() throws Exception {
		btnHelper.click(rejectedRecordStatus);
	}

	public void verifyRejectedRecordCheckboxFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//input[@ng-change='vm.validateFields()' and @aria-checked='true']")).size() != 0)
		{
			actual = true;
		}else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void verifyRecordStatusRemoveIconVisible() throws Exception {
		Assert.assertEquals(textBoxHelper.isDisplayed(recordStatusRemoveIcon), true);
	}

	public void verifyRecordStatusRemoveIconColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(recordStatusRemoveIcon), ObjectRepo.reader.getRemoveIconColor());
	}

	public void clickRecordStatusRemoveIcon() throws Exception {
		btnHelper.click(recordStatusRemoveIcon);
	}

	public void verifyRecordStatusRemoveIconFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//div[2]/div[2]/div[2]/div/ul/li[6]")).size() != 0)
		{
			actual = false;
		}else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickViewLinkOfApp() throws Exception {
		btnHelper.click(viewLinkApp);
	}

	public void verifyViewLinkOfApp(String app) throws Exception {
		String actual = textBoxHelper.getText(viewLinkAppTitle);
		Assert.assertEquals(actual, app);
	}

	public void verifyAdvancedSearchPagination() throws Exception {
		String nextBtnColor, Pagination, TotlRcd;
		int i = 0, count = 0, add = 0;

		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();

		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@ng-click='vm.getActivityinfo(etry._id,etry.App)']"));

				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				btnHelper.click(nextPagging);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver.findElements(By.xpath("//*[@ng-click='vm.getActivityinfo(etry._id,etry.App)']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;
			}
			System.out.println(i + "-grid list: " + temp.size());

		}

		TotlRcd = Integer.toString(temp.size());
		Pagination = driver.findElement(By.xpath("//*[@class='pull-right showing-text-adva-search ng-binding']")).getText();

		String[] arryOfPagination = Pagination.split(" ");

		if (arryOfPagination[3].equalsIgnoreCase(arryOfPagination[6])) {

			if (temp.size() > 10) {

				System.out.println("sss Showing " + i + "1 to " + TotlRcd + " of total " + TotlRcd + " entries");
				Assert.assertEquals("Showing " + i + "1 to " + TotlRcd + " of total " + TotlRcd + " entries",
						Pagination);
			} else {

				Assert.assertEquals("Showing 1 to " + TotlRcd + " of total " + TotlRcd + " entries", Pagination);
			}
		} else {
			Assert.assertEquals(true, false);
		}
	}	
	
	//santosh
	protected void waitThread() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}	
	}	
	
	public void clickRecordsYouCreatedToday() {	
		waitThread();
		btnHelper.click(recordsYouCreatedTodayLink);
	} 
	public void clickRecordsYouAccessedToday() {
		waitThread();
		btnHelper.click(recordsYouAccessedTodayLink);
	}
	public void clickRecordsAssignedToYou() {
		waitThread();
		btnHelper.click(recordsAssignedToYouLink);
	}
	public void clickRecordsPendingYourReview() {
		waitThread();
		btnHelper.click(recordsPendingYourReviewLink);
	}
	
	public boolean findAppInAdvSeachGrid(String expected) {

		waitThread();
		boolean flag = false;

		if (driver.findElements(By.xpath(
				"//*[@ng-if='vm.commands && !(vm.accordianApps && vm.accordianApps.length) && vm.isLoadingAppsForQuick']"))
				.size() == 0) {

			btnHelper.click(clickAppToCollapse);

			List<WebElement> nowSplit = driver.findElements(By.xpath("//*[@ng-if='app.id']"));

			for (int i = 0; i < nowSplit.size(); i++) {

				System.out.println("App Name is::" + nowSplit.get(i).getText().split("\\(")[0].trim());
				if (nowSplit.get(i).getText().split("\\(")[0].trim().equals(expected)) {
					flag = true;
					break;
				} else {
					flag = false;
				}
			}
			System.out.println("Does Expected App found in Adv Seach Grid? :: Result is - " + flag);
		}
		return flag;
	}
	
	public void  verifyQuickAccessLinkClickable(){
		
		Assert.assertEquals(findAppInAdvSeachGrid(new AppBuilderPageObject(driver).getAppTitle), true, "Expected App Not Found");
	}

	public void verifyOrderOfQuickAccess() {

		SoftAssert sf = new SoftAssert();

		if (recordsYouCreatedToday.getText().equals("Records You Created Today")) {

			if (recordsYouAccessedToday.getText().equals("Records You Accessed Today")) {

				if (recordsAssignedToYou.getText().equals("Records Assigned To You")) {

					if (recordsPendingYourReview.getText().equals("Records Pending Your Review")) {

						sf.assertEquals(true, true);

					} else {
						sf.assertEquals(recordsPendingYourReview.getText(), "Records Pending Your Review");
					}
				} else {
					sf.assertEquals(recordsAssignedToYou.getText(), "Records Assigned To You");
				}
			} else {
				sf.assertEquals(recordsYouAccessedToday.getText(), "Records You Accessed Today");
			}
		} else {
			sf.assertEquals(recordsYouCreatedToday.getText(), "Records You Created Today");
		}
		sf.assertAll();
	}

	public void clickOnViewLinkOfCreatedApp() {

//		expectedAppName = "Test demo 1 - 838150";
		waitThread();

		btnHelper.click(clickAppToCollapse);

		List<WebElement> nowSplit = driver.findElements(By.xpath("//*[@ng-if='app.id']"));

		boolean flag = false;

		for (int i = 1; i <= nowSplit.size(); i++) {

			System.out.println("App Name is::" + nowSplit.get(i-1).getText().split("\\(")[0].trim());
			if (nowSplit.get(i-1).getText().split("\\(")[0].trim().equals(new AppBuilderPageObject(driver).getAppTitle)) {
				flag = true;
//				btnHelper.click(nowSplit.get(i));
				btnHelper.click(driver.findElement(By.xpath("(//*[@class='pull-right glyphicon glyphicon-chevron-right'])["+i+"]")));
				btnHelper.click(viewLinkOfRecords);
				break;
			} else {
				flag = false;
			}
		}
	}

	public void clickOnBackBtnOfRecord(){
		new Scroll().scrollTillElem(BackBtnOfRecord);
		btnHelper.click(BackBtnOfRecord);
	}

	public void selectCurnUserAsAssignee() throws Exception {
		btnHelper.click(assigneeIcon);
		btnHelper.click(assigneeDrpDwn);
		
		 ExcelUtils loginExcel= new ExcelUtils();
	     loginExcel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Login");
	      String username = ExcelUtils.readXLSFile("Login", 2, 1);  
		System.out.println("user Name: "+ username);

		textBoxHelper.sendKeys(assigneeDrpDwnSrchBox, username);
		

		List<WebElement> list = driver.findElements(By.xpath("//label[@class='ng-binding']"));

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getText().equals(username)) {
				 driver.findElement(By.xpath("(//label[@class='ng-binding'])[" + (i+1) + "]")).click();
				break;
			}
		}
		btnHelper.click(assignBtn);
		waitThread();
	}
	
	public void verifyTodaysAllRecordsOfGrid()throws InterruptedException {
		waitThread();
		int exCout = 0,actCount=0;
		String nextBtnColor="#337ab7", viewAuditTrail;
        int counter =0;
        String pagination = null; 
     
		btnHelper.click(clickAppToCollapse);
		
		//no of apps
		List<WebElement> noOfApps = driver.findElements(By.xpath("//*[@ng-if='app.id']"));
		
		for(int i = 0; i < noOfApps.size(); i++) {
			
			new Scroll().scrollTillElem(noOfApps.get(i));
			WebElement openApp = driver.findElement(By.xpath("(//*[@class='pull-right glyphicon glyphicon-chevron-right'])["+(i+1)+"]"));

			btnHelper.click(openApp);

			 //if found more than 10 records
	        if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
	                .size() != 0 == true) {
	        	//no of records in single app
				
	        	pagination = textBoxHelper.getText(recodsCount);
	        	   String [] words = pagination.split(" ");
	               System.err.println(words[6]);
	            while(!nextBtnColor.equals("#777777")) {
	                Thread.sleep(1000);
					List<WebElement> noOfRecds = driver.findElements(By.xpath("//*[@data-th='Created Date']"));

	    			for(int k = 1; k<=noOfRecds.size(); k++) {
	    				String createdDate = driver.findElement(By.xpath("(//*[@data-th='Created Date'])["+k+"]")).getText();
	    				exCout++;
	    				
	    				if(genricHelper.getCurrentSystemDate().equals(createdDate.split(" ")[0])){
	    					actCount++;	
	    				}else {
	    					Assert.assertEquals(genricHelper.getCurrentSystemDate(), createdDate.split(" ")[0],"Date doesn't matched");
	    				}
	                }
	                System.err.println(counter);
	                nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
	                btnHelper.click(nextPagging);
	                Thread.sleep(1000);

	            }
	        } else {
	        	//no of records in single app
				List<WebElement> noOfRecds = driver.findElements(By.xpath("//*[@data-th='Created Date']"));
				
	        	for(int j = 1; j <= noOfRecds.size() ;j++) {
					
					String createdDate = driver.findElement(By.xpath("(//*[@data-th='Created Date'])["+j+"]")).getText();
					exCout++;
					
					if(genricHelper.getCurrentSystemDate().equals(createdDate.split(" ")[0])){
						actCount++;	
					}else {
						Assert.assertEquals(genricHelper.getCurrentSystemDate(), createdDate.split(" ")[0],"Date doesn't matched");
					}
	        }	
			Thread.sleep(500);
		}
			helper.javascriptExecutorClick(closeBtnOfAppGrip);
	}
		System.err.println("actual value::"+actCount+"\nexpected value::"+exCout);	
		Assert.assertEquals(actCount, exCout,"Expected Count doesn't match");
	}

	public void verifyAssigneeRecordsOfGrid()throws InterruptedException {
		waitThread();
		int exCout = 0,actCount=0;
		String nextBtnColor="#337ab7", viewAuditTrail;
        int counter =0;
        String pagination = null; 
     
		btnHelper.click(clickAppToCollapse);
		
		//no of apps
		List<WebElement> noOfApps = driver.findElements(By.xpath("//*[@ng-if='app.id']"));
		
		for(int i = 0; i < noOfApps.size(); i++) {
			
			new Scroll().scrollTillElem(noOfApps.get(i));
			WebElement openApp = driver.findElement(By.xpath("(//*[@class='pull-right glyphicon glyphicon-chevron-right'])["+(i+1)+"]"));

			btnHelper.click(openApp);

			 //if found more than 10 records
	        if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
	                .size() != 0 == true) {
	        	//no of records in single app
				
	        	pagination = textBoxHelper.getText(recodsCount);
	        	   String [] words = pagination.split(" ");
	               System.err.println(words[6]);
	            while(!nextBtnColor.equals("#777777")) {
	                Thread.sleep(1000);
					List<WebElement> noOfRecds = driver.findElements(By.xpath("//*[@data-th='Created Date']"));

	    			for(int k = 1; k<=noOfRecds.size(); k++) {
	    				String createdDate = driver.findElement(By.xpath("(//*[@data-th='Assignee'])["+k+"]")).getText();
	    				exCout++;
	    				
	    					if(new UsersPageObject(driver).userDeatils.equals(createdDate)){
	    					actCount++;	
	    				}else {
	    					Assert.assertEquals(new UsersPageObject(driver).userDeatils, createdDate,"Date doesn't matched");
	    				}
	                }
	                System.err.println(counter);
	                nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
	                btnHelper.click(nextPagging);
	                Thread.sleep(1000);

	            }
	        } else {
	        	//no of records in single app
				List<WebElement> noOfRecds = driver.findElements(By.xpath("//*[@data-th='Created Date']"));
				
	        	for(int j = 1; j <= noOfRecds.size() ;j++) {
					
					String createdDate = driver.findElement(By.xpath("(//*[@data-th='Assignee'])["+j+"]")).getText();
					exCout++;
					
					if(new UsersPageObject(driver).userDeatils.equals(createdDate)){
						actCount++;	
					}else {
						Assert.assertEquals(new UsersPageObject(driver).userDeatils, createdDate,"Date doesn't matched");
					}
	        }	
			Thread.sleep(500);
		}
			helper.javascriptExecutorClick(closeBtnOfAppGrip);
	}
		System.err.println("actual value::"+actCount+"\nexpected value::"+exCout);	
		Assert.assertEquals(actCount, exCout,"Expected Count doesn't match");
	}

	public void verifyPendingRecordsOfGrid()throws InterruptedException {
		waitThread();
		int exCout = 0,actCount=0;
		String nextBtnColor="#337ab7", viewAuditTrail;
        int counter =0;
        String pagination = null; 
     
		btnHelper.click(clickAppToCollapse);
		
		//no of apps
		List<WebElement> noOfApps = driver.findElements(By.xpath("//*[@ng-if='app.id']"));
		
		for(int i = 0; i < noOfApps.size(); i++) {
			
			new Scroll().scrollTillElem(noOfApps.get(i));
			WebElement openApp = driver.findElement(By.xpath("(//*[@class='pull-right glyphicon glyphicon-chevron-right'])["+(i+1)+"]"));

			btnHelper.click(openApp);

			 //if found more than 10 records
	        if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
	                .size() != 0 == true) {
	        	
	        	//no of records in single app
	        	pagination = textBoxHelper.getText(recodsCount);
	        	   String [] words = pagination.split(" ");
	               System.err.println(words[6]);
	            while(!nextBtnColor.equals("#777777")) {
	                Thread.sleep(1000);
					List<WebElement> noOfRecds = driver.findElements(By.xpath("//*[@data-th='Created Date']"));

	    			for(int k = 1; k<=noOfRecds.size(); k++) {
	    				String createdDate = driver.findElement(By.xpath("(//*[@data-th='Record Status'])["+k+"]")).getText();
	    				exCout++;
	    				System.err.println("value of Record Status column:"+createdDate.split(" - ")[0]);
	    					if("Review Pending".equals(createdDate.split(" - ")[0].trim())){
	    					actCount++;	
	    				}else {
	    					Assert.assertEquals("Review Pending", createdDate.split(" - ")[0].trim(),"Date doesn't matched");
	    				}
	                }
	                System.err.println(counter);
	                nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
	                btnHelper.click(nextPagging);
	                Thread.sleep(1000);
	            }
	        } else {
	        	
	        	//no of records in single app
				List<WebElement> noOfRecds = driver.findElements(By.xpath("//*[@data-th='Created Date']"));
				
	        	for(int j = 1; j <= noOfRecds.size() ;j++) {
					
					String createdDate = driver.findElement(By.xpath("(//*[@data-th='Record Status'])["+j+"]")).getText();
					exCout++;
    				System.err.println("value of Record Status column:"+createdDate.split(" - ")[0]);

					if("Review Pending".equals(createdDate.split(" - ")[0].trim())){
						actCount++;	
					}else {
    					Assert.assertEquals("Review Pending", createdDate.split(" - ")[0].trim(),"Date doesn't matched");
					}
	        }	
			Thread.sleep(500);
		}
			helper.javascriptExecutorClick(closeBtnOfAppGrip);
	}
		System.err.println("actual value::"+actCount+"\nexpected value::"+exCout);	
		Assert.assertEquals(actCount, exCout,"Expected Count doesn't match");
	}

	
	public void verifyAppWithoutPermission() {
		Assert.assertEquals(findAppInAdvSeachGrid(new AppBuilderPageObject(driver).getAppTitle), false, "App found which is not having permission");
	}
	
	public void clickMyFavTab() {
		btnHelper.click(myFavoritesLabel);
	}
	
	//incomplete
	public void verifyMyFavTabColumns() {
		btnHelper.click(myFavoritesLabel);
	}
	
	public void addAppMyFav() throws Throwable  {
		
		if (driver.findElements(By.xpath("(//div[@ng-if='!vm.loadMyFavouriteData && vm.myFavoriteData && !vm.myFavoriteData.length'])[1]")).size() != 0) {
			
			new AdvancedSearch().select_App_from_Search_for_Apps_by_Names_search_box();
			new AdvancedSearch().click_on_Go_button();
			new AdvancedSearch().click_on_Add_To_Favorite_button();
			//below code will save my fav current time
			enterAddToFavoriteValidChar("Temp ("+ new DateTimeHelper().getCurrentDateTime()+")");
			new AdvancedSearch().click_on_Save_button();

		} else {
			System.err.println("App found in My Favourite tab");
		}

	}
	

	public void removeAppMyFav() throws Exception {
		clickRemoveIconMyFavorites();
		waitThread();
		btnHelper.click(removeYesMsgMyFavorite);
	}

	public void removeAllFromAppMyFav() throws Exception {

		driver.navigate().refresh();
		btnHelper.click(clickMyFavoriteTable);
		waitThread();

		while (driver.findElements(By.xpath("(//div[@ng-if='vm.myFavoriteData.length'])[2]/following::t[1]")).size() != 0) {

			int totlEnty = Integer.parseInt(totlEntyNoMyFavorite.getText());

			WebElement removeBtnOfMyfav = driver.findElement(By.xpath("(//a[@title='Remove']//following::tr)[" + (1 + totlEnty) + "]//i"));

			btnHelper.click(removeBtnOfMyfav);

			btnHelper.click(removeYesMsgMyFavorite);
			Thread.sleep(1000);
		}
	}
	
	public void verify_SearchName_LastUpdated_Action() throws Exception {
		
		if(myFavSrchName.isDisplayed()) {
			Assert.assertEquals(textBoxHelper.getText(myFavSrchName).trim(), "Search Name");
		} if(myFavLstUpdate.isDisplayed()) {
			Assert.assertEquals(textBoxHelper.getText(myFavLstUpdate).trim(), "Last Updated");
		} if(myFavAction.isDisplayed()) {
			Assert.assertEquals(textBoxHelper.getText(myFavAction).trim(), "Action");
		}
		
		removeAppMyFav();
	}
	
	public void verifySavedMyFavClickable() throws Exception {	
		Assert.assertEquals(findAppInAdvSeachGrid(new AppBuilderPageObject(driver).getAppTitle), false, "App which saved in My Favourite is not matching");
	}
	
	public void verifySavedMyFavLastUpdatedTime() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(myFavLastUpdateValue), myFavSaveTime);
	}	
	
}//end
