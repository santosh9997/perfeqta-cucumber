package com.cucumber.framework.helper.PageObject;

import java.awt.event.KeyAdapter;
import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.CheckBox.CheckBoxOrRadioButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Generic.GenericHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.settings.ObjectRepo;

public class CreateWindowPageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	public CheckBoxOrRadioButtonHelper checkBoxAndBtnHelper;
	public TextBoxHelper textBoxHelper;
	public DropDownHelper drpdwnHelper;
	public GenericHelper genricHelper;
	private final Logger log = LoggerHelper.getLogger(CreateWindowPageObject.class);
	public static String enterWindowName, windowId;
	Helper helper = new Helper();

	public CreateWindowPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		checkBoxAndBtnHelper = new CheckBoxOrRadioButtonHelper(driver);
		textBoxHelper = new TextBoxHelper(driver);
		drpdwnHelper = new DropDownHelper(driver);
		genricHelper = new GenericHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.qualificationPerformance.home']")
	public WebElement qualificationPerformanceTile;

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.qualificationPerformance.createWindow']")
	public WebElement createWindowTile;

	@FindBy(how = How.XPATH, using = "//input[@name='windowName']")
	public WebElement createWindowTextfield;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.windows.module._id']")
	public WebElement moduleDrpDwn;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.windows.app._id']")
	public WebElement appDrpDwn;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.windows.windowType']")
	public WebElement windowTypeDrpDwn;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.windows.windowPlan']")
	public WebElement windowPlanDrpDwn;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.thisSampleSize']")
	public WebElement sampleSizeDrpDwn;

	@FindBy(how = How.XPATH, using = "//input[@name='sampleSize']")
	public WebElement sampleSizeTextField;

	@FindBy(how = How.XPATH, using = "//input[@name='populationSize']")
	public WebElement populationSizeTextfield;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.windows.windowPeriod.month']")
	public WebElement monthDrpDwn;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.windows.windowPeriod.year']")
	public WebElement yearDrpDwn;

	@FindBy(how = How.XPATH, using = "//input[@name='sendEmailTextBox']")
	public WebElement emailAddressTextfield;

	@FindBy(how = How.XPATH, using = "//button[@class='btn btn-success ng-binding ng-scope']")
	public WebElement saveBtn;

	@FindBy(how = How.XPATH, using = "//button[@class='confirm']")
	public WebElement okConfirmBtn;

	@FindBy(how = How.XPATH, using = "//p[@style='display: block;']")
	public WebElement confirmMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.WINDOWNAMEISREQUIRED']")
	public WebElement windowNameRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.WINDOWNAMESHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement windowNameMinimumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.WINDOWNAMESHOULDNOTBEMORETHAN50CHARACTERSLONG']")
	public WebElement windowNameMaximumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.WINDOWNAMEMUSTBEUNIQUE']")
	public WebElement windowNameUniqueMsg;

	@FindBy(how = How.XPATH, using = "//textarea[@name='windowDescription']")
	public WebElement descriptionTextArea;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.DESCRIPTIONSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement descriptionMinimumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.DESCRIPTIONSHOULDNOTBEMORETHAN800CHARACTERSLONG']")
	public WebElement descriptionMaximumMsg;

	@FindBy(how = How.XPATH, using = "//div[3]/h3[@class='ng-binding']")
	public WebElement appDetailsHeading;

	@FindBy(how = How.XPATH, using = "//div[4]/h3[@class='ng-binding']")
	public WebElement testTypeSamplePlanHeading;

	@FindBy(how = How.XPATH, using = "//div[5]/h3[@class='ng-binding']")
	public WebElement frequencySettingHeading;

	@FindBy(how = How.XPATH, using = "//p[@translate='import.modr']")
	public WebElement moduleDrpDwnRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='text.ps2']")
	public WebElement appDrpDwnRequiredMsg;

	@FindBy(how = How.XPATH, using = "//select[@name='attributeSelect0']")
	public WebElement attributeEntityDrpDwn;

	@FindBy(how = How.XPATH, using = "//p[@class='err-message ng-binding ng-scope']")
	public WebElement attributeEntityRequiredMsg;

	@FindBy(how = How.XPATH, using = "//input[@name='attributeTextbox0']")
	public WebElement attributeEntityValueTextfield;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addAttribute($index)']")
	public WebElement attributeEntityAddIcon;

	@FindBy(how = How.XPATH, using = "//input[@name='attributeTextbox1']")
	public WebElement attributeEntityAddMultiple;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.removeAttribute($index)']")
	public WebElement attributeEntityRemove;

	@FindBy(how = How.XPATH, using = "//input[@aria-checked='true' and @name='Procedure']")
	public WebElement procedureByDefaulSelected;

	@FindBy(how = How.XPATH, using = "//input[@name='Procedure']")
	public WebElement procedureSelected;

	@FindBy(how = How.XPATH, using = "//input[@name='Question']")
	public WebElement questionSelected;

	@FindBy(how = How.XPATH, using = "//select[@name='testTypeProcedure']")
	public WebElement procedureDrpDwn;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.logireports.analysisreports.drilldown.PROC']")
	public WebElement procedureDrpDwnRequiredMsg;

	@FindBy(how = How.XPATH, using = "//select[@name='testTypeQuestion']")
	public WebElement questionDrpDwn;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.QUESTIONISREQUIRED']")
	public WebElement questionDrpDwnRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.WINDOWTYPEISREQUIRED']")
	public WebElement windowTypeRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.WINDOWPLANISREQUIRED']")
	public WebElement windowPlanRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.SAMPLESIZEISREQUIRED']")
	public WebElement sampleSizeRequiredMsg;

	@FindBy(how = How.XPATH, using = "//input[@name='maximumAllowedProcessFailure1']")
	public WebElement maximumAllowedProcessRequiredMsg;

	@FindBy(how = How.XPATH, using = "//input[@type='checkbox']")
	public WebElement checkAllowSecondStageCheckbox;

	@FindBy(how = How.XPATH, using = "//input[@name='allowSecondStage' and @aria-checked='true']")
	public WebElement checkAllowSecondStageCheckboxSelected;

	@FindBy(how = How.XPATH, using = "//p[@class='err-message ng-binding ng-scope']")
	public WebElement collectionDateRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='text.monthisrequired']")
	public WebElement monthRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='text.yearisrequired']")
	public WebElement yearRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@ng-if='vm.windowStartDate']")
	public WebElement monthYearMsg;

	@FindBy(how = How.XPATH, using = "//input[@name='repeatWindow']")
	public WebElement repeatWindowCheckbox;

	@FindBy(how = How.XPATH, using = "//input[@name='repeatWindow' and @aria-checked='true']")
	public WebElement repeatWindowCheckboxStatus;

	@FindBy(how = How.XPATH, using = "//span[@class='glyphicon glyphicon-info-sign text-primary hoversymbol repeat-window-size']")
	public WebElement informationIcon;

	@FindBy(how = How.XPATH, using = "//div[@class='tooltip ng-isolate-scope top fade in']")
	public WebElement informationIconMsg;

	@FindBy(how = How.XPATH, using = "//input[@aria-checked='true' and @value='All']")
	public WebElement siteAllIsDefaultSelected;

	@FindBy(how = How.XPATH, using = "//input[@value='All']")
	public WebElement selectSiteAll;

	@FindBy(how = How.XPATH, using = "//input[@value='Selection']")
	public WebElement selectSiteSelection;

	@FindBy(how = How.XPATH, using = "//em[@title='At least one site is required from each level']")
	public WebElement selectionRequiredMsg;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.allSelected[level._id]']")
	public WebElement clickAllCheckbox;

	@FindBy(how = How.XPATH, using = "//span[@ng-bind-html='value']")
	public WebElement clickAllCheckboxMsg;

	@FindBy(how = How.XPATH, using = "//ul/li[2]/div[2]/input")
	public WebElement clickAllCheckboxDepartment;

	@FindBy(how = How.XPATH, using = "//ul/li[3]/div[2]/input")
	public WebElement clickAllCheckboxSites;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.POPULATIONSIZEISREQUIRED']")
	public WebElement populationRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.THEPOPULATIONSIZESHOULDBEGREATERTHAN0']")
	public WebElement populationMinimumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.POPULATIONSIZESHOULDNOTBEMORETHAN10000']")
	public WebElement populationMaximumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.SAMPLESIZESHOULDBEATLEAST1']")
	public WebElement sampleSizeMinimumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.SAMPLESIZESHOULDNOTBEMORETHAN999']")
	public WebElement sampleSizeMaximumMsg;

	@FindBy(how = How.XPATH, using = "//input[@name='maximumAllowedProcessFailure']")
	public WebElement maximumAllowedProcessFailureTextfield;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.MAXIMUMALLOWEDPROCESSFAILUREINPERIODISREQUIRED']")
	public WebElement maximumAllowedProcessFailureRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.MAXIMUMALLOWEDPROCESSFAILUREINPERIODSHOULDBEATLEAST1']")
	public WebElement maximumAllowedProcessFailureMinimumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.MAXIMUMALLOWEDPROCESSFAILUREINPERIODSHOULDNOTBEMORETHAN99']")
	public WebElement maximumAllowedProcessFailureMaximumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.MAXIMUMALLOWEDPROCESSFAILUREINPERIODSHOULDNOTBEMORETHANSAMPLESIZE']")
	public WebElement sampleSizeLessThan;

	@FindBy(how = How.XPATH, using = "//input[@value='Hourly']")
	public WebElement hourlyRadioBtn;

	@FindBy(how = How.XPATH, using = "//input[@value='Weekly']")
	public WebElement weeklyRadioBtn;

	@FindBy(how = How.XPATH, using = "//input[@value='Monthly']")
	public WebElement monthlyRadioBtn;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.clearFrequencyAlert(createWindowForm)']")
	public WebElement windowFrequencyClearLink;

	@FindBy(how = How.XPATH, using = "//input[@aria-checked='true' and @value='Hourly']")
	public WebElement hourlyRadioClickable;

	@FindBy(how = How.XPATH, using = "//input[@aria-checked='true' and @value='Weekly']")
	public WebElement weeklyRadioClickable;

	@FindBy(how = How.XPATH, using = "//input[@aria-checked='true' and @value='Monthly']")
	public WebElement monthlyRadioClickable;

	@FindBy(how = How.XPATH, using = "//div/div[1]/div[2]/div/div[2]/div[1]")
	public WebElement hourlyColumnLabel;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.schedule.edit.EVERYHOURISREQUIRED']")
	public WebElement hourlyRequiredMsg;

	@FindBy(how = How.XPATH, using = "//input[@name='hourlyTextBox']")
	public WebElement hourlyTextfield;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.INVALIDNUMBERSHOULDBEATLEAST1']")
	public WebElement hourlyMinimumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.qualificationPerformance.edit.createwindow.INVALIDNUMBERCANNOTADDMORETHAN24HOURS']")
	public WebElement hourlyMaximumMsg;

	@FindBy(how = How.XPATH, using = "//p[@ng-if='!vm.windows.windowFrequencyStatusAlert.weekly.repeats.days.length']")
	public WebElement weeklyRequiredMsg;

	@FindBy(how = How.XPATH, using = "//input[@name='sendEmailTextBox']")
	public WebElement emailTextfield;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.EMAILREQ']")
	public WebElement emailRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.INVALIDEMAIL']")
	public WebElement emailInvalidMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.importdata.automated.Duplicate']")
	public WebElement emailDuplicateMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.schedule.edit.FORMULTIPLEEMAILADDRESSESUSECOMMA']")
	public WebElement emailInformationMsg;

	@FindBy(how = How.XPATH, using = "//button[@ui-sref='secure.qualificationPerformance.home']")
	public WebElement cancelBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='confirm']")
	public WebElement okBtn;

	@FindBy(how = How.XPATH, using = "//*[@style='display: block;']")
	public WebElement getId;

	// --------------------public methods--------------------

	public WebDriver getDriver() {
		return this.driver;

	}

	public void clickQualificationPerformanceTile() throws Exception {
		Thread.sleep(2000);
		// wait.until(ExpectedConditions.elementToBeClickable(qualificationPerformanceTile));
		btnHelper.click(qualificationPerformanceTile);
		// Thread.sleep(1000);
	}

	public void clickCreateWindowTile() throws Exception {
		// wait.until(ExpectedConditions.elementToBeClickable(createWindowTile));
		btnHelper.click(createWindowTile);
		// Thread.sleep(1000);
	}

	public void enterValidData(String windowName, String moduleSelect, String appSelect, String windowTypeSelect,
			String windowPlanSelect, String sampleSizeSelect, String monthSelect, String yearSelect,
			String emailAddress) throws Exception {
		enterWindowName = windowName + " - " + new Helper().randomNumberGeneration();
		textBoxHelper.clearAndSendKeys(createWindowTextfield, enterWindowName);
		// Thread.sleep(500);
		drpdwnHelper.selectDropDownText(moduleDrpDwn, moduleSelect);
		// Thread.sleep(500);
		drpdwnHelper.selectDropDownText(appDrpDwn, appSelect);
		// Thread.sleep(500);
		drpdwnHelper.selectDropDownText(windowTypeDrpDwn, windowTypeSelect);
		// Thread.sleep(500);
		drpdwnHelper.selectDropDownText(windowPlanDrpDwn, windowPlanSelect);
		// Thread.sleep(500);
		drpdwnHelper.selectDropDownText(sampleSizeDrpDwn, sampleSizeSelect);
		// Thread.sleep(500);
		drpdwnHelper.selectDropDownText(monthDrpDwn, monthSelect);
		// Thread.sleep(500);
		drpdwnHelper.selectDropDownText(yearDrpDwn, yearSelect);
		// Thread.sleep(500);
		textBoxHelper.clearAndSendKeys(emailAddressTextfield, emailAddress);
	}

	public void enterValidDataForQPFail(String windowName, String moduleSelect, String appSelect,
			String windowTypeSelect, String windowPlanSelect, String sampleSizeSelect, String monthSelect,
			String yearSelect, String emailAddress) throws Exception {
		
		enterWindowName = windowName + " - " + new Helper().randomNumberGeneration();
		textBoxHelper.clearAndSendKeys(createWindowTextfield, enterWindowName);
		 // Thread.sleep(500);
		drpdwnHelper.selectDropDownText(moduleDrpDwn, moduleSelect);
		  Thread.sleep(500);
		drpdwnHelper.selectDropDownText(appDrpDwn, appSelect);
		 Thread.sleep(500);
		drpdwnHelper.selectDropDownText(windowTypeDrpDwn, windowTypeSelect);
		 Thread.sleep(500);
		drpdwnHelper.selectDropDownText(windowPlanDrpDwn, windowPlanSelect);
		 Thread.sleep(500);
		drpdwnHelper.selectDropDownText(sampleSizeDrpDwn, sampleSizeSelect);
		// Thread.sleep(500);
		drpdwnHelper.selectDropDownText(monthDrpDwn, monthSelect);
		 Thread.sleep(500);
		drpdwnHelper.selectDropDownText(yearDrpDwn, yearSelect);
		// Thread.sleep(500);
		textBoxHelper.clearAndSendKeys(emailAddressTextfield, emailAddress);
	}

	public void clickSaveBtn() throws Exception {
		Thread.sleep(1000);
		saveBtn.click();
		// Thread.sleep(2000);

		String id = textBoxHelper.getText(getId);
		String[] words = id.split("ID ");

		String id1 = words[1];
		windowId = id1.substring(0, id1.length() - 1);
		System.out.println(windowId);
		Thread.sleep(1000);
		btnHelper.click(okBtn);
	}

	public void verifySaveFunctionality(String windowName) throws Exception {
		// int i=203;
		Assert.assertEquals(textBoxHelper.getText(confirmMsg), windowName + " " + ObjectRepo.reader.getConfirmMsg());
		btnHelper.click(okConfirmBtn);
		// Thread.sleep(1000);
	}

	public void emptyWindowNameField(String emptyValue) throws Exception {
		textBoxHelper.sendKeys(createWindowTextfield, emptyValue);
		textBoxHelper.clear(createWindowTextfield);
	}

	public void clickTabKey() throws Exception {
		createWindowTextfield.sendKeys(Keys.TAB);
	}

	public void verifyWindowNameRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(windowNameRequiredMsg), ObjectRepo.reader.getWindowNameRequiredMsg());
	}

	public void verifyWindowNameRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(windowNameRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void minWindowNameField(String minValue) throws Exception {
		textBoxHelper.clearAndSendKeys(createWindowTextfield, minValue);
	}

	public void verifyWindowNameMinimumMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(windowNameMinimumMsg), ObjectRepo.reader.getWindowNameMinimumMsg());
	}

	public void verifyWindowNameMinimumMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(windowNameMinimumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void maxWindowNameField(String maxValue) throws Exception {
		textBoxHelper.clearAndSendKeys(createWindowTextfield, maxValue);
	}

	public void verifyWindowNameMaximumMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(windowNameMaximumMsg), ObjectRepo.reader.getWindowNameMaximumMsg());
	}

	public void verifyWindowNameMaximumMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(windowNameMaximumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void uniqueWindowNameField(String uniqueValue) throws Exception {
		textBoxHelper.clearAndSendKeys(createWindowTextfield, uniqueValue);
	}

	public void verifyWindowNameUniqueMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(windowNameUniqueMsg), ObjectRepo.reader.getWindowNameUniqueMsg());
	}

	public void verifyWindowNameUniqueMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(windowNameUniqueMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void clickDescriptionTextarea() throws Exception {
		btnHelper.click(descriptionTextArea);
	}

	public void minDescriptionTextarea(String minDesc) throws Exception {
		textBoxHelper.clearAndSendKeys(descriptionTextArea, minDesc);
	}

	public void verifyDescriptionMinimumMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(descriptionMinimumMsg), ObjectRepo.reader.getDescriptionMinimumMsg());
	}

	public void verifyDescriptionMinimumMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(descriptionMinimumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void maxDescriptionTextarea(String maxDesc) throws Exception {
		textBoxHelper.clear(descriptionTextArea);
		for (int i = 0; i < 15; i++) {
			textBoxHelper.sendKeys(descriptionTextArea, maxDesc);
		}
	}

	public void verifyDescriptionMaximumMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(descriptionMaximumMsg), ObjectRepo.reader.getDescriptionMaximumMsg());
	}

	public void verifyDescriptionMaximumMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(descriptionMaximumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyAppDetailsHeading() throws Exception {
		Assert.assertEquals(genricHelper.isDisplayed(appDetailsHeading), true);
	}

	public void verifyTestTypeSamplePlanHeading() throws Exception {
		Assert.assertEquals(genricHelper.isDisplayed(testTypeSamplePlanHeading), true);
	}

	public void verifyFrequencySettingHeading() throws Exception {
		new Scroll().scrollTillElem(frequencySettingHeading);
		Assert.assertEquals(genricHelper.isDisplayed(frequencySettingHeading), true);
	}

	public void verifyModuleDefaultValue() throws Exception {
		Assert.assertEquals(drpdwnHelper.getFirstSelectedOption(moduleDrpDwn), ObjectRepo.reader.getModuleDrpDwn());
	}

	public void verifyAppDefaultValue() throws Exception {
		Assert.assertEquals(drpdwnHelper.getFirstSelectedOption(appDrpDwn), ObjectRepo.reader.getModuleDrpDwn());
	}

	public void clickModuleDrpDwn() throws Exception {
		btnHelper.click(moduleDrpDwn);
	}

	public void verifyclickModuleDrpDwn() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(moduleDrpDwn), ObjectRepo.reader.getModuleDrpDwnAllVal());
	}

	public void selectModuleDrpDwn() throws Exception {
		drpdwnHelper.selectDropDownIndex(moduleDrpDwn, 1);
	}

	public void selectAppDrpDwn(String appName) throws Exception {
		Thread.sleep(1000);
		drpdwnHelper.selectDropDownText(appDrpDwn, appName);
	}

	public void verifyModuledrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(moduleDrpDwnRequiredMsg),
				ObjectRepo.reader.getModuleDrpDwnRequiredMsg());
	}

	public void verifyModuledrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(moduleDrpDwnRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void clickAppDrpDwn() throws Exception {
		btnHelper.click(appDrpDwn);
	}

	public void verifyAppDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(appDrpDwnRequiredMsg), ObjectRepo.reader.getAppRequireValMsg());
	}

	public void verifyAppDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(appDrpDwnRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void clickAttributeEntityDrpDwn(String din) throws Exception {
		drpdwnHelper.selectDropDownText(attributeEntityDrpDwn, din);
	}

	public void selectAttributeEntityDrpDwn(String value) throws Exception {
		textBoxHelper.sendKeys(attributeEntityValueTextfield, value);
	}

	public void verifyAttributeEntityDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(attributeEntityRequiredMsg),
				ObjectRepo.reader.getAttributeEntityRequiredMsg());
	}

	public void verifyAttributeEntityDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(attributeEntityRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyAttributeEntityValueTextfield() throws Exception {
		Assert.assertEquals(genricHelper.isDisplayed(attributeEntityValueTextfield), true);
	}

	public void clickAddIconAttributeEntity() throws Exception {
		btnHelper.click(attributeEntityAddIcon);
	}

	public void verifyMultipleAddAttributeEntity() throws Exception {
		Assert.assertEquals(genricHelper.isDisplayed(attributeEntityAddMultiple), true);
	}

	public void clickRemoveIconAttributeEntity() throws Exception {
		btnHelper.click(attributeEntityRemove);
	}

	public void verifyRemoveAttributeEntity() throws Exception {
		boolean actual;
		if (driver.findElements(By.xpath("//input[@name='attributeTextbox1']")).size() != 0) {
			actual = false;
		} else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void verifyProcedureRadioBtnByDefault() throws Exception {
		Assert.assertEquals(checkBoxAndBtnHelper.isIselected(procedureByDefaulSelected), true);
	}

	public void clickQuestionRadioBtn() throws Exception {
		btnHelper.click(questionSelected);
	}

	public void clickProcedureRadioBtn() throws Exception {
		btnHelper.click(procedureSelected);
	}

	public void verifyOneRadioBtnIsSelected() throws Exception {
		boolean actual;
		if (driver.findElements(By.xpath("//input[@aria-checked='true' and @name='Question']")).size() != 0) {
			actual = false;
		} else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickProcedureTestTypeDrpDwn() throws Exception {
		drpdwnHelper.SelectUsingIndex(procedureDrpDwn, 1);
		// Thread.sleep(1000);
		drpdwnHelper.selectDropDownText(procedureDrpDwn, ObjectRepo.reader.getModuleDrpDwn());
	}

	public void verifyProcedureTestTypeDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(procedureDrpDwnRequiredMsg),
				ObjectRepo.reader.getProcedureDrpDwnRequiredMsg());
	}

	public void verifyProcedureTestTypeDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(procedureDrpDwnRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	/*
	 * public void selectAppsDrpDwn() throws Exception {
	 * drpdwnHelper.selectDropDownText(appDrpDwn, "test Automation don't touch");
	 * 
	 * }
	 */

	public void selectQuestionDrpDwn() throws Exception {
		clickQuestionRadioBtn();
		// Thread.sleep(1000);
		//ObjectRepo.reader.getModuleDrpDwn();
		drpdwnHelper.selectDropDownIndex(questionDrpDwn, 0);
		questionDrpDwn.sendKeys(Keys.TAB);
	}

	public void verifyQuestionTestTypeDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(questionDrpDwnRequiredMsg),
				ObjectRepo.reader.getQuestionDrpDwnRequiredMsg());
	}

	public void verifyQuestionTestTypeDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(questionDrpDwnRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void selectWindowTypeDrpDwn() throws Exception {
		drpdwnHelper.selectDropDownIndex(windowTypeDrpDwn, 1);
		// Thread.sleep(1000);
		drpdwnHelper.selectDropDownText(windowTypeDrpDwn, ObjectRepo.reader.getModuleDrpDwn());
	}

	public void verifyWindowTypeDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(windowTypeRequiredMsg), ObjectRepo.reader.getWindowTypeRequiredMsg());
	}

	public void verifyWindowTypeDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(windowTypeRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyWindowTypeDrpDwnOptions() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(windowTypeDrpDwn), ObjectRepo.reader.getWindowTypeDrpDwn());
	}

	public void selectWindowTypeDrpDwnFirstOption() throws Exception {
		drpdwnHelper.selectDropDownIndex(windowTypeDrpDwn, 1);
	}

	public void clickWindowPlanDrpDwnFirst() throws Exception {
		btnHelper.click(windowPlanDrpDwn);
	}

	public void selectWindowPlanDrpDwnFirstOption() throws Exception {
		drpdwnHelper.SelectUsingIndex(windowPlanDrpDwn, 1);
	}

	public void selectWindowPlanDrpDwnDefaultOption() throws Exception {
		// Thread.sleep(1000);
		drpdwnHelper.selectDropDownText(windowPlanDrpDwn, ObjectRepo.reader.getModuleDrpDwn());
	}

	public void verifyWindowPlanDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(windowPlanRequiredMsg), ObjectRepo.reader.getWindowPlanRequiredMsg());
	}

	public void verifyWindowPlanDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(windowPlanRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyWindowPlanDrpDwnOptions() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(windowPlanDrpDwn), ObjectRepo.reader.getWindowPlanDrpDwn());
	}

	public void selectBinomialWindowPlanDrpDwnOption(String windowTypeName) throws Exception {
		drpdwnHelper.selectDropDownText(windowTypeDrpDwn, windowTypeName);
	}

	public void verifyAfterSelectBinomialOptionDisplayOtherDrpDwn() throws Exception {
		boolean plan = genricHelper.isDisplayed(windowPlanDrpDwn);
		boolean size = genricHelper.isDisplayed(sampleSizeDrpDwn);
		Assert.assertEquals(plan, size);
	}

	public void selectHypergeometricWindowPlanDrpDwnOption(String windoTypeDrpDwn) throws Exception {
		drpdwnHelper.selectDropDownText(windowTypeDrpDwn, windoTypeDrpDwn);
	}

	public void verifyAfterSelectHypergeometricOptionDisplayOtherDrpDwn() throws Exception {
		boolean population = genricHelper.isDisplayed(populationSizeTextfield);
		boolean size = genricHelper.isDisplayed(sampleSizeDrpDwn);
		Assert.assertEquals(population, size);
	}

	public void selectCustomizedWindowPlanDrpDwnOption(String customizewindow) throws Exception {
		drpdwnHelper.selectDropDownText(windowTypeDrpDwn, customizewindow);
	}

	public void verifyAfterSelectCustomizedOptionDisplayOtherDrpDwn() throws Exception {
		boolean size = genricHelper.isDisplayed(sampleSizeTextField);
		Assert.assertEquals(true, size);
	}

	public void verifySampleSizeDrpDwnIsDisabled() throws Exception {
		boolean size = textBoxHelper.isEnabled(sampleSizeDrpDwn);
		Assert.assertEquals(false, size);
	}

	public void clickSampleSizeDrpDwn() throws Exception {
		drpdwnHelper.SelectUsingIndex(sampleSizeDrpDwn, 1);
		// Thread.sleep(1000);
		drpdwnHelper.selectDropDownText(sampleSizeDrpDwn, ObjectRepo.reader.getModuleDrpDwn());
	}

	public void verifySampleSizeDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(sampleSizeRequiredMsg), ObjectRepo.reader.getSampleSizeRequiredMsg());
	}

	public void verifySampleSizeDrpDwnRequiredMsgColor() throws Exception {
		new Scroll().scrollTillElem(windowPlanDrpDwn);
		Assert.assertEquals(btnHelper.FontColorCodeHex(sampleSizeRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyMaximumAllowedProcessFailurePeriodIsDisabled() throws Exception {
		boolean size = textBoxHelper.isEnabled(maximumAllowedProcessRequiredMsg);
		Assert.assertEquals(false, size);
	}

	public void selectWindowPlanDrpDwnValue() throws Exception {
		drpdwnHelper.selectDropDownIndex(windowPlanDrpDwn, 1);
	}

	public void selectSampleSizeDrpDwnValue() throws Exception {
		drpdwnHelper.selectDropDownIndex(sampleSizeDrpDwn, 1);
	}

	public void checkMaxAllowedProcessFailureIsDisabled() throws Exception {
		btnHelper.click(checkAllowSecondStageCheckbox);
	}

	public void verifyMaxAllowedProcessFailureIsDisabled() throws Exception {
		btnHelper.click(checkAllowSecondStageCheckbox);
	}

	public void verifyMaxAllowedProcessFailureClickable() throws Exception {
		boolean actual = checkBoxAndBtnHelper.isIselected(checkAllowSecondStageCheckboxSelected);
		Assert.assertEquals(actual, true);
	}

	public void selectCollectedDateAttributeEntityDrpDwn(String collectionDate) throws Exception {
		drpdwnHelper.selectDropDownText(attributeEntityDrpDwn, collectionDate);
	}

	public void verifyCollectionRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(collectionDateRequiredMsg),
				ObjectRepo.reader.getCollectionDateRequiredMsg());
	}

	public void verifyCollectionRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(collectionDateRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void clickMonthDrpDwn() throws Exception {
		drpdwnHelper.SelectUsingIndex(monthDrpDwn, 1);
		// Thread.sleep(2000);
		drpdwnHelper.SelectUsingIndex(monthDrpDwn, 0);
	}

	public void clickonMonthDrpDwn() throws Exception {
		new Scroll().scrollTillElem(emailAddressTextfield);
		// Thread.sleep(1000);
		btnHelper.click(monthDrpDwn);

	}

	public void verifyMonthDrpDwnValues() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(monthDrpDwn), ObjectRepo.reader.getMonthDrpDwn());
	}

	public void verifyMonthDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(monthRequiredMsg), ObjectRepo.reader.getMonthRequireValMsg());
	}

	public void verifyMonthDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(monthRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void clickYearDrpDwn() throws Exception {
		drpdwnHelper.selectDropDownIndex(yearDrpDwn, 1);
		// Thread.sleep(500);
		drpdwnHelper.selectDropDownIndex(yearDrpDwn, 0);
	}

	public void verifyYearDrpDwnDefaultValues() throws Exception {
		Assert.assertEquals(drpdwnHelper.getFirstSelectedOption(yearDrpDwn), ObjectRepo.reader.getYearDrpDwn());
	}

	public void verifyYearDrpDwnRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(yearRequiredMsg), ObjectRepo.reader.getYearRequiredValMsg());
	}

	public void verifyYearDrpDwnRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(yearRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void selectCurrentMonthInDrpDwn(String month) throws Exception {
		drpdwnHelper.selectDropDownText(monthDrpDwn, month);
	}

	public void selectCurrentYearInDrpDwn(String year) throws Exception {
		drpdwnHelper.selectDropDownText(yearDrpDwn, year);
	}

	public void verifyMonthYearInformationMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(monthYearMsg), ObjectRepo.reader.getMonthYearMsgForCurWindow());
	}

	public void verifyMonthYearInformationMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(monthYearMsg), ObjectRepo.reader.getEmailInfoColor());
	}

	public void selectNextMonthInDrpDwn(String nextMonth) throws Exception {
		drpdwnHelper.selectDropDownText(monthDrpDwn, nextMonth);
	}

	public void verifySecondMonthYearInformationMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(monthYearMsg), ObjectRepo.reader.getMonthYearMsg());
	}

	public void clickRepeatWindowCheckbox() throws Exception {
		btnHelper.click(repeatWindowCheckbox);
	}

	public void verifyClickableRepeatWindowCheckbox() throws Exception {
		boolean actual = checkBoxAndBtnHelper.isIselected(repeatWindowCheckboxStatus);
		Assert.assertEquals(actual, true);
	}

	public void mouseHoverInformation() throws Exception {
		btnHelper.click(informationIcon);
	}

	public void verifyMouseHoverInformationMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(informationIconMsg), ObjectRepo.reader.getInformationIconMsg());
	}

	public void verifySiteAllIsSelectedByDrfault() throws Exception {
		boolean actual = checkBoxAndBtnHelper.isIselected(siteAllIsDefaultSelected);
		new Scroll().scrollTillElem(siteAllIsDefaultSelected);
		Assert.assertEquals(actual, true);
	}

	public void clickSitesAllRadioBtn() throws Exception {
		btnHelper.click(selectSiteAll);
	}

	public void clickSitesSelectionRadioBtn() throws Exception {
		btnHelper.click(selectSiteSelection);
	}

	public void verifySiteSelectionRequiredMsg() throws Exception {
		new Scroll().scrollTillElem(selectionRequiredMsg);
		Assert.assertEquals(textBoxHelper.getText(selectionRequiredMsg), ObjectRepo.reader.getSelectionRequiredMsg());
	}

	public void verifySiteSelectionRequiredMsgColor() throws Exception {
		new Scroll().scrollTillElem(selectionRequiredMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(selectionRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void selectValueInModuleDrpDwn(String selectModule) throws Exception {
		textBoxHelper.sendKeys(moduleDrpDwn, selectModule);
	}

	public void selectValueInAppDrpDwn(String selectApp) throws Exception {
		// Thread.sleep(1000);
		textBoxHelper.sendKeys(appDrpDwn, selectApp);
	}

	/*
	 * public void selectAllCheckboxOfSelection() throws Exception { appDrpDwn
	 * textBoxHelper.sendKeys(); }
	 */

	public void clickAllCheckboxOfSelection() throws Exception {
		btnHelper.click(clickAllCheckbox);
		// Thread.sleep(500);
	}

	public void verifyAllSelectionOfRegion() throws Exception {
		new Scroll().scrollTillElem(monthDrpDwn);
		Assert.assertEquals(textBoxHelper.getText(clickAllCheckboxMsg), ObjectRepo.reader.getClickAllCheckboxMsg());
	}

	public void verifyAllSelectionOfRegionColor() throws Exception {
		new Scroll().scrollTillElem(monthDrpDwn);
		Assert.assertEquals(btnHelper.FontColorCodeHex(clickAllCheckboxMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void clickAllCheckboxOfDepartment() throws Exception {
		btnHelper.click(clickAllCheckboxDepartment);
		// Thread.sleep(500);
	}

	public void verifyAllSelectionOfDepatment() throws Exception {
		new Scroll().scrollTillElem(monthDrpDwn);
		Assert.assertEquals(textBoxHelper.getText(clickAllCheckboxMsg),
				ObjectRepo.reader.getClickAllCheckboxMsgForSite());
	}

	public void clickAllCheckboxOfSites() throws Exception {
		btnHelper.click(clickAllCheckboxSites);
		// Thread.sleep(500);
	}

	public void verifyAllSelectionOfSitesNoMsg() throws Exception {
		new Scroll().scrollTillElem(monthDrpDwn);
		boolean sites = driver.findElements(By.xpath("//span[@ng-bind-html='value']")).size() == 0;
		boolean actual;
		if (sites == true) {
			actual = true;
		} else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void selectHypergeometricOption() throws Exception {
		drpdwnHelper.SelectUsingIndex(windowTypeDrpDwn, 2);
		// Thread.sleep(500);
	}

	public void clickPopulationTextbox() throws Exception {
		btnHelper.click(populationSizeTextfield);
		populationSizeTextfield.sendKeys(Keys.TAB);
	}

	public void verifyPopulationRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(populationRequiredMsg), ObjectRepo.reader.getPopulationRequiredMsg());
	}

	public void verifyPopulationRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(populationRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterPopulationMinimumTextbox(String minValue) throws Exception {
		textBoxHelper.sendKeys(populationSizeTextfield, minValue);
	}

	public void verifyPopulationMinimumMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(populationMinimumMsg), ObjectRepo.reader.getPopulationMinimumMsg());
	}

	public void verifyPopulationMinimumMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(populationMinimumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterPopulationMaximumTextbox(String maxValue) throws Exception {
		textBoxHelper.sendKeys(populationSizeTextfield, maxValue);
	}

	public void verifyPopulationMaximumMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(populationMaximumMsg), ObjectRepo.reader.getPopulationMaximumMsg());
	}

	public void verifyPopulationMaximumMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(populationMaximumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterPopulationValidTextbox(String validValue) throws Exception {
		textBoxHelper.sendKeys(populationSizeTextfield, validValue);
	}

	public void verifyPopulationNoValidation() throws Exception {
		boolean min = driver.findElements(By.xpath(
				"//p[@translate='secure.qualificationPerformance.edit.createwindow.THEPOPULATIONSIZESHOULDBEGREATERTHAN0']"))
				.size() == 0;
		boolean max = driver.findElements(By.xpath(
				"//p[@translate='secure.qualificationPerformance.edit.createwindow.POPULATIONSIZESHOULDNOTBEMORETHAN10000']"))
				.size() == 0;
		boolean req = driver
				.findElements(By.xpath(
						"//p[@translate='secure.qualificationPerformance.edit.createwindow.POPULATIONSIZEISREQUIRED']"))
				.size() == 0;
		boolean actual;
		if (min == true && max == true && req == true) {
			actual = true;
		} else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void selectCustomizedInWindowTypeDrpDwn() throws Exception {
		drpdwnHelper.selectDropDownIndex(windowTypeDrpDwn, 3);
	}

	public void clickSampleSizeTextbox() throws Exception {
		btnHelper.click(sampleSizeTextField);
	}

	public void enterSampleSizeMinimumValue(String minValue) throws Exception {
		textBoxHelper.sendKeys(sampleSizeTextField, minValue);
	}

	public void verifySampleSizeMinimumMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(sampleSizeMinimumMsg), ObjectRepo.reader.getSampleSizeMinimumMsg());
	}

	public void verifySampleSizeMinimumMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(sampleSizeMinimumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterSampleSizeMaximumValue(String maxValue) throws Exception {
		textBoxHelper.sendKeys(sampleSizeTextField, maxValue);
	}

	public void verifySampleSizeMaximumMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(sampleSizeMaximumMsg), ObjectRepo.reader.getSampleSizeMaximumMsg());
	}

	public void verifySampleSizeMaximumMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(sampleSizeMaximumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterSampleSizeValidValue(String validValue) throws Exception {
		textBoxHelper.sendKeys(sampleSizeTextField, validValue);
	}

	public void verifySampleSizeNoValidation() throws Exception {
		boolean min = driver.findElements(By.xpath(
				"//p[@translate='secure.qualificationPerformance.edit.createwindow.SAMPLESIZESHOULDBEATLEAST1']"))
				.size() == 0;
		boolean max = driver.findElements(By.xpath(
				"//p[@translate='secure.qualificationPerformance.edit.createwindow.SAMPLESIZESHOULDNOTBEMORETHAN999']"))
				.size() == 0;
		boolean req = driver
				.findElements(By.xpath(
						"//p[@translate='secure.qualificationPerformance.edit.createwindow.SAMPLESIZEISREQUIRED']"))
				.size() == 0;
		boolean actual;
		if (min == true && max == true && req == true) {
			actual = true;
		} else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickMaximumAllowedProcessFailureTextfield() throws Exception {
		btnHelper.click(maximumAllowedProcessFailureTextfield);
		maximumAllowedProcessFailureTextfield.sendKeys(Keys.TAB);
	}

	public void verifyMaximumAllowedProcessFailureRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(maximumAllowedProcessFailureRequiredMsg),
				ObjectRepo.reader.getMaximumAllowedProcessFailureRequiredMsg());
	}

	public void verifyMaximumAllowedProcessFailureRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(maximumAllowedProcessFailureRequiredMsg),
				ObjectRepo.reader.getValMsgColor());
	}

	public void enterMaximumAllowedProcessFailureMinimum(String minValue) throws Exception {
		textBoxHelper.sendKeys(maximumAllowedProcessFailureTextfield, minValue);
	}

	public void verifyMaximumAllowedProcessFailureMinimumMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(maximumAllowedProcessFailureMinimumMsg),
				ObjectRepo.reader.getMaximumAllowedProcessFailureMinimumMsg());
	}

	public void verifyMaximumAllowedProcessFailureMinimumMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(maximumAllowedProcessFailureMinimumMsg),
				ObjectRepo.reader.getValMsgColor());
	}

	public void enterMaximumAllowedProcessFailureMaximum(String maxValue) throws Exception {
		textBoxHelper.sendKeys(maximumAllowedProcessFailureTextfield, maxValue);
	}

	public void verifyMaximumAllowedProcessFailureMaximumMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(maximumAllowedProcessFailureMaximumMsg),
				ObjectRepo.reader.getMaximumAllowedProcessFailureMaximumMsg());
	}

	public void verifyMaximumAllowedProcessFailureMaximumMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(maximumAllowedProcessFailureMaximumMsg),
				ObjectRepo.reader.getValMsgColor());
	}

	public void enterNumericValueSampleSizeTextfield(String lessThan) throws Exception {
		textBoxHelper.sendKeys(sampleSizeTextField, lessThan);
	}

	public void enterNumericValueMaximumAllowedProcessFailureTextfield(String greaterThan) throws Exception {
		textBoxHelper.sendKeys(maximumAllowedProcessFailureTextfield, greaterThan);
	}

	public void verifySampleSizeMinimumAllowedProcessFailureMaximumMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(sampleSizeLessThan), ObjectRepo.reader.getSampleSizeLessThan());
	}

	public void verifySampleSizeMinimumAllowedProcessFailureMaximumMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(sampleSizeLessThan), ObjectRepo.reader.getValMsgColor());
	}

	public void enterValidNumericValueMaximumAllowedProcessFailureTextfield(String validValue) throws Exception {
		textBoxHelper.sendKeys(maximumAllowedProcessFailureTextfield, validValue);
	}

	public void verifyValidNumericValueMaximumAllowedProcessFailureTextfield() throws Exception {
		boolean actual = driver.findElements(By.xpath(
				"//p[@translate='secure.qualificationPerformance.edit.createwindow.MAXIMUMALLOWEDPROCESSFAILUREINPERIODSHOULDNOTBEMORETHANSAMPLESIZE']"))
				.size() == 0;
		Assert.assertEquals(actual, true);
	}

	public void clickHourlyRadioBtn() throws Exception {
		btnHelper.click(hourlyRadioBtn);
	}

	public void clickWeeklyRadioBtn() throws Exception {
		btnHelper.click(weeklyRadioBtn);
		// new Scroll().scrollDown(ObjectRepo.driver);
		new Scroll().scrollTillElem(weeklyRadioBtn);
	}

	public void clickMonthlyRadioBtn() throws Exception {
		btnHelper.click(monthlyRadioBtn);
	}

	public void verifyHourlyRadioBtnClickable() throws Exception {
		boolean actual = checkBoxAndBtnHelper.isIselected(hourlyRadioClickable);
		Assert.assertEquals(actual, true);
	}

	public void verifyWeeklyRadioBtnClickable() throws Exception {
		boolean actual = checkBoxAndBtnHelper.isIselected(weeklyRadioClickable);
		Assert.assertEquals(actual, true);
	}

	public void verifyMontlyRadioBtnClickable() throws Exception {
		boolean actual = checkBoxAndBtnHelper.isIselected(monthlyRadioClickable);
		Assert.assertEquals(actual, true);
	}

	public void verifyWindowFrequencyClearLinkColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(windowFrequencyClearLink), ObjectRepo.reader.getBlueColor());
	}

	public void clickWindowFrequencyClearLink() throws Exception {
		btnHelper.click(windowFrequencyClearLink);
	}

	public void verifyHourlyRadioBtnAfterClear() throws Exception {
		boolean actual;
		if (driver.findElements(By.xpath("//input[@aria-checked='true' and @value='Hourly']")).size() != 0) {
			actual = false;
		} else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void verifyWeeklyRadioBtnAfterClear() throws Exception {
		boolean actual;
		if (driver.findElements(By.xpath("//input[@aria-checked='true' and @value='Weekly']")).size() != 0) {
			actual = false;
		} else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void verifyMontlyRadioBtnAfterClear() throws Exception {
		boolean actual;
		if (driver.findElements(By.xpath("//input[@aria-checked='true' and @value='Monthly']")).size() != 0) {
			actual = false;
		} else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void verifyHourlyColumnLabel() throws Exception {
		String actual = textBoxHelper.getText(hourlyColumnLabel);
		Assert.assertEquals(actual, "Every\n" + "hour(s)");
	}

	public void verifyHourlyRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(hourlyRequiredMsg), ObjectRepo.reader.getHoursRequireValMsg());
	}

	public void verifyHourlyRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(hourlyRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void clickHourlyTextfield() throws Exception {
		btnHelper.click(hourlyTextfield);
	}

	public void enterMinimumHourlyTextfield(String minValue) throws Exception {
		textBoxHelper.sendKeys(hourlyTextfield, minValue);
	}

	public void verifyHourlyMinimumMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(hourlyMinimumMsg), ObjectRepo.reader.getHourlyMinimumMsg());
	}

	public void verifyHourlyMinimumMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(hourlyMinimumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterMaximumHourlyTextfield(String maxValue) throws Exception {
		textBoxHelper.sendKeys(hourlyTextfield, maxValue);
	}

	public void verifyHourlyMaximumMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(hourlyMaximumMsg), ObjectRepo.reader.getHourlyTextboxValMsgForMax());
	}

	public void verifyHourlyMaximumMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(hourlyMaximumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterValidHourlyTextfield(String validValue) throws Exception {
		textBoxHelper.sendKeys(hourlyTextfield, validValue);
	}

	public void verifyHourlyValidValue() throws Exception {
		boolean req = driver.findElements(By.xpath("//p[@translate='secure.admin.schedule.edit.EVERYHOURISREQUIRED']"))
				.size() == 0;
		boolean min = driver.findElements(By.xpath(
				"//p[@translate='secure.qualificationPerformance.edit.createwindow.INVALIDNUMBERSHOULDBEATLEAST1']"))
				.size() == 0;
		boolean max = driver.findElements(By.xpath(
				"//p[@translate='secure.qualificationPerformance.edit.createwindow.INVALIDNUMBERCANNOTADDMORETHAN24HOURS']"))
				.size() == 0;
		boolean actual;
		if (min == true && max == true && req == true) {
			actual = false;
		} else {
			actual = true;
		}
		Assert.assertEquals(actual, false);
	}

	public void verifyWeeklyCheckboxAllSelectedByDefault() throws Exception {
		int count = 0;
		for (int i = 1; i <= 7; i++) {
			if (driver.findElements(By.xpath("//div/div/div/label[" + i + "]/input")).size() != 0) {
				count++;
			} else {
				System.out.println("... Else ...");
			}
		}
		Assert.assertEquals(count, 7);
	}

	public void clickAllWeeklyCheckbox() throws Exception {
		for (int i = 1; i <= 7; i++) {
			driver.findElement(By.xpath("//div/div/div/label[" + i + "]/input")).click();
		}
	}

	public void verifyWeeklyCheckboxAllUnselected() throws Exception {
		int count = 0;
		for (int i = 1; i <= 7; i++) {
			if (driver.findElements(By.xpath("//div/div/div/label[" + i + "]/input")).size() == 0) {
				count++;
			} else {
				System.out.println("... Else ...");
			}
		}

		Assert.assertEquals(count, 0);
	}

	public void uncheckAllWeeklyCheckbox() throws Exception {
		for (int i = 1; i <= 7; i++) {
			driver.findElement(By.xpath("//div/div/div/label[" + i + "]/input")).click();
		}
	}

	public void verifyWeeklyRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(weeklyRequiredMsg), ObjectRepo.reader.getWeeklyRequiredMsg());
	}

	public void verifyWeeklyRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(weeklyRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyWeeklyNoValidationMsg() throws Exception {
		boolean actual;
		if (driver
				.findElements(
						By.xpath("//p[@ng-if='!vm.windows.windowFrequencyStatusAlert.weekly.repeats.days.length']"))
				.size() != 0) {
			actual = false;
		} else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void clickEmailAddressTextfield() throws Exception {
		btnHelper.click(emailTextfield);
		emailTextfield.sendKeys(Keys.TAB);
	}

	public void verifyEmailAddressRequiredMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(emailRequiredMsg), ObjectRepo.reader.getEmailRequireValMsg());
	}

	public void verifyEmailAddressRequiredMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(emailRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterInvalidEmailAddress(String invalidEmail) throws Exception {
		textBoxHelper.clearAndSendKeys(emailTextfield, invalidEmail);
	}

	public void verifyInvalidEmailAddressMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(emailInvalidMsg), ObjectRepo.reader.getInvalidEmailValMsg());
	}

	public void verifyInvalidEmailAddressMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(emailInvalidMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyDuplicateEmailAddressMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(emailDuplicateMsg), ObjectRepo.reader.getEmailDuplicateMsg());
	}

	public void verifyDuplicateEmailAddressMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(emailDuplicateMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterDuplicateEmailAddress(String duplicateEmail) throws Exception {
		textBoxHelper.clearAndSendKeys(emailTextfield, duplicateEmail);
	}

	public void enterValidEmailAddress(String validEmail) throws Exception {
		textBoxHelper.clearAndSendKeys(emailTextfield, validEmail);
	}

	public void verifyValidEmailAddress() throws Exception {
		boolean req = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.EMAILREQ']"))
				.size() == 0;
		boolean invalid = driver
				.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.INVALIDEMAIL']"))
				.size() == 0;
		boolean duplicate = driver.findElements(By.xpath("//p[@translate='secure.importdata.automated.Duplicate']"))
				.size() == 0;
		boolean actual;
		if (invalid == true && duplicate == true && req == true) {
			actual = false;
		} else {
			actual = true;
		}
		Assert.assertEquals(actual, false);
	}

	public void verifyInformationEmailAddressMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(emailInformationMsg), ObjectRepo.reader.getEmailInformationMsg());
	}

	public void verifyInformationEmailAddressMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(emailInformationMsg), ObjectRepo.reader.getEmailInfoColor());
	}

	public void verifyTextOfSaveBtn() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(saveBtn), ObjectRepo.reader.getSaveBtn());
	}

	public void verifyTextOfSaveBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(saveBtn), ObjectRepo.reader.getLightGreenColor());
	}

	public void verifyTextOfCancelBtn() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(cancelBtn), ObjectRepo.reader.getCancelBtn());
	}

	public void verifyTextOfCancelBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(cancelBtn), ObjectRepo.reader.getDarkGreyColor());
	}

}
