package com.cucumber.framework.helper.PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.interfaces.IconfigReader;
import com.cucumber.framework.settings.ObjectRepo;

public class LoginPageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private CommonFunctionPageObject commonFnPageObject;
	private WaitHelper waitObj;
	private final Logger log = LoggerHelper.getLogger(LoginPageObject.class);
	private TextBoxHelper textBoxHelper;

	public LoginPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		commonFnPageObject = new CommonFunctionPageObject(driver);
		waitObj = new WaitHelper(driver, ObjectRepo.reader);
		textBoxHelper = new TextBoxHelper(driver);

	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ng-if='(loginForm.username.$dirty || loginForm.username.$touched) && loginForm.username.$error.required']")
	public WebElement usernameMandatoryValidation;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='(loginForm.password.$dirty || loginForm.password.$touched) && loginForm.password.$error.required']")
	public WebElement passwordMandatoryValidation;

	@FindBy(how = How.XPATH, using = "//p[@ng-if='vm.loginFailed']")
	public WebElement invalidLoginMessage;

	@FindBy(how = How.XPATH, using = "//*[@ng-bind-html='step.ncyBreadcrumbLabel']")
	public WebElement homepageBreadcrumb;

	@FindBy(how = How.XPATH, using = "//h3[contains(text(),'User Login')]")
	public WebElement loginlabel;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.No()']") // button[@ng-click='vm.No()']
	public WebElement btnNo;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.Yes({id: vm.adminId})']")
	public WebElement btnYes;

	@FindBy(how = How.XPATH, using = "//*[@ng-mousedown='vm.onCLickForgotpassword()']")
	public WebElement forgotPassLink;

	@FindBy(how = How.XPATH, using = "//*[@id=\"forgot-password\"]/h3")
	public WebElement forgotPassLabel;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.forgotPassword.username']")
	public WebElement forgotPassUserName;

	@FindBy(how = How.XPATH, using = "//*[@translate='div.usernamereq']")
	public WebElement forgotPassUsrNameReqValiMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='div.div-user2char']")
	public WebElement forgotPassUsrNameMinValiMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='div.div-user20char']")
	public WebElement forgotPassUsrNameMaxValiMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.submit(vm.forgotPassword)']")
	public WebElement forgotPassSubmitBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-danger11 btn-default ng-binding']")
	public WebElement forgotPassCancelBtn;

	@FindBy(how = How.XPATH, using = "//*[@id=\"bs-example-navbar-collapse-6\"]/ul/li[1]/a[contains(text(),'Home')]")
	public WebElement homeLabel;

	// --------------------public methods--------------------

	public WebDriver getDriver() {
		return this.driver;
	}

	public void verifyLoginBtnLable() {
		Assert.assertEquals(loginlabel.getText(), ObjectRepo.reader.getLoginlabel());
	}

	public String validateUsernamemsg(String validateUsername) {
		log.info(validateUsername);
		waitObj.waitForElementVisible(usernameMandatoryValidation);
		Assert.assertEquals(textBoxHelper.getText(usernameMandatoryValidation), ObjectRepo.reader.getUsernameRequireValMsg());
		return validateUsername;
	}

	public String validatePasswordmsg(String validatePasswrod) {
		log.info(validatePasswrod);
		waitObj.waitForElementVisible(passwordMandatoryValidation);
		Assert.assertEquals(textBoxHelper.getText(passwordMandatoryValidation), ObjectRepo.reader.getPasswordMandatoryValidation());
		return validatePasswrod;
	}

	public void verifyInvalidLoginMsg() {
		log.info(" verification of inValidLogin message");
		waitObj.waitForElementVisible(invalidLoginMessage);
		Assert.assertEquals(textBoxHelper.getText(invalidLoginMessage),ObjectRepo.reader.getInvalidLoginMessage());
	}

	public void homepageverification() {
		log.info(homepageBreadcrumb);
		waitObj.waitForElementVisible(homepageBreadcrumb);
		Assert.assertEquals(textBoxHelper.getText(homepageBreadcrumb), ObjectRepo.reader.getHomeLabel());
	}

	public void loginlabelverification() {
		log.info(loginlabel);
		waitObj.waitForElementVisible(loginlabel);
		Assert.assertEquals(textBoxHelper.getText(loginlabel), ObjectRepo.reader.getLoginlabel());
	}

	public void verifyUserNameReqMsgColor() {
		log.info(usernameMandatoryValidation);
		waitObj.waitForElementVisible(usernameMandatoryValidation);
		Assert.assertEquals(btnHelper.FontColorCodeHex(usernameMandatoryValidation), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyPasswordReqMsgColor() {
		log.info(passwordMandatoryValidation);
		waitObj.waitForElementVisible(passwordMandatoryValidation);
		Assert.assertEquals(btnHelper.FontColorCodeHex(passwordMandatoryValidation), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyInvalidLoginMsgColor() {
		log.info(invalidLoginMessage);
		waitObj.waitForElementVisible(invalidLoginMessage);
		Assert.assertEquals(btnHelper.FontColorCodeHex(invalidLoginMessage), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyBtnLoginColor() {
		log.info(commonFnPageObject.btn_login);
		waitObj.waitForElementVisible(commonFnPageObject.btn_login);
		Assert.assertEquals(btnHelper.BtnColor(commonFnPageObject.btn_login), ObjectRepo.reader.getCommonFnPageObjectColor());
	}

	public void btnNoColor() {
		log.info("Already loged in user No button color verification");
		waitObj.waitForElementVisible(btnNo);
		Assert.assertEquals(btnHelper.BtnColor(btnNo), ObjectRepo.reader.getCommonFnPageObjectColor());
	}

	public void btnYesColor() {
		log.info("Already logged in user Yes buton color verification");
		waitObj.waitForElementVisible(btnYes);
		Assert.assertEquals(btnHelper.BtnColor(btnYes), ObjectRepo.reader.getCommonFnPageObjectColor());
	}

	public void verifyforgotPassLinkColor() {
		log.info(forgotPassLink);
		waitObj.waitForElementVisible(forgotPassLink);
		Assert.assertEquals(btnHelper.FontColorCodeHex(forgotPassLink), ObjectRepo.reader.getForgotPassLinkGreycolor());
	}

	public void verifyforgotPassLinkText() {
		log.info(forgotPassLink);
		waitObj.waitForElementVisible(forgotPassLink);
		Assert.assertEquals(textBoxHelper.getText(forgotPassLink), ObjectRepo.reader.getForgotPassLink());
	}

	public void verifyforgotPassLableText() {
		log.info(forgotPassLabel);
		waitObj.waitForElementVisible(forgotPassLabel);
		Assert.assertEquals(textBoxHelper.getText(forgotPassLabel), ObjectRepo.reader.getForgotPassLabel());
	}

	public void forgotPassLinkClick() {
		log.info(forgotPassLink);
		waitObj.waitForElementVisible(forgotPassLink);
		btnHelper.click(forgotPassLink);
	}

	public void verifyforgotPassUsrNameReqValiMsg() {
		log.info(forgotPassUsrNameReqValiMsg);
		waitObj.waitForElementVisible(forgotPassUsrNameReqValiMsg);
		Assert.assertEquals(textBoxHelper.getText(forgotPassUsrNameReqValiMsg), ObjectRepo.reader.getUsernameRequireValMsg());
	}

	public void verifyforgotPassUsrNameReqValiMsgColor() {
		log.info(forgotPassUsrNameReqValiMsg);
		waitObj.waitForElementVisible(forgotPassUsrNameReqValiMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(forgotPassUsrNameReqValiMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyforgotPassUsrNameMinValiMsg() {
		log.info(forgotPassUsrNameMinValiMsg);
		waitObj.waitForElementVisible(forgotPassUsrNameMinValiMsg);
		Assert.assertEquals(textBoxHelper.getText(forgotPassUsrNameMinValiMsg), ObjectRepo.reader.getUsernameMinValMsg());
	}

	public void verifyforgotPassUsrNameMinValiMsgColor() {
		log.info(forgotPassUsrNameMinValiMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(forgotPassUsrNameMinValiMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyforgotPassUsrNameMaxValiMsg() {
		log.info(forgotPassUsrNameMaxValiMsg);
		waitObj.waitForElementVisible(forgotPassUsrNameMaxValiMsg);
		Assert.assertEquals(textBoxHelper.getText(forgotPassUsrNameMaxValiMsg), ObjectRepo.reader.getUsernameMaxValMsg());
	}

	public void verifyforgotPassUsrNameMaxValiMsgColor() {
		log.info(forgotPassUsrNameMaxValiMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(forgotPassUsrNameMaxValiMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyForgotPassSubmitBtnLabel() {
		log.info(forgotPassSubmitBtn);
		waitObj.waitForElementVisible(forgotPassSubmitBtn);
		Assert.assertEquals(textBoxHelper.getText(forgotPassSubmitBtn), ObjectRepo.reader.getSubmitBtnLabl());
	}

	public void verifyForgotPassCancelBtnLabel() {
		log.info(forgotPassCancelBtn);
		waitObj.waitForElementVisible(forgotPassCancelBtn);
		Assert.assertEquals(textBoxHelper.getText(forgotPassCancelBtn), ObjectRepo.reader.getCancelBtn());
	}

	public void verifyForgotPassSubmitBtnColor() {
		log.info(forgotPassSubmitBtn);
		waitObj.waitForElementVisible(forgotPassSubmitBtn);
		Assert.assertEquals(btnHelper.BtnColor(forgotPassSubmitBtn), ObjectRepo.reader.getLightGreenColor());
	}

	public void verifyForgotPassCancelBtnColor() {
		log.info(forgotPassCancelBtn);
		waitObj.waitForElementVisible(forgotPassCancelBtn);
		Assert.assertEquals(btnHelper.BtnColor(forgotPassCancelBtn), ObjectRepo.reader.getDarkGreyColor());
	}

	public void verifyForgotPassSubmitBtnClick() {
		log.info(forgotPassSubmitBtn);
		waitObj.waitForElementVisible(forgotPassSubmitBtn);
		btnHelper.click(forgotPassSubmitBtn);
	}

	public void verifyForgotPassCancelBtnClick() throws Exception {
		log.info(forgotPassCancelBtn);
		waitObj.waitForElementVisible(forgotPassCancelBtn);
		new Helper().javascriptExecutorClick(forgotPassCancelBtn);
	}

	public void verifyHomeLabel() {
		log.info(homeLabel);
		waitObj.waitForElementVisible(homeLabel);
		Assert.assertEquals(textBoxHelper.getText(homeLabel), ObjectRepo.reader.getHomeLabel());
	}
}
