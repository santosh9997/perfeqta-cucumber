package com.cucumber.framework.helper.PageObject;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.CheckBox.CheckBoxOrRadioButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;


import cucumber.api.java.lu.a;
import gherkin.lexer.Th;

import com.cucumber.framework.helper.Sorting.SortingOnColumn;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;

public class EntityRecordsPageObject extends PageBase
{
	private WebDriver driver;
	public ButtonHelper btnHelper;
	public CheckBoxOrRadioButtonHelper checkBoxAndBtnHelper;
	public TextBoxHelper textBoxHelper;
	public DropDownHelper drpdwnHelper;
	private final Logger log = LoggerHelper.getLogger(EntityRecordsPageObject.class);
	private SortingOnColumn sortColumn;
	private Search search;
	private Scroll scroll;
	private CommonFunctionPageObject commonFnPageObject;
	//private WaitHelper //waitObj;

	public EntityRecordsPageObject(WebDriver driver)
	{
		super(driver);
		this.driver = driver;
		sortColumn = new SortingOnColumn();
		search = new Search();
		scroll = new Scroll();
		//waitObj = new WaitHelper(driver, ObjectRepo.reader);
		btnHelper = new ButtonHelper(driver);
		checkBoxAndBtnHelper = new CheckBoxOrRadioButtonHelper(driver);
		textBoxHelper = new TextBoxHelper(driver);
		drpdwnHelper = new DropDownHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ng-click=\"vm.go('secure.entityrecords.list',vm.allPer.entityRecords)\"]")
	public WebElement entityRecdsTile;

	@FindBy(how = How.XPATH, using = "//SPAN[@class='currentStep ng-binding'][text()='Entity Records']")
	public WebElement entityRecdsLabel;	

	@FindBy(how = How.XPATH, using = "//A[@href=''][text()='Home']")
	public WebElement entityBreadCrum;

	@FindBy(how = How.XPATH, using = "//SPAN[@ng-bind-html='step.ncyBreadcrumbLabel']")
	public WebElement breadCrum;

	@FindBy(how = How.XPATH, using = "(//A[@class='app-record-search-tab-normal ng-binding'])[2]")
	public WebElement entitySearchLabl;

	@FindBy(how = How.XPATH, using = "//*[@class='list']")
	public WebElement entityModule;

	public WebElement entityModuleChecked;

	@FindBy(how = How.XPATH, using = "//*[@class='ui-select-match']")
	public WebElement entitySearchBox;

	@FindBy(how = How.XPATH, using = "//*[@type='search']")
	public WebElement enterValue;

	@FindBy(how = How.XPATH, using = "//*[@class='ui-select-no-choice dropdown-menu']")
	public WebElement noEntityFound;

	@FindBy(how = How.XPATH, using = "//*[@title='Click to save your search']")
	public WebElement addToFavorite;

	@FindBy(how = How.XPATH, using = "//input[@name='searchNameTextbox']")
	public WebElement enterFavoriteName;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.saveData(favoriteSearchForm)']")
	public WebElement saveButton;

	@FindBy(how = How.XPATH, using = "(//*[@class='col-md-10 col-md-offset-1 alert alert-warning no-records ng-binding ng-scope'])[1]")
	public WebElement noRecordFoundAtFavorite;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='col-md-10 col-md-offset-1 alert alert-warning no-records ng-binding ng-scope'])[2]")
	public WebElement noRecordFoundAtEntityValue;

//	@FindBy(how = How.XPATH, using = "//*[@translate='text.FAVORITENAMEISREQUIRED']")
//	public WebElement favoriteNameMessage;
	
	@FindBy(how = How.XPATH, using = "//*[@class='err-message ng-scope']")
	public WebElement favoriteNameMessageValidation;
	
	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-success ng-binding']")
	public WebElement goButton;
	
	@FindBy(how = How.XPATH, using = "//*[@class='col-sm-11 ng-binding']")
	public WebElement addToFavoriteLabelPopup;
	
	@FindBy(how = How.XPATH, using = "//*[@class='col-sm-1']")
	public WebElement favoritePopupCrossButton;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='btn btn-danger11 ng-binding'])[1]")
	public WebElement favoritePopupCancelButton;
	
	@FindBy(how = How.XPATH, using = "//*[@class='form-group']/span[1]/input")
	public WebElement addToFavoriteMyFavRadioBtn;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.favoriteClicked(data)'])[1]")
	public WebElement addToFavoriteFirstData;
	
	@FindBy(how = How.XPATH, using = "(//*[@class='glyphicon glyphicon-remove critearia-01 ng-click-active'])[1]")
	public WebElement clickOnRemoveButtonOfFirstRecordInMyFavorite;
	
	@FindBy(how = How.XPATH, using = "//*[@style='display: block;']")
	public WebElement removePopupMyFavoriteLabel;
	
	@FindBy(how = How.XPATH, using = "//*[@class='confirm']")
	public WebElement removePopupMyFavoriteYesButton;
	
	@FindBy(how = How.XPATH, using = "//*[@class='cancel']")
	public WebElement removePopupMyFavoriteNoButton;
	
	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-danger11 ng-binding']")
	public WebElement clearAllButton;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if=\"vm.entityPermission.create\"]")
	public WebElement adBtnGrid;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.onDeleteAll()']")
	public WebElement delBtnGrid;
	
	@FindBy(how = How.XPATH, using = "//*[@name='attributeSelect0']")
	public WebElement filterByEnAtt;
	
	@FindBy(how = How.XPATH, using = "//*[@name='attributeTextbox0']")
	public WebElement filterByEnAttField;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='selectPage(page + 1, $event)']")
	public WebElement gridPaggingNextBtn;


	// -----------------------------------public methods ----------------------------------------------------------
	public void entityRecdsTileClick() throws Exception {
		//waitObj.//waitForElementVisible(entityRecdsTile);
		btnHelper.click(entityRecdsTile);
	}

	public void verifyEntityRecdsLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(entityRecdsLabel), ObjectRepo.reader.getEntityRecdsLabel());	
	}


	public void entityBrdCrumClick() throws InterruptedException {
		btnHelper.click(entityBreadCrum);
	}

	public void verifyBrdCrum() throws InterruptedException {
		Assert.assertEquals(textBoxHelper.getText(breadCrum), ObjectRepo.reader.getHomeLabel());
	}

	public void verifyRecdSerchLblEntitiy() throws InterruptedException {
		Assert.assertEquals(textBoxHelper.getText(entitySearchLabl), ObjectRepo.reader.getEntitySearchLabl());
	}

	public void verifyEntityRecordSearchColor() throws InterruptedException {
		Assert.assertEquals(btnHelper.FontColorCodeHex(entitySearchLabl), ObjectRepo.reader.getBrightBlueColor());
	}

	public void verifyEntityAllModuleNameforEntityRecordsModule() throws InterruptedException {
		Assert.assertEquals(textBoxHelper.getText(entityModule), ObjectRepo.reader.getEntityModule());
	}

	public void verifyEntityAllModuleCheckBoxforEntityRecordsModule() throws InterruptedException {
		int selectedModule = 0;

		List<WebElement> count = driver.findElements(By.xpath("//*[@ng-repeat='module in vm.filteredModules']"));


		for (int i = 0; i < count.size(); i++) {
			entityModuleChecked = ObjectRepo.driver.findElement(By.xpath("//*[@id='module" + i + "']"));
			
			System.err.println("aaaaaaaaaaaaaaa: "+entityModuleChecked);
			if (checkBoxAndBtnHelper.isIselected(entityModuleChecked)) {
				selectedModule++;
			}
		}
		Assert.assertEquals(selectedModule, count.size());
	}

	public void uncheckCheckedModule() throws InterruptedException {

		List<WebElement> count = driver.findElements(By.xpath("//*[@ng-repeat='module in vm.filteredModules']"));


		for (int i = 0; i < count.size() ; i++ ) {
			entityModuleChecked = ObjectRepo.driver.findElement(By.xpath("//*[@id='module" + i + "']"));

			JavascriptExecutor js = (JavascriptExecutor)driver;		
			js.executeScript("arguments[0].click();", entityModuleChecked);

		}
	}
	public void verifyEntityAllModuleUnCheckBoxforEntityRecordsModule() throws InterruptedException {
		int selectedModule = 0;

		List<WebElement> count = driver.findElements(By.xpath("//*[@ng-repeat='module in vm.filteredModules']"));


		for (int i = 0; i < count.size(); i++) {
			entityModuleChecked = ObjectRepo.driver.findElement(By.xpath("//*[@id='module" + i + "']"));

			if (checkBoxAndBtnHelper.isIselected(entityModuleChecked)) {

			}
			else {
				selectedModule++;
			}
		}
		Assert.assertEquals(selectedModule, count.size());
	}
	public void checkUnCheckedModule() throws InterruptedException {

		List<WebElement> count = driver.findElements(By.xpath("//*[@ng-repeat='module in vm.filteredModules']"));


		for (int i = 0; i < count.size() ; i++ ) {
			entityModuleChecked = ObjectRepo.driver.findElement(By.xpath("//*[@id='module" + i + "']"));

			JavascriptExecutor js = (JavascriptExecutor)driver;		
			js.executeScript("arguments[0].click();", entityModuleChecked);


		}
	}

	public void clickSearchBoxEntity() {
		btnHelper.click(entitySearchBox);

	}

	public void enterWrongEntityName(String entityRecordValue) throws Exception {
		textBoxHelper.sendKeys(enterValue,entityRecordValue);
		enterValue.sendKeys(Keys.TAB);
	}

	public void verifyEntityNoModuleFoundEntityRecordSearch() throws InterruptedException {
		Assert.assertEquals(textBoxHelper.getText(noEntityFound).trim(),ObjectRepo.reader.getNoEntityFound());
	}

	public void enterEntityNameWhichHaveNoRecords(String entityRecordValue) throws Exception {
		textBoxHelper.sendKeys(enterValue,entityRecordValue);
		enterValue.sendKeys(Keys.TAB);
	}

	public void clickAddToFavorite() throws Exception {
		Thread.sleep(1000);
		btnHelper.click(addToFavorite);
	}

	public void enterFavoriteName(String favoriteName) throws Exception {
		textBoxHelper.sendKeys(enterFavoriteName,favoriteName);
	}

	public void clickOnSave() throws Exception {
		Thread.sleep(1000);
		btnHelper.click(saveButton);
	}

	public void verifyNoRecords() throws InterruptedException {
		Assert.assertEquals(textBoxHelper.getText(noRecordFoundAtEntityValue).trim(),ObjectRepo.reader.getNoRecordFoundMsg());
	}

	public void verifyFavoriteNameReuired() throws InterruptedException {
		Assert.assertEquals(textBoxHelper.getText(favoriteNameMessageValidation), ObjectRepo.reader.getFavNameRequireValMsg());
	}
	
	public void verifyFavoriteNameMinimumSize() throws InterruptedException {
		Assert.assertEquals(textBoxHelper.getText(favoriteNameMessageValidation), ObjectRepo.reader.getFavNameMinValMsg());
	}
	
	public void verifyFavoriteErrorColor() throws InterruptedException {
		Assert.assertEquals(btnHelper.FontColorCodeHex(favoriteNameMessageValidation), ObjectRepo.reader.getValMsgColor());
	}
	
	public void verifyFavoriteNameMaximumSize() throws InterruptedException {
		Assert.assertEquals(textBoxHelper.getText(favoriteNameMessageValidation), ObjectRepo.reader.getFavNameMaxValMsg());
	}
	
	public void verifyNoValidationMessage() throws InterruptedException {
		boolean messageValidaton = driver.findElements(By.xpath("//*[@class='err-message ng-scope']")).size()!=0;
		String actual;
		if(messageValidaton == false) {
			 actual = "passed";
		}
		else {
			actual = "Failed";
		}
		Assert.assertEquals(actual, "passed");
	}
	
	public void clickOnGo() throws Exception {
		//waitObj.//waitForElementVisible(goButton);
		Thread.sleep(1000);;
		btnHelper.click(goButton);
		//waitObj.//waitForElementVisible(adBtnGrid);
	}
	
	public void verifyNoRecordsInMyFavorite() throws InterruptedException {
		Assert.assertEquals(textBoxHelper.getText(noRecordFoundAtFavorite), " " + ObjectRepo.reader.getNoRecordFoundMsg());
	}
	
	public void verifyLabelAddToFavoriteOnPopup() throws InterruptedException {
		Assert.assertEquals(textBoxHelper.getText(addToFavoriteLabelPopup), ObjectRepo.reader.getAddToFavoriteLabel());
	}
	
	public void clickOnCrossButtonOnAddToFavoritePopup() throws Exception {
		btnHelper.click(favoritePopupCrossButton);
	}
	
	public void verifyPopupToEntityRecordScreen() throws InterruptedException {
		boolean IspopupDisplay = driver.findElements(By.xpath("//*[@class='err-message ng-scope']")).size()!=0;
		String actual;
		if(IspopupDisplay == false) {
			 actual = "passed";
		}
		else {
			actual = "Failed";
		}
		Assert.assertEquals(actual, "passed");
	}
	
	public void clickOnCancleButtonOnAddToFavoritePopup() throws Exception {
		btnHelper.click(favoritePopupCancelButton);
	}
	
	public void verifyAddToFavoriteDefalutMyFavRadioSelect() throws Exception {
		String actual;
		boolean radioBtnIsSelected = checkBoxAndBtnHelper.isIselected(addToFavoriteMyFavRadioBtn);
		if(radioBtnIsSelected == true) {
			 actual = "passed";
		}
		else {
			actual = "Failed";
		}
		Assert.assertEquals(actual, "passed");	
	}
	
	public void verifyAddFavoriteSaveButtonColor() throws InterruptedException {
		Assert.assertEquals(btnHelper.BtnColor(saveButton), ObjectRepo.reader.getLightGreenColor());
	}
	
	public void verifyAddFavoriteCancelButtonColor() throws InterruptedException {
		Assert.assertEquals(btnHelper.BtnColor(favoritePopupCancelButton), ObjectRepo.reader.getDarkGreyColor());
	}
	
	public void verifyGoButtonColor() throws InterruptedException {
		Assert.assertEquals(btnHelper.BtnColor(goButton), ObjectRepo.reader.getLightGreenColor());
	}
	
	public void verifyAddToFavoriteSaveButton() throws IOException {
		String Input = ExcelUtils.readXLSFile("Entity Records", 2, 2);
		Assert.assertEquals(textBoxHelper.getText(addToFavoriteFirstData), Input);
	}
	
	public void verifyFavoriteNameMustBeUnique() throws InterruptedException {
		Assert.assertEquals(textBoxHelper.getText(favoriteNameMessageValidation), ObjectRepo.reader.getFavNameUniqueValMsg());
	}

	public void clickOnRemoveButtonOfFirstRecordInMyFavorite() throws Exception {
		btnHelper.click(clickOnRemoveButtonOfFirstRecordInMyFavorite);
	}
	
	public void verifyRemovePopupMyFavoriteLabel() throws InterruptedException {
		Assert.assertEquals(textBoxHelper.getText(removePopupMyFavoriteLabel), ObjectRepo.reader.getRemovePopupMyFavoriteLabel());
	}
	
	public void verifyConfirmationOfDeleteMyFavoriteYesButtonColor() throws InterruptedException {
		Assert.assertEquals(btnHelper.BtnColor(removePopupMyFavoriteYesButton), ObjectRepo.reader.getLightGreenColor());
	}
	
	public void verifyConfirmationOfDeleteMyFavoriteNoButtonColor() throws InterruptedException {
		Assert.assertEquals(btnHelper.BtnColor(removePopupMyFavoriteNoButton), ObjectRepo.reader.getRemoveMsgColor());
	}
	
	public void verifyConfirmationOfDeleteMyFavoriteNoButtonClick() throws InterruptedException {
		btnHelper.click(removePopupMyFavoriteNoButton);
	}
	
	public void verifyConfirmationOfDeleteMyFavoriteNoButtonFunctionality() throws Exception {
		boolean isPopupDisplay = driver.findElements(By.xpath("//*[@class='sweet-alert showSweetAlert visible']")).size() !=0;
		Assert.assertEquals(isPopupDisplay , false );	
	}
	
	public void verifyConfirmationOfDeleteMyFavoriteYesButtonClick() throws InterruptedException {
		//waitObj.//waitForElementVisible(removePopupMyFavoriteYesButton);
		Thread.sleep(500);
		btnHelper.click(removePopupMyFavoriteYesButton);
	}
	
	public void verifyClearAllButtonColor() throws InterruptedException {
		Assert.assertEquals(btnHelper.BtnColor(clearAllButton), ObjectRepo.reader.getDarkGreyColor());
	}
	
	public void verifyAddToFavoriteColor() throws InterruptedException {
		Assert.assertEquals(btnHelper.BtnColor(addToFavorite), ObjectRepo.reader.getBlueColor());
	}
	
	public void clkClrAllBut() throws InterruptedException {
		btnHelper.click(clearAllButton);
	}
	
	public void clrAllBtnFun() throws InterruptedException {

		int selectedModule = 0;

		List<WebElement> count = driver.findElements(By.xpath("//*[@ng-repeat='module in vm.filteredModules']"));
		String checkbox,actual;

		for (int i = 0; i < count.size(); i++) {
			entityModuleChecked = ObjectRepo.driver.findElement(By.xpath("//*[@id='module" + i + "']"));

			if (checkBoxAndBtnHelper.isIselected(entityModuleChecked)) {

			}
			else {
				selectedModule++;
			}
		}
		if(selectedModule==count.size()) {
			checkbox = "passed";
		}
		else {
			checkbox = "failed";
		}
		
		boolean grid1abel = driver.findElements(By.xpath("//*[@class='entity-values-lable ng-binding']")).size()!=0;
		//entitySearchBox.click();
		WebElement entitySearchbox = driver.findElement(By.xpath("//*[@ng-hide='$select.isEmpty()']"));
		String entityName = textBoxHelper.getText(entitySearchbox);
		if(checkbox.equals("passed") && grid1abel==false && entityName.equals("")) {
			actual = "cleared";
		}
		else {
			actual = "Unclered";
		}
		Assert.assertEquals(actual, "cleared");
	}
	
	public void enterEntityName() throws Exception {
		//Thread.sleep(2000);
		textBoxHelper.sendKeys(enterValue,"Reagent Master");
		enterValue.sendKeys(Keys.TAB);
	}
	
	public void verifyGrdAddBtnColor() throws InterruptedException {
		Assert.assertEquals(btnHelper.BtnColor(adBtnGrid), ObjectRepo.reader.getBlueColor());
	}
	
	public void verifyGrdDelBtnColor() throws InterruptedException {
		Assert.assertEquals(btnHelper.BtnColor(delBtnGrid), ObjectRepo.reader.getRemoveMsgColor());
	}
	
	public void clkFilByDrdwn() throws InterruptedException {
		btnHelper.click(filterByEnAtt);
	}
	
	public void selectkFilByDrdwn(String attribute) throws InterruptedException {
		
//		Select Drpdwn = new Select(filterByEnAtt);
//		Drpdwn.selectByVisibleText("Equipment type");
		drpdwnHelper.selectDropDownText(filterByEnAtt, attribute);
	}
	
	public void enterEntityNaAttText(String attributeNameText) throws Exception {
		textBoxHelper.sendKeys(enterValue,attributeNameText);
		enterValue.sendKeys(Keys.TAB);
	}
	
	public void selectkFilByDrdwnData() throws InterruptedException {
		btnHelper.click(filterByEnAtt);
	}
	
	public void enterEntityNaAttData(String attData) throws Exception {
		//Thread.sleep(2000);
		textBoxHelper.sendKeys(filterByEnAttField,attData);
		filterByEnAttField.sendKeys(Keys.TAB);
	}
	
	public void verifyEntityAttSrchFunclity(String attData) throws Exception {
		
		int i = 0, count = 0, add = 0 ;
		String nextBtnColor, selectAttributeValue = attData;
		 
		List<org.openqa.selenium.WebElement> inList = new ArrayList<WebElement>();
		List<String> temp = new ArrayList<String>();
		
		
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			System.out.println("found more than 10 records");

			for (i = 0; i < 100; i++) {

				// this line is storing a hex code of next button.
				nextBtnColor = btnHelper.FontColorCodeHex(gridPaggingNextBtn);

				// this line is taking a "Created Date column" all value.
				inList = (ArrayList<org.openqa.selenium.WebElement>) driver
						.findElements(By.xpath("//*[@data-th='" + selectAttributeValue + "']"));

				try {

					for (add = 0; add < inList.size(); add++) {
						temp.add(count, inList.get(add).getText());
						count++;
					}

					System.out.println(i + "-grid list: " + temp.size());
					System.out.println("--------------------------------------");
					System.out.println(Arrays.deepToString(temp.toArray()));
					System.out.println("--------------------------------------");

				} catch (Exception e) {
					System.out.println(e);
				}
				btnHelper.click(gridPaggingNextBtn);
				Thread.sleep(3000);

				if (nextBtnColor.equalsIgnoreCase("#777777")) {
					break;
				}
			}
		} else {
			inList = (ArrayList<org.openqa.selenium.WebElement>) driver
					.findElements(By.xpath("//*[@data-th='" + selectAttributeValue + " :']"));

			for (add = 0; add < inList.size(); add++) {
				temp.add(count, inList.get(add).getText());
				count++;

				System.out.println(i + "-grid list: " + temp.size());
				System.out.println("--------------------------------------");
				System.out.println(Arrays.deepToString(temp.toArray()));
				System.out.println("--------------------------------------");
			}
		}

		String[] outArray = new String[temp.size()]; // instantiate Array

		int actualMatchedRecords = 0;
		int expectedMatchedRecords = temp.size();

		for (int j = 0; j < temp.size(); j++) {
			outArray[j] = temp.get(j);

			if (outArray[0].equalsIgnoreCase(selectAttributeValue)) {
				actualMatchedRecords++;
				System.out.println("--------------------------------");
				System.out.println("Record Matched                |");
				System.out.println("actual: " + outArray[0] + "\nexpected: " + selectAttributeValue);

			} else {
				System.out.println("--------------------------------");
				System.out.println("Record Does't Matched          |");
				System.out.println("actual: " + outArray[0] + "\nexpected: " + selectAttributeValue);

			}
			System.out.println("--------------------------------");

		}
		System.out.println("Actual no of record is: " + actualMatchedRecords);
		System.out.println("Total no of record is: " + expectedMatchedRecords);

		
	}
	
	public void verifysearch(String searchedItem) throws Exception {

		AttributesPageObject obj = new AttributesPageObject(ObjectRepo.driver);

		String PaginationVal = null, PaginationSelectedVal = null, srchResultNo = null;
		try {
			PaginationVal = textBoxHelper.getText(obj.paginationText);
			srchResultNo = textBoxHelper.getText(obj.srcResultPageNo);
			PaginationSelectedVal = obj.pageSizeDrp_GetSelectedValue();
			new Search().SearchVerification(PaginationVal, PaginationSelectedVal, obj.grid_data, searchedItem,obj.textSearch, srchResultNo);
		} catch (Exception e) {
			System.err.println(e);
		}

	}
}
