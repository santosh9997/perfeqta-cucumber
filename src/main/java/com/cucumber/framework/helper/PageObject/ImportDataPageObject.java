package com.cucumber.framework.helper.PageObject;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.CheckBox.CheckBoxOrRadioButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.lu.a;
import gherkin.lexer.Th;

import com.cucumber.framework.helper.Sorting.SortingOnColumn;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;

public class ImportDataPageObject extends PageBase
{
	private WebDriver driver;
	public ButtonHelper btnHelper;
	public CheckBoxOrRadioButtonHelper checkBoxAndBtnHelper;
	public TextBoxHelper textBoxHelper;
	public DropDownHelper drpdwnHelper;
	private final Logger log = LoggerHelper.getLogger(ImportDataPageObject.class);
	private SortingOnColumn sortColumn;
	private Search search;
	private Scroll scroll;
	private CommonFunctionPageObject commonFnPageObject;
	//private WaitHelper waitObj;

	public ImportDataPageObject(WebDriver driver)
	{
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		checkBoxAndBtnHelper = new CheckBoxOrRadioButtonHelper(driver);
		textBoxHelper = new TextBoxHelper(driver);
		drpdwnHelper = new DropDownHelper(driver);
		//waitObj = new WaitHelper(driver, ObjectRepo.reader);
		sortColumn = new SortingOnColumn();
		search = new Search();
		scroll = new Scroll();
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ng-click=\"vm.go('secure.importdata.home',vm.allPer.importdata)\"]")
	public WebElement importDataRecdsTile;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement importDataRecdsLbl;

	@FindBy(how = How.XPATH, using = "//*[@class='crumb breadcrumb gray ng-isolate-scope']")
	public WebElement breadcrumImportData;

	@FindBy(how = How.LINK_TEXT, using = "Manual Import")
	public WebElement manualImport;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement manualImportLabel;

	@FindBy(how = How.XPATH, using = "//*[@class='crumb breadcrumb gray ng-isolate-scope']")
	public WebElement breadcrumManImp;

	@FindBy(how = How.XPATH, using = "//*[@class='col-md-4 col-sm-12 col-xs-12']")
	public WebElement uploadCsvFileLabel;

	@FindBy(how = How.XPATH, using = "//*[@name='qcModuleSelect']")
	public WebElement moduleDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@translate='import.modr']")
	public WebElement moduleDrpDwnValMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='qcTypeSelect']")
	public WebElement typeDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@class='help-block ng-binding ng-scope']")
	public WebElement typeDrpDwnValMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='mappingNameTextBox']")
	public WebElement mappingName;

	@FindBy(how = How.XPATH, using = "//*[@translate='manual.mapr']")
	public WebElement mappingNameValMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='manual.mapu']")
	public WebElement mappingNameValMsgForDup;

	@FindBy(how = How.XPATH, using = "//*[@translate='manual.map2']")
	public WebElement mappingNameValMsgForMinSize;

	@FindBy(how = How.XPATH, using = "//*[@translate='manual.map50']")
	public WebElement mappingNameValMsgForMaxSize;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-success ng-binding']")
	public WebElement uploadCsvBtn;

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.importdata.home']")
	public WebElement cancelManUpBtn;

	@FindBy(how = How.LINK_TEXT, using = "View Saved Mapping")
	public WebElement viewSavedMapping;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement viewSavedMappingLabel;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement paginationText;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.options.data && vm.options.data.length']")
	public WebElement grid;

	@FindBy(how = How.XPATH, using = "//*[@ng-change='vm.onPageNumberChange()']")
	public WebElement pageSizeDrp;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.textSearch']")
	public WebElement viewMapSrchBox;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.go(field,row,vm.resource);'])[2]")
	public WebElement viewAuditLink;
	

	// -----------------------------------public methods ----------------------------------------------------------
	public void importDataRecdsTileClick() throws Exception {
		btnHelper.click(importDataRecdsTile);
	}

	public void verifyImportDaLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(importDataRecdsLbl), ObjectRepo.reader.getImportDataRecdsLbl());
	}

	public void verifyImportDaBreadCrum() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(breadcrumImportData), ObjectRepo.reader.getBreadcrumImportData());
	}

	public void manualImpoDataRecdsTileClick() throws Exception {
		btnHelper.click(manualImport);
	}

	public void verifyManualImLbl() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(manualImportLabel), ObjectRepo.reader.getManualImportLabel());
	}

	public void verifyManImpBreadCrum() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(breadcrumManImp), ObjectRepo.reader.getBreadcrumManImp());
	}

	public void verifyuploadCsFilebl() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(uploadCsvFileLabel), ObjectRepo.reader.getUploadCsvFileLabel());
	}

	public void selectDrpDwnModule() throws Exception {
		drpdwnHelper.SelectUsingIndex(moduleDrpDwn, 0);
	}

	public void verifyModuleDrpDwnValMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(moduleDrpDwnValMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void selectDrpDwnType() throws Exception {
		drpdwnHelper.SelectUsingIndex(typeDrpDwn, 0);
	}

	public void verifyTypeDrpDwnValMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(typeDrpDwnValMsg), "Type is required.");
	}

	public void verifyTypeDrpDwnValMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(typeDrpDwnValMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void mappingNameClick() throws Exception {
		btnHelper.click(mappingName);
	}

	public void verifyMappingNameValMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(mappingNameValMsg), ObjectRepo.reader.getMappingNameValMsg());
	}

	public void verifyMappingNameValMsgColor() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(mappingNameValMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterDupMapName(String mappingNameValue) throws Exception {
		textBoxHelper.sendKeys(mappingName,mappingNameValue);
	}

	public void verifyMappingNameValMsgForDup() throws Exception {
		Thread.sleep(1000);
		Assert.assertEquals(textBoxHelper.getText(mappingNameValMsgForDup), ObjectRepo.reader.getMappingNameValMsgForDup());
	}

	public void verifyMappingNameValMsgColorForDup() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(mappingNameValMsgForDup), ObjectRepo.reader.getValMsgColor());
	}

	public void enterMinSizeMapName(String mappingNameValue) throws Exception {
		textBoxHelper.sendKeys(mappingName,mappingNameValue);
	}

	public void verifyMappingNameValMsgForMinSize() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(mappingNameValMsgForMinSize), ObjectRepo.reader.getMappingNameValMsgForMinSize());
	}

	public void verifyMappingNameValMsgColorForMinSize() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(mappingNameValMsgForMinSize), ObjectRepo.reader.getValMsgColor());
	}

	public void enterMaxSizeMapName(String mappingNameValue) throws Exception {
		textBoxHelper.sendKeys(mappingName,mappingNameValue);
	}

	public void verifyMappingNameValMsgForMaxSize() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(mappingNameValMsgForMaxSize), ObjectRepo.reader.getMappingNameValMsgForMaxSize());
	}

	public void verifyMappingNameValMsgColorForMaxSize() throws Exception {
		Assert.assertEquals(btnHelper.FontColorCodeHex(mappingNameValMsgForMaxSize), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyUploadCsvBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(uploadCsvBtn), ObjectRepo.reader.getLightGreenColor());
	}

	public void verifyCancelManImBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(cancelManUpBtn), ObjectRepo.reader.getDarkGreyColor());
	}

	public void clickManImCancelBtn() throws Exception {
		btnHelper.click(cancelManUpBtn);
	}

	public void viewSavedMapRecdsTileClick() throws Exception {
		viewSavedMapping.click();
	}
	
	public void clickViewAuditLink() throws Exception {
		btnHelper.click(viewAuditLink);
	}

	public void verifyViewSavMaLbl() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(viewSavedMappingLabel),ObjectRepo.reader.getViewSavedMappingLabel());
	}

	public void viewMapSrchBoxSendkeys(String SrchValue) {
		btnHelper.click(viewMapSrchBox);
		textBoxHelper.sendKeys(viewMapSrchBox,SrchValue);
	}

	public void verifysearch(String searchedItem) throws Exception {

		AttributesPageObject obj = new AttributesPageObject(ObjectRepo.driver);

		String PaginationVal = null, PaginationSelectedVal = null, srchResultNo = null;
		try {
			PaginationVal = textBoxHelper.getText(obj.paginationText);
			srchResultNo =textBoxHelper.getText(obj.srcResultPageNo);
			PaginationSelectedVal = obj.pageSizeDrp_GetSelectedValue();
			new Search().SearchVerification(PaginationVal, PaginationSelectedVal, obj.grid_data, searchedItem,obj.textSearch, srchResultNo);
		} catch (Exception e) {
			System.err.println(e);
		}

	}

}
