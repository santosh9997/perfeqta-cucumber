package com.cucumber.framework.helper.PageObject;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;

import gherkin.lexer.Th;

public class SmartAlertsPageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(SmartAlertsPageObject.class);
	private WaitHelper waitObj;
	private TextBoxHelper textBoxHelper;

	public SmartAlertsPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		waitObj = new WaitHelper(driver, ObjectRepo.reader);
		textBoxHelper = new TextBoxHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ng-click=\"vm.go('secure.threshold.list',vm.allPer.thresholds)\"]")
	public WebElement smartAlertsTile;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement smartAlertsLableName;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-primary add-new ng-binding']")
	public WebElement addNewBtn;

	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[3]/span")
	public WebElement addEditSmartAlrtLabel;

	@FindBy(how = How.XPATH, using = "//*[@class='pull-right smart-alert-current-vertion ng-binding']")
	public WebElement CurrVersion;

	@FindBy(how = How.XPATH, using = "//*[@name='thresholdname']")
	public WebElement alertNameTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@name='thresholddescription']")
	public WebElement alertDescpTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.threshold.edit.SMARTR' ]")
	public WebElement alertNameTxtBoxRquiMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.threshold.edit.SMART2' ]")
	public WebElement alertNameTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.threshold.edit.SMART50' ]")
	public WebElement alertNameTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.threshold.edit.SMARTU' ]")
	public WebElement alertNameTxtBoxUniqMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.threshold.edit.SAD2' ]")
	public WebElement alertDescpTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.threshold.edit.SAD800' ]")
	public WebElement alertDescpTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='messageinstruction']")
	public WebElement msgAndInstToEmailTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.threshold.edit.MAITSIMR' ]")
	public WebElement msgAndInstToEmailTxtBoxRquiMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.threshold.edit.MAITSIM2' ]")
	public WebElement msgAndInstToEmailTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.threshold.edit.MAITSIM800' ]")
	public WebElement msgAndInstToEmailTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='isSendEmailTextBox']")
	public WebElement emailAddTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.threshold.edit.EMAILONE' ]")
	public WebElement emailAddTxtBoxReqMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.EMAILADDI' ]")
	public WebElement emailAddTxtBoxInvalidMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='qcModuleSelect']")
	public WebElement mdulDrpdwn;

	@FindBy(how = How.XPATH, using = "//*[@translate='import.modr' ]")
	public WebElement mdulDrpdwnRqiMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-class='settings.buttonClasses']")
	public WebElement appDrpdwn;

	@FindBy(how = How.XPATH, using = "//*[@data-ng-click='selectAll()']")
	public WebElement appDrpdwnChckAllOpt;

	@FindBy(how = How.XPATH, using = "//*[@data-ng-click='deselectAll();']")
	public WebElement appDrpdwnUnchckAllOpt;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.ps2']")
	public WebElement appDrpdwnRqiMsg;

	@FindBy(how = How.XPATH, using = "//*[@value='Daily' ]")
	public WebElement dailyRdoBtn;

	@FindBy(how = How.XPATH, using = "//*[@value='Weekly' ]")
	public WebElement weeklyRdoBtn;

	@FindBy(how = How.XPATH, using = "//*[@value='Monthly' ]")
	public WebElement monthlyRdoBtn;

	@FindBy(how = How.XPATH, using = "//*[@name='dailyTextBox']")
	public WebElement dailyTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@name='weeklyTextBox']")
	public WebElement weeklyTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.importdata.automated.Everydayr' ]")
	public WebElement dailyTxtBoxRqiMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schedule.edit.INVALIDNUMBERCANNOTADDMORETHAN31DAYS' ]") 
	public WebElement dailyTxtBoxLimitMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.importdata.automated.Invalidn' ]")
	public WebElement dailyTxtBoxInvalidMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.importdata.automated.Everyweekr' ]")
	public WebElement weeklyTxtBoxRqiMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.threshold.edit.INVALID10' ]")
	public WebElement weeklyTxtBoxLimitMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.importdata.automated.Invalidn' ]")
	public WebElement weeklyTxtBoxInvalidMsg;

	// --------------------public methods--------------------

	public WebDriver getDriver() {
		return this.driver;

	}

	public void smartAlertsTileClick() throws Exception {
		Thread.sleep(2000);
		btnHelper.click(smartAlertsTile);
	}

	public void verifySmartAlertsLableName() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(smartAlertsLableName), ObjectRepo.reader.getSmartAlertsLableName());
	}

	public void addNewBtnClick() throws Exception {
		btnHelper.click(addNewBtn);
	}

	public void verifyAddEditSmartAlrtLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(addEditSmartAlrtLabel), ObjectRepo.reader.getAddEditSmartAlrtLabel());
	}

	public void verifyCurrVersion() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(CurrVersion), ObjectRepo.reader.getCurrVersion());
	}

	public void alertNameTxtBoxSendkeys(String inputValue) throws Exception {
		textBoxHelper.sendKeys(alertNameTxtBox, inputValue);
	}

	public void alertDescpTxtBoxSendkeys(String inputValue) throws Exception {
		textBoxHelper.sendKeys(alertDescpTxtBox, inputValue);
	}

	public void msgAndInstToEmailTxtBoxSendkeys(String inputValue) throws Exception {
		textBoxHelper.sendKeys(msgAndInstToEmailTxtBox, inputValue);
	}

	public void emailAddTxtBoxSendkeys(String inputValue) throws Exception {
		textBoxHelper.sendKeys(emailAddTxtBox, inputValue);
	}

	public void verifyAlertNameTxtBoxRquiMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(alertNameTxtBoxRquiMsg), ObjectRepo.reader.getAlertNameTxtBoxRquiMsg());
	}

	public void verifyAlertNameTxtBoxMinMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(alertNameTxtBoxMinMsg), ObjectRepo.reader.getAlertNameTxtBoxMinMsg());
	}

	public void verifyAlertNameTxtBoxMaxMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(alertNameTxtBoxMaxMsg), ObjectRepo.reader.getAlertNameTxtBoxMaxMsg());
	}

	public void verifyAlertNameTxtBoxUniqMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(alertNameTxtBoxUniqMsg), ObjectRepo.reader.getAlertNameTxtBoxUniqMsg());
	}

	public void verifyAlertDescpTxtBoxMinMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(alertDescpTxtBoxMinMsg), ObjectRepo.reader.getAlertDescpTxtBoxMinMsg());
	}

	public void verifyAlertDescpTxtBoxMaxMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(alertDescpTxtBoxMaxMsg), ObjectRepo.reader.getAlertDescpTxtBoxMaxMsg());
	}

	public void verifyAlertDescpTxtBoxNoValid() {

		boolean minMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.threshold.edit.SAD2' ]"))
				.size() == 0,
				maxMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.threshold.edit.SAD800' ]"))
						.size() == 0,
				flag;

		if (minMsg && maxMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void verifyMsgAndInstToEmailTxtBoxRquiMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(msgAndInstToEmailTxtBoxRquiMsg),ObjectRepo.reader.getMsgAndInstToEmailTxtBoxRquiMsg());
	}

	public void verifyMsgAndInstToEmailTxtBoxMinMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(msgAndInstToEmailTxtBoxMinMsg),ObjectRepo.reader.getMsgAndInstToEmailTxtBoxMinMsg());
	}

	public void verifyMsgAndInstToEmailTxtBoxMaxMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(msgAndInstToEmailTxtBoxMaxMsg),ObjectRepo.reader.getMsgAndInstToEmailTxtBoxMaxMsg());
	}

	public void verifyMsgAndInstToEmailTxtNoValidMsg() {

		boolean reqMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.threshold.edit.MAITSIMR' ]"))
				.size() == 0,

				minMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.threshold.edit.MAITSIM2' ]"))
						.size() == 0,
				maxMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.threshold.edit.MAITSIM800' ]"))
						.size() == 0,
				flag;

		if (reqMsg && minMsg && maxMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void verifyEmailAddTxtBoxReqMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(emailAddTxtBoxReqMsg), ObjectRepo.reader.getEmailAddTxtBoxReqMsg());
	}

	public void verifyEmailAddTxtBoxInvalidMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(emailAddTxtBoxInvalidMsg), ObjectRepo.reader.getInvalidEmailValMsg());
	}

	public void selectMdulDrpDwn(String inputValue) {
		Select select = new Select(mdulDrpdwn);
		select.selectByVisibleText(inputValue);
	}
//pending
	public void verifySelectMdulDrpDwn(String inputValue) {
		Select select = new Select(mdulDrpdwn);
		select.selectByVisibleText(inputValue);

		Assert.assertEquals(select.getFirstSelectedOption().getText(), inputValue);
	}

	public void verifyMdulDrpDwnReqMsg() {
		Select select = new Select(mdulDrpdwn);
		select.selectByIndex(0);
		Assert.assertEquals(textBoxHelper.getText(mdulDrpdwnRqiMsg), ObjectRepo.reader.getModuleDrpDwnRequiredMsg());
	}

	public void appDrpdwnClick() throws InterruptedException {
		btnHelper.click(appDrpdwn);
	}

	public void appDrpdwnChckAllOptClick() {
		btnHelper.click(appDrpdwnChckAllOpt);
	}

	public void appDrpdwnUnchckAllOptClick() {
		btnHelper.click(appDrpdwnUnchckAllOpt);
	}

	public void verifyAppDrpDwnReqMsg() {

		Assert.assertEquals(textBoxHelper.getText(appDrpdwnRqiMsg), ObjectRepo.reader.getAppRequireValMsg());
	}

	public void verifyAppDrpdwnChckAllOpt() {

		int count = 6, CheckedValue = 0;

		List<WebElement> arry = ObjectRepo.driver.findElements(
				By.xpath("//*[@ng-click='checkboxClick($event, getPropertyForObject(option,settings.idProp))']"));

		for (int i = 0; i < arry.size(); i++) {

			if (ObjectRepo.driver
					.findElement(By.xpath(
							"//*[@class='dropdown-menu dropdown-menu-form']/li[" + count + "]/a/div/label/input"))
					.isSelected())

				CheckedValue++;
		}
		count++;

		Assert.assertEquals(CheckedValue, arry.size());
	}

	public void verifyAppDrpdwnUnchckAllOpt() {

		int count = 6, CheckedValue = 0;

		List<WebElement> arry = ObjectRepo.driver.findElements(
				By.xpath("//*[@ng-click='checkboxClick($event, getPropertyForObject(option,settings.idProp))']"));

		for (int i = 0; i < arry.size(); i++) {

			if (ObjectRepo.driver
					.findElement(By.xpath(
							"//*[@class='dropdown-menu dropdown-menu-form']/li[" + count + "]/a/div/label/input"))
					.isSelected())

				CheckedValue++;
		}
		count++;

		Assert.assertEquals(CheckedValue, 0);
	}

	public void dailyRdoBtnClick() throws Exception {
		btnHelper.click(dailyRdoBtn);
	}

	public void weeklyRdoBtnClick() throws Exception {
		new Scroll().scrollDown(ObjectRepo.driver);
		btnHelper.click(weeklyRdoBtn);
	}

	public void monthlyRdoBtnClick() throws Exception {
		btnHelper.click(monthlyRdoBtn);
	}

	public void dailyTxtBoxSendkeys(String inputValue) throws Exception {
		textBoxHelper.sendKeys(dailyTxtBox, inputValue);
	}

	public void weeklyTxtBoxSendkeys(String inputValue) {
		textBoxHelper.sendKeys(weeklyTxtBox, inputValue);
	}

	public void verifyDailyTxtBoxReqMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(dailyTxtBoxRqiMsg), ObjectRepo.reader.getDailyTextboxValMsg());
	}

	public void verifyDailyTxtBoxLimitMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(dailyTxtBoxLimitMsg), ObjectRepo.reader.getDailyTxtBoxLimitMsg());
	}

	public void verifyDailyTxtBoxInvalidMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(dailyTxtBoxInvalidMsg), ObjectRepo.reader.getInvalidNumberValMsg());
	}

	public void verifyWeeklyTxtBoxReqMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(weeklyTxtBoxRqiMsg), ObjectRepo.reader.getWeeklyTxtBoxRqiMsg());
	}

	public void verifyWeeklyTxtBoxLimitMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(weeklyTxtBoxLimitMsg), ObjectRepo.reader.getWeeklyTxtBoxLimitMsg());
	}

	public void verifyWeeklyTxtBoxInvalidMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(weeklyTxtBoxInvalidMsg), ObjectRepo.reader.getInvalidNumberValMsg());
	}

	public void verifyDailyTxtBoxNoValidMsg() {

		boolean reqMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.importdata.automated.Everydayr' ]"))
				.size() == 0,

				limitMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.schedule.edit.INVALIDNUMBERCANNOTADDMORETHAN31DAYS' ]"))
						.size() == 0,
				invalidMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.importdata.automated.Invalidn' ]"))
						.size() == 0,
				flag;

		if (reqMsg && limitMsg && invalidMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}
	public void verifyWeeklyTxtBoxNoValidMsg() {

		boolean reqMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.importdata.automated.Everyweekr' ]"))
				.size() == 0,

				limitMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.threshold.edit.INVALID10' ]"))
						.size() == 0,
				invalidMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.importdata.automated.Invalidn' ]"))
						.size() == 0,
				flag;

		if (reqMsg && limitMsg && invalidMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}
	
}
