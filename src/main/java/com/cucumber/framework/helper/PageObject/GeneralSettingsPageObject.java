package com.cucumber.framework.helper.PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;




public class GeneralSettingsPageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(GeneralSettingsPageObject.class);
	private Scroll scroll;
	private WaitHelper waitObj;
	private TextBoxHelper textBoxHelper;
	public static boolean beforeClickCheckBox, afterClickCheckbox;

	public GeneralSettingsPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		scroll = new Scroll();
		waitObj = new WaitHelper(driver, ObjectRepo.reader);
		textBoxHelper = new TextBoxHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.entities.list']")
	public WebElement entitiesTile;

	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.admin.generalSettings.edit']")
	public WebElement generalSettingModuleName;

	@FindBy(how = How.XPATH, using = "//span[@class='currentStep ng-binding']")
	public WebElement generalSettingName;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"breadcrum-ipad-768\"]/li[2]/a")
	public WebElement ClickBread;
	
	
	@FindBy(how = How.XPATH, using = "//*[@class='crumb breadcrumb gray ng-isolate-scope']")
	public WebElement breadcrumb;
	
	@FindBy(how = How.XPATH, using = "//input[@ng-model=\"vm.generalSettings.siteTitle\"]")
	public WebElement clickRecordTitle;
	
	@FindBy(how = How.XPATH, using = "/html/body/div/div/ui-view/ui-view/fieldset/div/form/div/div[2]/div[1]/p")
	public WebElement validationRecordTitle;
	
	@FindBy(how = How.XPATH, using = "//fieldset/div/form/div/div[2]/div[1]/input")
	public WebElement minLengthvalidationMsg;
	
	@FindBy(how = How.XPATH, using = "//fieldset/div/form/div/div[2]/div[1]/p")
	public WebElement verifyMinLengthvalidationMsg;
	
	@FindBy(how = How.XPATH, using = "//ui-view/fieldset/div/form/div/div[2]/div[1]/p")
	public WebElement verifyMaxLengthvalidationMsg;
	
	@FindBy(how = How.XPATH, using = "//form/div/div[2]/div[2]/input")
	public WebElement stdReportsubTitlebox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[2]/div[2]/p")
	public WebElement stdReportsubTitleRequiredValidation;
	
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[2]/div[2]/p")
	public WebElement stdReportsubTitleMinValidation;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[2]/div[2]/p")
	public WebElement stdReportsubTitleMaxValidation;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[2]/div[4]/textarea")
	public WebElement clickContactdetails;
	
	@FindBy(how = How.XPATH, using = "//fieldset/div/form/div/div[2]/div[4]/p")
	public WebElement textareaMinValidation;
	
	@FindBy(how = How.XPATH, using = " //div/form/div/div[2]/div[4]/p")
	public WebElement textareaMaxValidation;
	
	@FindBy(how = How.XPATH, using = " //div/form/div/div[3]/div[1]/input")
	public WebElement passwordAgingBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[3]/div[1]/p")
	public WebElement verify0passwordAgingBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[3]/div[1]/p")
	public WebElement verify999passwordAgingBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[3]/div[2]/input")
	public WebElement PasswordAgingMsgbox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[3]/div[2]/p")
	public WebElement PasswordAgingMsgMinValidation;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[3]/div[2]/p")
	public WebElement PasswordAgingMsgMaxValidation;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[4]/div[1]/input")
	public WebElement PreviousUsedPasswordsbox;
	
	@FindBy(how = How.XPATH, using = "//fieldset/div/form/div/div[4]/div[1]/p")
	public WebElement enterZeroValidationPreviousUsedPasswordsBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[4]/div[1]/p")
	public WebElement lessthan4ValidationPreviousUsedPasswordsBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[4]/div[1]/p")
	public WebElement greaterthan99ValidationPreviousUsedPasswordsBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[4]/div[2]/input")
	public WebElement PreviousUsedPasswordsMsgBox;
	
	@FindBy(how = How.XPATH, using = "//form/div/div[4]/div[2]/p")
	public WebElement MinValidationPreviousUsedPasswordsMsgBox;
	
	@FindBy(how = How.XPATH, using = "//fieldset/div/form/div/div[4]/div[2]/p")
	public WebElement MaxValidationPreviousUsedPasswordsMsgBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[5]/div[1]/input")
	public WebElement FailedLoginAttemptsBox;
	
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[5]/div[1]/p")
	public WebElement MinValidationFailedLoginAttemptsBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[5]/div[2]/input")
	public WebElement FailedLoginAttemptsAlertBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[5]/div[2]/p")
	public WebElement ValidationFailedLoginAttemptsAlertBox;
	
	@FindBy(how = How.XPATH, using = "//div/form/div/div[6]/div[1]/div[2]/input")
	public WebElement monthlyWindowsBox;
	
	@FindBy(how = How.XPATH, using = "//fieldset/div/form/div/div[6]/div[2]/p")
	public WebElement ValidatioMonthlyWindow;
	
	@FindBy(how = How.XPATH, using = "//*[@id='hideProcTitle']")
	public WebElement ShowSequenceCheckbox;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"showHideCriteriaDetails\"]")
	public WebElement AcceptanceCriteriaCheckbox;
	

	@FindBy(how = How.XPATH, using = "//*[@id=\"displayNullValues\"]")
	public WebElement DoNotCheckbox;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"allowPrintSites\"]")
	public WebElement AllowAllSiteCheckbox;
	
	@FindBy(how = How.XPATH, using = "//div[12]/div/button")
	public WebElement SaveButton;
	
	
	
	 

	// --------------------public methods--------------------

	public WebDriver getDriver() {
		return this.driver;

	}

	public void clickGeneralSetting() throws Exception{
		btnHelper.click(generalSettingModuleName);
	}
	public void verifyGeneralSetting() {
		Assert.assertEquals(textBoxHelper.getText(generalSettingName), ObjectRepo.reader.getGeneralSettingName());
	}
public void clickBreadcrumb() {
	ClickBread.click();
		
	}
		
	public void verifyRedirectPreviousPage() {
		Assert.assertEquals(textBoxHelper.getText(breadcrumb), ObjectRepo.reader.getBreadcrumb());
	}
		
	public void clickStandardReport() {
		btnHelper.click(clickRecordTitle);
	}
		
	public void removePreviousData() {
		textBoxHelper.clear(clickRecordTitle);
	}
		
	public void verifyMandatoryValidationMessage() {
		
		Assert.assertEquals(textBoxHelper.getText(validationRecordTitle), ObjectRepo.reader.getValidationRecordTitle());
	}
	
	public void lessThanCharactersMsg(String val) {
		
		textBoxHelper.sendKeys(minLengthvalidationMsg, val);
	}
	public void verifyMinimumValidationMsg() {
		
		Assert.assertEquals(textBoxHelper.getText(verifyMinLengthvalidationMsg),ObjectRepo.reader.getVerifyMinLengthvalidationMsg());
	}
		
	public void moreThanCharacters(String value) {
		textBoxHelper.sendKeys(clickRecordTitle, value);
	}
		
	public void verifyMaximumValidationMsg() {
		Assert.assertEquals(textBoxHelper.getText(verifyMaxLengthvalidationMsg),ObjectRepo.reader.getVerifyMaxLengthvalidationMsg());
	}
public void enterValidData(String value) {
		textBoxHelper.sendKeys(clickRecordTitle, value);
}
public void verifyValidDataAllowed() {
	Assert.assertEquals(true, true);
}

public void clickStandardReportSubtitle() {
		btnHelper.click(stdReportsubTitlebox);
}

public void removePreviousSubtitletext() {
		textBoxHelper.clear(stdReportsubTitlebox);
}
	
	public void verifySubTitleRequiredMsg() {

		Assert.assertEquals(textBoxHelper.getText(stdReportsubTitleRequiredValidation),ObjectRepo.reader.getStdReportsubTitleRequiredValidation());
	
}
	
	public void enterlessCharactersforSubtitlebox(String value) {
		textBoxHelper.sendKeys(stdReportsubTitlebox, value);
}

public void verifyMinlengthValidationMsg() {
	
		Assert.assertEquals(textBoxHelper.getText(stdReportsubTitleMinValidation),ObjectRepo.reader.getStdReportsubTitleMinValidation());
	
}

public void enterMorethanCharactersSubtitle(String value) {
		textBoxHelper.sendKeys(stdReportsubTitlebox, value);
	
}

public void verifyMaxlengthValidationMsgSubtitle() {
	
		Assert.assertEquals(textBoxHelper.getText(stdReportsubTitleMaxValidation),ObjectRepo.reader.getStdReportsubTitleMaxValidation());
}
	
	public void validDataallowedSubtitle(String value) {
		textBoxHelper.sendKeys(stdReportsubTitlebox, value);
}
public void verifyvalidDataSubtitle() {
	
	Assert.assertEquals(true, true);

}
	
	public void clickContactdetail() {
		btnHelper.click(clickContactdetails);
}
	
	public void removePreviousFromTextarea() {
		textBoxHelper.clear(clickContactdetails);
}
	
	public void enterLessforTextarea(String value) {
		textBoxHelper.sendKeys(clickContactdetails, value);
}
	
	public void verifyMinValidation() {

		Assert.assertEquals(textBoxHelper.getText(textareaMinValidation),ObjectRepo.reader.getTextareaMinValidation());
}
	
	public void enterMorethanCharactersinTextarea(String value) {
		textBoxHelper.sendKeys(clickContactdetails, value);
}

public void verifyMaxValidationTextArea() {
	
		Assert.assertEquals(textBoxHelper.getText(textareaMaxValidation),ObjectRepo.reader.getTextareaMaxValidation());
	
}
	
	public void enterValidforTextArea(String value) {
		textBoxHelper.sendKeys(clickContactdetails, value);
}
public void verifyValidDataForTextarea() {
	Assert.assertEquals(true,true);		
}
	 
	public void clickPasswordAgingLimit() {
		btnHelper.click(passwordAgingBox);
}
 
 public void removePreviousDatapasswordAging() {
		textBoxHelper.clear(passwordAgingBox);
}
 
 public void enterZeroinTextbox(String value) {
	 
		textBoxHelper.sendKeys(passwordAgingBox, value);
}
 
 public void verifyMinValidationforPasswordAging() {
	 
		Assert.assertEquals(textBoxHelper.getText(verify0passwordAgingBox), ObjectRepo.reader.getVerify0passwordAgingBox());
}
 
 
 public void entermorethan999Numeric(String value) {
		textBoxHelper.sendKeys(passwordAgingBox, value);

}
 
 public void verifyMaxValidationPasswordAging() {
		scroll.scrollDown(driver);
		Assert.assertEquals(textBoxHelper.getText(verify999passwordAgingBox), ObjectRepo.reader.getVerify999passwordAgingBox());
}
 
 public void enterValidinPasswordAging(String value) {
		textBoxHelper.sendKeys(passwordAgingBox, value);
}
 public void verifypasswordAgingBoxallowValidData() {
	 
	 Assert.assertEquals(true, true);
	
}
 
 public void enterlessPasswordAging(String value) {
		textBoxHelper.sendKeys(passwordAgingBox, value);
}
 
 public void clickPasswordAgingLimitAlertMsg() {
		btnHelper.click(PasswordAgingMsgbox);
}
	 
	public void removepreviousPasswordAgingMsg() {
		textBoxHelper.clear(PasswordAgingMsgbox);
}
 
 public void enterlessCharactersPasswordAgingMsg(String value) {
		textBoxHelper.sendKeys(PasswordAgingMsgbox, value);
}
 
 public void verifyMinValidationPasswordAgingMsg() {
		Assert.assertEquals(textBoxHelper.getText(PasswordAgingMsgMinValidation), ObjectRepo.reader.getPasswordAgingMsgMinValidation());
}
 
 public void enterMoreCharPasswordAgingMsg(String value) {
		textBoxHelper.sendKeys(PasswordAgingMsgbox, value);
}
 
 public void verifyMaxValidationPasswordAgingMsgalert() {
	 
		Assert.assertEquals(textBoxHelper.getText(PasswordAgingMsgMaxValidation),ObjectRepo.reader.getPasswordAgingMsgMaxValidation());
}
	 
	public void enterValidDataPasswordAgingMsgBox(String value) {
		textBoxHelper.sendKeys(PasswordAgingMsgbox, value);
}
 public void verifyValidDataAlertMsg() {
	 
	 Assert.assertEquals(true, true);

}
 
 public void clickOnNumberofPreviousUsedPassword() {
		btnHelper.click(PreviousUsedPasswordsbox);
}
 public void removePreviousUseddatainTextbox() {
		textBoxHelper.clear(PreviousUsedPasswordsbox);
}
 public void enterZeroinPreviousPasswordTextbox(String value) {
		textBoxHelper.sendKeys(PreviousUsedPasswordsbox, value);
}
	 
	public void verifyMinValidationMsgPreviousAllowedBox() {

		Assert.assertEquals(textBoxHelper.getText(enterZeroValidationPreviousUsedPasswordsBox),ObjectRepo.reader.getEnterZeroValidationPreviousUsedPasswordsBox());
}
 
 public void enterLess4inPreviousUsedTextBox(String value) {
		textBoxHelper.sendKeys(PreviousUsedPasswordsbox, value);
}

	public void verifyValidationMsglessThan4PreviousPasswordUsed() {
		Assert.assertEquals(textBoxHelper.getText(lessthan4ValidationPreviousUsedPasswordsBox),ObjectRepo.reader.getLessthan4ValidationPreviousUsedPasswordsBox());
}
 
 public void enterMorethan99inNumberofPreviousUsedPasswords(String value) {
		textBoxHelper.sendKeys(PreviousUsedPasswordsbox, value);
}
 
 public void verifyMorethan99inNumberofPreviousUsed() {
		Assert.assertEquals(textBoxHelper.getText(greaterthan99ValidationPreviousUsedPasswordsBox),ObjectRepo.reader.getGreaterthan99ValidationPreviousUsedPassBox());
}
 
 public void entervaliddatainPreviousUsed(String value) {
		textBoxHelper.sendKeys(PreviousUsedPasswordsbox, value);
}
 public void verifyValidDatainPreviousUsedTextBox() {
	Assert.assertEquals(true, true);
}

public void clickOnNumberofPreviousUsedPasswordMsgBox() {
		btnHelper.click(PreviousUsedPasswordsMsgBox);
}

public void removePreviousDataofPreviousUsedPasswordAlertMsgBox() {
		textBoxHelper.clear(PreviousUsedPasswordsMsgBox);
}

public void enterLessthan2CharinAmlertMsgBox(String value) {
		textBoxHelper.sendKeys(PreviousUsedPasswordsMsgBox, value);
}
public void verifyMinimumValidationMsgforAlertBox() {
		Assert.assertEquals(textBoxHelper.getText(MinValidationPreviousUsedPasswordsMsgBox),ObjectRepo.reader.getPasswordAgingMsgMinValidation());
}

public void enterMorethan200ValidationMsg(String value) {
		textBoxHelper.sendKeys(PreviousUsedPasswordsMsgBox, value);
}

public void verifyMaxValidationMsgforAlertMsgBox() {
		Assert.assertEquals(textBoxHelper.getText(MaxValidationPreviousUsedPasswordsMsgBox),ObjectRepo.reader.getPasswordAgingMsgMaxValidation());
}
	
	public void enterValidDatainPreviouslyUsedAlertMsgBox(String value) {
		textBoxHelper.sendKeys(PreviousUsedPasswordsMsgBox, value);
}

public void verifyValidDatainPreviousUsedAlertMsgBox() {
	Assert.assertEquals(true, true);
}

public void clickFailedLoginAttemptsTextbox() {
		btnHelper.click(FailedLoginAttemptsBox);
}

public void removePreviousDataofFailedLoginAttemps() {
		textBoxHelper.clear(FailedLoginAttemptsBox);
}
public void enterZeroinFailedLoginAttemptsTextbox(String value) {
		textBoxHelper.sendKeys(FailedLoginAttemptsBox, value);
}
public void verifyMinforFailedLoginAttemptsTextbox() {
		Assert.assertEquals(textBoxHelper.getText(MinValidationFailedLoginAttemptsBox),ObjectRepo.reader.getMinValidationFailLoginAttemptsBox());
	
}
public void enterMorethan999FailedLoginAttemptsTextbox(String value) {
		textBoxHelper.sendKeys(FailedLoginAttemptsBox, value);
	
}
public void verifyMaxforFailedLoginAttemptsTextbox() {
		Assert.assertEquals(textBoxHelper.getText(MinValidationFailedLoginAttemptsBox),ObjectRepo.reader.getMaxValidationFailLoginAttemptsBox());
	
}
public void enterValidDataforFailedLoginAttempts(String value) {
		textBoxHelper.sendKeys(FailedLoginAttemptsBox, value);
}
public void verifyValidDataforFailedLoginAttemptsTextbox() {
	Assert.assertEquals(true, true);
	
}

public void clickFailedLoginAttemptsMsgTextbox() {
		btnHelper.click(FailedLoginAttemptsAlertBox);
}
public void removeDatafromAlertMsgBox() {
		textBoxHelper.clear(FailedLoginAttemptsAlertBox);
}

public void enterLessthan2CharinFailedAlertMsgBox(String value) {
		textBoxHelper.sendKeys(FailedLoginAttemptsAlertBox, value);
}

public void verifyMinCharValidationMsgFailedLoginAlertBox() {
		Assert.assertEquals(textBoxHelper.getText(ValidationFailedLoginAttemptsAlertBox),ObjectRepo.reader.getPasswordAgingMsgMinValidation());
}

public void enterMorethan200CharinMsgAlertBox(String value) {
		textBoxHelper.sendKeys(FailedLoginAttemptsAlertBox, value);
}
public void verifyMaxCharFailedAlertMsg() {
		Assert.assertEquals(textBoxHelper.getText(ValidationFailedLoginAttemptsAlertBox),ObjectRepo.reader.getPasswordAgingMsgMaxValidation());
}
public void enterValidDatainFailedAlertMsgbox(String value) {
		textBoxHelper.sendKeys(FailedLoginAttemptsAlertBox, value);
}

public void verifyValidCharFailedAlertMsg() {
	Assert.assertEquals(true, true);
}
	
	public void clickmonthlywindowsbox() {
		btnHelper.click(monthlyWindowsBox);
}
public void removedataFrommontlyWindows() {
		textBoxHelper.clear(monthlyWindowsBox);
}
public void enterZeroInMonthlyWindows(String value) {
		textBoxHelper.sendKeys(monthlyWindowsBox, value);
}
public void verifyZeroinMonthlyWindows() {
		Assert.assertEquals(textBoxHelper.getText(ValidatioMonthlyWindow), ObjectRepo.reader.getValMinMonthlyWindow());
}
	
	public void enterMaxinMonthlyWindows(String value) {
		textBoxHelper.sendKeys(monthlyWindowsBox, value);
}
public void verifyMaxMontlyWindows() {
		Assert.assertEquals(textBoxHelper.getText(ValidatioMonthlyWindow), ObjectRepo.reader.getValMaxMonthlyWindow());
	
}
	
	public void enterValidDatainMonthlyWindow(String value) {
		textBoxHelper.sendKeys(monthlyWindowsBox, value);
}
public void verifyValidDataMonthlyWindows() {
	Assert.assertEquals(true, true);
}

public void clickCheckboxofShowSequence() {
	    beforeClickCheckBox = ShowSequenceCheckbox.isSelected();
		btnHelper.click(ShowSequenceCheckbox);
}

public void verifyChecboxofShowSequence() {
	afterClickCheckbox= ShowSequenceCheckbox.isSelected();
	if (beforeClickCheckBox==afterClickCheckbox) {
		Assert.assertEquals(false, true);
	} else {
		Assert.assertEquals(true, true);
	        }
	
}
public void clickShowAcceptanceCheckbox() {
		btnHelper.click(AcceptanceCriteriaCheckbox);
}
public void verifyChecboxofAcceptanceCriteria() {
	
	if (AcceptanceCriteriaCheckbox.isSelected()) {
		Assert.assertEquals(false, true);
	} else {
		Assert.assertEquals(true, true);
	        }
	
}

public void clickonDoNoTallowCheckbox() {
		btnHelper.click(DoNotCheckbox);
}
public void verifyCheckboxofDoNotAllow() {
	if (DoNotCheckbox.isSelected()) {
		Assert.assertEquals(true, true);
	} else {
		Assert.assertEquals(false, true);
	        }
	
}

public void clickAllowAllSiteCheckbox() {
		btnHelper.click(AllowAllSiteCheckbox);
}

public void verifyCheckboxofAllowAllSite() {
	if (AllowAllSiteCheckbox.isSelected()) {
		Assert.assertEquals(false, true);
	} else {
		Assert.assertEquals(true, true);
	        }
	
}
public void ClickonSaveButton() {
		btnHelper.click(SaveButton);
}

public void verifySavebutton() throws Exception {
	//SaveButton
		Thread.sleep(3000);
		scroll.scrollUp(driver);
		Assert.assertEquals(textBoxHelper.getText(breadcrumb), ObjectRepo.reader.getBreadcrumb());
}



}// end
