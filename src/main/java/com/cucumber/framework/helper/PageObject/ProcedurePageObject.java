package com.cucumber.framework.helper.PageObject;

import java.awt.Desktop.Action;
import java.awt.RenderingHints.Key;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Browser.BrowserHelper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Pagination.Pagination;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

public class ProcedurePageObject extends PageBase {
	private WebDriver driver;
	private Helper helper = new Helper();
	public static String getProcedureName;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(SitesPageObject.class);
	private Search sObj;
	private WaitHelper waitObj;
	public static String procedureName, beforeStatus;
	private DropDownHelper drpHelper;
	public BrowserHelper browserHelper = new BrowserHelper(ObjectRepo.driver);
	private TextBoxHelper textboxHelper;
	public ProcedurePageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		sObj = new Search();
		waitObj = new WaitHelper(ObjectRepo.driver, ObjectRepo.reader);
		drpHelper = new DropDownHelper(ObjectRepo.driver);
		textboxHelper =new TextBoxHelper(ObjectRepo.driver);
	}

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.home']")
	public WebElement administrationTile;
	@FindBy(how = How.XPATH, using = "//IMG[@src='./assets/images/procedures.png']")
	public WebElement ProcedureTile;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[2]/a")
	public WebElement previousTileclick;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[2]/span")
	public WebElement previousTile;
	@FindBy(how = How.XPATH, using = "//tr[1]/td[1]/span[1]/span/a")
	public WebElement firstRecord;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[4]/span")
	public WebElement addEditProcedure;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']")
	public WebElement addEditProcedurebreadcrumb;
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save(vm.procedure,procedureForm)']")
	public WebElement saveBtn;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[3]/span")
	public WebElement procedureBreadcrumb;
	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.admin.procedures.list']")
	public WebElement cancelBtn;
	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.admin.procedures.edit.procedure']")
	public WebElement addnewBtn;
	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.procedure.title']")
	public WebElement procedurename;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURENAMEISREQUIRED']")
	public WebElement procedurenameRequired;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURETAGSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement procedurenameMinvalidation;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURENAMESHOULDBEATLEAST200CHARACTERSLONG']")
	public WebElement procedurenameMaxvalidation;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURENAMEMUSTBEUNIQUE']")
	public WebElement procedurenameUniquevalidation;
	@FindBy(how = How.XPATH, using = "//tr[1]/td[1]/span[1]/span/a")
	public WebElement firstProcedureRecord;
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save(vm.procedure,procedureForm)']")
	public WebElement editrecordSaveBtn;
	@FindBy(how = How.XPATH, using = "//tr[1]/td[6]/span[1]/span/a/img")
	public WebElement CopyIcon;
	@FindBy(how = How.XPATH, using = "//button[@class='confirm']")
	public WebElement OkBtnofCopyIcon;
	@FindBy(how = How.XPATH, using = "//fieldset/input")
	public WebElement procedurenameincopypopup;
	@FindBy(how = How.XPATH, using = "//input[@name='proceduretag']")
	public WebElement proceduretagInputbox;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURETAGISREQUIRED']")
	public WebElement proceduretagRequired;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURETAGSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement proceduretagMinValidation;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURETAGSHOULDNOTBEMORETHAN20CHARACTERSLONG']")
	public WebElement proceduretagMaxValidation;
	@FindBy(how = How.XPATH, using = "//input[@name='isInstructionForUser']")
	public WebElement instructionForUsercheckbox;
	@FindBy(how = How.XPATH, using = "//textarea[@ng-change='vm.onChangeInstructionForUserTextbox()']")
	public WebElement instructionForUserInput;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERISREQUIRED']")
	public WebElement instructionForUserRequired;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement instructionForUserMinValidation;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDNOTBEMORETHAN800CHARACTERSLONG']")
	public WebElement instructionForUserMaxValidation;
	@FindBy(how = How.XPATH, using = "//input[@name='instructionForUserRadioButtonURL']")
	public WebElement UrlCheckbox;
	@FindBy(how = How.XPATH, using = "//input[@name='urlTextBox']")
	public WebElement UrlInput;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.URLISREQUIRED']")
	public WebElement UrlInputrequired;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.procedure.edit.procedure.NOTVALIDURL2']")
	public WebElement invalidUrlInput;
	@FindBy(how = How.XPATH, using = "//input[@id='txtSearch']")
	public WebElement searchBox;
	
	@FindBy(how = How.XPATH, using = "//div[@class='view-previous-drop-down-menu ng-scope']")
	public WebElement previousVersionLink;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.viewVersion(field,row,version)']")
	public WebElement previousVersionLinkList;
	
	@FindBy(how = How.XPATH, using = "(//a[@ng-click='openModal(row._id,row.title,row.route);'])[1]")
	public WebElement linkedInformation;

	@FindBy(how = How.XPATH, using = "//h4[@class='col-sm-11 ng-binding']")
	public WebElement linkedInformationScreen;
	
	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.closePopup()']")
	public WebElement linkedInformationPopupClose;
	
	@FindBy(how = How.XPATH, using = "(//a[@ng-click='vm.go(field,row,vm.resource);'])[1]")
	public WebElement procedureNameLink;
	
	@FindBy(how = How.XPATH, using = "//input[@name='procedureName']")
	public WebElement verifyProcedureName;
	
	@FindBy(how = How.XPATH, using = "//a[@ng-if='vm.isCreate']")
	public WebElement addNewBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@name='procedureName']")
	public WebElement procedureTextField;
	
	@FindBy(how = How.XPATH, using = "//input[@name='proceduretag']")
	public WebElement tagTextField;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addProceduerTags(vm.tag)']")
	public WebElement addTagBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search and select Question']")
	public WebElement questionTextField;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addQuestion(vm.selectedQuestion)']")
	public WebElement addQuestionBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save(vm.procedure,procedureForm)']")
	public WebElement saveProcedureBtn;
	
	@FindBy(how = How.XPATH, using = "//div[@class='col-md-10 col-sm-10 col-xs-10 schedule-startdat-left-padi-none ng-binding']")
	public WebElement verifyTagName;
	
	@FindBy(how = How.XPATH, using = "//input[@ng-model='question.isoptionalquestion']")
	public WebElement optionQuestionCheckbox;
	
	@FindBy(how = How.XPATH, using = "//p[@ng-bind-html='vm.getOptionalQuestionMessage']")
	public WebElement mandatoryValidation;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-if=\"question.type.title !== 'File Attachment'\"]")
	public WebElement acceptanceCriteria;
	
	@FindBy(how = How.XPATH, using = "//select[@ng-model='criteria.criteria.condition']")
	public WebElement conditionDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//select[@ng-model='criteria.criteria.value']")
	public WebElement valueField;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.saveAC(acceptanceCriteriaForm)']")
	public WebElement saveBtnAcceptanceScreen;
	
	@FindBy(how = How.XPATH, using = "//span[@class='procedure-Acceptance_Criteria_text ng-binding ng-scope']")
	public WebElement acceptanceCriteriaAdded;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.removeQuestionAdded(question, $index)']")
	public WebElement removeQuestionIcon;
	
	@FindBy(how = How.XPATH, using = "//div[@class='col-md-1 col-sm-1 col-xs-12 displaybackgroundfordate procedure-optinal-question-height']")
	public WebElement questionRemoved;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='wrap-site-title col-md-2 col-sm-2 col-xs-12 displaybackground question-name-font displaybackground-procedure-01 ng-binding'])[1]")
	public WebElement firstQuestionName;
	
	@FindBy(how = How.XPATH, using = "//span[@title='move down']")
	public WebElement clickSortBtn;
	
	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.textSearch']")
	public WebElement searchTextfield;
	
	@FindBy(how = How.XPATH, using = "//div[@class='col-md-10 col-md-offset-1 alert alert-warning no-records ng-binding']")
	public WebElement tagNoRecordFoundMsg;
	
	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.options.pageSize']")
	public WebElement pageSizeDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//td[@data-th='Linked Information :']")
	public WebElement countRecords;
	
	@FindBy(how = How.XPATH, using = "//button[@class='btn btn-danger11 ng-binding']")
	public WebElement backBtnPreviousVersion;
	
	@FindBy(how = How.XPATH, using = "//h4[@class='col-sm-11 ng-binding']")
	public WebElement linkedInfoPopupHeading;
	
	@FindBy(how = How.XPATH, using = "//i[@class='ng-binding']")
	public WebElement currentVersion;
	
	@FindBy(how = How.XPATH, using = "//label[@for='searchbytag']")
	public WebElement filterQuestionTagLbl;
	
	@FindBy(how = How.XPATH, using = "//label[@for='selectquestion']")
	public WebElement selectQuestionLbl;
	
	@FindBy(how = How.XPATH, using = "(//a[@ng-click='vm.createduplicate(field,row)'])[1]")
	public WebElement copyIconTooltip;
	
	@FindBy(how = How.XPATH, using = "(//span[@ng-switch-when='active/inactive'])[1]")
	public WebElement getStatus;
	
	@FindBy(how = How.XPATH, using = "//label[@ng-style=\"!vm.save ? {'cursor':'Default'} : {'cursor':'pointer'}\"]")
	public WebElement statusToggleBtn;
	
	@FindBy(how = How.XPATH, using = "(//a[@ng-click='vm.go(field,row,vm.resource);'])[1]")
	public WebElement selectFirstProcedure;
	
	@FindBy(how = How.XPATH, using = "(//span[@class='ng-binding'])[1]")
	public WebElement acceptanceCriteriaScreen;
	
	@FindBy(how = How.XPATH, using = "//input[@name='advance0']")
	public WebElement advancedAcceptanceCriteria;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addCondition()']")
	public WebElement addConditionAdvancedAC;
	
	@FindBy(how = How.XPATH, using = "//select[@class='form-control acceptancecriteria-mobile-mar pull-left Acceptance_Criteria_Please_Select_Question ng-pristine ng-untouched ng-valid ng-scope']")
	public WebElement questionDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//select[@class='form-control acceptancecriteria-mobile-mar margin-between pull-left ng-pristine ng-untouched ng-valid']")
	public WebElement operatorDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//select[@class='form-control build-drop ng-pristine ng-untouched ng-valid ng-scope']")
	public WebElement valueTextField;
	
	@FindBy(how = How.XPATH, using = "(//button[@ng-click='vm.addToCriteria(vm.criteria[$index], $index)'])[1]")
	public WebElement addBtnAdvancedAC;
	
	@FindBy(how = How.XPATH, using = "//div[@class='well col-md-10']")
	public WebElement verifyACAdded;
	
	
	
	
	
	
	
	
	
	
//	==================================== Comman Function ====================================
	
	
	public void clickOnProcedureTile() {
		//ProcedureTile.click();
		btnHelper.click(ProcedureTile);
	}
	public void clickonBreadcrumb() {
		//previousTileclick.click();
		btnHelper.click(previousTileclick);
	}
	public void verifyPageRedirection() {
		Assert.assertEquals( textboxHelper.getText(previousTile), ObjectRepo.reader.getAdministrationTile());
	}
	public void clickonFirstProcedure() {
		//firstRecord.click();
		btnHelper.click(firstRecord);
	}
	public void verifyRecordEditscreen() {
		Assert.assertEquals( textboxHelper.getText(addEditProcedure), ObjectRepo.reader.getaddEditProcedure());
	}
	public void verifyWholebreadcrumbEditRecordScreen() throws Exception {
		Thread.sleep(2000);
		Assert.assertEquals(textboxHelper.getText(addEditProcedurebreadcrumb), ObjectRepo.reader.getaddEditProcedurebreadcrumb());
	}
	public void clickOnSaveBtn() {
		//saveBtn.click();
		btnHelper.click(saveBtn);
	}
	public void verifyProcedurelistingScreen() {
		Assert.assertEquals(  textboxHelper.getText(procedureBreadcrumb), ObjectRepo.reader.getprocedureBreadcrumb());
	}
	public void clickOnCancelBtn() {
		//cancelBtn.click();
		btnHelper.click(cancelBtn);
	}
	public void verifycancelBtnFunctionality() {
		Assert.assertEquals(  textboxHelper.getText(procedureBreadcrumb), ObjectRepo.reader.getprocedureBreadcrumb());
	}
	public void verifySaveBtnColor() {
		Assert.assertEquals(helper.BtnColor(saveBtn), ObjectRepo.reader.getLightGreenColor());
	}
	public void verifyCancelBtnColor() {
		Assert.assertEquals(helper.BtnColor(cancelBtn), ObjectRepo.reader.getDarkGreyColor());
	}
	public void verifybreadcrumbiAudittrail() {
		Assert.assertEquals(textboxHelper.getText(addEditProcedurebreadcrumb),ObjectRepo.reader.getaddEditProcedurebreadcrumbForAuditTrail());
	}
	public void verifybackBtnofviewAuditredirection() {
		Assert.assertEquals(  textboxHelper.getText(procedureBreadcrumb), ObjectRepo.reader.getprocedureBreadcrumb());
	}
	public void clickonAddnewBtn() {
//		btnHelper.click(addnewBtn);
		helper.javascriptExecutorClick(addnewBtn);
	}
	public void clickonProcedurenameInput() {
		procedurename.sendKeys(Keys.TAB);
	}
	public void verifyreuiredValidation() {
		Assert.assertEquals( textboxHelper.getText(procedurenameRequired), ObjectRepo.reader.getprocedurenameRequired());
	}
	public void enterLessThan2inProcedurename(String value) {
		procedurename.sendKeys(value);

	}
	public void verifyMinValidationProcedureName() {
		Assert.assertEquals( textboxHelper.getText(procedurenameMinvalidation), ObjectRepo.reader.getProcedurenameMinvalidation());
	}
	public void enterMorethan200CharProcedureName(String value) {
		//procedurename.sendKeys(value);
		textboxHelper.sendKeys(procedurename, value);
	}
	public void verifyMaxValidatioProcedureName() {
		
		Assert.assertEquals( textboxHelper.getText(procedurenameMaxvalidation),ObjectRepo.reader.getProcedurenameMaxvalidation());	
	}
	public void entervalidDataProcedurename(String value) {
		//procedurename.sendKeys(value);
		textboxHelper.sendKeys(procedurename, value);
	}
	public void verifyValiddataProcedurename() {
		boolean noValMSg;
		boolean maxVal= driver.findElements(By.xpath("//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURENAMESHOULDBEATLEAST200CHARACTERSLONG']")).size()!=0;
		boolean uniqueVal = driver.findElements(By.xpath("//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURENAMEMUSTBEUNIQUE']")).size()!=0;
		boolean minVal = driver.findElements(By.xpath("//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURETAGSHOULDBEATLEAST2CHARACTERSLONG']")).size()!=0;
		boolean requiredVal = driver.findElements(By.xpath("//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURENAMEISREQUIRED']")).size()!=0;
		
		if(maxVal==false && uniqueVal==false && minVal==false && requiredVal==false) {
			noValMSg=false;
		}else {
			noValMSg=true;
		}
		Assert.assertEquals(noValMSg, false);
	}
	public void enterDuplicateDataProcedurename(String value) {
		//procedurename.sendKeys(value);
		textboxHelper.sendKeys(procedurename, value);
	}
	public void verifyUniqueValidationProcedurename() throws Exception {
		Assert.assertEquals(textboxHelper.getText(procedurenameUniquevalidation), ObjectRepo.reader.getProcedurenameUniquevalidation());
	}
	public void clickonFirstprocedureRecord() {
		//firstProcedureRecord.click();
		btnHelper.click(firstProcedureRecord);
	}
	public void clickonProcedurenameEdit() {
		btnHelper.click(procedurename);
	}
	public void editProcedurename(String value) {
		//procedurename.clear();
		//btnHelper.click(procedurename);
		//procedurename.sendKeys(value);
		textboxHelper.clearAndSendKeys(procedurename, value);

	}
	public void clickOnSaveBtnProcedurerecord() {
		//editrecordSaveBtn.click();
		btnHelper.click(editrecordSaveBtn);
	}
	public void verifyEditedModule(String value) {
		Assert.assertEquals(textboxHelper.getText(firstProcedureRecord),value);
	}
	public void clickonCopyIcon() {
		//CopyIcon.click();
		btnHelper.click(CopyIcon);
	}
	public void verifypopupwithProcedurename() {
		waitObj.waitForElementClickable(driver.findElement(By.xpath("//fieldset/input")));
		
		if(driver.findElements(By.xpath("//fieldset/input")).size() != 0)
		{
		Assert.assertEquals(true, true);
		//OkBtnofCopyIcon.click();
		btnHelper.click(OkBtnofCopyIcon);
		}
		else
		{
			Assert.assertEquals(false, true);
		}
	}
	public void verifyPostfixofprocedurecopiedIcon() 
	{
		
		String s = textboxHelper.getText(procedurenameincopypopup); //procedurenameincopypopup.getText();
		//procedurenameincopypopup.clear();
		textboxHelper.clear(procedurenameincopypopup);
		String[] cicon = s.split("Copy");
		String p = cicon[1];
	}
	public void clickonOkofCopyIcon() {
		//OkBtnofCopyIcon.click();
		btnHelper.click(OkBtnofCopyIcon);
	}
	 
	public void clickonProcedureTag() {
		//proceduretagInputbox.click();
		btnHelper.click(proceduretagInputbox);
		proceduretagInputbox.sendKeys(Keys.TAB);
	}
	public void verifyProcedureTagRequiredValidation() {
		Assert.assertEquals(textboxHelper.getText(proceduretagRequired),  ObjectRepo.reader.getProceduretagRequired());
	}
	public void enterlessthan2ProcedureTag(String value) {
		//proceduretagInputbox.sendKeys(value);
		textboxHelper.sendKeys(proceduretagInputbox, value);
	}
	public void verifyMinValidationProceduretag() {
		Assert.assertEquals( textboxHelper.getText(proceduretagMinValidation), ObjectRepo.reader.getProceduretagMinValidation());
	}
	public void enterMorethan200CharProcedureTag(String value) {
		//proceduretagInputbox.sendKeys(value);
		textboxHelper.sendKeys(proceduretagInputbox, value);
	}
	public void verifyMaxValidatioProcedureTag() {
		Assert.assertEquals(  textboxHelper.getText(proceduretagMaxValidation), ObjectRepo.reader.getProceduretagMaxValidation());
	}
	public void entervalidDataProcedureTag(String value) {
		textboxHelper.sendKeys(proceduretagInputbox, value);
	}
	public void verifyvaliddataProcedureTag() {
		boolean noValMSg;
		boolean maxVal= driver.findElements(By.xpath("//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURETAGSHOULDNOTBEMORETHAN20CHARACTERSLONG']")).size()!=0;
		boolean minVal = driver.findElements(By.xpath("//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURETAGSHOULDBEATLEAST2CHARACTERSLONG']")).size()!=0;
		boolean requiredVal = driver.findElements(By.xpath("//p[@translate='secure.admin.procedure.edit.procedure.PROCEDURETAGISREQUIRED']")).size()!=0;
		
		if(maxVal==false && minVal==false && requiredVal==false) {
			noValMSg=false;
		}else {
			noValMSg=true;
		}
		Assert.assertEquals(noValMSg, false);
	}
	public void verifyinstructionCheckbox() {
		if(instructionForUsercheckbox.isSelected()) {
			Assert.assertEquals(true, true);
		}else {
			Assert.assertEquals(false, true);
		}
	}
	public void clickonInstructionInput() {
		btnHelper.click(instructionForUserInput);
	}
	public void pressTabInstruction() {
		instructionForUserInput.sendKeys(Keys.TAB);
	}
	public void verifyInstructionRequired() {
		Assert.assertEquals(  textboxHelper.getText(instructionForUserRequired), ObjectRepo.reader.getInstForUsrTxtBoxRqiMsg());
	}
	public void enterLessThan2inInstruction(String value) {
		textboxHelper.sendKeys(instructionForUserInput, value);
	}
	public void minValidationInstruction() {
		
		Assert.assertEquals(  textboxHelper.getText(instructionForUserMinValidation), ObjectRepo.reader.getInstForUsrTxtBoxMinMsg());
	}
	public void enterMorethan200CharInstruction(String value) {
		//instructionForUserInput.sendKeys(value);
		textboxHelper.sendKeys(instructionForUserInput, value);
	}
	public void verifyMaxValidatioInstruction() {
		
		Assert.assertEquals(  textboxHelper.getText(instructionForUserMaxValidation), ObjectRepo.reader.getInstForUsrTxtBoxMaxMsg());
	}
	public void entervalidDataInstruction(String value) {
		//instructionForUserInput.sendKeys(value);
		textboxHelper.sendKeys(instructionForUserInput, value);
	}
	public void verifyValiddataInstruction() {
		boolean noValMSg;
		boolean maxVal= driver.findElements(By.xpath("//p[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDNOTBEMORETHAN800CHARACTERSLONG']")).size()!=0;
		boolean minVal = driver.findElements(By.xpath("//p[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDBEATLEAST2CHARACTERSLONG']")).size()!=0;
		boolean requiredVal = driver.findElements(By.xpath("//p[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERISREQUIRED']")).size()!=0;
		
		if(maxVal==false && minVal==false && requiredVal==false) {
			noValMSg=false;
		}else {
			noValMSg=true;
		}
		Assert.assertEquals(noValMSg, false);
	}
	public void clickonUrlChecbox() {
		//UrlCheckbox.click();
		btnHelper.click(UrlCheckbox);
	}
	public void verifyUlrcheckboxClickable() {
		if(UrlCheckbox.isSelected()) {
			Assert.assertEquals(true, true);
		}else {
			Assert.assertEquals(false, true);
		}
	}
	public void pressTabUrl() {
		UrlInput.sendKeys(Keys.TAB);
	}
	public void verifyRequiredMsgUrl() {
		Assert.assertEquals( textboxHelper.getText(UrlInputrequired), ObjectRepo.reader.getUrlInputrequired());
	}
	public void enterinvalidUrl(String value) {
	//	UrlInput.sendKeys(value);
		textboxHelper.sendKeys(UrlInput, value);
	}
	public void verifyInvalidUrl() {
		Assert.assertEquals( textboxHelper.getText(invalidUrlInput), ObjectRepo.reader.getInvalidUrlInput());
	}
	public void enterValidURLdata(String value) {
		//UrlInput.sendKeys(value);
		textboxHelper.sendKeys(UrlInput, value);
		
	}
	public void verifyValidUrl() {
		//String actual = UrlInput.getText();
		Assert.assertEquals(textboxHelper.getText(UrlInput), "");
	}
	public void enterprocedurenameSearch() {
		//searchBox.click();
		btnHelper.click(searchBox);
	}
	
	public void clickPreviousVersionLink() throws Exception {
		btnHelper.click(previousVersionLink);
	}
	
	public void verifyPreviousVersionLinkFunctionality() throws Exception {
		boolean actual = previousVersionLinkList.isDisplayed();
		Assert.assertEquals(actual, true);
	}
	
	public void clickLinkedInformation() throws Exception {
		btnHelper.click(linkedInformation);
	}
	
	public void verifyLinkedInformationFunctionality() throws Exception {
		boolean actual = linkedInformationScreen.isDisplayed();
		browserHelper.refresh();
		Assert.assertEquals(actual, true);
	}
	
	public void clickProcedureNameLink() throws Exception {
		getProcedureName = textboxHelper.getText(procedureNameLink);
		btnHelper.click(procedureNameLink);
	}
	
	public void verifyEditProcedureScreen() throws Exception {
		Assert.assertEquals(helper.getClipboardContents(verifyProcedureName), getProcedureName);
	}
	
	public void clickAddNewBtn() throws Exception {
		btnHelper.click(addNewBtn);
	}
	
	public void enterProcedureName(String enterProcedureName) throws Exception {
		procedureName = enterProcedureName+ " - " + helper.randomNumberGeneration();
		textboxHelper.clearAndSendKeys(procedureTextField, procedureName);
	}
	
	public void addTagInTextfield(String enterTagName) throws Exception {
		textboxHelper.clearAndSendKeys(tagTextField, enterTagName);
		btnHelper.click(addTagBtn);
	}
	
	public void addQuestionInTextfield(String enterQuestion) throws Exception {
		textboxHelper.sendKeys(questionTextField, enterQuestion);
		Thread.sleep(500);
		driver.findElement(By.xpath("//a[@ng-attr-title='{{match.label}}']")).click();
		btnHelper.click(addQuestionBtn);
	}
	
	public void clickSaveProcedureBtn() throws Exception {
		btnHelper.click(saveProcedureBtn);
	}
	
	public void verifyTagFunctionality(String enterTagName) throws Exception {
		Assert.assertEquals(enterTagName, verifyTagName.getText());
	}
	
	public void clickOptionalQuestionCheckbox() throws Exception {
		btnHelper.click(optionQuestionCheckbox);
	}
	
	public void verifyOptionalQuestionFunctionality() throws Exception {
		boolean actual = mandatoryValidation.isDisplayed();
		Assert.assertEquals(actual, true);
	}
	
	public void clickAcceptanceCriteriaOfQuestionAdded() throws Exception {
		btnHelper.click(acceptanceCriteria);
	}
	
	public void selectConditionDrpDwn() throws Exception {
		drpHelper.selectDropDownIndex(conditionDrpDwn, 1);
	}
	
	public void selectValueInField() throws Exception {
		drpHelper.selectDropDownIndex(valueField, 1);
	}
	
	public void clickSaveBtnAcceptanceScreen() throws Exception {
		btnHelper.click(saveBtnAcceptanceScreen);
	}
	
	public void verifyAcceptanceCriteriaAddedSuccess() throws Exception {
		boolean actual = acceptanceCriteriaAdded.isDisplayed();
		Assert.assertEquals(actual, true);
	}
	
	public void clickRemoveQuestionIcon() throws Exception {
		btnHelper.click(removeQuestionIcon);
	}
	
	public void verifyRemoveIconFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//div[@class='col-md-1 col-sm-1 col-xs-12 displaybackgroundfordate procedure-optinal-question-height']")).size() != 0)
		{
			actual = false;
		}
		else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void verifySortingFunctionality() throws Exception {
		boolean actual;
		String before = firstQuestionName.getText();
		System.out.println("Question Before Sort: "+ before);
		btnHelper.click(firstQuestionName);
		btnHelper.click(clickSortBtn);
		String after = firstQuestionName.getText();
		System.out.println("Question After Sort: "+ after);
		if(after.equals(before))
		{
			actual = false;
		}
		else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void searchByTagName(String searchText) throws Exception {
		textboxHelper.clearAndSendKeys(searchTextfield, searchText);
	}
	
	public void verifyNoRecordFoundMsg() throws Exception {
		Assert.assertEquals(tagNoRecordFoundMsg.getText().trim(), "No Records Found.");
	}
	
	public void clickOnPageSizeDrpDwn() throws Exception {
		drpHelper.selectDropDownIndex(pageSizeDrpDwn, 2);
	}
	
	public void verifyNumberOfRecordsOfPageSizeDrpDwn() throws Exception {
		
		Thread.sleep(2000);
		int count = driver.findElements(By.xpath("//td[@data-th='Linked Information :']")).size();
		
		System.out.println("Total Number Of Records: "+ count);
		Assert.assertEquals(count, 50);
	}
	
	public void clickOn20OfPageSizeDrpDwn() throws Exception {
		drpHelper.selectDropDownIndex(pageSizeDrpDwn, 1);
	}
	
	public void verifyNumberOfRecordsOf20PageSizeDrpDwn() throws Exception {
		Thread.sleep(2000);
		int count = driver.findElements(By.xpath("//td[@data-th='Linked Information :']")).size();
		System.out.println("Total Number Of Records: "+ count);
		Assert.assertEquals(count, 20);
	}
	
	public void verifyNumberOfRecordsOf10PageSizeDrpDwn() throws Exception {
		Thread.sleep(2000);
		int count = driver.findElements(By.xpath("//td[@data-th='Linked Information :']")).size();
		System.out.println("Total Number Of Records: "+ count);
		Assert.assertEquals(count, 10);
	}
	
	public void clickOnPreviousVersionData() throws Exception {
		btnHelper.click(previousVersionLinkList);
	}
	
	public void verifyNoSaveBtnInPreviousVersionScreen() throws Exception {
		boolean actual = procedureTextField.getAttribute("disabled")!= null;
		Assert.assertEquals(actual, true);
	}
	
	public void verifyBackBtnOfPreviousVersionIsFunctional() throws Exception {
		btnHelper.click(backBtnPreviousVersion);
		boolean actual = addnewBtn.isDisplayed();
		Assert.assertEquals(actual, true);
	}
	
	public void clickViewLinkedInformationLink() throws Exception {
		btnHelper.click(linkedInformation);
	}
	
	public void verifyLinkedInformationPopupAppsHeading() throws Exception {
		Assert.assertEquals(linkedInfoPopupHeading.getText(), "Your current procedure is used in these Apps:");
		browserHelper.refresh();
	}
	
	public void verifyAddEditProcedureScreenForCurrentVersion() throws Exception {
		boolean actual = currentVersion.isDisplayed();
		Assert.assertEquals(actual, true);
	}
	
	public void verifyAllQuestionsFieldTitle() throws Exception {
		SoftAssert sf = new SoftAssert();
		boolean tag = filterQuestionTagLbl.isDisplayed();
		boolean selection = selectQuestionLbl.isDisplayed();
		sf.assertEquals(tag, true);
		sf.assertEquals(selection, true);
		sf.assertAll();
	}
	
	public void verifyCopyIconTooltipMsg() throws Exception {
		Assert.assertEquals(helper.getToolTipText(copyIconTooltip), "Create Copy");
	}
	
	public String getFirstProcedureStatus() throws Exception {
		beforeStatus = getStatus.getText();
		return beforeStatus;
	}
	public String afterGetFirstProcedureStatus() throws Exception {
		return getStatus.getText();
	}
	
	public void clickFirstProcedureLink() throws Exception {
		btnHelper.click(selectFirstProcedure);
	}
	
	public void clickStatusToggleBtn() throws Exception {
		btnHelper.click(statusToggleBtn);
	}
	
	public void verifyTheStatusToggleBtnFunctionality() throws Exception {
		waitObj.waitForElementVisible(getStatus);
		boolean actual;
		browserHelper.refresh();
		if(beforeStatus.equals(afterGetFirstProcedureStatus())) {
			actual =  false;
		}
		else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void verifyAcceptanceCriteriaBtnFunctionality() throws Exception {
		boolean actual = acceptanceCriteriaScreen.isDisplayed();
		Assert.assertEquals(actual, true);
	}
	
	public void clickAdvancedAcceptanceCriteriaRadioBtn() throws Exception {
		btnHelper.click(advancedAcceptanceCriteria);
	}
	
	public void clickAddConditionIconAdvancedAcceptanceCriteria() throws Exception {
		btnHelper.click(addConditionAdvancedAC);
	}
	
	public void clickQuestionDrpDwnAdvancedAcceptanceCriteria() throws Exception {
		waitObj.waitForElementVisible(questionDrpDwn);
	}
	
	public void selectQuestionDrpDwnAdvancedAcceptanceCriteria() throws Exception {
		drpHelper.selectDropDownIndex(questionDrpDwn, 1);
	}
	
	public void selectOperatorDrpDwnAdvancedAcceptanceCriteria() throws Exception {
		drpHelper.selectDropDownIndex(operatorDrpDwn, 1);
	}
	
	public void valueTextFieldAdvancedAcceptanceCriteria() throws Exception {
//		textboxHelper.sendKeys(valueTextField, "12345");
		drpHelper.selectDropDownIndex(valueTextField, 1);
	}
	
	public void clickOnAddBtnAdvancedAcceptanceCriteria() throws Exception {
		btnHelper.click(addBtnAdvancedAC);
	}
	
	public void verifyAdvancedAcceptanceCriteriaFunctionality() throws Exception {
		boolean actual = verifyACAdded.isDisplayed();
		Assert.assertEquals(actual, true);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}