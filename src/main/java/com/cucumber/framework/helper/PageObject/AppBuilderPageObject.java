package com.cucumber.framework.helper.PageObject;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Browser.BrowserHelper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.CheckBox.CheckBoxOrRadioButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Javascript.JavaScriptHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.PageObject.PageBase;
import com.cucumber.framework.helper.PageObject.RolesAndPermissionsPageObject;
import com.cucumber.framework.helper.Pagination.Pagination;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;
import com.google.common.base.Strings;

public class AppBuilderPageObject extends PageBase {

	private WebDriver driver;
	public ButtonHelper btnHelper;
	ArrayList<String> masterAppName = new ArrayList<>();
	private final Logger log = LoggerHelper.getLogger(AppBuilderPageObject.class);
	public Helper helper = new Helper();
	public static String getAppTitle=null, getPublishVersion;
	public Pagination paginationHelper = new Pagination();
	public static String getAttribute, getProcedureName, getAppSelected, getNoRecordsAppName, getCopyAppNameAfter, getCopyAppNameBefore;
	public DropDownHelper drpdwn = new DropDownHelper(ObjectRepo.driver);
	public static String entityName, selectEntityValue = "Anti-D Antisera";
	public String activeInactiveStatus, failedAttempsStatus, currentTimeAndDate, createdBy;
	public String newFirstName, userName,afterMiddleName, lastName, departmentName, authentication, emailName, rolesName, passwordMethod;
	private String paginationVal=null, PaginationSelectedVal=null;
	public Scroll scroll = new Scroll();
	public WaitHelper waitObj = new WaitHelper(ObjectRepo.driver, ObjectRepo.reader);
	public BrowserHelper browserHelper = new BrowserHelper(ObjectRepo.driver);
	public static String procedureName;
	public CheckBoxOrRadioButtonHelper checkBoxRadio = new CheckBoxOrRadioButtonHelper(ObjectRepo.driver);
	public TextBoxHelper txtboxHelper = new TextBoxHelper(ObjectRepo.driver);
	public DropDownHelper drpdwnHelper = new DropDownHelper(ObjectRepo.driver);
	public JavaScriptHelper javaHelper = new JavaScriptHelper(ObjectRepo.driver);

	WebDriverWait wait =new WebDriverWait (ObjectRepo.driver,60);
	private TextBoxHelper textBoxHelper;

	public AppBuilderPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		waitObj = new WaitHelper(driver, ObjectRepo.reader);
		textBoxHelper = new TextBoxHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.schemas.list']")
	public WebElement appBuilderTile;
	
	@FindBy(how = How.XPATH, using = "//div[@class='paddingLeft-15px ng-binding']")
	public WebElement paginationShowing;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement pageTxtLabel;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.createduplicate(field,row)']")
	public WebElement copyAction;

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.schemas.edit.basic']")
	public WebElement addNewBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.gotoState(1)']")
	public WebElement basicTabHeader;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.gotoState(2)']")
	public WebElement attributeTabHeader;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.gotoState(3)']")
	public WebElement workflowTabHeader;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.gotoState(4)']")
	public WebElement acceptanceTabHeader;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.gotoState(5)']")
	public WebElement previewTabHeader;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.gotoState(6)']")
	public WebElement permissionsTabHeader;

	@FindBy(how = How.XPATH, using = "//*[@class='pull-left ng-binding']")
	public WebElement appDetailsLabel;

	@FindBy(how = How.XPATH, using = "//*[@class='pull-right qc-form-tab-attributs']")
	public WebElement currntVersion;

	@FindBy(how = How.XPATH, using = "//*[@name='qcModuleSelect']")
	public WebElement appMdul;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='(editSchemaForm.qcModuleSelect.$dirty || editSchemaForm.qcModuleSelect.$touched) && editSchemaForm.qcModuleSelect.$error.required']")
	public WebElement appMdulReqMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.saveAsDraft(editSchemaForm)']")
	public WebElement saveDraftApp;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.continue(editSchemaForm)']")
	public WebElement continueBasicBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.continue(editAttributeForm)']")
	public WebElement continueAttributeBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.ShowConfirm()']")
	public WebElement cancelBtn;

	@FindBy(how = How.XPATH, using = "//*[@style='display: block;']")
	public WebElement cancelAlertPopupMsg;

	@FindBy(how = How.XPATH, using = "//*[@class='cancel']")
	public WebElement cancelAlertPopupNoBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='confirm']")
	public WebElement cancelAlertPopupYesBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.title']")
	public WebElement appTitleTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.APPTITLER']")
	public WebElement appTitleTxtBoxReqMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.APPTITLE2']")
	public WebElement appTitleTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.APPTITLE50']")
	public WebElement appTitleTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.description']")
	public WebElement appDescptTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.APPDESCRIPTION2']")
	public WebElement appDescptTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.APPDESCRIPTION800']")
	public WebElement appDescptTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='instructionForUserRadioButtonURL']")
	public WebElement URLRadio;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.instructionURL']")
	public WebElement URLTextBox;

	@FindBy(how = How.XPATH, using = "//*[@name='instructionForUserRadioButtonUploadHelpDocument']")
	public WebElement UploadDocumentRadio;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.URLNOTVALID']")
	public WebElement invalidURLValMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='!editSchemaForm.qcUploader.$error.validation']")
	public WebElement removeURLValMsg;

	@FindBy(how = How.XPATH, using = ".//div[3]/div/div[7]/div/input")
	public WebElement sendEmailChkbox;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.emailIdForQCFormFail']")
	public WebElement sendEmailTxtbox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.EMAILADDMUL']")
	public WebElement emailInfoMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.threshold.edit.EMAILONE']")
	public WebElement sendEmailReqMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.EMAILADDI']")
	public WebElement sendEmailInvalidMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.yesSelected']")
	public WebElement siteSelectionYesInfoMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.noSelected']")
	public WebElement siteSelectionNoInfoMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.siteYesNo']")
	public WebElement SiteSelectionDrp;

	@FindBy(how = How.XPATH, using = "//*[@ng-repeat='(key, value) in vm.siteValidationMsg']")
	public WebElement siteSelectionReqMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.headMsgPrint']")
	public WebElement defaultHeaderMsgTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.headMsgPrintForPass']")
	public WebElement passedHeaderMsgTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.headMsgPrintForFail']")
	public WebElement failedHeaderMsgTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.HEADERMESSAGEFORPRINT2']")
	public WebElement defaultHeaderMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.basic.HEADERMESSAGEFORPRINT200']")
	public WebElement defaultHeaderMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.HEADERMSGFORPRINTWHENSTATUSISPASSED']")
	public WebElement passedHeaderMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.HEADERMSGNOTMORETHAN200']")
	public WebElement passedHeaderMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.HEADERMESSAGEFORPRINTFAILED2CHAR']")
	public WebElement failedHeaderMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.HEADERMSGFORPINTFAILEDNOT200']")
	public WebElement failedHeaderMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schema.setreviewworkflow']")
	public WebElement reviewWorkflowChkBox;

	@FindBy(how = How.XPATH, using = "//*[@class='breadcrumb breadcrumb-1']//following::p")
	public WebElement attributeInfoMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.attribute']")
	public WebElement attributeDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.add(vm.attribute)']")
	public WebElement addAttributeBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.removeAttribute(attr._id,$index)']")
	public WebElement attributeRemoveBtn;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.workflow.procedure.ENTITYWORKFLOW']")
	public WebElement entityWorkflowLable;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.workflow.procedure.PROCEDUREWORKFLOW']")
	public WebElement procedureWorkflowLable;

	@FindBy(how = How.XPATH, using = "//*[@class='Set-Entity-Rule-font-size']")
	public WebElement applyEntityFilterMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.addEntity(vm.selectedEntity)']")
	public WebElement addEntityBtn;

	@FindBy(how = How.XPATH, using = "//*[@placeholder='Search and select Entity']")
	public WebElement SelectEntityTextbox;

	@FindBy(how = How.XPATH, using = "(//*[@ng-click='selectMatch($index, $event)'])[1]")
	public WebElement SelectEntityAutocomplete;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-default ng-binding' and contains(text(),'Filter')]")
	public WebElement entityFilterBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-default ng-binding' and contains(text(),'Add Rule')]")
	public WebElement entityAddRuleBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='Set-Entity-Rule-font-size']")
	public WebElement applyEntityFilterLabel;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schema.edit.workflow.setrule.DEFINERULE']")
	public WebElement defineRuleLabel;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='showme=true; vm.setindex = -1']")
	public WebElement showLink;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='showme=false; vm.setindex = -1']")
	public WebElement hideLink;

	//	=================================== Chirag Kariya ===================================

	@FindBy(how = How.XPATH, using = "//*[@placeholder='Search and select Procedure']")
	public WebElement selectProcedureTextbox;

	@FindBy(how = How.XPATH, using = "//a[@ng-attr-title='{{match.label}}']")
	public WebElement selectProcedureAutocomplete;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.continue(schemaProcedureForm)']")
	public WebElement continueBtnWorkflowTab;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.continue()']")
	public WebElement continueBtnAcceptanceTab;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.continue()']")
	public WebElement publishContinueBtnPreviewTab;

	@FindBy(how = How.XPATH, using = "//div[@class='col-md-8 col-sm-9 preview-tab-align-left']")
	public WebElement appName;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save()']")
	public WebElement saveBtnPermissionTab;

	@FindBy(how = How.XPATH, using = "//select[@name='qcProcedureSelect0']")
	public WebElement selectProcedureAcceptance;

	@FindBy(how = How.XPATH, using = "//button[@title='Add selected procedure']")
	public WebElement addProcedureAcceptanceBtn;

	@FindBy(how = How.XPATH, using = "//tr[1]/td[1]/span[1]/span/a")
	public WebElement verifyNewApp;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addEntity(vm.selectedEntity)']")
	public WebElement addEntityBtnWorkflow;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addProcedure(vm.selectedProcedure)']")
	public WebElement addProcedureBtnWorkflow;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='item.editSchema']")
	public WebElement clickBuildAppCheckbox;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='item.review']")
	public WebElement clickOtherCheckbox;

	@FindBy(how = How.XPATH, using = "//div[@class='col-md-12 overflow-ipad-grid']")
	public WebElement appListingsScreen;

	@FindBy(how = How.XPATH, using = "//div[@class='pull-left ng-binding']/b")
	public WebElement appNamePermissionTab;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.schema.edit.permission.DEFINEPER']")
	public WebElement definePermissionApp;

	@FindBy(how = How.XPATH, using = "//div[@class='col-md-4 col-sm-3 preview-tab-align-rt']")
	public WebElement currentVersionPermissionTab;

	@FindBy(how = How.XPATH, using = "//div[@class='col-md-8 col-sm-9 preview-tab-align-left']")
	public WebElement appNamePreviewTab;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.gotoState(6)']")
	public WebElement permissionTabNavigation;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.gotoState(5)']")
	public WebElement previewTabNavigation;

	@FindBy(how = How.XPATH, using = "//*[@class='disabled']")
	public WebElement nextTabDisabled;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.schema.edit.preview.PUBLISHEDAPP']")
	public WebElement previewTabInformationMsg;

	@FindBy(how = How.XPATH, using = "//i[@ng-click='vm.removeProcedure($parent.$index, $index, criteria)']")
	public WebElement removeProcedureAcceptanceTab;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.schema.edit.acceptance.TALL']")
	public WebElement keyAttributeMsgAcceptanceTab;

	@FindBy(how = How.XPATH, using = "//p[@translate='option.DEFINEPASSCRITE']")
	public WebElement definePassCriteriaMsgAcceptanceTab;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.schema.edit.acceptance.DEFINEAPPANDAPPPASS']")
	public WebElement addProcedureMsgAcceptanceTab;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.gotoState(4)']")
	public WebElement acceptanceTabNavigation;

	@FindBy(how = How.XPATH, using = "//div[@class='pull-left ng-binding']")
	public WebElement acceptanceTabLabel;

	@FindBy(how = How.XPATH, using = "//i[@class='ng-binding']")
	public WebElement acceptanceTabCurrentVersion;

	@FindBy(how = How.XPATH, using = "//p[@class='heightforpannel-schema-font ng-binding']")
	public WebElement trackUniqueRecordsMsg;

	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.go(field,row,vm.resource);'])[3]")
	public WebElement viewAduitTrail;

	@FindBy(how = How.XPATH, using = "(//*[@ng-hide='showme'])[3]")
	public WebElement showBtnOfProcedureClick;

	@FindBy(how = How.XPATH, using = "(//*[@class='btn btn-primary acceptance-criteria-btn-schema ng-binding'])[1]")
	public WebElement firstAcceptacneCriteria;

	@FindBy(how = How.XPATH, using = "//select[@name='condition0']")
	public WebElement conditionDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@name='value0']")
	public WebElement conditionVal;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.saveAC(acceptanceCriteriaForm)']")
	public WebElement saveBtnOfAccepCria;

	@FindBy(how = How.XPATH, using = "//input[@name='isReviewWorkflow']")
	public WebElement selectPeerReview;

	@FindBy(how = How.XPATH, using = "//input[@name='isAllowCreater']")
	public WebElement selectAllowCreator;

	@FindBy(how = How.XPATH, using = "//button[@ng-class='settings.buttonClasses']")
	public WebElement selectRoleDrpDwn;

	@FindBy(how = How.XPATH, using = "//a[@data-ng-click='selectAll()']")
	public WebElement clickCheckAllOption;

	@FindBy(how = How.XPATH, using = "//button[@ng-click=\"vm.addWorkflowUser(); vm.userSelectionType = ''\"]")
	public WebElement addWorkflowUser;

	@FindBy(how = How.XPATH, using = "//a[contains(text(), 'Home')]")
	public WebElement backToHome;

	@FindBy(how = How.XPATH, using = "//div/div/div[1]/span/div/p/span/button")
	public WebElement collectionDate;

	@FindBy(how = How.XPATH, using = "//button[@ng-click=\"select('today', $event)\"]")
	public WebElement collectionDateTodayOption;

	@FindBy(how = How.XPATH, using = "(//*[@type='text'])[4]")
	public WebElement enterDINNumber;

	@FindBy(how = How.XPATH, using = "(//*[@type='text'])[5]")
	public WebElement enterLotNumber;

	@FindBy(how = How.XPATH, using = "(//*[@type='text'])[11]")
	public WebElement enterFirstName;

	@FindBy(how = How.XPATH, using = "(//*[@type='number'])[2]")
	public WebElement enterPhoneNo;

	@FindBy(how = How.XPATH, using = "(//select[@class='form-control ng-pristine ng-valid ng-touched'])[1]")
	public WebElement selectWeakDDailyDrpDwn;

	@FindBy(how = How.XPATH, using = "(//select[@ng-model=\"vm.model[item['s#']][group['s#']]\"])[10]")
	public WebElement selectWeakDDailyDrpDwnClick;

	@FindBy(how = How.XPATH, using = "(//select[@ng-model=\"vm.model[item['s#']][group['s#']]\"])[1]")
	public WebElement selectInstrumentTypeDrpdwn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Enter one or multiple App names to search']")
	public WebElement searchAppInAdvSearch;

	@FindBy(how = How.XPATH, using = "//div[@ng-bind-html=\"(schema.module.title + ' > ' + schema.title) | highlight: $select.search\"]")
	public WebElement autoCompleteSearchedApp;

	@FindBy(how = How.XPATH, using = "(//a[@title='View'])[1]")
	public WebElement clickViewLinkOfApp;

	@FindBy(how = How.XPATH, using = "//div[@style='font-weight: bold;margin-bottom: 4px;font-size: 17px;']")
	public WebElement workflowLabel;

	@FindBy(how = How.XPATH, using = "(//button[@title='Click to search'])")
	public WebElement goBtnAdvSearch;

	@FindBy(how = How.XPATH, using = "//input[@name='markSetValueInActive2']")
	public WebElement clickMarkChkbox;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.textSearch']")
	public WebElement searchSets;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.go(field,row,vm.resource);']")
	public WebElement searchedAppNameLink;

	@FindBy(how = How.XPATH, using = "(//span[@class='wrap-site-title ng-binding ng-scope'])[1]")
	public WebElement moduleOfGrid;

	@FindBy(how = How.XPATH, using = "//a[@title='TM: Equipment Type']")
	public WebElement selectsearchedSet;

	@FindBy(how = How.XPATH, using = "//input[@name='defaltvaluecheckbox']")
	public WebElement getSetValue;

	@FindBy(how = How.XPATH, using = "//button[@class='confirm']")
	public WebElement acceptanceYesBtn;

	@FindBy(how = How.XPATH, using = "//a[@ui-sref=\"secure.admin.schemas.edit.workflow.entityfilter({entityId: ent._id,sequenceId: ent['s#']})\"]")
	public WebElement clickFilterEntity;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addToCriteria(vm.workflow.conditions)']")
	public WebElement clickAddFilterBtn;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addCondition()']")
	public WebElement clickAddCondition;

	@FindBy(how = How.XPATH, using = "//select[@ng-options='operand as operand.title for operand in vm.leftOperands track by operand.comboId']")
	public WebElement selectQuestion;

	@FindBy(how = How.XPATH, using = "//select[@ng-options='o as o for o in vm.getComparisonOperators(c.leftOperand)']")
	public WebElement selectOperator;

	@FindBy(how = How.XPATH, using = "//input[@class='form-control ng-isolate-scope ng-valid ng-valid-required ng-dirty ng-valid-parse ng-valid-validation-set ng-valid-key ng-touched']")
	public WebElement lotNumberTextbox;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.saveC()']")
	public WebElement saveBtnFilter;

	@FindBy(how = How.XPATH, using = "(//span[@ng-if=\"header.title !== 'App' && header.title !== 'Assignee' && header.title !== 'Master App' && header !== 'Activity Information' && header.title !== 'Record Status'\"])[4]")
	public WebElement verifyAppRecord;

	@FindBy(how = How.XPATH, using = "//a[@ui-sref=\"secure.admin.schemas.edit.workflow.setrule({entityId: ent._id,sequenceId: ent['s#']})\"]")
	public WebElement clickAddRuleBtn;

	@FindBy(how = How.XPATH, using = "//select[@default-value-options='vm.allowedQuestions']")
	public WebElement selectAttributeDrpDwn;

	@FindBy(how = How.XPATH, using = "//select[@name='operator0']")
	public WebElement selectOperatorDrpDwn;

	@FindBy(how = How.XPATH, using = "//button[@ng-class='settings.buttonClasses']")
	public WebElement clickLotNoDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[contains(text(), '14624')]")
	public WebElement selectLotNoDrpDwn;

	@FindBy(how = How.XPATH, using = "//select[@ng-change='vm.onChangeAction(vm.workflow.action)']")
	public WebElement selectActionDrpDwn;

	@FindBy(how = How.XPATH, using = "//textarea[@name='actionTextarea']")
	public WebElement enterAlertMsg;

	@FindBy(how = How.XPATH, using = "//button[@ng-show='vm.workflow.action && !vm.Active']")
	public WebElement addRule;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.saveC(editSetRuleForm)']")
	public WebElement saveBtnRule;

	@FindBy(how = How.XPATH, using = "//div[@class='sweet-alert showSweetAlert visible']/h2")
	public WebElement alertMsgPopup;

	@FindBy(how = How.XPATH, using = "//button[@class='confirm']")
	public WebElement alertMsgPopupOkBtn;

	@FindBy(how = How.XPATH, using = "//select[@name='procedureSelect']")
	public WebElement selectProcedureRule;

	@FindBy(how = How.XPATH, using = "(//div[@ng-if='!proc.isOptionalProcedure'])[2]")
	public WebElement verifyProcedureName;

	@FindBy(how = How.XPATH, using = "//div/a[@ng-hide='showme']")
	public WebElement clickShowLink;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='ent.getInactiveRecord']")
	public WebElement checkAllowInactiveRecord;

	@FindBy(how = How.XPATH, using = "//b[@class='ng-binding']")
	public WebElement inactiveRecordLabel;

	@FindBy(how = How.XPATH, using = "//input[@ng-change='vm.markEntityValueInActiveChanged($index)']")
	public WebElement inactiveRecordOnFail;

	@FindBy(how = How.XPATH, using = "//*[@type='search']")
	public WebElement entitySearchBox;
	
	@FindBy(how = How.XPATH, using = "//*[@class='ui-select-match']")
	public WebElement clickOnEntitySearchBox;

	@FindBy(how = How.XPATH, using = "//select[@name='attributeSelect0']")
	public WebElement clickFilterDrpDwn;

	@FindBy(how = How.XPATH, using = "//select[@name='attributeTextbox0']")
	public WebElement lotNumberValueDrpDwn;

	@FindBy(how = How.XPATH, using = "//a[@ng-click=\"vm.toggleStatusValue(val,'Activate')\"]")
	public WebElement statusEntityRecords;

	@FindBy(how = How.XPATH, using = "//input[@name='enableEntityRecord']")
	public WebElement enableEntityRecords;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[6]")
	public WebElement enableEntityInApp;

	@FindBy(how = How.XPATH, using = "//a[@id='workflow-tab-condition-btn']")
	public WebElement addRulesProcedure;

	@FindBy(how = How.XPATH, using = "//select[@name='questionSelect0']")
	public WebElement questionDrpDwn;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='item.value']")
	public WebElement enterValue;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.workflow.selctedType']")
	public WebElement selectAction;

	@FindBy(how = How.XPATH, using = "//select[@name='procedureSelect']")
	public WebElement selectProcedure;

	@FindBy(how = How.XPATH, using = "//button[@ng-click=\"vm.addCondition('addRuleInactiveID')\"]")
	public WebElement addRulesOfRules;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.saveC()']")
	public WebElement saveBtnOfRules;

	@FindBy(how = How.XPATH, using = "//select[@name='qcProcedureSelect0']")
	public WebElement selectDefinePass;

	@FindBy(how = How.XPATH, using = "(//div[@ng-if='!proc.isOptionalProcedure'])[3]")
	public WebElement initiatedProcedure;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.workflow.moduleForApp']")
	public WebElement selectModuleDrpDwn;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.workflow.appToExpand']")
	public WebElement selectAppDrpDwn;

	@FindBy(how = How.XPATH, using = "(//select[@default-value-model=\"vm.workflow.appmapping.maapingfield[$index]['s#']\"])[1]")
	public WebElement selectCollectionDate;

	@FindBy(how = How.XPATH, using = "(//select[@default-value-model=\"vm.workflow.appmapping.maapingfield[$index]['s#']\"])[2]")
	public WebElement selectDIN;

	@FindBy(how = How.XPATH, using = "(//select[@default-value-model=\"vm.workflow.appmapping.maapingfield[$index]['s#']\"])[3]")
	public WebElement selectReagentMaster;

	@FindBy(how = How.XPATH, using = "//button[@ng-click=\"vm.addCondition('addRuleID')\"]")
	public WebElement addRulesOfInitApp;

	@FindBy(how = How.XPATH, using = "//div[@class='col-md-8 col-sm-6 col-xs-12 qcFormTitle']")
	public WebElement getInitAppName;

	@FindBy(how = How.XPATH, using = "//label[@ng-if='vm.parentfields.length > 0']")
	public WebElement initAppScreen;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.workflow.updateEntity']")
	public WebElement entityDrpDwn;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.workflow.updateEntityType']")
	public WebElement entityActionDrpDwn;

	@FindBy(how = How.XPATH, using = "(//select[@default-value-model=\"vm.workflow.entityRecordMapping[vm.workflow.updateEntity['s#'] + '-' + question['s#']].mappedValue\"])[1]")
	public WebElement entityMapDrpDwn;

	@FindBy(how = How.XPATH, using = "//button[@ng-click=\"vm.addCondition('addRuleUpdateID')\"]")
	public WebElement addRulesOfModifyEntity;

	@FindBy(how = How.XPATH, using = "//select[@name='attributeSelect0']")
	public WebElement selectEntityFilter;

	@FindBy(how = How.XPATH, using = "//select[@name='attributeTextbox0']")
	public WebElement selectEntityValueFilter;

	@FindBy(how = How.XPATH, using = "(//span[@ng-if=\"qus.type.format.title !== 'Date' && qus.type.format.title !== 'Time' && qus.type.title !== 'File Attachment' && !qus.isKeyValue\"])[1]")
	public WebElement getReagentTypeValue;

	@FindBy(how = How.XPATH, using = "//a[@ng-click=\"vm.toggleStatusValue(val,'Activate')\"]")
	public WebElement activeEntityRecords;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='showme=true']")
	public WebElement showLinkProcedure;

	@FindBy(how = How.XPATH, using = "(//a[@class='btn btn-primary acceptance-criteria-btn-schema ng-binding'])[1]")
	public WebElement acceptanceCriteriaBtnProcedure;

	@FindBy(how = How.XPATH, using = "//label[@for='single']")
	public WebElement standardAcceptanceCriteria;

	@FindBy(how = How.XPATH, using = "//select[@name='condition0']")
	public WebElement conditionDrpDwnAcceptanceCriteria;

	@FindBy(how = How.XPATH, using = "//input[@name='value0']")
	public WebElement valueTextfieldAcceptanceCriteria;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.saveAC(acceptanceCriteriaForm)']")
	public WebElement saveBtnAcceptanceCriteria;

	@FindBy(how = How.XPATH, using = "//button[@class='confirm']")
	public WebElement entryIsCorrentBtn;

	@FindBy(how = How.XPATH, using = "//input[@name='advance0']")
	public WebElement advancedAcceptanceCriteriaRadioBtn;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addCondition()']")
	public WebElement addConditionAdvanced;

	@FindBy(how = How.XPATH, using = "//select[@class='form-control acceptancecriteria-mobile-mar pull-left Acceptance_Criteria_Please_Select_Question ng-pristine ng-untouched ng-valid ng-scope']")
	public WebElement selectQuestionAdvanced;

	@FindBy(how = How.XPATH, using = "//select[@class='form-control acceptancecriteria-mobile-mar margin-between pull-left ng-pristine ng-untouched ng-valid']")
	public WebElement selectOperatorAdvanced;

	@FindBy(how = How.XPATH, using = "//input[@class='form-control acceptancecriteria-mobile-mar margin-between ac-area-bx-top-mar ng-pristine ng-untouched ng-valid ng-scope ng-valid-maxlength']")
	public WebElement valueTextfieldAdvanced;

	@FindBy(how = How.XPATH, using = "(//button[@ng-click='vm.addToCriteria(vm.criteria[$index], $index)'])[1]")
	public WebElement addBtnAdvanced;

	@FindBy(how = How.XPATH, using = "//div[@class='form-group']/div[1]/input")
	public WebElement optionalProcedureCheckbox;

	@FindBy(how = How.XPATH, using = "//input[@name='optionalprocedure0']")
	public WebElement optionalProcedureMsgTextfield;

	@FindBy(how = How.XPATH, using = "//em[@ng-if='proc.isOptionalProcedure']")
	public WebElement optionalProcedureMsgInApp;

	@FindBy(how = How.XPATH, using = "(//input[@ng-model=\"vm.isUniqueKeyValue[item['s#']]\"])[2]")
	public WebElement keyAttributeCheckbox;

	@FindBy(how = How.XPATH, using = "(//div[@ng-if='!proc.isOptionalProcedure'])[2]")
	public WebElement defineCriteriaPassInAppEntry;

	@FindBy(how = How.XPATH, using = "(//a[@ng-click='vm.changeStatus(field,row,vm.resouce);'])[1]")
	public WebElement statusAppBuilder;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.saveAsDraft()']")
	public WebElement saveAsDraftBtn;

	@FindBy(how = How.XPATH, using = "(//td[@title='Publish Version'])[1]")
	public WebElement publishedVersion;

	@FindBy(how = How.XPATH, using = "(//span[@class='ng-binding ng-scope'])[1]")
	public WebElement versionAppBuilderVersion;

	@FindBy(how = How.XPATH, using = "//i[@class='ng-binding']")
	public WebElement versionAppBuilder;

	@FindBy(how = How.XPATH, using = "(//a[@ng-click='openModal(row._id,row.title,row.route);'])[1]")
	public WebElement viewLinkLinkedInformation;

	@FindBy(how = How.XPATH, using = "//h4[@class='col-sm-11 ng-binding']")
	public WebElement linkLinkedInformationPopup;

	@FindBy(how = How.XPATH, using = "//div[@class='col-md-10 col-md-offset-1 alert alert-warning no-records ng-binding']")
	public WebElement noRecordsFoundLinkedInformationPopup;
	
	@FindBy(how = How.XPATH, using = "(//a[@ng-bind-html='step.ncyBreadcrumbLabel'])[2]")
	public WebElement administrationBreadcrumbs;
	
	@FindBy(how = How.XPATH, using = "//input[@name='masterqcTextBox']")
	public WebElement masterAppTextField;
	
	@FindBy(how = How.XPATH, using = "//select[@name='qcModule']")
	public WebElement masterModuleDrpDwn;
	
	@FindBy(how = How.XPATH, using = "(//button[@ng-click='toggleDropdown()'])[2]")
	public WebElement masterAppDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click=\"vm.addForm(vm.masterqc.qcform,'true')\"]")
	public WebElement masterAppAddBtn;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save(editmasterqcForm)']")
	public WebElement masterAppSaveBtn;
	
	@FindBy(how = How.XPATH, using = "//td[@class='entityRecordTableOverflwo ng-scope ng-binding']")
	public WebElement linkedPopupMasterAppName;

	@FindBy(how = How.XPATH, using = "//*[@class='confirm']")
	public WebElement entryIsCorrectBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.masterqcsettings.edit']")
	public WebElement masterAppAddNewBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.save()']")
	public WebElement saveBtn;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-click='selectPage(page + 1, $event)'])[1]")
	public WebElement nextPagging;
	
	@FindBy(how = How.XPATH, using = "//i[@class='glyphicon glyphicon-remove pull-right critearia-01 ng-click-active']")
	public WebElement popupCrossIcon;
	
	@FindBy(how = How.XPATH, using = "//div[@ng-show='vm.options.totalItems > 10']/label")
	public WebElement pageSizeLabel;
	
	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.options.pageSize']")
	public WebElement pageSizeDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//img[@src='./assets/images/imgpsh_fullsize.png']")
	public WebElement actionIcon;
	
	@FindBy(how = How.XPATH, using = "//button[@class='confirm']")
	public WebElement okBtnActionIcon;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='App Title']")
	public WebElement copyPopUpTextField;
	
	@FindBy(how = How.XPATH, using = "(//a[@ng-class=\"{'linkDisabled': vm.resource === 'masterqcsettings' && !row.isReadPermission}\"])[1]")
	public WebElement firstAppTitle;
	
	@FindBy(how = How.XPATH, using = "//p[@style='display: block;']")
	public WebElement copyPopupMsg;
	
	@FindBy(how = How.XPATH, using = "//input[@name='headMsgPrintTextBox']")
	public WebElement headerMsgPrintField;
	
	@FindBy(how = How.XPATH, using = "//div[@class='sa-error-container show']/p")
	public WebElement copyPopupMandatoryMsg;
	
	@FindBy(how = How.XPATH, using = "//button[@class='cancel']")
	public WebElement copyPopupCancelBtn;
	
	@FindBy(how = How.XPATH, using = "//div[@class='sa-error-container show']/p")
	public WebElement copyPopupMinMsg;
	
	@FindBy(how = How.XPATH, using = "(//a[@title='View Audit Trail'])[1]")
	public WebElement firstAuditTrailLink;
	
	@FindBy(how = How.XPATH, using = "//*[@class='paddingLeft-15px ng-binding']")
	public WebElement recordsCount;
	
	@FindBy(how = How.XPATH, using = "//i[@class='header-username-bx ng-binding']")
	public WebElement usernameTab;
	
	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.myprofile']")
	public WebElement clickMyProfile;
	
	@FindBy(how = How.XPATH, using = "//input[@name='firstNameTextBox']")
	public WebElement firstnameSameAsLogin;

	@FindBy(how = How.XPATH, using = "//input[@name='lastNameTextBox']")
	public WebElement lastnameValForLoggedUser;

	@FindBy(how = How.XPATH, using = "//input[@name='departmentTextBox']")
	public WebElement departmentNameOfLoggedUser;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.saveAsDraft(editSchemaForm)']")
	public WebElement saveDraftBtn;
	
	@FindBy(how = How.XPATH, using = "(//span[@class='wrap-site-title ng-binding ng-scope'])[6]")
	public WebElement auditTrailVersion;
	
	@FindBy(how = How.XPATH, using = "(//a[@ng-click='vm.restoreStableVersion(row)'])[1]")
	public WebElement restoreLink;
	
	@FindBy(how = How.XPATH, using = "//span[@ng-switch-when='version']")
	public WebElement restoreLinkVersion;
	
	@FindBy(how = How.XPATH, using = "//p[@style='display: block;']")
	public WebElement restoreLinkInfoMsg;
	
	
	
	
	
	
	
	
	





	
	


	// --------------------public methods--------------------

	public WebDriver getDriver() {

		return this.driver;

	}

	public void appBuilderTileClick() throws Exception {
		waitObj.waitForElementVisible(appBuilderTile);
		btnHelper.click(appBuilderTile);
	}

	public void continueBasicBtnClick() throws Exception {
				btnHelper.click(continueBasicBtn);
		Thread.sleep(1000);
	}

	public void continueAttributeBtnClick() {
				btnHelper.click(continueAttributeBtn);
		new Scroll().scrollTillElem(pageTxtLabel);
	}

	public void verifyAppBuilderLabel() throws Exception {

		new Scroll().scrollUp(driver);
		Assert.assertEquals(textBoxHelper.getText(pageTxtLabel), ObjectRepo.reader.getPageTxtForAppBuildLabel());
	}

	public void copyActionClick() throws Exception {
		btnHelper.click(copyAction);
	}

	public void VerifyCopyPopup(String PopupTitle) throws Exception {

		if (ObjectRepo.driver
				.findElements(By.xpath("//*[@class='sweet-alert cross-button show-input showSweetAlert visible']"))
				.size() != 0) {
			String Title = ObjectRepo.driver.findElement(By.xpath("//*[contains(text(),'" + PopupTitle + "')]"))
					.getText();
			System.out.println("Title: " + Title + " and Popup Title:  " + PopupTitle);
			Assert.assertEquals(Title, PopupTitle);
		}
	}

	public void verifyCopyPopupDefaultValue() throws Exception {
		Thread.sleep(2000);
		String Expected = ObjectRepo.driver.findElement(By.xpath(".//table/tbody/tr[1]/td[1]")).getText() + ObjectRepo.reader.getCopyPopupDefaultValue();
		Thread.sleep(1000);
		ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.createduplicate(field,row)']")).click();
		Thread.sleep(1000);
		if (ObjectRepo.driver
				.findElements(By.xpath("//*[@class='sweet-alert cross-button show-input showSweetAlert visible']"))
				.size() != 0) {
			Thread.sleep(3000);

			WebElement txtFName = ObjectRepo.driver.findElement(By.xpath("//*[@tabindex='3' and @type='text']"));
			Actions a = new Actions(ObjectRepo.driver);
			a.sendKeys(txtFName, Keys.chord(Keys.CONTROL, "a")).perform();
			a.sendKeys(Keys.chord(Keys.CONTROL, "c")).perform();
			Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
			System.out.println(c.getData(DataFlavor.stringFlavor));
			String Actual = (String) c.getData(DataFlavor.stringFlavor);
			System.out.println("Expected: " + Expected + " and Actual:  " + Actual);

			Assert.assertEquals(true, true);
		}
	}

	public void verifyAddNewBtnColor() {
		String btnColor = new Helper().BtnColor(addNewBtn);

		Assert.assertEquals(btnColor, ObjectRepo.reader.getBlueColor());
	}

	public void addNewBtnClick() throws Exception {
		waitObj.waitForElementClickable(addNewBtn);
		btnHelper.click(addNewBtn);
		waitObj.waitForElementVisible(basicTabHeader);
	}

	public void verifyAddNewBtnFucn() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(pageTxtLabel), ObjectRepo.reader.getPageTxtLabelForBuildApp());
	}

	public void verifyBasicHeaderTabLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(basicTabHeader), ObjectRepo.reader.getBasicTabHeader());
	}
	public void verifyAttributeHeaderTabLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(attributeTabHeader), ObjectRepo.reader.getAttributeTabHeader());
	}
	public void verifyWorkflowHeaderTabLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(workflowTabHeader), ObjectRepo.reader.getWorkflowTabHeader());
	}
	public void verifyAcceptanceHeaderTabLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(acceptanceTabHeader), ObjectRepo.reader.getAcceptanceTabHeader());
	}
	public void verifyPreviewHeaderTabLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(previewTabHeader), ObjectRepo.reader.getPreviewTabHeader());
	}
	public void verifyPermissionsHeaderTabLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(permissionsTabHeader), ObjectRepo.reader.getPermissionsTabHeader());
	}


	public void verifyBasicHeaderTabColor() {
		String Color = new Helper().BtnColor(basicTabHeader);

		Assert.assertEquals(Color, ObjectRepo.reader.getNavigationColor());
	}
	public void verifyAttributeHeaderTabColor() {
		String Color =btnHelper.BtnColor(attributeTabHeader);

		Assert.assertEquals(Color, ObjectRepo.reader.getNavigationColor());
	}
	public void verifyWorkflowHeaderTabColor() {
		String Color = new Helper().BtnColor(workflowTabHeader);

		Assert.assertEquals(Color, ObjectRepo.reader.getNavigationColor());
	}
	public void verifyAppDetailsLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(appDetailsLabel), ObjectRepo.reader.getAppDetailsLabel());
	}

	public void verifyCurreVersionLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(currntVersion), ObjectRepo.reader.getCurrntVersion());
	}

	public void verifyCurreVersionColor() {
		String Color = new Helper().FontColor(currntVersion);

		Assert.assertEquals(Color, ObjectRepo.reader.getcurrntVersioncolor());
	}

	public void verifyBasicTabLables() throws Exception {

		String Expected = ObjectRepo.reader.getBasicTabLables();
		String Actual = null;
		List<WebElement> links = driver.findElements(By.xpath("//*[contains(@class,'breadcrumb')]/h4"));

		Iterator<WebElement> iter = links.iterator();

		while (iter.hasNext()) {
			WebElement we = iter.next();
			if (Strings.isNullOrEmpty(Actual)) {
				Actual = we.getText();
			} else {
				Actual = Actual + "\n" + we.getText();
			}
		}
		System.out.println(Actual);
		Assert.assertEquals(Actual, Expected);
	}

	public void verifyDisabledTab_Basic() throws Exception {

		String Expected = ObjectRepo.reader.getDisabledTabBasic();
		String Actual = null;
		List<WebElement> links = driver.findElements(By.xpath("//*[@class='disabled']"));

		Iterator<WebElement> iter = links.iterator();

		while (iter.hasNext()) {
			WebElement we = iter.next();
			if (Strings.isNullOrEmpty(Actual)) {
				Actual = we.getText();
			} else {
				Actual = Actual + "\n" + we.getText();
			}
		}

		System.out.println(Actual);
		Assert.assertEquals(Actual, Expected);
	}
	public void verifyDisabledTab_Attribute() throws Exception {

		String Expected = ObjectRepo.reader.getDisabledTabAttribute();
		String Actual = null;
		List<WebElement> links = driver.findElements(By.xpath("//*[@class='disabled']"));

		Iterator<WebElement> iter = links.iterator();

		while (iter.hasNext()) {
			WebElement we = iter.next();
			if (Strings.isNullOrEmpty(Actual)) {
				Actual = we.getText();
			} else {
				Actual = Actual + "\n" + we.getText();
			}
		}

		System.out.println(Actual);
		Assert.assertEquals(Actual, Expected);
	}
	public void verifyDisabledTab_Workflow() throws Exception {

		String Expected = ObjectRepo.reader.getDisabledTabWorkflow();
		String Actual = null;
		List<WebElement> links = driver.findElements(By.xpath("//*[@class='disabled']"));

		Iterator<WebElement> iter = links.iterator();

		while (iter.hasNext()) {
			WebElement we = iter.next();
			if (Strings.isNullOrEmpty(Actual)) {
				Actual = we.getText();
			} else {
				Actual = Actual + "\n" + we.getText();
			}
		}

		System.out.println(Actual);
		Assert.assertEquals(Actual, Expected);
	}
	public void verifyAppMdlReqMsg() {

		Assert.assertEquals(textBoxHelper.getText(appMdulReqMsg), ObjectRepo.reader.getAppMdulReqMsg());
	}

	public void verifySaveDraftAppColor() {
		String Color = new Helper().BtnColor(saveDraftApp);

		Assert.assertEquals(Color, ObjectRepo.reader.getBlueColor());
	}

	public void verifyContinueBtnColor() {
		String Color = new Helper().BtnColor(continueBasicBtn);

		Assert.assertEquals(Color, ObjectRepo.reader.getLightGreenColor());
	}

	public void verifyCancelBtnColor() {
		String Color = new Helper().BtnColor(cancelBtn);

		Assert.assertEquals(Color, ObjectRepo.reader.getDarkGreyColor());
	}

	public void CancelBtnClick() throws InterruptedException {
		cancelBtn.click();
		Thread.sleep(1000);
	}

	public void verifyAlrtBoxDisply() {

		if (driver.findElements(By.xpath("//*[@class='sweet-alert showSweetAlert visible']")).size() != 0) {
			Assert.assertEquals(true, true);
		} else {
			Assert.assertEquals(true, false);
		}
	}

	public void verifyCancelAlertPopUpMsg() {

		Assert.assertEquals(textBoxHelper.getText(cancelAlertPopupMsg), ObjectRepo.reader.getCancelAlertPopupMsg());
	}

	public void verifyCancelAlertboxNoBtnColor() {

		String Color = new Helper().BtnColor(cancelAlertPopupNoBtn);
		Assert.assertEquals(Color, ObjectRepo.reader.getRemoveMsgColor());
	}

	public void verifyCancelAlertboxYesBtnColor() {
		String Color = new Helper().BtnColor(cancelAlertPopupYesBtn);
		Assert.assertEquals(Color, ObjectRepo.reader.getLightGreenColor());
	}

	public void cancelAlertPopupNoBtnClick() throws InterruptedException {
		btnHelper.click(cancelAlertPopupNoBtn);
	}

	public void cancelAlertPopupYesBtnClick() throws InterruptedException {
		btnHelper.click(cancelAlertPopupYesBtn);
	}

	public void verifyCancelAlertPopupNoBtnClick() {

		if (driver.findElements(By.xpath("//*[@class='sweet-alert showSweetAlert visible']")).size() != 0) {
			Assert.assertEquals(false, true);
		} else {
			Assert.assertEquals(true, true);
		}
	}

	public void verifyCancelAlertPopupyesBtnClick() {

		Assert.assertEquals(pageTxtLabel.getText(), ObjectRepo.reader.getPageTxtForAppBuildLabel());
	}

	public void appTitleTxtBoxSendkeys(String inputValue) {
		textBoxHelper.sendKeys(appTitleTxtBox, inputValue);
	}

	public void verifyAppTitleTxtBoxReqMsg() {
		Assert.assertEquals(textBoxHelper.getText(appTitleTxtBoxReqMsg), ObjectRepo.reader.getAppTitleTxtBoxReqMsg());
	}

	public void verifyAppTitleTxtBoxReqMsgColor() {

		Assert.assertEquals(new Helper().FontColor(appTitleTxtBoxReqMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyAppTitleTxtBoxMinMsg() {
		Assert.assertEquals(textBoxHelper.getText(appTitleTxtBoxMinMsg), ObjectRepo.reader.getAppTitleTxtBoxMinMsg());
	}

	public void verifyAppTitleTxtBoxMinMsgColor() {

		Assert.assertEquals(new Helper().FontColor(appTitleTxtBoxMinMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyAppTitleTxtBoxMaxMsg() {
		Assert.assertEquals(textBoxHelper.getText(appTitleTxtBoxMaxMsg), ObjectRepo.reader.getAppTitleTxtBoxMaxMsg());
	}

	public void verifyAppTitleTxtBoxMaxMsgColor() {

		Assert.assertEquals(new Helper().FontColor(appTitleTxtBoxMaxMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyAppTitleTxtBoxNoMsg() {

		boolean reqMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.APPTITLER']")).size() == 0,

				MaxMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.APPTITLE2']"))
				.size() == 0,
				MinMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.APPTITLE50']"))
				.size() == 0,
				flag;

		if (reqMsg && MaxMsg && MinMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	// ----------------------------------------------------------------

	public void appDescptTxtBoxSendkeys(String inputValue) {
		textBoxHelper.sendKeys(appDescptTxtBox, inputValue);
	}

	public void verifyAppDescptTxtBoxMinMsg() {
		Assert.assertEquals(textBoxHelper.getText(appDescptTxtBoxMinMsg), ObjectRepo.reader.getAppDescptTxtBoxMinMsg());
	}

	public void verifyAppDescptTxtBoxMinMsgColor() {

		Assert.assertEquals(new Helper().FontColor(appDescptTxtBoxMinMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyAppDescptTxtBoxMaxMsg() {
		Assert.assertEquals(textBoxHelper.getText(appDescptTxtBoxMaxMsg), ObjectRepo.reader.getAppDescptTxtBoxMaxMsg());
	}

	public void verifyAppDescptTxtBoxMaxMsgColor() {

		Assert.assertEquals(new Helper().FontColor(appDescptTxtBoxMaxMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyAppDescptTxtBoxNoMsg() {

		boolean MaxMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.APPDESCRIPTION2']")).size() == 0,
				MinMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.APPDESCRIPTION800']"))
				.size() == 0,
				flag;

		if (MaxMsg && MinMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void URLRadioClick() throws InterruptedException {
		btnHelper.click(URLRadio);
	}

	public void UploadDocumentRadioClick() throws InterruptedException {
		btnHelper.click(UploadDocumentRadio);
	}

	public void URLTextBoxSendkeys(String inputValue) {
		textBoxHelper.sendKeys(URLTextBox, inputValue);

	}

	public void verifyURLTextBoxInvalidMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(textBoxHelper.getText(invalidURLValMsg), ObjectRepo.reader.getInvalidURLValMsg());
	}

	public void verifyURLTextBoxRemoveMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(textBoxHelper.getText(removeURLValMsg), ObjectRepo.reader.getRemoveURLValMsg());

	}

	public void verifyURLTextBoxInvalidMsgColor() {
		new Scroll().scrollTillElem(URLRadio);

		Assert.assertEquals(new Helper().FontColor(invalidURLValMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyURLTextBoxNoMsg() {

		boolean invalidMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.URLNOTVALID']")).size() == 0,
				flag;

		if (invalidMsg) {
			flag = true;
		} else {
			flag = false;
		}
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(flag, true);
	}

	public void sendEmailChkboxClick() throws Exception {

		for (int i = 0; i < 10; i++) {
			if (sendEmailChkbox.isSelected()) {
				System.err.println("isSelected...");
				break;
			} else {
				System.err.println("not selected ......");
				sendEmailChkbox.click();
			}
		}
		Thread.sleep(2000);
	}

	public void sendEmailChkboxClickUnchk() throws Exception {

		for (int i = 0; i < 10; i++) {
			if (sendEmailChkbox.isSelected()) {
				System.err.println("isSelected...");
				sendEmailChkbox.click();
			} else {
				System.err.println("not selected ......");
				break;
			}
		}
		Thread.sleep(2000);
	}

	public void verifySendEmailTxtBoxAppear() {

		boolean emailTxtBox = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-model='vm.schema.emailIdForQCFormFail']")).size() != 0;

		if (emailTxtBox) {
		} else {
		}
		new Scroll().scrollTillElem(URLRadio);

	}

	public void verifySendEmailTxtBoxDisappear() {

		boolean emailTxtBox = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-model='vm.schema.emailIdForQCFormFail']")).size() != 0, flag;

		if (emailTxtBox) {
			flag = true;
		} else {
			flag = false;
		}
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(flag, false);
	}

	public void verifySendEmailInfoMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(textBoxHelper.getText(emailInfoMsg), ObjectRepo.reader.getEmailInfoMsg());
	}

	public void verifySendEmailInfoMsgColor() {
		new Scroll().scrollTillElem(URLRadio);

		Assert.assertEquals(new Helper().FontColor(emailInfoMsg), ObjectRepo.reader.getEmailInfoColor());
	}

	public void sendEmailTxtboxSendkeys(String inputValue) {
		sendEmailTxtbox.sendKeys(inputValue);
	}

	public void verifySendEmailReqMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(textBoxHelper.getText(sendEmailReqMsg), ObjectRepo.reader.getSendEmailReqMsg());
	}

	public void verifySendEmailInvalidMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(textBoxHelper.getText(sendEmailInvalidMsg), ObjectRepo.reader.getInvalidEmailValMsg());
	}

	public void verifySendEmailNoMsg() {

		boolean ReqMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.threshold.edit.EMAILONE']"))
				.size() == 0,
				InvalidMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.EMAILADDI']"))
				.size() == 0,
				flag;

		if (ReqMsg && InvalidMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void verifySiteSelectionYesInfoMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(textBoxHelper.getText(siteSelectionYesInfoMsg),
				ObjectRepo.reader.getSiteSelectionYesInfoMsg());
	}

	public void verifySiteSelectionNoInfoMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(textBoxHelper.getText(siteSelectionNoInfoMsg),
				ObjectRepo.reader.getSiteSelectionNoInfoMsg());
	}

	public void SelectionNoOptiClick() {
		new DropDownHelper(driver).selectDropDownText(SiteSelectionDrp, ObjectRepo.reader.getSiteSelectionNoMsg());
	}

	public void verifySiteSelectionYesInfoMsgColor() {
		new Scroll().scrollTillElem(URLRadio);

		Assert.assertEquals(new Helper().FontColor(siteSelectionYesInfoMsg), ObjectRepo.reader.getEmailInfoColor());
	}

	public void verifySiteSelectionNoInfoMsgColor() {
		new Scroll().scrollTillElem(URLRadio);

		Assert.assertEquals(new Helper().FontColor(siteSelectionNoInfoMsg), ObjectRepo.reader.getEmailInfoColor());
	}

	public void verifySiteSelectionReqMsg() {
		new Scroll().scrollTillElem(URLRadio);
		Assert.assertEquals(textBoxHelper.getText(siteSelectionReqMsg), ObjectRepo.reader.getSiteSelectionReqMsg());
	}

	public void verifySiteSelectionReqMsgColor() {
		new Scroll().scrollTillElem(URLRadio);

		Assert.assertEquals(new Helper().FontColor(siteSelectionReqMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void defaultHeaderMsgTxtBoxSendkeys(String inputValue) {
		textBoxHelper.sendKeys(defaultHeaderMsgTxtBox, inputValue);
	}

	public void passedHeaderMsgTxtBoxSendkeys(String inputValue) {
		textBoxHelper.sendKeys(passedHeaderMsgTxtBox, inputValue);
	}

	public void failedHeaderMsgTxtBoxSendkeys(String inputValue) {
		textBoxHelper.sendKeys(failedHeaderMsgTxtBox, inputValue);
	}

	public void verifyDefaultHeaderMinMsg() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);
		Assert.assertEquals(textBoxHelper.getText(defaultHeaderMinMsg), ObjectRepo.reader.getDefaultHeaderMinMsg());
	}

	public void verifydefaultHeaderMinMsgColor() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);

		Assert.assertEquals(new Helper().FontColor(defaultHeaderMinMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyDefaultHeaderMaxMsg() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);
		Assert.assertEquals(textBoxHelper.getText(defaultHeaderMaxMsg),
				ObjectRepo.reader.getDefaultHeaderMaxMsg());
	}

	public void verifyDefaultHeaderMaxMsgColor() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);

		Assert.assertEquals(new Helper().FontColor(defaultHeaderMaxMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyDefaultHeaderNoMsg() {

		boolean minMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.schema.edit.basic.HEADERMESSAGEFORPRINT2']"))
				.size() == 0,
				maxMsg = ObjectRepo.driver
				.findElements(
						By.xpath("//*[@translate='secure.admin.schema.edit.basic.HEADERMESSAGEFORPRINT200']"))
				.size() == 0,
				flag;

		if (minMsg && maxMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void verifyPassedHeaderMinMsg() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);
		Assert.assertEquals(textBoxHelper.getText(passedHeaderMinMsg),
				ObjectRepo.reader.getPassedHeaderMinMsg());
	}

	public void verifyPassedHeaderMinMsgColor() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);

		Assert.assertEquals(new Helper().FontColor(passedHeaderMinMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyPassedHeaderMaxMsg() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);
		Assert.assertEquals(textBoxHelper.getText(passedHeaderMaxMsg),
				ObjectRepo.reader.getPassedHeaderMaxMsg());
	}

	public void verifyPassedHeaderMaxMsgColor() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);

		Assert.assertEquals(new Helper().FontColor(passedHeaderMaxMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyPassedHeaderNoMsg() {

		boolean minMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='text.HEADERMSGFORPRINTWHENSTATUSISPASSED']"))
				.size() == 0,
				maxMsg = ObjectRepo.driver
				.findElements(
						By.xpath("//*[@translate='text.HEADERMSGNOTMORETHAN200']"))
				.size() == 0,
				flag;

		if (minMsg && maxMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void verifyFailedHeaderMinMsg() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);
		Assert.assertEquals(textBoxHelper.getText(failedHeaderMinMsg),
				ObjectRepo.reader.getFailedHeaderMinMsg());
	}

	public void verifyFailedHeaderMinMsgColor() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);

		Assert.assertEquals(new Helper().FontColor(failedHeaderMinMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyFailedHeaderMaxMsg() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);
		Assert.assertEquals(textBoxHelper.getText(failedHeaderMaxMsg),
				ObjectRepo.reader.getFailedHeaderMaxMsg());
	}

	public void verifyFailedHeaderMaxMsgColor() {
		new Scroll().scrollTillElem(defaultHeaderMsgTxtBox);

		Assert.assertEquals(new Helper().FontColor(failedHeaderMaxMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyFailedHeaderNoMsg() {

		boolean minMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='text.HEADERMESSAGEFORPRINTFAILED2CHAR']"))
				.size() == 0,
				maxMsg = ObjectRepo.driver
				.findElements(
						By.xpath("//*[@translate='text.HEADERMSGFORPINTFAILEDNOT200']"))
				.size() == 0,
				flag;

		if (minMsg && maxMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void reviewWorkflowChkBoxClick() {
		btnHelper.click(reviewWorkflowChkBox);
		new Scroll().scrollTillElem(reviewWorkflowChkBox);
	}

	public void verifyReviewWorkflowOPtiVisible() {
		boolean isDisplay = driver.findElements(By.xpath("//*[@selected-model='vm.rolesValue']"))
				.size() != 0;

		Assert.assertEquals(isDisplay, true);
	}

	public void verifyReviewWorkflowOPtiInvisible() {
		boolean isDisplay = driver.findElements(By.xpath("//*[@selected-model='vm.rolesValue']"))
				.size() != 0;

		Assert.assertEquals(isDisplay, false);
	}
	public static void SiteClick(WebDriver driver, String Site) {
		driver
		.findElement(
				By.xpath("//*[contains(text(),'" + Site + "')]//preceding-sibling::input[@type='checkbox']"))
		.click();
	}

	public void addBasicTabDetails(String mdlName, String appTitle, String SiteName) throws Exception {
		drpdwn.selectDropDownText(appMdul, mdlName);
		getAppTitle = appTitle +" - "+ new Helper().randomNumberGeneration();
		appTitleTxtBoxSendkeys(getAppTitle);
		String[] SplitSites = SiteName.split(">");
		SiteClick(driver, SplitSites[0]);
		SiteClick(driver, SplitSites[1]);
		SiteClick(driver, SplitSites[2]);
	}

	public void viewFirstAuditTrailClick() throws Exception {
		btnHelper.click(viewAduitTrail);
	}

	public void verifyAttributeDetailsLabel(String appTitle) throws Exception {
		Thread.sleep(1000);
		Assert.assertEquals(textBoxHelper.getText(appDetailsLabel), getAppTitle+" " + ObjectRepo.reader.getAppDetailsLabelForAttribute());
	}

	public void verifyAttributeInfoMsg() {
		Assert.assertEquals(textBoxHelper.getText(attributeInfoMsg),ObjectRepo.reader.getAttributeInfoMsg());
	}

	public void verifyAttributeInfoMsgColor() {

		Assert.assertEquals(new Helper().FontColor(attributeInfoMsg), ObjectRepo.reader.getEmailInfoColor());
	}

	public void verifyInitiallyAddAttribute() {

		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-click='vm.add(vm.attribute)' and @aria-disabled='false']"))
				.size() == 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void selectAttributeDrpDwn(String visibleValue) {
		new DropDownHelper(driver).selectDropDownText(attributeDrpDwn, visibleValue);
	}

	public void verifyAttributeAddBtnVisible() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-click='vm.add(vm.attribute)' and @aria-disabled='false']")).size() == 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, false);
	}	

	public void addAttributeBtnClick() throws Exception {
		btnHelper.click(addAttributeBtn);
	}
	public void attributeRemoveBtnClick() throws Exception {
		btnHelper.click(attributeRemoveBtn);
	}
	public void verifyAttributeRemoveBtnClickable() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-if='vm.schema.attributes.length > 0']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, false);
	}	
	public void verifyAttributeAddBtnClickable() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-if='vm.schema.attributes.length > 0']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}
	public void verifyAttributeAddBtnColor() {
		String Color = new Helper().BtnColor(addAttributeBtn);

		Assert.assertEquals(Color, ObjectRepo.reader.getBlueColor());
	}
	public static String firstvalue,secondvalue;

	public void addAttributeTabDetails() throws Exception {

		waitObj.waitForElementClickable(attributeDrpDwn);
		helper.SelectDrpDwnValueByIndex(attributeDrpDwn, 1); 
		 firstvalue = drpdwn.getFirstSelectedOption(attributeDrpDwn);
		addAttributeBtnClick();

		// wait.until(ExpectedConditions.elementToBeClickable(attributeDrpDwn));
		waitObj.waitForElementClickable(attributeDrpDwn);
		helper.SelectDrpDwnValueByIndex(attributeDrpDwn, 2); 
		secondvalue = drpdwn.getFirstSelectedOption(attributeDrpDwn);
		addAttributeBtnClick();
	}


	public void verifyWorkflowDetailsLabel(String appTitle) throws Exception {
		Assert.assertEquals(textBoxHelper.getText(appDetailsLabel), getAppTitle + " "+ ObjectRepo.reader.getAppDetailsLabelForWorkflow());
	}


	public void verifyEntityWorkflowLable() {

		Assert.assertEquals(textBoxHelper.getText(entityWorkflowLable),ObjectRepo.reader.getEntityWorkflowLable());
	}

	public void verifyProcedureWorkflowLable() {

		Assert.assertEquals(textBoxHelper.getText(procedureWorkflowLable),ObjectRepo.reader.getProcedureWorkflowLable());
	}

	public void verifyAddEntityInvisible() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-click='vm.addEntity(vm.selectedEntity)' and @aria-hidden='false']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, false);
	}

	public void verifyAddEntityVisible() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-click='vm.addEntity(vm.selectedEntity)' and @aria-hidden='false']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void addEntityBtnClick() throws Exception {
		btnHelper.click(addEntityBtn);
	}

	public void selectEntitySendkeys(String entName) throws Exception {
		textBoxHelper.sendKeys(SelectEntityTextbox, entName);
		Thread.sleep(500);
		btnHelper.click(SelectEntityAutocomplete);
		Thread.sleep(500);
	}

	public void verifyEntityAddBtnColor() {
		String Color = new Helper().BtnColor(addEntityBtn);

		Assert.assertEquals(Color, "#5bc0de");
	}

	public void verifyAddEntityBtnClickable() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-click='vm.removeAddedEntity($index,ent)']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}
	public void verifyAddProcedureBtnClickable() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-click='vm.removeAddedProcedure($index, pro)']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}
	public void entityFilterBtnClick() throws Exception {
		btnHelper.click(entityFilterBtn);
	}

	public void entityAddRuleBtnClick() throws Exception {
		btnHelper.click(entityAddRuleBtn);
	}

	public void verifyRedirectFilterPage() {

	}

	public void verifyApplyEntityFilterLabel() {

		Assert.assertEquals(textBoxHelper.getText(applyEntityFilterLabel),ObjectRepo.reader.getApplyEntityFilterLabel());
	}

	public void verifyDefineRuleLabelLabel() {

		Assert.assertEquals(textBoxHelper.getText(defineRuleLabel), ObjectRepo.reader.getDefineRuleLabel());
	}

	public void showLinkClick() throws Exception {
		showLink.click();
		Thread.sleep(1000);
	}
	public void hideLinkClick() throws Exception {
		hideLink.click();
		Thread.sleep(1000);
	}

	public void verifyShowLinkClickable() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//button[@class='glyphicon glyphicon-chevron-right hide-show-icon ng-hide']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void verifyHideLinkClickable() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//button[@class='glyphicon glyphicon-chevron-right hide-show-icon ng-hide']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, false);
	}

	public void verifyAddProcedureVisible() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@class='btn btn-info ng-binding']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void verifyAddProcedureInvisible() {
		boolean addBtnDisplay = ObjectRepo.driver
				.findElements(By.xpath("//*[@class='btn btn-info ng-binding']")).size() != 0,
				flag;

		if (addBtnDisplay) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, false);
	}
	//	=================================== Chirag Kariya ===================================
	
	public void enterAllRequiredDataWorkflow(String entity, String procedure) throws Throwable {
		Thread.sleep(500);
		driver.findElement(By.xpath(
				"//*[@class='form-group col-md-5 col-sm-9 select-menu-default-height select-menu-default-weight-typeahead']"))
				.click();
		SelectEntityTextbox.sendKeys(entity);
		Thread.sleep(1000);
		SelectEntityTextbox.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		driver.findElement(
				By.xpath("(//*[@ng-bind-html='match.label | uibTypeaheadHighlight:query'])[@title='" + entity + "']"))
				.click();
		btnHelper.click(SelectEntityAutocomplete);
		Thread.sleep(500);
		btnHelper.click(addEntityBtnWorkflow);
		Thread.sleep(500);
		addEntityBtnWorkflow.click();
		selectProcedureTextbox.clear();
		selectProcedureTextbox.sendKeys("a");
		selectProcedureTextbox.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(500);
		selectProcedureTextbox.sendKeys(procedure);
		Thread.sleep(500);
		selectProcedureTextbox.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(2000);
		// click from autocomplete
		for (int i = 1; i <= driver.findElements(By.xpath("(//*[@ng-attr-title='{{match.label}}'])")).size(); i++) {
			WebElement value = driver.findElement(By.xpath("(//*[@ng-attr-title='{{match.label}}'])[" + i + "]"));
			if (value.getText().equals(procedure)) {
				value.click();
				break;
			}
		}
		Thread.sleep(2000);
		addProcedureBtnWorkflow.click();
		Thread.sleep(2000);
	}

	public void selectProcedureInAutoComSendkeys(String proceName) throws InterruptedException {

		//		"DemoP_MJ"
		textBoxHelper.sendKeys(selectProcedureTextbox, proceName);
		Thread.sleep(500);
		btnHelper.click(selectProcedureAutocomplete);
		Thread.sleep(500);
	}

	public void clickContinueBtnWorkflowTab() throws Exception {
		btnHelper.click(continueBtnWorkflowTab);
	}

	public void clickContinueBtnAcceptanceTab() throws Exception {
		waitObj.waitForElementVisible(continueBtnAcceptanceTab);
		btnHelper.click(continueBtnAcceptanceTab);
	}

	public void getCurrentAppName(String appNameCurrent) throws Exception {
		waitObj.waitForElementVisible(appName);
		String currentAppName = appName.getText();
		System.err.println("Current App Name is: "+currentAppName);
	}

	public void clickPublishContinueBtnPreviewTab() throws Exception {
		waitObj.waitForElementClickable(publishContinueBtnPreviewTab);
		publishContinueBtnPreviewTab.click();
		Thread.sleep(1000);
	}
	
	public void verifySaveBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(saveBtn), ObjectRepo.reader.getLightGreenColor());
	}

	//change
	public void clickSaveBtnPermissionTab(String roleName) throws Exception {
		btnHelper.click(publishContinueBtnPreviewTab);
		wait.until(ExpectedConditions.elementToBeClickable(clickBuildAppCheckbox));
		new RolesAndPermissionsPageObject(driver).checkAppAllPermissions(roleName);
		btnHelper.click(saveBtnPermissionTab);
		wait.until(ExpectedConditions.visibilityOf(addNewBtn));
	}

	public void selectProcedureAcceptanceTab() throws Exception {
		waitObj.waitForElementClickable(addProcedureAcceptanceBtn);
		addProcedureAcceptanceBtn.click();
	}

	public void verifyAddNewAppFunctionality() throws Exception {
		SoftAssert SF = new SoftAssert();
		textBoxHelper.sendKeys(searchSets, getAppTitle);
		String appName = searchedAppNameLink.getText();
		String moduleName = moduleOfGrid.getText();
		waitObj.waitForElementVisible(versionAppBuilderVersion);
		String version = versionAppBuilderVersion.getText();
		btnHelper.click(searchedAppNameLink);
		String current = versionAppBuilder.getText();
		SF.assertEquals(appName, helper.getClipboardContents(appTitleTxtBox));
		SF.assertEquals(moduleName, helper.getDrpDwnFirstSelectedOptn(appMdul));
		//		The following line will convert 1 decimal value to 2 decimal value after (.)
		SF.assertEquals(version.trim(), String.format("%.2f", new BigDecimal(Double.parseDouble(current.split(" ")[2].trim()))));
		SF.assertAll();
	}

	public void verifySaveBtnNavigateAppListings() throws Exception {
		boolean actual = appListingsScreen.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyAppNameInPermissionTab() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(appNamePermissionTab), getAppTitle);
	}

	public void verifyDefinedPermissionForApp() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(definePermissionApp), ObjectRepo.reader.getDefinePermissionApp());
	}

	public void verifyDefinedPermissionForAppColor() throws Exception {
		Assert.assertEquals(helper.FontColor(definePermissionApp), ObjectRepo.reader.getEmailInfoColor());
	}

	public void verifyCurrentVersionOfPreviewTab() throws Exception {
		boolean actual = currentVersionPermissionTab.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyAppNameInPreviewTab() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(appNamePreviewTab), getAppTitle);
	}

	public void verifyNavigateToPermissionTab() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(appNamePreviewTab), getAppTitle);
	}

	public void verifyPermissionTabNavigation() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//p[@translate='secure.admin.schema.edit.permission.DEFINEPER']")).size() != 0)
		{
			actual = true;
		} else {
			actual = false;
		}

		Assert.assertEquals(actual, true);
	}

	public void verifyPermissionTabLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(permissionTabNavigation), ObjectRepo.reader.getPermissionTabNavigation());
	}

	public void verifyPermissionTabLabelColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(permissionTabNavigation), ObjectRepo.reader.getNavigationColor());
	}

	public void verifyPreviewTabLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(previewTabNavigation), ObjectRepo.reader.getPreviewTabNavigation());
	}

	public void verifyPreviewTabLabelColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(previewTabNavigation), ObjectRepo.reader.getNavigationColor());
	}

	public void verifyPermissionTabDisabled() throws Exception {
		boolean actual = nextTabDisabled.isEnabled();
		Assert.assertEquals(actual, true);
	}

	public void verifyPreviewTabInformationMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(previewTabInformationMsg), ObjectRepo.reader.getPreviewTabInformationMsg());
	}

	public void verifyPreviewTabInformationMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(previewTabInformationMsg), ObjectRepo.reader.getEmailInfoColor());
	}

	public void selectProcedureDrpDwnAcceptance() throws Exception {
		System.err.println("Nothing here");
	}

	public void clickRemoveProcedureAcceptanceTab() throws Exception {
		btnHelper.click(removeProcedureAcceptanceTab);
	}

	public void verifyRemoveProcedureAcceptanceTabFunctionality() throws Exception {
		Thread.sleep(1000);
		boolean actual;
		if(driver.findElements(By.xpath("//h4[@class='text-center ng-binding']")).size() != 0)
		{
			actual = false;
		} else {
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void verifyNavigateToPreviewTabFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//p[@translate='secure.admin.schema.edit.preview.PUBLISHEDAPP']")).size() != 0)
		{
			actual = true;
		} else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void verifyAddProcedureBtnVisibleAcceptanceTab() throws Exception {
		Thread.sleep(500);
		boolean actual = addProcedureAcceptanceBtn.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void verifyAddProcedureBtnFunctionalAcceptanceTab() throws Exception {
		Thread.sleep(500);
		boolean actual;
		if(driver.findElements(By.xpath("//h4[@class='text-center ng-binding']")).size() != 0)
		{
			actual = true;
		} else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void verifyAddProcedureBtnVisibleAcceptanceTabColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(addProcedureAcceptanceBtn), ObjectRepo.reader.getBlueColor());
	}

	public void verifyKeyAttributesInformationMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(keyAttributeMsgAcceptanceTab), ObjectRepo.reader.getKeyAttributeMsgAcceptanceTab());
	}

	public void verifyKeyAttributesInformationMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(keyAttributeMsgAcceptanceTab), ObjectRepo.reader.getEmailInfoColor());
	}

	public void verifyDefinePassCriteriaInformationMsgColor() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(definePassCriteriaMsgAcceptanceTab), ObjectRepo.reader.getDefinePassCriteriaMsgAcceptanceTab());
	}

	public void verifyAddProcedureInformationMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(addProcedureMsgAcceptanceTab), ObjectRepo.reader.getAddProcedureMsgAcceptanceTab());
	}

	public void verifyAddProcedureInformationMsgColor() throws Exception {
		Assert.assertEquals(helper.FontColor(addProcedureMsgAcceptanceTab), ObjectRepo.reader.getEmailInfoColor());
	}

	public void verifyAccpetanceCriteriaRedirection() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//p[@translate='secure.admin.schema.edit.acceptance.TALL']")).size() != 0)
		{
			actual = true;
		} else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void verifyAcceptanceTabLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(acceptanceTabNavigation), ObjectRepo.reader.getAcceptanceTabNavigation());
	}

	public void verifyAcceptanceTabLabelColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(acceptanceTabNavigation), ObjectRepo.reader.getNavigationColor());
	}

	public void verifyAcceptanceTabAppLabel() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(acceptanceTabLabel), getAppTitle+ " " + ObjectRepo.reader.getAcceptanceTabLabel());
	}

	public void verifyAcceptanceTabCurrentVersion() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(acceptanceTabCurrentVersion), ObjectRepo.reader.getCurrntVersion());
	}

	public void verifyAcceptanceTabCurrentVersionColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(acceptanceTabNavigation), ObjectRepo.reader.getNavigationColor());
	}

	public void verifyPreviewTabDisabled() throws Exception {
		boolean actual = nextTabDisabled.isEnabled();
		Assert.assertEquals(actual, true);
	}

	public void verifyTrackUniqueRecordsMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(trackUniqueRecordsMsg), ObjectRepo.reader.getTrackUniqueRecordsMsg());
	}

	public void clickShowBtnOfProcedure() throws Exception {
		btnHelper.click(showBtnOfProcedureClick);
	}

	public void clickFirstAcceptanceCriteria() throws Exception {
		waitObj.waitForElementClickable(firstAcceptacneCriteria);
		firstAcceptacneCriteria.click();	
	}

	public void selectConditionDrpDwnVal() throws Exception {
		Thread.sleep(2000);
		helper.SelectDrpDwnValueByIndex(conditionDrpDwn, 1);	
	}

	public void entercConditionVal(String value) throws Exception {
		textBoxHelper.sendKeys(conditionVal, value);
	}

	public void clickSaveBtnAcceptanceCriteria() throws Exception {
		btnHelper.click(saveBtnOfAccepCria);
		wait.until(ExpectedConditions.elementToBeClickable(continueBtnWorkflowTab));

	}

	public void selectPeerReviewChkBox() throws Exception {
		checkBoxRadio.selectCheckBox(selectPeerReview);
	}

	public void selectAllowRecordCreatorToApproveApp() throws Exception {
		checkBoxRadio.selectCheckBox(selectAllowCreator);
	}

	public void clickSelectRoleDrpDwn() throws Exception {
		btnHelper.click(selectRoleDrpDwn);
		btnHelper.click(clickCheckAllOption);
	}

	public void clickAddBtnOfReviewWorkflow() throws Exception {
		btnHelper.click(addWorkflowUser);
	}

	public void clickBackToHome() throws Exception {
		btnHelper.click(backToHome);
	}

	public void enterDataInApp(String din, String lotNo, String firstName, String phoneNo) throws Exception {
		collectionDate.click();
		collectionDateTodayOption.click();
		Thread.sleep(500);
		enterDINNumber.sendKeys(din);
		Thread.sleep(500);
		enterLotNumber.sendKeys(lotNo);
		enterFirstName.sendKeys(firstName);
		enterPhoneNo.sendKeys(phoneNo);;
	}

	public void searchForTheApp() throws Exception {
		waitObj.waitForElementVisible(searchAppInAdvSearch);
		txtboxHelper.sendKeys(searchAppInAdvSearch, getAppTitle);
	}

	public void clickOnGoBtn() throws Exception {
		waitObj.waitForElementClickable(goBtnAdvSearch);
		btnHelper.click(goBtnAdvSearch);
	}

	public void clickSearchedApp() throws Exception {
		waitObj.waitForElementClickable(autoCompleteSearchedApp);
		btnHelper.click(autoCompleteSearchedApp);
	}

	public void clickViewLinkOfApp() throws Exception {
		waitObj.waitForElementClickable(clickViewLinkOfApp);
		btnHelper.click(clickViewLinkOfApp);
	}

	public void verifyESignatureTableIsVisible() throws Exception {
		waitObj.waitForElementVisible(workflowLabel);
		scroll.scrollTillElem(workflowLabel);
		Assert.assertEquals(workflowLabel.getText(), "Review Workflow");
	}

	public void selectAttributeAndCheckbox() throws Exception {
		waitObj.waitForElementClickable(attributeDrpDwn);
		helper.SelectDrpDwnValueByIndex(attributeDrpDwn, 1);	
		secondvalue = drpdwnHelper.getFirstSelectedOption(attributeDrpDwn);
		addAttributeBtnClick();
		waitObj.waitForElementVisible(clickMarkChkbox);
		btnHelper.click(clickMarkChkbox);
	}

	public void enterDataInAppAcceptanceFail(String din, String lotNo, String firstName, String phoneNo) throws Exception {
		collectionDate.click();
		collectionDateTodayOption.click();
		Thread.sleep(500);
		enterDINNumber.sendKeys(din);
		Thread.sleep(500);

		drpdwn.selectDropDownIndex(selectInstrumentTypeDrpdwn, 1);
		getAttribute = drpdwn.getFirstSelectedOption(selectInstrumentTypeDrpdwn);

		drpdwnHelper.selectDropDownIndex(selectInstrumentTypeDrpdwn, 1);
		getAttribute = drpdwnHelper.getFirstSelectedOption(selectInstrumentTypeDrpdwn);
		Thread.sleep(500);
		enterLotNumber.sendKeys(lotNo);
		enterFirstName.sendKeys(firstName);
		enterPhoneNo.sendKeys(phoneNo);;
	}

	public void searchForTheSets() throws Exception {
		waitObj.waitForElementVisible(searchSets);
		btnHelper.click(searchSets);
	}

	public void enterAttribute() throws Exception {
		txtboxHelper.sendKeys(searchSets, "Equipment Type");
	}

	public void selectTheSet() throws Exception {
		btnHelper.click(selectsearchedSet);
	}

	public void verifyStatusOfRecord() throws Exception {
		boolean actual=false;
		List<WebElement> setValueSize = driver.findElements(By.xpath("//input[@name='defaltvaluecheckbox']"));
		for(int i=2; i<=setValueSize.size(); i++)
		{
			String valueFound = driver.findElement(By.xpath("//div[1]/div[3]/div[2]/div/li["+i+"]/div[2]")).getText();
			if(getAttribute.equals(valueFound))
			{
				String statusFound = driver.findElement(By.xpath("//div[1]/div[3]/div[2]/div/li["+i+"]/div[3]")).getText();
				if("Inactive".equals(statusFound))
				{
					actual = true;
					break;
				}
				else {
					actual = false;
				}
			}
		}
		Assert.assertEquals(actual, true);
	}

	public void selectYesInAcceptancePopup() throws Exception {
		waitObj.waitForElementClickable(acceptanceYesBtn);
		btnHelper.click(acceptanceYesBtn);
	}

	public void clickFilterBtnOfEntity(String lotNo) throws Exception {
		btnHelper.click(clickFilterEntity);
		waitObj.waitForElementVisible(clickAddFilterBtn);
		btnHelper.click(clickAddCondition);
		drpdwnHelper.selectDropDownIndex(selectQuestion, 1);
		drpdwnHelper.selectDropDownIndex(selectOperator, 1);
		txtboxHelper.sendKeys(lotNumberTextbox, lotNo);
		btnHelper.click(clickAddFilterBtn);
		btnHelper.click(saveBtnFilter);
	}

	public void verifyAppRecordIsDisplay(String lotNo) throws Exception {
		Assert.assertEquals(verifyAppRecord.getText(), lotNo);
	}

	public void enterAllMandatoryField(String din, String lotNo, String firstName, String phoneNo) throws Exception {
		collectionDate.click();
		collectionDateTodayOption.click();
		Thread.sleep(1000);
		enterDINNumber.sendKeys(din);
		Thread.sleep(500);
		drpdwnHelper.selectDropDownIndex(selectInstrumentTypeDrpdwn, 1);
		getAttribute = drpdwnHelper.getFirstSelectedOption(selectInstrumentTypeDrpdwn);
		Thread.sleep(500);
		enterFirstName.sendKeys(firstName);
		waitObj.waitForElementVisible(enterPhoneNo);
		enterPhoneNo.sendKeys(phoneNo);
		enterLotNumber.sendKeys(lotNo);
		enterFirstName.sendKeys(firstName);
		enterPhoneNo.sendKeys(phoneNo);
	}

	public void clickAddRuleBtnOfWorkflow() throws Exception {
		btnHelper.click(clickAddRuleBtn);
		waitObj.waitForElementVisible(saveBtnRule);
	}

	public void selectAttributeAndOperator() throws Exception {
		waitObj.waitForElementVisible(saveBtnRule);
		drpdwnHelper.selectDropDownIndex(selectAttributeDrpDwn, 1);
		drpdwnHelper.selectDropDownIndex(selectOperatorDrpDwn, 1);
		btnHelper.click(clickLotNoDrpDwn);
		btnHelper.click(selectLotNoDrpDwn);
	}

	public void selectActionDrpDwn(String actionOption) throws Exception {
		drpdwnHelper.selectDropDownText(selectActionDrpDwn, actionOption);
	}

	public void enterAlertMessageInTextarea(String alertMsg) throws Exception {
		txtboxHelper.sendKeys(enterAlertMsg, alertMsg);
	}

	public void clickOnAddRuleBtnOfRuleScreen() throws Exception {
		btnHelper.click(addRule);
		btnHelper.click(saveBtnRule);
	}

	public void clickOnAddRuleSaveBtnOfRuleScreen(String din, String lotNo, String firstName, String phoneNo) throws Exception {
		collectionDate.click();
		collectionDateTodayOption.click();
		Thread.sleep(1000);
		enterDINNumber.sendKeys(din);
		Thread.sleep(500);
		drpdwnHelper.selectDropDownIndex(selectInstrumentTypeDrpdwn, 1);
		getAttribute = drpdwnHelper.getFirstSelectedOption(selectInstrumentTypeDrpdwn);
		Thread.sleep(500);
		enterFirstName.sendKeys(firstName);
		waitObj.waitForElementVisible(enterPhoneNo);
		enterPhoneNo.sendKeys(phoneNo);
		enterLotNumber.sendKeys(lotNo);
		enterLotNumber.sendKeys(Keys.TAB);
	}

	public void verifyAlertMsgOfPopup(String verifyMsg) throws Exception {
		waitObj.waitForElementVisible(alertMsgPopupOkBtn);
		String getAlertMsg = txtboxHelper.getText(alertMsgPopup);
		btnHelper.click(alertMsgPopupOkBtn);
		Assert.assertEquals(getAlertMsg, verifyMsg);
	}

	public void verifyLotNumberFieldNotClear(String lotNo) throws Exception {
		String getLotNoData = helper.getClipboardContents(lotNumberTextbox);
		Assert.assertEquals(lotNo, getLotNoData);
	}

	public void selectActionDrpDwnSecondOption(String actionOption) throws Exception {
		drpdwnHelper.selectDropDownText(selectActionDrpDwn, actionOption);
	}

	public void clickAlertPopup() throws Exception {
		waitObj.waitForElementVisible(alertMsgPopupOkBtn);
		txtboxHelper.getText(alertMsgPopup);
		btnHelper.click(alertMsgPopupOkBtn);
	}


	public void verifyLotNumberFieldClear() throws Exception {
		String found = textBoxHelper.getText(enterLotNumber);
		System.err.println(found);
		if(found.equals(""))
		{
			Assert.assertEquals(true, true);
		}
		else {
			Assert.assertEquals(true, false);
		}
	}

	public void selectActionDrpDwnThirdOption(String actionOption) throws Exception {
		drpdwnHelper.selectDropDownText(selectActionDrpDwn, actionOption);
	}

	public void selectProcedureInRule(String selectProcedure) throws Exception {
		drpdwnHelper.selectDropDownText(selectProcedureRule, selectProcedure);
	}

	public void enterAllMandatoryDataAndInitiateProcedure(String din, String lotNo, String firstName, String phoneNo) throws Exception {
		collectionDate.click();
		collectionDateTodayOption.click();
		Thread.sleep(1000);
		enterDINNumber.sendKeys(din);
		Thread.sleep(500);
		drpdwnHelper.selectDropDownIndex(selectInstrumentTypeDrpdwn, 1);
		getAttribute = drpdwnHelper.getFirstSelectedOption(selectInstrumentTypeDrpdwn);
		Thread.sleep(500);
		enterFirstName.sendKeys(firstName);
		waitObj.waitForElementVisible(enterPhoneNo);
		enterPhoneNo.sendKeys(phoneNo);
		enterLotNumber.sendKeys(lotNo);
		enterLotNumber.sendKeys(Keys.TAB);
	}

	public void verifyInitiatedProcedure(String procedureName) throws Exception {
		waitObj.waitForElementVisible(verifyProcedureName);
		Assert.assertEquals(verifyProcedureName.getText().trim(), procedureName + " " + ObjectRepo.reader.getVerifyProcedureName());
	}

	public void clickOnShowLinkOfEntity() throws Exception {
		waitObj.waitForElementClickable(clickShowLink);
		btnHelper.click(clickShowLink);
	}

	public void clickAllowInactiveRecordsCheckbox() throws Exception {
		waitObj.waitForElementClickable(checkAllowInactiveRecord);
		btnHelper.click(checkAllowInactiveRecord);
	}

	public void enterAllMandatoryDataAndInactiveEntity(String din, String lotNo, String firstName, String phoneNo) throws Exception {
		collectionDate.click();
		collectionDateTodayOption.click();
		Thread.sleep(1000);
		enterDINNumber.sendKeys(din);
		Thread.sleep(500);
		drpdwnHelper.selectDropDownIndex(selectInstrumentTypeDrpdwn, 1);
		getAttribute = drpdwnHelper.getFirstSelectedOption(selectInstrumentTypeDrpdwn);
		Thread.sleep(500);
		enterFirstName.sendKeys(firstName);
		waitObj.waitForElementVisible(enterPhoneNo);
		enterPhoneNo.sendKeys(phoneNo);
		enterLotNumber.sendKeys(lotNo);
		enterLotNumber.sendKeys(Keys.TAB);
	}

	public void verifyInactiveEntityLabel() throws Exception {
		waitObj.waitForElementClickable(inactiveRecordLabel);
		boolean actual = inactiveRecordLabel.isDisplayed();
		Assert.assertEquals(actual, true);
	}

	public void clickMarkEntityRecordAsInactiveWhenAppSubmissionStatusIsFailedCheckbox() throws Exception {
		waitObj.waitForElementClickable(inactiveRecordOnFail);
		btnHelper.click(inactiveRecordOnFail);
	}

	public void enterValidDataAndInactiveEntity(String din, String lotNo, String firstName, String phoneNo) throws Exception {
		collectionDate.click();
		collectionDateTodayOption.click();
		Thread.sleep(1000);
		enterDINNumber.sendKeys(din);
		Thread.sleep(500);
		drpdwnHelper.selectDropDownIndex(selectInstrumentTypeDrpdwn, 1);
		getAttribute = drpdwnHelper.getFirstSelectedOption(selectInstrumentTypeDrpdwn);
		Thread.sleep(500);
		textBoxHelper.sendKeys(enterLotNumber,lotNo);
		textBoxHelper.sendKeys(enterPhoneNo,phoneNo);
		textBoxHelper.sendKeys(enterFirstName,firstName);
		enterFirstName.sendKeys(Keys.TAB);
		btnHelper.click(entryIsCorrectBtn);
		Thread.sleep(1000);
		
	}

	public void enterEntityNameUsedInApp(String entityName) throws Exception {
		clickOnEntitySearchBox.click();
		Thread.sleep(500);
		txtboxHelper.sendKeys(entitySearchBox, entityName);
		entitySearchBox.sendKeys(Keys.TAB);
	}

	public void clickEntityRecordsFilterDrpdwn() throws Exception {
		waitObj.waitForElementVisible(clickFilterDrpDwn);
	}

	public void selectEntityAndValue(String Filter, String lotNo) throws Exception {
		drpdwnHelper.selectDropDownText(clickFilterDrpDwn, Filter);
		drpdwnHelper.selectDropDownText(lotNumberValueDrpDwn, lotNo);
	}

	public void verifyStatusOfEntityRecords() throws Exception {
		waitObj.waitForElementVisible(statusEntityRecords);
		boolean expected;
		String actual = statusEntityRecords.getText();
		if(actual.equals("Inactive"))
		{
			expected = true;
		}
		else {
			expected = false;
		}
		Assert.assertEquals(true, expected);
	}

	public void clickEnableEntityInEditModeCheckbox() throws Exception {
		btnHelper.click(enableEntityRecords);
	}

	public void verifyEnableEntityInEditModeFunctionality() throws Exception {
		boolean actual = enableEntityInApp.isEnabled();
		Assert.assertEquals(actual, true);
	}

	public void clickAddRuleBtnProcedure() throws Exception {
		btnHelper.click(addRulesProcedure);
	}

	public void selectQuestionFromSelectQuestionDrpDwn() throws Exception {
		drpdwnHelper.SelectUsingIndex(questionDrpDwn, 1);
	}

	public void selectValueFromSelectValueTextfield(String valueQuestion) throws Exception {
		textBoxHelper.sendKeys(enterValue, valueQuestion);
	}

	public void selectInitiateProcedureOption() throws Exception {
		drpdwnHelper.selectDropDownIndex(selectAction, 1);
	}

	public void selectProcedureFromSelectProcedure() throws Exception {
		drpdwnHelper.selectDropDownIndex(selectProcedure, 1);
		getProcedureName = drpdwnHelper.getFirstSelectedOption(selectProcedure);
	}

	public void clickAddRuleBtnOfAddRulesScreen() throws Exception {
		btnHelper.click(addRulesOfRules);
	}

	public void selectDefinePassCriteriaDrpDwn() throws Exception {
		waitObj.waitForElementVisible(selectDefinePass);
		drpdwnHelper.selectDropDownIndex(selectDefinePass, 1);
		addProcedureAcceptanceBtn.click();
	}

	public void clickSaveBtnOfAddRulesScreen() throws Exception {
		btnHelper.click(saveBtnOfRules);
	}

	public void verifyInitiatedProcedureName() throws Exception {
		waitObj.waitForElementVisible(initiatedProcedure);
		Assert.assertEquals(initiatedProcedure.getText(), getProcedureName);
	}

	public void selectInitiateAppOption(String selectApp) throws Exception {
		drpdwnHelper.selectDropDownIndex(selectAction, 2);
		drpdwnHelper.selectDropDownIndex(selectModuleDrpDwn, 1);
		drpdwnHelper.selectDropDownText(selectAppDrpDwn, selectApp);
		waitObj.waitForElementVisible(selectCollectionDate);
		drpdwnHelper.selectDropDownIndex(selectCollectionDate, 1);
		drpdwnHelper.selectDropDownIndex(selectDIN, 2);
		drpdwnHelper.selectDropDownIndex(selectReagentMaster, 4);
	}

	public void verifyInitiatedAppName(String selectApp) throws Exception {
		waitObj.waitForElementVisible(initAppScreen);
		Assert.assertEquals(selectApp, getInitAppName.getText());
	}

	public void clickSaveBtnOfAddRulesScreenForAppInit() throws Exception {
		btnHelper.click(addRulesOfInitApp);
	}

	public void selectUpdateEntityRecordsOption() throws Exception {
		drpdwnHelper.selectDropDownIndex(selectAction, 3);
	}

	public void selectEntityFromSelectEntityDrpDwn() throws Exception {
		drpdwnHelper.selectDropDownIndex(entityDrpDwn, 1);
	}

	public void selectEntityActionFromSelectActionDrpDwn() throws Exception {
		drpdwnHelper.selectDropDownIndex(entityActionDrpDwn, 1);
		drpdwnHelper.selectDropDownText(entityMapDrpDwn, selectEntityValue);
		btnHelper.click(addRulesOfModifyEntity);
	}

	public void verifyModifyAndUpdateEntityFunctionality() throws Exception {
		waitObj.waitForElementVisible(getReagentTypeValue);
		Assert.assertEquals(getReagentTypeValue.getText(), selectEntityValue);
	}

	public void selectActiveEntityFromSelectActionDrpDwn() throws Exception {
		drpdwnHelper.selectDropDownIndex(entityActionDrpDwn, 2);
		btnHelper.click(addRulesOfModifyEntity);
	}

	public void verifyActiveFunctionalityOfEntityRecords() throws Exception {
		waitObj.waitForElementVisible(statusEntityRecords);
		boolean expected;
		String actual = statusEntityRecords.getText();
		if(actual.equals("Active"))
		{
			expected = true;
		}
		else {
			expected = false;
		}
		Assert.assertEquals(true, expected);
	}

	public void selectDeactiveEntityFromSelectActionDrpDwn() throws Exception {
		drpdwnHelper.selectDropDownIndex(entityActionDrpDwn, 3);
		btnHelper.click(addRulesOfModifyEntity);
	}

	public void verifyDeactiveFunctionalityOfEntityRecords() throws Exception {
		waitObj.waitForElementVisible(activeEntityRecords);
		boolean expected;
		String actual = activeEntityRecords.getText();
		if(actual.equals("Inactive"))
		{
			expected = true;
		}
		else {
			expected = false;
		}
		scroll.scrollTillElem(activeEntityRecords);
		Assert.assertEquals(true, expected);
	}

	public void clickShowLinkOfProcedureInWorkflow() throws Exception {
		btnHelper.click(showLinkProcedure);
	}

	public void clickAcceptanceCriteriaBtnOfProcedureWorkflow() throws Exception {
		btnHelper.click(acceptanceCriteriaBtnProcedure);
	}

	public void clickAcceptanceCriteriaIconOfAddEditAcceptanceScreen() throws Exception {
		waitObj.waitForElementVisible(standardAcceptanceCriteria);
	}

	public void clickConditionDrpDwnOfAddEditAcceptanceScreen() throws Exception {
		drpdwnHelper.selectDropDownIndex(conditionDrpDwnAcceptanceCriteria, 1);
	}

	public void enterValueInTextfieldOfAddEditAcceptanceScreen(String valueAcceptanceCriteria) throws Exception {
		textBoxHelper.sendKeys(valueTextfieldAcceptanceCriteria, valueAcceptanceCriteria);
	}

	public void clickSaveBtnOfAddEditAcceptanceScreen() throws Exception {
		btnHelper.click(saveBtnAcceptanceCriteria);
	}

	public void enterAllMaandatoryDataAndEnableAcceptanceCriteriaPopup(String din, String lotNo, String phoneNo, String firstName) throws Exception {
		collectionDate.click();
		collectionDateTodayOption.click();
		Thread.sleep(1000);
		enterDINNumber.sendKeys(din);
		Thread.sleep(500);
		drpdwnHelper.selectDropDownIndex(selectInstrumentTypeDrpdwn, 1);
		getAttribute = drpdwnHelper.getFirstSelectedOption(selectInstrumentTypeDrpdwn);
		Thread.sleep(500);
		enterLotNumber.sendKeys(lotNo);
		Thread.sleep(500);
		waitObj.waitForElementVisible(enterPhoneNo);
		enterPhoneNo.sendKeys(phoneNo);
		enterFirstName.sendKeys(firstName);
		Thread.sleep(1000);
		enterFirstName.sendKeys(Keys.TAB);
	}

	public void verifyAcceptanceCriteriaPopupFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//button[@class='confirm']")).size() != 0)
		{
			actual = true;
		}else {
			actual = false;
		}
		waitObj.waitForElementClickable(entryIsCorrentBtn);
		btnHelper.click(entryIsCorrentBtn);
		Thread.sleep(2500);
		Assert.assertEquals(actual, true);
	}

	public void clickAdvancedAcceptanceCriteriaIconOfAddEditAcceptanceScreen() throws Exception {
		btnHelper.click(advancedAcceptanceCriteriaRadioBtn);
	}

	public void clickAddConditionIconOfAddEditAcceptanceScreen() throws Exception {
		btnHelper.click(addConditionAdvanced);
	}

	public void selectQuestionFromSelectQuestionDrpDwnOfAddEditAcceptanceScreen() throws Exception {
		drpdwnHelper.selectDropDownIndex(selectQuestionAdvanced, 1);
	}

	public void selectOperatorFromSelectOperatorDrpDwnOfAddEditAcceptanceScreen() throws Exception {
		drpdwnHelper.selectDropDownIndex(selectOperatorAdvanced, 1);
	}

	public void enterValueInEnterValueTextfieldOfAddEditAcceptanceScreen(String enterValue) throws Exception {
		textBoxHelper.sendKeys(valueTextfieldAdvanced, enterValue);
	}

	public void clickAddBtnOfAddEditAcceptanceScreen() throws Exception {
		btnHelper.click(addBtnAdvanced);
	}

	public void clickOptionalProcedureCheckboxOfProcedureWorkflow() throws Exception {
		waitObj.waitForElementClickable(optionalProcedureCheckbox);
		helper.javascriptExecutorClick(optionalProcedureCheckbox);
	}

	public void enterOptionalProcedureMessageInTextfieldOfProcedureWorkflow(String optionalMsg) throws Exception {
		waitObj.waitForElementVisible(optionalProcedureMsgTextfield);
		textBoxHelper.sendKeys(optionalProcedureMsgTextfield, optionalMsg);
	}

	public void verifyOptionalProcedureMessageFunctionalityInAppEntry(String optionalMsg) throws Exception {
		waitObj.waitForElementVisible(optionalProcedureMsgInApp);
		scroll.scrollTillElem(optionalProcedureMsgInApp);
		Assert.assertEquals(optionalProcedureMsgInApp.getText(), optionalMsg);
	}

	public void clickKeyAttributeCheckboxOfAcceptanceTab() throws Exception {
		waitObj.waitForElementClickable(keyAttributeCheckbox);
		helper.javascriptExecutorClick(keyAttributeCheckbox);
	}	

	public void enterKeyAttributeValueInDINTextfield(String din) throws Exception {
		enterDINNumber.sendKeys(din);
		Thread.sleep(500);
		enterDINNumber.sendKeys(Keys.TAB);
	}

	public void verifyKeyAttributeValidationMessageInAppEntry() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//p[@translate='secure.forms.tabs.VALPRESPROVOTHER']")).size() != 0)
		{
			actual = true;
		}else {
			actual = false;
		}
		Assert.assertEquals(actual, true);
	}

	public void verifyDefinePassCriteriaInAppEntry() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("(//div[@ng-if='!proc.isOptionalProcedure'])[2]")).size() != 0)
		{
			actual = true;
		}else {
			actual = false;
		}
		scroll.scrollTillElem(defineCriteriaPassInAppEntry);
		Assert.assertEquals(actual, true);
	}

	public void verifyActiveInactiveFunctionalityOfAppBuilder() throws Exception {
		boolean flag;
		String actual = statusAppBuilder.getText();
		if(actual.equals("Active"))
		{
			flag = true;
		}else {
			flag = false;
		}
		scroll.scrollTillElem(statusAppBuilder);
		Assert.assertEquals(true, flag);
	}

	public void clickOnSaveAsDraftBtnInAppBuilder() throws Exception {
		waitObj.waitForElementClickable(saveAsDraftBtn);
		btnHelper.click(saveAsDraftBtn);
	}

	public void verifySaveAsDraftFunctionalityInAppBuilderListing() throws Exception {
		SoftAssert SF = new SoftAssert();
		SF.assertEquals(publishedVersion.getText(), "N/A");
		SF.assertEquals(versionAppBuilderVersion.getText(), "0.01");
		scroll.scrollTillElem(publishedVersion);
		SF.assertAll();
	}

	public void verifySaveAndPublishFunctionalityInAppBuilderListing() throws Exception {
		scroll.scrollTillElem(publishedVersion);
		Assert.assertEquals(publishedVersion.getText().trim(), "1.00");
	}

	public void clickLinkedInformationLink() throws Exception {
		waitObj.waitForElementClickable(viewLinkLinkedInformation);
		btnHelper.click(viewLinkLinkedInformation);
	}

	public void verifyLinkedInformationPopup() throws Exception {
		boolean actual = linkLinkedInformationPopup.isDisplayed();
		browserHelper.refresh();
		Assert.assertEquals(actual, true);
	}

	public void verifyNoRecordsFoundInLinkedInformationPopup() throws Exception {
		boolean actual = noRecordsFoundLinkedInformationPopup.isDisplayed();
		Assert.assertEquals(actual, true);
		browserHelper.refresh();
	}

	public String pageSizeDrp_GetSelectedValue() {
		String defaultItem;
		try 
		{
			waitObj.waitForElementVisible(driver.findElement(By.xpath("//*[@ng-change='vm.onPageNumberChange()']")));
			if (driver.findElements(By.xpath("//*[@ng-change='vm.onPageNumberChange()']")).size() != 0) 
			{
				defaultItem = drpdwnHelper.getFirstSelectedOption(pageSizeDrpDwn);

				//Select select = new Select(pageSizeDrp);
				//WebElement option = select.getFirstSelectedOption();
				//defaultItem = option.getText();
				return defaultItem;
			}
			else 
			{
				defaultItem = "1";
				return defaultItem;
			}
		}
		catch(Exception e)
		{
			defaultItem = "1";
		}
		return defaultItem;
	}
	
	public void getpageval()
	{
		try
		{
			paginationVal =textBoxHelper.getText(paginationShowing); //obj.paginationText.getText();

			if(driver.findElements(By.xpath("//*[ng-model='vm.options.pageSize']")).size()!=0)
			{
				PaginationSelectedVal = pageSizeDrp_GetSelectedValue();
			}
			else
			{
				PaginationSelectedVal="1";
			}
		}
		catch(Exception e)
		{
			e.toString();
		}

	}
	
	public void getAppNameWhichIsNotLinkedWithMasterApp() throws Exception {
	
		getpageval();
		
		int totalEntry = paginationHelper.paginationverification(paginationVal,PaginationSelectedVal );
//		paginationHelper.paginationverification(paginationShowing, pageSizeDrpDwn);
		for(int i=1; i<=driver.findElements(By.xpath("//a[@ng-click='openModal(row._id,row.title,row.route);']")).size(); i++) 
		{
			driver.findElement(By.xpath("(//a[@ng-click='openModal(row._id,row.title,row.route);'])["+i+"]")).click();
			boolean getNoRecords = noRecordsFoundLinkedInformationPopup.isDisplayed();
			if(getNoRecords == true)
			{
				browserHelper.refresh();
				getNoRecordsAppName = driver.findElement(By.xpath("(//td[@data-th='Apps :'])["+i+"]")).getText();
				break;
			}
			else 
			{
				System.out.println("Not Found Any Apps ...");
				browserHelper.refresh();
			}
		}
	}
	
	public void clickAdministrationBreadcrumbs() throws Exception {
		Thread.sleep(5000);
		btnHelper.click(administrationBreadcrumbs);
	}
	
	public void enterAllMandatoryFieldsInMasterAppScreen() throws Exception {
		waitObj.waitForElementVisible(masterAppTextField);
		textBoxHelper.sendKeys(masterAppTextField, getNoRecordsAppName);
		drpdwnHelper.selectDropDownIndex(masterModuleDrpDwn, 1);
		btnHelper.click(masterAppDrpDwn);
		driver.findElement(By.xpath("//label[contains(text(), '"+getNoRecordsAppName+"')]")).click();
		btnHelper.click(masterAppAddBtn);
	}
	
	public void clickSaveBtnMasterAppSettings() throws Exception {
		btnHelper.click(masterAppSaveBtn);
	}
	
	public void searchAppInAppBuilder() throws Exception {
		textBoxHelper.sendKeys(searchSets, getNoRecordsAppName);
		Thread.sleep(2000);
		btnHelper.click(viewLinkLinkedInformation);	
	}
	
	public void verifyLinkedInformationFunctionality() throws Exception {
		waitObj.waitForElementVisible(linkedPopupMasterAppName);
		String actual = linkedPopupMasterAppName.getText();
		Assert.assertEquals(actual, getNoRecordsAppName);
		browserHelper.refresh();
	}
	
	public void addAppInMultipleMasterApp() throws Exception {
		for(int i=1; i<=8; i++)
		{
			btnHelper.click(masterAppAddNewBtn);
			waitObj.waitForElementVisible(masterAppTextField);
			textBoxHelper.sendKeys(masterAppTextField, getNoRecordsAppName+i);
			masterAppName.add(i-1, helper.getClipboardContents(masterAppTextField));
			drpdwnHelper.selectDropDownIndex(masterModuleDrpDwn, 1);
			btnHelper.click(masterAppDrpDwn);
			waitObj.waitForElementClickable(driver.findElement(By.xpath("//label[contains(text(), '"+getNoRecordsAppName+"')]")));
			driver.findElement(By.xpath("//label[contains(text(), '"+getNoRecordsAppName+"')]")).click();
			btnHelper.click(masterAppAddBtn);
			waitObj.waitForElementClickable(masterAppSaveBtn);
			btnHelper.click(masterAppSaveBtn);
		}
	}
	
	public void verifyMultipleMasterAppFunctionality() throws Exception {
		btnHelper.click(popupCrossIcon);
		waitObj.waitForElementVisible(linkedPopupMasterAppName);
		SoftAssert SF = new SoftAssert();
		String nextBtnColor = "#337ab7", masterApp;
		
			if (driver.findElements(By.xpath("(//*[@ng-click='selectPage(page + 1, $event)'])[1]"))
					.size() != 0 == false) {
				
				for (int i = 1; i <= driver
						.findElements(By.xpath("//td[@class='entityRecordTableOverflwo ng-scope ng-binding']")).size(); i++)
				{				
					masterApp = driver
							.findElement(
									By.xpath("(//td[@class='entityRecordTableOverflwo ng-scope ng-binding'])[" + i + "]"))
							.getText();
					if (masterAppName.contains(masterApp))
					{
						System.out.println("Array List Data: "+ masterAppName);
						System.out.println("Index Of Value: "+ masterAppName.indexOf(masterApp));
						SF.assertEquals(masterApp, masterAppName.get(masterAppName.indexOf(masterApp)));
						masterAppName.remove(masterApp);
						System.err.println("Removed App: " + masterApp);
					} else {
						System.err.println("No Master App Found");
					}
				}
			} else {
				while (!nextBtnColor.equals("#777777")) {
                    for (int k = 1; k <= driver
    						.findElements(By.xpath("//td[@class='entityRecordTableOverflwo ng-scope ng-binding']")).size(); k++) {
                    	masterApp = driver.findElement(By.xpath("(//td[@class='entityRecordTableOverflwo ng-scope ng-binding'])[" + k + "]")).getText();
    					if (masterAppName.contains(masterApp))
    					{
    						System.out.println("Array List Data: "+ masterAppName);
    						System.out.println("Index Of Value: "+ masterAppName.indexOf(masterApp));
    						SF.assertEquals(masterApp, masterAppName.get(masterAppName.indexOf(masterApp)));
    						masterAppName.remove(masterApp);
    						System.err.println("Removed App: " + masterApp);
    					} else {
    						System.err.println("No Master App Found");
    					}	
                    }
					nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
					btnHelper.click(nextPagging);
					Thread.sleep(1000);
				}
			}
		
		SF.assertAll();
		browserHelper.refresh();
	}
	
	public void verifyPageSizeLabel() throws Exception {
		String actual = pageSizeLabel.getText();
		String pageSize = helper.getDrpDwnFirstSelectedOptn(pageSizeDrpDwn);
		Assert.assertEquals(actual, "Page Size: "+pageSize);
	}
	
	public void clickCrossIconLinkedInfoPopUp() throws Exception {
		btnHelper.click(popupCrossIcon);
		waitObj.waitForElementVisible(linkLinkedInformationPopup);
	}
	
	public void verifyCloseFunctionalityLinkedInfoPopUp() throws Exception {
		Thread.sleep(1000);
		boolean actual;
		if(driver.findElements(By.xpath("//h4[@class='col-sm-11 ng-binding']")).size() != 0)
		{
			actual = false;
		} else {
			actual = true;
		}
		
		Assert.assertEquals(actual, true);
	}
	
	public void clickActionIcon() throws Exception {
		getCopyAppNameBefore = firstAppTitle.getText();
		btnHelper.click(actionIcon);
	}
	
	public void clickOkBtnActionIcon() throws Exception {
		getCopyAppNameAfter = helper.getClipboardContents(copyPopUpTextField);
		waitObj.waitForElementVisible(okBtnActionIcon);
		btnHelper.click(okBtnActionIcon);
	}
	
	public void selectRolesInPermissionTabAction(String roleName) throws Exception {
		btnHelper.click(okBtnActionIcon);
		wait.until(ExpectedConditions.elementToBeClickable(clickBuildAppCheckbox));
		new RolesAndPermissionsPageObject(driver).checkAppAllPermissions(roleName);
	}
	
	public void clickSaveBtnOfPermissionTab() throws Exception {
		btnHelper.click(saveBtnPermissionTab);
		wait.until(ExpectedConditions.visibilityOf(addNewBtn));
	}
	
	public void verifyCopyFunctionality() throws Exception {
		Assert.assertEquals(firstAppTitle.getText(), getCopyAppNameAfter);
	}
	
	public void verifyCopyPopupMsg() throws Exception {
		waitObj.waitForElementVisible(copyPopupMsg);
		String actual = copyPopupMsg.getText();
		Thread.sleep(500);
		scroll.scrollTillElem(copyPopupMsg);
		Assert.assertEquals(actual, "Nice job! The copy of "+getCopyAppNameBefore+" was generated successfully.");
		btnHelper.click(okBtnActionIcon);
	}
	
	public void verifyColorOfOKBtn() throws Exception {
		waitObj.waitForElementVisible(okBtnActionIcon);
		Assert.assertEquals(btnHelper.BtnColor(okBtnActionIcon), "#5cb85c");
		btnHelper.click(okBtnActionIcon);
		waitObj.waitForElementVisible(clickBuildAppCheckbox);
	}
	
	public void clearAppTitleField() throws Exception {
		textBoxHelper.clear(copyPopUpTextField);
		Thread.sleep(2500);
		btnHelper.click(okBtnActionIcon);
	}
	
	public void verifyRequiredMsgOfCopyPopup() throws Exception {
		Assert.assertEquals(copyPopupMandatoryMsg.getText(), "You must fill App Title.");
		Thread.sleep(2500);
		btnHelper.click(copyPopupCancelBtn);
	}
	
	public void enterOneCharAppTitleField(String minChar) throws Exception {
		textBoxHelper.clear(copyPopUpTextField);
		textBoxHelper.sendKeys(copyPopUpTextField, minChar);
		Thread.sleep(2500);
		btnHelper.click(okBtnActionIcon);
	}
	
	public void verifyMinMsgOfCopyPopup() throws Exception {
		Assert.assertEquals(copyPopupMinMsg.getText(), "App Title should be at least 2 characters long.");
		Thread.sleep(2500);
		btnHelper.click(copyPopupCancelBtn);
	}
	
	public void enterMaxCharAppTitleField(String maxChar) throws Exception {
		textBoxHelper.clear(copyPopUpTextField);
		textBoxHelper.sendKeys(copyPopUpTextField, maxChar);
		Thread.sleep(2500);
		btnHelper.click(okBtnActionIcon);
	}
	
	public void verifyMaxMsgOfCopyPopup() throws Exception {
		Assert.assertEquals(copyPopupMinMsg.getText(), "App Title cannot be more than 50 characters.");
		Thread.sleep(2500);
		btnHelper.click(copyPopupCancelBtn);
	}
	
	public void clickNewCreatedAppAuditTrail() throws Exception {
		btnHelper.click(firstAuditTrailLink);
	}
	
	public void verifyCorrectDataofAuditTrail() throws Exception {
		btnHelper.click(usernameTab);
		btnHelper.click(clickMyProfile);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].removeAttribute('disabled','disabled')",departmentNameOfLoggedUser);
		createdBy = helper.getClipboardContents(firstnameSameAsLogin) + " " + helper.getClipboardContents(lastnameValForLoggedUser) + " (" + helper.getClipboardContents(departmentNameOfLoggedUser) + ")";
		driver.navigate().back();
		btnHelper.click(firstAuditTrailLink);
		String nextBtnColor="#337ab7", fieldName , oldValueField, newValueField;
		int counter =0;
		SoftAssert sf = new SoftAssert();
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			while(!nextBtnColor.equals("#777777")) {
				Thread.sleep(1000);
				List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='Field Name :']"));
				for(int i = 1; i<=count.size(); i++) {
					fieldName= driver.findElement(By.xpath("(//*[@data-th='Field Name :'])[" + i + "]")).getText().trim();
					oldValueField = driver.findElement(By.xpath("(//*[@data-th='Old Value :'])[" + i + "]")).getText().trim();
					newValueField = driver.findElement(By.xpath("(//*[@data-th='New Value :'])[" + i + "]")).getText().trim();
					switch(fieldName) {
					case "First Name":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, newFirstName);
						break;
					case "Password Generation Method":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, passwordMethod);
						break;
					case "Status":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, activeInactiveStatus);
						break;
					case "Authentication Method":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, authentication);
						break;
					case "Allow user to Login after Failed Attempts":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, failedAttempsStatus);
						break;
					case "User Name":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, userName);
						break;
					case "Last Name":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, lastName);
						break;
					case "Department":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, departmentName);
						break;
					case "Email":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, emailName);
						break;
					case "Display Roles":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, rolesName);
						break;
					case "Created By":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, createdBy);
						break;
					case "Modified By":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, createdBy);
						break;
					case "Created Date":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField.substring(0, newValueField.length() - 3), currentTimeAndDate);
						break;
					case "Modified Date":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField.substring(0, newValueField.length() - 3), currentTimeAndDate);
						break;
					case "Roles > #1 > Title":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, rolesName);
						break;
					default:
						System.out.println("Nothing ");
						break;	  

					}
				}
				System.err.println(counter);
				nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
				btnHelper.click(nextPagging);
			}
		}else
		{
			List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='Field Name :']"));
			for(int i = 1; i<=count.size(); i++) {
				fieldName= driver.findElement(By.xpath("(//*[@data-th='Field Name :'])[" + i + "]")).getText().trim();
				oldValueField = driver.findElement(By.xpath("(//*[@data-th='Old Value :'])[" + i + "]")).getText().trim();
				newValueField = driver.findElement(By.xpath("(//*[@data-th='New Value :'])[" + i + "]")).getText().trim();
				switch(fieldName) {
				case "First Name":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, newFirstName);
					break;
				case "Password Generation Method":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, passwordMethod);
					break;
				case "Status":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, activeInactiveStatus);
					break;
//				case "Authentication Method":
//					sf.assertEquals(oldValueField, "");
//					sf.assertEquals(newValueField, authenticationMethod);
//					break;
				case "Allow user to Login after Failed Attempts":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, failedAttempsStatus);
					break;
				case "User Name":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, userName);
					break;
				case "Last Name":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, lastName);
					break;
				case "Department":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, departmentName);
					break;
				case "Email":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, emailName);
					break;
				case "Display Roles":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, rolesName);
					break;
				case "Created By":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, createdBy);
					break;
				case "Modified By":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, createdBy);
					break;
				case "Created Date":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField.substring(0, newValueField.length() - 3), currentTimeAndDate);
					break;
				case "Modified Date":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField.substring(0, newValueField.length() - 3), currentTimeAndDate);
					break;
				case "Roles > #1 > Title":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, rolesName);
					break;
				default:
					System.out.println("Nothing ");
					break;
				}
			}
		}
		System.err.println(counter);
		scroll.scrollTillElem(recordsCount);
		sf.assertAll();
		//Assert.assertEquals(counter,Integer.parseInt(id1));
	}
	
	public void clickFirstAppNameLink() throws Exception {
		btnHelper.click(firstAppTitle);
	}
	
	public void enterHeaderMsgPrint(String enterVal) throws Exception {
		textBoxHelper.sendKeys(headerMsgPrintField, enterVal);
	}
	
	public void clickSaveDraftBtn() throws Exception {
		btnHelper.click(saveDraftBtn);
		waitObj.waitForElementClickable(searchSets);
	}
	
	public void clickAuditTrailOfCreatedApp() throws Exception {
		btnHelper.click(firstAuditTrailLink);
	}
	
	public void verifyAuditTrailOfCreatedApp() throws Exception {
		Assert.assertEquals(auditTrailVersion.getText(), "1.01");
	}
	
	public void clickRestoreLink() throws Exception {
		browserHelper.refresh();
		textBoxHelper.sendKeys(searchSets, getAppTitle);
		Thread.sleep(3500);
		btnHelper.click(restoreLink);
		Thread.sleep(1000);
		btnHelper.click(okBtnActionIcon);
		Thread.sleep(5000);
	}
	
	public void verifyRestoreFunctionality() throws Exception {
		textBoxHelper.sendKeys(searchSets, getAppTitle);
		Thread.sleep(3500);
		Assert.assertEquals(restoreLinkVersion.getText(), "1.02");
	}
	
	public void verifyRestoreInfoMsg() throws Exception {
		textBoxHelper.sendKeys(searchSets, getAppTitle);
		Thread.sleep(3500);
		btnHelper.click(restoreLink);
		Thread.sleep(1000);
		String publishVersion = publishedVersion.getText();
		Assert.assertEquals(restoreLinkInfoMsg.getText(), "Once it's done, it's done. Are you sure you want to restore the previous stable version"+publishVersion+"? You cannot change your mind after you select Yes!");
		btnHelper.click(okBtnActionIcon);
		Thread.sleep(1000);
	}
	
	public void clickSaveDraftApp() throws Exception {
		textBoxHelper.sendKeys(searchSets, getAppTitle);
		Thread.sleep(3500);
		getPublishVersion = publishedVersion.getText();
		btnHelper.click(firstAppTitle);
	}
	
	public void verifyPublishVersionAfterSave() throws Exception {
		float version = Float.parseFloat(getPublishVersion.trim());
		Assert.assertEquals(version+1.00, publishedVersion.getText().trim());
	}
	
	
	
	












}// end