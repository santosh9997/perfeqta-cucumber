package com.cucumber.framework.helper.PageObject;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Javascript.JavaScriptHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Pagination.Pagination;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.DateTimeHelper;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

public class BatchScenarioPageObject extends PageBase {
	private WebDriver driver;
	private Helper helper = new Helper();
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(BatchScenarioPageObject.class);
	private Search sObj;
	public DateTimeHelper datehelper = new DateTimeHelper();
	public TextBoxHelper txtHelper = new TextBoxHelper(ObjectRepo.driver);
	public DropDownHelper drpdwnHelper = new DropDownHelper(ObjectRepo.driver);
	public static String getAppName = null;
	public AppBuilderPageObject appbuilder = new AppBuilderPageObject(ObjectRepo.driver);
	public MasterAppSettingPageObject masterSetting = new MasterAppSettingPageObject(ObjectRepo.driver);
	public JavaScriptHelper jshelper = new JavaScriptHelper(ObjectRepo.driver);
	public BatchScenarioPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		sObj = new Search();
	}

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.home']")
	public WebElement administrationTile;
	
	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.admin.batchentrysettings']")
	public WebElement BtachEntryTile;
	
	@FindBy(how = How.XPATH, using = "//select[@name='qcModule']")
	public WebElement moduleDrpdown;
	
	@FindBy(how = How.XPATH, using = "//select[@ng-change='vm.onChangeForm()']")
	public WebElement appDrpdown;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addBatchEntry()']")
	public WebElement enableAppBatchEntry;
	
	@FindBy(how = How.XPATH, using = "(//input[@ng-model='item.copySiteSelections'])[1]")
	public WebElement siteSelectionChkBox;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save(vm.batchentrysettings,editBatchEntrySettings)']")
	public WebElement saveBtn;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(), 'Home')]")
    public WebElement backToHome;
	
	@FindBy(how = How.XPATH, using = "//*[@name='searchTxt']")
	public WebElement searchAppTextBox;
	
	@FindBy(how = How.XPATH, using = "//select[@ng-change='vm.levelChange(vm.selectedItem[$index], 2)']")
	public WebElement siteDrpdown;
	
	@FindBy(how = How.XPATH, using = "//select[@ng-change='vm.levelChange(vm.selectedItem[$index], 1)']")
	public WebElement siteDepartmentDrpdown;
	
	@FindBy(how = How.XPATH, using = "//select[@ng-change='vm.levelChange(vm.selectedItem[$index], 0)']")
	public WebElement siteRegionDrpdown;
	
	@FindBy(how = How.XPATH, using = "(//input[@ng-model='item.copyNonKeyAttributes'])[1]")
	public WebElement copynonKeyChkbox;
	
	@FindBy(how = How.XPATH, using = "(//a[@ng-click='vm.go(field,row,vm.resource);'])[1]")
	public WebElement firstApp;
	
	@FindBy(how = How.XPATH, using = "(//button[@ng-disabled='vm.isreadonly'])[1]")
	public WebElement datepickIcon;
	
	@FindBy(how = How.XPATH, using = "//button[@ng-click=\"select('today', $event)\"]")
	public WebElement datepickToday;
	
	@FindBy(how = How.XPATH, using = "(//*[@type='text'])[5]")
	public WebElement enterDIN;
	
	@FindBy(how = How.XPATH, using = "(//*[@type='text'])[6]")
	public WebElement lotNumber;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-options=\"option.value as option.title for option in vm.existingSetValue[item['s#']]\"])[9]")
	public WebElement weekdayDrpdown;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-model=\"vm.model[item['s#']][group['s#']]\"])[18]")
	public WebElement weekdayDrpdownSecondApp;
	
	
	
	@FindBy(how = How.XPATH, using = "(//input[@ng-model=\"vm.model[item['s#']][group['s#']]\"])[1]")
	public WebElement dateTxtBox;
	
	@FindBy(how = How.XPATH, using = "(//input[@ng-model='item.copyNonKeyEntities'])[1]")
	public WebElement NonkeyEntitiesCheckbox;
	
	@FindBy(how = How.XPATH, using = "(//input[@ng-model=\"item.isDefaultBatchMode\"])[1]")
	public WebElement StartAppBatchModeCheckbox;
	
	@FindBy(how = How.XPATH, using = "//input[@ng-change='vm.onChangeBatchName()']")
	public WebElement AppBatchMode;
	
	@FindBy(how = How.XPATH, using = "(//input[@ng-change=\"vm.setBatchFormat(item.entryType)\"])[1]")
	public WebElement YYMMDDcheckbox;
	

	@FindBy(how = How.XPATH, using = "(//input[@ng-model=\"vm.BatchEntryFormat\"])[2]")
	public WebElement MMDDYYcheckbox;
	
	@FindBy(how = How.XPATH, using = "(//input[@ng-model=\"vm.BatchEntryFormat\"])[3]")
	public WebElement customIncrementcheckbox;
	
	@FindBy(how = How.XPATH, using = "(//input[@ng-model=\"vm.BatchEntryFormat\"])[4]")
	public WebElement autoIncrementcheckbox;
	
	@FindBy(how = How.XPATH, using = "(//input[@ng-model=\"vm.BatchEntryFormat\"])[5]")
	public WebElement Manualcheckbox;
	 
	@FindBy(how = How.XPATH, using = "//input[@ng-model=\"vm.batchentrysettings.batchEntryFormat[vm.BatchEntryFormat].batchEntryFormatValue\"]")
	public WebElement customIncrementvalue;
	
	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.batchentrysettings.batchEntryFormat[vm.BatchEntryFormat].batchEntryFormatValue']")
	public WebElement autoIncrementvalueText;
	
	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.batchName']")
	public WebElement BatchEntryvalueText;
	
	@FindBy(how = How.XPATH, using = "//div[@title=\"Click here to expand\"]/div[1]/h4/span")
	public WebElement expectedBatchEntryvalue;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"DataTables_Table_6\"]/tbody/tr/td[2]")
	public WebElement Batchtableappname;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click=\"vm.showBatchList == true ? vm.showBatchList=false : vm.showBatchList=true\"]/div[2]")
	public WebElement ExpandBatchtable;
	
	@FindBy(how = How.XPATH, using = "(//input[@ng-change=\"vm.onChangeModule()\"])[2]")
	public WebElement masterAppRadioBtn;
	
	@FindBy(how = How.XPATH, using = "(//input[@ng-model=\"vm.model[item['s#']][group['s#']]\"])[2]")
	public WebElement DinForFirstApp;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-model=\"vm.model[item['s#']][group['s#']]\"])[2]")
	public WebElement DinForSecondApp;
	
	@FindBy(how = How.XPATH, using = "(//input[@question=\"vm.getQuestion(item['s#'])\"])[2]")
	public WebElement LotForFirstApp;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-model=\"vm.model[item['s#']][group['s#']]\"])[3]")
	public WebElement LotForSecondApp;
	
	@FindBy(how = How.XPATH, using = "//div[2]/div/entry-form-tabs/div/form/div[2]/div/entry-form/form/div[1]/div/div/div/div[1]/span/div/p/span/button/i")
	public WebElement datePickIconSecondApp;
	
	@FindBy(how = How.XPATH, using = "(//button[@id='saveAndAcceptID'])[1]")
	public WebElement saveFirstapp;
	
	@FindBy(how = How.XPATH, using = "(//a[@ng-click=\"select($event)\"])[2]")
	public WebElement clickonSecondapp;
	@FindBy(how = How.XPATH, using = "(//input[@ng-model=\"vm.model[item['s#']][group['s#']]\"])[1]")
	public WebElement FirstAppDateTxt;
	
	@FindBy(how = How.XPATH, using = "	(//a[@ng-click=\"select($event)\"])[1]")
	public WebElement FirstAppIcon;

	@FindBy(how = How.XPATH, using = "(//input[@ng-model=\"vm.model[item['s#']][group['s#']]\"])[3]")
	public WebElement lotTxtAfterFirstEntry;

	
	
	
	
	
	
	public static String appName;
	public static String Sitename;
	public static String SiteDeparment;
	public static String SiteRegion;
	public static String Customvalue;
	 
	public void clickOnBatchEntryTile() {
		btnHelper.click(BtachEntryTile);
	}
	public void selectModule() {
		drpdwnHelper.SelectUsingIndex(moduleDrpdown, 1);
		 
	}
	public void selectApp() {
		btnHelper.click(appDrpdown);
		drpdwnHelper.selectDropDownIndex(appDrpdown, 1);
		getAppName = drpdwnHelper.getSelectedValue(appDrpdown);
		System.err.println(getAppName);
		
	}
	public void clickonEnableAppBatch() {
		btnHelper.click(enableAppBatchEntry);
	}
	public void clickonSiteselectionChkBox() {
		btnHelper.click(siteSelectionChkBox);
	}
	public void clickonSaveBtn() {
		btnHelper.click(saveBtn);
		
	}
	public void clickBackToHome() throws Exception {
        btnHelper.click(backToHome);
    }
	public void enterSearchAppdata() {
		txtHelper.sendKeys(searchAppTextBox, getAppName);
	}
	public void verifySiteCopied() {
		Assert.assertEquals(Sitename, drpdwnHelper.getSelectedValue(siteDrpdown));
	}
	public void clickonCopynonKeyAtrributes() {
		btnHelper.click(copynonKeyChkbox);
	}
	public void verifynoKeyAttributesCopied(String DIN) throws Exception {
		boolean actual; 
		String date=  helper.getClipboardContents(dateTxtBox);
		 String din=  helper.getClipboardContents(enterDIN);
		 String Today = datehelper.getCurrentDateFormat();
	     
		 if(DIN.equals(din) && Today.equals(date)) {
			 actual = true;
		 } else {
			 actual = false;
		 }
		 
		 Assert.assertEquals(actual,true);
	}
	public void getFirstAppname() {
		appName = txtHelper.getText(firstApp);
		
	}
	public void copytheSiteName() {
		Sitename = drpdwnHelper.getSelectedValue(siteDrpdown);
	}
	public void enterValidDataNecessaryFields(String din, String lot) throws Exception {
		
		btnHelper.click(datepickIcon);
		btnHelper.click(datepickToday);
		txtHelper.clear(enterDIN);
		txtHelper.sendKeys(enterDIN, din);
		txtHelper.clear(lotNumber);
		txtHelper.sendKeys(lotNumber, lot);
		helper.SelectDrpDwnValueByIndex(weekdayDrpdown, 1);
		
	}
	
	public void selectAppwithuncheckedKeyAttribute() throws Exception {
		helper.SelectDrpDwnValueByText(appDrpdown, appbuilder.getAppTitle);
	}
	public void searchAppuncheckedKeyAttribute() {
		txtHelper.sendKeys(searchAppTextBox, appbuilder.getAppTitle);
	}
	public void clickonNonKeyEntities() {
		btnHelper.click(NonkeyEntitiesCheckbox);
	}
	public void verifyNonKeyEntitiesCopied(String lot) throws Exception {
		boolean actual;
		String LOT=  helper.getClipboardContents(lotNumber);
		 if(LOT.equals(lot)) {
			 actual = true;
		 } else {
			 actual = false;
		 }
		 
		 Assert.assertEquals(actual,true);
	}
	public void clickonStartAppDefaultBatchCheckbox() {
		StartAppBatchModeCheckbox.click();
		StartAppBatchModeCheckbox.click();
	}
	public void verifyDefaultrecordinBatchmode() throws Exception {
		Thread.sleep(1000);
		//String batchmode = helper.getClipboardContents(AppBatchMode);
//		WebElement yourButton= driver.findElement(By.name("shippingAddress_save"));

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].removeAttribute('disabled','disabled')",AppBatchMode);
		Thread.sleep(1000);
		 String batchmode = helper.getClipboardContents(AppBatchMode);
		boolean actual;

		if(batchmode.length() > 0) {
			actual = true;
			
		}else {
			actual = false;
		}
		Assert.assertEquals(actual,true);
	}
	public void selectYYMMDDautoincrementFormat() {
		btnHelper.click(YYMMDDcheckbox);
	}
	public void verifyBatchEntryYYMMDDformat() throws Exception {
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("arguments[0].removeAttribute('disabled','disabled')",AppBatchMode);
//		Thread.sleep(1000);
//		String batchmode = helper.getClipboardContents(AppBatchMode);
//		String arr [] = batchmode.split("-");
//		 String dateString [] = arr[0].split("/");
//		 boolean actual;
//		int month = Integer.parseInt(dateString[1]);
//		int day = Integer.parseInt(dateString[2]);
//		 
//		 if(dateString[0].length()<= 4 && month <=12 && month >=1 && day <=31 && day >= 1) {
//				actual = true;
//				
//			}else {
//				actual = false;
//			}
//			Assert.assertEquals(true, actual);
		
		 
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
 		js.executeScript("arguments[0].removeAttribute('disabled','disabled')",AppBatchMode);
 		Thread.sleep(1000);
 		String dateInApp = helper.getClipboardContents(AppBatchMode);
 		String arr [] = dateInApp.split("-");
 		String dateinDisplayInBatchmode = arr[0];
		
		
		String Batchdate = datehelper.getCurrentDateYYYYMMDD();
		boolean actual;
	     
		 if(dateinDisplayInBatchmode.equals(Batchdate)) {
			 actual = true;
		 } else {
			 actual = false;
		 }
		 
		 Assert.assertEquals(actual,true);
		
	}
	
	public void selectMMDDYYAutoincrementFormat() {
		btnHelper.click(MMDDYYcheckbox);
	}
	public void verifyBatchEntryMMDDYYformat() throws Exception {
		
		//*** ----This is different case please do not remove this code----***
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
 		js.executeScript("arguments[0].removeAttribute('disabled','disabled')",AppBatchMode);
 		Thread.sleep(1000);
 		String dateInApp = helper.getClipboardContents(AppBatchMode);
 		String arr [] = dateInApp.split("-");
 		String dateinDisplayInBatchmode = arr[0];
		String getdatefromSystem = datehelper.getCurrentDateFormat();
		
		boolean actual;
	     
		 if(getdatefromSystem.equals(dateinDisplayInBatchmode)) {
			 actual = true;
		 } else {
			 actual = false;
		 }
		 
		 Assert.assertEquals(actual,true);
	}
	public void selectCustomIncrement() {
		btnHelper.click(customIncrementcheckbox);
	}

	public void verifyCustomIncrementFormat () throws Exception {
		
		//*** ----This is different case please do not remove this code----***
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
 		js.executeScript("arguments[0].removeAttribute('disabled','disabled')",AppBatchMode);
 	 
 		String customName = helper.getClipboardContents(AppBatchMode);
 		String arr [] = customName.split("-");
 		String CustomNameInBatchmode = arr[0];
		
		boolean actual;
	      System.out.println(Customvalue);
		 if(Customvalue.equals(CustomNameInBatchmode)) {
			 actual = true;
		 } else {
			 actual = false;
		 }
		 
		 Assert.assertEquals(actual, true);
	}
	public void getCustomValue() throws Exception {
		btnHelper.click(customIncrementvalue);
		  Customvalue = helper.getClipboardContents(customIncrementvalue);
		 
	}
	public void selectAutoIncrement() {
		btnHelper.click(autoIncrementcheckbox);
	}
	public void getIncrementVAlue(String value) {
		btnHelper.click(autoIncrementvalueText);
		txtHelper.clearAndSendKeys(autoIncrementvalueText, value);
		 
		
	}
	public void verifyBatchEntryAutoIncrementFormat() {
		System.out.println("This is a bug and System is not incrementing value Properly");
		Assert.assertEquals(true, false);
		
	}
	public void selectManualChkBox() {
		btnHelper.click(Manualcheckbox);
	}
	public void verifyManualFormat(String value) throws InterruptedException {
		 
		String actual = getAppName+ " - "+ value;
		
		String expected = txtHelper.getText(expectedBatchEntryvalue);
		
		boolean actualdefault;
	       
		 if(actual.equals(expected)) {
			 actualdefault = true;
		 } else {
			 actualdefault = false;
		 }
		 
		 Assert.assertEquals(actualdefault,true);
		 
		
	}
	public void enterValueofBatchEntryID(String value) throws Exception {
		Thread.sleep(3000);
		btnHelper.click(BatchEntryvalueText);
		txtHelper.clearAndSendKeys(BatchEntryvalueText, value);
	}
	public void verifybatchTableDisplay() {
		
		//String expected = txtHelper.getText(Batchtableappname);
		//String app_name = appbuilder.getAppTitle;
		boolean actualdefault;
	       
		 if(driver.findElements(By.xpath("//*[@id='DataTables_Table_0']/tbody/tr/td[1]")).size() != 0) {
			 actualdefault = true;
		 } else {
			 actualdefault = false;
		 }
		 
		 Assert.assertEquals(actualdefault, true);
		
	}
	public void clickonExapndBatchTable() throws Exception {
		btnHelper.click(ExpandBatchtable);
		Thread.sleep(4000);
	}
	public void selectMasterApp() throws Exception {
		helper.SelectDrpDwnValueByText(appDrpdown, masterSetting.getMasterAppTitle+ " (Master App)");
	}
	public void searchMasterApp() {
		txtHelper.sendKeys(searchAppTextBox, masterSetting.getMasterAppTitle);
		//txtHelper.sendKeys(searchAppTextBox, "Master App Test - 76548");
	}
	public void clickonMasterAppRadioBtn() {
		btnHelper.click(masterAppRadioBtn);
	}
	public void addAllnecessaryFieldFirstApp(String Din, String lot) throws Exception {
		btnHelper.click(datepickIcon);
		btnHelper.click(datepickToday);
		txtHelper.sendKeys(DinForFirstApp, Din);
		txtHelper.sendKeys(LotForFirstApp, lot);
		helper.SelectDrpDwnValueByIndex(weekdayDrpdown, 1);
	}
	public void addAllnecessaryFieldSecondApp(String Din, String lot) throws Exception {
		Thread.sleep(3000);
		jshelper.javascriptClick(datePickIconSecondApp);
		//datePickIconSecondApp.click();
		btnHelper.click(datepickToday);
		txtHelper.sendKeys(DinForSecondApp, Din);
		txtHelper.sendKeys(LotForSecondApp, lot);
		helper.SelectDrpDwnValueByIndex(weekdayDrpdownSecondApp, 1);
	}
	
	public void saveFirstApp() throws Exception {
		btnHelper.click(saveFirstapp);
		Thread.sleep(4000);
	}
	
	public void clickonSecondAppIcon() {
		btnHelper.click(clickonSecondapp);
	}
	public void copyAllSiteSelectionValues() {
		Sitename = drpdwnHelper.getSelectedValue(siteDrpdown);
		SiteDeparment = drpdwnHelper.getSelectedValue(siteDepartmentDrpdown);
		SiteRegion = drpdwnHelper.getSelectedValue(siteRegionDrpdown);
	}
	public void verifySiteSelectionDataCopied() {
		
		Assert.assertEquals(Sitename, drpdwnHelper.getSelectedValue(siteDrpdown));
		Assert.assertEquals(SiteDeparment, drpdwnHelper.getSelectedValue(siteDepartmentDrpdown));
		Assert.assertEquals(SiteRegion, drpdwnHelper.getSelectedValue(siteRegionDrpdown));
	}
	public void verifynonKeyAttributeForMasterApp(String DIN) throws Exception {
		boolean actual; 
		String date=  helper.getClipboardContents(FirstAppDateTxt);
		 String din=  helper.getClipboardContents(DinForFirstApp);
		 String Today = datehelper.getCurrentDateFormat();
	     
		 if(DIN.equals(din) && Today.equals(date)) {
			 actual = true;
		 } else {
			 actual = false;
		 }
		 
		 Assert.assertEquals(actual,true);
	}
	public void clickonFirstAppIcon() {
		btnHelper.click(FirstAppIcon);
	}
	public void verifyNonKeyEntitiesCopiedMasterApp(String lot) throws Exception {
		boolean actual;
		String LOT=  helper.getClipboardContents(lotTxtAfterFirstEntry);
		 if(LOT.equals(lot)) {
			 actual = true;
		 } else {
			 actual = false;
		 }
		 
		 Assert.assertEquals(actual,true);
	}
	public void verifyDefaultrecordinBatchmodeforMaster() throws Exception {
		Thread.sleep(1000);
 		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].removeAttribute('disabled','disabled')",AppBatchMode);
		Thread.sleep(1000);
		 String batchmode = helper.getClipboardContents(AppBatchMode);
		boolean actual;

		if(batchmode.length() > 12) {
			actual = true;
			
		}else {
			actual = false;
		}
		Assert.assertEquals(actual,true);
	}
	public void verifyBatchEntryYYMMDDformatMasterApp() throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
 		js.executeScript("arguments[0].removeAttribute('disabled','disabled')",AppBatchMode);
 		Thread.sleep(1000);
 		String dateInApp = helper.getClipboardContents(AppBatchMode);
 		String arr [] = dateInApp.split("-");
 		String dateinDisplayInBatchmode = arr[0];
		
		
		String Batchdate = datehelper.getCurrentDateYYYYMMDD();
		boolean actual;
	     
		 if(dateinDisplayInBatchmode.equals(Batchdate)) {
			 actual = true;
		 } else {
			 actual = false;
		 }
		 
		 Assert.assertEquals(actual,true);
		
	}
	public void verifyBatchEntryMMDDYYformatMasterApp() throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
 		js.executeScript("arguments[0].removeAttribute('disabled','disabled')",AppBatchMode);
 		Thread.sleep(1000);
 		String dateInApp = helper.getClipboardContents(AppBatchMode);
 		String arr [] = dateInApp.split("-");
 		String dateinDisplayInBatchmode = arr[0];
		String getdatefromSystem = datehelper.getCurrentDateFormat();
		
		boolean actual;
	     
		 if(getdatefromSystem.equals(dateinDisplayInBatchmode)) {
			 actual = true;
		 } else {
			 actual = false;
		 }
		 
		 Assert.assertEquals(actual,true);
	}
	public void verifyCustomIncrementFormatMasterApp() throws Exception {
		//*** ----This is different case please do not remove this code----***
		
				JavascriptExecutor js = (JavascriptExecutor) driver;
		 		js.executeScript("arguments[0].removeAttribute('disabled','disabled')",AppBatchMode);
		 	 
		 		String customName = helper.getClipboardContents(AppBatchMode);
		 		String arr [] = customName.split("-");
		 		String CustomNameInBatchmode = arr[0];
		 		Customvalue = "27";
				boolean actual;
			      System.out.println(Customvalue);
				 if(Customvalue.equals(CustomNameInBatchmode)) {
					 actual = true;
				 } else {
					 actual = false;
				 }
				 
				 Assert.assertEquals(actual, true);
	}
	public void verifyBatchEntryAutoIncrementFormatMasterApp() {
		System.out.println("This is a bug and System is not incrementing value Properly");
		Assert.assertEquals(true, false);
	}
	public void verifyManualFormatMasterApp(String value) {
	String actual = masterSetting.getMasterAppTitle+ " - "+ value;
		//String actual = "Master App Test - 76548"+ " - "+ value;
		
		String expected = txtHelper.getText(expectedBatchEntryvalue);
		
		boolean actualdefault;
	       
		 if(actual.equals(expected)) {
			 actualdefault = true;
		 } else {
			 actualdefault = false;
		 }
		 
		 Assert.assertEquals(actualdefault,true);
	}
	public void verifybatchTableDisplayMasterApp() {
		boolean actualdefault;
	       
		 if(driver.findElements(By.xpath("//div[2]/table/tbody/tr/td[1]")).size() != 0) {
			 actualdefault = true;
		 } else {
			 actualdefault = false;
		 }
		 
		 Assert.assertEquals(actualdefault, true);
	}
	
}