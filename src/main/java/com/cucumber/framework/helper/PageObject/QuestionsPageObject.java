package com.cucumber.framework.helper.PageObject;

import static org.testng.Assert.assertTrue;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Browser.BrowserHelper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Copy.Copy;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Grid.GridHelper;
import com.cucumber.framework.helper.Javascript.JavaScriptHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Pagination.Pagination;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.Sorting.SortingOnColumn;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;

public class QuestionsPageObject extends PageBase
{
	private WebDriver driver;
	private ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(QuestionsPageObject.class);
	private SortingOnColumn sortColumn;
	private Search search;
	//private Helper helper = new Helper();
	private Scroll scroll;
	//private Copy copy;
	private ExcelUtils excel;
	private WaitHelper waitObj;
	private DropDownHelper drpHelper;
	private TextBoxHelper textboxHelper;
	private JavaScriptHelper javaScriptHelper;
	private GridHelper gridHelper;
	private PaginationPageObject pageObject;
	private String paginationVal=null, PaginationSelectedVal=null,srchResultNo=null;
	//private int pageSizeDropval;
	private Pagination paginationObj = new Pagination();
	private QuestionsPageObject obj;
	private BrowserHelper browserHelper;
	private int searchBeforeCount=0;
	private int searchAfterCount=0;
	public QuestionsPageObject(WebDriver driver)

	{
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		sortColumn = new SortingOnColumn();
		search = new Search();
		scroll = new Scroll();
		waitObj = new WaitHelper(ObjectRepo.driver, ObjectRepo.reader);
		drpHelper = new DropDownHelper(ObjectRepo.driver);
		textboxHelper =new TextBoxHelper(ObjectRepo.driver);
		javaScriptHelper = new JavaScriptHelper(ObjectRepo.driver);
		gridHelper = new GridHelper(ObjectRepo.driver);
		pageObject = new PaginationPageObject(ObjectRepo.driver);
		browserHelper = new BrowserHelper(driver);

	}

	public String excelData(int rowVal, int colVal) throws Exception {
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Questions");
		System.err.println();
		return excel.readXLSFile("Questions", rowVal, colVal);
	}


	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.questions.list']")
	public WebElement questionsTile;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement questionsModuleName;

	@FindBy(how = How.NAME, using = "searchBy")
	public WebElement questionsDropFilterBy;

	@FindBy(how=How.XPATH, using = "//*[@ng-model='vm.qtype']")
	public WebElement drpQuestionType;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement paginationText;

	@FindBy(how = How.ID, using = "//*[@ng-change='vm.onPageNumberChange()']")
	public WebElement pageSizeDrp;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.options.data && vm.options.data.length']")
	public WebElement grid_data;

	@FindBy(how = How.XPATH, using = "//*[@class='paddingLeft-15px ng-binding']")
	public WebElement srcResultPageNo;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.textSearch']")
	public WebElement textSearch;

	@FindBy(how = How.XPATH, using = "//*[@class='crumb breadcrumb gray ng-isolate-scope']")
	public WebElement editQuestionBreadCrumbs;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.loadsave?return:vm.save(vm.question, editQuestionForm)']")
	public WebElement editQuestionSaveButton;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-danger11 ng-binding ng-scope']")
	public WebElement editQuestionCancelButton;

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.questions.edit']")
	public WebElement questionsAddNewBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.question.title']")
	public WebElement questionsNameTitle;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.question.edit.QUESTIONNAMEISREQUIRED']")
	public WebElement questionsRequiredVal;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.question.edit.QUESTIONNAMESHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement questionsMinimumVal;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.question.edit.QUESTIONNAMESHOULDNOTBEMORETHAN200CHARACTERSLONG']")
	public WebElement questionsMaximumVal;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.question.edit.QUESTIONNAMEMUSTBEUNIQUE']")
	public WebElement questionsUniqueVal;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.questiontag']")
	public WebElement questionsTag;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.question.edit.QUESTIONRAGISREQUIRED']")
	public WebElement questionsTagRequiredVal;

	@FindBy(how = How.XPATH, using = "//*[text()='Question Details']")
	public WebElement questionsDetails;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.question.edit.QUESTIONTAGSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement questionsTagMinimumVal;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.question.edit.QUESTIONTAGSHOULDNOTBEMORETHAN20CHARACTERSLONG']")
	public WebElement questionsTagMaximumVal;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.questiontag']")
	public WebElement questionsAddTag;

	@FindBy(how = How.XPATH, using = "//*[@ng-bind-html='vm.message1']")
	public WebElement questionsTagUniqueVal;

	@FindBy(how = How.XPATH, using = "//*[@name='isInstructionForUser']")
	public WebElement questionsInstForUserCheck;

	@FindBy(how = How.XPATH, using = "//*[@name='instructionForUser']")
	public WebElement questionsInstForUserTextbox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERISREQUIRED']")
	public WebElement questionsInstForUserRequiredVal;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement questionsInstForUserMinimumVal;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDNOTBEMORETHAN800CHARACTERSLONG']")
	public WebElement questionsInstForUserMaximumVal;

	@FindBy(how = How.XPATH, using = "//*[@name='typeSelect']")
	public WebElement questionsType;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.loadsave?return:vm.save(vm.question, editQuestionForm)']")
	public WebElement questionsSaveButton;

	@FindBy(how = How.XPATH, using = "//span[@class='currentStep ng-binding']")
	public WebElement verifyAuditTrail;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-danger11 sets-logspage-left-mar ng-binding']")
	public WebElement auditTrailBackButton;

	@FindBy (how=How.XPATH, using="//*[@ng-repeat='row in vm.options.data']")
	public WebElement tableIdoRxPath;

	@FindBy (how=How.XPATH, using="//*[@ng-click='selectPage(page + 1, $event)']")
	public WebElement nextBtnColor;

	@FindBy (how=How.XPATH, using="//button[@id='shareapp-remove-search-text']")
	public WebElement iconRemoveSearch;

	@FindBy (how=How.XPATH,using="//div[@class='form-group']/child::label")
	public WebElement pagesizelabel;

	@FindBy (how=How.XPATH, using="(//li[@ng-class='{disabled: noNext()||ngDisabled}']/preceding-sibling::li[1])[1]")
	public WebElement lastpageval;

	public void getpageval()
	{
		try
		{
			obj = new QuestionsPageObject(ObjectRepo.driver);

			paginationVal =textboxHelper.getText(obj.paginationText); //obj.paginationText.getText();

			if(driver.findElements(By.xpath("//*[ng-model='vm.options.pageSize']")).size()!=0)
			{
				srchResultNo = textboxHelper.getText(obj.srcResultPageNo);//obj.srcResultPageNo.getText();
				PaginationSelectedVal = obj.pageSizeDrp_GetSelectedValue();
			}
			else
			{
				PaginationSelectedVal="1";
			}
		}
		catch(Exception e)
		{
			e.toString();
		}

	}
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.go(field,row,vm.resource);'])[1]")
	public WebElement firstQuestionName;
	
	
	
	public void clickOnQuestionsTile()
	{
		try
		{
			//Thread.sleep(3000);
			btnHelper.click(questionsTile);
			//	questionsTile.click();

		}
		catch(Exception e)
		{
			e.toString();
		}

	}
	public void verifyQuestionModuleName() throws Exception
	{
		//String val = questionsModuleName.getText();
		Assert.assertEquals(textboxHelper.getText(questionsModuleName), ObjectRepo.reader.getQuestionsModuleName());
	}
	public void verifyQuestionsDrpFilter() throws Exception
	{
		boolean bool = questionsDropFilterBy.isEnabled();
		assertTrue(bool);
	}

	public void verifyQuestionsDrpClickable() throws Exception
	{
		boolean bool = questionsDropFilterBy.isEnabled();
		Assert.assertEquals(bool, true);
	}

	public void selectQuestionTypeOption() throws Exception
	{
		drpHelper.selectDropDownText(questionsDropFilterBy, "Question Type");
		//helper.SelectDrpDwnValueByText(questionsDropFilterBy, "Question Type");
	}

	public void verifyQuestionsTypeDrpFilter() throws Exception
	{
		boolean bool = questionsDropFilterBy.isEnabled();
		assertTrue(bool);
	}

	public void verifyQuestionsTypeClickable() throws Exception
	{
		boolean bool = questionsDropFilterBy.isEnabled();
		Assert.assertEquals(bool, true);
	}

	public String pageSizeDrp_GetSelectedValue() {
		String defaultItem;
		try 
		{
			waitObj.waitForElementVisible(driver.findElement(By.xpath("//*[@ng-change='vm.onPageNumberChange()']")));
			if (driver.findElements(By.xpath("//*[@ng-change='vm.onPageNumberChange()']")).size() != 0) 
			{
				defaultItem = drpHelper.getFirstSelectedOption(pageSizeDrp);

				//Select select = new Select(pageSizeDrp);
				//WebElement option = select.getFirstSelectedOption();
				//defaultItem = option.getText();
				return defaultItem;
			}
			else 
			{
				defaultItem = "1";
				return defaultItem;
			}
		}
		catch(Exception e)
		{
			defaultItem = "1";
		}
		return defaultItem;
	}

	public void verifyAscendingOrder() {
		System.out.println("click on ascending");
	}

	public void verifyAscendingOrderResult(String sortingColName) {
		try 
		{
			String PaginationVal = textboxHelper.getText(paginationText); //paginationText.getText();
			String PaginationSelectedVal = pageSizeDrp_GetSelectedValue();
			sortColumn.verifySorting(sortingColName, 1, PaginationVal, PaginationSelectedVal, grid_data);
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
	}

	public void verifysearch(String searchedItem) throws Exception
	{
		//QuestionsPageObject obj = new QuestionsPageObject(ObjectRepo.driver);
		getpageval();
		paginationVal =textboxHelper.getText(obj.paginationText); //obj.paginationText.getText();
		srchResultNo = textboxHelper.getText(obj.srcResultPageNo);//obj.srcResultPageNo.getText();
		PaginationSelectedVal = obj.pageSizeDrp_GetSelectedValue();
		search.SearchVerification(paginationVal, PaginationSelectedVal, obj.grid_data, searchedItem, obj.textSearch,srchResultNo);
	}

	public void clickToFirstQuestion() {
		btnHelper.click(firstQuestionName);
	}

	public void verifyEditQuestionBreadCrumbs() 
	{
		try
		{
			Assert.assertEquals(textboxHelper.getText(editQuestionBreadCrumbs), ObjectRepo.reader.getBreadCrumbsForAddEditQuestion());
		}
		catch(Exception e)
		{
			e.toString();
		}
	} 

	public void clickToSaveButton() {
		//editQuestionSaveButton.click();
		btnHelper.click(editQuestionSaveButton);
	}

	public void verifyQuestionsSaveButton() throws Exception
	{
		boolean bool = editQuestionSaveButton.isEnabled();
		assertTrue(bool);
	}

	public void clickToCancelButton() {
		//editQuestionCancelButton.click();
		btnHelper.click(editQuestionCancelButton);
	}

	public void verifyQuestionPageRedirection() {
		Assert.assertEquals( textboxHelper.getText(questionsModuleName), ObjectRepo.reader.getQuestionsModuleName());
	}

	public void verifySaveButtonColor() throws Exception {
		//scroll.scrollDown(ObjectRepo.driver);
		//scroll.scrollDown(ObjectRepo.driver);
		scroll.scrollTillElem(editQuestionSaveButton);
		Assert.assertEquals(btnHelper.BtnColor(editQuestionSaveButton), ObjectRepo.reader.getLightGreenColor());
	}

	public void verifyCancelButtonColor() {
		//scroll.scrollDown(ObjectRepo.driver);
		//scroll.scrollDown(ObjectRepo.driver);
		scroll.scrollTillElem(editQuestionCancelButton);
		Assert.assertEquals(btnHelper.BtnColor(editQuestionCancelButton), ObjectRepo.reader.getDarkGreyColor());
	}

	public void CopyClick(String firstcopyname) throws Exception {
		Copy.VerifyCopyPopup(firstcopyname);
	}

	public void CopyDefaultValue() throws Exception {
		Copy.CopyPopupDefaultValue();
	}

	public void CreateCopy() throws Exception {
		Copy.CreateCopy();
	}
	public void clickonAddNewBtn() throws InterruptedException
	{
		btnHelper.click(questionsAddNewBtn);
	}
	public void questionsNamesendkeys(String questionsnameminival) throws Exception
	{
		textboxHelper.clearAndSendKeys(questionsNameTitle, questionsnameminival);
	}
	public void verifyQuestionRequiredVal()
	{
		try	
		{
			//Thread.sleep(1000);
			//String questionval = questionsRequiredVal.getText();
			Assert.assertEquals(ObjectRepo.reader.getQuestionsRequiredVal(), textboxHelper.getText(questionsRequiredVal));
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionMinimumVal()  
	{
		try  
		{
			//Thread.sleep(1000);  
			//String questionminival = questionsMinimumVal.getText();
			Assert.assertEquals(ObjectRepo.reader.getQuestionsMinimumVal(),textboxHelper.getText(questionsMinimumVal) );
		}
		catch(Exception e)
		{
			e.toString();
		} 
	}
	public void verifyQuestionMaximumVal()
	{
		try
		{
			//Thread.sleep(1000);
			//String questionmaxval = questionsMaximumVal.getText();
			Assert.assertEquals(ObjectRepo.reader.getQuestionsMaximumVal(), textboxHelper.getText(questionsMaximumVal));
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionNoVal()
	{
		boolean reqMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.question.edit.QUESTIONNAMEISREQUIRED']")).size() == 0,

				minMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.question.edit.QUESTIONNAMESHOULDBEATLEAST2CHARACTERSLONG']")).size() == 0,

				maxMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.question.edit.QUESTIONNAMESHOULDNOTBEMORETHAN200CHARACTERSLONG']")).size() == 0,                  

				uniqMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.question.edit.QUESTIONNAMEMUSTBEUNIQUE']")).size() == 0,                         

				flag;

		if (reqMsg && minMsg && maxMsg && uniqMsg)
		{
			flag = true;
		} 
		else 
		{
			flag = false;
		}
		Assert.assertEquals(flag, true);
	}
	public void verifyQuestionUniqueVal()
	{
		try
		{
			//Thread.sleep(1000);
			Assert.assertEquals(ObjectRepo.reader.getQuestionsUniqueVal(), textboxHelper.getText(questionsUniqueVal));
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void questionsTagsendkeys(String questionstagminival) throws Exception
	{
		textboxHelper.sendKeys(questionsTag, questionstagminival);
	}
	public void verifyQuestionTagRequiredVal()
	{
		try
		{  
			//Thread.sleep(1000);
			//String questiontagval = textboxHelper.getText(questionsTagRequiredVal);//questionsTagRequiredVal.getText();
			Assert.assertEquals(ObjectRepo.reader.getQuestionsTagRequiredVal(), textboxHelper.getText(questionsTagRequiredVal));
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionTagMinimumVal()
	{
		try
		{
			//Thread.sleep(1000);
			String questiontagminival = textboxHelper.getText(questionsTagMinimumVal);//questionsTagMinimumVal.getText();
			//questionsDetails.click();
			btnHelper.click(questionsDetails);
			Assert.assertEquals(ObjectRepo.reader.getQuestionsTagMinVal(), questiontagminival);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionTagMaximumVal()
	{
		try
		{
			//Thread.sleep(1000);
			//String questiontagmaxval = questionsTagMaximumVal.getText();
			Assert.assertEquals(ObjectRepo.reader.getQuestionsTagMaxVal(), textboxHelper.getText(questionsTagMaximumVal));
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void clickonAddTagBtn() throws InterruptedException
	{
		//Thread.sleep(3000);
		//questionsAddTag.click();
		btnHelper.click(questionsAddTag);
	}
	public void verifyQuestionTagUniqueVal()
	{
		try
		{
			//Thread.sleep(1000);
			Assert.assertEquals(ObjectRepo.reader.getQuestionsTagUniqueVal(), textboxHelper.getText(questionsTagUniqueVal));
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionTagNoVal()
	{
		boolean reqTagMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.question.edit.QUESTIONRAGISREQUIRED']")).size() == 0,

				minTagMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.question.edit.QUESTIONTAGSHOULDBEATLEAST2CHARACTERSLONG']")).size() == 0,

				maxTagMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.question.edit.QUESTIONTAGSHOULDNOTBEMORETHAN20CHARACTERSLONG']")).size() == 0,                  

				uniqTagMsg = ObjectRepo.driver.findElements(By.xpath("//*[contains(text(), 'Question Tag already exists.')]")).size() == 0,                         

				flag;

		if (reqTagMsg && minTagMsg && maxTagMsg && uniqTagMsg) {
			flag = true;
		} else {
			flag = false;
		}
		Assert.assertEquals(flag, true);
	}
	public void selectInstructionForUser() throws InterruptedException
	{
		//questionsInstForUserCheck.click();
		btnHelper.click(questionsInstForUserCheck);
		//Thread.sleep(3000);
	}
	public void verifyInstructionForUser() throws InterruptedException
	{
		waitObj.waitForElementVisible(ObjectRepo.driver.findElement(By.xpath("//*[@name='instructionForUser']")));
		boolean instTxtBox = ObjectRepo.driver.findElements(By.xpath("//*[@name='instructionForUser']")).size() != 0,
				flag;

		if (instTxtBox) 
		{
			flag = true;
		}
		else
		{
			flag = false;
		}
		Assert.assertEquals(flag, true);
	}
	public void questionsInstructionForUsersendkeys(String questionsinstforuserminival) throws Exception
	{
		//questionsInstForUserTextbox.clear();
		textboxHelper.clear(questionsInstForUserTextbox);
		textboxHelper.sendKeys(questionsInstForUserTextbox, questionsinstforuserminival);
		//questionsInstForUserTextbox.sendKeys(questionsinstforuserminival);
	}
	public void verifyQuestionInstructionForUserRequiredVal()
	{
		try
		{
			String questioninstructionforuserReqval =  textboxHelper.getText(questionsInstForUserRequiredVal);//questionsInstForUserRequiredVal.getText();
			//questionsDetails.click();	
			btnHelper.click(questionsDetails);
			Assert.assertEquals(ObjectRepo.reader.getInstForUsrTxtBoxRqiMsg(), questioninstructionforuserReqval);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionInstForUserMinimumVal()
	{
		try
		{
			//Thread.sleep(1000);
			String questioninstforuserminival = textboxHelper.getText(questionsInstForUserMinimumVal);//questionsInstForUserMinimumVal.getText();
			//questionsDetails.click();
			btnHelper.click(questionsDetails);
			Assert.assertEquals(ObjectRepo.reader.getInstForUsrTxtBoxMinMsg(), questioninstforuserminival);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionInstForUserMaximumVal()
	{
		try
		{
			//Thread.sleep(1000);
			String questioninstforusermaxval = textboxHelper.getText(questionsInstForUserMaximumVal);
			//questionsDetails.click();
			btnHelper.click(questionsDetails);
			Assert.assertEquals(ObjectRepo.reader.getInstForUsrTxtBoxMaxMsg(), questioninstforusermaxval);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyQuestionInstForUserNoVal()
	{
		boolean reqInstForUserMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERISREQUIRED']")).size() == 0,

				minInstForUserMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDBEATLEAST2CHARACTERSLONG']")).size() == 0,

				maxInstForUserMsg = ObjectRepo.driver.findElements(By.xpath("//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDNOTBEMORETHAN800CHARACTERSLONG']")).size() == 0,                                           

				flag;

		if (reqInstForUserMsg && minInstForUserMsg && maxInstForUserMsg) {
			flag = true;
		} else {
			flag = false;
		}
		Assert.assertEquals(flag, true);
	}
	public void selectTypeOption() throws Exception
	{
		//helper.SelectDrpDwnValueByText(questionsType, "Textbox");
		
		drpHelper.selectDropDownText(questionsType, "Textbox");
	}
	public void questionsSaveButton() throws InterruptedException {
		//Thread.sleep(3000);
		//questionsSaveButton.click();
		btnHelper.click(questionsSaveButton);
		//Thread.sleep(3000);
	}
	public void verifyAuditTrailPage() throws Exception {
		log.info(verifyAuditTrail);
		Assert.assertEquals(textboxHelper.getText(verifyAuditTrail), ObjectRepo.reader.getViewAuditTrailText());
	}
	public void verifyAuditTrailBreadCrumbs() {
		try
		{
			String val = editQuestionBreadCrumbs.getText();
			//Thread.sleep(1000);
			Assert.assertEquals(val,ObjectRepo.reader.getEditQuestionBreadCrumbsForAuditTrail());
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyBackButtonColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(auditTrailBackButton), ObjectRepo.reader.getDarkGreyColor());
	}
	public void clickBackButton() throws InterruptedException {
		//Thread.sleep(3000);
		//auditTrailBackButton.click();
		btnHelper.click(auditTrailBackButton);
	}
	public void verifyAuditTrailBackPage() throws Exception {
		log.info(questionsModuleName);
		Assert.assertEquals(questionsModuleName.getText(), ObjectRepo.reader.getQuestionsModuleName());
	}
	public void verifyQuestionsBreadCrumbs()
	{
		try
		{
			//String val = editQuestionBreadCrumbs.getText();
			//Thread.sleep(1000);
			Assert.assertEquals(textboxHelper.getText(editQuestionBreadCrumbs), ObjectRepo.reader.getEditQuestionBreadCrumbs() );
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void selectQuestionTypeFilter(String filterType)
	{		
		try
		{
			drpHelper.selectDropDownText(questionsDropFilterBy, filterType);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void selectQuestionType(String questionType)
	{
		try
		{
			drpHelper.selectDropDownText(drpQuestionType,questionType );
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyAutoCalculatedFilter()
	{
		SoftAssert sf =new SoftAssert();		
		try
		{
			QuestionsPageObject obj = new QuestionsPageObject(ObjectRepo.driver);
			getpageval();
			int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
			int count=0;
			for(int i = 1; i <= totalEntry; i++)
			{
				String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
				String txtval = gridHelper.getGridColumnText(rowindex, i , 2);
				sf.assertEquals(txtval, "Auto Calculated");
				count++;	
				if(i==paginationObj.pageSizeDropval)
				{
					clickonNextButon();
					i=0;
				}
				if(count==totalEntry)
				{
					break;
				}
			}
			sf.assertAll();
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
	public void verifyTextboxFilter()
	{
		SoftAssert sf =new SoftAssert();		
		getpageval();
		int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
		int count=0;
		for(int i = 1; i <= totalEntry; i++)
		{
			String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
			String txtval = gridHelper.getGridColumnText(rowindex, i , 2);
			sf.assertEquals(txtval, "Textbox");
			count++;	
			if(i==paginationObj.pageSizeDropval)
			{
				clickonNextButon();
				i=0;
			}
			if(count==totalEntry)
			{
				break;
			}
		}
		sf.assertAll();
	}
	public void verifyCheckboxFilter()
	{
		SoftAssert sf =new SoftAssert();		
		getpageval();
		int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
		int count=0;
		for(int i = 1; i <= totalEntry; i++)
		{
			String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
			String txtval = gridHelper.getGridColumnText(rowindex, i , 2);
			sf.assertEquals(txtval, "Checkbox");
			count++;	
			if(i==paginationObj.pageSizeDropval)
			{
				clickonNextButon();
				i=0;
			}
			if(count==totalEntry)
			{
				break;
			}
		}
		sf.assertAll();
	}
	public void verifyTextAreaFilter()
	{
		SoftAssert sf =new SoftAssert();		
		getpageval();
		int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
		int count=0;
		for(int i = 1; i <= totalEntry; i++)
		{
			String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
			String txtval = gridHelper.getGridColumnText(rowindex, i , 2);
			sf.assertEquals(txtval, "Textarea");
			count++;	
			if(i==paginationObj.pageSizeDropval)
			{
				clickonNextButon();
				i=0;
			}
			if(count==totalEntry)
			{
				break;
			}
		}
		sf.assertAll();
	}
	public void verifyDropdownFilter()
	{
		SoftAssert sf =new SoftAssert();		
		getpageval();
		int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
		int count=0;
		for(int i = 1; i <= totalEntry; i++)
		{
			String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
			String txtval = gridHelper.getGridColumnText(rowindex, i , 2);
			sf.assertEquals(txtval, "Dropdown");
			count++;	
			if(i==paginationObj.pageSizeDropval)
			{
				clickonNextButon();
				i=0;
			}
			if(count==totalEntry)
			{
				break;
			}
		}
		sf.assertAll();
	}
	public void verifyFileAttahcmentFilter()
	{
		SoftAssert sf =new SoftAssert();		
		int count=0;
		getpageval();

		int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );

		for(int i = 1; i <= totalEntry; i++)
		{
			String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
			String txtval = gridHelper.getGridColumnText(rowindex, i , 2);
			sf.assertEquals(txtval, "File Attachment");
			count++;	
			if(i==paginationObj.pageSizeDropval)
			{
				clickonNextButon();
				i=0;
			}
			if(count==totalEntry)
			{
				break;
			}
		}
		sf.assertAll();
	}
	public void EnterTexttoSearch(String searchText)
	{
		try
		{
			textboxHelper.sendKeys(textSearch, searchText);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	private void clickonNextButon()
	{
		WebElement linkElement = ObjectRepo.driver
				.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
		((JavascriptExecutor) ObjectRepo.driver).executeScript("arguments[0].click();", linkElement);
	}
	public void verifyQuestionSearchResult(String questionName)
	{


		SoftAssert sf =new SoftAssert();		
		int count=0;
		try
		{
			Thread.sleep(5000);
			getpageval();
			int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
			boolean flag=false;
			for(int i = 1; i <= totalEntry; i++)
			{
				String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
				String txtval = gridHelper.getGridColumnText(rowindex, i , 1);

				if(txtval.equals(questionName))
				{
					//sf.assertEquals(true,true);
					flag=true;
					break;
				}
				/*else
				{
					sf.assertEquals(flag, true, "The expected value doesn't found in grid");
				}*/

				count++;	
				if(i==paginationObj.pageSizeDropval && i>=10)
				{
					clickonNextButon();
					i=0;
				}
				if(count==totalEntry)
				{
					break;
				}
				/*if(!flag)
				{
					sf.assertEquals(flag, true, "The expected value doesn't found in grid");	
				}*/

			}
			Assert.assertEquals(flag, true, "The expected value doesn't found in grid");
		}
		catch(Exception e)
		{
			e.toString();
		}
		sf.assertAll();

	}
	public void verifyAutoCalculatedSearchResult(String questionName,String fileType)throws Exception
	{
		SoftAssert sf =new SoftAssert();		
		int count=0;
		Thread.sleep(5000);
		getpageval();
		int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
		boolean flag=false;
		for(int i = 1; i <= totalEntry; i++)
		{
			String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
			String colQuestion = gridHelper.getGridColumnText(rowindex, i , 1);
			String colType = gridHelper.getGridColumnText(rowindex, i , 2);
			if(colQuestion.equals(questionName) && colType.equals(fileType))
			{
				sf.assertEquals(true,true);
				flag=true;
				break;
			}
			count++;	
			if(i==paginationObj.pageSizeDropval && i>=10)
			{
				clickonNextButon();
				i=0;
			}
			if(count==totalEntry)
			{
				break;
			}			
		}
		if(!flag)
		{
			sf.assertEquals(flag, true, "The expected value doesn't found in grid");	
		}
		sf.assertAll();

	}
	public void verifyTextBoxSearchResult(String questionName,String fileType)throws Exception
	{
		SoftAssert sf =new SoftAssert();		
		int count=0;
		Thread.sleep(5000);
		getpageval();
		int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
		boolean flag=false;
		for(int i = 1; i <= totalEntry; i++)
		{
			String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
			String colQuestion = gridHelper.getGridColumnText(rowindex, i , 1);
			String colType = gridHelper.getGridColumnText(rowindex, i , 2);
			if(colQuestion.equals(questionName) && colType.equals(fileType))
			{
				sf.assertEquals(true,true);
				flag=true;
				break;
			}
			count++;	
			if(i==paginationObj.pageSizeDropval && i>=10)
			{
				clickonNextButon();
				i=0;
			}
			if(count==totalEntry)
			{
				break;
			}			
		}
		if(!flag)
		{
			sf.assertEquals(flag, true, "The expected value doesn't found in grid");	
		}
		sf.assertAll();

	}
	public void verifyCheckBoxSearchResult(String questionName,String fileType)throws Exception
	{
		SoftAssert sf =new SoftAssert();		
		int count=0;
		Thread.sleep(1000);
		getpageval();
		int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
		boolean flag=false;
		for(int i = 1; i <= totalEntry; i++)
		{
			String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
			String colQuestion = gridHelper.getGridColumnText(rowindex, i , 1);
			String colType = gridHelper.getGridColumnText(rowindex, i , 2);
			if(colQuestion.equals(questionName) && colType.equals(fileType))
			{
				sf.assertEquals(true,true);
				flag=true;
				break;
			}
			count++;	
			if(i==paginationObj.pageSizeDropval && i>=10)
			{
				clickonNextButon();
				i=0;
			}
			if(count==totalEntry)
			{
				break;
			}			
		}
		if(!flag)
		{
			sf.assertEquals(flag, true, "The expected value doesn't found in grid");	
		}
		sf.assertAll();
	}
	public void verifyTextAreaSearchResult(String questionName,String fileType)throws Exception
	{
		SoftAssert sf =new SoftAssert();		
		int count=0;
		Thread.sleep(5000);
		getpageval();
		int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
		boolean flag=false;
		for(int i = 1; i <= totalEntry; i++)
		{
			String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
			String colQuestion = gridHelper.getGridColumnText(rowindex, i , 1);
			String colType = gridHelper.getGridColumnText(rowindex, i , 2);
			if(colQuestion.equals(questionName) && colType.equals(fileType))
			{
				sf.assertEquals(true,true);
				flag=true;
				break;
			}
			count++;	
			if(i==paginationObj.pageSizeDropval && i>=10)
			{
				clickonNextButon();
				i=0;
			}
			if(count==totalEntry)
			{
				break;
			}			
		}
		if(!flag)
		{
			sf.assertEquals(flag, true, "The expected value doesn't found in grid");	
		}
		sf.assertAll();
	}
	public void verifyDropdownSearchResult(String questionName,String fileType)throws Exception
	{
		SoftAssert sf =new SoftAssert();		
		int count=0;
		Thread.sleep(5000);
		getpageval();
		int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
		boolean flag=false;
		for(int i = 1; i <= totalEntry; i++)
		{
			String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
			String colQuestion = gridHelper.getGridColumnText(rowindex, i , 1);
			String colType = gridHelper.getGridColumnText(rowindex, i , 2);
			if(colQuestion.equals(questionName) && colType.equals(fileType))
			{
				sf.assertEquals(true,true);
				flag=true;
				break;
			}
			count++;	
			if(i==paginationObj.pageSizeDropval && i>=10)
			{
				clickonNextButon();
				i=0;
			}
			if(count==totalEntry)
			{
				break;
			}			
		}
		if(!flag)
		{
			sf.assertEquals(flag, true, "The expected value doesn't found in grid");	
		}
		sf.assertAll();
	}
	public void verifyFileAttachmentSearchResult(String questionName,String fileType)throws Exception
	{
		SoftAssert sf =new SoftAssert();		
		int count=0;
		Thread.sleep(1000);
		getpageval();
		int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
		boolean flag=false;
		for(int i = 1; i <= totalEntry; i++)
		{
			String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
			String colQuestion = gridHelper.getGridColumnText(rowindex, i , 1);
			String colType = gridHelper.getGridColumnText(rowindex, i , 2);
			if(colQuestion.equals(questionName) && colType.equals(fileType))
			{
				sf.assertEquals(true,true);
				flag=true;
				break;
			}
			count++;	
			if(i==paginationObj.pageSizeDropval && i>=10)
			{
				clickonNextButon();
				i=0;
			}
			if(count==totalEntry)
			{
				break;
			}			
		}
		if(!flag)
		{
			sf.assertEquals(flag, true, "The expected value doesn't found in grid");	
		}
		sf.assertAll();
	}
	public void verifyQuestionTagSearchVal(String searchText,String questionName)
	{
		SoftAssert sf =new SoftAssert();
		try
		{

			String[] questionNames = questionName.split(",");

			int count=0;
			Thread.sleep(1000);
			getpageval();
			int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
			boolean flag=false;
			for(int i = 1; i <= totalEntry; i++)
			{
				String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
				String colQuestion = gridHelper.getGridColumnText(rowindex, i , 1);

				for(int j=0; j<questionNames.length; j++)
				{
					if(colQuestion.equals(questionNames[j]))
					{
						btnHelper.click(By.xpath("//a[text()='"+questionNames[j]+"']"));
						String srcText =driver.findElement(By.xpath("//div[text()='"+searchText+"']")).getText();
						if(searchText.equals(srcText))
						{
							sf.assertEquals(true,true);
							flag=true;
							Thread.sleep(1000);
							browserHelper.goBack();
							break;
						}
						else
						{
							sf.assertEquals(flag, true, "The expected value doesn't found in grid");
							Thread.sleep(1000);
							browserHelper.goBack();
						}
					}
				}
				count++;	
				if(i==paginationObj.pageSizeDropval && i>=10)
				{
					clickonNextButon();
					i=0;
				}
				if(count==totalEntry)
				{
					break;
				}			
			}
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
			e.toString();
		}
		sf.assertAll();
	}
	public void verifyAutocalQuestionTagSearchVal(String searchText,String questionName)
	{
		SoftAssert sf =new SoftAssert();
		try
		{
			String[] questionNames = questionName.split(",");
			int count=0;
			Thread.sleep(1000);
			getpageval();
			int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
			boolean flag=false;
			for(int i = 1; i <= totalEntry; i++)
			{
				String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
				String colQuestion = gridHelper.getGridColumnText(rowindex, i , 1);

				for(int j=0; j<questionNames.length; j++)
				{
					if(colQuestion.equals(questionNames[j]))
					{
						btnHelper.click(By.xpath("//a[text()='"+questionNames[j]+"']"));
						String srcText =driver.findElement(By.xpath("//div[text()='"+searchText+"']")).getText();
						if(searchText.equals(srcText))
						{
							sf.assertEquals(true,true);
							flag=true;
							Thread.sleep(1000);
							browserHelper.goBack();
							break;
						}
						else
						{
							sf.assertEquals(flag, true, "The expected value doesn't found in grid");
							Thread.sleep(1000);
							browserHelper.goBack();
						}
					}
				}
				count++;	
				if(i==paginationObj.pageSizeDropval && i>=10)
				{
					clickonNextButon();
					i=0;
				}
				if(count==totalEntry)
				{
					break;
				}			
			}
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
			e.toString();
		}

		sf.assertAll();
	}
	public void verifyTextBoxlQuestionTagSearchVal(String searchText,String questionName)
	{
		SoftAssert sf =new SoftAssert();
		try
		{
			String[] questionNames = questionName.split(",");
			int count=0;
			Thread.sleep(1000);
			getpageval();
			int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
			boolean flag=false;
			for(int i = 1; i <= totalEntry; i++)
			{
				String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
				String colQuestion = gridHelper.getGridColumnText(rowindex, i , 1);

				for(int j=0; j<questionNames.length; j++)
				{
					if(colQuestion.equals(questionNames[j]))
					{
						btnHelper.click(By.xpath("//a[text()='"+questionNames[j]+"']"));
						String srcText =driver.findElement(By.xpath("//div[text()='"+searchText+"']")).getText();
						if(searchText.equals(srcText))
						{
							sf.assertEquals(true,true);
							flag=true;
							Thread.sleep(1000);
							browserHelper.goBack();
							break;
						}
						else
						{
							sf.assertEquals(flag, true, "The expected value doesn't found in grid");
							Thread.sleep(1000);
							browserHelper.goBack();
						}
					}
				}
				count++;	
				if(i==paginationObj.pageSizeDropval && i>=10)
				{
					clickonNextButon();
					i=0;
				}
				if(count==totalEntry)
				{
					break;
				}			
			}
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
			e.toString();
		}

		sf.assertAll();
	}
	public void verifyCheckboxlQuestionTagSearchVal(String searchText,String questionName)
	{
		SoftAssert sf =new SoftAssert();
		try
		{
			String[] questionNames = questionName.split(",");
			int count=0;
			Thread.sleep(1000);
			getpageval();
			int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
			boolean flag=false;
			for(int i = 1; i <= totalEntry; i++)
			{
				String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
				String colQuestion = gridHelper.getGridColumnText(rowindex, i , 1);

				for(int j=0; j<questionNames.length; j++)
				{
					if(colQuestion.equals(questionNames[j]))
					{
						btnHelper.click(By.xpath("//a[text()='"+questionNames[j]+"']"));
						String srcText =driver.findElement(By.xpath("//div[text()='"+searchText+"']")).getText();
						if(searchText.equals(srcText))
						{
							sf.assertEquals(true,true);
							flag=true;
							Thread.sleep(1000);
							browserHelper.goBack();
							break;
						}
						else
						{
							sf.assertEquals(flag, true, "The expected value doesn't found in grid");
							Thread.sleep(1000);
							browserHelper.goBack();
						}
					}
				}
				count++;	
				if(i==paginationObj.pageSizeDropval && i>=10)
				{
					clickonNextButon();
					i=0;
				}
				if(count==totalEntry)
				{
					break;
				}			
			}
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
			e.toString();
		}

		sf.assertAll();
	}
	public void verifyTextareaQuestionTagSearchVal(String searchText,String questionName)
	{
		SoftAssert sf =new SoftAssert();
		try
		{
			String[] questionNames = questionName.split(",");
			int count=0;
			Thread.sleep(1000);
			getpageval();
			int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
			boolean flag=false;
			for(int i = 1; i <= totalEntry; i++)
			{
				String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
				String colQuestion = gridHelper.getGridColumnText(rowindex, i , 1);

				for(int j=0; j<questionNames.length; j++)
				{
					if(colQuestion.equals(questionNames[j]))
					{
						btnHelper.click(By.xpath("//a[text()='"+questionNames[j]+"']"));
						String srcText =driver.findElement(By.xpath("//div[text()='"+searchText+"']")).getText();
						if(searchText.equals(srcText))
						{
							sf.assertEquals(true,true);
							flag=true;
							Thread.sleep(1000);
							browserHelper.goBack();
							break;
						}
						else
						{
							sf.assertEquals(flag, true, "The expected value doesn't found in grid");
							Thread.sleep(1000);
							browserHelper.goBack();
						}
					}
				}
				count++;	
				if(i==paginationObj.pageSizeDropval && i>=10)
				{
					clickonNextButon();
					i=0;
				}
				if(count==totalEntry)
				{
					break;
				}			
			}
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
			e.toString();
		}

		sf.assertAll();
	}
	public void verifyFileAttachmentQuestionTagSearchVal(String searchText,String questionName)
	{
		SoftAssert sf =new SoftAssert();
		try
		{
			String[] questionNames = questionName.split(",");
			int count=0;
			Thread.sleep(1000);
			getpageval();
			int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
			boolean flag=false;
			for(int i = 1; i <= totalEntry; i++)
			{
				String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
				String colQuestion = gridHelper.getGridColumnText(rowindex, i , 1);

				for(int j=0; j<questionNames.length; j++)
				{
					if(colQuestion.equals(questionNames[j]))
					{
						btnHelper.click(By.xpath("//a[text()='"+questionNames[j]+"']"));
						String srcText =driver.findElement(By.xpath("//div[text()='"+searchText+"']")).getText();
						if(searchText.equals(srcText))
						{
							sf.assertEquals(true,true);
							flag=true;
							Thread.sleep(1000);
							browserHelper.goBack();
							break;
						}
						else
						{
							sf.assertEquals(flag, true, "The expected value doesn't found in grid");
							Thread.sleep(1000);
							browserHelper.goBack();
						}
					}
				}
				count++;	
				if(i==paginationObj.pageSizeDropval && i>=10)
				{
					clickonNextButon();
					i=0;
				}
				if(count==totalEntry)
				{
					break;
				}			
			}
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
			e.toString();
		}

		sf.assertAll();
	}
	public void verifyDropdownQuestionTagSearchVal(String searchText,String questionName)
	{
		SoftAssert sf =new SoftAssert();
		try
		{
			String[] questionNames = questionName.split(",");
			int count=0;
			Thread.sleep(1000);
			getpageval();
			int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
			boolean flag=false;
			for(int i = 1; i <= totalEntry; i++)
			{
				String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
				String colQuestion = gridHelper.getGridColumnText(rowindex, i , 1);

				for(int j=0; j<questionNames.length; j++)
				{
					if(colQuestion.equals(questionNames[j]))
					{
						btnHelper.click(By.xpath("//a[text()='"+questionNames[j]+"']"));
						String srcText =driver.findElement(By.xpath("//div[text()='"+searchText+"']")).getText();
						if(searchText.equals(srcText))
						{
							sf.assertEquals(true,true);
							flag=true;
							Thread.sleep(1000);
							browserHelper.goBack();
							break;
						}
						else
						{
							sf.assertEquals(flag, true, "The expected value doesn't found in grid");
							Thread.sleep(1000);
							browserHelper.goBack();
						}
					}
				}
				count++;	
				if(i==paginationObj.pageSizeDropval && i>=10)
				{
					clickonNextButon();
					i=0;
				}
				if(count==totalEntry)
				{
					break;
				}			
			}
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
			e.toString();
		}

		sf.assertAll();
	}
	public void enterSearchValtoRemove(String searchText)
	{
		SoftAssert sf =new SoftAssert();
		try
		{
			obj = new QuestionsPageObject(ObjectRepo.driver);
			paginationVal =textboxHelper.getText(obj.paginationText); 
			String[] splited = paginationVal.split("\\s+");
			searchBeforeCount = Integer.parseInt(splited[6]);
			textboxHelper.sendKeys(textSearch, searchText);
			int count=0;
			Thread.sleep(2000);
			getpageval();
			int totalEntry = paginationObj.paginationverification(paginationVal,PaginationSelectedVal );
			boolean flag=false;
			for(int i = 1; i <= totalEntry; i++)
			{
				String rowindex = ObjectRepo.driver.findElement(By.xpath("//table/tbody")).getText();
				String colQuestion = gridHelper.getGridColumnText(rowindex, i , 1);
				String colType = gridHelper.getGridColumnText(rowindex, i , 2);
				if(colQuestion.equals(searchText))
				{
					sf.assertEquals(true,true);
					flag=true;
					break;
				}
				count++;	
				if(i==paginationObj.pageSizeDropval && i>=10)
				{
					clickonNextButon();
					i=0;
				}
				if(count==totalEntry)
				{
					break;
				}			
			}
			if(!flag)
			{
				sf.assertEquals(flag, true, "The expected value doesn't found in grid");	
			}

		}
		catch(Exception e)
		{
			e.toString();
		}
		sf.assertAll();
	}
	public void clickonRemoveIcon()
	{

		btnHelper.click(iconRemoveSearch);

	}
	public void verifyRemoveSearch()
	{
		try
		{	
			paginationVal =textboxHelper.getText(obj.paginationText); //obj.paginationText.getText();
			String[] splited = paginationVal.split("\\s+");
			searchAfterCount = Integer.parseInt(splited[6]);
		}
		catch(Exception e)
		{
			e.toString();
		}
		Assert.assertEquals(searchAfterCount, searchBeforeCount);
	}
	public void selectPageSizeTen()
	{
		getpageval();
		try
		{
			drpHelper.SelectUsingIndex(pageObject.pageSizeDrpDwn, 0);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyPagesizeten()
	{
		SoftAssert sf = new SoftAssert();
		try
		{
			String pageSizeLabel = textboxHelper.getText(pagesizelabel);	
			sf.assertEquals(pageSizeLabel.substring(0,10),"Page Size:");
			sf.assertEquals(pageSizeLabel.substring(11,13),"10");
			paginationVal =textboxHelper.getText(obj.paginationText); //obj.paginationText.getText();
			pageObject.clickLastBtnPagination();
			sf.assertEquals(textboxHelper.getText(lastpageval), String.valueOf((int)(Math.ceil((double) Integer.parseInt(paginationVal.split("\\s+")[6])/10))));
		}
		catch(Exception e)
		{
			e.toString();
		}
		sf.assertAll();
	}
	public void selectPageSizeTwenty()
	{
		getpageval();
		try
		{
			drpHelper.SelectUsingIndex(pageObject.pageSizeDrpDwn, 1);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyPagesizetwenty()
	{
		SoftAssert sf = new SoftAssert();
		try
		{
			String pageSizeLabel = textboxHelper.getText(pagesizelabel);	
			sf.assertEquals(pageSizeLabel.substring(0,10),"Page Size:");
			sf.assertEquals(pageSizeLabel.substring(11,13),"20");
			paginationVal =textboxHelper.getText(obj.paginationText); //obj.paginationText.getText();
			pageObject.clickLastBtnPagination();
			sf.assertEquals(textboxHelper.getText(lastpageval), String.valueOf((int)(Math.ceil((double) Integer.parseInt(paginationVal.split("\\s+")[6])/20))));
		}
		catch(Exception e)
		{
			e.toString();
		}
		sf.assertAll();
	}
	public void selectPageSizeFifty()
	{
		getpageval();
		try
		{
			drpHelper.SelectUsingIndex(pageObject.pageSizeDrpDwn, 2);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyPagesizeFifty()
	{
		SoftAssert sf = new SoftAssert();
		try
		{
			String pageSizeLabel = textboxHelper.getText(pagesizelabel);	
			sf.assertEquals(pageSizeLabel.substring(0,10),"Page Size:");
			sf.assertEquals(pageSizeLabel.substring(11,13),"50");
			paginationVal =textboxHelper.getText(obj.paginationText); //obj.paginationText.getText();
			pageObject.clickLastBtnPagination();
			sf.assertEquals(textboxHelper.getText(lastpageval), String.valueOf((int)(Math.ceil((double) Integer.parseInt(paginationVal.split("\\s+")[6])/50))));
		}
		catch(Exception e)
		{
			e.toString();
		}
		sf.assertAll();
	}

}