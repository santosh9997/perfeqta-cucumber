package com.cucumber.framework.helper.PageObject;

import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.Text;

import org.apache.log4j.Logger;
import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.ClickAction;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Pagination.Pagination;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import gherkin.lexer.Th;

public class AppsPageObject extends PageBase {
	private WebDriver driver;
	private Helper helper = new Helper();
	public ButtonHelper btnHelper;
	public TextBoxHelper textBoxHelper;
	private final Logger log = LoggerHelper.getLogger(SitesPageObject.class);
	private Search sObj;
	private WaitHelper waitObj = new WaitHelper(ObjectRepo.driver, ObjectRepo.reader);
	public AppBuilderPageObject appPageObj;
	public static String searchDin,searchDemo_MJ;
	WebDriverWait wait =new WebDriverWait (ObjectRepo.driver,60);
	public AppsPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(ObjectRepo.driver);
		textBoxHelper = new TextBoxHelper(ObjectRepo.driver);
		sObj = new Search();
	}

	@FindBy(how = How.XPATH, using = "//*[@class='ng-binding'][text()='Apps']")
	public WebElement appsTile;

	@FindBy(how = How.XPATH, using = "//div[@style='font-weight: bold;margin-bottom: 4px;font-size: 17px;']")
	public WebElement workflowLabel;

	@FindBy(how = How.XPATH, using = "//*[@name='searchTxt']")
	public WebElement searchAppTextBox;

	@FindBy(how = How.XPATH, using = "//*[@class='ng-binding'][text()='Other Apps']")
	public WebElement otherApptile;

	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.goToEntry(form)'])[1]")
	public WebElement firstApp;

	@FindBy(how = How.XPATH, using = "(//*[@type='text'])[4]")
	public WebElement enterDIN;

	@FindBy(how = How.XPATH, using = "(//*[@type='text'])[5]")
	public WebElement enterLotNumber;

	@FindBy(how = How.XPATH, using = "(//*[@type='text'])[11]")
	public WebElement enterFirstName;

	@FindBy(how = How.XPATH, using = "(//*[@type='number'])[2]")
	public WebElement enterPhoneNo;

	@FindBy(how = How.XPATH, using = "//*[@type='submit']")
	public WebElement saveAndAcceptBtn;

	@FindBy(how = How.XPATH, using = "//*[@style='display: block;']")
	public WebElement succesfullyPass;

	@FindBy(how = How.XPATH, using = "//*[@class='confirm']")
	public WebElement okBtnPopUp;

	@FindBy(how = How.XPATH, using = "//*[@class='confirm']")
	public WebElement entryIsCorrectBtn;

	@FindBy(how = How.XPATH, using = "(//*[@class='btn btn-default Single_QC_Form_Entries_Date_Width'])[1]")
	public WebElement collectionDatePicker;

	@FindBy(how = How.XPATH, using = "//*[@ng-disabled=\"isDisabled('today')\"]")
	public WebElement todayDateBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='glyphicon glyphicon-edit text-primary']")
	public WebElement assigneeIcon;

	@FindBy(how = How.XPATH, using = "(//*[@ng-class='settings.buttonClasses'])[2]")
	public WebElement assigneeDrpDwn;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-model='searchFilter'])[2]")
	public WebElement assigneeDrpDwnSrchBox;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.assign(changeAssigneeForm)']")
	public WebElement assignBtn;
	//------------------------------------------Methods------------------------------------

	public void clickOnAppTile() throws Exception {
		Thread.sleep(2000);
		btnHelper.click(appsTile);

	}

	public void clickOnSearcgAppTextBox() throws Exception {
		btnHelper.click(searchAppTextBox);

	}

	//change
	public void enterAppName(String appname) throws Exception {
		textBoxHelper.sendKeys(searchAppTextBox, appname);
		System.out.println("aaaa" + appname);
	}

	public void clickOtherApp() throws Exception {
		btnHelper.click(otherApptile);


	}

	public void clickOnFirstApp() throws Exception {
		btnHelper.click(firstApp);	
	}

	public void enterNecessaryDatatoApp(String din, String lotNumber, String FirstName, String PhoneNo)
			throws Exception {
		
		btnHelper.click(collectionDatePicker);
		btnHelper.click(todayDateBtn);
		textBoxHelper.sendKeys(enterDIN, din);
		searchDin = din;
		textBoxHelper.sendKeys(enterLotNumber, lotNumber);
		searchDemo_MJ = lotNumber;
		Thread.sleep(1000);
		textBoxHelper.sendKeys(enterFirstName, FirstName);
		textBoxHelper.sendKeys(enterPhoneNo, PhoneNo);
	}

	public void clickOnSaveAndAcceptBtn() throws Exception {
		btnHelper.click(saveAndAcceptBtn);
		btnHelper.click(okBtnPopUp);
		waitObj.waitForElementVisible(appsTile);
	}

	public void verifyAppPassedMsg() throws Exception {
		SoftAssert sf = new SoftAssert();
		sf.assertEquals(textBoxHelper.getText(succesfullyPass), ObjectRepo.reader.getSuccesfullyPass());
		btnHelper.click(okBtnPopUp);
		sf.assertAll();
	}

	public void enterNecessaryInValDatatoApp(String din , String lotNumber , String FirstName , String PhoneNo) throws Exception {
		btnHelper.click(collectionDatePicker);
		btnHelper.click(todayDateBtn);
		textBoxHelper.sendKeys(enterDIN,din);
		searchDin=din;
		textBoxHelper.sendKeys(enterLotNumber,lotNumber);
		searchDemo_MJ=lotNumber;
		Thread.sleep(1000);
		textBoxHelper.sendKeys(enterPhoneNo,PhoneNo);
		textBoxHelper.sendKeys(enterFirstName,FirstName);
		enterFirstName.sendKeys(Keys.TAB);
		btnHelper.click(entryIsCorrectBtn);
		Thread.sleep(1000);
		System.err.println(PhoneNo);
	}
	protected void waitThread() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}	
	}	
	public void selectCurnUserAsAssignee() throws Exception {
		btnHelper.click(assigneeIcon);
		btnHelper.click(assigneeDrpDwn);
		
		 ExcelUtils loginExcel= new ExcelUtils();
	     loginExcel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Login");
	      String username = ExcelUtils.readXLSFile("Login", 2, 1);  
		System.out.println("user Name: "+ username);

		textBoxHelper.sendKeys(assigneeDrpDwnSrchBox, username);
		

		List<WebElement> list = driver.findElements(By.xpath("//label[@class='ng-binding']"));

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getText().equals(username)) {
				 driver.findElement(By.xpath("(//label[@class='ng-binding'])[" + (i+1) + "]")).click();
				break;
			}
		}
		btnHelper.click(assignBtn);
		waitThread();
	}
}
