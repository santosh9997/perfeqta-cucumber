package com.cucumber.framework.helper.PageObject;


import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.text.StyledEditorKit.BoldAction;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.CheckBox.CheckBoxOrRadioButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.lu.a;
import gherkin.lexer.Th;

import com.cucumber.framework.helper.Sorting.SortingOnColumn;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;

public class SetFrequencyPageObject extends PageBase
{
	private WebDriver driver;
	public ButtonHelper btnHelper;
	public CheckBoxOrRadioButtonHelper checkBoxAndBtnHelper;
	public TextBoxHelper textBoxHelper;
	public DropDownHelper drpdwnHelper;
	private final Logger log = LoggerHelper.getLogger(SetFrequencyPageObject.class);
	private SortingOnColumn sortColumn;
	private Search search;
	private Scroll scroll;
	private CommonFunctionPageObject commonFnPageObject;

	public SetFrequencyPageObject(WebDriver driver)
	{
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		checkBoxAndBtnHelper = new CheckBoxOrRadioButtonHelper(driver);
		textBoxHelper = new TextBoxHelper(driver);
		drpdwnHelper = new DropDownHelper(driver);
		sortColumn = new SortingOnColumn();
		search = new Search();
		scroll = new Scroll();
	}

	/** WebElements */

	@FindBy(how = How.LINK_TEXT, using = "Set Frequency")
	public WebElement setFreqRecdsTile;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement setFreqRecdsLabl;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.textSearch']")
	public WebElement frequencySrchBox;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-primary add-new pull-right ng-binding']")
	public WebElement addNewBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement addEditFreLabl;
	
	@FindBy(how = How.XPATH, using = "//*[@name='frequencyNameTextBox']")
	public WebElement freNameTextBox;
	
	@FindBy(how = How.XPATH, using = "//*[@class='help-block error-message-color ng-scope']")
	public WebElement freNameTextBoxValMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schedule.edit.FREQUENCYNAMESHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement freNameTextBoxValMsgForMinSize;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schedule.edit.FREQUENCYNAMESHOULDNOTBEMORETHAN50CHARACTERSLONG']")
	public WebElement freNameTextBoxValMsgForMaxSize;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schedule.edit.FREQUENCYNAMEMUSTBEUNIQUE']")
	public WebElement freNameTextBoxValMsgForDup;
	
	@FindBy(how = How.XPATH, using = "//*[@name='qcFormSelect']")
	public WebElement appDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='text.ps2']")
	public WebElement appDrpDwnValMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@value='master']")
	public WebElement radioBtnMasterApp;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.schedules.master._id']")
	public WebElement masterAppDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.masterqcsettings.edit.MASTERAPPR']")
	public WebElement masterAppDrpDwnValMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@class=\"Frequency_app_text22\"][text()='Hourly']")
	public WebElement hourlyLabl;
	
	@FindBy(how = How.XPATH, using = "//*[@class=\"Frequency_app_text22\"][text()='Daily']")
	public WebElement dailyLabl;
	
	@FindBy(how = How.XPATH, using = "//*[@class=\"Frequency_app_text22\"][text()='Weekly']")
	public WebElement weeklyLabl;
	
	@FindBy(how = How.XPATH, using = "//*[@class=\"Frequency_app_text22\"][text()='Monthly']")
	public WebElement monthlyLabl;
	
	@FindBy(how = How.XPATH, using = "//*[@name='hourlyTextBox']")
	public WebElement hourlyTextbox;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.schedule.edit.EVERYHOURISREQUIRED']")
	public WebElement hourlyTextboxValMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.qualificationPerformance.edit.createwindow.INVALIDNUMBERCANNOTADDMORETHAN24HOURS']")
	public WebElement hourlyTextboxValMsgForMax;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.importdata.automated.Invalidn']")
	public WebElement everyTextboxValMsgForString;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Weekly']")
	public WebElement weeklyRadBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@name='weeklyTextBox']")
	public WebElement weeklyTextbox;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.importdata.automated.Everyweekr']")
	public WebElement weeklyTextboxValMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.threshold.edit.INVALID10']")
	public WebElement weeklyTextboxValMsgForMax;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Daily']")
	public WebElement dailyRadBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@name='dailyTextBox']")
	public WebElement dailyTextbox;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.importdata.automated.Everydayr']")
	public WebElement dailyTextboxValMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.threshold.edit.CANNOT31']")
	public WebElement dailyTextboxValMsgForMax;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-model='vm.schedules.occurenceState'])[2]")
	public WebElement endAfterRadioBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@name='endAfterTextBox']")
	public WebElement endAfterTextbox;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.importdata.automated.EndAR']")
	public WebElement endAfterTextboxValMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='text.INVALIDNUMBERCANNOTBEMORE9999']")
	public WebElement endAfterTextboxValMsgForMax;
	
	@FindBy(how = How.XPATH, using = "(//*[@translate='secure.importdata.automated.Invalidn'])[2]")
	public WebElement endAfterTextboxValMsgForInNum;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='toggleMeridian()' ]")
	public WebElement amPm;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-model='hours' and @ng-change='updateHours()'] ")
	public WebElement hhTxtBox;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-model='minutes']")
	public WebElement mmTxtBox;
	
	@FindBy(how = How.XPATH, using = "//*[@name='sendEmailCheckBox']")
	public WebElement emailRemCheckBox;
	
	@FindBy(how = How.XPATH, using = "//*[@name='sendEmailTextBox']")
	public WebElement emailRemTextBox;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.myprofile.EMAILREQ']")
	public WebElement emailRemTextBoxValMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.myprofile.INVALIDEMAIL']")
	public WebElement emailRemTextBoxValMsgInVal;
	
	@FindBy(how = How.XPATH, using = "//*[@class='dropdown-toggle ng-binding btn btn-default']")
	public WebElement selectAppRole;
	
	@FindBy(how = How.XPATH, using = "//*[@data-ng-click='selectAll()']")
	public WebElement checkRoll;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.userSelectionType && vm.rolesValue.length']")
	public WebElement addRole;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-show='vm.isValidUser && vm.isValidUserforEdit && !vm.schedules.scheduleInProcess']")
	public WebElement saveBtn;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.go(field,row,vm.resource);'])[1]")
	public WebElement findRecentFreName;



	// -----------------------------------public methods ----------------------------------------------------------
	public void setFreqRecdsTileClick() throws Exception {
		//Thread.sleep(1000);
		btnHelper.click(setFreqRecdsTile);
	}

	public void verifySetFreLabl() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(setFreqRecdsLabl), ObjectRepo.reader.getSetFreqRecdsLabl());
	}

	public void frequencySrchBoxSendkeys(String SrchValue) {
		btnHelper.click(frequencySrchBox);
		textBoxHelper.sendKeys(frequencySrchBox,SrchValue);
	}

	public void verifysearch(String searchedItem) throws Exception {

		AttributesPageObject obj = new AttributesPageObject(ObjectRepo.driver);

		String PaginationVal = null, PaginationSelectedVal = null, srchResultNo = null;
		try {
			PaginationVal = textBoxHelper.getText(obj.paginationText);
			srchResultNo = textBoxHelper.getText(obj.srcResultPageNo);
			PaginationSelectedVal = obj.pageSizeDrp_GetSelectedValue();
			new Search().SearchVerification(PaginationVal, PaginationSelectedVal, obj.grid_data, searchedItem,obj.textSearch, srchResultNo);
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	public void clickAddNewBtn() throws Exception {
		btnHelper.click(addNewBtn);
		//Thread.sleep(1000);
	}
	
	public void verifyAddEditFreLabl() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(addEditFreLabl), ObjectRepo.reader.getAddEditFreLabl());
	}
	
	public void clickfrencyNameTextBox() throws Exception {
		btnHelper.click(freNameTextBox);
	//	Thread.sleep(1000);
	}
	
	public void verifyFrencyNameTexBoxValMsgl() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(freNameTextBoxValMsg), ObjectRepo.reader.getFreNameTextBoxValMsg());
	}
	
	public void frequencyNameSendkeys(String FrqName) {
		textBoxHelper.sendKeys(freNameTextBox,FrqName);
	}
	
	public void verifyFrencyNameValMsglForMinSize() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(freNameTextBoxValMsgForMinSize), ObjectRepo.reader.getFreNameTextBoxValMsgForMinSize());
	}
	
	public void verifyFrencyNameValMsglForMaxSize() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(freNameTextBoxValMsgForMaxSize), ObjectRepo.reader.getFreNameTextBoxValMsgForMaxSize());
	}
	
	public void verifyFrencyNameValMsglForDup() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(freNameTextBoxValMsgForDup), ObjectRepo.reader.getFreNameTextBoxValMsgForDup());
	}
	
	public void verifyFrencyNameValMsglForVal() throws Exception {
		boolean actual;
		boolean validName = driver.findElements(By.xpath("//*[@class='help-block error-message-color ng-scope']")).size()!=0;
		boolean validName1 = driver.findElements(By.xpath("//*[@translate='secure.admin.schedule.edit.FREQUENCYNAMESHOULDNOTBEMORETHAN50CHARACTERSLONG']")).size()!=0;
		if(validName == false && validName1 == false ) {
			actual = false ;
		}
		else {
			actual = true;
		}
		
		Assert.assertEquals(actual, false);	
	}
	
	public void selectDrpDwnApp() throws Exception {
//		Select drpDwn = new Select(appDrpDwn);
//		drpDwn.selectByIndex(0);
		drpdwnHelper.selectDropDownIndex(appDrpDwn, 0);
	}
	
	public void verifyAppDrpDwnValMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(appDrpDwnValMsg), ObjectRepo.reader.getAppRequireValMsg());
	}
	
	public void clickMasAppRadBtn() throws Exception {
		btnHelper.click(radioBtnMasterApp);
		//Thread.sleep(1000);
	}
	
	public void selectDrpDwnMasterApp() throws Exception {
//		Select drpDwn = new Select(masterAppDrpDwn);
//		drpDwn.selectByIndex(0);
		drpdwnHelper.selectDropDownIndex(masterAppDrpDwn, 0);
	}
	
	public void verifyMasAppDrpDwnValMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(masterAppDrpDwnValMsg), ObjectRepo.reader.getMasterAppDrpDwnValMsg());
	}
	
	public void verifyHourlyLabl() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(hourlyLabl), ObjectRepo.reader.getHourlyLabl());
	}
	
	public void verifyDailyLabl() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(dailyLabl), ObjectRepo.reader.getDailyLabl());
	}
	
	public void verifyWeeklyLabl() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(weeklyLabl), ObjectRepo.reader.getWeeklyLabl());
	}
	
	public void verifyMonthlyLabl() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(monthlyLabl), ObjectRepo.reader.getMonthlyLabl());
	}
	
	public void clickhourlyTextbox() throws Exception {
		btnHelper.click(hourlyTextbox);
	}
	
	public void verifyhourlyTextBoxValMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(hourlyTextboxValMsg), ObjectRepo.reader.getHoursRequireValMsg());
	}
	
	public void enterhourlyTextbox(String hour) throws Exception {
		textBoxHelper.sendKeys(hourlyTextbox,hour);
	}
	
	public void verifyhourlyTextBoxValMsgForMax() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(hourlyTextboxValMsgForMax), ObjectRepo.reader.getHourlyTextboxValMsgForMax());
	}
	
	public void verifyEveryTextBoxValMsgForString() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(everyTextboxValMsgForString), ObjectRepo.reader.getInvalidNumberValMsg());
	}
	
	public void verifyHourlytextboxValMsglForValData() throws Exception {
		boolean actual;
		boolean noValue = driver.findElements(By.xpath("//*[@translate='secure.admin.schedule.edit.EVERYHOURISREQUIRED']")).size()!=0;
		boolean maxValue = driver.findElements(By.xpath("//*[@translate='secure.qualificationPerformance.edit.createwindow.INVALIDNUMBERCANNOTADDMORETHAN24HOURS']")).size()!=0;
		boolean stringValue = driver.findElements(By.xpath("//*[@translate='secure.importdata.automated.Invalidn']")).size()!=0;
		if(noValue == false && maxValue == false && stringValue == true ) {
			actual = false ;
		}
		else {
			actual = true;
		}
		
		Assert.assertEquals(actual, false);	
	}
	
	public void selectWeeklyRadBtn() throws Exception {
		btnHelper.click(weeklyRadBtn);
	//	Thread.sleep(1000);
	}
	
	public void clickWeeklyTextbox() throws Exception {
		btnHelper.click(weeklyTextbox);
	}
	
	public void verifyweeklyTextBoxValMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(weeklyTextboxValMsg), ObjectRepo.reader.getWeeklyTxtBoxRqiMsg());
	}
	
	public void enterweeklyTextbox(String week) throws Exception {
		textBoxHelper.sendKeys(weeklyTextbox,week);
	}
	
	public void verifyweeklyTextBoxValMsgForMax() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(weeklyTextboxValMsgForMax), ObjectRepo.reader.getWeeklyTxtBoxLimitMsg());
	}
	
	public void verifyWeeklytextboxValMsglForValData() throws Exception {
		boolean actual;
		boolean noValue = driver.findElements(By.xpath("//*[@translate='secure.importdata.automated.Everyweekr']")).size()!=0;
		boolean maxValue = driver.findElements(By.xpath("//*[@translate='secure.threshold.edit.INVALID10']")).size()!=0;
		boolean stringValue = driver.findElements(By.xpath("//*[@translate='secure.importdata.automated.Invalidn']")).size()!=0;
		if(noValue == false && maxValue == false && stringValue == false ) {
			actual = false ;
		}
		else {
			actual = true;
		}
		
		Assert.assertEquals(actual, false);	
	}
	
	public void selectDailyRadBtn() throws Exception {
		btnHelper.click(dailyRadBtn);
		//Thread.sleep(1000);
	}
	
	public void clickDailyTextbox() throws Exception {
		btnHelper.click(dailyTextbox);
	}
	
	public void verifyDailyTextBoxValMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(dailyTextboxValMsg), ObjectRepo.reader.getDailyTextboxValMsg());
	}
	
	public void enterDailyTextbox(String days) throws Exception {
		textBoxHelper.sendKeys(dailyTextbox,days);
	}
	
	public void verifyDailyTextBoxValMsgForMax() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(dailyTextboxValMsgForMax), ObjectRepo.reader.getDailyTxtBoxLimitMsg());
	}
	
	public void verifyDailytextboxValMsglForValData() throws Exception {
		boolean actual;
		boolean noValue = driver.findElements(By.xpath("//*[@translate='secure.importdata.automated.Everydayr']")).size()!=0;
		boolean maxValue = driver.findElements(By.xpath("//*[@translate='secure.threshold.edit.CANNOT31']")).size()!=0;
		boolean stringValue = driver.findElements(By.xpath("//*[@translate='secure.importdata.automated.Invalidn']")).size()!=0;
		if(noValue == false && maxValue == false && stringValue == true ) {
			actual = false ;
		}
		else {
			actual = true;
		}
		
		Assert.assertEquals(actual, false);	
	}
	
	public void selectEndAfterRadBtn() throws Exception {
		btnHelper.click(endAfterRadioBtn);
	//	Thread.sleep(1000);
	}
	
	public void clickEndAfterTextBtn() throws Exception {
		btnHelper.click(endAfterTextbox);
		//Thread.sleep(1000);
	}
	
	public void verifyEndAfterTextBoxValMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(endAfterTextboxValMsg), ObjectRepo.reader.getEndAfterTextboxValMsg());
	}
	
	public void enterEndAfterTextbox(String endAfter) throws Exception {
		textBoxHelper.sendKeys(endAfterTextbox,endAfter);
	}
	
	public void verifyEndAfterTextBoxValMsgForMax() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(endAfterTextboxValMsgForMax), ObjectRepo.reader.getEndAfterTextboxValMsgForMax());
	}
	
	public void verifyEndAfterTextBoxValMsgFoInNum() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(endAfterTextboxValMsgForInNum), ObjectRepo.reader.getInvalidNumberValMsg());
	}
	
	public void verifyEndAftertextboxValMsglForValData() throws Exception {
		boolean actual;
		boolean noValue = driver.findElements(By.xpath("//*[@translate='secure.importdata.automated.EndAR']")).size()!=0;
		boolean maxValue = driver.findElements(By.xpath("//*[@translate='text.INVALIDNUMBERCANNOTBEMORE9999']")).size()!=0;
		boolean stringValue = driver.findElements(By.xpath("(//*[@translate='secure.importdata.automated.Invalidn'])[2]")).size()!=0;
		if(noValue == false && maxValue == false && stringValue == false ) {
			actual = false ;
		}
		else {
			actual = true;
		}
		
		Assert.assertEquals(actual, false);	
	}
	
	public void verifyCurrentTime() throws Exception {
		
		driver.navigate().refresh();
		//ExtentBase.reportLable("Expected Time: " + DateTimeFormatter.ofPattern("hh:mm a").format(LocalTime.now()));
		String AmPm = textBoxHelper.getText(amPm);
		Actions hh = new Actions(driver);
		hh.sendKeys(hhTxtBox, Keys.chord(Keys.CONTROL, "a")).perform();
		hh.sendKeys(Keys.chord(Keys.CONTROL, "c")).perform();
		Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
		String HH = (String) c.getData(DataFlavor.stringFlavor);
		Actions mm = new Actions(driver);
		mm.sendKeys(mmTxtBox, Keys.chord(Keys.CONTROL, "a")).perform();
		mm.sendKeys(Keys.chord(Keys.CONTROL, "c")).perform();
		Clipboard c1 = Toolkit.getDefaultToolkit().getSystemClipboard();
		String MM = (String) c1.getData(DataFlavor.stringFlavor);
		//ExtentBase.reportLable("Actual Time: " + HH + ":" + MM + " " + AmPm);
		String Actual = (HH + ":" + MM + " " + AmPm);
		//ExtentBase.reportLable(Actual);
		Assert.assertEquals(Actual, DateTimeFormatter.ofPattern("hh:mm a").format(LocalTime.now()));
	}
	
	public void selectSendEmailCheckBox() throws Exception {
		btnHelper.click(emailRemCheckBox);
		//Thread.sleep(1000);
	}
	
	public void selectSendEmailTextBox() throws Exception {
		btnHelper.click(emailRemTextBox);
	}
	
	public void verifySendEmailValMsg() throws Exception {
		new Scroll().scrollTillElem(emailRemTextBoxValMsg);
		Assert.assertEquals(textBoxHelper.getText(emailRemTextBoxValMsg), ObjectRepo.reader.getEmailRequireValMsg());
	}
	
	public void enterEmailRemTextbox(String emailRem) throws Exception {
		textBoxHelper.sendKeys(emailRemTextBox,emailRem);
	}
	
	public void verifySendEmailValMsgForInValEmail() throws Exception {
		new Scroll().scrollTillElem(emailRemTextBoxValMsgInVal);
		Assert.assertEquals(textBoxHelper.getText(emailRemTextBoxValMsgInVal), ObjectRepo.reader.getInvalidEmailValMsg());
	}
	
	public void verifyEmailRemiForValData() throws Exception {
		boolean actual;
		boolean noValue = driver.findElements(By.xpath("//*[@translate='secure.admin.user.myprofile.myprofile.EMAILREQ']")).size()!=0;
		boolean maxValue = driver.findElements(By.xpath("//*[@translate='secure.admin.user.myprofile.myprofile.INVALIDEMAIL']")).size()!=0;
		if(noValue == false && maxValue == false ) {
			actual = false ;
		}
		else {
			actual = true;
		}
		new Scroll().scrollTillElem(emailRemTextBox);
		Assert.assertEquals(actual, false);
	}
	
	public void selectDrpDwnAppValData() throws Exception {
//		Select drpDwn = new Select(appDrpDwn);
//		drpDwn.selectByIndex(1);
		drpdwnHelper.selectDropDownIndex(appDrpDwn, 1);
	}
	
	public void selectAppRole() throws Exception {
		btnHelper.click(selectAppRole);
		btnHelper.click(checkRoll);
		btnHelper.click(addRole);
	}
	
	public void saveBtn() throws Exception {
		btnHelper.click(saveBtn);
		//Thread.sleep(2000);
	}
	
	public void verifyFirFreName(String frequencyNam) throws Exception {
		
		//Thread.sleep(30000);
		Assert.assertEquals(textBoxHelper.getText(findRecentFreName), frequencyNam);
		
	}

}
