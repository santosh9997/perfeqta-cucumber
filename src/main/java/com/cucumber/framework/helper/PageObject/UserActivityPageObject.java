package com.cucumber.framework.helper.PageObject;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.CheckBox.CheckBoxOrRadioButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Pagination.Pagination;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.settings.ObjectRepo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

public class UserActivityPageObject extends PageBase {
	private WebDriver driver;
	private Helper helper = new Helper();
	public ButtonHelper btnHelper;
	public CheckBoxOrRadioButtonHelper checkBoxAndBtnHelper;
	public TextBoxHelper textBoxHelper;
	public DropDownHelper drpdwnHelper;
	private final Logger log = LoggerHelper.getLogger(SitesPageObject.class);
	private Search sObj;

	public UserActivityPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		checkBoxAndBtnHelper = new CheckBoxOrRadioButtonHelper(driver);
		textBoxHelper = new TextBoxHelper(driver);
		drpdwnHelper = new DropDownHelper(driver);
		sObj = new Search();
	}

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.home']")
	public WebElement administrationTile;
    
	@FindBy(how = How.XPATH, using = "//div[15]/div/a")
	public WebElement UserActivityTile;
	
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[3]/span")
	public WebElement verifyUserActivityTile;
	
	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.setTab(1)']")
	public WebElement loggedinUserTile;
	
	
	@FindBy(how = How.XPATH, using = "//form/div/div/a")
	public WebElement loggedinUserBackBtn;
	

	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[2]/span")
	public WebElement AdminTile;
	@FindBy(how = How.XPATH, using = "//input[@ng-change='vm.searchChanged()']")
	public WebElement searchbox;
	
	@FindBy(how = How.XPATH, using = "//tr[1]/td[1]/span/span")
	public WebElement verifysearchboxresult;
	
	@FindBy(how = How.XPATH, using = "//thead/tr/th[1]")
	public WebElement SortingIcon;
	 
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[4]")
	public WebElement LogoutBtn;
	@FindBy(how = How.XPATH, using = "//table/tbody/tr[1]/td[1]")
	public WebElement verifyLogoutBtn;
	
	

	/* -------------------------- |----------------| -------------------------- */
	/* -------------------------- | Public Methods | -------------------------- */
	/* -------------------------- |----------------| -------------------------- */

	public void clicktoAdministrationTile() throws Exception {
		log.info(administrationTile);
		btnHelper.click(administrationTile);
	}
	public void clickonuserActivityTile() {
		btnHelper.click(UserActivityTile);
	}
	public void verifyUserActivityTile() {
		Assert.assertEquals(textBoxHelper.getText(verifyUserActivityTile), ObjectRepo.reader.getVerifyUserActivityTile());
	}
	public void clickonLoggedin() {
		btnHelper.click(loggedinUserTile);
	}
	public void clickonLoggedCancelBtn() {
		btnHelper.click(loggedinUserBackBtn);
	}
    public void redirectAdministration() {
    	Assert.assertEquals(textBoxHelper.getText(AdminTile), ObjectRepo.reader.getAdministrationTile());
	}
	 public void enterUsernameinSearchBox(String value) throws Exception{
		btnHelper.click(searchbox);
		textBoxHelper.sendKeys(searchbox,value);
	}
	 public void verifySearchresult() throws Exception {
		 String search = textBoxHelper.getText(verifysearchboxresult);
		 String[] searchresult = search.split(" ");
		 String actualResult = searchresult[0];
		 Assert.assertEquals(actualResult, ObjectRepo.reader.getVerifysearchboxresult());
	}
	 public void clickonSortingIcon() {
		 btnHelper.click(SortingIcon);
	}
	 
	 public void verifyredirctLoggedInUserTile() {
		Assert.assertEquals(textBoxHelper.getText(verifyUserActivityTile), ObjectRepo.reader.getVerifyUserActTileForLoggedUser());
	}
	 public void clickonLogout() {
		 btnHelper.click(LogoutBtn);
	}
    public void verifyLoggedOutUser() {
		if(verifyLogoutBtn.isDisplayed()) {
			Assert.assertEquals(true, true);
		}
		else {
			Assert.assertEquals(false, true);
		}
	}
    public void clickonSortingIconDscending() {
    	 btnHelper.click(SortingIcon);
    	 btnHelper.click(SortingIcon);
	}
	 
}