package com.cucumber.framework.helper.PageObject;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Pagination.Pagination;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;

public class SetsPageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(SetsPageObject.class);
	private Search sObj;
	private DropDownHelper drpHelper;
	private TextBoxHelper textboxHelper;
	private WaitHelper waitObj;
	public SetsPageObject(WebDriver driver) 
	{
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		sObj = new Search();
		waitObj = new WaitHelper(ObjectRepo.driver, ObjectRepo.reader);
		drpHelper = new DropDownHelper(ObjectRepo.driver);
		textboxHelper =new TextBoxHelper(ObjectRepo.driver);
	}

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.home']")
	public WebElement administrationTile;

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.sets.list']")
	public WebElement setTile;

	@FindBy(how = How.XPATH, using = "//div/span[@class='currentStep ng-binding']")
	public WebElement setsModuleName;

	@FindBy(how = How.XPATH, using = "/html/body/div/div/ui-view/ui-view/div/div[2]/grid/div[3]/div[2]/ul/li[3]/a")
	public WebElement verifyFirstPage;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement verifyNextData;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement verifyPreviousData;

	@FindBy(how = How.XPATH, using = "//select[@id='ddlPageSize']")
	public WebElement pageSizeDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement showingText;

	@FindBy(how = How.XPATH, using = "//span/a[@ng-click='openModal(row._id,row.title,row.route);']")
	public WebElement linkInformation;

	@FindBy(how = How.XPATH, using = "//h4[@class='col-sm-11 ng-binding']")
	public WebElement linkInformationPopUp;

	@FindBy(how = How.XPATH, using = "//i[@class='glyphicon glyphicon-remove pull-right critearia-01 ng-click-active']")
	public WebElement linkInformationPopUpClose;

	@FindBy(how = How.XPATH, using = "//tr[1]//*[contains(text(),'View Audit Trail')]")
	public WebElement clickAuditTrail;

	@FindBy(how = How.XPATH, using = "//span[@class='currentStep ng-binding']")
	public WebElement verifyAuditTrail;

	@FindBy(how = How.XPATH, using = "//input[@id='txtSearch']")
	public WebElement searchTextBox;

	@FindBy(how = How.XPATH, using = "//a[@ng-show='vm.isValidUser']")
	public WebElement clickAddBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='form-group']")
	public WebElement currentVersion;

	@FindBy(how = How.XPATH, using = "//input[@name='setNameTextBox']")
	public WebElement setTextBox;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.SETNAMEISREQUIRED']")
	public WebElement requiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.SETNAMESHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement minimumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.SETVALUESHOULDNOTBEMORETHAN50CHARACTERSLONG']")
	public WebElement maximumMsg;

	@FindBy(how = How.XPATH, using = "//p[@ng-bind-html='vm.allowcharacter']")
	public WebElement specialCharMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.SETNAMEMUSTBEUNIQUE']")
	public WebElement duplicateMsg;

	@FindBy(how = How.XPATH, using = "//input[@name='addSetValueTextBox']")
	public WebElement enterSetValue;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.addsetvalue(vm.setvalue.value)']")
	public WebElement addBtnSetValue;

	@FindBy(how = How.XPATH, using = "/html/body/div/div/ui-view/ui-view/fieldset/div/form/div[3]/div[1]/div[3]/div[2]/div/li[2]/div[2]")
	public WebElement setValueTable;

	@FindBy(how = How.XPATH, using = "//input[@aria-checked='true']")
	public WebElement toggleBtn;

	@FindBy(how = How.XPATH, using = "//label[@for='isActiveCheckbox']")
	public WebElement toggleBtnInactive;

	@FindBy(how = How.XPATH, using = "//a[@class='btn btn-danger11 ng-binding ng-scope']")
	public WebElement cancelBtnSet;

	@FindBy(how = How.XPATH, using = "//input[@aria-checked='false']")
	public WebElement Verify_checkbox_is_clickable_of_Add_Set_Screen;

	@FindBy(how = How.XPATH, using = "//div[3]/div[2]/div/li[2]/div[1]/input")
	public WebElement Verify_checkbox_checked_unchecked;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.editsetvalue(set,$index)']")
	public WebElement click_on_edit_button;

	@FindBy(how = How.XPATH, using = "//input[@name='addSetValueTextBox']")
	public WebElement edit_value;

	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.setvalue.value']")
	public WebElement new_edited_value;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.deletesetvalue(set,$index)']")
	public WebElement delete_s_value;

	@FindBy(how = How.XPATH, using = "//li[@ng-model='vm.set.setvalues']")
	public WebElement delete_verify;

	@FindBy(how = How.XPATH, using = "//span[@ng-click='vm.orderalphabet()']")
	public WebElement ascendingIcon;

	@FindBy(how = How.XPATH, using = "//span[@ng-click='vm.orderalphabetalt()']")
	public WebElement descendingIcon;

	@FindBy(how = How.XPATH, using = "//span[@ng-click='vm.moveDown(vm.setindex)']")
	public WebElement moveDown;
	@FindBy(how = How.XPATH, using = "//span[@ng-click='vm.moveUp(vm.setindex)']")
	public WebElement moveUp;

	@FindBy(how = How.XPATH, using = "//form/div[3]/div[1]/div[3]/div[2]/div/li[4]/div[2]")
	public WebElement verifyDown;

	@FindBy(how = How.XPATH, using = "//*[@value='importFromExistingSet']")
	public WebElement radioExistingSet;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.existingset.existingset']")
	public WebElement selectExistingSet;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.selectAllValue']")
	public WebElement selectAllSet;

	@FindBy(how = How.XPATH, using = "/html/body/div/div/ui-view/ui-view/fieldset/div/form/div[3]/div[3]/div/span")
	public WebElement moveArrow;

	@FindBy(how = How.XPATH, using = "/html/body/div[1]/div/ui-view/ui-view/fieldset/div/form/div[3]/div[1]/div[3]/div[2]/div/li[7]/div[2]")
	public WebElement beforeclickDownbtn;

	/* -------------------------- |----------------| -------------------------- */
	/* -------------------------- | Public Methods | -------------------------- */
	/* -------------------------- |----------------| -------------------------- */

	public void clicktoAdministrationTile() throws Exception {
		log.info(administrationTile);
		//administrationTile.click();
		btnHelper.click(administrationTile);
	}

	public void clicktoSetTile() throws Exception {
		log.info(setTile);
		//setTile.click();
		btnHelper.click(setTile);
	}

	public void verifySetsModuleName() throws Exception {
		log.info(setsModuleName);
		Assert.assertEquals(textboxHelper.getText(setsModuleName),ObjectRepo.reader.getSetsModuleName());

	}

	public void verifyFirstPagePagination() throws Exception {
		log.info(verifyFirstPage);
		Assert.assertEquals(textboxHelper.getText(verifyFirstPage), ObjectRepo.reader.getVerifyFirstPage());

	}

	public void verifyNextBtnPagination() throws Exception {
		log.info(verifyNextData);
		///Thread.sleep(1000);
		String output = textboxHelper.getText(verifyNextData);//verifyNextData.getText();
		boolean actual = output.startsWith("11", 8);
		Assert.assertEquals(actual, true);
	}

	public void clickLinkInformationLink() throws Exception {
		log.info(linkInformation);
		//Thread.sleep(1000);
		//linkInformation.click();
		btnHelper.click(linkInformation);
	}

	public void verifyLinkInformationPopUp() throws Exception {
		log.info(linkInformationPopUp);
		//Thread.sleep(1000);

		Assert.assertEquals(textboxHelper.getText(linkInformationPopUp),ObjectRepo.reader.getLinkInformationPopUp());
		//linkInformationPopUpClose.click();
		btnHelper.click(linkInformationPopUpClose);
	}

	public void searchTextBoxSendkeys(String searchValue) throws Exception {
		log.info(clickAuditTrail);
		//Thread.sleep(1500);
		textboxHelper.sendKeys(searchTextBox, searchValue);
		//searchTextBox.sendKeys(searchValue);
		//return searchValue;
	}

	public void verifySearchTextBoxInput(String searchValue) throws Exception {
		log.info(clickAuditTrail);
		//Thread.sleep(1500);
		String paginationText = textboxHelper.getText(showingText);//showingText.getText();
		//Select select = new Select(pageSizeDrpDwn);
		//WebElement option = select.getFirstSelectedOption();
		String pageSizeDrpDwnVal = drpHelper.getFirstSelectedOption(pageSizeDrpDwn);//option.getText();

		waitObj.waitForElementVisible(driver.findElement(By.xpath("//*[@ng-if='vm.options.data && vm.options.data.length']")));
		WebElement Grid = driver.findElement(By.xpath("//*[@ng-if='vm.options.data && vm.options.data.length']"));
		sObj.SearchVerification(paginationText, pageSizeDrpDwnVal, Grid, searchValue, searchTextBox, paginationText);
	}

	public void clickViewAuditTrailLink() throws Exception {
		log.info(clickAuditTrail);
		//Thread.sleep(1500);
		btnHelper.click(clickAuditTrail);
		//clickAuditTrail.click();
	}

	public void verifyAuditTrailPage() throws Exception {
		log.info(verifyAuditTrail);
		Assert.assertEquals(textboxHelper.getText(verifyAuditTrail), ObjectRepo.reader.getViewAuditTrailText());
	}

	public void clickAddBtnSets() throws Exception {
		log.info(clickAddBtn);
		//Thread.sleep(1500);
		btnHelper.click(clickAddBtn);
		//clickAddBtn.click();
	}

	public void verifyCurrentVersion() throws Exception {
		log.info(currentVersion);
		Thread.sleep(1500);
		ArrayList<Object> list = new ArrayList<Object>();
		list.add(currentVersion.getText());
		Assert.assertEquals(list.toString(), ObjectRepo.reader.getCurrentVersion());
	}

	public void enterSetTextbox(String reqVal) throws Exception {
		log.info(setTextBox);
		//Thread.sleep(1500);
		textboxHelper.sendKeys(setTextBox, reqVal);
		//	setTextBox.sendKeys(reqVal);
		textboxHelper.clear(setTextBox);
		//setTextBox.clear();
		//textboxHelper.sendKeys(setTextBox, Keys.TAB);
		setTextBox.sendKeys(Keys.TAB);
	}

	public void verifySetRequiredValidationMsg() throws Exception {
		log.info(requiredMsg);
		//String actual = requiredMsg.getText();
		String expected = ObjectRepo.reader.getRequiredMsg();
		Assert.assertEquals(textboxHelper.getText(requiredMsg), expected);
	}

	public void enterOneChar(String reqVal) throws Exception {
		log.info(setTextBox);
		//Thread.sleep(1500);
		textboxHelper.sendKeys(setTextBox, reqVal);
		//setTextBox.sendKeys(reqVal);
	}

	public void verifySetMinimumValidationMsg() throws Exception {
		log.info(minimumMsg);
		//String actual = minimumMsg.getText();
		String expected = ObjectRepo.reader.getMinimumMsg();
		Assert.assertEquals(textboxHelper.getText(minimumMsg), expected);
	}

	public void enterFiftyOneChar(String reqVal) throws Exception {
		log.info(setTextBox);
		//Thread.sleep(1500);
		//setTextBox.sendKeys(reqVal);
		textboxHelper.sendKeys(setTextBox, reqVal);
	}

	public void verifySetMaximumValidationMsg() throws Exception {
		log.info(minimumMsg);
		//	String actual = maximumMsg.getText();
		String expected = ObjectRepo.reader.getMaximumMsg();
		Assert.assertEquals(textboxHelper.getText(maximumMsg), expected);
	}

	public void enterSpecialChar(String reqVal) throws Exception {
		log.info(setTextBox);
		//Thread.sleep(1500);
		//setTextBox.sendKeys(reqVal);
		textboxHelper.sendKeys(setTextBox, reqVal);
		setTextBox.sendKeys(Keys.TAB);
	}

	public void verifySetSpecialCharValidationMsg() throws Exception {
		log.info(specialCharMsg);
		//String actual = specialCharMsg.getText();
		String expected = ObjectRepo.reader.getSpecialCharMsg();
		Assert.assertEquals(textboxHelper.getText(specialCharMsg), expected);
	}

	public void enterValidSets(String reqVal) throws Exception {
		log.info(setTextBox);
		//Thread.sleep(1500);
		//setTextBox.sendKeys(reqVal);
		textboxHelper.sendKeys(setTextBox, reqVal);
		setTextBox.sendKeys(Keys.TAB);
	}

	public void verifyValidSetsNoMsg() throws Exception {
		boolean noValMSg;
		boolean maxVal= driver.findElements(By.xpath("//p[@translate='secure.admin.groupsettings.edit.SETVALUESHOULDNOTBEMORETHAN50CHARACTERSLONG']")).size()!=0;
		boolean uniqueVal = driver.findElements(By.xpath("//p[@translate='secure.admin.groupsettings.edit.SETNAMEMUSTBEUNIQUE']")).size()!=0;
		boolean minVal = driver.findElements(By.xpath("//p[@translate='secure.admin.groupsettings.edit.SETNAMESHOULDBEATLEAST2CHARACTERSLONG']")).size()!=0;
		boolean requiredVal = driver.findElements(By.xpath("//p[@translate='secure.admin.groupsettings.edit.SETNAMEISREQUIRED']")).size()!=0;

		if(maxVal==false && uniqueVal==false && minVal==false && requiredVal==false) {
			noValMSg=false;
		}else {
			noValMSg=true;
		}
		Assert.assertEquals(noValMSg, false);
	}

	public void enterDuplicateSetsName(String duplicateVal) throws Exception {
		log.info(specialCharMsg);
		//setTextBox.sendKeys(duplicateVal);
		textboxHelper.sendKeys(setTextBox, duplicateVal);
		setTextBox.sendKeys(Keys.TAB);
	}

	public void verifyDuplicateValidationMsg() throws Exception {
		log.info(duplicateMsg);
		//	String actual = duplicateMsg.getText();
		String expected = ObjectRepo.reader.getDuplicateMsg();
		Assert.assertEquals(textboxHelper.getText(duplicateMsg), expected);
	}

	public void enterSetValueTextBox(String enterSet) throws Exception {
		log.info(enterSetValue);
		//enterSetValue.sendKeys(enterSet);
		textboxHelper.sendKeys(enterSetValue, enterSet);
	}

	public void clickAddBtnSetValue() throws Exception {
		log.info(addBtnSetValue);
		//addBtnSetValue.click();
		btnHelper.click(addBtnSetValue);
	}

	public void verifySetValueTextBoxValue(String enterSet) throws Exception {
		log.info(setValueTable);
		//Thread.sleep(1500);
		//String actual = setValueTable.getText();
		Assert.assertEquals(textboxHelper.getText(setValueTable), enterSet);
	}

	public void verifyToggleBtnDefaultVal() throws Exception {
		log.info(toggleBtn);
		//Thread.sleep(1500);
		boolean actual = toggleBtn.isSelected();
		Assert.assertEquals(actual, true);
	}

	public void clickToggleBtnInactive() throws Exception {
		log.info(toggleBtn);
		//toggleBtnInactive.click();
		btnHelper.click(toggleBtnInactive);
	}

	public void verifyToggleBtnInactiveVal() throws Exception {
		log.info(toggleBtn);
		waitObj.waitForElementVisible(driver.findElement(By.xpath("//input[@aria-checked='false']")));
		WebElement active = driver.findElement(By.xpath("//*[@id='isActiveCheckbox']"));
		String activeInactive = active.getAttribute("aria-checked");
		Assert.assertEquals(activeInactive, "false");
	}

	public void clickCancelBtnAddSet() throws Exception {
		log.info(toggleBtn);
		//cancelBtnSet.click();
		btnHelper.click(cancelBtnSet);
	}

	public void verifyCancelBtnFunctionality() throws Exception {
		log.info(toggleBtn);
		//Thread.sleep(1500);

		Assert.assertEquals(textboxHelper.getText(setsModuleName), ObjectRepo.reader.getSetsModuleName());
	}

	public void checkedDefaultValue() throws Exception {

		//Verify_checkbox_checked_unchecked.click();
		btnHelper.click(Verify_checkbox_checked_unchecked);
	}

	public void verifyCheckboxClickable() throws Exception {
		if (Verify_checkbox_checked_unchecked.isSelected()) 
		{
			Assert.assertEquals(true, true);
		}
		else
		{
			Assert.assertEquals(false, true);
		}
	}

	public void clickonEdit() throws Exception {
		//click_on_edit_button.click();
		btnHelper.click(click_on_edit_button);
	}

	public void editSetValue(String value) throws Exception {
		//edit_value.click();
		btnHelper.click(edit_value);
	}

	public void verifyEditFunctionality(String val) {
		//String v = new_edited_value.getText();
		//edit_value.clear();
		textboxHelper.clear(edit_value);
		if (textboxHelper.getText(new_edited_value) != val) 
		{
			Assert.assertEquals(true, true);
			textboxHelper.sendKeys(edit_value, val);
			//edit_value.sendKeys(val);
		}
		else
		{
			Assert.assertEquals(false, true);
		}
	}

	public void deleteSetValue() {
		//delete_s_value.click();
		btnHelper.click(delete_s_value);
	}

	public void deleteSetVerify() {
		boolean actual;
		if (driver.findElements(By.xpath("//li[@ng-model='vm.set.setvalues']")).size() != 0) 
		{
			actual = false;
		}
		else
		{
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}

	public void addMultipleSet(String setvalue) {
		String[] arraySetValue = setvalue.split(",");
		for (int i = 0; i < arraySetValue.length; i++) 
		{
			textboxHelper.sendKeys(enterSetValue, arraySetValue[i]);
			//enterSetValue.sendKeys(arraySetValue[i]);
			btnHelper.click(addBtnSetValue);
			//addBtnSetValue.click();
		}
	}

	public void clickAscendingIcon() {
		//ascendingIcon.click();
		btnHelper.click(ascendingIcon);
	}

	public void verifyAscendingOrder(String setValues) {
		System.out.println(
				"We can't perform this test case due to Java sorting function giving different output as compared to perfeqta's output ");
	}

	public void clickDescendingIcon() {
		//descendingIcon.click();
		btnHelper.click(descendingIcon);
	}

	public void verifyDescendingOrder() {
		System.out.println(
				"We can't perform this test case due to Java sorting function giving different output as compared to perfeqta's output ");
	}

	public void clickMoveDown() {

		// functionality implemented in verifyMoveDown();
	}

	public void verifyMoveDown(String setvalue) {

		String[] arraySetValue = setvalue.split(",");
		String beforeClick = arraySetValue[2];
		//moveDown.click();
		btnHelper.click(moveDown);
		String afterClick = textboxHelper.getText(verifyDown);//verifyDown.getText();
		if (beforeClick.equals(afterClick))
		{
			Assert.assertEquals(false, true);
		} 
		else
		{
			Assert.assertEquals(true, true);
		}
	}

	public void clickMoveUp() {
		// functionality implemented in verifyMoveUp();
	}

	public void verifyMoveUp(String setvalue) {
		String[] arraySetValue = setvalue.split(",");
		String beforeClick = arraySetValue[2];
		//moveUp.click();
		btnHelper.click(moveUp);
		String afterClick =textboxHelper.getText(verifyDown); //verifyDown.getText();
		if (beforeClick.equals(afterClick))
		{
			Assert.assertEquals(false, true);

		}
		else
		{
			Assert.assertEquals(true, true);
		}
	}

	public void radiobtnAddfromExistingSet() {
		//radioExistingSet.click();
		btnHelper.click(radioExistingSet);
		//selectExistingSet.click();
		btnHelper.click(selectExistingSet);
	}

	public void selectValExistingSetfromDropdown() 
	{
		drpHelper.SelectUsingIndex(driver.findElement(By.xpath("//select[@ng-model='vm.existingset.existingset']")), 16);
		//Select dropdown = new Select(driver.findElement(By.xpath("//select[@ng-model='vm.existingset.existingset']")));
		//dropdown.selectByIndex(16);
		btnHelper.click(selectAllSet);
		//selectAllSet.click();
	}

	public void moveArrow()
	{
		//moveArrow.click();
		btnHelper.click(moveArrow);
	}

	public void verifyRecordofExistingSetvalueMoveDown() {
		int  count = driver.findElements(By.xpath("//*[@ng-model='set.isDefaultValue']")).size();
		WebElement firstValue = driver.findElement(By.xpath("(//*[@ng-repeat='set in vm.set.values'])[1]"));
		btnHelper.click(firstValue);
		for(int i = 2; i<=count ;i++) {
			String beforeClickDown = driver.findElement(By.xpath("//div[3]/div[1]/div[3]/div[2]/div/li["+i+"]/div[2]")).getText();
			btnHelper.click(moveDown);
			String afterClickDown = driver.findElement(By.xpath("//div[3]/div[1]/div[3]/div[2]/div/li["+(i+1)+"]/div[2]")).getText();//beforeclickDownbtn.getText();
			if (beforeClickDown.equals(afterClickDown)) 
			{
				Assert.assertEquals(true, true);
			} 
			else
			{
				Assert.assertEquals(false, true);
			}

		}
	}

	public void verifyRecordofExistingSetvalueMoveUp() {
		int  count = driver.findElements(By.xpath("//*[@ng-model='set.isDefaultValue']")).size();
//		WebElement lastValue = driver.findElement(By.xpath("(//*[@ng-repeat='set in vm.set.values'])["+ count + "]"));
//		btnHelper.click(lastValue);
		for(int i=count+1;i>2 ;i--) {
			String beforeClickDown = driver.findElement(By.xpath("//div[3]/div[1]/div[3]/div[2]/div/li["+i+"]/div[2]")).getText();
			btnHelper.click(moveUp);
			String afterClickDown = driver.findElement(By.xpath("//div[3]/div[1]/div[3]/div[2]/div/li["+(i-1)+"]/div[2]")).getText();
			if (beforeClickDown.equals(afterClickDown))
			{
				Assert.assertEquals(true, true);
			} 
			else 
			{
				Assert.assertEquals(false, true);
			}
		}
	}
}