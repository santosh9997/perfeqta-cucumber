package com.cucumber.framework.helper.PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.helper.Sorting.SortingOnColumn;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;

public class AttributesPageObject extends PageBase
{
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(AttributesPageObject.class);
	private SortingOnColumn sortColumn;
	private Search search;
	private Scroll scroll;
	private TextBoxHelper textboxHelper;
	private DropDownHelper drpHelper;
	private WaitHelper waitObj;

	public AttributesPageObject(WebDriver driver)
	{
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		sortColumn = new SortingOnColumn();
		search = new Search();
		scroll = new Scroll();
		textboxHelper =new TextBoxHelper(ObjectRepo.driver);
		drpHelper = new DropDownHelper(ObjectRepo.driver);
		waitObj = new WaitHelper(ObjectRepo.driver, ObjectRepo.reader);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.attributes.list']")
	public WebElement attributesTile;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement attributesModuleName;

	@FindBy(how = How.XPATH, using = "//*[@ng-change='vm.changed()']")
	public WebElement moduleDrpClickable;

	@FindBy(how = How.ID, using = "txtSearch")
	public WebElement textSearch;

	@FindBy(how = How.XPATH, using = "//*[@class='paddingLeft-15px ng-binding']")
	public WebElement srcResultPageNo;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement paginationText;

	@FindBy(how = How.ID, using = "//*[@ng-change='vm.onPageNumberChange()']")
	public WebElement pageSizeDrp;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.options.data && vm.options.data.length']")
	public WebElement grid_data;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement attributeAuditTrailPageName;

	@FindBy(how = How.XPATH, using = "//*[@class='crumb breadcrumb gray ng-isolate-scope']")
	public WebElement attributeAuditTrailBreadCrumbs;

	@FindBy(how = How.LINK_TEXT, using = "View")
	public WebElement attributeViewLinkInformation;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Your current Attribute is used in these Apps:')]")
	public WebElement attributeViewPopuplabel;

	@FindBy(how = How.XPATH, using = "//*[@class='pull-right qc-form-tab-attributs']")
	public WebElement editAttributeCurrentVersion;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='!vm.readOnlyPermission']")
	public WebElement editAttributeCanceButton;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.save(vm.attribute,editAttributeForm)']")
	public WebElement editAttributeSaveButton;

	@FindBy(how = How.XPATH, using = "//li/a[@ng-click='selectPage(page - 1, $event)']")
	public WebElement clickPreviousPage;

	@FindBy(how = How.XPATH, using = "//li[@class='pagination-last ng-scope disabled']")
	public WebElement verifyLastDisabled;

	@FindBy(how = How.XPATH, using = "//select[@id='ddlPageSize']")
	public WebElement pageSizeDrpDwn;

	@FindBy(how = How.XPATH, using = "//i[@class='glyphicon glyphicon-remove pull-right critearia-01 ng-click-active']")
	public WebElement linkInformationPopUpClose;

	public void clicktoAttributesTile() throws Exception {
		attributesTile.click();
	}

	public void verifyAttributesModuleName() throws Exception {
		log.info("Attributes Module Name");
		//String attributeModuleName = attributesModuleName.getText().toString();
		Assert.assertEquals(textboxHelper.getText(attributesModuleName), ObjectRepo.reader.getAttributesModuleName());
	}

	public void verifyFirstModulValue(String moduleName) throws Exception {
		if (driver.findElements(By.xpath("//*[@ng-change='vm.changed()' ]")).size() != 0)
		{
			//Select select = new Select(moduleDrpClickable);
			//WebElement option = select.getFirstSelectedOption();
			String defaultItem = drpHelper.getFirstSelectedOption(moduleDrpClickable);
			Assert.assertEquals(defaultItem, moduleName);
		}
	}

	public void verifyModuledropdownenable() {
		boolean a = moduleDrpClickable.isDisplayed();
		boolean b = moduleDrpClickable.isEnabled();

		/* Need to ask */

		Assert.assertEquals(a, b);
	}

	public void selectModuleName(String moduleName) {
		//moduleDrpClickable.click();
		btnHelper.click(moduleDrpClickable);
	}

	public String sendSearchAttributeval(String searchAttribute) {
		textboxHelper.sendKeys(textSearch, searchAttribute);
		//textSearch.sendKeys(searchAttribute);
		return searchAttribute;
	}

	public void verifysearch(String searchedItem, String modulename) throws Exception {
		waitObj.waitForElementVisible(driver.findElement(By.xpath("//*[@ng-change='vm.changed()' ]")));
		if (driver.findElements(By.xpath("//*[@ng-change='vm.changed()' ]")).size() != 0)
		{
			String PaginationVal=null, PaginationSelectedVal=null,srchResultNo=null;
			try 
			{
				PaginationVal = paginationText.getText();
				srchResultNo = srcResultPageNo.getText();
				PaginationSelectedVal = pageSizeDrp_GetSelectedValue();
			} 
			catch (Exception e)
			{
				System.err.println(e);
			}
				search.SearchVerification(PaginationVal, PaginationSelectedVal, grid_data, searchedItem, textSearch,srchResultNo);
			
			
		}
	}

	public void verifyAscendingOrder(String sortingColName) {
		try 
		{
			String PaginationVal = paginationText.getText();
			String PaginationSelectedVal = pageSizeDrp_GetSelectedValue();
			sortColumn.verifySorting(sortingColName, 1, PaginationVal, PaginationSelectedVal, grid_data);
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
	}

	public String pageSizeDrp_GetSelectedValue() {
		String defaultItem;
		try 
		{
			
			if (driver.findElements(By.xpath("//*[@ng-change='vm.onPageNumberChange()']")).size() != 0) 
			{
				defaultItem = drpHelper.getFirstSelectedOption(pageSizeDrp);
				//Select select = new Select(pageSizeDrp);
				//WebElement option = select.getFirstSelectedOption();
				//defaultItem = option.getText();
				return defaultItem;
			}
			else 
			{
				defaultItem = "1";
				return defaultItem;
			}
		}
		catch(Exception e)
		{
			defaultItem = "1";
		}
		return defaultItem;
	}

	//not in use
	public void verifyAscendingOrderResult() {
		try 
		{
			Assert.assertEquals(sortColumn.sorting, true);
		} 
		catch (Exception e)
		{
			e.toString();
		}
	}
	public void verifyAuditTrailNavigation() {
		try
		{
			//waitObj.waitForElementVisible(attributeAuditTrailPageName);
			//String PageName = attributeAuditTrailPageName.getText();
			//Thread.sleep(2000);
			Assert.assertEquals(textboxHelper.getText(attributeAuditTrailPageName), ObjectRepo.reader.getViewAuditTrailText());
		}
		catch(Exception e)
		{
			e.toString();
		}
	}

	public void verifyAttributeAuditTrailBreadCrumbs() {
		try
		{
			//String val = attributeAuditTrailBreadCrumbs.getText();
			//Thread.sleep(1000);
			Assert.assertEquals(textboxHelper.getText(attributeAuditTrailBreadCrumbs), ObjectRepo.reader.getAttributeAuditTrailBreadCrumbs());
		}
		catch(Exception e)
		{
			e.toString();
		}
	}

	public void clicktoAttributesViewLink() {
		//attributeViewLinkInformation.click();
		btnHelper.click(attributeViewLinkInformation);
	}

	public void verifyViewPopupLabel() {

		try
		{
			//String ViewPopupLabel = attributeViewPopuplabel.getText();
			Assert.assertEquals(textboxHelper.getText(attributeViewPopuplabel), ObjectRepo.reader.getAttributeViewPopuplabel());
			//Thread.sleep(1000);
			clicktoViewPopupCloseicon();	
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}

	public void clickToFirstAttribute(String moduleName, String firstAttributeName) {
		waitObj.waitForElementClickable(driver.findElement(By.xpath("//*[@title='" + firstAttributeName + "']")));
		driver.findElement(By.xpath("//*[@title='" + firstAttributeName + "']")).click();
	}

	public void verifyEditAttribute() {
		try
		{
			SoftAssert editAssert = new SoftAssert();
			//String txtValue = attributeAuditTrailPageName.getText();
			editAssert.assertEquals(textboxHelper.getText(attributeAuditTrailPageName), ObjectRepo.reader.getAttributeAuditTrailPageName());
			String CurrentVersion = textboxHelper.getText(editAttributeCurrentVersion);//.getText();
			editAssert.assertTrue(CurrentVersion.contains(ObjectRepo.reader.getEditAttributeCurrentVersion()));
			editAssert.assertAll();
		}
		catch(Exception e)
		{
			e.toString();
		}
	}

	public void attributeUpdate(String moduleName, String firstAttributeName) {
		waitObj.waitForElementClickable(driver.findElement(By.xpath("//*[@title='" + firstAttributeName + "']")));
		driver.findElement(By.xpath("//*[@title='" + firstAttributeName + "']")).click();
	}

	public void clickToSaveButton() {
		//editAttributeSaveButton.click();
		btnHelper.click(editAttributeSaveButton);
	}

	public void verifyAttributeUpdate() {
		try
		{
			//Thread.sleep(1000);
			//String attributeModuleName = attributesModuleName.getText();
			Assert.assertEquals(textboxHelper.getText(attributesModuleName), ObjectRepo.reader.getAttributesModuleName());
		}
		catch(Exception e)
		{
			e.toString();
		}
	}

	public void clickToCancelButton() {
	//	editAttributeCanceButton.click();
		btnHelper.click(editAttributeCanceButton);
	}

	public void verifyAttributePageRedirection() {
		//String attributeModuleName = attributesModuleName.getText();
		Assert.assertEquals(textboxHelper.getText(attributesModuleName), ObjectRepo.reader.getAttributesModuleName());
	}

	public void verifyCancelButtonColor() {
		scroll.scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.BtnColor(editAttributeCanceButton), ObjectRepo.reader.getDarkGreyColor());
	}

	public void verifySaveButtonColor() {
		scroll.scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.BtnColor(editAttributeSaveButton), ObjectRepo.reader.getLightGreenColor());
	}
	public void  clicktoViewPopupCloseicon()
	{	
		btnHelper.click(linkInformationPopUpClose);
		//linkInformationPopUpClose.click();
	}
}
