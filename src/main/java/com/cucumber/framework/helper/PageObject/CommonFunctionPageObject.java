package com.cucumber.framework.helper.PageObject;

import java.awt.Button;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.interfaces.IconfigReader;
import com.cucumber.framework.settings.ObjectRepo;

public class CommonFunctionPageObject extends PageBase {
	private WebDriver driver;
	private WaitHelper waitObj;
	private ButtonHelper btnHelper;

	public CommonFunctionPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		waitObj = new WaitHelper(ObjectRepo.driver, ObjectRepo.reader);
		btnHelper = new ButtonHelper(driver);
	}

	private final Logger log = LoggerHelper.getLogger(AttributesPageObject.class);

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.user.username']")
	public WebElement username;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.user.password']")
	public WebElement password;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.login()']")
	public WebElement btn_login;

	@FindBy(how = How.XPATH, using = "//span[@id='wootric-x']")
	public WebElement dismissClick;

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.home']")
	public WebElement administrationTile;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-href='#']")
	public WebElement homeBtn;

	public void clicktologin() throws Exception {
		waitObj.waitForElementVisible(btn_login);
		btnHelper.click(btn_login);
	}

	public void clickDismissIcon() throws Exception {
		waitObj.waitForElementVisible(dismissClick);
		btnHelper.click(dismissClick);
	}

	public void clickAdministrationTile() throws Exception {
		log.info(administrationTile);
		//waitObj.waitForElementVisible(administrationTile);
		Thread.sleep(2000);
		btnHelper.click(administrationTile);
	}
	
	public void clickHomeLink() throws Exception {
        log.info(homeBtn);
       // Thread.sleep(1000);
        homeBtn.click();
    }

	public void tearDownLogout() throws InterruptedException {

		if (ObjectRepo.driver.findElements(By.xpath("//*[@class='header-username-bx ng-binding']")).size() == 1) {

			// this code will perform logout 
			System.out.println("loging out from site");
			ObjectRepo.driver.findElement(By.xpath("//*[@class='header-username-bx ng-binding']")).click();
			Thread.sleep(1000);
			ObjectRepo.driver.findElement(By.xpath("//*[@ui-sref='logout']")).click();
			Thread.sleep(2000);
		} 

		System.out.println("this is logout function");
	}
	
	
	public void temp() {
		
	}
}
