package com.cucumber.framework.helper.PageObject;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.CheckBox.CheckBoxOrRadioButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Generic.GenericHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Pagination.Pagination;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.settings.ObjectRepo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

public class GroupSettingsPageObject extends PageBase {
	private WebDriver driver;
	private Helper helper = new Helper();
	public ButtonHelper btnHelper;
	
	public TextBoxHelper textBoxHelper;
	
	private final Logger log = LoggerHelper.getLogger(SitesPageObject.class);
	private Search sObj;

	public GroupSettingsPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		textBoxHelper = new TextBoxHelper(driver);
		sObj = new Search();
	}

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.home']")
	public WebElement administrationTile;
	@FindBy(how = How.XPATH, using = "//div[18]/div/a")
	public WebElement GrpSetingsTile;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[3]/span")
	public WebElement verifyGrpSetingsTile;
	@FindBy(how = How.XPATH, using = "(//*[@title='View Audit Trail'])[1]")
	public WebElement FirstAudit;
	
	@FindBy(how = How.XPATH, using = "//th[1]")
	public WebElement Groupname;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[4]/span")
	public WebElement AddEditpage;
	@FindBy(how = How.XPATH, using = "//a[@href='#/administration/groupsettings/new/edit']")
	public WebElement newBtn;
	@FindBy(how = How.XPATH, using = "//input[@ng-change='vm.checkgroupsettingvalue(vm.groupsetting.title)']")
	public WebElement Groupnametxt;
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.GROUPNAMEISREQUIRED']")
	public WebElement ValidationGroupnametxt;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.GROUPNAMESHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement minValidationGroupnametxt;
	
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.GROUPNAMESHOULDNOTBEMORETHAN50CHARACTERSLONG']")
	public WebElement MaxValidationGroupnametxt;
	
	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.groupsettings.edit.GROUPNAMEMUSTBEUNIQUE']")
	public WebElement UniqueValidationGroupnametxt;
	 
	@FindBy(how = How.XPATH, using = "//li[7]/a/div/label")
	public WebElement selectmodule;
	@FindBy(how = How.XPATH, using = "//option[1]")
	public WebElement selectapp;
	@FindBy(how = How.XPATH, using = "//div[3]/div/span")
	public WebElement movebutton;
	@FindBy(how = How.XPATH, using = "//div/li/span")
	public WebElement selectedapp;
	
	@FindBy(how = How.XPATH, using = "//span[@ng-click='vm.moveDown(vm.groupsettingindex)']")
	public WebElement moveDown;
	@FindBy(how = How.XPATH, using = "//li[3]/span")
	public WebElement moveDownafter;
	@FindBy(how = How.XPATH, using = "//li[1]/span")
	public WebElement moveUpafter;
	@FindBy(how = How.XPATH, using = "//li[2]/span")
	public WebElement heighlightselectedapp;
	@FindBy(how = How.XPATH, using = "//span[@ng-click='vm.moveUp(vm.groupsettingindex)']")
	public WebElement moveup;
	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save(vm.groupsetting,editGroupsettingsForm)']")
	public WebElement saveBtnColor;
	
	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.admin.groupsettings.list']")
	public WebElement calcelBtn;
	@FindBy(how = How.XPATH, using = "//input[@name = 'appFilter']")
	public WebElement SearchApp;
	@FindBy(how = How.XPATH, using = "//div[2]/div/li/span")
	public WebElement selectedAppforGrp;
	
	
	
	
	
	/* -------------------------- |----------------| -------------------------- */
	/* -------------------------- | Public Methods | -------------------------- */
	/* -------------------------- |----------------| -------------------------- */

	public void clicktoAdministrationTile() throws Exception {
		log.info(administrationTile);
		Thread.sleep(2000);
		 btnHelper.click(administrationTile);
	}
	public void clickonGrpSettingTile() {
		 btnHelper.click(GrpSetingsTile);
	}
	 public void verifyGrpSetting() {
		Assert.assertEquals( textBoxHelper.getText(verifyGrpSetingsTile), ObjectRepo.reader.getVerifyGrpSetingsTile());

	 }
	 public void clickonFirstRecord() {
		  btnHelper.click(FirstAudit);
	}
	 public void clickonGroupName() {
		  btnHelper.click(Groupname);
	}
	 public void clickonNewBtn() {
		 btnHelper.click(newBtn);
	}
	 public void verifynewBtnRedirection() {
		Assert.assertEquals( textBoxHelper.getText(AddEditpage), ObjectRepo.reader.getAddEditpage());
	}
	 public void clickonGrpnamebox() {
		  btnHelper.click(Groupnametxt);
	}
	 public void pressTab() {
		 Groupnametxt.sendKeys(Keys.TAB);
	}
	 public void verifyValidationMsg() {
		Assert.assertEquals( textBoxHelper.getText(ValidationGroupnametxt), ObjectRepo.reader.getValGroupnametxt());
	}
	 public void entervalueGrpnameTxt(String value) {
		 textBoxHelper.sendKeys(Groupnametxt,value);
	}
	 public void verifyMinValidation() {
		Assert.assertEquals( textBoxHelper.getText(minValidationGroupnametxt), ObjectRepo.reader.getMinValGroupnametxt());
	}
	 public void enterMaxValue(String value) {
		 textBoxHelper.sendKeys(Groupnametxt,value);
	}
	 public void verifyenterMaxValue() {
			Assert.assertEquals( textBoxHelper.getText(MaxValidationGroupnametxt), ObjectRepo.reader.getMaxValGroupnametxt());

	}
	 public void enterDuplicateValue(String value) {
		  textBoxHelper.sendKeys(Groupnametxt,value);
    }
	 public void verifyenterDuplicateValue() {
			Assert.assertEquals( textBoxHelper.getText(UniqueValidationGroupnametxt), ObjectRepo.reader.getUniqueValGroupnametxt());

	}
	 public void enterValidValue(String value) {
		  textBoxHelper.sendKeys(Groupnametxt,value);
	}
	 public void verifyenterValidValue() {
			Assert.assertEquals(true,true);

	}
	 public void selectModule() {
		  btnHelper.click(selectmodule);
	}
	 public void selectApp() throws Exception{ 
		 //Thread.sleep(4000);
		  btnHelper.click(SearchApp);
		 btnHelper.click(selectapp);
		String p =  textBoxHelper.getText(selectapp);
		  btnHelper.click(movebutton);
		verifyselectedApp(p);
	}
	 public void ClickonMove() {
		
	}
	 public void verifyselectedApp(String value) {
		Assert.assertEquals(value,  textBoxHelper.getText(selectedAppforGrp));
	}
	 public void verifyAscendingorderIcon() {
		  btnHelper.click(selectapp);  btnHelper.click(movebutton); btnHelper.click(selectapp);  btnHelper.click(movebutton);
		 System.out.println(
					"We can't perform this test case due to Java sorting function giving different output as compared to perfeqta's output ");
		
	}
	 
	 public void verifydsendingIcon() {
		  btnHelper.click(selectapp);  btnHelper.click(movebutton); btnHelper.click(selectapp);  btnHelper.click(movebutton);
		 System.out.println(
					"We can't perform this test case due to Java sorting function giving different output as compared to perfeqta's output ");
		
	}
	 public void selectMultipleApps() {
		 btnHelper.click(selectapp);  btnHelper.click(movebutton); btnHelper.click(selectapp);  btnHelper.click(movebutton); btnHelper.click(selectapp);  btnHelper.click(movebutton);
	}
	 public void clickMoveDown() {
		  btnHelper.click(heighlightselectedapp);
		 String heighlightedapp =  textBoxHelper.getText(heighlightselectedapp);
		  btnHelper.click(moveDown);
		 verifymovedown(heighlightedapp);
	}
	 
	 public void  verifymovedown(String value) {
		Assert.assertEquals(value, textBoxHelper.getText(moveDownafter));
	}
	 public void clickMoveUP() {
		  btnHelper.click(heighlightselectedapp);
		 String heighlightedapp =  textBoxHelper.getText(heighlightselectedapp);
		  btnHelper.click(moveup);
		 verifyMoveUp(heighlightedapp);
	}
	 public void verifyMoveUp(String value) {
		 Assert.assertEquals(value , textBoxHelper.getText(moveUpafter));
	}
	 public void verifysaveBtnGreen() {
		 Assert.assertEquals(btnHelper.BtnColor(saveBtnColor), ObjectRepo.reader.getLightGreenColor());
	}
	 public void verifycancelBtnBlack() {
		 Assert.assertEquals(btnHelper.BtnColor(calcelBtn), ObjectRepo.reader.getDarkGreyColor());
	}
	 public void clickCancelBtn() throws Exception {
		 //Thread.sleep(4000);
		  btnHelper.click(calcelBtn);
	}
	 public void verifyRedirectiontoGroupSetting() {
		Assert.assertEquals( textBoxHelper.getText(verifyGrpSetingsTile), ObjectRepo.reader.getVerifyGrpSetingsTile());
	}
}