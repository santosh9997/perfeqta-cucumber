package com.cucumber.framework.helper.PageObject;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Generic.GenericHelper;
import com.cucumber.framework.helper.Javascript.JavaScriptHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.Sorting.SortingOnColumn;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;

import gherkin.lexer.Th;
public class UsersPageObject extends PageBase
{
	private WebDriver driver;
	private ButtonHelper btnHelper;
	private Scroll scroll;
	private Search search;
	private SortingOnColumn sortColumn;
	static SoftAssert softAssertion = new SoftAssert();
	private WaitHelper waitObj;
	private DropDownHelper drpHelper;
	private Helper helper;
	private GenericHelper genericHelper;
	//private static CommonFunctionPageObject commonFnPageObject;
	private ExcelUtils excel;
	private TextBoxHelper textboxHelper;
	private JavaScriptHelper javaScriptHelper;
	public String newFirstName, userName,afterMiddleName, lastName, departmentName, authentication, emailName, rolesName, passwordMethod;
	public String activeInactiveStatus, failedAttempsStatus, currentTimeAndDate, createdBy;
	public String firstUsername;
	public static String newPassword, userDeatils,afterEmail;
	public double versionNumber;
	public int beforeEntries, afterEntries;
	public static String beforeFirstName, beforeModifiedBy,beforeMiddleName, beforeLastName, beforeEmail, beforeDepartment, beforeStatus, beforeFailedAttempts , beforeHippaUser, oldCurrentTime;

	public UsersPageObject(WebDriver driver) 
	{
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		scroll = new Scroll();
		search =new Search();
		sortColumn= new SortingOnColumn();
		waitObj = new WaitHelper(ObjectRepo.driver, ObjectRepo.reader);
		drpHelper = new DropDownHelper(ObjectRepo.driver);
		textboxHelper =new TextBoxHelper(ObjectRepo.driver);
		javaScriptHelper = new JavaScriptHelper(ObjectRepo.driver);
		helper = new Helper();
		genericHelper = new GenericHelper(ObjectRepo.driver);
	}
	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, ObjectRepo.reader.getBreadcrumbtxtForUser());
		System.err.println();
		return excel.readXLSFile(ObjectRepo.reader.getBreadcrumbtxtForUser(), rowVal, colVal);
	}
	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.users.list']")
	public WebElement usersTile;

	@FindBy(how=How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement usersModuleName;

	@FindBy (how=How.XPATH, using="//*[@ng-click='vm.save(vm.user,editUsersForm)']")
	public WebElement usersSavebtn; 

	@FindBy(how=How.XPATH, using="//*[@class='currentStep ng-binding']")
	public WebElement usersBreadcrumbtxt;

	@FindBy (how=How.XPATH, using="//*[@ng-if='!vm.readOnlyPermission']")
	public WebElement usersCancelBtn;

	@FindBy (how=How.XPATH, using="//*[contains(normalize-space(text()),'Showing')]")
	public WebElement paginationText;

	@FindBy (how=How.XPATH, using="//*[@ng-change='vm.onPageNumberChange()']")
	public WebElement pageSizeDrp;

	@FindBy (how=How.XPATH, using="//*[@ng-if='vm.options.data && vm.options.data.length']")
	public WebElement grid_data;

	@FindBy (how=How.XPATH, using="//*[@id='txtSearch']")
	public WebElement srcData;

	@FindBy (how=How.XPATH, using="//*[@class='paddingLeft-15px ng-binding']")
	public WebElement srcResultPageNo;

	@FindBy (how=How.XPATH, using="//*[@class='btn btn-danger11 sets-logspage-left-mar ng-binding']")
	public WebElement usersAuditTrailBackbtn;

	@FindBy (how=How.XPATH, using="//*[@class='crumb breadcrumb gray ng-isolate-scope']")
	public WebElement usersListingBreadCrumbs;

	@FindBy (how=How.XPATH, using ="//*[@ui-sref='secure.admin.users.edit']")
	public WebElement usersAddNewBtn;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.user.username']")
	public WebElement usersTxtUserName;

	@FindBy (how=How.XPATH, using="//*[@ng-if='(editUsersForm.usernameTextBox.$dirty || editUsersForm.usernameTextBox.$touched) && editUsersForm.usernameTextBox.$error.required']")
	public WebElement userNameMandatoryValMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.usernameTextBox.$dirty && editUsersForm.usernameTextBox.$error.minlength']")
	public WebElement userNameMinimumvalMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.usernameTextBox.$dirty && editUsersForm.usernameTextBox.$error.maxlength']")
	public WebElement userNameMaxvalMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.usernameTextBox.$dirty && editUsersForm.usernameTextBox.$error.unique']")
	public WebElement userNameDuplicatevalMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='(editUsersForm.firstNameTextBox.$dirty || editUsersForm.firstNameTextBox.$touched) && editUsersForm.firstNameTextBox.$error.required']")
	public WebElement firstNameMandatoryValMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.firstNameTextBox.$dirty && editUsersForm.firstNameTextBox.$error.minlength']")
	public WebElement firstNameMiniValMsg;

	@FindBy (how=How.XPATH, using="//*[@name='firstNameTextBox']")
	public WebElement txtFirstName;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.firstNameTextBox.$dirty && editUsersForm.firstNameTextBox.$error.maxlength']")
	public WebElement firstNameMaxValMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.user.lastname']")
	public WebElement txtLastName;

	@FindBy (how=How.XPATH, using="//*[@ng-if='(editUsersForm.lastNameTextBox.$dirty || editUsersForm.lastNameTextBox.$touched) && editUsersForm.lastNameTextBox.$error.required']")
	public WebElement lastNameMandatoryValMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.lastNameTextBox.$dirty && editUsersForm.lastNameTextBox.$error.minlength']")
	public WebElement lastNameMinValMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.lastNameTextBox.$dirty && editUsersForm.lastNameTextBox.$error.maxlength']")
	public WebElement lastNameMaxValMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.user.currentEmail']")
	public WebElement txtEmailAddress;

	@FindBy (how=How.XPATH, using="//*[@translate='secure.admin.user.myprofile.myprofile.EMAILREQ']")
	public WebElement emailAddMandatoryValMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.emailTextBox.$dirty && (editUsersForm.emailTextBox.$error.pattern || editUsersForm.emailTextBox.$error.email)']")
	public WebElement invalidEmailAdd;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editUsersForm.emailTextBox.$dirty && editUsersForm.emailTextBox.$error.unique']")
	public WebElement uniqueEmailAdd;

	@FindBy (how=How.XPATH, using="//input[@name='departmentTextBox']")
	public WebElement userDepartment;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.user.passwordGenerationMethod']")
	public WebElement userGeneratePassword;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.changepassword.newPassword']")
	public WebElement usersNewPassword;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.user.password']")
	public WebElement usersConfirmPassword;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editResetPasswordForm.newPasswordTextBox.$viewValue.length < 8' or @translate='secure.admin.user.myprofile.changepassword.MUSTBE8CHARS']")
	public static WebElement atleastMinCharMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if='editResetPasswordForm.newPasswordTextBox.$viewValue.length > 20' or @translate='secure.admin.user.myprofile.changepassword.NOTLONGERTHAN20CHAR']")
	public static WebElement atleastMaxCharMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if=\\\"!vm.matchPattern('(?=.*[a-z])',editResetPasswordForm.newPasswordTextBox.$viewValue)\\\" or @translate='secure.admin.user.myprofile.changepassword.HAVEATLEASTSMALL']")
	public static WebElement atleastSmallCaseMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if=\\\"!vm.matchPattern('(?=.*[A-Z])',editResetPasswordForm.newPasswordTextBox.$viewValue)\\\" or @translate='secure.admin.user.myprofile.changepassword.HAVEATLEASTCAPITAL']")
	public static WebElement atleastCapitalCaseMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if=\\\"!vm.matchPattern('(?=.*[!@#$*])',editResetPasswordForm.newPasswordTextBox.$viewValue)\\\" or @translate='secure.admin.user.myprofile.changepassword.HAVEATLEASTSPECSYM']")
	public static WebElement atleastSpecialMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-if=\\\"!vm.matchPattern('(?=.*[0-9])',editResetPasswordForm.newPasswordTextBox.$viewValue)\\\" or @translate='secure.admin.user.myprofile.changepassword.HAVEATLEASTNUMBER']")
	public static WebElement atleastDigitMsg;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.signup.newPassword']")
	public static WebElement SignUpNewPassword;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.signup.confirmPassword']")
	public static WebElement SignUpConfirmPassword;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.signup.verificationCode']")
	public static WebElement SignUpEsign;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.signup.securityQuestion']")
	public static WebElement SignUpSecurityQue;

	@FindBy (how=How.XPATH, using="//*[@ng-model='vm.signup.securityAnswer']")
	public static WebElement SignUpSecurityAns;

	@FindBy (how=How.XPATH, using="//*[@ng-click='vm.save(vm.signup, editSignUpForm)']")
	public static WebElement SignSave;

	@FindBy (how=How.XPATH, using="//*[@class='currentStep ng-binding']")
	public static WebElement breadcrumbTxtUsers;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.user.username']")
	public WebElement username;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.user.password']")
	public WebElement password;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.login()']")
	public WebElement btn_login;

	@FindBy(how = How.XPATH, using = "//*[@title='Reset']")
	public WebElement resetLInk;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Reset User Access')]//preceding-sibling::input[@type='radio' and @ng-model='vm.resetAction']")
	public WebElement rdbtnResetUserAccess;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.CLICKONSUBMITBTNTORESETUSER']")
	public WebElement activeUserResetAccessMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.submit(editResetPasswordForm)']")
	public WebElement saveButton;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='!vm.isInactiveUser && vm.isValidUser && vm.isVerifiedUser']")
	public WebElement inactiveUserResetAccessMsg;

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.users.list']")
	public WebElement resetCancelBtn;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Reset User Password')]//preceding-sibling::input[@type='radio' and @ng-model='vm.resetAction']")
	public WebElement resetPasswordRadioBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.resetpassword.passwordGenerationMethod']")
	public WebElement generatePassword;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.resetpassword.newPassword']")
	public WebElement resetPageNewPwd;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='(editResetPasswordForm.newPasswordTextBox.$dirty || editResetPasswordForm.newPasswordTextBox.$touched) && editResetPasswordForm.newPasswordTextBox.$error.required']")
	public WebElement newPasswordManValMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.resetpassword.password']")
	public WebElement resetPageConfirmPwd;

	@FindBy(how = How.XPATH, using = "//*[@ng-if=\"(editResetPasswordForm.confirmPasswordTextBox.$dirty || editResetPasswordForm.confirmPasswordTextBox.$touched) && editResetPasswordForm.confirmPasswordTextBox.$error.required\"]")
	public WebElement resetPageConfirmPwdMandValmsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='(editResetPasswordForm.confirmPasswordTextBox.$dirty && (vm.resetpassword.password != (vm.resetpassword.newPassword || editResetPasswordForm.newPasswordTextBox.$viewValue)))']")
	public WebElement confirmPasswordMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.submit(editResetPasswordForm)']")
	public WebElement passwordActionsubmitBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.resetpassword.passwordGenerationMethod']")
	public WebElement resetPageGeneratePwd;

	@FindBy(how = How.XPATH, using = "//*[@id='shareapp-remove-search-text']")
	public WebElement crossBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-change='vm.onPageNumberChange()']")
	public WebElement pageSizeDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@class='paddingLeft-15px ng-binding']")
	public WebElement recodsCount;

	@FindBy(how = How.XPATH, using = "(//*[@ng-click='selectPage(page.number, $event)'])[4]")
	public WebElement lastPageNumber;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='selectPage(totalPages, $event)']")
	public WebElement lastBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='selectPage(page + 1, $event)']")
	public WebElement nextPagging;

	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.go(field,row,vm.resource);'])[1]")
	public WebElement firstUserName;

	@FindBy(how = How.LINK_TEXT, using = "View Audit Trail")
	public WebElement firstAuditTrail;

	@FindBy(how = How.XPATH, using = "(//*[@data-th='Old Value :'])[1]")
	public WebElement oldValue;

	@FindBy(how = How.XPATH, using = "(//*[@data-th='New Value :'])[1]")
	public WebElement newValue;

	/*
	 * @FindBy(how = How.XPATH, using = "//*[@name='userTypeTextBox']") public
	 * WebElement authenticationMethod;
	 */

	@FindBy(how = How.XPATH, using = "//i[@class='header-username-bx ng-binding']")
	public WebElement usernameTab;

	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.myprofile']")
	public WebElement clickMyProfile;

	@FindBy(how = How.XPATH, using = "//input[@name='firstNameTextBox']")
	public WebElement firstnameSameAsLogin;

	@FindBy(how = How.XPATH, using = "//input[@name='lastNameTextBox']")
	public WebElement lastnameValForLoggedUser;

	@FindBy(how = How.XPATH, using = "//input[@name='departmentTextBox']")
	public WebElement departmentNameOfLoggedUser;

	@FindBy(how = How.XPATH, using = "(//*[@class='ng-binding'])[10]")
	public WebElement auditTrailName;

	@FindBy(how = How.XPATH, using = "(//*[@data-th='Version :'])[1]")
	public WebElement versionNo;

	@FindBy(how = How.XPATH, using = "(//*[@data-th='Modified By :'])[1]")
	public WebElement firstModifiedbyName;

	@FindBy(how = How.XPATH, using = "(//*[@data-th='Date & Time :'])[1]")
	public WebElement firstDatAndtime;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.isValidUserForLicenseAgreement']")
	public WebElement licenseAggBtn;

	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.sort(field)'])[text()='License Agreement Status']")
	public WebElement licenseAggStatus;

	@FindBy(how = How.XPATH, using = "//input[@name='middleNameTextBox']")
	public WebElement middleName;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.user.isActive']")
	public WebElement active;

	@FindBy(how = How.XPATH, using = "//*[@id='allowAfterfailedLoginAttempts']")
	public WebElement failedAttempts;

	@FindBy(how = How.XPATH, using = "(//*[@class='from-group ng-binding ng-scope'])[1]")
	public WebElement userRole;

	@FindBy(how = How.XPATH, using = "(//*[@title='MD Anderson Cancer Center'])")
	public WebElement siteSelectd;

	@FindBy(how = How.XPATH, using = "//*[@id='isHippaCheck']")
	public WebElement hippaUser;

	/*Public Methods */
	public void clickToUsersicon()
	{
		//usersTile.click();
		btnHelper.click(usersTile);
	}

	public void verifyUsersModuleName() throws Exception
	{
		 waitObj.waitForElementVisible(usersAddNewBtn);
		//	String usersModuleNm = usersModuleName.getText().toString();
		//Thread.sleep(2000);
		Assert.assertEquals(ObjectRepo.reader.getBreadcrumbtxtForUser(), textboxHelper.getText(usersModuleName));
	}
	public void clickToFirstRecordUnm() throws Exception
	{	
		btnHelper.click(firstUserName);
	}
	/*public void clickToSavebtn()
	{
		waitObj.waitForElementVisible(usersSavebtn);
		usersSavebtn.click();
	}*/
	public void  verifyUsersEditBreadCrumbs()
	{
		//waitObj.waitForElementVisible(usersBreadcrumbtxt);
		//String vcl = usersBreadcrumbtxt.getText();
		Assert.assertEquals(ObjectRepo.reader.getUsersBreadcrumbtxt(),textboxHelper.getText(usersBreadcrumbtxt) );
	}
	
	public void clickonSaveBtnForSimple() {
		btnHelper.click(usersSavebtn);
	}
	
	public void clickonSaveBtn() throws Exception
	{
		afterMiddleName = helper.getClipboardContents(middleName);
		lastName = helper.getClipboardContents(txtLastName);
		departmentName = helper.getClipboardContents(userDepartment);
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("arguments[0].removeAttribute('disabled','disabled')",authenticationMethod);
	//	authentication = helper.getClipboardContents(authenticationMethod);
		afterEmail = helper.getClipboardContents(txtEmailAddress);
		if(driver.findElements(By.xpath("//*[@for='newPasswordTextBox']")).size()!=0) {
			passwordMethod = "Admin";
		}else {
			passwordMethod = "System";
		}
		String activeInactive = active.getAttribute("aria-checked");
		if(activeInactive.equals("true")) {
			activeInactiveStatus = "Active";
		}else {
			activeInactiveStatus = "Inactive";
		}
		failedAttempsStatus = failedAttempts.getAttribute("aria-checked");
		rolesName = textboxHelper.getText(userRole);
		//selectdSites = textboxHelper.getText(siteSelectd);
		System.err.println(lastName);
		System.err.println(departmentName);
		//System.err.println(authentication);
		System.err.println(passwordMethod);
		System.err.println(activeInactiveStatus);
		System.err.println(failedAttempsStatus);
		System.err.println(rolesName);
		//System.err.println(selectdSites);
		//driver.findElement(By.xpath("(//*[@class='siteTitle wrap-site-title ng-binding'])[1]")).click();
		btnHelper.click(usersSavebtn);
		currentTimeAndDate = genericHelper.getCurrentTimeAndDate();
		//		genericHelper.getCurrentTimeAndDate();
	}
	public void clickonCancelBtn() throws InterruptedException
	{
		scroll.scrollTillElem(usersCancelBtn);
		//waitObj.waitForElementVisible(usersCancelBtn);
		btnHelper.click(usersCancelBtn);
	}
	public void verifySaveButtonColor() throws InterruptedException
	{
		//waitObj.waitForElementVisible(usersSavebtn);
		Assert.assertEquals(btnHelper.BtnColor(usersSavebtn), ObjectRepo.reader.getLightGreenColor());
	}
	public void verifyCanceButtonColor()
	{
		//waitObj.waitForElementVisible(usersCancelBtn);
		Assert.assertEquals(btnHelper.BtnColor(usersCancelBtn), ObjectRepo.reader.getDarkGreyColor());
	}
	public void userSendSearchData()
	{
		System.out.println("test");
	}
	public void verifyusersSearchData(String searchData) throws Exception
	{
		String FirstRowData[] = getFirstRowData(driver);
		//waitObj.waitForElementVisible(paginationText);
		String PaginationVal = textboxHelper.getText(paginationText); //paginationText.getText();
		String PaginationSelectedVal =pageSizeDrp_GetSelectedValue();
		WebElement Grid = grid_data;
		WebElement srcbox = srcData;
		//waitObj.waitForElementVisible(srcResultPageNo);
		String srchResultNo = textboxHelper.getText(srcResultPageNo);//srcResultPageNo.getText();
		search.SearchVerification(PaginationVal, PaginationSelectedVal, Grid, FirstRowData[0], srcbox, srchResultNo);
	}
	public String[] getFirstRowData(WebDriver driver) throws Exception 
	{
		// Example:gopi Admin Gopi Gor Reset 07/16/2018 02:15:49 Keval Prajapati (keval)
		// Active View Audit Trail
		String Username = getFirstRowValue(driver,"1").getText();
		String UserRoles = getFirstRowValue(driver, "2").getText();
		String Full_Name = getFirstRowValue(driver, "3").getText();
		String Action = getFirstRowValue(driver, "4").getText();
		String Last_Updated = getFirstRowValue(driver, "5").getText();
		String Modified_By = getFirstRowValue(driver, "6").getText();
		String Status = getFirstRowValue(driver, "7").getText();
		String Audit_Trail = getFirstRowValue(driver, "8").getText();
		String[] splited = { Username, UserRoles, Full_Name, Action, Last_Updated, Modified_By, Status, Audit_Trail };
		return splited;
	}
	public WebElement getFirstRowValue(WebDriver driver,String ColumnNo)
	{
		waitObj.waitForElementVisible(driver.findElement(By.xpath(".//table/tbody/tr[1]/td[" + ColumnNo + "]")));
		WebElement ele =driver.findElement(By.xpath(".//table/tbody/tr[1]/td[" + ColumnNo + "]"));
		return ele;
	}
	public String pageSizeDrp_GetSelectedValue() 
	{
		String defaultItem;
		try 
		{
			waitObj.waitForElementVisible(driver.findElement(By.xpath("//*[@ng-change='vm.onPageNumberChange()']")));
			if (driver.findElements(By.xpath("//*[@ng-change='vm.onPageNumberChange()']")).size() != 0) 
			{
				defaultItem = drpHelper.getFirstSelectedOption(pageSizeDrp);
			}
			else 
			{
				defaultItem = "1";
				return defaultItem;
			}
		}
		catch(Exception e)
		{
			defaultItem = "1";
		}
		return defaultItem;
	}
	public void verifyAscendingorder() throws Exception
	{
		//waitObj.waitForElementVisible(paginationText);
		String PaginationVal = textboxHelper.getText(paginationText); //paginationText.getText();
		String PaginationSelectedVal = pageSizeDrp_GetSelectedValue();
		WebElement Grid = grid_data;
		sortColumn.verifySorting("Username", 1, PaginationVal, PaginationSelectedVal, Grid);
		Thread.sleep(1000);
	}
	public void verifyBackbuttonColor()
	{
		Assert.assertEquals(btnHelper.BtnColor(usersAuditTrailBackbtn), ObjectRepo.reader.getDarkGreyColor());
	}
	public void clickonBackButton()
	{
		btnHelper.click(usersAuditTrailBackbtn);
	}
	public void verifyUserlistingBreadcrumbs() throws Exception
	{
		//waitObj.waitForElementVisible(usersListingBreadCrumbs);
		scroll.scrollTillElem(usersListingBreadCrumbs);
		//String breadcrumb = usersListingBreadCrumbs.getText();
		Assert.assertEquals(ObjectRepo.reader.getUsersListingBreadCrumbs(),textboxHelper.getText(usersListingBreadCrumbs));
	}
	public void clickonAddNewbtn()
	{
		btnHelper.click(usersAddNewBtn);
	}
	public void verifyUserNameMandatoryValMsg()	
	{
		//waitObj.waitForElementVisible(userNameMandatoryValMsg);
		//String userNameMandatory = userNameMandatoryValMsg.getText();
		Assert.assertEquals(ObjectRepo.reader.getUsernameRequireValMsg(), textboxHelper.getText(userNameMandatoryValMsg));
	}
	
	public void userNameForMinMaxSendkeys(String usersName)
	{
		textboxHelper.clearAndSendKeys(usersTxtUserName, usersName);
	}
	
	public void userNameSendkeys(String usersName)
	{
		userName = usersName + new Helper().randomNumberGeneration(); 
		textboxHelper.clearAndSendKeys(usersTxtUserName, userName);
	}
	public void userNamemMinimumValMsg()
	{
		//waitObj.waitForElementVisible(userNameMinimumvalMsg);
		//String userNameMini= userNameMinimumvalMsg.getText();
		Assert.assertEquals(ObjectRepo.reader.getUsernameMinValMsg(), textboxHelper.getText(userNameMinimumvalMsg));
	}
	public void userNamemMaximumValMsg()
	{
		//waitObj.waitForElementVisible(userNameMaxvalMsg);
		//String userNameMaximumvalMsg= userNameMaxvalMsg.getText();
		Assert.assertEquals(ObjectRepo.reader.getUsernameMaxValMsg(), textboxHelper.getText(userNameMaxvalMsg));
	}
	public void verifyDuplicateUserName()
	{
		try
		{
			//Thread.sleep(1000);
			//waitObj.waitForElementVisible(userNameDuplicatevalMsg);
			//String duplicatemsg=userNameDuplicatevalMsg.getText();
			Assert.assertEquals(ObjectRepo.reader.getUserNameDuplicatevalMsg(), textboxHelper.getText(userNameDuplicatevalMsg));
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	
	public void firstNameSendkeys(String firstName) throws Exception
	{
		//oldFirstName = helper.getClipboardContents(txtFirstName);
		btnHelper.click(txtFirstName);
		textboxHelper.sendKeys(txtFirstName, firstName);
		newFirstName = helper.getClipboardContents(txtFirstName);
	}
	public void clickOnEveryFieldOfUser(String firstName, String middlename , String Lastname, String department,String email) throws Exception
	{
		//oldFirstName = helper.getClipboardContents(txtFirstName);
		btnHelper.click(txtFirstName);
		textboxHelper.sendKeys(txtFirstName, firstName);
		newFirstName = helper.getClipboardContents(txtFirstName);
		textboxHelper.sendKeys(middleName, middlename);
		textboxHelper.sendKeys(txtLastName, Lastname);
		textboxHelper.sendKeys(txtEmailAddress, email + new Helper().randomNumberGeneration() +"@yopmail.com");
		textboxHelper.sendKeys(departmentNameOfLoggedUser, department);
		driver.findElement(By.xpath("//label[@for='allowAfterfailedLoginAttempts']"));
		//btnHelper.click(failedAttempts);
		//btnHelper.click(hippaUser);
	}
	public void verifyFirstNameValMsg()
	{
		//waitObj.waitForElementVisible(firstNameMandatoryValMsg);
		//String firstNameValMsg = firstNameMandatoryValMsg.getText();
		Assert.assertEquals(ObjectRepo.reader.getFirstnameRequiredMsg(), textboxHelper.getText(firstNameMandatoryValMsg));
	}
	public void verifyMinimumValMsg()
	{
		//waitObj.waitForElementVisible(firstNameMiniValMsg);
		//String firstNmMinValMsg=firstNameMiniValMsg.getText();
		Assert.assertEquals(ObjectRepo.reader.getFirstnameMinimumMsg(), textboxHelper.getText(firstNameMiniValMsg));
	}
	public void verifyFirstMaxValMsg()
	{
		//waitObj.waitForElementVisible(firstNameMaxValMsg);
		//String firstNmMaxValMsg =firstNameMaxValMsg.getText();
		Assert.assertEquals(ObjectRepo.reader.getFirstnameMaximumMsg(), textboxHelper.getText(firstNameMaxValMsg));
	}
	public void lastNameSendkeys(String lastname) throws Exception
	{
		textboxHelper.sendKeys(txtLastName, lastname);

		//txtLastName.sendKeys(lastName);
	}
	public void verifyLastNmMandatoryValMsg()
	{
		//waitObj.waitForElementVisible(lastNameMandatoryValMsg);
		//String lastNmMandatoryValMsg= lastNameMandatoryValMsg.getText();
		Assert.assertEquals(ObjectRepo.reader.getLastnameRequiredMsg(), textboxHelper.getText(lastNameMandatoryValMsg));
	}
	public void verifyLastNmMinVaMsg()
	{
		//waitObj.waitForElementVisible(lastNameMinValMsg);
		//String lastNmMinValMsg = lastNameMinValMsg.getText();
		Assert.assertEquals(ObjectRepo.reader.getLastnameMinimumMsg(), textboxHelper.getText(lastNameMinValMsg));
	}
	public void verifyLastNmMaxVakMsg()
	{
		//waitObj.waitForElementVisible(lastNameMaxValMsg);
		//String lastNmMaxValMsg = lastNameMaxValMsg.getText();
		Assert.assertEquals(ObjectRepo.reader.getLastnameMaximumMsg(), textboxHelper.getText(lastNameMaxValMsg));
	}
	public void verifyInvalidEmailAddManValMsg()
	{
		//waitObj.waitForElementVisible(emailAddMandatoryValMsg);
		//String inValidEmailAdd = emailAddMandatoryValMsg.getText();
		Assert.assertEquals(ObjectRepo.reader.getEmailRequireValMsg(), textboxHelper.getText(emailAddMandatoryValMsg));
	}
	public void txtEmailSendkeys(String email)
	{
		textboxHelper.sendKeys(txtEmailAddress, email);
		//txtEmailAddress.sendKeys(email);
	}
	public void verifyInValidEmailAdd()
	{ 
		//waitObj.waitForElementVisible(invalidEmailAdd);
		//String inValidEmailAdd = invalidEmailAdd.getText();
		Assert.assertEquals(ObjectRepo.reader.getInvalidEmailValMsg(), textboxHelper.getText(invalidEmailAdd));
	}
	public void verifyDuplicateValMsg()
	{
		//waitObj.waitForElementVisible(uniqueEmailAdd);
		//String emailDuplicateValMsg = uniqueEmailAdd.getText();
		Assert.assertEquals(ObjectRepo.reader.getUniqueEmailAdd(), textboxHelper.getText(uniqueEmailAdd));
	}
	public void departmentSendKeys(String department) throws Exception
	{
		textboxHelper.sendKeys(userDepartment, department);

		//userDepartment.sendKeys(department);
	}
	public void selectGeneratePassword(String generatedBy) throws Exception
	{
		drpHelper.selectDropDownText(userGeneratePassword, generatedBy);

	}
	public void sendNewPassword(String newPassword)
	{
		textboxHelper.sendKeys(usersNewPassword, newPassword);
		//usersNewPassword.sendKeys(newPassword);
	}
	public void sendConfirmPassword(String confirmPassword)
	{
		textboxHelper.sendKeys(usersConfirmPassword, confirmPassword);
		//usersConfirmPassword.sendKeys(confirmPassword);
	}
	public void selectRole(String roleName)
	{
		waitObj.waitForElementVisible(driver.findElement(By.xpath("//*[contains(text(),'"+ roleName + "')]//preceding-sibling::input[@type='checkbox']")));
		WebElement role = driver.findElement(By.xpath("//*[contains(text(),'"+ roleName + "')]//preceding-sibling::input[@type='checkbox']"));
		javaScriptHelper.javascriptClick(role);
		//((JavascriptExecutor) driver).executeScript("arguments[0].click();", role);
	}
	public void selectSite()
	{
		waitObj.waitForElementVisible(driver.findElement(By.xpath("//*[@class='siteTitle wrap-site-title ng-binding']")));
		List<WebElement> SiteCount = driver.findElements(By.xpath("//*[@class='siteTitle wrap-site-title ng-binding']"));
		int liTagcount = SiteCount.size();
		WebElement humanresources = driver.findElement(By.xpath("(//*[@ng-model='vm[site._id]'])[1]"));
		javaScriptHelper.javascriptClick(humanresources);
		for (int s = 2; s <= liTagcount; s++) {
			WebElement SitesAll = driver.findElement(
					By.xpath("(//*[@ng-model='vm.allSelected[level._id]'])["
							+ s + "]"));
			javaScriptHelper.javascriptClick(SitesAll);
			//((JavascriptExecutor)driver).executeScript("arguments[0].click();",SitesAll);
		}
	}
	public static void SiteClick(WebDriver driver, String Site) {
		driver
		.findElement(
				By.xpath("//*[contains(text(),'" + Site + "')]//preceding-sibling::input[@type='checkbox']"))
		.click();
	}


	public void selectSiteForCorrectData(String SiteName) {
		String[] SplitSites = SiteName.split(">");
		SiteClick(driver, SplitSites[0]);
		SiteClick(driver, SplitSites[1]);
		SiteClick(driver, SplitSites[2]);
	}
	
	public void verifyUesrCreation(String userName, String Password) 
	{
		try
		{
			waitObj.waitForElementVisible(ObjectRepo.driver.findElement(By.xpath("//*[@class='header-username-bx ng-binding']")));
			if (ObjectRepo.driver.findElements(By.xpath("//*[@class='header-username-bx ng-binding']")).size() == 1) 
			{
				//System.out.println("loging out from site");
				waitObj.waitForElementClickable(ObjectRepo.driver.findElement(By.xpath("//*[@class='header-username-bx ng-binding']")));
				ObjectRepo.driver.findElement(By.xpath("//*[@class='header-username-bx ng-binding']")).click();
				//Thread.sleep(1000);
				waitObj.waitForElementClickable(ObjectRepo.driver.findElement(By.xpath("//*[@ui-sref='logout']")));
				ObjectRepo.driver.findElement(By.xpath("//*[@ui-sref='logout']")).click();

			}
			else 
			{
				System.out.println("test");
			}
			//waitObj.waitForElementVisible(username);
			textboxHelper.sendKeys(username, userName);
			//username.sendKeys(userName);
			textboxHelper.sendKeys(password, Password);
			//password.sendKeys(Password);
			//Thread.sleep(1000);
			btnHelper.click(btn_login);
			//btn_login.click();
			//Thread.sleep(5000);
			String newPassword = excelData(12, 18);
			String newConfirmPassword = excelData(12, 19);
			String securityQuestion = excelData(12,15);
			String securityAns = excelData(12, 16);
			String esign = excelData(12, 17);
			SignUpForm(newPassword, newConfirmPassword, userName, securityQuestion, securityAns, esign);

			textboxHelper.sendKeys(username, userName);
			textboxHelper.sendKeys(password, newPassword);
			//username.sendKeys(userName);
			//password.sendKeys(newPassword);
			btnHelper.click(btn_login);
			//btn_login.click();
			//Thread.sleep(1000);
			//waitObj.waitForElementVisible(breadcrumbTxtUsers);
			//a.assertEquals(breadcrumbTxtUsers.getText(), ObjectRepo.reader.getHomeLabel());
			Assert.assertEquals(textboxHelper.getText(breadcrumbTxtUsers), ObjectRepo.reader.getHomeLabel());
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void SignUpForm(String NewPwdByUser, String ConfirmPwdByUser, String Username, String SecurityQue,
			String SecurityAns, String eSign) throws Exception {

		//Thread.sleep(1000);
		if (PasswordValidator(NewPwdByUser) && NewPwdByUser.equals(ConfirmPwdByUser)) {
			//Thread.sleep(1000);

			//Thread.sleep(1000);
			textboxHelper.clear(SignUpNewPassword);
			//SignUpNewPassword.clear();
			textboxHelper.sendKeys(SignUpNewPassword, NewPwdByUser);
			//SignUpNewPassword.sendKeys(NewPwdByUser);
			//Thread.sleep(1000);
			textboxHelper.clear(SignUpConfirmPassword);
			//SignUpConfirmPassword.clear();
			textboxHelper.sendKeys(SignUpConfirmPassword, ConfirmPwdByUser);
			//SignUpConfirmPassword.sendKeys(ConfirmPwdByUser);
			//Thread.sleep(1000);
			//Thread.sleep(1000);
			textboxHelper.clear(SignUpEsign);
			//SignUpEsign.clear();
			textboxHelper.sendKeys(SignUpEsign, eSign);
			//SignUpEsign.sendKeys(eSign);
			//Thread.sleep(1000);
			SelectSecurityQue(SecurityQue);
			//Thread.sleep(1000);
			textboxHelper.sendKeys(SignUpSecurityAns, SecurityAns);
			//SignUpSecurityAns.sendKeys(SecurityAns);
			//Thread.sleep(1000);
			btnHelper.click(SignSave);
			//SignSave.click();
			//Thread.sleep(1000);

		} 
		else 
		{
			softAssertion.assertEquals(true, false);
		}
	}
	public  boolean PasswordValidator(String passwd) throws Exception {
		String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$*])(?=\\S+$).{8,}";

		boolean LowerCase = false, UpperCase = false, Digit = false, SpecialCase = false,
				UnicodeCase = false,
				ValidPassword = false;

		if (passwd.matches(pattern)) {

			ValidPassword = true;
		} else {
			ValidPassword = true;

			if (passwd.length() < 8) {
				try {

					//Thread.sleep(1000); 
					//waitObj.waitForElementVisible(atleastMinCharMsg);

					//String MinCaseVal = atleastMinCharMsg.getText();
					softAssertion.assertEquals(textboxHelper.getText(atleastMinCharMsg), ObjectRepo.reader.getAtleastMinCharMsg());
				} catch (NoSuchElementException e) {
					System.err.println(e.toString());
				}
			} else if (passwd.length() > 20) {
				try {

					//Thread.sleep(1000);
					//waitObj.waitForElementVisible(atleastMaxCharMsg);
					//String MaxCaseVal = atleastMaxCharMsg.getText();
					softAssertion.assertEquals(textboxHelper.getText(atleastMaxCharMsg), ObjectRepo.reader.getAtleastMaxCharMsg());
				} catch (NoSuchElementException e) {
					e.toString();
				}
			}

			for (int i = 0; i < passwd.length(); i++) {
				char c = passwd.charAt(i);

				// See if the character is a letter or not.
				if (Character.isLetter(c)) {
					// ExtentBase.reportLog("This " + c + " = LETTER");

					if ((c < 65 || c > 90)) {
						// ExtentBase.reportLog("This " + c + " = LOWER LETTER");
						LowerCase = true;

					}
					if ((c < 97 || c > 122)) {
						// ExtentBase.reportLog("This " + c + " = CAPITAL LETTER");
						UpperCase = true;
					}
				}

				if (Character.isDigit(c)) {
					// ExtentBase.reportLog("This " + c + " DIGIT");
					Digit = true;

				}

				if (("" + c).matches("\\p{M}")) {
					// ExtentBase.reportLog("This " + c + " = UNICODE LETTER");
					UnicodeCase = true;

				}

				if (isSpecialCharacter(c)) {
					// ExtentBase.reportLog("This " + c + " = Special LETTER");
					SpecialCase = true;
				}
			}

			if (!LowerCase) {
				try {
					//Thread.sleep(1000);
					//waitObj.waitForElementVisible(atleastSmallCaseMsg);
					//String SmallCaseVal = atleastSmallCaseMsg.getText();
					softAssertion.assertEquals(textboxHelper.getText(atleastSmallCaseMsg), ObjectRepo.reader.getAtleastSmallCaseMsg());

				} catch (NoSuchElementException e) {
					System.out.println(e.toString());
				}
			}
			if (!UpperCase) {
				try {
					//Thread.sleep(1000);
					//waitObj.waitForElementVisible(atleastCapitalCaseMsg);
					//String UpperCaseVal = atleastCapitalCaseMsg.getText();
					softAssertion.assertEquals(textboxHelper.getText(atleastCapitalCaseMsg), ObjectRepo.reader.getAtleastCapitalCaseMsg());

				} catch (NoSuchElementException e) {
					System.out.println(e.toString());
				}
			}


			if (!SpecialCase) {
				try {
					//Thread.sleep(1000);
					//waitObj.waitForElementVisible(atleastSpecialMsg);
					//String SpecialCaseVal = atleastSpecialMsg.getText();
					softAssertion.assertEquals(textboxHelper.getText(atleastSpecialMsg), ObjectRepo.reader.getAtleastSpecialMsg());

				} catch (NoSuchElementException e) {
					System.out.println(e.toString());
				}
			}
			if (!Digit) {
				try {
					//Thread.sleep(1000);
					//waitObj.waitForElementVisible(atleastDigitMsg);
					//String DigitCase = atleastDigitMsg.getText();
					softAssertion.assertEquals(textboxHelper.getText(atleastDigitMsg), ObjectRepo.reader.getDigitCase());

				} catch (NoSuchElementException e) {
					e.toString();
				}
			}
		}
		softAssertion.assertAll();
		return ValidPassword;
	}
	public static  boolean isSpecialCharacter(Character c) {
		return c.toString().matches("[!@#$*]");
	}
	public void SelectSecurityQue(String Que) throws Exception {

		drpHelper.SelectUsingVisibleValue(SignUpSecurityQue, Que);
		//Select select = new Select(SignUpSecurityQue);
		//select.selectByVisibleText(Que);
	}
	public void clickonResetLink() throws Exception
	{
		Thread.sleep(2000);
		btnHelper.click(resetLInk);
		//resetLInk.click();
	}
	public void verifyActionpgbreadcrumb()
	{
		waitObj.waitForElementVisible(usersBreadcrumbtxt);
		String actionBreadcrumbs = usersBreadcrumbtxt.getText();
		Assert.assertEquals(ObjectRepo.reader.getHeaderType(), actionBreadcrumbs);
	}
	public void selectResetAccessBtn()
	{
		btnHelper.click(rdbtnResetUserAccess);
		//rdbtnResetUserAccess.click();
	}
	public void verifyResetFunctionality()
	{
		boolean actualValue;
		try
		{
			String FirstRowData[] = getFirstRowData(driver);
			if (FirstRowData[6].equals("Active"))
			{
				actualValue = userActiveORInactive();
				softAssertion.assertEquals(actualValue, true);
				//waitObj.waitForElementVisible(activeUserResetAccessMsg);
				//String ActiveInfoMsg = activeUserResetAccessMsg.getText();
				softAssertion.assertEquals(textboxHelper.getText(activeUserResetAccessMsg), ObjectRepo.reader.getActiveUserResetAccessMsg());
				btnHelper.click(saveButton);
				//saveButton.click();
				//Thread.sleep(6000);
				//waitObj.waitForElementVisible(usersBreadcrumbtxt);;
				//String PageName = usersBreadcrumbtxt.getText();
				softAssertion.assertEquals(textboxHelper.getText(usersBreadcrumbtxt), ObjectRepo.reader.getBreadcrumbtxtForUser());
			}
			else if (FirstRowData[6].equals("Inactive")) 
			{
				actualValue = userActiveORInactive();
				softAssertion.assertEquals(actualValue, false);
				//waitObj.waitForElementVisible(inactiveUserResetAccessMsg);
				//String InactiveInfoMsg = inactiveUserResetAccessMsg.getText();
				softAssertion.assertEquals( textboxHelper.getText(inactiveUserResetAccessMsg),
						ObjectRepo.reader.getInactiveUserResetAccessMsgOne() + " " + FirstRowData[0] + " " + ObjectRepo.reader.getInactiveUserResetAccessMsgTwo());
				btnHelper.click(resetCancelBtn);
				//resetCancelBtn.click();
				//Thread.sleep(6000);
				//waitObj.waitForElementVisible(usersBreadcrumbtxt);
				//String PageName = usersBreadcrumbtxt.getText();
				softAssertion.assertEquals(textboxHelper.getText(usersBreadcrumbtxt), ObjectRepo.reader.getBreadcrumbtxtForUser());
			}
			softAssertion.assertAll();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public boolean userActiveORInactive()
	{
		waitObj.waitForElementVisible(driver.findElement(By.xpath("//*[@ng-click='vm.submit(editResetPasswordForm)']")));
		if (driver.findElements(By.xpath("//*[@ng-click='vm.submit(editResetPasswordForm)']")).size() != 0)
		{	
			return true;
		}
		else
		{	
			return false;
		}
	}
	public void selectGeneratePassword()
	{
		try
		{
			btnHelper.click(resetPasswordRadioBtn);
			//resetPasswordRadioBtn.click();
			String generatePass  = excelData(12,20);
			drpHelper.selectDropDownText(generatePassword, generatePass);
			//Select select = new Select(generatePassword);
			//select.selectByVisibleText(generatePass);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		} 

	}
	public void enterNewPassword()
	{
		try
		{
			String newPassword  = excelData(16,18);
			textboxHelper.clear(resetPageNewPwd);
			//resetPageNewPwd.clear();
			textboxHelper.sendKeys(resetPageNewPwd, newPassword);
			//resetPageNewPwd.sendKeys(newPassword);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyInvalidNewPassword()
	{
		try
		{
			String newPassword  = excelData(14,18);
			PasswordValidator(newPassword);
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyNewPassMandatoryValMsg()
	{
		//String newPassValMsg= newPasswordManValMsg.getText();
		Assert.assertEquals(ObjectRepo.reader.getNewPasswordManValMsg(), textboxHelper.getText(newPasswordManValMsg));
	}
	public void verifyNewConfirmPassMandatoryValMsg()
	{
		//String newConfirmPassValMsg= resetPageConfirmPwdMandValmsg.getText();
		Assert.assertEquals(ObjectRepo.reader.getResetPageConfirmPwdMandValmsg(), textboxHelper.getText(resetPageConfirmPwdMandValmsg));
	}
	public void enterConfirmPassword()
	{
		String confirmPasswrod ="Abc@1234";
		textboxHelper.sendKeys(resetPageConfirmPwd, confirmPasswrod);
		//resetPageConfirmPwd.sendKeys();
	}
	public void verifyConfirmPasswordMsg()
	{ 
		try
		{
			//Thread.sleep(1000);
			//String misConfirmPassword = confirmPasswordMsg.getText();
			//Thread.sleep(1000);
			Assert.assertEquals(ObjectRepo.reader.getConfirmPasswordMsg(), textboxHelper.getText(confirmPasswordMsg));
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void clickonSubmitBtn()
	{
		try
		{
			//Thread.sleep(2000);
			btnHelper.click(passwordActionsubmitBtn);
			//passwordActionsubmitBtn.click();	

		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void verifyPasswordReset(String userName)
	{
		try
		{
			//Thread.sleep(8000);
			//	String PageName = usersBreadcrumbtxt.getText();
			Thread.sleep(2000);
			//Assert.assertEquals(textboxHelper.getText(usersBreadcrumbtxt), ObjectRepo.reader.getBreadcrumbtxtForUser());
			if (ObjectRepo.driver.findElements(By.xpath("//*[@class='header-username-bx ng-binding']")).size() == 1) 
			{
				System.out.println("loging out from site");
				ObjectRepo.driver.findElement(By.xpath("//*[@class='header-username-bx ng-binding']")).click();
				Thread.sleep(1000);
				ObjectRepo.driver.findElement(By.xpath("//*[@ui-sref='logout']")).click();
				Thread.sleep(2000);
			}
			else 
			{
				System.out.println("test");
			}

//			ExcelUtils loginExcel= new ExcelUtils();
//			loginExcel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Login");
			username.sendKeys(userName);
			password.sendKeys(newPassword);

			btn_login.click();
			Thread.sleep(1000);
			if(driver.findElements(By.xpath("//*[@name='editRecoverPasswordForm']")).size()!=0) {
				Assert.assertEquals(true, true);
			}else {
				Assert.assertEquals(false, true);
			}
		}
		catch(Exception e)
		{
			e.toString();
		}
	}
	public void autoGeneratePassword() throws Exception 
	{
		//String defaultItem = drpHelper.getFirstSelectedOption(resetPageGeneratePwd);
		//Select select = new Select(resetPageGeneratePwd);
		//WebElement option = select.getFirstSelectedOption();
		//String defaultItem = option.getText();
		Thread.sleep(2000);
		btnHelper.click(saveButton);
		//saveButton.click();
		//Thread.sleep(1000);
		//String PageName = usersBreadcrumbtxt.getText();
		waitObj.waitForElementVisible(usersAddNewBtn);
		Assert.assertEquals(textboxHelper.getText(usersBreadcrumbtxt), ObjectRepo.reader.getBreadcrumbtxtForUser());
	}

	public void enterValidUsername(String name) {
		textboxHelper.sendKeys(srcData, name);
	}

	public void clickOnCrossBtnOfSearch() {
		btnHelper.click(crossBtn);
	}

	public void verifyCrossBtnFun() {
		Assert.assertEquals(textboxHelper.getText(srcData), ""); 
	}

	public void selectPageSizeDrpDwn(String pagesize) {
		drpHelper.selectDropDownText(pageSizeDrpDwn, pagesize);
	}

	public void verifyPageSizeDrpDwn(int pageSize) throws Exception {
		Thread.sleep(1000);
		WebElement element = driver.findElement(By.xpath("//*[@ng-click='selectPage(page.number, $event)']"));
		scroll.scrollTillElem(element);
		List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='Action :']"));
		Assert.assertEquals(count.size(), pageSize);
	}

	public void verifyPaginationWithRecds(int pageSize) {
		String pagination = textboxHelper.getText(recodsCount);
		String [] words = pagination.split(" ");
		System.err.println(words[6]);
		String id1= words[6];
		int count = Integer.parseInt(id1);
		System.err.println(count);
		int countFinal = genericHelper.getCellingValue(count, pageSize);
		System.err.println(countFinal);
		btnHelper.click(lastBtn);
		Assert.assertEquals(countFinal, Integer.parseInt(textboxHelper.getText(lastPageNumber)));
	}

	public void verifyAuditTrailLinkForAllUsers() throws Exception {
		String nextBtnColor="#337ab7", viewAuditTrail;
		int counter =0;
		Thread.sleep(2000);
		String pagination = textboxHelper.getText(recodsCount);
		String [] words = pagination.split(" ");
		System.err.println(words[6]);
		String id1= words[6];
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			while(!nextBtnColor.equals("#777777")) {
				Thread.sleep(1000);
				List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='Audit Trail :']"));
				for(int i = 1; i<=count.size(); i++) {
					viewAuditTrail= driver.findElement(By.xpath("(//*[@data-th='Audit Trail :'])[" + i + "]")).getText();
					if(viewAuditTrail.equals("View Audit Trail")) {
						counter++;
					}else {
						System.out.println("This is nothing");
					}
				}
				System.err.println(counter);
				nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
				btnHelper.click(nextPagging);

			}

		}else
		{
			List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='Audit Trail :']"));
			for(int i = 1; i<=count.size(); i++) {
				viewAuditTrail= driver.findElement(By.xpath("(//*[@data-th='Audit Trail :'])[" + i + "]")).getText();
				if(viewAuditTrail.equals("View Audit Trail")) {
					counter++;
				}else {
					System.out.println("This is nothing");
				}
			}
		}
		System.err.println(counter);
		scroll.scrollTillElem(recodsCount);
		Assert.assertEquals(counter,Integer.parseInt(id1));

	}

	public void verifySearchForUserName(String username) throws Exception {
		String nextBtnColor="#337ab7", viewAuditTrail;
		int counter =0;
		Thread.sleep(2000);
		String pagination = textboxHelper.getText(recodsCount);
		String [] words = pagination.split(" ");
		System.err.println(words[6]);
		String id1= words[6];
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			while(!nextBtnColor.equals("#777777")) {
				Thread.sleep(1000);
				List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='Username :']"));
				for(int i = 1; i<=count.size(); i++) {
					viewAuditTrail= driver.findElement(By.xpath("(//*[@data-th='Username :'])[" + i + "]")).getText().trim();
					if(viewAuditTrail.equals(username)) {
						counter++;
					}else {
						System.out.println("This is nothing");
					}
				}
				System.err.println(counter);
				nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
				btnHelper.click(nextPagging);

			}

		}else
		{
			List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='Username :']"));
			for(int i = 1; i<=count.size(); i++) {
				viewAuditTrail= driver.findElement(By.xpath("(//*[@data-th='Username :'])[" + i + "]")).getText().trim();
				if(viewAuditTrail.equals(username)) {
					counter++;
				}else {
					System.out.println("This is nothing");
				}
			}
		}
		System.err.println(counter);
		scroll.scrollTillElem(recodsCount);
		Assert.assertEquals(counter,Integer.parseInt(id1));

	}

	public void verifySearchForUserRole(String userrole) throws Exception {
		String nextBtnColor="#337ab7", viewAuditTrail;
		int counter =0;
		Thread.sleep(2000);
		String pagination = textboxHelper.getText(recodsCount);
		String [] words = pagination.split(" ");
		System.err.println(words[6]);
		String id1= words[6];
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			while(!nextBtnColor.equals("#777777")) {
				Thread.sleep(1000);
				List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='User Roles :']"));
				for(int i = 1; i<=count.size(); i++) {
					viewAuditTrail= driver.findElement(By.xpath("(//*[@data-th='User Roles :'])[" + i + "]")).getText().trim();
					if(viewAuditTrail.equals(userrole)) {
						counter++;
					}else {
						System.out.println("This is nothing");
					}
				}
				System.err.println(counter);
				nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
				btnHelper.click(nextPagging);

			}

		}else
		{
			List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='User Roles :']"));
			for(int i = 1; i<=count.size(); i++) {
				viewAuditTrail= driver.findElement(By.xpath("(//*[@data-th='User Roles :'])[" + i + "]")).getText().trim();
				if(viewAuditTrail.equals(userrole)) {
					counter++;
				}else {
					System.out.println("This is nothing");
				}
			}
		}
		System.err.println(counter);
		scroll.scrollTillElem(recodsCount);
		Assert.assertEquals(counter,Integer.parseInt(id1));

	}

	public void verifySearchForUserFullName(String userfullname) throws Exception {
		String nextBtnColor="#337ab7", viewAuditTrail;
		int counter =0;
		Thread.sleep(2000);
		String pagination = textboxHelper.getText(recodsCount);
		String [] words = pagination.split(" ");
		System.err.println(words[6]);
		String id1= words[6];
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			while(!nextBtnColor.equals("#777777")) {
				Thread.sleep(1000);
				List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='Full Name :']"));
				for(int i = 1; i<=count.size(); i++) {
					viewAuditTrail= driver.findElement(By.xpath("(//*[@data-th='Full Name :'])[" + i + "]")).getText().trim();
					if(viewAuditTrail.equals(userfullname)) {
						counter++;
					}else {
						System.out.println("This is nothing");
					}
				}
				System.err.println(counter);
				nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
				btnHelper.click(nextPagging);

			}

		}else
		{
			List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='Full Name :']"));
			for(int i = 1; i<=count.size(); i++) {
				viewAuditTrail= driver.findElement(By.xpath("(//*[@data-th='Full Name :'])[" + i + "]")).getText().trim();
				if(viewAuditTrail.equals(userfullname)) {
					counter++;
				}else {
					System.out.println("This is nothing");
				}
			}
		}
		System.err.println(counter);
		scroll.scrollTillElem(recodsCount);
		Assert.assertEquals(counter,Integer.parseInt(id1));

	}

	public void verifySearchForDepartment(String department) throws Exception {
		String nextBtnColor="#337ab7", viewAuditTrail;
		int counter =0;
		Thread.sleep(2000);
		String pagination = textboxHelper.getText(recodsCount);
		String [] words = pagination.split(" ");
		System.err.println(words[6]);
		String id1= words[6];
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			while(!nextBtnColor.equals("#777777")) {
				Thread.sleep(1000);
				List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='Department :']"));
				for(int i = 1; i<=count.size(); i++) {
					viewAuditTrail= driver.findElement(By.xpath("(//*[@data-th='Department :'])[" + i + "]")).getText().trim();
					if(viewAuditTrail.equals(department)) {
						counter++;
					}else {
						System.out.println("This is nothing");
					}
				}
				System.err.println(counter);
				nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
				btnHelper.click(nextPagging);

			}

		}else
		{
			List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='Department :']"));
			for(int i = 1; i<=count.size(); i++) {
				viewAuditTrail= driver.findElement(By.xpath("(//*[@data-th='Department :'])[" + i + "]")).getText().trim();
				if(viewAuditTrail.equals(department)) {
					counter++;
				}else {
					System.out.println("This is nothing for else");
				}
			}
		}
		System.err.println(counter);
		scroll.scrollTillElem(recodsCount);
		Assert.assertEquals(counter,Integer.parseInt(id1));

	}

	public void clickOnFirstUserNameOf() throws Exception {
		btnHelper.click(firstUserName);
		beforeFirstName = helper.getClipboardContents(oldValue);
		beforeLastName = helper.getClipboardContents(middleName);
		beforeLastName = helper.getClipboardContents(txtLastName);
		beforeEmail = helper.getClipboardContents(txtEmailAddress);
		beforeDepartment = helper.getClipboardContents(userDepartment);
		String activeInactive = active.getAttribute("aria-checked");
		if(activeInactive.equals("true")) {
			beforeStatus = "Active";
		}else {
			beforeStatus = "Inactive";
		}
		beforeFailedAttempts = failedAttempts.getAttribute("aria-checked");
		beforeHippaUser = hippaUser.getAttribute("aria-checked");

	}

//	public void clickOnFirstAuditTrail() throws Exception {
//		Thread.sleep(1000);
//		btnHelper.click(firstAuditTrail);
//	}

	public void verifyOldAndNewFirstName(String SiteName) throws Exception {
		String[] SplitSites = SiteName.split(">");
		Thread.sleep(1000);
		btnHelper.click(usernameTab);
		btnHelper.click(clickMyProfile);
		double counter = 0;
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].removeAttribute('disabled','disabled')",departmentNameOfLoggedUser);
		createdBy = helper.getClipboardContents(firstnameSameAsLogin) + " " + helper.getClipboardContents(lastnameValForLoggedUser) + " (" + helper.getClipboardContents(departmentNameOfLoggedUser) + ")";
		Thread.sleep(1000);
		driver.navigate().back();
		btnHelper.click(firstAuditTrail);
		String fieldName , oldValueField, newValueField;
		SoftAssert sf = new SoftAssert();
		int pageSize = driver.findElements(By.xpath("//*[@data-th='Field Name :']")).size();
		String pagination = textboxHelper.getText(recodsCount);
		String [] words = pagination.split(" ");
		System.err.println(words[6]);
		afterEntries= Integer.parseInt(words[6]);
		int loopLimit= afterEntries-beforeEntries;
		if (loopLimit>pageSize) {
			int k = genericHelper.getCellingValue(loopLimit, pageSize);
			for(int j = 0; j<k; j++) {
				Thread.sleep(1000);
				for(int i = 1; i<=pageSize; i++) {
					fieldName= driver.findElement(By.xpath("(//*[@data-th='Field Name :'])[" + i + "]")).getText().trim();
					oldValueField = driver.findElement(By.xpath("(//*[@data-th='Old Value :'])[" + i + "]")).getText().trim();
					newValueField = driver.findElement(By.xpath("(//*[@data-th='New Value :'])[" + i + "]")).getText().trim();
					switch(fieldName) {
					case "First Name":
						sf.assertEquals(oldValueField, beforeFirstName);
						sf.assertEquals(newValueField, newFirstName);
						break;
					case "Status":
						sf.assertEquals(oldValueField, beforeStatus);
						sf.assertEquals(newValueField, activeInactiveStatus);
						break;
					case "Allow user to Login after Failed Attempts":
						sf.assertEquals(oldValueField, beforeFailedAttempts);
						sf.assertEquals(newValueField, failedAttempsStatus);
						break;
					case "Last Name":
						sf.assertEquals(oldValueField, beforeLastName);
						sf.assertEquals(newValueField, lastName);
						break;
					case "middlename":
						sf.assertEquals(oldValueField, beforeMiddleName);
						sf.assertEquals(newValueField, afterMiddleName);
						break;
					case "department":
						sf.assertEquals(oldValueField, beforeDepartment);
						sf.assertEquals(newValueField, departmentName);
						break;
					case "Email":
						sf.assertEquals(oldValueField, beforeEmail);
						sf.assertEquals(newValueField, afterEmail);
						break;
					case "Display Roles":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, rolesName);
						break;
					case "Modified By":
						sf.assertEquals(oldValueField, beforeModifiedBy);
						sf.assertEquals(newValueField, createdBy);
						break;
					case "Modified Date":
						sf.assertEquals(oldValueField.substring(0, oldValueField.length() - 3), oldCurrentTime);
						sf.assertEquals(newValueField.substring(0, newValueField.length() - 3), currentTimeAndDate);
						break;
					case "Roles > #1 > Title":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, rolesName);
						break;
					case "Sites > #1 > #1 > Title":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, SplitSites[0]);
						break;
					case "Sites > #2 > #1 > Title":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, SplitSites[1]);
						break;
					case "Sites > #3 > #1 > Title":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, SplitSites[2]);
						break;
					default:
						System.out.println("Nothing ");
						break;


					}
					counter++;
					if(counter==loopLimit) {
						break;
					}else {
						System.out.println("Counter Method else condition");
					}


				}
				btnHelper.click(nextPagging);
				

			}

		}else
		{
			for(int i = 1; i<=loopLimit; i++) {
				fieldName= driver.findElement(By.xpath("(//*[@data-th='Field Name :'])[" + i + "]")).getText().trim();
				oldValueField = driver.findElement(By.xpath("(//*[@data-th='Old Value :'])[" + i + "]")).getText().trim();
				newValueField = driver.findElement(By.xpath("(//*[@data-th='New Value :'])[" + i + "]")).getText().trim();
				switch(fieldName) {
				case "First Name":
					sf.assertEquals(oldValueField, beforeFirstName);
					sf.assertEquals(newValueField, newFirstName);
					break;
				case "Status":
					sf.assertEquals(oldValueField, beforeStatus);
					sf.assertEquals(newValueField, activeInactiveStatus);
					break;
				case "Allow user to Login after Failed Attempts":
					sf.assertEquals(oldValueField, beforeFailedAttempts);
					sf.assertEquals(newValueField, failedAttempsStatus);
					break;
				case "Last Name":
					sf.assertEquals(oldValueField, beforeLastName);
					sf.assertEquals(newValueField, lastName);
					break;
				case "Middle Name":
					sf.assertEquals(oldValueField, beforeMiddleName);
					sf.assertEquals(newValueField, afterMiddleName);
					break;
				case "department":
					sf.assertEquals(oldValueField, beforeDepartment);
					sf.assertEquals(newValueField, departmentName);
					break;
				case "Email":
					sf.assertEquals(oldValueField, beforeEmail);
					sf.assertEquals(newValueField, afterEmail);
					break;
				case "Display Roles":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, rolesName);
					break;
				case "Modified By":
					sf.assertEquals(oldValueField, beforeModifiedBy);
					sf.assertEquals(newValueField, createdBy);
					break;
				case "Modified Date":
					sf.assertEquals(oldValueField.substring(0, oldValueField.length() - 3), oldCurrentTime);
					sf.assertEquals(newValueField.substring(0, newValueField.length() - 3), currentTimeAndDate);
					break;
				case "Roles > #1 > Title":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, rolesName);
					break;
				case "Sites > #1 > #1 > Title":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, SplitSites[0]);
					break;
				case "Sites > #2 > #1 > Title":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, SplitSites[1]);
					break;
				case "Sites > #3 > #1 > Title":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, SplitSites[2]);
					break;
				default:
					System.out.println("Nothing ");
					break;

				}
			}
		}
		scroll.scrollTillElem(recodsCount);
		sf.assertAll();

	}

	public void enterEmailAddress()
	{
		System.err.println(userName);
		emailName = userName + "@yopmail.com";
		textboxHelper.sendKeys(txtEmailAddress, emailName);
	}

	public void verifyCorrectDataofAuditTrail(String SiteName) throws Exception {
		String[] SplitSites = SiteName.split(">");
		btnHelper.click(usernameTab);
		btnHelper.click(clickMyProfile);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].removeAttribute('disabled','disabled')",departmentNameOfLoggedUser);
		createdBy = helper.getClipboardContents(firstnameSameAsLogin) + " " + helper.getClipboardContents(lastnameValForLoggedUser) + " (" + helper.getClipboardContents(departmentNameOfLoggedUser) + ")";
		driver.navigate().back();
		btnHelper.click(firstAuditTrail);
		String nextBtnColor="#337ab7", fieldName , oldValueField, newValueField;
		int counter =0;
		SoftAssert sf = new SoftAssert();
		if (driver.findElements(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"))
				.size() != 0 == true) {
			while(!nextBtnColor.equals("#777777")) {
				Thread.sleep(1000);
				List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='Field Name :']"));
				for(int i = 1; i<=count.size(); i++) {
					fieldName= driver.findElement(By.xpath("(//*[@data-th='Field Name :'])[" + i + "]")).getText().trim();
					oldValueField = driver.findElement(By.xpath("(//*[@data-th='Old Value :'])[" + i + "]")).getText().trim();
					newValueField = driver.findElement(By.xpath("(//*[@data-th='New Value :'])[" + i + "]")).getText().trim();
					switch(fieldName) {
					case "First Name":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, newFirstName);
						break;
					case "Password Generation Method":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, passwordMethod);
						break;
					case "Status":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, activeInactiveStatus);
						break;
					case "Authentication Method":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, authentication);
						break;
					case "Allow user to Login after Failed Attempts":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, failedAttempsStatus);
						break;
					case "User Name":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, userName);
						break;
					case "Last Name":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, lastName);
						break;
					case "Department":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, departmentName);
						break;
					case "Email":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, emailName);
						break;
					case "Display Roles":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, rolesName);
						break;
					case "Created By":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, createdBy);
						break;
					case "Modified By":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, createdBy);
						break;
					case "Created Date":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField.substring(0, newValueField.length() - 3), currentTimeAndDate);
						break;
					case "Modified Date":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField.substring(0, newValueField.length() - 3), currentTimeAndDate);
						break;
					case "Roles > #1 > Title":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, rolesName);
						break;
					case "Sites > #1 > #1 > Title":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, SplitSites[0]);
						break;
					case "Sites > #2 > #1 > Title":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, SplitSites[1]);
						break;
					case "Sites > #3 > #1 > Title":
						sf.assertEquals(oldValueField, "");
						sf.assertEquals(newValueField, SplitSites[2]);
						break;
					default:
						System.out.println("Nothing ");
						break;	  

					}
				}
				System.err.println(counter);
				nextBtnColor = btnHelper.FontColorCodeHex(nextPagging);
				btnHelper.click(nextPagging);
			}
		}else
		{
			List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='Field Name :']"));
			for(int i = 1; i<=count.size(); i++) {
				fieldName= driver.findElement(By.xpath("(//*[@data-th='Field Name :'])[" + i + "]")).getText().trim();
				oldValueField = driver.findElement(By.xpath("(//*[@data-th='Old Value :'])[" + i + "]")).getText().trim();
				newValueField = driver.findElement(By.xpath("(//*[@data-th='New Value :'])[" + i + "]")).getText().trim();
				switch(fieldName) {
				case "First Name":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, newFirstName);
					break;
				case "Password Generation Method":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, passwordMethod);
					break;
				case "Status":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, activeInactiveStatus);
					break;
//				case "Authentication Method":
//					sf.assertEquals(oldValueField, "");
//					sf.assertEquals(newValueField, authenticationMethod);
//					break;
				case "Allow user to Login after Failed Attempts":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, failedAttempsStatus);
					break;
				case "User Name":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, userName);
					break;
				case "Last Name":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, lastName);
					break;
				case "Department":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, departmentName);
					break;
				case "Email":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, emailName);
					break;
				case "Display Roles":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, rolesName);
					break;
				case "Created By":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, createdBy);
					break;
				case "Modified By":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, createdBy);
					break;
				case "Created Date":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField.substring(0, newValueField.length() - 3), currentTimeAndDate);
					break;
				case "Modified Date":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField.substring(0, newValueField.length() - 3), currentTimeAndDate);
					break;
				case "Roles > #1 > Title":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, rolesName);
					break;
				case "Sites > #1 > #1 > Title":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, SplitSites[0]);
					break;
				case "Sites > #2 > #1 > Title":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, SplitSites[1]);
					break;
				case "Sites > #3 > #1 > Title":
					sf.assertEquals(oldValueField, "");
					sf.assertEquals(newValueField, SplitSites[2]);
					break;
				default:
					System.out.println("Nothing ");
					break;
				}
			}
		}
		System.err.println(counter);
		scroll.scrollTillElem(recodsCount);
		sf.assertAll();
		//Assert.assertEquals(counter,Integer.parseInt(id1));
	}

	public void getFirstUsername() {
		firstUsername = textboxHelper.getText(firstUserName);
	}

	public void verifyAuditTrailUserName() {
		Assert.assertEquals(textboxHelper.getText(auditTrailName).trim(),firstUsername);
	}

	public void getFirstVersionNo() {
		versionNumber = Double.parseDouble(textboxHelper.getText(versionNo).trim());
	}

	public void verifyUpdatedVersionNo() {
		String updatedVersioNo =textboxHelper.getText(versionNo).trim();
		Assert.assertEquals(updatedVersioNo.substring(0,updatedVersioNo.length()-1), Double.toString(versionNumber + 1));
	}

	public void verifyModifiedByNameField() throws Exception {
		btnHelper.click(usernameTab);
		btnHelper.click(clickMyProfile);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].removeAttribute('disabled','disabled')",departmentNameOfLoggedUser);
		createdBy = helper.getClipboardContents(firstnameSameAsLogin) + " " + helper.getClipboardContents(lastnameValForLoggedUser) + " (" + helper.getClipboardContents(departmentNameOfLoggedUser) + ")";
		driver.navigate().back();
		Assert.assertEquals(textboxHelper.getText(firstModifiedbyName).trim(), createdBy);
	}

	public void verifyDateAndTime() {
		String dateAndTime = textboxHelper.getText(firstDatAndtime).trim();
		Assert.assertEquals(dateAndTime.substring(0, dateAndTime.length() - 3), currentTimeAndDate);
	}

	public void clickOnLicenseAggBtn() {
		btnHelper.click(licenseAggBtn);
	}

	public void verifyLicenseAggBtnFunction() {
		Assert.assertEquals(textboxHelper.getText(licenseAggStatus).trim(), ObjectRepo.reader.getLicenseAggStatus());
	}

	public void clickOnFirstUserNameForOldAndNew() throws Exception {
		Thread.sleep(1000);
		btnHelper.click(firstUserName);
		beforeFirstName = helper.getClipboardContents(txtFirstName);
		beforeMiddleName = helper.getClipboardContents(middleName);
		beforeLastName = helper.getClipboardContents(txtLastName);
		beforeEmail = helper.getClipboardContents(txtEmailAddress);
		beforeDepartment = helper.getClipboardContents(userDepartment);
		String activeInactive = active.getAttribute("aria-checked");
		if(activeInactive.equals("true")) {
			beforeStatus = "Active";
		}else {
			beforeStatus = "Inactive";
		}
		beforeFailedAttempts = failedAttempts.getAttribute("aria-checked");
	//	beforeHippaUser = hippaUser.getAttribute("aria-checked");

	}

	public void getAuditTrailEntries() {
		WebElement lastDateAndTime = driver.findElement(By.xpath("(//*[@data-th='Date & Time :'])[1]"));
		String pagination = textboxHelper.getText(recodsCount);
		String [] words = pagination.split(" ");
		System.err.println(words[6]);
		beforeEntries= Integer.parseInt(words[6]);
		String dateandtime = lastDateAndTime.getText().trim();
		oldCurrentTime = dateandtime.substring(0,dateandtime.length()-3);
		String fieldName , newValueField;
		int flag =0; 
		List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='Field Name :']"));
		for(int i = 1; i<=count.size(); i++) {
			if(flag==0) {
				fieldName= driver.findElement(By.xpath("(//*[@data-th='Field Name :'])[" + i + "]")).getText().trim();
				newValueField = driver.findElement(By.xpath("(//*[@data-th='New Value :'])[" + i + "]")).getText().trim();
				switch(fieldName) {
				case "Modified By":
					beforeModifiedBy = newValueField;
					System.err.println(beforeModifiedBy);
					flag=1;
					break;
				default:
					System.out.println("This is nothing");
					break;
				}
			}else {
				break;
			}
		}
	}

	public void enterNewPasssword(String startofPassword) {
		newPassword = startofPassword + "@" + new Helper().randomNumberGeneration();
		textboxHelper.sendKeys(resetPageNewPwd, newPassword);
	}
	
	public void enterConfirmPasssword() {
		textboxHelper.sendKeys(resetPageConfirmPwd, newPassword);
	}
	
	public String getCurrnUserDetails() {
		btnHelper.click(usernameTab);
		btnHelper.click(clickMyProfile);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].removeAttribute('disabled','disabled')",departmentNameOfLoggedUser);
		try {
			userDeatils = helper.getClipboardContents(firstnameSameAsLogin) + " " + helper.getClipboardContents(lastnameValForLoggedUser);
		System.out.println("user details: "+userDeatils);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return createdBy;
	}
}
