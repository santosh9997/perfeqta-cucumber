package com.cucumber.framework.helper.PageObject;

import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;

public class EntitiesPageObject extends PageBase {
	private WebDriver driver;
	private final Logger log = LoggerHelper.getLogger(EntitiesPageObject.class);
	private WaitHelper waitObj;
	private TextBoxHelper textBoxHelper;
	public ButtonHelper btnHelper;

	public EntitiesPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		waitObj = new WaitHelper(driver, ObjectRepo.reader);
		textBoxHelper = new TextBoxHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.entities.list']")
	public WebElement entitiesTile;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-primary add-new pull-right ng-binding']")
	public WebElement addNewBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement pageTxtLabel;

	@FindBy(how = How.XPATH, using = "//*[@class='pull-right qc-form-tab-attributs']")
	public WebElement currVrsonTxtLbl;

	@FindBy(how = How.XPATH, using = "//*[@class='dropdown-toggle ng-binding btn btn-default']")
	public WebElement moduleDrpdwn;

	@FindBy(how = How.XPATH, using = "//*[@data-ng-click='selectAll()']")
	public WebElement chkAllBtn;

	@FindBy(how = How.XPATH, using = "//*[@data-ng-click='deselectAll();']")
	public WebElement unchkAllBtn;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='searchFilter']")
	public WebElement moduleSrchBox;

	@FindBy(how = How.XPATH, using = "//*[@name='entityName']")
	public WebElement entyNameTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMEISREQUIRED']")
	public WebElement entyNameTxtBoxReqMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMESHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement entyNameTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMESHOULDNOTBEMORETHAN200CHARACTERSLONG']")
	public WebElement entyNameTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMEMUSTBEUNIQUE']")
	public WebElement entyNameTxtBoxUniqMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='entitytag']")
	public WebElement entyTagTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.tag']")
	public WebElement entyTagAddBtn;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.ENTITYTAGISREQUIRED']")
	public WebElement entyTagTxtBoxReqMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.ENTITYTAGSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement entyTagTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.ENTITYTAGSHOULDNOTBEMORETHAN20CHARACTERSLONG']")
	public WebElement entyTagTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='!entityForm.entitytag.$invalid']")
	public WebElement entyTagTxtBoxUniqMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='entityDescription']")
	public WebElement entyDscipTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.entities.edit.entity.DESCRIPTIONSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement entyDscipTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.groupsettings.edit.DESCRIPTIONSHOULDBEATLEAST800CHARACTERSLONG']")
	public WebElement entyDscipTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-change='vm.isInstructionForUser()']")
	public WebElement instForUsrChkBox;

	@FindBy(how = How.XPATH, using = "//*[@ng-change='vm.onChangeInstructionForUserTextbox()']")
	public WebElement instForUsrTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERISREQUIRED']")
	public WebElement instForUsrTxtBoxRqiMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement instForUsrTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDNOTBEMORETHAN800CHARACTERSLONG']")
	public WebElement instForUsrTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='instructionForUserRadioButtonUploadHelpDocument']")
	public WebElement instForUsrUpldDocRdoBtn;

	@FindBy(how = How.XPATH, using = "//*[@name='questionSelection']")
	public WebElement attriNameTxtBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.ATTRIBUTENAMEISREQUIRED']")
	public WebElement attriNameTxtBoxRqiMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.ATTRIBUTENAMESHOULDBEATLEAST2CHARACTERSLONG']")
	public WebElement attriNameTxtBoxMinMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.procedure.edit.procedure.ATTRIBUTENAMESHOULDNOTBEMORETHAN200CHARACTERSLONG']")
	public WebElement attriNameTxtBoxMaxMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.question.isKeyValue']")
	public WebElement attkeyIdchkBox;

	@FindBy(how = How.XPATH, using = "//*[@id=\"breadcrum-ipad-768\"]/li[3]/a")
	public WebElement entBrdCrum;

	@FindBy(how = How.XPATH, using = "//tr[1]/td[9]/span[1]/span/a")
	public WebElement entityViewLinkInformation;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Your current Entity is used in these Apps:')]")
	public WebElement entityViewPopuplabel;
	// --------------------public methods--------------------

	public WebDriver getDriver() {
		return this.driver;

	}

	public void clickEntitiesTile() throws Exception {
		btnHelper.click(entitiesTile);
	}

	public void clickAddNewBtn() throws Exception {
		btnHelper.click(addNewBtn);
	}

	public void verifyAddEditTxtLabel() {
		Assert.assertEquals(textBoxHelper.getText(pageTxtLabel), ObjectRepo.reader.getPageTxtLabelForEditEntity());
	}

	public void verifyEntitiesTxtLabel() {
		Assert.assertEquals(textBoxHelper.getText(pageTxtLabel), ObjectRepo.reader.getPageTxtLabelForEntity());
	}

	public void verifyCurrVrsnTxtLabel() {
		Assert.assertEquals(textBoxHelper.getText(currVrsonTxtLbl), ObjectRepo.reader.getCurrVrsonTxtLbl());
	}

	public void moduleDrpdwnClick() {
		btnHelper.click(moduleDrpdwn);
	}

	public void chkAllBtnClick() {
		btnHelper.click(chkAllBtn);
	}

	public void attkeyIdchkBoxClick() {
		btnHelper.click(attkeyIdchkBox);
	}

	public void unchkAllBtnClick() {
		btnHelper.click(unchkAllBtn);
	}

	public void verifyChkAllBtnFun() {

		int count = 6, CheckedValue = 0, expectedChkedValue = 3;

		// number of available modules
		int numOfModule = 3;

		for (int i = 0; i < numOfModule; i++) {
			if (ObjectRepo.driver.findElement(By.xpath("//div/ul/li[" + count + "]/a/div/label/input")).isSelected())

				CheckedValue++;
			count++;
		}
		System.out.println("Checked Values: " + CheckedValue);

		Assert.assertEquals(CheckedValue, expectedChkedValue);
	}

	public void verifyUnchkAllBtnFun() {

		int count = 6, CheckedValue = 0, expectedChkedValue = 0;

		// number of available modules
		int numOfModule = 3;

		for (int i = 0; i < numOfModule; i++) {
			if (ObjectRepo.driver.findElement(By.xpath("//div/ul/li[" + count + "]/a/div/label/input")).isSelected())

				CheckedValue++;
			count++;
		}
		System.out.println("Checked Values: " + CheckedValue);

		Assert.assertEquals(CheckedValue, expectedChkedValue);
	}

	public void moduleSrchBoxSendkeys(String SrchValue) {
		textBoxHelper.sendKeys(moduleSrchBox, SrchValue);
	}

	public void verifyModuleSrchBoxFun(String SrchValue) {

		String module = null, allModule = "";

		List<WebElement> srchResult = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-repeat='option in options | filter: searchFilter']"));

		for (int i = 0; i < srchResult.size(); i++) {
			module = srchResult.get(i).getText();
			allModule = module + allModule;
			System.out.println(srchResult.get(i).getText());
		}
		System.out.println(allModule);
		Assert.assertEquals(allModule, SrchValue);
	}

	public void entyNameTxtBoxSendkeys(String inputValue) {
		textBoxHelper.sendKeys(entyNameTxtBox, inputValue);
	}

	public void verifyEntyNameTxtBoxRequiValid() {

		Assert.assertEquals(textBoxHelper.getText(entyNameTxtBoxReqMsg), ObjectRepo.reader.getEntyNameTxtBoxReqMsg());
	}

	public void verifyEntyNameTxtBoxMinValid() {

		Assert.assertEquals(textBoxHelper.getText(entyNameTxtBoxMinMsg), ObjectRepo.reader.getEntyNameTxtBoxMinMsg());
	}

	public void verifyEntyNameTxtBoxMaxValid() {

		Assert.assertEquals(textBoxHelper.getText(entyNameTxtBoxMaxMsg), ObjectRepo.reader.getEntyNameTxtBoxMaxMsg());
	}

	public void verifyEntyNameTxtBoxUniqValid() {

		Assert.assertEquals(textBoxHelper.getText(entyNameTxtBoxUniqMsg), ObjectRepo.reader.getEntyNameTxtBoxUniqMsg());
	}

	public void verifyEntyNameTxtBoxNoValid() {

		boolean reqMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMEISREQUIRED']"))
				.size() == 0,
				minMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMESHOULDBEATLEAST2CHARACTERSLONG']"))
						.size() == 0,
				maxMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMESHOULDNOTBEMORETHAN200CHARACTERSLONG']"))
						.size() == 0,
				uniqMsg = ObjectRepo.driver
						.findElements(
								By.xpath("//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMEMUSTBEUNIQUE']"))
						.size() == 0,
				flag;

		if (reqMsg && minMsg && maxMsg && uniqMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void entyTagTxtBoxSendkeys(String inputValue) {
		textBoxHelper.sendKeys(entyTagTxtBox, inputValue);
	}

	public void entyTagAddBtnClick() {
		btnHelper.click(entyTagAddBtn);
	}

	public void verifyEntyTagTxtBoxRequiValid() {

		Assert.assertEquals(textBoxHelper.getText(entyTagTxtBoxReqMsg), ObjectRepo.reader.getEntyTagTxtBoxReqMsg());
	}

	public void verifyEntyTagTxtBoxMinValid() {

		Assert.assertEquals(textBoxHelper.getText(entyTagTxtBoxMinMsg), ObjectRepo.reader.getEntyTagTxtBoxMinMsg());
	}

	public void verifyEntyTagTxtBoxMaxValid() {

		Assert.assertEquals(textBoxHelper.getText(entyTagTxtBoxMaxMsg), ObjectRepo.reader.getEntyTagTxtBoxMaxMsg());
	}

	public void verifyEntyTagTxtBoxUniqValid() {

		Assert.assertEquals(textBoxHelper.getText(entyTagTxtBoxUniqMsg), ObjectRepo.reader.getEntyTagTxtBoxUniqMsg());
	}

	public void verifyEntyTagTxtBoxNoValid() {

		boolean reqMsg = ObjectRepo.driver
				.findElements(By.xpath("//*[@translate='secure.admin.entities.edit.entity.ENTITYTAGISREQUIRED']"))
				.size() == 0,
				minMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.entities.edit.entity.ENTITYTAGSHOULDBEATLEAST2CHARACTERSLONG']"))
						.size() == 0,
				maxMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.entities.edit.entity.ENTITYTAGSHOULDNOTBEMORETHAN20CHARACTERSLONG']"))
						.size() == 0,
				uniqMsg = ObjectRepo.driver
						.findElements(
								By.xpath("//*[@translate='secure.admin.entities.edit.entity.ENTITYNAMEMUSTBEUNIQUE']"))
						.size() == 0,
				flag;

		if (reqMsg && minMsg && maxMsg && uniqMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void entyDscipTxtBoxSendkeys(String inputValue) {
		textBoxHelper.sendKeys(entyDscipTxtBox, inputValue);
	}

	public void verifyEntyDscipTxtBoxMinValid() {

		Assert.assertEquals(textBoxHelper.getText(entyDscipTxtBoxMinMsg), ObjectRepo.reader.getEntyDscipTxtBoxMinMsg());
	}

	public void verifyEntyDscipTxtBoxMaxValid() {

		Assert.assertEquals(textBoxHelper.getText(entyDscipTxtBoxMaxMsg), ObjectRepo.reader.getEntyDscipTxtBoxMaxMsg());
	}

	public void verifyEntyDscipTxtBoxNoValid() {

		boolean minMsg = ObjectRepo.driver.findElements(By
				.xpath("//*[@translate='secure.admin.entities.edit.entity.DESCRIPTIONSHOULDBEATLEAST2CHARACTERSLONG']"))
				.size() == 0,
				maxMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.groupsettings.edit.DESCRIPTIONSHOULDBEATLEAST800CHARACTERSLONG']"))
						.size() == 0,
				flag;

		if (minMsg && maxMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void instForUsrTxtBoxSendkeys(String inputValue) {
		textBoxHelper.sendKeys(instForUsrTxtBox, inputValue);
	}

	public void instForUsrChkBoxClick() {
		btnHelper.click(instForUsrChkBox);
	}

	public void verifyInstForUsrTxtBoxRequiValid() {

		Assert.assertEquals(textBoxHelper.getText(instForUsrTxtBoxRqiMsg), ObjectRepo.reader.getInstForUsrTxtBoxRqiMsg());
	}

	public void verifyInstForUsrTxtBoxMinValid() {

		Assert.assertEquals(textBoxHelper.getText(instForUsrTxtBoxMinMsg), ObjectRepo.reader.getInstForUsrTxtBoxMinMsg());
	}

	public void verifyInstForUsrTxtBoxMaxValid() {

		Assert.assertEquals(textBoxHelper.getText(instForUsrTxtBoxMaxMsg),ObjectRepo.reader.getInstForUsrTxtBoxMaxMsg());
	}

	public void verifyInstForUsrTxtBoxNoValid() {

		boolean reqMsg = ObjectRepo.driver
				.findElements(By
						.xpath("//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERISREQUIRED']"))
				.size() == 0,
				minMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDBEATLEAST2CHARACTERSLONG']"))
						.size() == 0,
				maxMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.procedure.edit.procedure.INSTRUCTIONFORUSERSHOULDNOTBEMORETHAN800CHARACTERSLONG']"))
						.size() == 0,
				flag;

		if (reqMsg && minMsg && maxMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void verifyInstForUsrTxtBoxAppears() {

		boolean instForUsrTxtBox = ObjectRepo.driver
				.findElements(By.xpath("//*[@ng-change='vm.onChangeInstructionForUserTextbox()']")).size() != 0, flag;

		if (instForUsrTxtBox) {
			flag = true;
		} else {
			flag = false;
		}
		Assert.assertEquals(flag, true);
	}

	public void verifyinstForUsrUpldDocAppears() {

		boolean instForUsrTxtBox = ObjectRepo.driver
				.findElements(By.xpath("//*[@title='Upload Document from Local Drive']")).size() != 0, flag;

		if (instForUsrTxtBox) {
			flag = true;
		} else {
			flag = false;
		}
		Assert.assertEquals(flag, true);
	}

	public void instForUsrUpldDocRdoBtnClick() {

		btnHelper.click(instForUsrUpldDocRdoBtn);
	}

	public void attriNameTxtBoxSendkeys(String inputValue) {
		textBoxHelper.sendKeys(attriNameTxtBox, inputValue);
	}

	public void verifyAttriNameTxtBoxRequiValid() {

		Assert.assertEquals(textBoxHelper.getText(attriNameTxtBoxRqiMsg), ObjectRepo.reader.getAttriNameTxtBoxRqiMsg());
	}

	public void verifyAttriNameTxtBoxMinValid() {

		Assert.assertEquals(textBoxHelper.getText(attriNameTxtBoxMinMsg), ObjectRepo.reader.getAttriNameTxtBoxMinMsg());
	}

	public void verifyAttriNameTxtBoxMaxValid() {

		Assert.assertEquals(textBoxHelper.getText(attriNameTxtBoxMaxMsg), ObjectRepo.reader.getAttriNameTxtBoxMaxMsg());
	}

	public void verifyAttriNameTxtBoxNoValid() {

		boolean reqMsg = ObjectRepo.driver
				.findElements(
						By.xpath("//*[@translate='secure.admin.procedure.edit.procedure.ATTRIBUTENAMEISREQUIRED']"))
				.size() == 0,
				minMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.procedure.edit.procedure.ATTRIBUTENAMESHOULDBEATLEAST2CHARACTERSLONG']"))
						.size() == 0,
				maxMsg = ObjectRepo.driver.findElements(By.xpath(
						"//*[@translate='secure.admin.procedure.edit.procedure.ATTRIBUTENAMESHOULDNOTBEMORETHAN200CHARACTERSLONG']"))
						.size() == 0,
				flag;

		if (reqMsg && minMsg && maxMsg) {
			flag = true;
		} else {
			flag = false;
		}

		Assert.assertEquals(flag, true);
	}

	public void verifyLnkToEntyRdoNotAppears() {

		new Scroll().scrollDown(driver);

		boolean LnkToEntyRdo = ObjectRepo.driver.findElements(By.xpath("//*[@name='linkToEntity']")).size() != 0, flag;

		if (LnkToEntyRdo) {
			flag = true;
		} else {
			flag = false;
		}
		Assert.assertEquals(flag, false);
	}

	public void verifysearch(String searchedItem) throws Exception {

		AttributesPageObject obj = new AttributesPageObject(ObjectRepo.driver);

		String PaginationVal = null, PaginationSelectedVal = null, srchResultNo = null;
		try {
			PaginationVal = obj.paginationText.getText();
			srchResultNo = obj.srcResultPageNo.getText();
			PaginationSelectedVal = obj.pageSizeDrp_GetSelectedValue();
			new Search().SearchVerification(PaginationVal, PaginationSelectedVal, obj.grid_data, searchedItem,
					obj.textSearch, srchResultNo);
		} catch (Exception e) {
			System.err.println(e);
		}

	}

	public void entBrdCrumClick() throws Exception {
		btnHelper.click(entBrdCrum);
	}

	public void verifyRedrctToEntPage() {

		boolean LnkToEntyRdo = ObjectRepo.driver.findElements(By.xpath("//*[@id='breadcrum-ipad-768']/li[3]/a"))
				.size() != 0, flag;

		if (LnkToEntyRdo) {
			flag = true;
		} else {
			flag = false;
		}
		Assert.assertEquals(flag, false);
	}

	public void entityViewLinkClick() {
		btnHelper.click(entityViewLinkInformation);
	}

	public void verifyViewPopupLabel() {

		try {
			String ViewPopupLabel = entityViewPopuplabel.getText();
			Assert.assertEquals(ViewPopupLabel, ObjectRepo.reader.getEntityViewPopuplabel());
			Thread.sleep(1000);
			// ObjectRepo.driver.navigate().refresh();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}

	// Rough work ...
	public void unique() {
		int[] a = { 1, 2, 3, 4, 5, 1, 2, 3, 4 };
		int count = 0;

		for (int i = 0; i < a.length; i++) {
			if (a[i] == count) {

				count = a[i];
			} else {

			}
			System.out.println("this is uniq: " + a.toString());
		}
	}

}// end
