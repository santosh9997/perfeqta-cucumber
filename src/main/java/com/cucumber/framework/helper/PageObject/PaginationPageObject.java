package com.cucumber.framework.helper.PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Pagination.Pagination;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.settings.ObjectRepo;

public class PaginationPageObject extends PageBase
{
	private final Logger log = LoggerHelper.getLogger(SetsPageObject.class);
	private Pagination paginationObj = new Pagination();
	private WebDriver driver;
	public static String firstPage;
	public static boolean lastPage,nextPage,previousPage;
	public ButtonHelper btnHelper;
	private TextBoxHelper textBoxHelper;
	private DropDownHelper drpHelper;

	public PaginationPageObject(WebDriver driver)
	{
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		textBoxHelper = new TextBoxHelper(driver);
		drpHelper = new DropDownHelper(ObjectRepo.driver);
	}

	@FindBy(how = How.LINK_TEXT, using = "View Audit Trail")
	public WebElement ViewAuditTrail_Link;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='selectPage(totalPages, $event)']")
	public WebElement clickLastBtn;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='selectPage(1, $event)']")
	public WebElement clickFirstBtn;

	@FindBy(how = How.XPATH, using = ".//grid/div[3]/div[2]/ul/li[3]/a")
	public WebElement verifyFirstPage;

	@FindBy(how = How.XPATH, using = "//li/a[@ng-click='selectPage(page + 1, $event)']")
	public WebElement clickNextPage;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement verifyNextData;

	@FindBy(how = How.XPATH, using = "//li/a[@ng-click='selectPage(page - 1, $event)']")
	public WebElement clickPreviousPage;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement verifyPreviousData;

	@FindBy(how = How.XPATH, using = "//li[@class='pagination-last ng-scope disabled']")
	public WebElement verifyLastDisabled;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement showingText;

	@FindBy(how = How.XPATH, using = "//select[@id='ddlPageSize']")
	public WebElement pageSizeDrpDwn;

	public void clickToViewAuditTrailLink() throws Exception 
	{
		Thread.sleep(2000);
		btnHelper.click(ViewAuditTrail_Link);
	}

	public void clickLastBtnPagination() throws Exception 
	{
		if(driver.findElements(By.xpath("//a[@ng-click='selectPage(totalPages, $event)']")).size()!=0) {
			btnHelper.click(clickLastBtn);
			if (driver.findElements(By.xpath("//li[@class='pagination-last ng-scope disabled']")).size() == 0) 
			{
				lastPage = false;
			} 
			else {
				lastPage = true;
			}
		}else {
			lastPage = true;
		}
	}

	public void clickFirstBtnPagination() throws Exception
	{
		if(driver.findElements(By.xpath("//a[@ng-click='selectPage(1, $event)']")).size()!=0) {
			btnHelper.click(clickFirstBtn);
			firstPage=textBoxHelper.getText(verifyFirstPage);
		}else {
			firstPage="1";
		}

	}
	public void verifyFirstPagePagination() 
	{
		log.info(verifyFirstPage);
		System.out.println(firstPage);
		Assert.assertEquals(firstPage, "1");
	}
	public void clickNextBtnPagination() throws Exception
	{
		if(driver.findElements(By.xpath("//li/a[@ng-click='selectPage(page + 1, $event)']")).size()!=0) {
			clickNextPage.click();
			String output = textBoxHelper.getText(verifyNextData);
			nextPage = output.startsWith("11", 8);
		}else {
			nextPage = true;
		}

	}
	public void verifyNextBtnPagination() throws Exception
	{
		log.info(verifyNextData);
		Thread.sleep(1000);

		Assert.assertEquals(nextPage, true);
	}
	public void clickPreviousBtnPagination() throws Exception
	{
		log.info(clickPreviousPage);
		if(driver.findElements(By.xpath("//li/a[@ng-click='selectPage(page + 1, $event)']")).size()!=0) {
			btnHelper.click(clickNextPage);
			//Thread.sleep(1000);
			btnHelper.click(clickPreviousPage);
			//Thread.sleep(1000);
			String output = textBoxHelper.getText(verifyPreviousData);
			previousPage = output.startsWith("1", 8);
			firstPage=textBoxHelper.getText(verifyFirstPage);
		}else {
			previousPage = true;
			firstPage = "1";
		}

	}
	public void verifyPreviousBtnPagination() throws Exception 
	{
		log.info(clickPreviousPage);
		System.err.println(previousPage);
		Assert.assertEquals(previousPage, true);
	}
	public void verifyLastBtnPagination() 
	{
		log.info(verifyLastDisabled);
		Assert.assertEquals(lastPage, true);

	}
	public void verifyCountOfListingScreen()
	{
		log.info("verify count of listing screen");
		String paginationText = textBoxHelper.getText(showingText);
		//		Select select = new Select(pageSizeDrpDwn);
		//		WebElement option = select.getFirstSelectedOption();
		//		String pageSizeDrpDwnVal = option.getText();
		//if(pageSizeDrpDwn!=null)
		//{
			 String pageSizeDrpDwnVal = drpHelper.getFirstSelectedOption(pageSizeDrpDwn);
			if (pageSizeDrpDwnVal!=null)
			{
				WebElement Grid = driver.findElement(By.xpath("//*[@ng-if='vm.options.data && vm.options.data.length']"));
				try
				{
					paginationObj.PaginationVerification(paginationText, pageSizeDrpDwnVal, Grid);
				}
				catch(Exception e)
				{
					e.toString();
				}
			}
		}
	
}
