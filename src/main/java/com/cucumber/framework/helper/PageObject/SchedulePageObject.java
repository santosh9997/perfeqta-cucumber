package com.cucumber.framework.helper.PageObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.CheckBox.CheckBoxOrRadioButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Generic.GenericHelper;
import com.cucumber.framework.helper.Javascript.JavaScriptHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.settings.ObjectRepo;

public class SchedulePageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	public CheckBoxOrRadioButtonHelper checkBoxAndBtnHelper;
	public TextBoxHelper textBoxHelper;
	public DropDownHelper drpdwnHelper;
	public GenericHelper genricHelper;
	public Helper helper = new Helper();
	private final Logger log = LoggerHelper.getLogger(SchedulePageObject.class);

	public SchedulePageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		checkBoxAndBtnHelper = new CheckBoxOrRadioButtonHelper(driver);
		textBoxHelper = new TextBoxHelper(driver);
		drpdwnHelper = new DropDownHelper(driver);
		genricHelper = new GenericHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//*[@ng-click=\"vm.go('secure.qc_calendar',vm.allPer.calendar)\"]")
	public WebElement scheduleTile;

	@FindBy(how = How.XPATH, using = "//span[@class='currentStep ng-binding']")
	public WebElement scheduleHeading;

	@FindBy(how = How.XPATH, using = "//div/div[2]/div/button")
	public WebElement appDrpDwn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search...']")
	public WebElement appDrpDwnSearchfield;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.setTab(3)']")
	public WebElement reviewCompletedAppTab;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.setTab(1)']")
	public WebElement startScheduleApps;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.caledardetails.dateRangebydrop']")
	public WebElement dateDrpDwn;

	@FindBy(how = How.XPATH, using = "//ul/div/div[2]/div/div/div/button")
	public WebElement filterByDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@class='col-md-10 col-sm-10 col-xs-10 alert alert-warning no-records ng-binding']")
	public WebElement noRecordsFound;

	@FindBy(how = How.XPATH, using = "//*[contains(normalize-space(text()),'Showing')]")
	public WebElement paginationText;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.calendar.length > 0']")
	public WebElement gridData;

	@FindBy(how = How.XPATH, using = "//a[@data-ng-click='deselectAll();']")
	public WebElement uncheckAllDrpDwn;

	@FindBy(how = How.XPATH, using = "//div[2]/div/ul/li[6]/a/div/label/input")
	public WebElement selectApp;

	@FindBy(how = How.XPATH, using = "//ul/div/div[2]/div/div/div/ul/li[6]/a/div/label/input")
	public WebElement selectUpcomingStatus;

	@FindBy(how = How.XPATH, using = "//ul/div/div[2]/div/div/div/ul/li[7]/a/div/label/input")
	public WebElement selectOverdueStatus;

	@FindBy(how = How.XPATH, using = "//ul/div/div[2]/div/div/div/ul/li[8]/a/div/label/input")
	public WebElement selectReadyToPerformStatus;

	@FindBy(how = How.XPATH, using = "//ul/div/div[2]/div/div/div/ul/li[9]/a/div/label/input")
	public WebElement selectIncompleteStatus;

	@FindBy(how = How.XPATH, using = "//ul/div/div[2]/div/div/div/ul/li[10]/a/div/label/input")
	public WebElement selectCompletedStatus;

	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.caledardetails.searchby']")
	public WebElement searchByDrpDwn;
	
	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.collapsBox = !vm.collapsBox;']")
	public WebElement collapseExpandLink;
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[2]/td[4]/a/strong/span")
	public WebElement firstAppLink;
	
	@FindBy(how = How.XPATH, using = "//div[@class='col-md-8 col-sm-6 col-xs-12 qcFormTitle']")
	public WebElement appName;







	// --------------------public methods--------------------

	public WebDriver getDriver() {
		return this.driver;
	}

	public void clickScheduleTile() throws Exception {
		Thread.sleep(2000);
		 btnHelper.click(scheduleTile);
		//Thread.sleep(4000);
	}

	public void verifyScheduleHeading() throws Exception {
		Assert.assertEquals( textBoxHelper.getText(scheduleHeading), ObjectRepo.reader.getScheduleHeading());
	}

	public void clickAppDrpDwn() throws Exception {
		 btnHelper.click(appDrpDwn);
		//Thread.sleep(3000);
	}

	public void verifyAppDrpDwnClickable() throws Exception {
		boolean actual =  genricHelper.isDisplayed(appDrpDwnSearchfield);
		Assert.assertEquals(actual, true);
	}

	public void verifyReviewCompletedAppLabel() throws Exception {
		Assert.assertEquals( textBoxHelper.getText(reviewCompletedAppTab), ObjectRepo.reader.getReviewCompletedAppTab());
	}

	public void verifyStartScheduleAppsLabel() throws Exception {
		Assert.assertEquals( textBoxHelper.getText(startScheduleApps), ObjectRepo.reader.getStartScheduleApps());
	}

	public void searchSpecificApp(String SearchApp) throws Exception {
		 btnHelper.click(uncheckAllDrpDwn);
		 textBoxHelper.sendKeys(appDrpDwnSearchfield,SearchApp);
		 btnHelper.click(selectApp);
		 btnHelper.click(appDrpDwn);
	}

	public void clickDateDrpDwn() throws Exception {
		 btnHelper.click(dateDrpDwn);
		//Thread.sleep(3000);
	}

	public void selectThisYearOptionDateDrpDwn() throws Exception {
		drpdwnHelper.selectDropDownText(dateDrpDwn, "This Year");
	}

	public void clickFilterByStatusDrpDwn() throws Exception {
		 btnHelper.click(filterByDrpDwn);
		//Thread.sleep(3000);
	}

	public void selectCompletedOptionFilterByStatusDrpDwn() throws Exception {
		//		drpdwnHelper.selectDropDownText(filterByDrpDwn, status);
		 btnHelper.click(selectCompletedStatus);
	}

	public void verifyHourlyAndCompletedData() throws Exception
	{
		if (driver.findElements(By.xpath("//*[@class='col-md-10 col-sm-10 col-xs-10 alert alert-warning no-records ng-binding']")).size() != 0) 
		{
			System.out.println("No record Found Condition met.");
		} else {
			int upcomingcount = 0, overduecount = 0, incompletecount = 0, completedcount = 0, readyToPerformcount = 0;
			boolean actual = false;
			//Thread.sleep(1000);

			String Pagination = textBoxHelper.getText(paginationText);
			String PaginationDrp = "10";

			String[] splited = Pagination.split("\\s+");

			String Time, ScheduleName, AppName, Type, Status, ScheduleDate;
			ArrayList<String> DuplicateSchedule = new ArrayList<String>();

			Arrays.toString(splited);

			if (PaginationDrp.equalsIgnoreCase("1")) {
				PaginationDrp = splited[5];
			}
			int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
					TotalEntry = Integer.parseInt(splited[5]), PageSizeDrp = Integer.parseInt(PaginationDrp);
			if (TotalEntry <= 10) {
				PageSizeDrp = 1;
			}
			for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {

				WebElement Webtable = gridData;
				; // Replace TableID with Actual Table ID or Xpath
				//Thread.sleep(1000);

				// To locate rows of table.
				List<WebElement> rows_table = Webtable.findElements(By.tagName("tr"));
				// To calculate no of rows In table.
				int rows_count = rows_table.size();

				// Loop will execute till the last row of table.
				for (int row = 1; row < rows_count; row++) {
					// To locate columns(cells) of that specific row.

					List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
					// To calculate no of columns (cells). In that specific row.
					int columns_count = Columns_row.size();

					ScheduleDate = Columns_row.get(0).getText().replaceAll(",", "").replaceAll("\n", " ");
					Time = Columns_row.get(1).getText().replaceAll(":", ".");
					ScheduleName = Columns_row.get(2).getText();
					AppName = Columns_row.get(3).getText();
					Type = Columns_row.get(4).getText();
					Status = Columns_row.get(7).getText();

					if (ScheduleName.equalsIgnoreCase("Frequency with mothly") && AppName.equalsIgnoreCase("Cell 3.4")
							&& Type.equalsIgnoreCase("Monthly")) {
						// Schedule_Action.CheckDates("27 Nov 2017 06.00 AM","28 Nov 2018 06.00 AM","28
						// Nov 2019 05.00 AM");

						actual = CheckDates("02 Apr 2018 03.00 AM", "27 Jun 2018 07.30 AM",
								ScheduleDate + " " + Time);

						DuplicateSchedule.add(ScheduleDate + " " + Time);

						if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Upcoming")) {
							upcomingcount++;
						} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Overdue")) {
							overduecount++;
						} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Incomplete")) {
							incompletecount++;
						} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status,
								"Ready to Perform")) {
							readyToPerformcount++;

						} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Completed")) {
							completedcount++;

						}
					}
				}
				ActualRowCount = ActualRowCount + rows_table.size();
				ToEntry = ActualRowCount;
				// Click Next button
				if (TotalEntry > 10) {
					WebElement linkElement = driver.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkElement);

				}
				//Thread.sleep(1000);
				//Thread.sleep(3000);
			}
			getDupCount(DuplicateSchedule);
			// Verify that schedules are withing the start date and end date
			Assert.assertEquals(actual, false);
		}
	}

	public static boolean CheckDates(String startDate, String endDate, String CurrentDate) throws Exception {

		SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM yyyy hh.mm a");

		boolean b = false;
		Date startDateToCompare = dfDate.parse(startDate);
		Date endDateToCompare = dfDate.parse(endDate);
		Date d = dfDate.parse(CurrentDate);
		CurrentDate = dfDate.format(d);
		if ((d.after(startDateToCompare) && (d.before(endDateToCompare)))
				|| (CurrentDate.equals(dfDate.format(startDateToCompare))
						|| CurrentDate.equals(dfDate.format(endDateToCompare)))) {
		} else {
			System.err.println("Wrong Schedule is generated...!! Here is the Schedule Info:" + CurrentDate);
			b = true;
		}

		return b;
	}

	public static boolean getDupCount(ArrayList<String> l) {
		Map<String, Integer> counts = new HashMap<String, Integer>();
		String Details = null;
		boolean b = false;
		for (String str : l) {
			if (counts.containsKey(str)) {
				counts.put(str, counts.get(str) + 1);
			} else {
				counts.put(str, 1);
			}
		}
		// Print all the value with its count
		for (Map.Entry<String, Integer> entry : counts.entrySet()) {
			if (entry.getValue() == 2) {
				Details = entry.getKey() + " = " + entry.getValue();
				b = true;
			}
		}
		return b;
	}

	public void selectUpcomingOptionFilterByStatusDrpDwn() throws Exception {
		//		drpdwnHelper.selectDropDownText(filterByDrpDwn, status);
		 btnHelper.click(selectUpcomingStatus);
	}

	public void verifyHourlyAndUpcomingData() throws Exception
	{
		if (driver.findElements(By.xpath("//*[@class='col-md-10 col-sm-10 col-xs-10 alert alert-warning no-records ng-binding']")).size() != 0) {

			System.out.println("No record Found Condition met.");

		} else {
			int upcomingcount = 0, overduecount = 0, incompletecount = 0, completedcount = 0, readyToPerformcount = 0;

			boolean actualupcomingcount = false, actualDuplicate = false;
			String Pagination = textBoxHelper.getText(paginationText);
			String PaginationDrp = "10";
			String[] splited = Pagination.split("\\s+");
			String Time, ScheduleName, AppName, Type, Status, ScheduleDate;
			ArrayList<String> DuplicateSchedule = new ArrayList<String>();
			Arrays.toString(splited);
			// If PaginationDrp is one then there is no PaginationDrp available
			if (PaginationDrp.equalsIgnoreCase("1")) {
				PaginationDrp = splited[5];
			}
			int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
					TotalEntry = Integer.parseInt(splited[5]), PageSizeDrp = Integer.parseInt(PaginationDrp);
			if (TotalEntry <= 10) {
				PageSizeDrp = 1;
			}
			for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {

				WebElement Webtable = gridData;
				; // Replace TableID with Actual Table ID or Xpath
				//Thread.sleep(1000);

				// To locate rows of table.
				List<WebElement> rows_table = Webtable.findElements(By.tagName("tr"));
				// To calculate no of rows In table.
				int rows_count = rows_table.size();

				// Loop will execute till the last row of table.
				for (int row = 1; row < rows_count; row++) {
					// To locate columns(cells) of that specific row.

					List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
					// To calculate no of columns (cells). In that specific row.
					int columns_count = Columns_row.size();
					ScheduleDate = Columns_row.get(0).getText().replaceAll(",", "").replaceAll("\n", " ");
					Time = Columns_row.get(1).getText().replaceAll(":", ".");
					ScheduleName = Columns_row.get(2).getText();
					AppName = Columns_row.get(3).getText();
					Type = Columns_row.get(4).getText();
					Status = Columns_row.get(7).getText();

					if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Upcoming")) {
						upcomingcount++;
						System.err.println("Upcoming" + upcomingcount);
						DuplicateSchedule.add(ScheduleDate + " " + Time + " " + AppName);
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Overdue")) {
						overduecount++;
						actualupcomingcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Incomplete")) {
						incompletecount++;
						actualupcomingcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Ready to Perform")) {
						readyToPerformcount++;
						actualupcomingcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Completed")) {
						completedcount++;
						actualupcomingcount = true;
					}
				}
				ActualRowCount = ActualRowCount + rows_count - 1;
				ToEntry = ActualRowCount;
				if (TotalEntry > 10) {
					WebElement linkElement = driver.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkElement);

				}
				System.err.println("-------------------------------------------------- ");
				//Thread.sleep(500);
			}
			actualDuplicate = getDupCount(DuplicateSchedule);
			// Verify any other type of schedule is not encountered
			Assert.assertEquals(actualupcomingcount, false);
			// Verify Total count of upcoming schedule
			Assert.assertEquals(upcomingcount, TotalEntry);
			// Verify No any duplicate entry of upcoming schedule is exists
			Assert.assertEquals(actualDuplicate, false);
		}
	}

	public void selectOverdueOptionFilterByStatusDrpDwn() throws Exception {
		//		drpdwnHelper.selectDropDownText(filterByDrpDwn, status);
		 btnHelper.click(selectOverdueStatus);
	}

	public void verifyHourlyAndOverdueData() throws Exception
	{
		if (driver.findElements(By.xpath("//*[@class='col-md-10 col-sm-10 col-xs-10 alert alert-warning no-records ng-binding']")).size() != 0)
		{

			System.out.println("No record Found Condition met.");

		} else {
			int upcomingcount = 0, overduecount = 0, incompletecount = 0, completedcount = 0, readyToPerformcount = 0;

			boolean actualOverdueCount = false;
			//Thread.sleep(1000);
			String Pagination =textBoxHelper.getText(paginationText);
			String PaginationDrp = "10";
			String[] splited = Pagination.split("\\s+");
			String Time, ScheduleName, AppName, Type, Status, ScheduleDate;
			Arrays.toString(splited);

			// If PaginationDrp is one then there is no PaginationDrp available
			if (PaginationDrp.equalsIgnoreCase("1")) {
				PaginationDrp = splited[5];
			}
			int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
					TotalEntry = Integer.parseInt(splited[5]), PageSizeDrp = Integer.parseInt(PaginationDrp);
			if (TotalEntry <= 10) {
				PageSizeDrp = 1;
			}
			for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {

				WebElement Webtable = gridData;
				; // Replace TableID with Actual Table ID or Xpath
				//Thread.sleep(1000);

				// To locate rows of table.
				List<WebElement> rows_table = Webtable.findElements(By.tagName("tr"));
				// To calculate no of rows In table.
				int rows_count = rows_table.size();

				// Loop will execute till the last row of table.
				for (int row = 1; row < rows_count; row++) {
					// To locate columns(cells) of that specific row.

					List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
					// To calculate no of columns (cells). In that specific row.
					int columns_count = Columns_row.size();
					ScheduleDate = Columns_row.get(0).getText().replaceAll(",", "").replaceAll("\n", " ");
					Time = Columns_row.get(1).getText().replaceAll(":", ".");
					ScheduleName = Columns_row.get(2).getText();
					AppName = Columns_row.get(3).getText();
					Type = Columns_row.get(4).getText();
					Status = Columns_row.get(7).getText();

					if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Upcoming")) {
						upcomingcount++;
						actualOverdueCount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Overdue")) {
						overduecount++;
						System.err.println("Overdue" + overduecount);
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Incomplete")) {
						incompletecount++;
						actualOverdueCount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Ready to Perform")) {
						readyToPerformcount++;
						actualOverdueCount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Completed")) {
						completedcount++;
						actualOverdueCount = true;
					}
				}
				ActualRowCount = ActualRowCount + rows_count - 1;
				ToEntry = ActualRowCount;
				//Thread.sleep(1000);
				if (TotalEntry > 10) {
					WebElement linkElement = driver
							.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkElement);

				}
				System.err.println("-------------------------------------------------- ");
			}
			// Verify any other type of schedule is not encountered
			Assert.assertEquals(actualOverdueCount, false);
			// Verify Total count of overdue schedule
			Assert.assertEquals(overduecount, TotalEntry);
		}
	}

	public void selectIncompleteOptionFilterByStatusDrpDwn() throws Exception {
		 btnHelper.click(selectIncompleteStatus);
	}

	public void verifyHourlyAndIncompleteData() throws Exception
	{
		if (driver.findElements(By.xpath("//*[@class='col-md-10 col-sm-10 col-xs-10 alert alert-warning no-records ng-binding']")).size() != 0)
		{

			System.out.println("No record Found Condition met.");

		} else {
			int upcomingcount = 0, overduecount = 0, incompletecount = 0, completedcount = 0, readyToPerformcount = 0;

			boolean actualIncompletecount = false, actualDuplicate = false;

			String Pagination =textBoxHelper.getText(paginationText);
			String PaginationDrp = "10";

			String[] splited = Pagination.split("\\s+");

			String Time, ScheduleName, AppName, Type, Status, ScheduleDate;
			ArrayList<String> DuplicateSchedule = new ArrayList<String>();

			Arrays.toString(splited);

			// If PaginationDrp is one then there is no PaginationDrp available
			if (PaginationDrp.equalsIgnoreCase("1")) {
				PaginationDrp = splited[5];
			}
			int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
					TotalEntry = Integer.parseInt(splited[5]), PageSizeDrp = Integer.parseInt(PaginationDrp);

			if (TotalEntry <= 10) {
				PageSizeDrp = 1;
			}
			for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {

				WebElement Webtable = gridData;
				; // Replace TableID with Actual Table ID or Xpath
				//Thread.sleep(1000);

				// To locate rows of table.
				List<WebElement> rows_table = Webtable.findElements(By.tagName("tr"));
				// To calculate no of rows In table.
				int rows_count = rows_table.size();

				// Loop will execute till the last row of table.
				for (int row = 1; row < rows_count; row++) {
					// To locate columns(cells) of that specific row.

					List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
					// To calculate no of columns (cells). In that specific row.
					int columns_count = Columns_row.size();

					ScheduleDate = Columns_row.get(0).getText().replaceAll(",", "").replaceAll("\n", " ");
					Time = Columns_row.get(1).getText().replaceAll(":", ".");
					ScheduleName = Columns_row.get(2).getText();
					AppName = Columns_row.get(3).getText();
					Type = Columns_row.get(4).getText();
					Status = Columns_row.get(7).getText();

					if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Upcoming")) {
						upcomingcount++;
						actualIncompletecount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Overdue")) {
						overduecount++;
						actualIncompletecount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Incomplete")) {
						incompletecount++;
						System.err.println("Incomplete" + incompletecount);
						DuplicateSchedule.add(ScheduleDate + " " + Time + " " + AppName);
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Ready to Perform")) {
						readyToPerformcount++;
						actualIncompletecount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Completed")) {
						completedcount++;
						actualIncompletecount = true;
					}
				}
				ActualRowCount = ActualRowCount + rows_count - 1;
				ToEntry = ActualRowCount;
				if (TotalEntry > 10) {
					WebElement linkElement = driver.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkElement);

				}
				System.err.println("-------------------------------------------------- ");
				//Thread.sleep(1000);

			}
			actualDuplicate = getDupCount(DuplicateSchedule);

			// Verify any other type of schedule is not encountered
			Assert.assertEquals(actualIncompletecount, false);
			// Verify Total count of incomplete schedule
			Assert.assertEquals(incompletecount, TotalEntry);
			// Verify No any duplicate entry of incomplete schedule is exists
			Assert.assertEquals(actualDuplicate, false);
		}
	}

	public void selectReadyToPerformOptionFilterByStatusDrpDwn() throws Exception {
		 btnHelper.click(selectReadyToPerformStatus);
	}

	public void verifyHourlyAndReadyToPerformData() throws Exception
	{
		if (driver.findElements(By.xpath("//*[@class='col-md-10 col-sm-10 col-xs-10 alert alert-warning no-records ng-binding']")).size() != 0)
		{

			System.out.println("No record Found Condition met.");

		} else {

			//Thread.sleep(2000);

			int upcomingcount = 0, overduecount = 0, incompletecount = 0, completedcount = 0, readyToPerformcount = 0;

			boolean actualCompletedcount = false, actualDuplicate = false;
			//Thread.sleep(2000);

			String Pagination =  textBoxHelper.getText(paginationText);
			String PaginationDrp = "10";

			String[] splited = Pagination.split("\\s+");

			String Time, ScheduleName, AppName, Type, Status, ScheduleDate;
			ArrayList<String> DuplicateSchedule = new ArrayList<String>();

			Arrays.toString(splited);

			// If PaginationDrp is one then there is no PaginationDrp available
			if (PaginationDrp.equalsIgnoreCase("1")) {
				PaginationDrp = splited[5];
			}
			int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
					TotalEntry = Integer.parseInt(splited[5]), PageSizeDrp = Integer.parseInt(PaginationDrp);

			if (TotalEntry <= 10) {
				PageSizeDrp = 1;
			}

			for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {

				WebElement Webtable = gridData;
				; // Replace TableID with Actual Table ID or Xpath
				//Thread.sleep(1000);

				// To locate rows of table.
				List<WebElement> rows_table = Webtable.findElements(By.tagName("tr"));
				// To calculate no of rows In table.
				int rows_count = rows_table.size();

				// Loop will execute till the last row of table.
				for (int row = 1; row < rows_count; row++) {
					// To locate columns(cells) of that specific row.

					List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
					// To calculate no of columns (cells). In that specific row.
					int columns_count = Columns_row.size();

					ScheduleDate = Columns_row.get(0).getText().replaceAll(",", "").replaceAll("\n", " ");
					Time = Columns_row.get(1).getText().replaceAll(":", ".");
					ScheduleName = Columns_row.get(2).getText();
					AppName = Columns_row.get(3).getText();
					Type = Columns_row.get(4).getText();
					Status = Columns_row.get(7).getText();

					if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Upcoming")) {
						upcomingcount++;
						actualCompletedcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Overdue")) {
						overduecount++;
						actualCompletedcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Incomplete")) {
						incompletecount++;
						actualCompletedcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Ready to Perform")) {
						readyToPerformcount++;
						System.err.println("readyToPerformcount" + readyToPerformcount);
						DuplicateSchedule.add(ScheduleDate + " " + Time + " " + AppName);
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Completed")) {
						completedcount++;
						actualCompletedcount = true;
					}
				}
				ActualRowCount = ActualRowCount + rows_count - 1;
				ToEntry = ActualRowCount;
				if (TotalEntry > 10) {
					WebElement linkElement = driver.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkElement);

				}
				System.err.println("-------------------------------------------------- ");
				//Thread.sleep(1000);

			}
			actualDuplicate = getDupCount(DuplicateSchedule);

			// Verify any other type of schedule is not encountered
			Assert.assertEquals(actualCompletedcount, false);
			// Verify Total count of readyToPerformcount schedule
			Assert.assertEquals(readyToPerformcount, TotalEntry);
			// Verify No any duplicate entry of readyToPerformcount schedule is exists
			Assert.assertEquals(actualDuplicate, false);
		}
	}

	public void clickSearchByDrpDwn() throws Exception {
		 btnHelper.click(searchByDrpDwn);
	}

	public void selectMasterAppsFromSearchByDrpDwn() throws Exception {
		drpdwnHelper.selectDropDownText(searchByDrpDwn, "Master apps");
	}

	public void verifyDisplayUpcomingMasterAppData() throws Exception
	{
		if (driver.findElements(By.xpath("//*[@class='col-md-10 col-sm-10 col-xs-10 alert alert-warning no-records ng-binding']")).size() != 0)
		{
			System.out.println("No record Found Condition met.");
		} else {
			int upcomingcount = 0, overduecount = 0, incompletecount = 0, completedcount = 0, readyToPerformcount = 0;
			boolean actualupcomingcount = false, actualDuplicate = false;
			String Pagination = textBoxHelper.getText(paginationText);
			String PaginationDrp = "10";
			String[] splited = Pagination.split("\\s+");
			String Time, ScheduleName, AppName, Type, Status, ScheduleDate;
			ArrayList<String> DuplicateSchedule = new ArrayList<String>();
			Arrays.toString(splited);
			// If PaginationDrp is one then there is no PaginationDrp available
			if (PaginationDrp.equalsIgnoreCase("1")) {
				PaginationDrp = splited[5];
			}
			int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
					TotalEntry = Integer.parseInt(splited[5]), PageSizeDrp = Integer.parseInt(PaginationDrp);

			if (TotalEntry <= 10) {
				PageSizeDrp = 1;
			}
			for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {

				WebElement Webtable = gridData;
				; // Replace TableID with Actual Table ID or Xpath
				//Thread.sleep(1000);
				// To locate rows of table.
				List<WebElement> rows_table = Webtable.findElements(By.tagName("tr"));
				// To calculate no of rows In table.
				int rows_count = rows_table.size();
				// Loop will execute till the last row of table.
				for (int row = 1; row < rows_count; row++) {
					// To locate columns(cells) of that specific row.

					List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
					// To calculate no of columns (cells). In that specific row.
					int columns_count = Columns_row.size();

					ScheduleDate = Columns_row.get(0).getText().replaceAll(",", "").replaceAll("\n", " ");
					Time = Columns_row.get(1).getText().replaceAll(":", ".");
					ScheduleName = Columns_row.get(2).getText();
					AppName = Columns_row.get(3).getText();
					Type = Columns_row.get(4).getText();
					Status = Columns_row.get(7).getText();

					if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Upcoming")) {
						upcomingcount++;
						System.err.println("Upcoming" + upcomingcount);
						DuplicateSchedule.add(ScheduleDate + " " + Time + " " + AppName);
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Overdue")) {
						overduecount++;
						actualupcomingcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Incomplete")) {
						incompletecount++;
						actualupcomingcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Ready to Perform")) {
						readyToPerformcount++;
						actualupcomingcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Completed")) {
						completedcount++;
						actualupcomingcount = true;
					}
				}
				ActualRowCount = ActualRowCount + rows_count - 1;
				ToEntry = ActualRowCount;

				if (TotalEntry > 10) {
					WebElement linkElement = driver.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkElement);

				}
				System.err.println("-------------------------------------------------- ");
				//Thread.sleep(1000);

			}
			actualDuplicate = getDupCount(DuplicateSchedule);
			// Verify any other type of schedule is not encountered
			Assert.assertEquals(actualupcomingcount, false);
			// Verify Total count of upcoming schedule
			Assert.assertEquals(upcomingcount, TotalEntry);
			// Verify No any duplicate entry of upcoming schedule is exists
			Assert.assertEquals(actualDuplicate, false);
		}
	}
		
	public void verifyDisplayOverdueMasterAppData() throws Exception
	{
		if (driver.findElements(By.xpath("//*[@class='col-md-10 col-sm-10 col-xs-10 alert alert-warning no-records ng-binding']")).size() != 0)
		{
			System.out.println("No record Found Condition met.");
		} else {
			int upcomingcount = 0, overduecount = 0, incompletecount = 0, completedcount = 0, readyToPerformcount = 0;
			boolean actualOverdueCount = false;	
			String Pagination = textBoxHelper.getText(paginationText);
			String PaginationDrp = "10";
			String[] splited = Pagination.split("\\s+");
			String Time, ScheduleName, AppName, Type, Status, ScheduleDate;
			Arrays.toString(splited);
			// If PaginationDrp is one then there is no PaginationDrp available
			if (PaginationDrp.equalsIgnoreCase("1")) {
				PaginationDrp = splited[5];
			}
			int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
					TotalEntry = Integer.parseInt(splited[5]), PageSizeDrp = Integer.parseInt(PaginationDrp);
			if (TotalEntry <= 10) {
				PageSizeDrp = 1;
			}
			for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {

				WebElement Webtable = gridData;
				List<WebElement> rows_table = Webtable.findElements(By.tagName("tr"));
				// To calculate no of rows In table.
				int rows_count = rows_table.size();

				// Loop will execute till the last row of table.
				for (int row = 1; row < rows_count; row++) {
					// To locate columns(cells) of that specific row.

					List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
					// To calculate no of columns (cells). In that specific row.
					int columns_count = Columns_row.size();

					ScheduleDate = Columns_row.get(0).getText().replaceAll(",", "").replaceAll("\n", " ");
					Time = Columns_row.get(1).getText().replaceAll(":", ".");
					ScheduleName = Columns_row.get(2).getText();
					AppName = Columns_row.get(3).getText();
					Type = Columns_row.get(4).getText();
					Status = Columns_row.get(7).getText();

					if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Upcoming")) {
						upcomingcount++;
						actualOverdueCount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Overdue")) {
						overduecount++;
						System.err.println("Overdue" + overduecount);
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Incomplete")) {
						incompletecount++;
						actualOverdueCount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Ready to Perform")) {
						readyToPerformcount++;
						actualOverdueCount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Completed")) {
						completedcount++;
						actualOverdueCount = true;
					}
				}
				ActualRowCount = ActualRowCount + rows_count - 1;
				ToEntry = ActualRowCount;
				if (TotalEntry > 10) {
					WebElement linkElement = driver.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkElement);

				}
				System.err.println("-------------------------------------------------- ");
				//Thread.sleep(1000);
			}
			// Verify any other type of schedule is not encountered
			Assert.assertEquals(actualOverdueCount, false);
			// Verify Total count of overdue schedule
			Assert.assertEquals(overduecount, TotalEntry);
		}
	}
	
	public void verifyDisplayIncompleteMasterAppData() throws Exception
	{
		if (driver.findElements(By.xpath("//*[@class='col-md-10 col-sm-10 col-xs-10 alert alert-warning no-records ng-binding']")).size() != 0)
		{
			System.out.println("No record Found Condition met.");
		} else {
			int upcomingcount = 0, overduecount = 0, incompletecount = 0, completedcount = 0, readyToPerformcount = 0;
			boolean actualIncompletecount = false, actualDuplicate = false;
			String Pagination = textBoxHelper.getText(paginationText);
			String PaginationDrp = "10";
			String[] splited = Pagination.split("\\s+");
			String Time, ScheduleName, AppName, Type, Status, ScheduleDate;
			ArrayList<String> DuplicateSchedule = new ArrayList<String>();
			Arrays.toString(splited);
			// If PaginationDrp is one then there is no PaginationDrp available
			if (PaginationDrp.equalsIgnoreCase("1")) {
				PaginationDrp = splited[5];
			}
			int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
					TotalEntry = Integer.parseInt(splited[5]), PageSizeDrp = Integer.parseInt(PaginationDrp);
			if (TotalEntry <= 10) {
				PageSizeDrp = 1;
			}
			for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {

				WebElement Webtable = gridData;
				; // Replace TableID with Actual Table ID or Xpath
				//Thread.sleep(1000);
				// To locate rows of table.
				List<WebElement> rows_table = Webtable.findElements(By.tagName("tr"));
				// To calculate no of rows In table.
				int rows_count = rows_table.size();
				// Loop will execute till the last row of table.
				for (int row = 1; row < rows_count; row++) {
					// To locate columns(cells) of that specific row.
					List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
					// To calculate no of columns (cells). In that specific row.
					int columns_count = Columns_row.size();
					ScheduleDate = Columns_row.get(0).getText().replaceAll(",", "").replaceAll("\n", " ");
					Time = Columns_row.get(1).getText().replaceAll(":", ".");
					ScheduleName = Columns_row.get(2).getText();
					AppName = Columns_row.get(3).getText();
					Type = Columns_row.get(4).getText();
					Status = Columns_row.get(7).getText();
					if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Upcoming")) {
						upcomingcount++;
						actualIncompletecount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Overdue")) {
						overduecount++;
						actualIncompletecount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Incomplete")) {
						incompletecount++;
						System.err.println("Incomplete" + incompletecount);
						DuplicateSchedule.add(ScheduleDate + " " + Time + " " + AppName);
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Ready to Perform")) {
						readyToPerformcount++;
						actualIncompletecount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Completed")) {
						completedcount++;
						actualIncompletecount = true;
					}
				}
				ActualRowCount = ActualRowCount + rows_count - 1;
				ToEntry = ActualRowCount;
				if (TotalEntry > 10) {
					WebElement linkElement = driver.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkElement);

				}
				System.err.println("-------------------------------------------------- ");
			}
			actualDuplicate = getDupCount(DuplicateSchedule);
			// Verify any other type of schedule is not encountered
			Assert.assertEquals(actualIncompletecount, false);
			// Verify Total count of incomplete schedule
			Assert.assertEquals(incompletecount, TotalEntry);
			// Verify No any duplicate entry of incomplete schedule is exists
			Assert.assertEquals(actualDuplicate, false);
		}
		
	}
	
	public void verifyDisplayCompleteMasterAppData() throws Exception
	{
		if (driver.findElements(By.xpath("//*[@class='col-md-10 col-sm-10 col-xs-10 alert alert-warning no-records ng-binding']")).size() != 0)
		{
			System.out.println("No record Found Condition met.");
		}  else {
			int upcomingcount = 0, overduecount = 0, incompletecount = 0, completedcount = 0, readyToPerformcount = 0;
			boolean actualCompletedcount = false, actualDuplicate = false;
			String Pagination = textBoxHelper.getText(paginationText);
			String PaginationDrp = "10";
			String[] splited = Pagination.split("\\s+");
			String Time, ScheduleName, AppName, Type, Status, ScheduleDate;
			ArrayList<String> DuplicateSchedule = new ArrayList<String>();
			Arrays.toString(splited);
			// If PaginationDrp is one then there is no PaginationDrp available
			if (PaginationDrp.equalsIgnoreCase("1")) {
				PaginationDrp = splited[5];
			}
			int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
					TotalEntry = Integer.parseInt(splited[5]), PageSizeDrp = Integer.parseInt(PaginationDrp);
			if (TotalEntry <= 10) {
				PageSizeDrp = 1;
			}
			for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {
				WebElement Webtable = gridData;
				List<WebElement> rows_table = Webtable.findElements(By.tagName("tr"));
				// To calculate no of rows In table.
				int rows_count = rows_table.size();
				// Loop will execute till the last row of table.
				for (int row = 1; row < rows_count; row++) {
					// To locate columns(cells) of that specific row.
					List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
					// To calculate no of columns (cells). In that specific row.
					int columns_count = Columns_row.size();
					ScheduleDate = Columns_row.get(0).getText().replaceAll(",", "").replaceAll("\n", " ");
					Time = Columns_row.get(1).getText().replaceAll(":", ".");
					ScheduleName = Columns_row.get(2).getText();
					AppName = Columns_row.get(3).getText();
					Type = Columns_row.get(4).getText();
					Status = Columns_row.get(7).getText();

					if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Upcoming")) {
						upcomingcount++;
						actualCompletedcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Overdue")) {
						overduecount++;
						actualCompletedcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Incomplete")) {
						incompletecount++;
						actualCompletedcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Ready to Perform")) {
						readyToPerformcount++;
						actualCompletedcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Completed")) {
						completedcount++;
						System.err.println("Complete" + completedcount);
						DuplicateSchedule.add(ScheduleDate + " " + Time + " " + AppName);
					}
				}
				ActualRowCount = ActualRowCount + rows_count - 1;
				ToEntry = ActualRowCount;
				if (TotalEntry > 10) {
					WebElement linkElement = driver.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkElement);

				}
				System.err.println("-------------------------------------------------- ");
			}
			actualDuplicate = getDupCount(DuplicateSchedule);
			// Verify any other type of schedule is not encountered
			Assert.assertEquals(actualCompletedcount, false);
			// Verify Total count of completedcount schedule
			Assert.assertEquals(completedcount, TotalEntry);
			// Verify No any duplicate entry of completedcount schedule is exists
			Assert.assertEquals(actualDuplicate, false);
		}
		
	}
	
	public void verifyDisplayReadyToPerformMasterAppData() throws Exception
	{
		if (driver.findElements(By.xpath("//*[@class='col-md-10 col-sm-10 col-xs-10 alert alert-warning no-records ng-binding']")).size() != 0)
		{
			System.out.println("No record Found Condition met.");
		}  else {
			int upcomingcount = 0, overduecount = 0, incompletecount = 0, completedcount = 0, readyToPerformcount = 0;
			boolean actualCompletedcount = false, actualDuplicate = false;
			String Pagination = textBoxHelper.getText(paginationText);
			String PaginationDrp = "10";
			String[] splited = Pagination.split("\\s+");
			String Time, ScheduleName, AppName, Type, Status, ScheduleDate;
			ArrayList<String> DuplicateSchedule = new ArrayList<String>();
			Arrays.toString(splited);
			// If PaginationDrp is one then there is no PaginationDrp available
			if (PaginationDrp.equalsIgnoreCase("1")) {
				PaginationDrp = splited[5];
			}
			int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
					TotalEntry = Integer.parseInt(splited[5]), PageSizeDrp = Integer.parseInt(PaginationDrp);
			if (TotalEntry <= 10) {
				PageSizeDrp = 1;
			}

			for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {
				WebElement Webtable = gridData;
				; // Replace TableID with Actual Table ID or Xpath
				//Thread.sleep(1000);

				// To locate rows of table.
				List<WebElement> rows_table = Webtable.findElements(By.tagName("tr"));
				// To calculate no of rows In table.
				int rows_count = rows_table.size();

				// Loop will execute till the last row of table.
				for (int row = 1; row < rows_count; row++) {
					// To locate columns(cells) of that specific row.

					List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
					// To calculate no of columns (cells). In that specific row.
					int columns_count = Columns_row.size();

					ScheduleDate = Columns_row.get(0).getText().replaceAll(",", "").replaceAll("\n", " ");
					Time = Columns_row.get(1).getText().replaceAll(":", ".");
					ScheduleName = Columns_row.get(2).getText();
					AppName = Columns_row.get(3).getText();
					Type = Columns_row.get(4).getText();
					Status = Columns_row.get(7).getText();

					if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Upcoming")) {
						upcomingcount++;
						actualCompletedcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Overdue")) {
						overduecount++;
						actualCompletedcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Incomplete")) {
						incompletecount++;
						actualCompletedcount = true;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Ready to Perform")) {
						readyToPerformcount++;
						System.err.println("readyToPerformcount" + readyToPerformcount);
						DuplicateSchedule.add(ScheduleDate + " " + Time + " " + AppName);
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Completed")) {
						completedcount++;
						actualCompletedcount = true;
					}
				}
				ActualRowCount = ActualRowCount + rows_count - 1;
				ToEntry = ActualRowCount;
				if (TotalEntry > 10) {
					WebElement linkElement = driver.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkElement);
				}
				System.err.println("-------------------------------------------------- ");
			}
			actualDuplicate = getDupCount(DuplicateSchedule);
			// Verify any other type of schedule is not encountered
			Assert.assertEquals(actualCompletedcount, false);
			// Verify Total count of readyToPerformcount schedule
			Assert.assertEquals(readyToPerformcount, TotalEntry);
			// Verify No any duplicate entry of readyToPerformcount schedule is exists
			Assert.assertEquals(actualDuplicate, false);
		}		
	}
	
	public void clickCollapseLink() throws Exception {
		//Thread.sleep(1000);
		 btnHelper.click(collapseExpandLink);
		//Thread.sleep(2000);
	}
	
	public void clickExpandLink() throws Exception {
		 btnHelper.click(collapseExpandLink);
		//Thread.sleep(2000);
	}
	
	public void verifyExpandFunctionality() throws Exception {
		boolean actual =  genricHelper.isDisplayed(dateDrpDwn);
		Assert.assertEquals(actual, true);
	}
	
	public void verifyCollapseFunctionality() throws Exception {
		boolean actual;
		if(driver.findElements(By.xpath("//select[@ng-model='vm.caledardetails.dateRangebydrop']")).size() != 0)
		{
			actual = false;
		}
		else
		{
			actual = true;
		}
		Assert.assertEquals(actual, true);
	}
	
	public void selectThisWeekOption() throws Exception {
		drpdwnHelper.selectDropDownText(dateDrpDwn, "This Week");
	}
	
	public void selectThisMonthOption() throws Exception {
		drpdwnHelper.selectDropDownText(dateDrpDwn, "This Month");
	}
	
	public void selectThisYearOption() throws Exception {
		drpdwnHelper.selectDropDownText(dateDrpDwn, "This Year");
	}
	
	public static String ThisYearStartDate(WebDriver driver) {
		Calendar c = Calendar.getInstance();
		c.getTime(); // ------>
		c.set(Calendar.DAY_OF_YEAR, c.getActualMinimum(Calendar.DAY_OF_YEAR));
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh.mm a");
		String StartDateWithCurrentTime = sdf.format(c.getTime());
		String[] splited = StartDateWithCurrentTime.split("\\s+");
		String StartDate = splited[0] + " " + splited[1] + " " + splited[2] + " " + "12.00 AM";
		return StartDate;
	}
	
	public void verifyThisWeekData() throws Exception {
		if (driver.findElements(By.xpath("//*[@class='col-md-10 col-sm-10 col-xs-10 alert alert-warning no-records ng-binding']")).size() != 0)
		{
			System.out.println("No record Found Condition met.");
		}  else {
			int upcomingcount = 0, overduecount = 0, incompletecount = 0, completedcount = 0, readyToPerformcount = 0;
			boolean actual = false;
			String Pagination = textBoxHelper.getText(paginationText);
			String PaginationDrp = "10";
			String[] splited = Pagination.split("\\s+");
			String Time, ScheduleName, AppName, Type, Status, ScheduleDate;
			ArrayList<String> DuplicateSchedule = new ArrayList<String>();
			Arrays.toString(splited);
			int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
					TotalEntry = Integer.parseInt(splited[5]), PageSizeDrp = Integer.parseInt(PaginationDrp);
			if (TotalEntry <= 10) {
				PageSizeDrp = 1;
			}

			for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {

				WebElement Webtable = gridData;
				; // Replace TableID with Actual Table ID or Xpath
				//Thread.sleep(1000);

				// To locate rows of table.
				List<WebElement> rows_table = Webtable.findElements(By.tagName("tr"));
				// To calculate no of rows In table.
				int rows_count = rows_table.size();

				// Loop will execute till the last row of table.
				for (int row = 1; row < rows_count; row++) {
					// To locate columns(cells) of that specific row.

					List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
					// To calculate no of columns (cells). In that specific row.
					int columns_count = Columns_row.size();

					ScheduleDate = Columns_row.get(0).getText().replaceAll(",", "").replaceAll("\n", " ");
					Time = Columns_row.get(1).getText().replaceAll(":", ".");
					ScheduleName = Columns_row.get(2).getText();
					AppName = Columns_row.get(3).getText();
					Type = Columns_row.get(4).getText();
					Status = Columns_row.get(7).getText();
					
					actual = CheckDates(ThisWeekStartDate(driver),
							ThisWeekEndDate(driver), ScheduleDate + " " + Time);

					DuplicateSchedule.add(ScheduleDate + " " + Time);
					if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Upcoming")) {
						upcomingcount++;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Overdue")) {
						overduecount++;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Incomplete")) {
						incompletecount++;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Ready to Perform")) {
						readyToPerformcount++;

					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Completed")) {
						completedcount++;
					}
				}
				ActualRowCount = ActualRowCount + rows_count - 1;
				ToEntry = ActualRowCount;
				// Click Next button
				if (TotalEntry > 10) {
					WebElement linkElement = driver.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkElement);

				}
				System.err.println("-------------------------------------------------- ");
			}
			getDupCount(DuplicateSchedule);
			// Verify that schedules are withing the start date and end date
			Assert.assertEquals(actual, true);
			//Thread.sleep(3000);
		}
	}
	
	public static String ThisWeekEndDate(WebDriver driver) {
		Calendar c = Calendar.getInstance();
		// ensure the method works within current month
		c.set(Calendar.DAY_OF_MONTH, Calendar.SATURDAY + 1);
		Date date = c.getTime();
		SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM yyyy hh.mm a");
		String EndDateWithCurrentTime = dfDate.format(date);
		String[] splited = EndDateWithCurrentTime.split("\\s+");
		String EndDate = splited[0] + " " + splited[1] + " " + splited[2] + " " + "12.00 AM";
		return EndDate;
	}
	
	public static String ThisWeekStartDate(WebDriver driver) {
		Calendar c = Calendar.getInstance();
		// ensure the method works within current month
		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		Date date = c.getTime();
		SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM yyyy hh.mm a");
		String StartDateWithCurrentTime = dfDate.format(date);
		String[] splited = StartDateWithCurrentTime.split("\\s+");
		String StartDate = splited[0] + " " + splited[1] + " " + splited[2] + " " + "12.00 AM";
		return StartDate;

	}
	
	public void verifyThisMonthData() throws Exception {
		if (driver.findElements(By.xpath("//*[@class='col-md-10 col-sm-10 col-xs-10 alert alert-warning no-records ng-binding']")).size() != 0)
		{
			System.out.println("No record Found Condition met.");
		}  else {
			int upcomingcount = 0, overduecount = 0, incompletecount = 0, completedcount = 0, readyToPerformcount = 0;
			boolean ActualEntries = false;
			String Pagination = textBoxHelper.getText(paginationText);
			String PaginationDrp = "10";
			String[] splited = Pagination.split("\\s+");
			String Time, ScheduleName, AppName, Type, Status, ScheduleDate;
			ArrayList<String> DuplicateSchedule = new ArrayList<String>();
			Arrays.toString(splited);
			int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
					TotalEntry = Integer.parseInt(splited[5]), PageSizeDrp = Integer.parseInt(PaginationDrp);

			if (TotalEntry <= 10) {
				PageSizeDrp = 1;
			}

			for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {
				WebElement Webtable = gridData;
				List<WebElement> rows_table = Webtable.findElements(By.tagName("tr"));
				// To calculate no of rows In table.
				int rows_count = rows_table.size();
				// Loop will execute till the last row of table.
				for (int row = 1; row < rows_count; row++) {
					// To locate columns(cells) of that specific row.
					List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
					// To calculate no of columns (cells). In that specific row.
					int columns_count = Columns_row.size();
					ScheduleDate = Columns_row.get(0).getText().replaceAll(",", "").replaceAll("\n", " ");
					Time = Columns_row.get(1).getText().replaceAll(":", ".");
					ScheduleName = Columns_row.get(2).getText();
					AppName = Columns_row.get(3).getText();
					Type = Columns_row.get(4).getText();
					Status = Columns_row.get(7).getText();
					ActualEntries = CheckDates(ThisMonthStartDate(driver),
							ThisMonthEndDate(driver), ScheduleDate + " " + Time);

					DuplicateSchedule.add(ScheduleDate + " " + Time);

					if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Upcoming")) {
						upcomingcount++;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Overdue")) {
						overduecount++;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Incomplete")) {
						incompletecount++;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Ready to Perform")) {
						readyToPerformcount++;

					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Completed")) {
						completedcount++;
					}
				}
				ActualRowCount = ActualRowCount + rows_count - 1;
				ToEntry = ActualRowCount;
				// Click Next button
				if (TotalEntry > 10) {
					WebElement linkElement = driver.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkElement);

				}
				System.err.println("-------------------------------------------------- ");
			}
			getDupCount(DuplicateSchedule);
			// Verify that schedules are withing the start date and end date
			Assert.assertEquals(ActualEntries, true);
			//Thread.sleep(3000);
		}
	}
	
	public static String ThisMonthEndDate(WebDriver driver) {
		Calendar c = Calendar.getInstance();
		c.getTime(); // ------>
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH) + 1);
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh.mm a");
		String EndDateWithCurrentTime = sdf.format(c.getTime());
		String[] splited = EndDateWithCurrentTime.split("\\s+");
		String EndDate = splited[0] + " " + splited[1] + " " + splited[2] + " " + "12.00 AM";
		return EndDate;
	}
	
	public static String ThisMonthStartDate(WebDriver driver) {
		Calendar c = Calendar.getInstance();
		c.getTime(); // ------>
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh.mm a");
		String StartDateWithCurrentTime = sdf.format(c.getTime());
		String[] splited = StartDateWithCurrentTime.split("\\s+");
		String StartDate = splited[0] + " " + splited[1] + " " + splited[2] + " " + "12.00 AM";
		return StartDate;
	}
	
	public void verifyThisYearData() throws Exception {
		if (driver.findElements(By.xpath("//*[@class='col-md-10 col-sm-10 col-xs-10 alert alert-warning no-records ng-binding']")).size() != 0)
		{
			System.out.println("No record Found Condition met.");
		}  else {
			int upcomingcount = 0, overduecount = 0, incompletecount = 0, completedcount = 0, readyToPerformcount = 0;
			boolean ActualEntries = false;
			String Pagination = textBoxHelper.getText(paginationText);
			String PaginationDrp = "10";
			String[] splited = Pagination.split("\\s+");
			String Time, ScheduleName, AppName, Type, Status, ScheduleDate;
			ArrayList<String> DuplicateSchedule = new ArrayList<String>();
			Arrays.toString(splited);
			int ActualRowCount = 0, FromEntry = Integer.parseInt(splited[1]), ToEntry = Integer.parseInt(splited[3]),
					TotalEntry = Integer.parseInt(splited[5]), PageSizeDrp = Integer.parseInt(PaginationDrp);

			if (TotalEntry <= 10) {
				PageSizeDrp = 1;
			}

			for (FromEntry = 1; ActualRowCount < TotalEntry; FromEntry = FromEntry + PageSizeDrp) {

				WebElement Webtable = gridData;
				; // Replace TableID with Actual Table ID or Xpath
				//Thread.sleep(1000);

				// To locate rows of table.
				List<WebElement> rows_table = Webtable.findElements(By.tagName("tr"));
				// To calculate no of rows In table.
				int rows_count = rows_table.size();

				// Loop will execute till the last row of table.
				for (int row = 1; row < rows_count; row++) {
					// To locate columns(cells) of that specific row.

					List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
					// To calculate no of columns (cells). In that specific row.
					int columns_count = Columns_row.size();
					ScheduleDate = Columns_row.get(0).getText().replaceAll(",", "").replaceAll("\n", " ");
					Time = Columns_row.get(1).getText().replaceAll(":", ".");
					ScheduleName = Columns_row.get(2).getText();
					AppName = Columns_row.get(3).getText();
					Type = Columns_row.get(4).getText();
					Status = Columns_row.get(7).getText();
					ActualEntries = CheckDates(ThisYearStartDate(driver),
							ThisYearEndDate(driver), ScheduleDate + " " + Time);
					DuplicateSchedule.add(ScheduleDate + " " + Time);
					if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Upcoming")) {
						upcomingcount++;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Overdue")) {
						overduecount++;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Incomplete")) {
						incompletecount++;
					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Ready to Perform")) {
						readyToPerformcount++;

					} else if (org.apache.commons.lang3.StringUtils.containsIgnoreCase(Status, "Completed")) {
						completedcount++;
					}
				}
				ActualRowCount = ActualRowCount + rows_count - 1;
				ToEntry = ActualRowCount;
				// Click Next button
				if (TotalEntry > 10) {
					WebElement linkElement = driver.findElement(By.xpath("//*[@ng-click='selectPage(page + 1, $event)']"));
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", linkElement);
				}
				System.err.println("-------------------------------------------------- ");
			}
			getDupCount(DuplicateSchedule);
			// Verify that schedules are withing the start date and end date
			Assert.assertEquals(ActualEntries, false);
		}	
	}
	
	public static String ThisYearEndDate(WebDriver driver) {
		Calendar c = Calendar.getInstance();
		c.getTime(); // ------>
		c.set(Calendar.DAY_OF_YEAR, c.getActualMaximum(Calendar.DAY_OF_YEAR) + 1);
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh.mm a");
		String EndDateWithCurrentTime = sdf.format(c.getTime());
		String[] splited = EndDateWithCurrentTime.split("\\s+");
		String EndDate = splited[0] + " " + splited[1] + " " + splited[2] + " " + "12.00 AM";
		return EndDate;
	}
	
	 public static String firstAppLinkApp;
	
	public String selectFirstAppLink() throws Exception {
		 firstAppLinkApp =  textBoxHelper.getText(firstAppLink);
		System.err.println(firstAppLinkApp);
		 btnHelper.click(firstAppLink);
		return firstAppLinkApp;
	}
	
	public void verifyAppName() throws Exception {
		//Thread.sleep(3000);
		String actual =  textBoxHelper.getText(appName);
		System.err.println(firstAppLinkApp);
		String expected = firstAppLinkApp;
		System.out.println("Expected App Name: "+ expected);
		System.err.println("Actual App Name: "+actual);
		Assert.assertEquals(actual, expected);
		
	}
	

























}// end
