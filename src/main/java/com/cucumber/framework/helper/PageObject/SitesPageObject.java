package com.cucumber.framework.helper.PageObject;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Javascript.JavaScriptHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;

public class SitesPageObject extends PageBase {
	private WebDriver driver;
	private Helper helper = new Helper();
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(SitesPageObject.class);
	private Search sObj;
	private WaitHelper waitObj;
	private DropDownHelper drpHelper;
	private TextBoxHelper textboxHelper;
	public SitesPageObject(WebDriver driver) 
	{
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		sObj = new Search();
		waitObj = new WaitHelper(ObjectRepo.driver, ObjectRepo.reader);
		drpHelper = new DropDownHelper(ObjectRepo.driver);
		textboxHelper =new TextBoxHelper(ObjectRepo.driver);

	}

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.admin.home']")
	public WebElement administrationTile;

	@FindBy(how = How.XPATH, using = "//div[2]/div/a")
	public WebElement sitesTile;
	@FindBy(how = How.XPATH, using = "//breadcrumb/div/div/span[1]")
	public WebElement verifySitesTile;
	@FindBy(how = How.XPATH, using = "//div/div[1]/ul/li[2]/a")
	public WebElement siteLevelNameTab;
	@FindBy(how = How.XPATH, using = "//div/div[3]/ui-view/div/p")
	public WebElement verifySiteLevelNameTab;
	@FindBy(how = How.XPATH, using = "//*[@id='txtSearch']")
	public WebElement searchbox;

	@FindBy(how = How.XPATH, using = "//*[@id='shareapp-remove-search-text']")
	public WebElement removeSearchboxBtn;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.setTab(1)']")
	public WebElement siteListingBtn;
	@FindBy(how = How.XPATH, using = "//table/tbody/tr[1]/td[1]/span[1]/span/a")
	public WebElement firstSiteofSiteListing;
	@FindBy(how = How.XPATH, using = "//breadcrumb/div/div/span[1]")
	public WebElement addEditSitesofSiteListing;
	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.addSitelevel()']")
	public WebElement addnewofSitenameLevel;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save(vm.sitelevel,editSiteLevelForm)']")
	public WebElement savebtnofnamelevelSite;
	@FindBy(how = How.XPATH, using = "//div[2]/a")
	public WebElement cancelbtnofnamelevelSite;

	@FindBy(how = How.XPATH, using = "//div[3]/grid/div[2]/div/table/tbody/tr[1]/td[1]/span[1]/span/a")
	public WebElement firstsiteofNameLevelSite;
	@FindBy(how = How.XPATH, using = "//div[2]/a")
	public WebElement cancelBtnfirstsiteofNameLevelSite;

	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[3]/span")
	public WebElement redirectedNameLevelSite;
	@FindBy(how = How.XPATH, using = "//tr[1]//*[contains(text(),'View Audit Trail')]")
	public WebElement ViewAuditoflistingscreen;
	@FindBy(how = How.XPATH, using = "/html/body/div/div/div/div[1]/a")
	public WebElement verifyCancelBtnColorViewAuditoflistingscreen;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[3]/span")
	public WebElement redirectedSiteListinScreen;
	@FindBy(how = How.XPATH, using = "//tr[1]/td[3]/span[1]/span/a")
	public WebElement viewAuditofSiteleveName;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[4]/span")
	public WebElement viewAuditscreenofSiteleveName;

	@FindBy(how = How.XPATH, using = "//div/div/div[1]/a")
	public WebElement verifyCancelBtnColorViewAuditSiteLevelNameScreen;
 
	
	@FindBy(how = How.XPATH, using = "//grid/div[3]/div")
	public WebElement verifysearchboxSiteListingScreen;

	@FindBy(how = How.XPATH, using = "//li[7]/a")
	public WebElement lastPageOfPaginationSiteListing;
	@FindBy(how = How.XPATH, using = "//div[3]/grid/div[2]/div/table/tbody/tr[1]")
	public WebElement countnumberEnriesinNamelevelSite;

	@FindBy(how = How.XPATH, using = "//div[3]/grid/div[3]/div")
	public WebElement verifyNumberEnriesinNamelevelSite;

	@FindBy(how = How.XPATH, using = "//div[3]/grid/div[2]/div/table/thead/tr/th[1]")
	public WebElement sortingIconNamelevelSite;

	@FindBy(how = How.XPATH, using = "//tr[1]/td[7]/span[1]/span/a")
	public WebElement AuditTrailSiteListing;

	@FindBy(how = How.XPATH, using = "//grid/div[3]/div[1]")
	public WebElement totalentriesSiteListing;

	@FindBy(how = How.XPATH, using = "//div[3]/div/label")
	public WebElement Selectedpagesize;
	
	 

	/* -------------------------- |----------------| -------------------------- */
	/* -------------------------- | Public Methods | -------------------------- */
	/* -------------------------- |----------------| -------------------------- */

	public void clicktoAdministrationTile() throws Exception {
		log.info(administrationTile);
		btnHelper.click(administrationTile);
	}

	public void ClickonSitesTile() {
		btnHelper.click(sitesTile);
	}

	public void verifySitesTile() 
	{
		Assert.assertEquals(textboxHelper.getText(verifySitesTile), ObjectRepo.reader.getVerifySitesTile());
	}
	public void clickonSiteLevelName()
	{
		btnHelper.click(siteLevelNameTab);
	}

	public void verifySiteLevelname() {
		Assert.assertEquals(textboxHelper.getText(verifySiteLevelNameTab),ObjectRepo.reader.getVerifySiteLevelNameTab());
	}

	public void enterSiteNameInSearchbox(String value) {
		textboxHelper.sendKeys(searchbox, value);
	}

	public void clickonRemoveBtn() {
		btnHelper.click(removeSearchboxBtn);
	}

	public void verifysearchboxRemovebtn() {
		Assert.assertEquals(textboxHelper.getText(searchbox), "");
	}

	public void clickonSitelisting() {
		btnHelper.click(siteListingBtn);
	}

	public void clickonFirstSiteofSitelisting() {
		btnHelper.click(firstSiteofSiteListing);
	}

	public void verifyaddEditofSitelisting() {
		Assert.assertEquals(textboxHelper.getText(addEditSitesofSiteListing), ObjectRepo.reader.getAddEditSitesofSiteListing());

	}

	public void clickonAddnewofsiteLevelName() {
		btnHelper.click(addnewofSitenameLevel);
	}

	public void verifyColorofSaveBtn() {
		Assert.assertEquals(helper.BtnColor(savebtnofnamelevelSite), ObjectRepo.reader.getLightGreenColor());
	}

	public void verifyColorofcancelBtn() {
		Assert.assertEquals(helper.BtnColor(cancelbtnofnamelevelSite), ObjectRepo.reader.getDarkGreyColor());
	}

	public void clickonFirstSiteofsitelevelName() {
		btnHelper.click(firstsiteofNameLevelSite);
	}

	public void clickcancelBtnofSiteLevelName() {
		btnHelper.click(cancelBtnfirstsiteofNameLevelSite);
	}

	public void verifycancelBtnofSiteLevelName() {
		Assert.assertEquals(textboxHelper.getText(redirectedNameLevelSite), ObjectRepo.reader.getRedirectedNameLevelSite());
	}

	public void clickonViewAuditSitelistingScreen() {
		btnHelper.click(ViewAuditoflistingscreen);
	}

	public void verifyColorofcancelBtnViewAuditScreen() {
		Assert.assertEquals(helper.BtnColor(verifyCancelBtnColorViewAuditoflistingscreen), ObjectRepo.reader.getDarkGreyColor());
	}

	public void clickonCancelBtnViewAuditScreen() {
		btnHelper.click(verifyCancelBtnColorViewAuditoflistingscreen);
	}

	public void verifycancelBtnofviewAuditredirection() {
		Assert.assertEquals(textboxHelper.getText(redirectedSiteListinScreen), ObjectRepo.reader.getVerifySitesTile());
	}

	public void verifySiteLevelNamescreen() {
		Assert.assertEquals(textboxHelper.getText(redirectedNameLevelSite), ObjectRepo.reader.getRedirectedNameLevelSite());
	}

	public void clickonViewAuditSiteLevename() {
		btnHelper.click(viewAuditofSiteleveName);
	}

	public void verifyredirectionofAudittrailScreen() {

		Assert.assertEquals(textboxHelper.getText(viewAuditscreenofSiteleveName), ObjectRepo.reader.getViewAuditTrailText());
	}

	public void verifyColorofCancelButtonAudittrailinSiteLevelName() {
		Assert.assertEquals(helper.BtnColor(verifyCancelBtnColorViewAuditSiteLevelNameScreen), ObjectRepo.reader.getDarkGreyColor());
	}

	public void clickonCancelButtonViewAuditofSitelevel() {
		btnHelper.click(verifyCancelBtnColorViewAuditSiteLevelNameScreen);
	}

	public void verifycancelBtnofviewAuditredirectionSiteLevel() {
		Assert.assertEquals(textboxHelper.getText(redirectedNameLevelSite), ObjectRepo.reader.getRedirectedNameLevelSite());
	}

	public void verifysearchoflisingscreen() throws Exception {
		Assert.assertEquals(textboxHelper.getText(verifysearchboxSiteListingScreen), ObjectRepo.reader.getVerifysearchboxSiteListingScreen());
	}

	public void clickLastbtnofSiteListing() {
		btnHelper.click(lastPageOfPaginationSiteListing);
	}

	public void verifyNumberofEntries() {
		String entryarray = textboxHelper.getText(verifyNumberEnriesinNamelevelSite); //verifyNumberEnriesinNamelevelSite.getText();
		String totalnumberofentries = entryarray.substring(24, 25);
		int result = Integer.parseInt(totalnumberofentries);
		waitObj.waitForElementVisible(driver.findElement(By.xpath("//div[3]/grid/div[2]/div/table/tbody/tr")));
		List<WebElement> TotalRowCount = driver.findElements(By.xpath("//div[3]/grid/div[2]/div/table/tbody/tr"));
		int p = TotalRowCount.size();
		Assert.assertEquals(TotalRowCount.size(), result);
	}

	public void clickonSortingIconOfSitelevel() {
		btnHelper.click(sortingIconNamelevelSite);
	}

	public void verifytotalEntriesSiteListing() throws Exception {
		double count = 0; double intPageSize;
		 
		String entryarray = textboxHelper.getText(totalentriesSiteListing); // totalentriesSiteListing.getText();
		String[] ab = entryarray.split("l ");
		String str = String.join(",", ab);
		String[] cd = str.split(" e");
		String[] pp = cd[0].split(",");
		String totalnumberofentries = pp[1];
		double totalEntries = Integer.parseInt(totalnumberofentries);
		if (Selectedpagesize.isDisplayed()) {
			String pageSizestring = textboxHelper.getText(Selectedpagesize);
			String pagesize = pageSizestring.substring(11, 13);
			intPageSize = Integer.parseInt(pagesize);
		} 
		else
		{
			intPageSize = 10;
		}
		
		double n = Math.ceil(totalEntries / intPageSize);

		for (double j = 0; j < n; j++) {

			for (int i = 1; i <= intPageSize; i++) {
				if (driver.findElements(By.xpath("//div/table/tbody/tr[" + i + "]")).size() == 1) 
				{
					count++;
				}
				else
				{
					System.err.println("element not found");
					break;
				}
			}
			if(driver.findElements(By.xpath("//li/a[@ng-click='selectPage(page + 1, $event)']")).size() == 1)
			new PaginationPageObject(driver).clickNextBtnPagination();
		
		}
		Assert.assertEquals(count, totalEntries);
	}

}