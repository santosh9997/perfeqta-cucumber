package com.cucumber.framework.helper.PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;

public class RolesAndPermissionsPageObject extends PageBase {
	private WebDriver driver;
	private final Logger log = LoggerHelper.getLogger(RolesAndPermissionsPageObject.class);
	private WaitHelper waitObj;
	private TextBoxHelper textBoxHelper;
	public ButtonHelper btnHelper;
	DropDownHelper drpDwnHelper;

	public RolesAndPermissionsPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		waitObj = new WaitHelper(driver, ObjectRepo.reader);
		textBoxHelper = new TextBoxHelper(driver);
		drpDwnHelper = new DropDownHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//h3[contains(text(),'Roles & Permissions')]")
	public WebElement rolesPermissionsTile;

	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.admin.roles.rolepermissions']")
	public WebElement rolePermsBtn;

	@FindBy(how = How.XPATH, using = "//select[@default-value-options='vm.roles']")
	public WebElement roleDrpDwn;

	@FindBy(how = How.XPATH, using = "//button[@ng-if='!vm.canSave']")
	public WebElement editBtn;

	@FindBy(how = How.XPATH, using = "//select[@name='qcModuleSelect']")
	public WebElement moduleDrpDwn;

	@FindBy(how = How.XPATH, using = "(//button[@ng-click='vm.saveRolePermissions()'])[2]")
	public WebElement saveBtn;

	@FindBy(how = How.XPATH, using = "//button[@class='confirm']")
	public WebElement popupOkBtn;

	// --------------------public methods--------------------

	public WebDriver getDriver() {
		return this.driver;
	}

	public void clickRolesPermissionsTile() throws Exception {
		btnHelper.click(rolesPermissionsTile);
	}

	public void clickRolesPermsBtn() throws Exception {
		btnHelper.click(rolePermsBtn);
	}

	public void selectRoleDrpDwn(String roleValue) {
		drpDwnHelper.selectDropDownText(roleDrpDwn, roleValue);
	}

	public void clickEditBtn() throws Exception {
		btnHelper.click(editBtn);
	}

	public void selectModuleDrpDwn(String moduleValue) {
		new Scroll().scrollTillElem(moduleDrpDwn);

		drpDwnHelper.selectDropDownText(moduleDrpDwn, moduleValue);
	}

	public void uncheckAppAllPermissions(String value) {

		new Scroll().scrollTillElem(driver.findElement(By.xpath("//*[contains(text(),'" + value + "')]")));

		for (int i = 5; i >= 1; i--) {
			WebElement getValue = driver.findElement(By.xpath("(//*[contains(text(),'" + value + "')]/following-sibling::td/input)[" + i + "]"));
		if (getValue.isSelected())
			getValue.click();
		}
	}

	public void checkAppAllPermissions(String value) {

		String split[] = value.split(",");
		
		for (String getName: split) {
			new Scroll().scrollTillElem(driver.findElement(By.xpath("//*[contains(text(),'" + getName + "')]")));

			for (int i = 5; i >= 1; i--) 
			{
					WebElement getValue = driver.findElement(By.xpath("(//*[contains(text(),'" + getName + "')]/following-sibling::td/input)[" + i + "]"));
				if (!getValue.isSelected())
					getValue.click();
				}
		}
	}		
	
	public void clickSaveAndPopup() {
		new Scroll().scrollTillElem(saveBtn);
		btnHelper.click(saveBtn);
		btnHelper.click(popupOkBtn);
	}

	
}// end
