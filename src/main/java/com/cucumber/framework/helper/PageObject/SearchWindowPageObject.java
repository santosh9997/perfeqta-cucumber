package com.cucumber.framework.helper.PageObject;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.swing.text.StyledEditorKit.BoldAction;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.CheckBox.CheckBoxOrRadioButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Generic.GenericHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.Search.Search;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import cucumber.api.java.eo.Se;
import cucumber.api.java.lu.a;
import gherkin.lexer.Th;
import mx4j.tools.config.DefaultConfigurationBuilder.Object;

import com.cucumber.framework.helper.Sorting.SortingOnColumn;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;

public class SearchWindowPageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	public CheckBoxOrRadioButtonHelper checkBoxAndBtnHelper;
	public TextBoxHelper textBoxHelper;
	public DropDownHelper drpdwnHelper;
	private final Logger log = LoggerHelper.getLogger(SearchWindowPageObject.class);
	private SortingOnColumn sortColumn;
	private Search search;
	private Scroll scroll;
	private CommonFunctionPageObject commonFnPageObject;
	public AppBuilderPageObject appBuildPageObj;
	public AppsPageObject appsPageObj;
	private GenericHelper genericHelper;
	public static String firstFavName;

	public SearchWindowPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		checkBoxAndBtnHelper = new CheckBoxOrRadioButtonHelper(driver);
		textBoxHelper = new TextBoxHelper(driver);
		drpdwnHelper = new DropDownHelper(driver);
		genericHelper = new GenericHelper(driver);
		sortColumn = new SortingOnColumn();
		search = new Search();
		scroll = new Scroll();
	}

	/** WebElements */

	@FindBy(how = How.LINK_TEXT, using = "Qualification Performance")
	public WebElement qualificationPerTile;

	@FindBy(how = How.LINK_TEXT, using = "Search Window")
	public WebElement searchWindTile;

	@FindBy(how = How.XPATH, using = "//*[@name='searchModuleSelect']")
	public WebElement moduleDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@title='Click to search']")
	public WebElement searchBtn;

	@FindBy(how = How.XPATH, using = "(//*[@ng-class='settings.buttonClasses'])[1]")
	public WebElement appDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='searchFilter']")
	public WebElement searchFilter;

	@FindBy(how = How.XPATH, using = "(//*[@class='checkboxInput'])[1]")
	public WebElement searchFirstAppRadBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='currentStep ng-binding']")
	public WebElement searchWindowLabl;

	@FindBy(how = How.XPATH, using = "//*[@class='crumb breadcrumb gray ng-isolate-scope']")
	public WebElement searchWindowBreCr;

	public WebElement appName;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.onChangeFilter()']")
	public WebElement filterByBtn;

	@FindBy(how = How.XPATH, using = "(//*[@ng-class='settings.buttonClasses'])[2]")
	public WebElement windowType;

	@FindBy(how = How.XPATH, using = "(//*[@data-ng-click='selectAll()'])[2]")
	public WebElement checkAllBtn2;

	@FindBy(how = How.XPATH, using = "(//*[@data-ng-click='deselectAll();'])[2]")
	public WebElement uncheckAllBtn2;

	public WebElement windowTypeCheckBox;


	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.window.searchByDrop']")
	public WebElement dateOpDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@name='startDateTextBox']")
	public WebElement dateOption;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-sm btn-info uib-datepicker-current ng-binding']")
	public WebElement todayDateBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-default margin-right-0-advanced-search']")
	public WebElement datePicker;

	@FindBy(how = How.XPATH, using = "(//a[@href=''][text()='Clear'])[1]")
	public WebElement clearLink;

	@FindBy(how = How.XPATH, using = "//*[@value='monthYear']")
	public WebElement monthYearRaBtn;

	@FindBy(how = How.XPATH, using = "//select[@name='yearSelect']")
	public WebElement yearDrpDwn;

	@FindBy(how = How.XPATH, using = "//*[@class='err-message ng-scope']")
	public WebElement monthyerValMsg;

	@FindBy(how = How.XPATH, using = "(//*[@class='nav-link ng-binding'])[2]")
	public WebElement systemFav;

	@FindBy(how = How.XPATH, using = "(//*[@ng-if=\"tab.heading == 'System Favorites'\"])[1]")
	public WebElement lastUpdatedBy;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='!vm.isFavoriteUpdate']")
	public WebElement addToFavBtn;

	@FindBy(how = How.XPATH, using = "//*[@name='searchNameTextbox']")
	public WebElement favNameTextBox;

	@FindBy(how = How.XPATH, using = "//*[@class='btn btn-success ng-binding']")
	public WebElement addToFavSaveBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='wrap-file-title-entity']")
	public WebElement favSearchName;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.FAVORITENAMEMUSTBEUNIQUE']")
	public WebElement favNameValMsgForDup;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.FAVORITENAMEISREQUIRED']")
	public WebElement favNameValMsgRequired;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.FAVORITENAMESHOULDBEAT2']")
	public WebElement favNameValMsgForMin;

	@FindBy(how = How.XPATH, using = "//*[@translate='text.FAVORITENAMESHOULDNOTMORE50']")
	public WebElement favNameValMsgForMax;

	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.closePopup()'])[2]")
	public WebElement addToFavCancelBtn;

	@FindBy(how = How.XPATH, using = "//*[@title='Click to clear filters and record results']")
	public WebElement clearAllBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='glyphicon glyphicon-remove critearia-01 ng-click-active']")
	public WebElement deleteFirstEntry;

	@FindBy(how = How.XPATH, using = "//*[@style='display: block;']")
	public WebElement deleteFirstEntryConfiPopup;

	@FindBy(how = How.XPATH, using = "//*[@class='cancel']")
	public WebElement deleteFirstEntryConfiPopupNoBtn;

	@FindBy(how = How.XPATH, using = "//*[@class='confirm']")
	public WebElement deleteFirstEntryConfiPopupYesBtn;

	@FindBy(how = How.XPATH, using = "//table[@ng-if='vm.getWindows && vm.getWindows.length']/tbody/tr[1]/td[1]/a")
	public WebElement firstAppId;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.countDetails.firstStage.getUnitTestedEntriesCount > 0']")
	public WebElement viewDetails;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.sortTable(vm.unitTestedRecordCount.query, header.seq)']")
	public WebElement qualityPerformanceForHead;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='!record.allRecords']")
	public WebElement qualityPerformanceForVal;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.favoriteClicked(data)'])[1]")
	public WebElement firstRecOfMyFav;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-click='vm.showRecord(record, vm.windows.app)'])[1]")
	public WebElement viewRecdLink;
	
	@FindBy(how = How.XPATH, using = "//*[@class='glyphicon glyphicon-remove pull-right critearia-01 ng-click-active entity-popup-close-btn']")
	public WebElement viewRecdPopUpCloseLink;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.countDetails.firstStage.pendingFailure > 0']")
	public WebElement pendingLink;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-bind-html='step.ncyBreadcrumbLabel'])[5]")
	public WebElement pendingFailureLabl;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.sortTable(vm.pendingFailureRecordCount.query, header.seq)']")
	public WebElement qpHeadForPendingFailLink;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-if='vm.assignPermission'])[1]")
	public WebElement firstAssignLink;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-bind-html='step.ncyBreadcrumbLabel'])[6]")
	public WebElement assignLabl;
	
	@FindBy(how = How.XPATH, using = "//*[@class=''][text()='Assign Failure']")
	public WebElement assignFailureLabl;

	@FindBy(how = How.XPATH, using = "//textarea[@name='windowComment']")
	public WebElement commentTextBox;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='assignPendingFailureForm.windowComment.$dirty && assignPendingFailureForm.windowComment.$error.minlength']")
	public WebElement commentTextBoxMinMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='assignPendingFailureForm.windowComment.$dirty && assignPendingFailureForm.windowComment.$error.maxlength']")
	public WebElement commentTextBoxMaxMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@class='margin-left-15px btn btn-success']")
	public WebElement saveBtnAssignScreen;
	
	@FindBy(how = How.XPATH, using = "//*[@class='alert alert-danger col-sm-10']")
	public WebElement requiredValMsgForAssignScreen;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.failureType']")
	public WebElement failureTypeOption;
	
	@FindBy(how = How.XPATH, using = "//*[@class='confirm']")
	public WebElement yesBtnPopup;
	
	@FindBy(how = How.XPATH, using = "//*[@class='cancel']")
	public WebElement noBtnPopup;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-bind-html='step.ncyBreadcrumbLabel'])[4]")
	public WebElement windowDetailBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.countDetails.firstStage.nonProcessFailure > 0']")
	public WebElement nonProcessLink;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.countDetails.firstStage.processFailure > 0']")
	public WebElement processLink;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-bind-html='step.ncyBreadcrumbLabel'])[5]")
	public WebElement nonProcessLabl;
	
	@FindBy(how = How.XPATH, using = "(//*[@ng-bind-html='step.ncyBreadcrumbLabel'])[5]")
	public WebElement processLabl;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='!record.allRecords']")
	public WebElement scrollElementForTable;
	
	@FindBy(how = How.XPATH, using = "//textarea[@name='windowComment']")
	public WebElement stopWindowCommentTextBox;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='viewWindowForm.windowComment.$dirty && viewWindowForm.windowComment.$error.minlength']")
	public WebElement stopWindowCommentMinValMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-if='viewWindowForm.windowComment.$dirty && viewWindowForm.windowComment.$error.maxlength']")
	public WebElement stopWindowCommentMaxValMsg;
	
	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.stopWindow(vm.windows,viewWindowForm)']")
	public WebElement stopWindowBtn;
	
	@FindBy(how = How.XPATH, using = "//*[@style='display: block;']")
	public WebElement popUpMsgOfStopWindow;
	
	@FindBy(how = How.XPATH, using = "//*[@class='cancel']")
	public WebElement noBtnOfStopWindow;
	
	@FindBy(how = How.XPATH, using = "//*[@class='confirm']")
	public WebElement yesBtnOfStopWindow;
	
	@FindBy(how = How.XPATH, using = "//b[@class='ng-binding']")
	public WebElement windowStatus;


	// -----------------------------------public method------------------------------
	public void clickQualificationPerTile() throws Exception {
		Thread.sleep(2000);
		btnHelper.click(qualificationPerTile);
	}

	public void clicksearchWindTile() throws Exception {
		Thread.sleep(1000);
		//waitObj.//waitForElementVisible(searchWindTile);
		btnHelper.click(searchWindTile);
	}

	public void verifyModDrpDwnDefVal() throws Exception {

//		Select selectedValue = new Select();
		String option =  drpdwnHelper.getFirstSelectedOption(moduleDrpDwn);
		Assert.assertEquals(option , ObjectRepo.reader.getModuleDrpDwn());
	}

	public void selectValOfModDrpDwn() throws Exception {

//		Select selectedValue = new Select(moduleDrpDwn);
//		selectedValue.selectByIndex(1);
		drpdwnHelper.SelectUsingIndex(moduleDrpDwn, 1);
	}

	public void verifySearchBtnDisabled() throws Exception {

		Assert.assertEquals(textBoxHelper.isEnabled(searchBtn), false);
	}

	public void selectValOfAppDrpDwn(String search) throws Exception {

		btnHelper.click(appDrpDwn);
		btnHelper.click(searchFilter);
		textBoxHelper.sendKeys(searchFilter,search);
		btnHelper.click(searchFirstAppRadBtn);
		btnHelper.click(appDrpDwn);
	}

	public void clickSearchBtn() throws Exception {
//		Thread.sleep(2000);
		////wait.until(ExpectedConditions.elementToBeClickable(searchBtn));
		btnHelper.click(searchBtn);
	}

	public void verifySearchFun(String appVal) throws Exception {
		int findValue = 0;

		List<WebElement> count = driver.findElements(By.xpath("//*[@data-th='App:']"));

		for (int i = 1; i <= count.size(); i++) {
			appName = ObjectRepo.driver.findElement(By.xpath("(//*[@data-th='App:'])[" + i + "]"));
			String AppName = textBoxHelper.getText(appName);
			if (AppName.equalsIgnoreCase(appVal)) {
				findValue++;
			} else {

			}
		}
		Assert.assertEquals(findValue, count.size());

	}

	public void verifySearchBtnEnabled() throws Exception {
		Assert.assertEquals(textBoxHelper.isEnabled(searchBtn), true);
	}

	public void verifySearchWindowLabl() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(searchWindowLabl), ObjectRepo.reader.getSearchWindowLabl());
	}

	public void verifySearchWindowBreCrum() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(searchWindowBreCr), ObjectRepo.reader.getSearchWindowBreCr());
	}

	public void clickFilterByBtn() throws Exception {
		btnHelper.click(filterByBtn);
	}

	public void clickWindowTypeBtn() throws Exception {
		btnHelper.click(windowType);
	}

	public void clickCheckAllBtn2() throws Exception {
		checkAllBtn2.click();
	}

	public void verifyCheckAllFunOfWinTyp() throws Exception {
		int findValue = 0;

		List<WebElement> count = driver.findElements(By.xpath("//*[@ng-click='checkboxClick($event, getPropertyForObject(option,settings.idProp))']"));

		for (int i = 1; i <= count.size(); i++) {
			windowTypeCheckBox = ObjectRepo.driver.findElement(By.xpath("(//*[@ng-click='checkboxClick($event, getPropertyForObject(option,settings.idProp))'])[" + i + "]"));

			if (checkBoxAndBtnHelper.isIselected(windowTypeCheckBox)) {
				findValue++;
			} else {

			}
		}
		Assert.assertEquals(findValue, count.size());

	}

	public void clickUncheckAllBtn2() throws Exception {
		btnHelper.click(uncheckAllBtn2);
	}

	public void verifyuncheckAllFunOfWinTyp() throws Exception {
		int findValue = 0;

		List<WebElement> count = driver.findElements(By.xpath("//*[@ng-click='checkboxClick($event, getPropertyForObject(option,settings.idProp))']"));

		for (int i = 1; i <= count.size(); i++) {
			windowTypeCheckBox = ObjectRepo.driver.findElement(By.xpath("(//*[@ng-click='checkboxClick($event, getPropertyForObject(option,settings.idProp))'])[" + i + "]"));

			if (checkBoxAndBtnHelper.isIselected(windowTypeCheckBox)) {

			} else {
				findValue++;
			}
		}
		Assert.assertEquals(findValue, count.size());

	}

	public void selectDateOpDrpDwn() throws Exception {

//		Select selectedValue = new Select(dateOpDrpDwn);
//		selectedValue.selectByIndex(1);
		drpdwnHelper.selectDropDownIndex(dateOpDrpDwn, 1);
	}

	public void verifyCurrentDate() throws Exception {
		Helper hs = new Helper();
		String dateVal =  hs.getClipboardContents(dateOption);

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		String dateFormatted = dateFormat.format(date);
		System.out.println("System Date with Date Picker: " + dateFormatted);

		Assert.assertEquals(dateFormatted, dateVal);
	}

	public void clickDatePicker() throws Exception {
		btnHelper.click(datePicker);
	}

	public void clickTodayDateBtn() throws Exception {
		btnHelper.click(todayDateBtn);
	}

	public void clickClearLink() throws Exception {
		btnHelper.click(clearLink);
	}

	public void verifyClearFun() throws Exception {
//		Select selectedValue = new Select(dateOpDrpDwn);
//		WebElement option = selectedValue.getFirstSelectedOption();
		Assert.assertEquals(drpdwnHelper.getFirstSelectedOption(dateOpDrpDwn), ObjectRepo.reader.getDateOpDrpDwn());
	}

	public void clickMonthYearRadBtn() throws Exception {
		btnHelper.click(monthYearRaBtn);
	}

	public void selectYearDrpDwn() throws Exception {

//		Select selectedValue = new Select(yearDrpDwn);
//		selectedValue.selectByIndex(1);
		drpdwnHelper.selectDropDownIndex(yearDrpDwn, 1);
	}

	public void verifyMonthValMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(monthyerValMsg), ObjectRepo.reader.getMonthRequireValMsg());
		
	}

	public void selectYearDrpDwnAndPressTab() throws Exception {
//		Select year = new Select(yearDrpDwn);
//		year.selectByIndex(0);
		drpdwnHelper.selectDropDownIndex(yearDrpDwn, 0);
		yearDrpDwn.sendKeys(Keys.TAB);
//		Thread.sleep(1000);
	}

	public void verifyYearValMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(monthyerValMsg), ObjectRepo.reader.getYearRequiredValMsg());
	}

	public void clickSystemFav() throws Exception {
		btnHelper.click(systemFav);
	}

	public void verifySystemfav() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(lastUpdatedBy), ObjectRepo.reader.getLastUpdatedBy());
	}

	public void verifyAddToFavLabl() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(addToFavBtn), ObjectRepo.reader.getAddToFavoriteLabel());
	}

	public void clickAddToFav() throws Exception {
		btnHelper.click(addToFavBtn);
	}

	public void clickFavNameTextBox() throws Exception {
		btnHelper.click(favNameTextBox);
	}

	public void enterDataInFavName(String favName) throws Exception {
		textBoxHelper.sendKeys(favNameTextBox,favName);
	}

	public void clickSaveBtn() throws Exception {
		btnHelper.click(addToFavSaveBtn);
		////wait.until(ExpectedConditions.visibilityOf(favSearchName));
		Thread.sleep(2000);
		
	}

	public void verifyAddToFavFun(String favName) throws Exception {
		Assert.assertEquals(textBoxHelper.getText(favSearchName), favName);
	}

	public void verifyUniqueFavNamValMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(favNameValMsgForDup),  ObjectRepo.reader.getFavNameUniqueValMsg());
	}

	public void verifyRequiredFavNamValMsg() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(favNameValMsgRequired),  ObjectRepo.reader.getFavNameRequireValMsg());
	}

	public void verifyFavNamValMsgForMin() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(favNameValMsgForMin),  ObjectRepo.reader.getFavNameMinValMsg());
	}

	public void verifyFavNamValMsgForMax() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(favNameValMsgForMax),  ObjectRepo.reader.getFavNameMaxValMsg());
	}

	public void verifyAddToFavSaveBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(addToFavSaveBtn),  ObjectRepo.reader.getLightGreenColor());
	}

	public void verifyAddToFavCancelBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(addToFavCancelBtn),  ObjectRepo.reader.getDarkGreyColor());
	}

	public void verifySearchBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(searchBtn),  ObjectRepo.reader.getLightGreenColor());
	}

	public void verifyClearAllBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(clearAllBtn),  ObjectRepo.reader.getDarkGreyColor());
	}

	public void verifyAddToFavBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(addToFavBtn),  ObjectRepo.reader.getBlueColor());
	}

	public void clickDeleteBtnOfMyFav() throws Exception {
		firstFavName = driver.findElement(By.xpath("(//*[@ng-click='vm.favoriteClicked(data)'])[1]")).getText();
		btnHelper.click(deleteFirstEntry);
	}

	public void verifyConfirmationPopUpLabl() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(deleteFirstEntryConfiPopup),  ObjectRepo.reader.getRemoveMsgMyFavorite() + firstFavName + ObjectRepo.reader.getQuestionMark());
	}

	public void verifyConfirmationNoBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(deleteFirstEntryConfiPopupNoBtn),  ObjectRepo.reader.getRemoveMsgColor());
	}

	public void clickConfirmationNoBtn() throws Exception {
		btnHelper.click(deleteFirstEntryConfiPopupNoBtn);
	}

	public void verifyConfirmationNoBtnFun() throws Exception {
		boolean noBtn = driver.findElements(By.xpath("//*[@class='wrap-file-title-entity']")).size()!=0;
		Assert.assertEquals(noBtn,  true);
	}

	public void verifyConfirmatinYesBtnColor() throws Exception {
		Assert.assertEquals(btnHelper.BtnColor(deleteFirstEntryConfiPopupYesBtn),  ObjectRepo.reader.getLightGreenColor());
	}

	public void clickConfirmationYesBtn() throws Exception {
		Thread.sleep(1000);
		btnHelper.click(deleteFirstEntryConfiPopupYesBtn);
		//Thread.sleep(2000);
	}

	public void verifyConfirmationYesBtnFun() throws Exception {
		Thread.sleep(1000);
		boolean noBtn = driver.findElements(By.xpath("(//*[@ng-click='vm.favoriteClicked(data)'])[text()='"+ firstFavName +"']")).size()!=0;
		Assert.assertEquals(noBtn, false);
	}

	public void clickClearAllBtn() throws Exception {
		//Thread.sleep(2000);
		btnHelper.click(clearAllBtn);
		//Thread.sleep(2000);
	}
	public void verifyClearAllFun() throws Exception {

//		Select moduleDropDown = new Select(moduleDrpDwn);
//		moduleDropDown.getFirstSelectedOption();
		String MudlSelct1 = drpdwnHelper.getFirstSelectedOption(moduleDrpDwn);
		System.out.println("After:" + MudlSelct1);
		String Appslct = textBoxHelper.getText(appDrpDwn);
		System.out.println("After:" + Appslct);
		String result = null;
		boolean table = driver.findElements(By.xpath("//*[@ng-if='vm.getWindows && vm.getWindows.length']")).size()!=0;
		if (Appslct.contains(ObjectRepo.reader.getModuleDrpDwn()) && MudlSelct1.contains(ObjectRepo.reader.getModuleDrpDwn()) && table==false) {
			result = "true";
		} else {
			result = "false";
		}

		Assert.assertEquals(result, "true");

	}

	public void clickOnFirstAppId() throws Exception {
		//Thread.sleep(2000);
		////wait.until(ExpectedConditions.elementToBeClickable(firstAppId));
		//Thread.sleep(1000);
		btnHelper.click(firstAppId);
	}

	public void clickOnViewDetails() throws Exception {
		//Thread.sleep(2000);
		//wait.until(ExpectedConditions.elementToBeClickable(viewDetails));
		btnHelper.click(viewDetails);
	}

	public String headerType;
	public void verifyQualityPerformanceForTableHead() throws Exception {
		new Scroll().scrollTillElem(qualityPerformanceForHead);

		List<WebElement> count = driver.findElements(By.
				xpath("//*[@ng-click='vm.sortTable(vm.unitTestedRecordCount.query, header.seq)']"));

		for (int i = 1; i <= count.size(); i++) { 
			headerType = ObjectRepo.driver.findElement(By. xpath("(//*[@ng-click='vm.sortTable(vm.unitTestedRecordCount.query, header.seq)'])["
					+ i + "]")).getText();

			switch (i)  {
			case 1:
				Assert.assertEquals(headerType, ObjectRepo.reader.getHeaderType());
				break;
			case 2:		
				Assert.assertEquals(headerType, appBuildPageObj.firstvalue);
				break;
			case 3:		
				Assert.assertEquals(headerType, appBuildPageObj.secondvalue);
				break;
			case 4:		
				String entity = appBuildPageObj.entityName + " " + ObjectRepo.reader.getHeaderTypeForEntity();
				Assert.assertEquals(headerType , entity);
				break;
			case 5:		
				String procedure = appBuildPageObj.procedureName + " "+ ObjectRepo.reader.getHeaderTypeForProcedure();
				Assert.assertEquals(headerType , procedure);
				break;
			case 6:		
				Assert.assertEquals(headerType , ObjectRepo.reader.getRecordStatusText());
				break;
			default:
				System.out.println("Default method");
				break;

			}
		}
	}
	
	public static String appNameSearchWindow;
	public void getTextOfFirstRecord() throws Exception {
		//Thread.sleep(2000);
		appNameSearchWindow = textBoxHelper.getText(firstRecOfMyFav);
	}
	
	public void clickOnFirstRecordOfMyFav() throws Exception {
		//Thread.sleep(2000);
		btnHelper.click(firstRecOfMyFav);
	}
	
	public String tableData;
	public void verifyQualityPerformanceForTableVal() throws Exception {
		new Scroll().scrollTillElem(qualityPerformanceForVal);
		String date = genericHelper.getTodayDate();
		List<WebElement> count = driver.findElements(By.
				xpath("//*[@ng-if='!record.allRecords']"));

		for (int i = 1; i <= count.size(); i++) { 
			tableData = ObjectRepo.driver.findElement(By. xpath("(//*[@ng-if='!record.allRecords'])[" + i + "]")).getText();

			switch (i)  {
			case 1:
				Assert.assertEquals(tableData, ObjectRepo.reader.getViewRcdLabl());
				break;
			case 2:
				Assert.assertEquals(tableData, date);
				break;	
			case 3:		
				Assert.assertEquals(tableData, appsPageObj.searchDin);
				break;
			case 4:		
				Assert.assertEquals(tableData, appsPageObj.searchDemo_MJ);
				break;
			case 5:		
				Assert.assertEquals(tableData , ObjectRepo.reader.getPassedMsg());
				break;
			case 6:		
				Assert.assertEquals(tableData , ObjectRepo.reader.getPassedMsg());
				break;
			default:
				System.out.println("Default method");
				break;

			}
		}
	}
	
	public void clickOnViewRecdLink() throws Exception {
		//wait.until(ExpectedConditions.elementToBeClickable(viewRecdLink));
		btnHelper.click(viewRecdLink);
		
	}

	public void verifyViewRecdPopupOpened() throws Exception {
		boolean popUp = driver.findElements(By.xpath("//*[@class='modal-content ng-scope']")).size()!=0;
		Assert.assertEquals(popUp, true);
		//Thread.sleep(500);
		btnHelper.click(viewRecdPopUpCloseLink);
	}
	
	public void clickOnRecentWindowId() throws Exception {
		// btnHelper.click(By.xpath("//*[@name='pagesize']"));
		// WebElement pageSize = driver.findElement(By.xpath("//*[@name='pagesize']"));
		// drpdwnHelper.selectDropDownText(pageSize, "100");
		// Thread.sleep(1000);
		// WebElement recentWindowId = driver.findElement(By.xpath("(//*[@ng-click=\"vm.onEditClick(window['ID'], window['_id'])\"])[text()='" + id +"']"));
		// btnHelper.click(recentWindowId);
		WebElement idclick = driver.findElement(By.xpath("//*[@ng-click=\"vm.sortTable(vm.getAllCountData.query, 'windowId')\"]"));
		btnHelper.click(idclick);
		Thread.sleep(1000);
		btnHelper.click(firstAppId);
	}
	
	public void verifyPendingLinkVisible() {
		scroll.scrollTillElem(pendingLink);
		boolean actual = driver.findElements(By.xpath("//*[@ng-if='vm.countDetails.firstStage.pendingFailure > 0']")).size()!=0;
	}
	
	public void clickPendingLink() throws Exception {
		Thread.sleep(1000);
		btnHelper.click(pendingLink);
	}
	
	public void verifyPendingFailureScreenOpened() {
		Assert.assertEquals(textBoxHelper.getText(pendingFailureLabl), ObjectRepo.reader.getPendingFailureLabl());
	}
	
	public void verifyQPTableHeadForFailure() throws Exception {
		new Scroll().scrollTillElem(qpHeadForPendingFailLink);

		List<WebElement> count = driver.findElements(By.
				xpath("//*[@ng-click='vm.sortTable(vm.pendingFailureRecordCount.query, header.seq)']"));

		for (int i = 1; i <= count.size(); i++) { 
			headerType = ObjectRepo.driver.findElement(By. xpath("(//*[@ng-click='vm.sortTable(vm.pendingFailureRecordCount.query, header.seq)'])["
					+ i + "]")).getText();

			switch (i)  {
			case 1:
				Assert.assertEquals(headerType, ObjectRepo.reader.getHeaderType());
				break;
			case 2:		
				Assert.assertEquals(headerType, appBuildPageObj.firstvalue);
				break;
			case 3:		
				Assert.assertEquals(headerType, appBuildPageObj.secondvalue);
				break;
			case 4:		
				String entity = appBuildPageObj.entityName + " " + ObjectRepo.reader.getHeaderTypeForEntity();
				Assert.assertEquals(headerType , entity);
				break;
			case 5:		
				String procedure = appBuildPageObj.procedureName + " "+ ObjectRepo.reader.getHeaderTypeForProcedure();
				Assert.assertEquals(headerType , procedure);
				break;
			case 6:		
				Assert.assertEquals(headerType , ObjectRepo.reader.getRecordStatusText());
				break;
			case 7:
				Assert.assertEquals(headerType, ObjectRepo.reader.getDispositionLablText());
				break;
			default:
				System.out.println("Default method");
				break;

			}
		}
	}
	
	public void verifyPendingFailureTableVal() throws Exception {
		new Scroll().scrollTillElem(pendingFailureLabl);
		String date = genericHelper.getTodayDate();
		List<WebElement> count = driver.findElements(By.
				xpath("//*[@ng-if='!record.allRecords']"));
		
		for (int i = 1; i <= count.size(); i++) { 
			tableData = ObjectRepo.driver.findElement(By. xpath("(//*[@ng-if='!record.allRecords'])[" + i + "]")).getText().trim();

			switch (i)  {
			case 1:
				Assert.assertEquals(tableData, ObjectRepo.reader.getViewRcdLabl());
				break;
			case 2:
				Assert.assertEquals(tableData, date);
				break;	
			case 3:		
				Assert.assertEquals(tableData, appsPageObj.searchDin);
				break;
			case 4:		
				Assert.assertEquals(tableData, appsPageObj.searchDemo_MJ);
				break;
			case 5:		
				Assert.assertEquals(tableData , ObjectRepo.reader.getFailedMsg());
				break;
			case 6:		
				Assert.assertEquals(tableData , ObjectRepo.reader.getFailedMsg());
				break;
			default:
				System.out.println("Default method");
				break;

			}
		}
	}
	
	public void clickOnFirstAssignLink() {
		btnHelper.click(firstAssignLink);
	}
	
	public void verifyAssignLinkClickable() {
		Assert.assertEquals(textBoxHelper.getText(assignLabl), ObjectRepo.reader.getAssignLabl());
	}
	
	public void verifyAssignFailureText() {
		Assert.assertEquals(textBoxHelper.getText(assignFailureLabl), ObjectRepo.reader.getAssignFailureLabl());
	}
	
	public void enterMsgInCommentTextBox(String msg) {
		textBoxHelper.sendKeys(commentTextBox, msg);
	}
	
	public void verifyCommentMinMsg() {
		scroll.scrollTillElem(commentTextBoxMinMsg);
		Assert.assertEquals(textBoxHelper.getText(commentTextBoxMinMsg), ObjectRepo.reader.getCommentTextBoxMinMsg());
	}
	
	public void verifyCommentMinMsgColor() {
		scroll.scrollTillElem(commentTextBoxMinMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(commentTextBoxMinMsg), ObjectRepo.reader.getValMsgColor());
	}
	
	public void verifyCommentMaxMsg() {
		scroll.scrollTillElem(commentTextBoxMaxMsg);
		Assert.assertEquals(textBoxHelper.getText(commentTextBoxMaxMsg), ObjectRepo.reader.getCommentTextBoxMaxMsg());
	}
	
	public void verifyCommentMaxMsgColor() {
		scroll.scrollTillElem(commentTextBoxMaxMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(commentTextBoxMaxMsg), ObjectRepo.reader.getValMsgColor());
	}
	
	public void clickOnSaveBtnOfAssignScreen() {
		btnHelper.click(saveBtnAssignScreen);
	}
	
	public void verifyRequiredValMsg() {
		Assert.assertEquals(textBoxHelper.getText(requiredValMsgForAssignScreen), ObjectRepo.reader.getRequiredValMsgForAssignScreen());
	}
	
	public void verifyRequiredValMsgColor() {
		Assert.assertEquals(btnHelper.FontColorCodeHex(requiredValMsgForAssignScreen), ObjectRepo.reader.getValMsgColor());
	}
	
	public void selectNonProcessFromFailureType() {
		drpdwnHelper.selectDropDownText(failureTypeOption, ObjectRepo.reader.getFailureTypeNonProcess());
	}
	
	public void clickOnYesBtnOfPopup() throws Exception {
		Thread.sleep(2000);
		btnHelper.click(yesBtnPopup);
	}
	
	public void clickOnWindowDetailLink() {
		btnHelper.click(windowDetailBtn);
	}
	
	public void selectProcessFromFailureType() {
		drpdwnHelper.selectDropDownText(failureTypeOption, ObjectRepo.reader.getFailureTypeProcess());
	}
	 
	public void clickOnNonProcessLink() {
		scroll.scrollTillElem(nonProcessLink);
		btnHelper.click(nonProcessLink);
	}
	
	public void verifyNonProcessLinkVisiblity() {
		scroll.scrollTillElem(nonProcessLink);
		boolean visiblity = driver.findElements(By.xpath("//*[@ng-if='vm.countDetails.firstStage.nonProcessFailure > 0']")).size()!=0;
		Assert.assertEquals(visiblity, true);
	}
	
	public void verifyNonProcessClickFunction() {
		Assert.assertEquals(textBoxHelper.getText(nonProcessLabl), ObjectRepo.reader.getNonProcessLabl());
	}
	
	public void verifyNonProcessHeaderData() throws Exception {
		new Scroll().scrollTillElem(scrollElementForTable);

		List<WebElement> count = driver.findElements(By.
				xpath("//*[@ng-click='vm.sortTable(vm.nonProcessFailureRecordsCount.query, header.seq)']"));

		for (int i = 1; i <= count.size(); i++) { 
			headerType = ObjectRepo.driver.findElement(By. xpath("(//*[@ng-click='vm.sortTable(vm.nonProcessFailureRecordsCount.query, header.seq)'])["
					+ i + "]")).getText().trim();

			switch (i)  {
			case 1:
				Assert.assertEquals(headerType, ObjectRepo.reader.getHeaderType());
				break;
			case 2:		
				Assert.assertEquals(headerType, appBuildPageObj.firstvalue);
				break;
			case 3:		
				Assert.assertEquals(headerType, appBuildPageObj.secondvalue);
				break;
			case 4:		
				String entity = appBuildPageObj.entityName + " " + ObjectRepo.reader.getHeaderTypeForEntity();
				Assert.assertEquals(headerType , entity);
				break;
			case 5:		
				String procedure = appBuildPageObj.procedureName + " "+ ObjectRepo.reader.getHeaderTypeForProcedure();
				Assert.assertEquals(headerType , procedure);
				break;
			case 6:		
				Assert.assertEquals(headerType , ObjectRepo.reader.getRecordStatusText());
				break;
			case 7:
				Assert.assertEquals(headerType, ObjectRepo.reader.getFailuerTypeHead());
				break;
			default:
				System.out.println("Default method");
				break;

			}
		}
	}
	
	public void verifyNonProcessTableVal() throws Exception {
		new Scroll().scrollTillElem(pendingFailureLabl);
		String date = genericHelper.getTodayDate();
		List<WebElement> count = driver.findElements(By.
				xpath("//*[@ng-if='!record.allRecords']"));
		
		for (int i = 1; i <= count.size(); i++) { 
			tableData = ObjectRepo.driver.findElement(By. xpath("(//*[@ng-if='!record.allRecords'])[" + i + "]")).getText().trim();

			switch (i)  {
			case 1:
				Assert.assertEquals(tableData, ObjectRepo.reader.getViewRcdLabl());
				break;
			case 2:
				Assert.assertEquals(tableData, date);
				break;	
			case 3:		
				Assert.assertEquals(tableData, appsPageObj.searchDin);
				break;
			case 4:		
				Assert.assertEquals(tableData, appsPageObj.searchDemo_MJ);
				break;
			case 5:		
				Assert.assertEquals(tableData , ObjectRepo.reader.getFailedMsg());
				break;
			case 6:
				Assert.assertEquals(tableData , ObjectRepo.reader.getFailedMsg());
				break;
			case 7:		
				Assert.assertEquals(tableData , ObjectRepo.reader.getFailureTypeNonProcess());
				break;
			default:
				System.out.println("Default method");
				break;

			}
		}
	}
	
	public void verifyProcessLinkVisiblity() {
		scroll.scrollTillElem(processLink);
		boolean visiblity = driver.findElements(By.xpath("//*[@ng-if='vm.countDetails.firstStage.processFailure > 0']")).size()!=0;
		Assert.assertEquals(visiblity, true);
	}
	
	public void clickOnProcessLink() {
		scroll.scrollTillElem(processLink);
		btnHelper.click(processLink);
	}
	
	public void verifyProcessLinkFunction() {
		Assert.assertEquals(textBoxHelper.getText(processLabl), ObjectRepo.reader.getProcessLabl());
	}
	
	public void verifyProcessHeaderData() throws Exception {
		new Scroll().scrollTillElem(scrollElementForTable);

		List<WebElement> count = driver.findElements(By.
				xpath("//*[@ng-click='vm.sortTable(vm.processFailureRecordsCount.query, header.seq)']"));

		for (int i = 1; i <= count.size(); i++) { 
			headerType = ObjectRepo.driver.findElement(By. xpath("(//*[@ng-click='vm.sortTable(vm.processFailureRecordsCount.query, header.seq)'])["
					+ i + "]")).getText().trim();

			switch (i)  {
			case 1:
				Assert.assertEquals(headerType, ObjectRepo.reader.getHeaderType());
				break;
			case 2:		
				Assert.assertEquals(headerType, appBuildPageObj.firstvalue);
				break;
			case 3:		
				Assert.assertEquals(headerType, appBuildPageObj.secondvalue);
				break;
			case 4:		
				String entity = appBuildPageObj.entityName + " " + ObjectRepo.reader.getHeaderTypeForEntity();
				Assert.assertEquals(headerType , entity);
				break;
			case 5:		
				String procedure = appBuildPageObj.procedureName + " "+ ObjectRepo.reader.getHeaderTypeForProcedure();
				Assert.assertEquals(headerType , procedure);
				break;
			case 6:		
				Assert.assertEquals(headerType , ObjectRepo.reader.getRecordStatusText());
				break;
			case 7:
				Assert.assertEquals(headerType, ObjectRepo.reader.getFailuerTypeHead());
				break;
			default:
				System.out.println("Default method");
				break;

			}
		}
	}
	
	public void verifyProcessTableVal() throws Exception {
		new Scroll().scrollTillElem(pendingFailureLabl);
		String date = genericHelper.getTodayDate();
		List<WebElement> count = driver.findElements(By.
				xpath("//*[@ng-if='!record.allRecords']"));
		
		for (int i = 1; i <= count.size(); i++) { 
			tableData = ObjectRepo.driver.findElement(By. xpath("(//*[@ng-if='!record.allRecords'])[" + i + "]")).getText().trim();

			switch (i)  {
			case 1:
				Assert.assertEquals(tableData, ObjectRepo.reader.getViewRcdLabl());
				break;
			case 2:
				Assert.assertEquals(tableData, date);
				break;	
			case 3:		
				Assert.assertEquals(tableData, appsPageObj.searchDin);
				break;
			case 4:		
				Assert.assertEquals(tableData, appsPageObj.searchDemo_MJ);
				break;
			case 5:		
				Assert.assertEquals(tableData , ObjectRepo.reader.getFailedMsg());
				break;
			case 6:
				Assert.assertEquals(tableData , ObjectRepo.reader.getFailedMsg());
				break;
			case 7:		
				Assert.assertEquals(tableData , ObjectRepo.reader.getFailureTypeProcess());
				break;
			default:
				System.out.println("Default method");
				break;

			}
		}
	}
	
	public void enterMsgInStopWindowCommentTextbox(String msg) {
		textBoxHelper.sendKeys(commentTextBox, msg);
	}
	
	public void verifyMinValMsgOfStopWindowComment() {
		Assert.assertEquals(textBoxHelper.getText(stopWindowCommentMinValMsg), ObjectRepo.reader.getCommentTextBoxMinMsg());
	}
	
	public void verifyMinValMsgColorOfStopWindowComment() {
		Assert.assertEquals(btnHelper.FontColorCodeHex(stopWindowCommentMinValMsg), ObjectRepo.reader.getValMsgColor());
	}
	
	public void verifyMaxValMsgOfStopWindowComment() {
		Assert.assertEquals(textBoxHelper.getText(stopWindowCommentMaxValMsg), ObjectRepo.reader.getCommentTextBoxMaxMsg());
	}
	
	public void verifyMaxValMsgColorOfStopWindowComment() {
		Assert.assertEquals(btnHelper.FontColorCodeHex(stopWindowCommentMaxValMsg), ObjectRepo.reader.getValMsgColor());
	}
	
	public void verifyStopWindowBtnColor() {
		Assert.assertEquals(btnHelper.BtnColor(stopWindowBtn), ObjectRepo.reader.getRemoveMsgColor());
	}
	
	public void cickOnStopWindowBtn() {
		btnHelper.click(stopWindowBtn);
	}
	
	public void verifyPopUpMsgOfStopWindowScreen() {
		Assert.assertEquals(textBoxHelper.getText(popUpMsgOfStopWindow), ObjectRepo.reader.getPopUpMsgOfStopWindow());
	}
	
	public void verifyNoBtnColorOfStopWindow() {
		Assert.assertEquals(btnHelper.BtnColor(noBtnOfStopWindow), ObjectRepo.reader.getRemoveMsgColor());
	}
	
	public void clickOnNoBtnOfStopWindow() {
		btnHelper.click(noBtnOfStopWindow);
	}
	
	public void verifyNoBtnFunctionalityOfStopWindow() {
		SoftAssert sf= new SoftAssert();
		boolean popup = driver.findElements(By.xpath("//*[@class='sweet-alert sweet-button showSweetAlert visible']")).size()==0;
		sf.assertEquals(popup, true);
		sf.assertEquals(textBoxHelper.getText(windowStatus), ObjectRepo.reader.getWindowStatus());
		sf.assertAll();
	}
	
	public void verifyYesBtnColorOfStopWindow() {
		Assert.assertEquals(btnHelper.BtnColor(yesBtnOfStopWindow), ObjectRepo.reader.getLightGreenColor());
	}
	
	public void clickOnYesBtnOfStopWindow() throws Exception {
		Thread.sleep(1000);
		btnHelper.click(yesBtnOfStopWindow);
	}
	
	public void verifyYesBtnFunctionalityOfStopWindow() {
		SoftAssert sf= new SoftAssert();
		boolean popup = driver.findElements(By.xpath("//*[@class='sweet-alert sweet-button showSweetAlert visible']")).size()==0;
		sf.assertEquals(popup, true);
		sf.assertEquals(textBoxHelper.getText(windowStatus), ObjectRepo.reader.getWindowStatusStopped());
		sf.assertAll();
	}
}
