package com.cucumber.framework.helper.PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;

public class ShareAppLinkPageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private Helper helper = new Helper();
	private final Logger log = LoggerHelper.getLogger(ShareAppLinkPageObject.class);
	private WaitHelper waitObj;
	private TextBoxHelper textBoxHelper;

	public ShareAppLinkPageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		waitObj = new WaitHelper(driver, ObjectRepo.reader);
		textBoxHelper = new TextBoxHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.admin.shareapplink.list']")
	public WebElement ShareLinkTile;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']/li[3]/span")
	public WebElement verifyShareLinkTile;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']")
	public WebElement verifybreadcrumb;

	@FindBy(how = How.XPATH, using = "//th[1]")
	public WebElement colNameForSorting;
	@FindBy(how = How.XPATH, using = "(//A[@ng-switch-when='true'][text()='View Records'])[1]")
	public WebElement FirstRecods;
	@FindBy(how = How.XPATH, using = "//*[@id='breadcrum-ipad-768']")
	public WebElement VerifyBreadcrumb;
	@FindBy(how = How.XPATH, using = "//A[@ng-show='vm.isValidUser'][text()='Share App']")
	public WebElement ShareAppBtn;

	@FindBy(how = How.XPATH, using = "//SPAN[@ng-bind-html='step.ncyBreadcrumbLabel'][text()='Add / Edit Share App Link']")
	public WebElement ShareAppBtnDirection;

	@FindBy(how = How.XPATH, using = "//DIV[@class='pull-left ng-binding'][text()='Share App Link Details']")
	public WebElement verifyLableDisplay;
	@FindBy(how = How.XPATH, using = "//DIV[@class='pull-left ng-binding'][text()='App Details']")
	public WebElement verifyLableAppDeails;
	@FindBy(how = How.XPATH, using = "//DIV[@class='pull-left ng-binding'][text()='Outside User Details']")
	public WebElement verifyLableOtherUserDeails;
	@FindBy(how = How.XPATH, using = "//SELECT[@title='Please Select']")
	public WebElement moduleDropdown;
	@FindBy(how = How.XPATH, using = "//DIV[@class='error-message-color ng-scope'][text()='Module is required.']")
	public WebElement moduleDropdownReqValidation;
	@FindBy(how = How.XPATH, using = "//*[@ng-if='(editAppShareLinkForm.app.$dirty || editAppShareLinkForm.app.$touched) && editAppShareLinkForm.app.$error.required']")
	public WebElement appDropdownReqValidation;
	@FindBy(how = How.XPATH, using = "//select[@ng-model='vm.appShareLink.app']")
	public WebElement appDropdownclick;
	@FindBy(how = How.XPATH, using = "//select[@title='Equipment']")
	public WebElement selectModule;
	@FindBy(how = How.XPATH, using = "//select[@title=\"Donation Test\"]")
	public WebElement selectApp;

	@FindBy(how = How.XPATH, using = "//A[@href=''][text()='Copy Link']")
	public WebElement copyLink;

	@FindBy(how = How.XPATH, using = "//input[@ng-change='vm.onChangeEmailTextbox()']")
	public WebElement emailInputBox;
	@FindBy(how = How.XPATH, using = "//P[@translate='option.INVALIDEMAILADDRESSES'][text()='Invalid Email Address(es).']")
	public WebElement invalidEmailMsg;
	@FindBy(how = How.XPATH, using = "//BUTTON[@ng-if='vm.emailAddress && !vm.isDuplicateEmail']")
	public WebElement EmailAddBtn;
	@FindBy(how = How.XPATH, using = "(//INPUT[@ng-disabled='vm.isVersion || !vm.save || !vm.isValidUserforEdit'])[2]")
	public WebElement VerifyMultipleEmail;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='obj.isDeleted=true;editAppShareLinkForm.emaillist.$validate();']")
	public WebElement deleteEmail;
	@FindBy(how = How.XPATH, using = "//div[2]/div/div[1]/p")
	public WebElement EmailInfoMsgColor;
	@FindBy(how = How.XPATH, using = "(//INPUT[@type='checkbox'])[1]")
	public WebElement AllowMultipleApp;
	@FindBy(how = How.XPATH, using = "//div[3]/div/div/label[1]/em")
	public WebElement twooptions1InAllowMultipleApp;
	@FindBy(how = How.XPATH, using = "//div[3]/div/div/label[2]/em")
	public WebElement twooptions2InAllowMultipleApp;
	@FindBy(how = How.XPATH, using = "//input[@ng-checked=\"(vm.appShareLink.isForPerEmail == false)\"]")
	public WebElement totalNumberofAppRadioBtn;
	@FindBy(how = How.XPATH, using = "//INPUT[@type='submit']")
	public WebElement shareAppBtn;
	@FindBy(how = How.XPATH, using = "//p[@ng-if='editAppShareLinkForm.noOfTimeAllowed.$dirty && editAppShareLinkForm.noOfTimeAllowed.$error.required']")
	public WebElement totalNumberofAppRadioBtnValidationMsg;
	@FindBy(how = How.XPATH, using = "//INPUT[@name='noOfTimeAllowed']")
	public WebElement totalNumberofAppSubmissionBox;

	@FindBy(how = How.XPATH, using = "//p[@ng-if='editAppShareLinkForm.noOfTimeAllowed.$dirty && editAppShareLinkForm.noOfTimeAllowed.$error.min']")
	public WebElement totalNumberofAppSubmissioZeroValidation;
	@FindBy(how = How.XPATH, using = "//p[@ng-if='editAppShareLinkForm.noOfTimeAllowed.$dirty && editAppShareLinkForm.noOfTimeAllowed.$error.max']")
	public WebElement totalNumberofAppSubmissioMaxValidation;
	@FindBy(how = How.XPATH, using = "//input[@ng-click='vm.onAppSubmissionOptionSelect(editAppShareLinkForm, true)']")
	public WebElement clickonNumberofSunmissionperIndiv;
	@FindBy(how = How.XPATH, using = "//p[@ng-if='editAppShareLinkForm.noOfTimeAllowed.$dirty && editAppShareLinkForm.noOfTimeAllowed.$error.required']")
	public WebElement blankValidationNumberofSunmissionperIndiv;
	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.appShareLink.noOfTimeAllowed']")
	public WebElement numberofSunmissionperIndivBox;
	@FindBy(how = How.XPATH, using = "//p[@ng-if='editAppShareLinkForm.noOfTimeAllowed.$dirty && editAppShareLinkForm.noOfTimeAllowed.$error.min']")
	public WebElement minValidationNumberofSunmissionperIndivBox;
	@FindBy(how = How.XPATH, using = "//div[3]/p")
	public WebElement maxValidationNumberofSunmissionperIndivBox;
	@FindBy(how = How.XPATH, using = "//input[@ng-model='vm.setExpiryDate']")
	public WebElement setLinkexpiryDatecheckbox;
	@FindBy(how = How.XPATH, using = "//p[@translate='option.EXPIRYDATEISREQ']")
	public WebElement requiredsetLinkexpiryDate;
	@FindBy(how = How.XPATH, using = "//p[@translate='option.EXPIRYDATEFORMATISINV']")
	public WebElement invalidsetLinkexpiryDate;
	@FindBy(how = How.XPATH, using = "//input[@name='expiryDateTextBox']")
	public WebElement setLinkexpiryDateinput;
	@FindBy(how = How.XPATH, using = "//textarea[@ng-model='vm.appShareLink.message']")
	public WebElement specialMsgInputbox;
	@FindBy(how = How.XPATH, using = "//div[@translate='option.SPLMSGSHOULD2TO800']")
	public WebElement minvalidationSpecialMsg;

	@FindBy(how = How.XPATH, using = "//*[@ng-model='vm.textSearch']")
	public WebElement searchBox;

	
	// --------------------public methods--------------------

	
	public void clickShareAppTile() {
		btnHelper.click(ShareLinkTile);
	}

	public void verifyShareAppTile() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(verifyShareLinkTile), ObjectRepo.reader.getVerifyShareLinkTile());
	}

	public void verifyBreadcrumb() {
		Assert.assertEquals(textBoxHelper.getText(verifybreadcrumb), ObjectRepo.reader.getVerifybreadcrumb());
	}

	public void clickFirstRecord() {
		btnHelper.click(FirstRecods);
	}

	public void verifyBreadcrumbRecods() throws Exception {
		Assert.assertEquals(textBoxHelper.getText(VerifyBreadcrumb), ObjectRepo.reader.getVerifyBreadcrumbForRcd());
	}

	public void clickonShareAppBtn() {
		btnHelper.click(ShareAppBtn);
	}

	public void verifyShareAppBtnRedirection() {
		Assert.assertEquals(textBoxHelper.getText(ShareAppBtnDirection), ObjectRepo.reader.getShareAppBtnDirection());
	}

	public void VerifyLabelDisplay() {
		Assert.assertEquals(textBoxHelper.getText(verifyLableDisplay), ObjectRepo.reader.getVerifyLableDisplay());
	}

	public void verifyAppdetailsLable() {
		Assert.assertEquals(textBoxHelper.getText(verifyLableAppDeails),  ObjectRepo.reader.getVerifyLableAppDeails());
	}

	public void verifyLableOutsideUserDetail() {
		Assert.assertEquals(textBoxHelper.getText(verifyLableOtherUserDeails),  ObjectRepo.reader.getVerifyLableOtherUserDeails());
	}

	public void pressTab() {
		moduleDropdown.sendKeys(Keys.TAB);
		moduleDropdown.sendKeys(Keys.TAB);
	}

	public void clickmoduleDropDown() {
		btnHelper.click(moduleDropdown);
	}

	public void verifyModuleDropdownReqValidation() {
		Assert.assertEquals(textBoxHelper.getText(moduleDropdownReqValidation), ObjectRepo.reader.getModuleDrpDwnRequiredMsg());
	}

	public void clickAppDropdown() {
		btnHelper.click(appDropdownclick);
	}

	public void verifyAppDropdownReqValidation() {
		Assert.assertEquals(textBoxHelper.getText(appDropdownReqValidation), ObjectRepo.reader.getAppRequireValMsg());
	}

	public void selectModulefromModuleDropdown() {

		Select dropdown = new Select(driver.findElement(By.xpath("//SELECT[@title='Please Select']")));
		dropdown.selectByIndex(1);
	}

	public void selectAppFromAppDropdown() {
		Select dropdown = new Select(driver.findElement(By.xpath("//select[@ng-change='vm.getToken()']")));
		dropdown.selectByIndex(1);
	}

	public void verifyDisplayedLink() {
		if (copyLink.isDisplayed()) {
			Assert.assertEquals(true, true);
		} else {
			Assert.assertEquals(false, true);
		}
	}

	public void enterEmailAddr(String value) {
		textBoxHelper.sendKeys(emailInputBox, value);
	}

	public void verifyInvalidEmail() {

		Assert.assertEquals(textBoxHelper.getText(invalidEmailMsg), ObjectRepo.reader.getInvalidEmailMsg());
	}

	public void entervalidEmail(String value) {
		textBoxHelper.sendKeys(emailInputBox, value);
	}

	public void verifyValidEmail() {

		Assert.assertEquals(true, true);

	}

	public void enter1stValidEmail(String value) {
		textBoxHelper.sendKeys(emailInputBox, value);
		btnHelper.click(EmailAddBtn);
	}

	public void enter2ndValidEmail(String value) {
		textBoxHelper.sendKeys(emailInputBox, value);
		btnHelper.click(EmailAddBtn);
	}

	public void verifyAddedEmail() {
		if (VerifyMultipleEmail.isSelected()) {
			Assert.assertEquals(true, true);
		}
	}

	public void clickonRemoveEmail() {
		btnHelper.click(deleteEmail);
	}

	public void verifyRemovedEmail() {
		Assert.assertEquals(true, true);

	}

	public void verifyColorInfoMsgEmail() {
		System.out.println("This color belongs to predefined library so we can't verify");
		Assert.assertEquals(true, true);
	}

	public void allowMultipleApp() {
		btnHelper.click(AllowMultipleApp);
	}

	public void systemDisplay2options() {
		if (AllowMultipleApp.isSelected()) {

			Assert.assertEquals(textBoxHelper.getText(twooptions1InAllowMultipleApp), ObjectRepo.reader.getTwooptions1InAllowMultipleApp());
			Assert.assertEquals(textBoxHelper.getText(twooptions2InAllowMultipleApp), ObjectRepo.reader.getTwooptions2InAllowMultipleApp());
		}
	}

	public void selectBtnofTotalNumberofApp() {
		btnHelper.click(totalNumberofAppRadioBtn);
		totalNumberofAppSubmissionBox.sendKeys(Keys.TAB);
	}

	public void clickOnShareAppBtn() throws Exception {
		btnHelper.click(shareAppBtn);
		Thread.sleep(2000);
	}

	public void verifyValidationMsg() {

		Assert.assertEquals(textBoxHelper.getText(totalNumberofAppRadioBtnValidationMsg),
				ObjectRepo.reader.getTotalNumberofAppRadioBtnValidationMsg());

	}

	public void enter0inTotalnuberofAppSubmission(String value) {
		textBoxHelper.sendKeys(totalNumberofAppSubmissionBox, value);
	}

	public void verifyZeroValidationNuberOfAppSubmission() {
		Assert.assertEquals(textBoxHelper.getText(totalNumberofAppSubmissioZeroValidation),
				ObjectRepo.reader.getTotalNumberofAppSubmissioZeroValidation());
	}

	public void entermorethan10000inNumberofApp(String value) {
		textBoxHelper.sendKeys(totalNumberofAppSubmissionBox, value);
	}

	public void verifyMorethan10000valueinNumberofAppSub() {
		Assert.assertEquals(textBoxHelper.getText(totalNumberofAppSubmissioMaxValidation),
				ObjectRepo.reader.getTotalNumberofAppSubmissioMaxValidation());

	}

	public void entervalidvalueInNumberofAppSub(String value) {
		textBoxHelper.sendKeys(totalNumberofAppSubmissionBox, value);
	}

	public void verifyValidvalueinNumberofAppSubmission() {
		Assert.assertEquals(true, true);

	}

	public void clickradioBtnofNumberofSubmissionIndividuals() {
		btnHelper.click(clickonNumberofSunmissionperIndiv);
		numberofSunmissionperIndivBox.sendKeys(Keys.TAB);
	}

	public void verifyBlankValidation() {
		Assert.assertEquals(textBoxHelper.getText(blankValidationNumberofSunmissionperIndiv),ObjectRepo.reader.getBlankValidationNumberofSunmissionperIndiv());

	}

	public void enterValueZeroinNumberofSubmissionper(String value) {
		textBoxHelper.sendKeys(numberofSunmissionperIndivBox, value);
	}

	public void verifyZeroValidationNumberofSubmission() {
		Assert.assertEquals(textBoxHelper.getText(minValidationNumberofSunmissionperIndivBox),ObjectRepo.reader.getMinValidationNumberofSunmissionperIndivBox());
	}

	public void enterMaxvalueNuberofSubmissionIndividuals(String value) {
		textBoxHelper.sendKeys(numberofSunmissionperIndivBox, value);
	}

	public void verifyMaxValidationInNumberofSubmission() {
		Assert.assertEquals(textBoxHelper.getText(maxValidationNumberofSunmissionperIndivBox),ObjectRepo.reader.getMaxValidationNumberofSunmissionperIndivBox());

	}

	public void entervaliddataNumberofSubmission(String value) {
		textBoxHelper.sendKeys(numberofSunmissionperIndivBox, value);
	}

	public void verifyValidvalueinNumberofSubmission() {
		Assert.assertEquals(true, true);

	}

	public void clickonCheckboxofSetLinkExpiryDate() {
		btnHelper.click(setLinkexpiryDatecheckbox);
	}

	public void pressTabsetLinkExpiry() {
		setLinkexpiryDatecheckbox.sendKeys(Keys.TAB);
		setLinkexpiryDatecheckbox.sendKeys(Keys.TAB);
	}

	public void verifyExpiryDateValidation() {
		Assert.assertEquals(textBoxHelper.getText(requiredsetLinkexpiryDate), ObjectRepo.reader.getRequiredsetLinkexpiryDate());

	}

	public void enterInvalidDate(String value) {
		textBoxHelper.sendKeys(setLinkexpiryDateinput, value);
	}

	public void verifyValidationMsgSetlinkExpiry() {
		Assert.assertEquals(textBoxHelper.getText(invalidsetLinkexpiryDate), ObjectRepo.reader.getInvalidsetLinkexpiryDate());
	}

	public void enterlessthan2InSpecialMsg(String value) {
		textBoxHelper.sendKeys(specialMsgInputbox, value);
	}

	public void verifyMinValidationSpecialMsg() {
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(textBoxHelper.getText(minvalidationSpecialMsg), ObjectRepo.reader.getMinvalidationSpecialMsg());
	}

	public void enterValidSpecialMsg(String value) {
		textBoxHelper.sendKeys(specialMsgInputbox, value);
	}

	public void verifyValidDataAcceptSpecialMsg() {
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(true, true);
	}

	public void verifyredirectiontoListingScreen() {
		Assert.assertEquals(textBoxHelper.getText(verifyShareLinkTile), ObjectRepo.reader.getVerifyShareLinkTile());
	}

	public void pressTabAppDropdown() {
		btnHelper.click(appDropdownclick);
		appDropdownclick.sendKeys(Keys.TAB);
		appDropdownclick.sendKeys(Keys.TAB);

	}

	public void enterSearchBox(String value) {
		textBoxHelper.sendKeys(searchBox, value);
	}

}
