package com.cucumber.framework.helper.PageObject;

import static org.testng.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.cucumber.framework.configreader.PropertyFileReader;
import com.cucumber.framework.helper.Helper;
import com.cucumber.framework.helper.InitializeWebDrive;
import com.cucumber.framework.helper.Button.ButtonHelper;
import com.cucumber.framework.helper.CheckBox.CheckBoxOrRadioButtonHelper;
import com.cucumber.framework.helper.DropDown.DropDownHelper;
import com.cucumber.framework.helper.Generic.GenericHelper;
import com.cucumber.framework.helper.Logger.LoggerHelper;
import com.cucumber.framework.helper.Scroll.Scroll;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.settings.ObjectRepo;

public class MyProfilePageObject extends PageBase {
	private WebDriver driver;
	public ButtonHelper btnHelper;
	private final Logger log = LoggerHelper.getLogger(MyProfilePageObject.class);
	private Helper helper = new Helper();
	public CheckBoxOrRadioButtonHelper checkBoxAndBtnHelper;
	public TextBoxHelper textBoxHelper;
	public DropDownHelper drpdwnHelper;
	public GenericHelper genricHelper;

	public MyProfilePageObject(WebDriver driver) {
		super(driver);
		this.driver = driver;
		btnHelper = new ButtonHelper(driver);
		checkBoxAndBtnHelper = new CheckBoxOrRadioButtonHelper(driver);
		textBoxHelper = new TextBoxHelper(driver);
		drpdwnHelper = new DropDownHelper(driver);
		genricHelper = new GenericHelper(driver);
	}

	/** WebElements */

	@FindBy(how = How.XPATH, using = "//i[@class='header-username-bx ng-binding']")
	public WebElement usernameTab;

	@FindBy(how = How.XPATH, using = "//a[@ui-sref='secure.myprofile']")
	public WebElement clickMyProfile;

	@FindBy(how = How.XPATH, using = "//span[@class='currentStep ng-binding']")
	public WebElement moduleNameMyProfile;

	@FindBy(how = How.XPATH, using = "//li[@ng-repeat='step in steps | limitTo:(steps.length-1)']")
	public WebElement myProfileBreadcrumbs;

	@FindBy(how = How.XPATH, using = "//label[@for='userNameTextBox']")
	public WebElement usernameLabel;

	@FindBy(how = How.XPATH, using = "//input[@name='userNameTextBox']")
	public WebElement usernameFieldDisable;

	@FindBy(how = How.XPATH, using = "//label[@for='firstNameTextBox']")
	public WebElement firstnameLabel;

	@FindBy(how = How.XPATH, using = "//input[@name='firstNameTextBox']")
	public WebElement firstnameSameAsLogin;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.FNAMEREQ']")
	public WebElement firstnameRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.FNAMESHOULDAT2']")
	public WebElement firstnameMinimumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.FNAMENOTMOR50']")
	public WebElement firstnameMaximumMsg;

	@FindBy(how = How.XPATH, using = "//label[@for='lastNameTextBox']")
	public WebElement lastnameLabel;

	@FindBy(how = How.XPATH, using = "//input[@name='lastNameTextBox']")
	public WebElement lastnameVal;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.LASTNAMEREQ']")
	public WebElement lastnameRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.LNAMESHOULDAT2']")
	public WebElement lastnameMinimumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.LNAMENOTMOR50']")
	public WebElement lastnameMaximumMsg;

	@FindBy(how = How.XPATH, using = "//label[@for='emailTextBox']")
	public WebElement emailLabel;

	@FindBy(how = How.XPATH, using = "//input[@name='emailTextBox']")
	public WebElement emailTextfield;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.EMAILREQ']")
	public WebElement emailAddressRequired;

	@FindBy(how = How.XPATH, using = "//button[@class='btn btn-success ng-binding']")
	public WebElement saveBtn;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.INVALIDEMAIL']")
	public WebElement emailAddressInvalid;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.importdata.automated.Email50']")
	public WebElement emailAddressMaximum;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.myprofile.EMAILMUSTBUNI']")
	public WebElement emailAddressUnique;

	@FindBy(how = How.XPATH, using = "//*[@ng-if='vm.user.newEmail']")
	public WebElement verifiedEmailInfo;

	@FindBy(how = How.XPATH, using = "//a[@ng-click='vm.cancelEmailVerificatoin(editMyProfileForm)']")
	public WebElement cancelVerificationBtn;

	// ----------------------------------------- Security Question -----------------------------------------

	@FindBy(how = How.XPATH, using = "//button[@ui-sref='secure.myprofile.changesecurity']")
	public WebElement clickSecQues;

	@FindBy(how = How.XPATH, using = "//div[@class='breadcrumb ng-binding']")
	public WebElement secQuesTitle;

	@FindBy(how = How.XPATH, using = "//label[@for='questionSelect']")
	public WebElement secQuesFieldName;

	@FindBy(how = How.XPATH, using = "//label[@for='answerTextBox']")
	public WebElement answerFieldName;

	@FindBy(how = How.XPATH, using = "//input[@name='answerTextBox']")
	public WebElement clickAnswerField;

	@FindBy(how = How.XPATH, using = "//select[@name='questionSelect']")
	public WebElement defaultDrpDwnVal;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changesecurity.SECQUEREQ']")
	public WebElement secQuesRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changesecurity.ANSISREQ']")
	public WebElement answerRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changesecurity.ANSSHOULDAT2CHAR']")
	public WebElement answerMinimumMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changesecurity.ANSNOTMOR50']")
	public WebElement answerMaximumMsg;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save(vm.user,editSecuritySettingsForm)']")
	public WebElement saveAnswerBtn;

	@FindBy(how = How.XPATH, using = "//p[@ng-bind-html='vm.result1']")
	public WebElement saveSuccessMsg;
	// ----------------------------------------- End of Security Question -----------------------------------------
	// -==========================================================================================================-
	// ---------------------------------------------- Change Password ---------------------------------------------

	@FindBy(how = How.XPATH, using = "//button[@ui-sref='secure.myprofile.changepassword']")
	public WebElement changePassBtn;

	@FindBy(how = How.XPATH, using = "//div[@class='breadcrumb ng-binding']")
	public WebElement changePassTitle;

	@FindBy(how = How.XPATH, using = "//label[@for='currentPasswordTextBox']")
	public WebElement changePassLabel;

	@FindBy(how = How.XPATH, using = "//label[@for='newPasswordTextBox']")
	public WebElement newPassLabel;

	@FindBy(how = How.XPATH, using = "//label[@for='confirmPasswordTextBox']")
	public WebElement confirmPassLabel;

	@FindBy(how = How.XPATH, using = "//span[@uib-tooltip='You cannot reuse your recent 4 passwords.']")
	public WebElement informationIcon;

	@FindBy(how = How.XPATH, using = "//input[@name='currentPasswordTextBox']")
	public WebElement currentPassField;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changepassword.CURRENTPASSREQ']")
	public WebElement currentPassRequiredMsg;

	@FindBy(how = How.XPATH, using = "//input[@name='newPasswordTextBox']")
	public WebElement newPassField;

	@FindBy(how = How.XPATH, using = "//input[@name='confirmPasswordTextBox']")
	public WebElement confirmPassField;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.save(vm.changepassword,editChangePasswordForm)']")
	public WebElement saveBtnChangePass;

	@FindBy(how = How.XPATH, using = "//div[1]/p")
	public WebElement currentPassInvalid;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changepassword.NEWPASSREQ']")
	public WebElement newPassRequiredMsg;

	@FindBy(how = How.XPATH, using = "//p[@ng-if='(editChangePasswordForm.confirmPasswordTextBox.$dirty && vm.changepassword.confirmPassword != (vm.changepassword.newPassword || editChangePasswordForm.newPasswordTextBox.$viewValue)) && vm.changepassword.confirmPassword']")
	public WebElement confirmPassMismatch;

	@FindBy(how = How.XPATH, using = "//span[@ng-if='editChangePasswordForm.newPasswordTextBox.$dirty && editChangePasswordForm.newPasswordTextBox.$error.pattern']")
	public WebElement invalidMsg;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changepassword.CONFIRMPASSREQ']")
	public WebElement confirmPassRequired;

	@FindBy(how = How.XPATH, using = "//p[@translate='secure.admin.user.myprofile.changepassword.CURNEWNOTSAME']")
	public WebElement newCurrentSamePass;

	@FindBy(how = How.XPATH, using = "//button[@ng-click='vm.cancel()']")
	public WebElement cancelBtn;

	@FindBy(how = How.XPATH, using = "//*[@ui-sref='secure.myprofile.changeverificationcode']")
	public WebElement changeESignPINBtn;

	@FindBy(how = How.XPATH, using = "//*[@for='currentVerificationCodeTextBox']")
	public WebElement changeESignPINFirstLabl;

	@FindBy(how = How.XPATH, using = "//*[@for='newVerificationCodeTextBox']")
	public WebElement changeESignPINSecondLabl;

	@FindBy(how = How.XPATH, using = "//*[@for='confirmNewVerificationCodeTextBox']")
	public WebElement changeESignPINThirdLabl;

	@FindBy(how = How.XPATH, using = "//*[@name='currentVerificationCodeTextBox']")
	public WebElement currentSignTextBox;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.CUREPINREQ']")
	public WebElement currentSignTextBoxValMsg;

	@FindBy(how = How.XPATH, using = "//*[@name='newVerificationCodeTextBox']")
	public WebElement newSignTextBox;

	@FindBy(how = How.XPATH, using = "//*[@name='confirmNewVerificationCodeTextBox']")
	public WebElement confirmSignTextBox;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.save(editVerificationCodeForm)']")
	public WebElement saveBtnOfESign;

	@FindBy(how = How.XPATH, using = "(//*[@ng-bind-html='vm.result'])[2]")
	public WebElement currentSignInValSignMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.NEWPINREQ']")
	public WebElement newSignValMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.CONNEWESIREQ']")
	public WebElement confirmValMsg;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.CURNEWNOTSAME']")
	public WebElement newSignValMsgForDup;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.ESIGNMISSMATCH']")
	public WebElement confirmSignValMsgForMisMatch;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.NEWEPINAT2']")
	public WebElement newSignValMsgForMin;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.NEWEPINNOT50']")
	public WebElement newSignValMsgForMax;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.CONESIGNAT2']")
	public WebElement confirmSignValMsgForMin;

	@FindBy(how = How.XPATH, using = "//*[@translate='secure.admin.user.myprofile.changeverificationcode.CONESIGNNOT50']")
	public WebElement confirmSignValMsgForMax;

	@FindBy(how = How.XPATH, using = "//*[@class='alert alert-danger col-sm-12']")
	public WebElement generalErr;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='vm.cancel()']")
	public WebElement cancelSignBtn;




	//	------ Public Methods ------

	public void clickUsernameHomePage() throws Exception{
		Thread.sleep(2000);
		btnHelper.click(usernameTab);
	}

	public void clickMyProfileLink() throws Exception{
		log.info(clickMyProfile);
		btnHelper.click(clickMyProfile);
	}

	public void verifyMyProfileModuleName() throws Exception{
		log.info(clickMyProfile);
		Assert.assertEquals( textBoxHelper.getText(moduleNameMyProfile), ObjectRepo.reader.getModuleNameMyProfile());
	}

	public void clickMyProfileBreadcrumbs() throws Exception{
		log.info(myProfileBreadcrumbs);
		btnHelper.click(myProfileBreadcrumbs);
		//Thread.sleep(1000);
	}

	public void verifyPreviousPageOfMyProfile() throws Exception{
		log.info(moduleNameMyProfile);
		Assert.assertEquals( textBoxHelper.getText(moduleNameMyProfile), ObjectRepo.reader.getHomeLabel());
	}

	public void verifyUsernameLabel() throws Exception{
		log.info(usernameLabel);
		Assert.assertEquals( textBoxHelper.getText(usernameLabel), ObjectRepo.reader.getUsernameLabel());
	}

	public void verifyUsernameLableDisable() throws Exception{
		log.info(usernameFieldDisable);
		boolean actual =  textBoxHelper.isEnabled(usernameFieldDisable);
		Assert.assertEquals(false, actual);
	}

	public void verifyFirstNameLabel() throws Exception{
		log.info(firstnameLabel);
		String actual =  textBoxHelper.getText(firstnameLabel);
		Assert.assertEquals(actual, ObjectRepo.reader.getFirstnameLabel());
	}

	public void verifyFirstNameAndLoginSame() throws Exception{
		log.info(firstnameSameAsLogin);
		String actual = helper.getClipboardContents(firstnameSameAsLogin);
		String expected =  textBoxHelper.getText(usernameTab);
		Assert.assertEquals(actual, expected);
	}

	public void clearFirstNameField() throws Exception{
		log.info(firstnameSameAsLogin);
		textBoxHelper.clear(firstnameSameAsLogin);
		firstnameSameAsLogin.sendKeys(Keys.TAB);
	}

	public void verifyFirstNameRequiredMsg() throws Exception{
		log.info(firstnameRequiredMsg);
		Assert.assertEquals( textBoxHelper.getText(firstnameRequiredMsg), ObjectRepo.reader.getFirstnameRequiredMsg());
	}

	public void verifyFirstNameRequiredMsgColor() throws Exception{
		log.info(firstnameRequiredMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(firstnameRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void minimumFirstnameMsg(String minVal) throws Exception{
		log.info(firstnameSameAsLogin);
		btnHelper.click(firstnameSameAsLogin);
		textBoxHelper.clearAndSendKeys(firstnameSameAsLogin,minVal);
	}

	public void verifyFirstNameMinimumMsg() throws Exception{
		log.info(firstnameMinimumMsg);
		Assert.assertEquals( textBoxHelper.getText(firstnameMinimumMsg), ObjectRepo.reader.getFirstnameMinimumMsg());
	}

	public void verifyFirstNameMinimumMsgColor() throws Exception{
		log.info(firstnameMinimumMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(firstnameMinimumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void maximumFirstnameMsg(String maxVal) throws Exception{
		log.info(firstnameSameAsLogin);
		textBoxHelper.clearAndSendKeys(firstnameSameAsLogin,maxVal);
	}

	public void verifyFirstNameMaximumMsg() throws Exception{
		log.info(firstnameMaximumMsg);
		Assert.assertEquals( textBoxHelper.getText(firstnameMaximumMsg), ObjectRepo.reader.getFirstnameMaximumMsg());
	}

	public void verifyFirstNameMaximumMsgColor() throws Exception{
		log.info(firstnameMaximumMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(firstnameMaximumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyLastnameLabel() throws Exception{
		log.info(lastnameLabel);
		Assert.assertEquals( textBoxHelper.getText(lastnameLabel), ObjectRepo.reader.getLastnameLabel());
	}

	public void lastnameValidValue(String lastVal) throws Exception{
		log.info(lastnameVal);
		textBoxHelper.clearAndSendKeys(lastnameVal,lastVal);
	}

	public void lastnameEmptyValue(String lastVal) throws Exception{
		log.info(lastnameVal);
		textBoxHelper.sendKeys(lastnameVal,lastVal);
		textBoxHelper.clear(lastnameVal);
	}

	public void verifyLastnameRequiredMsg() throws Exception{
		log.info(lastnameRequiredMsg);
		Assert.assertEquals( textBoxHelper.getText(lastnameRequiredMsg), ObjectRepo.reader.getLastnameRequiredMsg());
	}

	public void verifyLastnameRequiredMsgColor() throws Exception{
		log.info(lastnameRequiredMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(lastnameRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterOneCharLastname(String minVal) throws Exception{
		log.info(lastnameVal);
		textBoxHelper.clearAndSendKeys(lastnameVal,minVal);
	}

	public void verifyLastnameMinimumMsg() throws Exception{
		log.info(lastnameMinimumMsg);
		String firstresult =  textBoxHelper.getText(lastnameMinimumMsg);
		String secondresult = ObjectRepo.reader.getLastnameMinimumMsg();
		Assert.assertEquals(firstresult, secondresult);
	}

	public void verifyLastnameMinimumMsgColor() throws Exception{
		log.info(lastnameMinimumMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(lastnameMinimumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterFiftyOneCharLastname(String maxVal) throws Exception{
		log.info(lastnameVal);
		textBoxHelper.clearAndSendKeys(lastnameVal,maxVal);
	}

	public void verifyLastnameMaximumMsg() throws Exception{
		log.info(lastnameMaximumMsg);
		String actual =  textBoxHelper.getText(lastnameMaximumMsg);
		String expected = ObjectRepo.reader.getLastnameMaximumMsg();
		Assert.assertEquals(actual, expected);
	}

	public void verifyLastnameMaximumMsgColor() throws Exception{
		log.info(lastnameMaximumMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(lastnameMaximumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyEmailLabel() throws Exception{
		log.info(emailLabel);
		Assert.assertEquals( textBoxHelper.getText(emailLabel), ObjectRepo.reader.getEmailLabel());
	}

	public void emailEmptyValue(String emailEmptyVal) throws Exception{
		log.info(emailTextfield);
		textBoxHelper.sendKeys(emailTextfield,emailEmptyVal);
		textBoxHelper.clear(emailTextfield);
		btnHelper.click(saveBtn);
	}

	public void verifyEmailAddressRequiredMsg() throws Exception{
		log.info(emailAddressRequired);
		Assert.assertEquals( textBoxHelper.getText(emailAddressRequired), ObjectRepo.reader.getEmailRequireValMsg());
	}

	public void verifyEmailAddressRequiredMsgColor() throws Exception{
		log.info(emailAddressRequired);
		Assert.assertEquals(btnHelper.FontColorCodeHex(emailAddressRequired), ObjectRepo.reader.getValMsgColor());
	}

	public void invalidEmailAddress(String emailEmptyVal) throws Exception{
		log.info(emailTextfield);
		textBoxHelper.clearAndSendKeys(emailTextfield,emailEmptyVal);
	}

	public void verifyEmailAddressInvalidMsg() throws Exception{
		log.info(emailAddressInvalid);
		Assert.assertEquals( textBoxHelper.getText(emailAddressInvalid), ObjectRepo.reader.getInvalidEmailValMsg());
	}

	public void verifyEmailAddressInvalidMsgColor() throws Exception{
		log.info(emailAddressInvalid);
		Assert.assertEquals(btnHelper.FontColorCodeHex(emailAddressInvalid), ObjectRepo.reader.getValMsgColor());
	}

	public void maximumEmailAddress(String maxVal) throws Exception{
		log.info(emailTextfield);
		textBoxHelper.clearAndSendKeys(emailTextfield,maxVal);
	}

	public void verifyEmailAddressMaximumMsg() throws Exception{
		log.info(emailAddressMaximum);
		Assert.assertEquals( textBoxHelper.getText(emailAddressMaximum), ObjectRepo.reader.getEmailAddressMaximum());
	}

	public void verifyEmailAddressMaximumMsgColor() throws Exception{
		log.info(emailAddressMaximum);
		Assert.assertEquals(btnHelper.FontColorCodeHex(emailAddressMaximum), ObjectRepo.reader.getValMsgColor());
	}


	public void enterValidFirstName(String ValidFirstName) throws Exception{
		log.info(firstnameSameAsLogin);
		textBoxHelper.clearAndSendKeys(firstnameSameAsLogin,ValidFirstName);
	}

	public void verifyFirstNameWithValidValue() throws Exception{
		log.info(firstnameMinimumMsg);
		boolean actual;
		boolean min = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.FNAMESHOULDAT2']")).size() == 0;
		boolean max = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.FNAMENOTMOR50']")).size() == 0;
		boolean required = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.FNAMEREQ']")).size() == 0;
		if(min == true && max == true && required == true )
		{
			actual = true;
		} else {
			actual= false;
		}
		Assert.assertEquals(actual,true);
	}

	public void enterValidLastName(String ValidLastName) throws Exception{
		log.info(lastnameVal);
		textBoxHelper.clearAndSendKeys(lastnameVal,ValidLastName);
	}

	public void verifyLastNameWithValidValue() throws Exception{
		log.info(lastnameVal);
		boolean actual;
		boolean min = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.LNAMESHOULDAT2']")).size() == 0;
		boolean max = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.LNAMENOTMOR50']")).size() == 0;
		boolean required = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.myprofile.LASTNAMEREQ']")).size() == 0;
		if(min == true && max == true && required == true )
		{
			actual = true;
		} else {
			actual= false;
		}
		Assert.assertEquals(actual,true);
	}

	public void uniqueEmailAddress(String uniqueEmail) throws Exception{
		log.info(emailTextfield);
		textBoxHelper.clearAndSendKeys(emailTextfield,uniqueEmail);
	}

	public void uniqueEmailAddressMsg() throws Exception{
		log.info(emailTextfield);
		Assert.assertEquals( textBoxHelper.getText(emailAddressUnique), ObjectRepo.reader.getEmailAddressUnique());
	}

	public void emailAddressVerification(String verifyEmail) throws Exception{
		log.info(emailTextfield);
		btnHelper.click(emailTextfield);
		textBoxHelper.clearAndSendKeys(emailTextfield,verifyEmail);
	}

	public void clickSaveBtn() throws Exception{
		log.info(saveBtn);
		btnHelper.click(saveBtn);
	}

	public void verifyLastVerifiedEmailMsg() throws Exception{
		log.info(verifiedEmailInfo);
		String actual =  textBoxHelper.getText(verifiedEmailInfo);
		String expected = ObjectRepo.reader.getVerifiedEmailInfo();
		Assert.assertEquals(actual, expected);
		btnHelper.click(cancelVerificationBtn);
	}

	// ----------------------------------------- Security Question ----------------------------------------- 

	public void clickSecurityQuestion() throws Exception{
		btnHelper.click(clickSecQues);
	}

	public void verifySecurityQuestionTitle() throws Exception{
		log.info(secQuesTitle);
		//Thread.sleep(1500);
		String actual =  textBoxHelper.getText(secQuesTitle);
		Assert.assertEquals(actual, ObjectRepo.reader.getSecQuesTitle());
	}

	public void verifySecurityQuestionFieldName() throws Exception{
		log.info(secQuesFieldName);
		Assert.assertEquals( textBoxHelper.getText(secQuesFieldName), ObjectRepo.reader.getSecQuesFieldName());
	}

	public void verifySecurityQuestionAnswer() throws Exception{
		log.info(answerFieldName);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(answerFieldName),  ObjectRepo.reader.getAnswerFieldName());
	}

	public void selectDefaultValInDrpDwn() throws Exception{
		log.info(defaultDrpDwnVal);
		//Thread.sleep(1500);--Select Security Question--
		drpdwnHelper.selectDropDownIndex(defaultDrpDwnVal, 0);
	}

	public void verifyRequiredMsgSecQuesDrpDwn() throws Exception{
		log.info(secQuesRequiredMsg);
		Assert.assertEquals( textBoxHelper.getText(secQuesRequiredMsg), ObjectRepo.reader.getSecQuesRequiredMsg());
	}

	public void verifyRequiredMsgSecQuesDrpDwnColor() throws Exception{
		log.info(secQuesRequiredMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(secQuesRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void clickAnswerTextbox() throws Exception{
		log.info(clickAnswerField);
		//Thread.sleep(1000);
		btnHelper.click(clickAnswerField);
	}

	public void clickAnswerTextboxTab() throws Exception{
		log.info(clickAnswerField);
		//Thread.sleep(500);
		clickAnswerField.sendKeys(Keys.TAB);
	}

	public void verifyAnswerRequiredMsg() throws Exception{
		log.info(answerRequiredMsg);
		//Thread.sleep(1500);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(answerRequiredMsg), ObjectRepo.reader.getAnswerRequiredMsg());
	}

	public void verifyAnswerRequiredMsgColor() throws Exception{
		log.info(answerRequiredMsg);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.FontColorCodeHex(answerRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterOneCharAnswer(String minAnswer) throws Exception{
		log.info(clickAnswerField);
		textBoxHelper.clearAndSendKeys(clickAnswerField,minAnswer);
	}

	public void verifyAnswerMinimumMsg() throws Exception{
		log.info(answerMinimumMsg);
		//Thread.sleep(1500);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(answerMinimumMsg), ObjectRepo.reader.getAnswerMinimumMsg());
	}

	public void verifyAnswerMinimumMsgColor() throws Exception{
		log.info(answerMinimumMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(answerMinimumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterFiftyOneCharAnswer(String maxAnswer) throws Exception{
		log.info(clickAnswerField);
		 textBoxHelper.clearAndSendKeys(clickAnswerField,maxAnswer);
	}

	public void verifyAnswerMaximumMsg() throws Exception{
		log.info(answerMaximumMsg);
		//Thread.sleep(1500);
		Assert.assertEquals( textBoxHelper.getText(answerMaximumMsg), ObjectRepo.reader.getAnswerMaximumMsg());
	}

	public void verifyAnswerMaximumMsgColor() throws Exception{
		log.info(answerMaximumMsg);
		//Thread.sleep(1000);
		Assert.assertEquals(btnHelper.FontColorCodeHex(answerMaximumMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterValidAnswer(String validAnswer) throws Exception{
		log.info(clickAnswerField);
		 textBoxHelper.sendKeys(clickAnswerField,validAnswer);
		//Thread.sleep(1000);
	}

	public void verifyAnswerWithValidValue() throws Exception{
		log.info(clickAnswerField);
		boolean actual;
		boolean min = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.changesecurity.ANSSHOULDAT2CHAR']")).size() == 0;
		boolean max = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.changesecurity.ANSNOTMOR50']")).size() == 0;
		boolean required = driver.findElements(By.xpath("//p[@translate='secure.admin.user.myprofile.changesecurity.ANSISREQ']")).size() == 0;
		if(min == true && max == true && required == true )
		{
			actual = true;
		} else {
			actual= false;
		}
		Assert.assertEquals(actual,true);
	}

	public void clickAnswersaveBtn() throws Exception{
		log.info(saveAnswerBtn);
		 btnHelper.click(saveAnswerBtn);
		//Thread.sleep(1000);
	}

	public void verifySaveSuccessMsgColor() throws Exception{
		log.info(saveSuccessMsg);
		//Thread.sleep(500);
		Assert.assertEquals(btnHelper.FontColorCodeHex(saveSuccessMsg), ObjectRepo.reader.getSaveSuccessMsgcolor());
	}

	public void verifySaveSuccessMsg() throws Exception{
		log.info(saveSuccessMsg);
		Assert.assertEquals( textBoxHelper.getText(saveSuccessMsg), ObjectRepo.reader.getSaveSuccessMsg());
	}

	// ----------------------------------------- Change Password -----------------------------------------

	public void clickChangePassBtn() throws Exception{
		//Thread.sleep(500);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", changePassBtn);
		//Thread.sleep(500);
	}

	public void verifyChangePassTitle() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(changePassTitle), ObjectRepo.reader.getChangePassTitle());
	}

	public void verifyChangePassLabel() throws Exception{
		log.info(changePassLabel);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(changePassLabel), ObjectRepo.reader.getChangePassLabel());
	}

	public void verifyNewPassLabel() throws Exception{
		log.info(newPassLabel);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(newPassLabel), ObjectRepo.reader.getNewPassLabel());
	}

	public void verifyConfirmPassLabel() throws Exception{
		log.info(confirmPassLabel);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(confirmPassLabel), ObjectRepo.reader.getConfirmPassLabel());
	}

	public void clickInformationIcon() throws Exception{
		log.info(informationIcon);
		informationIcon.getAttribute("uib-tooltip");
	}

	public void verifyInformationIconMsg() throws Exception{
		log.info(informationIcon);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(informationIcon.getAttribute("uib-tooltip"), ObjectRepo.reader.getInformationIcon());
	}

	public void clickCurrentPass(String emptyVal) throws Exception{
		log.info(currentPassField);
		 textBoxHelper.sendKeys(currentPassField,emptyVal);
		textBoxHelper.clear(currentPassField);
	}

	public void verifyCurrentPassRequiredMsg() throws Exception{
		log.info(currentPassRequiredMsg);
		Assert.assertEquals( textBoxHelper.getText(currentPassRequiredMsg), ObjectRepo.reader.getCurrentPassRequiredMsg());
	}

	public void verifyCurrentPassRequiredMsgColor() throws Exception{
		log.info(currentPassRequiredMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(currentPassRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterInvalidCurrentPass(String invalidPass) throws Exception{
		log.info(currentPassField);
		 textBoxHelper.sendKeys(currentPassField,invalidPass);
	}

	public void clickNewPassField() throws Exception{
		log.info(newPassField);
		 btnHelper.click(newPassField);
		newPassField.sendKeys(Keys.TAB);
	}

	public void enterPassInNewPassField(String invalidPass) throws Exception{
		log.info(newPassField);
		 textBoxHelper.sendKeys(newPassField,invalidPass);
	}

	public void enterPassInConfirmPassField(String invalidPass) throws Exception{
		log.info(confirmPassField);
		 textBoxHelper.sendKeys(confirmPassField,invalidPass);
	}

	public void clickConfirmPassField() throws Exception{
		log.info(confirmPassField);
		 btnHelper.click(confirmPassField);
	}

	public void clickChangePassSaveBtn() throws Exception{
		log.info(saveBtnChangePass);
		 btnHelper.click(saveBtnChangePass);
		//Thread.sleep(500);
	}

	public void verifyInvalidCurrentPassMsg() throws Exception{
		log.info(currentPassInvalid);
		Assert.assertEquals( textBoxHelper.getText(currentPassInvalid), ObjectRepo.reader.getCurrentPassInvalid());
	}

	public void verifyInvalidCurrentPassMsgColor() throws Exception{
		log.info(currentPassInvalid);
		Assert.assertEquals(btnHelper.FontColorCodeHex(currentPassInvalid),  ObjectRepo.reader.getCurrentPassInvalidColor());
	}

	public void verifyNewPassRequiredMsg() throws Exception{
		log.info(newPassRequiredMsg);
		Assert.assertEquals( textBoxHelper.getText(newPassRequiredMsg), ObjectRepo.reader.getNewPasswordManValMsg());
	}

	public void verifyNewPassRequiredMsgColor() throws Exception{
		log.info(newPassRequiredMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(newPassRequiredMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterValidCurrentPass(String validCurrentPass) throws Exception{
		log.info(currentPassField);
		 textBoxHelper.sendKeys(currentPassField,validCurrentPass);
	}

	public void enterInvalidConfirmPass(String invalidConfirmPass) throws Exception{
		log.info(confirmPassField);
		 textBoxHelper.sendKeys(confirmPassField,invalidConfirmPass);
		confirmPassField.sendKeys(Keys.TAB);
	}

	public void verifyConfirmPassSameMsg() throws Exception{
		log.info(confirmPassMismatch);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(confirmPassMismatch), ObjectRepo.reader.getConfirmPasswordMsg());
	}

	public void verifyConfirmPassSameMsgColor() throws Exception{
		log.info(confirmPassMismatch);
		Assert.assertEquals(btnHelper.FontColorCodeHex(confirmPassMismatch), ObjectRepo.reader.getValMsgColor());
	}

	public void enterAlphabetsCharacters(String alphabetsVal) throws Exception{
		log.info(newPassField);
		 textBoxHelper.sendKeys(newPassField,alphabetsVal);
		//		newPassField textBoxHelper.sendKeys(Keys.TAB);
	}

	public void verifyInvalidNewPassMsg() throws Exception{
		log.info(invalidMsg);
		Assert.assertEquals( textBoxHelper.getText(invalidMsg), ObjectRepo.reader.getInvalidMsg());
	}

	public void verifyInvalidNewPassMsgColor() throws Exception{
		log.info(invalidMsg);
		Assert.assertEquals(btnHelper.FontColorCodeHex(invalidMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyConfirmPassRequiredMsg() throws Exception{
		log.info(confirmPassRequired);
		confirmPassField.sendKeys(Keys.TAB);
		Assert.assertEquals( textBoxHelper.getText(confirmPassRequired), ObjectRepo.reader.getResetPageConfirmPwdMandValmsg());
	}

	public void verifyConfirmPassRequiredMsgColor() throws Exception{
		log.info(confirmPassRequired);
		confirmPassField.sendKeys(Keys.TAB);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.FontColorCodeHex(confirmPassRequired), ObjectRepo.reader.getValMsgColor());
	}

	public void enterCurrentPass(String sameAsNew) throws Exception{
		log.info(currentPassField);
		 textBoxHelper.sendKeys(currentPassField,sameAsNew);
	}

	public void enterNewPass(String sameAsCurrent) throws Exception{
		log.info(newPassField);
		 textBoxHelper.sendKeys(newPassField,sameAsCurrent);
	}

	public void verifyNewCurrentSamePassMsg() throws Exception{
		log.info(newCurrentSamePass);
		Assert.assertEquals( textBoxHelper.getText(newCurrentSamePass), ObjectRepo.reader.getNewCurrentSamePass());
	}

	public void verifyNewCurrentSamePassMsgColor() throws Exception{
		log.info(newCurrentSamePass);
		Assert.assertEquals(btnHelper.FontColorCodeHex(newCurrentSamePass), ObjectRepo.reader.getValMsgColor());
	}

	public void clickCancelBtnChangePass() throws Exception{
		log.info(cancelBtn);
		 btnHelper.click(cancelBtn);
	}

	public void verifyCancelBtnFunctionality() throws Exception{
		log.info(cancelBtn);
		//Thread.sleep(1000);
		if(driver.findElements(By.xpath("//div[@class='breadcrumb ng-binding']")).size() == 0)
		{
			Assert.assertEquals(true, true);
		}
		else
		{
			Assert.assertEquals(false, true);
		}
	}

	// ----------------------------------------- E-Signature PIN -----------------------------------------

	public void clickChangeESignPIN() throws Exception{
		//Thread.sleep(2500);
		 btnHelper.click(changeESignPINBtn);
	}

	public void verifyChangeESignPINFun() throws Exception{
		log.info(changeESignPINBtn);
		new Scroll().scrollDown(ObjectRepo.driver);
		if(driver.findElements(By.xpath("//*[@name='editVerificationCodeForm']")).size() != 0)
		{
			Assert.assertEquals(true, true);
		}
		else
		{
			Assert.assertEquals(false, true);
		}
	}

	public void verifyFirstLablOfESignPIN() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(changeESignPINFirstLabl), ObjectRepo.reader.getChangeESignPINFirstLabl());
	}

	public void verifySecondLablOfESignPIN() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(changeESignPINSecondLabl), ObjectRepo.reader.getChangeESignPINSecondLabl());
	}

	public void verifyThirdLablOfESignPIN() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(changeESignPINThirdLabl), ObjectRepo.reader.getChangeESignPINThirdLabl());
	}

	public void clickFirstTextBoxOfESignPIN() throws Exception {
		 btnHelper.click(currentSignTextBox);
		//Thread.sleep(500);
	}

	public void verifyFirstESignPINValMsg() throws Exception{
		currentSignTextBox.sendKeys(Keys.TAB);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(currentSignTextBoxValMsg), ObjectRepo.reader.getCurrentSignTextBoxValMsg());
	}

	public void verifyFirstESignPINValMsgColor() throws Exception{
		currentSignTextBox.sendKeys(Keys.TAB);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.FontColorCodeHex(currentSignTextBoxValMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void enterCurrentESignPIN(String signature) throws Exception{
		 textBoxHelper.sendKeys(currentSignTextBox,signature);
	}

	public void clickNewSignTextBox() throws Exception {
		 btnHelper.click(newSignTextBox);
		//Thread.sleep(500);
	}

	public void enterNewSignPIN(String signature) throws Exception{
		 textBoxHelper.sendKeys(newSignTextBox,signature);
	}

	public void clickConfirmSignTextBox() throws Exception {
		 btnHelper.click(confirmSignTextBox);
		//Thread.sleep(500);
	}

	public void enterConfirmSignPIN(String signature) throws Exception{
		 textBoxHelper.sendKeys(confirmSignTextBox,signature);
	}

	public void clickSaveBtnOfESign() throws Exception {
		 btnHelper.click(saveBtnOfESign);
		//Thread.sleep(500);
	}

	public void verifyCurrentESignInValData() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(currentSignInValSignMsg), ObjectRepo.reader.getCurrentSignInValSignMsg());
	}

	public void verifyCurrentESignInValDataColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.FontColorCodeHex(currentSignInValSignMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyNewESignPINValMsg() throws Exception{
		newSignTextBox.sendKeys(Keys.TAB);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(newSignValMsg), ObjectRepo.reader.getNewSignValMsg());
	}

	public void verifyNewESignPINValMsgColor() throws Exception{
		newSignTextBox.sendKeys(Keys.TAB);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.FontColorCodeHex(newSignValMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyConfirmESignPINValMsg() throws Exception{
		confirmSignTextBox.sendKeys(Keys.TAB);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(confirmValMsg), ObjectRepo.reader.getConfirmValMsg());
	}

	public void verifyConfirmESignPINValMsgColor() throws Exception{
		confirmSignTextBox.sendKeys(Keys.TAB);
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.FontColorCodeHex(confirmValMsg), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyNewSignValMsgForDup() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(newSignValMsgForDup),ObjectRepo.reader.getNewSignValMsgForDup());
	}

	public void verifyNewSignValMsgForDupColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.FontColorCodeHex(newSignValMsgForDup), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyConfirmSignValMsgForMisMatch() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(confirmSignValMsgForMisMatch), ObjectRepo.reader.getConfirmSignValMsgForMisMatch());
	}

	public void verifyConfirmSignValMsgForMisMatchColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.FontColorCodeHex(confirmSignValMsgForMisMatch), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyNewSignValMsgForMin() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(newSignValMsgForMin), ObjectRepo.reader.getNewSignValMsgForMin());
	}

	public void verifyNewSignValMsgForMinColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.FontColorCodeHex(newSignValMsgForMin), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyNewSignValMsgForMax() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(newSignValMsgForMax), ObjectRepo.reader.getNewSignValMsgForMax());
	}

	public void verifyNewSignValMsgForMaxColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.FontColorCodeHex(newSignValMsgForMax), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyConfirmSignValMsgForMin() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(confirmSignValMsgForMin), ObjectRepo.reader.getConfirmSignValMsgForMin());
	}

	public void verifyConfirmSignValMsgForMinColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.FontColorCodeHex(confirmSignValMsgForMin), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyConfirmSignValMsgForMax() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(confirmSignValMsgForMax), ObjectRepo.reader.getConfirmSignValMsgForMax());
	}

	public void verifyConfirmSignValMsgForMaxColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.FontColorCodeHex(confirmSignValMsgForMax), ObjectRepo.reader.getValMsgColor());
	}

	public void verifyGeneralError() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals( textBoxHelper.getText(generalErr), ObjectRepo.reader.getGeneralErr());
	}

	public void verifyGeneralErrorColor() throws Exception{
		new Scroll().scrollDown(ObjectRepo.driver);
		Assert.assertEquals(btnHelper.FontColorCodeHex(generalErr), ObjectRepo.reader.getValMsgColor());
	}

	public void clickCancelBtnChangeSign() throws Exception{
		log.info(cancelSignBtn);
		 btnHelper.click(cancelSignBtn);
	}

}
