/**
 * rsr 
 *
 *Aug 6, 2016
 */
package com.cucumber.framework.configuration.browser;

/**
 * @author vishal bhut
 */
public enum BrowserType {
	Firefox,
	Chrome,
	Iexplorer,
	PhantomJs,
	HtmlUnitDriver

}
