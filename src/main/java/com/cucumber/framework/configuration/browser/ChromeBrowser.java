/**
 * rsr 
 *
 *Aug 5, 2016
 */
package com.cucumber.framework.configuration.browser;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.cucumber.framework.utility.DateTimeHelper;
import com.cucumber.framework.utility.ResourceHelper;
import com.cucumber.framework.utility.Utility;

/**
 * @author vishal bhut
 */
public class ChromeBrowser 
{

	public Capabilities getChromeCapabilities() 
	{
		ChromeOptions option = new ChromeOptions();
		option.addArguments("--no-sandbox"); // Bypass OS security model
		option.addArguments("--start-maximized"); // open Browser in maximized mode
		option.addArguments("disable-infobars"); // disabling infobars
		option.addArguments("--disable-extensions"); // disabling extensions
		option.addArguments("--disable-gpu"); // applicable to windows os only
		option.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
	//	option.addArguments("--incognito");//for incognito mode 
//		option.addArguments("window-size=1024,768"); 
		option.addArguments("--headless"); 
		DesiredCapabilities chrome = DesiredCapabilities.chrome();
		chrome.setJavascriptEnabled(true);
		chrome.setCapability(ChromeOptions.CAPABILITY, option);
		return chrome;
	}
	
	public WebDriver getChromeDriver(Capabilities cap) 
	{
		//System.setProperty("webdriver.chrome.driver",
			//	ResourceHelper.getResourcePath("driver/chromedriver.exe"));
		System.setProperty("webdriver.chrome.driver",Utility.ChromeDriver);
		System.setProperty("webdriver.chrome.logfile",
				ResourceHelper.getResourcePath("logs/chromelogs/")
						+ "chromelog" + DateTimeHelper.getCurrentDateTime()
						+ ".log");
		return new ChromeDriver(cap);
	}
	
	public WebDriver getChromeDriver(String hubUrl,Capabilities cap) throws MalformedURLException 
	{
		return new RemoteWebDriver(new URL(hubUrl), cap);
	}

}
