package com.cucumber.framework.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
/**
 * @author vishal bhut
 */
public class ExcelUtils {
	
	public static Workbook ExcelWorkbook = null;
	public static Sheet ExcelSheet = null;
	public static HSSFSheet sheet = null;
	public static Row Excel_row;
	public static Cell Excel_cell;
	public static FileInputStream inputStream;
	public static FileOutputStream fileOut;
	public static int rowCount, colCount;

	// This method is to set the File path and to open the Excel file, Pass Excel
	// Path and Sheetname as Arguments to this method

	public void setExcelFile(String Path, String SheetName) throws Exception {

		try {
			// Create an object of File class to open xlsx file
			File file = new File(Constant.Path_TestData + Constant.File_TestData);

			// Create an object of FileInputStream class to read excel file
			inputStream = new FileInputStream(file);

			// Find the file extension by splitting file name in substring and getting only
			// extension name
			String fileExtensionName = Constant.File_TestData.substring(Constant.File_TestData.indexOf("."));

			// Check condition if the file is xlsx file
			if (fileExtensionName.equals(".xlsx")) {
				// If it is xlsx file then create object of XSSFWorkbook class
				ExcelWorkbook = new XSSFWorkbook(inputStream);
			}
			// Check condition if the file is xls file
			else if (fileExtensionName.equals(".xls")) {

				// If it is xls file then create object of XSSFWorkbook class

				ExcelWorkbook = new HSSFWorkbook(inputStream);
			}
			// Read sheet inside the workbook by its name
			ExcelSheet = ExcelWorkbook.getSheet(SheetName);
			rowCount = ExcelSheet.getLastRowNum();
			colCount = ExcelSheet.getRow(0).getLastCellNum();

		} catch (Exception e) {
			throw (e);
		}
	}

	public static String[][] excelread() throws Exception {

		/*
		 * FileInputStream ExcelFile = new FileInputStream(Path); ExcelWBook = new
		 * XSSFWorkbook(ExcelFile); ExcelWSheet = ExcelWBook.getSheet("Input") ;
		 */

		int rowNum, colNum;
		rowNum = ExcelSheet.getLastRowNum() + 1;
		colNum = ExcelSheet.getRow(0).getLastCellNum();
		String[][] data = new String[rowNum][colNum];

		for (int i = 1; i < rowNum; i++) {
			Excel_row = ExcelSheet.getRow(i);
			for (int j = 0; j < colNum; j++) {
				Excel_cell = Excel_row.getCell(j);
				if (Excel_cell != null) {
					String value = cellToString(Excel_cell);
					data[i][j] = value;
				}
			}
		}
		return data;
	}

	public static int getLastRowNum() throws Exception {

		// created by santosh
		// Desc: it's return a Last Row Number.

		/*
		 * FileInputStream ExcelFile = new FileInputStream(Path); ExcelWBook = new
		 * XSSFWorkbook(ExcelFile); ExcelWSheet = ExcelWBook.getSheet("Input") ;
		 */
		int rowNum, colNum;
		rowNum = ExcelSheet.getLastRowNum() + 1;
		colNum = ExcelSheet.getRow(0).getLastCellNum();
		System.err.println("\n get last row:" + rowNum + "\n last col: " + colNum);
		return rowNum;
	}

	public static String cellToString(Cell Ex_cell) {

		int type;
		Object result;
		type = Ex_cell.getCellType();

		switch (type) {

		case 0: // numeric value in Excel
			result = Ex_cell.getNumericCellValue();
			break;
		case 1: // String Value in Excel
			result = Ex_cell.getStringCellValue();
			break;
		case 3:
			result = Ex_cell.getStringCellValue();
			break;
		default:
			throw new RuntimeException("There are no support for this type of cell");
		}

		return result.toString();
	}

	// This method is to write in the Excel cell, Row num and Col num are the
	// parameters

	public static void setCellData(String Result, int row, int col) throws Exception {
		// Access the required test data sheet
		try {
			// String str=String.valueOf(data1);
			// int row= Integer.parseInt(str);
			Excel_row = ExcelSheet.getRow(row);
			// String str1 = String.valueOf(data2);
			// int cell=Integer.parseInt(str1);
			Excel_cell = Excel_row.getCell(col, Row.RETURN_BLANK_AS_NULL);

			if (Excel_cell == null) {
				Excel_cell = Excel_row.createCell(col);
				Excel_cell.setCellValue(Result);
				System.out.println("Row:" + row + "||" + "Col:" + col + "||" + "Result:" + Result);

			} else {
				Excel_cell.setCellValue(Result);
				System.out.println("Inside else" + "||" + "Row:" + row + "||" + "Col:" + col + "||" + "Result:" + Result);
			}
			inputStream.close();
			// Constant variables Test Data path and Test Data file name

			fileOut = new FileOutputStream(Constant.Path_TestData + Constant.File_TestData);
			ExcelWorkbook.write(fileOut);
			fileOut.close();
			// ExcelWorkbook = new XSSFWorkbook(new FileInputStream(Constant.Path_TestData +
			// Constant.File_TestData));

		} catch (Exception e) {

			throw (e);
		}
	}

	public static String readXLSFile(String Sheet, int rowVal, int colVal) throws IOException {
		
		String User_Val = null;
		DataFormatter formatter = new DataFormatter();

		for (int i = 0; i < ExcelWorkbook.getNumberOfSheets(); i++) {
			String CurrentSheet = ExcelWorkbook.getSheetName(i);
			if (CurrentSheet.equalsIgnoreCase(Sheet)) {
				ExcelSheet = (HSSFSheet) ExcelWorkbook.getSheetAt(i);
				System.out.println(CurrentSheet);
				User_Val = formatter.formatCellValue(ExcelSheet.getRow(rowVal).getCell(colVal));

				// User_Val = sheet.getRow(rowVal).getCell(colVal).getStringCellValue();
				System.out.println(User_Val);
				// rowCount = ExcelSheet.getLastRowNum();
				// colCount = ExcelSheet.getRow(0).getLastCellNum();
			}
		}
		inputStream.close();
		return User_Val;
	}
	
	public void fileOut(String sendAppName) throws Exception {
		 FileInputStream fis = new FileInputStream("../perfeqta-cucumber/src/main/resources/exceldata/inputData.xls");
		 XSSFWorkbook workbook = new XSSFWorkbook(fis);
		 XSSFSheet sheet = workbook.getSheet("TestData");
		 Row row = sheet.createRow(1);
		 Cell cell = row.createCell(1);
		 cell.setCellType(cell.CELL_TYPE_STRING);
		 cell.setCellValue(sendAppName);
		 FileOutputStream fos = new FileOutputStream("../perfeqta-cucumber/src/main/resources/exceldata/inputData.xls");
		 workbook.write(fos);
		 fos.close();
		 System.out.println("END OF WRITING DATA IN EXCEL");
	}
}
