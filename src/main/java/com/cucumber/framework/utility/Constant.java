package com.cucumber.framework.utility;
/**
 * @author vishal bhut
 */
public class Constant {

	public static final String URL = "https://test1.beperfeqta.com/mdav33";

	public static final String Username = "vishal";

	public static final String Password = "Admin@123";

	//use this when want to run project on jenkins.
//	public static final String Path_TestData = "src/main/resources/exceldata/";
	
	public static final String Path_TestData = "../perfeqta-cucumber/src/main/resources/exceldata/";

	public static final String File_TestData = "inputData.xls";

	public static final String Extent_Report_Path = "/home/protl-unnati/Desktop/Unnati_Main/Other_Task/Automation New Structure/beperfeqta/Extent_Reports/";

	public static final String Extent_ReportHTML_Path = "ExtentReportFile.html";

	public static final String ExtentReportXML_Path = "extent-config.xml";
}

