/**
 * rsr 
 *
 *Aug 6, 2016
 */
package com.cucumber.framework.configreader;

import java.util.Properties;

import org.apache.log4j.Level;

import com.cucumber.framework.configuration.browser.BrowserType;
import com.cucumber.framework.interfaces.IconfigReader;
import com.cucumber.framework.utility.ResourceHelper;


/**
 * @author vishal bhut
 */
public class PropertyFileReader implements IconfigReader
{
	
	private Properties prop = null;

	public PropertyFileReader() {
		prop = new Properties();
		try {
			prop.load(ResourceHelper
					.getResourcePathInputStream("configfile/"
							+ "config.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*public PropertyFileReader(String fileName) {

		prop = new Properties();
		try {
			prop.load(ResourceHelper
					.getResourcePathInputStream("configfile/"
							+ fileName));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public String getUserName() {
		return prop.getProperty("https://google.com");
	}

	public String getPassword() {
		return prop.getProperty("Password");
	}

	public String getWebsite() {
		return prop.getProperty("Website");
	}

	public int getPageLoadTimeOut() {
		return Integer.parseInt(prop.getProperty("PageLoadTimeOut"));
	}

	public int getImplicitWait() {
		return Integer.parseInt(prop.getProperty("ImplcitWait"));
	}

	public int getExplicitWait() {
		return Integer.parseInt(prop.getProperty("ExplicitWait"));
	}
	
	public String getDbType() {
		return prop.getProperty("DataBase.Type");
	}

	public String getDbConnStr() {
		return prop.getProperty("DtaBase.ConnectionStr");
	}

	public BrowserType getBrowser() {
		return BrowserType.valueOf(prop.getProperty("Browser"));
	}
	
	// added by @santosh
	public int getTimeOutInSeconds() {
		return Integer.parseInt(prop.getProperty("timeOutInSeconds"));
	}
	public int getPollingEveryInMiliSec() {
		return Integer.parseInt(prop.getProperty("pollingEveryInMiliSec"));
	}
	
	public Level getLoggerLevel() {
		
		switch (prop.getProperty("Logger.Level")) {
		
		case "DEBUG":
			return Level.DEBUG;
		case "INFO":
			return Level.INFO;
		case "WARN":
			return Level.WARN;
		case "ERROR":
			return Level.ERROR;
		case "FATAL":
			return Level.FATAL;
		}
		return Level.ALL;
	}

	// Added By @Siddharth
	// Common Function
	public String getBrightBlueColor() {
		return prop.getProperty("color.BrightBlue");
	}
	
	public String getBlueColor() {
		return prop.getProperty("color.Blue");
	}
	
	public String getLightGreenColor() {
		return prop.getProperty("color.LightGreen");
	}
	
	public String getDarkGreyColor() {
		return prop.getProperty("btnColor.DarkGrey");
	}

	public String getValMsgColor() {
		return prop.getProperty("valMsgColor.StrongRed");
	}
	
	public String getRemoveMsgColor() {
		return prop.getProperty("removeMsg.Red");
	}
	
	public String getRemoveIconColor() {
		return prop.getProperty("removeIcon.Black");
	}
	
	public String getNavigationColor() {
		return prop.getProperty("navigationColor.PakistanGreen");
	}
	
	public String getEmailInfoColor() {
		return prop.getProperty("emailInfoColor.VividNavy");
	}
	
	public String getNoRecordFoundMsg() {
		return prop.getProperty("noRecordFoundMsg");
	}
	
	public String getFavNameRequireValMsg() {
		return prop.getProperty("favNameRequireValMsg");
	}
	
	public String getFavNameMinValMsg() {
		return prop.getProperty("favNameMinValMsg");
	}
	
	public String getFavNameMaxValMsg() {
		return prop.getProperty("favNameMaxValMsg");
	}
	
	public String getFavNameUniqueValMsg() {
		return prop.getProperty("favNameUniqueValMsg");
	}
	
	public String getQuestionMark() {
		return prop.getProperty("questionMark");
	}
	
	public String getRecordStatusText() {
		return prop.getProperty("recordStatusText");
	}
	
	public String getMonthRequireValMsg() {
		return prop.getProperty("monthRequireValMsg");
	}
	
	public String getYearRequiredValMsg() {
		return prop.getProperty("yearRequiredValMsg");
	}
	
	public String getAppRequireValMsg() {
		return prop.getProperty("appRequireValMsg");
	}
	
	public String getEmailRequireValMsg() {
		return prop.getProperty("emailRequireValMsg");
	}
	
	public String getInvalidEmailValMsg() {
		return prop.getProperty("invalidEmailValMsg");
	}
	
	public String getHoursRequireValMsg() {
		return prop.getProperty("hoursRequireValMsg");
	}
	
	public String getHomeLabel() {
		return prop.getProperty("homeLabel");
	}
	
	public String getUsernameRequireValMsg() {
		return prop.getProperty("usernameRequireValMsg");
	}
	
	public String getUsernameMinValMsg() {
		return prop.getProperty("usernameMinValMsg");
	}
	
	public String getUsernameMaxValMsg() {
		return prop.getProperty("usernameMaxValMsg");
	}
	
	public String getAdministrationTile() {
		return prop.getProperty("administrationTile");
	}
	
	public String getViewAuditTrailText() {
		return prop.getProperty("viewAuditTrailText");
	}
	
	public String getInvalidNumberValMsg() {
		return prop.getProperty("invalidNumberValMsg");
	}
	//Advance Search
	public String getAdvSearchLabel() {
		return prop.getProperty("advSearchLabel");
	}
	
	public String getNoAppsFoundDrpdwn() {
		return prop.getProperty("NoAppsFoundDrpdwn");
	}
	
	public String getRecordsPendingYourReview() {
		return prop.getProperty("recordsPendingYourReview");
	}
	
	public String getRecordsYouAccessedToday() {
		return prop.getProperty("recordsYouAccessedToday");
	}
	
	public String getRecordsAssignedToYou() {
		return prop.getProperty("recordsAssignedToYou");
	}
	
	public String getRecordsYouCreatedToday() {
		return prop.getProperty("recordsYouCreatedToday");
	}

	public String getAddToFavoriteLabel() {
		return prop.getProperty("addToFavoriteLabel");
	}
	
	public String getRemoveMsgMyFavorite() {
		return prop.getProperty("removeMsgMyFavorite");
	}
	
	public String getFilterByLabel() {
		return prop.getProperty("filterByLabel");
	}
	
	public String getFilterByIcons() {
		return prop.getProperty("filterByIcons");
	}
	
	public String getDateRangeSpelling() {
		return prop.getProperty("dateRangeSpelling");
	}
	
	public String getAttributesEntitiesSpelling() {
		return prop.getProperty("attributesEntitiesSpelling");
	}
	
	public String getSitesSpelling() {
		return prop.getProperty("sitesSpelling");
	}
	
	public String getAssigneeSpelling() {
		return prop.getProperty("assigneeSpelling");
	}
	
	public String getDateDrpDwnFilterBy() {
		return prop.getProperty("dateDrpDwnFilterBy");
	}

	public String getDateDrpDwnForWeek() {
		return prop.getProperty("dateDrpDwnForWeek");
	}

	public String getDateDrpDwnForMonth() {
		return prop.getProperty("dateDrpDwnForMonth");
	}
	
	public String getValueRequiredMsg() {
		return prop.getProperty("valueRequiredMsg");
	}
	
	public String getAttributeDrpDwn() {
		return prop.getProperty("attributeDrpDwn");
	}

	public String getSitesRegionDrpDwnForSelect() {
		return prop.getProperty("sitesRegionDrpDwnForSelect");
	}
	
	public String getSitesRegionDrpDwn() {
		return prop.getProperty("sitesRegionDrpDwn");
	}
	
	public String getBatchEntriesSpelling() {
		return prop.getProperty("batchEntriesSpelling");
	}
	
	public String getBatchEntryMaximumMsg() {
		return prop.getProperty("batchEntryMaximumMsg");
	}
	
	// App Builder 
	public String getPageTxtForAppBuildLabel() {
		return prop.getProperty("pageTxtLabelForAppBuild");
	}
	
	public String getCopyPopupDefaultValue() {
		return prop.getProperty("CopyPopupDefaultValue");
	}
	
	public String getPageTxtLabelForBuildApp() {
		return prop.getProperty("pageTxtLabelForBuildApp");
	}
	
	public String getBasicTabHeader() {
		return prop.getProperty("basicTabHeader");
	}
	
	public String getAttributeTabHeader() {
		return prop.getProperty("attributeTabHeader");
	}
	
	public String getWorkflowTabHeader() {
		return prop.getProperty("workflowTabHeader");
	}
	
	public String getAcceptanceTabHeader() {
		return prop.getProperty("acceptanceTabHeader");
	}
	
	public String getPreviewTabHeader() {
		return prop.getProperty("previewTabHeader");
	}
	
	public String getPermissionsTabHeader() {
		return prop.getProperty("permissionsTabHeader");
	}
	
	public String getAppDetailsLabel() {
		return prop.getProperty("appDetailsLabel");
	}
	
	public String getCurrntVersion() {
		return prop.getProperty("currntVersion");
	}
	
	public String getcurrntVersioncolor() {
		return prop.getProperty("currntVersion.PacificBluecolor");
	}
	
	public String getBasicTabLables() {
		return prop.getProperty("BasicTabLables");
	}
	
	public String getDisabledTabBasic() {
		return prop.getProperty("DisabledTabBasic");
	}
	
	public String getDisabledTabAttribute() {
		return prop.getProperty("DisabledTabAttribute");
	}
	
	public String getDisabledTabWorkflow() {
		return prop.getProperty("DisabledTabWorkflow");
	}
	
	public String getAppMdulReqMsg() {
		return prop.getProperty("appMdulReqMsg");
	}
	
	public String getCancelAlertPopupMsg() {
		return prop.getProperty("cancelAlertPopupMsg");
	}
	
	public String getAppTitleTxtBoxReqMsg() {
		return prop.getProperty("appTitleTxtBoxReqMsg");
	}
	
	public String getAppTitleTxtBoxMinMsg() {
		return prop.getProperty("appTitleTxtBoxMinMsg");
	}
	
	public String getAppTitleTxtBoxMaxMsg() {
		return prop.getProperty("appTitleTxtBoxMaxMsg");
	}
	
	public String getAppDescptTxtBoxMinMsg() {
		return prop.getProperty("appDescptTxtBoxMinMsg");
	}
	
	public String getAppDescptTxtBoxMaxMsg() {
		return prop.getProperty("appDescptTxtBoxMaxMsg");
	}
	
	public String getInvalidURLValMsg() {
		return prop.getProperty("invalidURLValMsg");
	}
	
	public String getRemoveURLValMsg() {
		return prop.getProperty("removeURLValMsg");
	}
	
	public String getEmailInfoMsg() {
		return prop.getProperty("emailInfoMsg");
	}
	
	public String getSendEmailReqMsg() {
		return prop.getProperty("sendEmailReqMsg");
	}
	
	public String getSiteSelectionYesInfoMsg() {
		return prop.getProperty("siteSelectionYesInfoMsg");
	}

	public String getSiteSelectionNoMsg() {
		return prop.getProperty("SiteSelectionNoMsg");
	}
	
	public String getSiteSelectionNoInfoMsg() {
		return prop.getProperty("siteSelectionNoInfoMsg");
	}
	
	public String getSiteSelectionReqMsg() {
		return prop.getProperty("siteSelectionReqMsg");
	}
	
	public String getDefaultHeaderMinMsg() {
		return prop.getProperty("defaultHeaderMinMsg");
	}
	
	public String getDefaultHeaderMaxMsg() {
		return prop.getProperty("defaultHeaderMaxMsg");
	}
	
	public String getPassedHeaderMinMsg() {
		return prop.getProperty("passedHeaderMinMsg");
	}
	
	public String getPassedHeaderMaxMsg() {
		return prop.getProperty("passedHeaderMaxMsg");
	}
	
	public String getFailedHeaderMinMsg() {
		return prop.getProperty("failedHeaderMinMsg");
	}
	
	public String getFailedHeaderMaxMsg() {
		return prop.getProperty("failedHeaderMaxMsg");
	}
	
	public String getAppDetailsLabelForAttribute() {
		return prop.getProperty("appDetailsLabelForAttribute");
	}
	
	public String getAttributeInfoMsg() {
		return prop.getProperty("attributeInfoMsg");
	}
	
	public String getAppDetailsLabelForWorkflow() {
		return prop.getProperty("appDetailsLabelForWorkflow");
	}
	
	public String getEntityWorkflowLable() {
		return prop.getProperty("entityWorkflowLable");
	}
	
	public String getProcedureWorkflowLable() {
		return prop.getProperty("procedureWorkflowLable");
	}
	
	public String getAddEntityBtnColor() {
		return prop.getProperty("addEntityBtn.MalibuColor");
	}
	
	public String getApplyEntityFilterLabel() {
		return prop.getProperty("applyEntityFilterLabel");
	}
	
	public String getDefineRuleLabel() {
		return prop.getProperty("defineRuleLabel");
	}
	
	public String getDefinePermissionApp() {
		return prop.getProperty("definePermissionApp");
	}
	
	public String getPermissionTabNavigation() {
		return prop.getProperty("permissionTabNavigation");
	}
	
	public String getPreviewTabNavigation() {
		return prop.getProperty("previewTabNavigation");
	}
	
	public String getPreviewTabInformationMsg() {
		return prop.getProperty("previewTabInformationMsg");
	}
	
	public String getKeyAttributeMsgAcceptanceTab() {
		return prop.getProperty("keyAttributeMsgAcceptanceTab");
	}
	
	public String getDefinePassCriteriaMsgAcceptanceTab() {
		return prop.getProperty("definePassCriteriaMsgAcceptanceTab");
	}
	
	public String getAddProcedureMsgAcceptanceTab() {
		return prop.getProperty("addProcedureMsgAcceptanceTab");
	}
	
	public String getAcceptanceTabNavigation() {
		return prop.getProperty("acceptanceTabNavigation");
	}
	
	public String getAcceptanceTabLabel() {
		return prop.getProperty("acceptanceTabLabel");
	}
	
	public String getAcceptanceTabCurrentVersion() {
		return prop.getProperty("acceptanceTabCurrentVersion");
	}

	public String getTrackUniqueRecordsMsg() {
		return prop.getProperty("trackUniqueRecordsMsg");
	}
	
	public String getVerifyProcedureName() {
		return prop.getProperty("verifyProcedureName");
	}
	
	// App 
	public String getSuccesfullyPass() {
		return prop.getProperty("succesfullyPass");
	}
	
	//Attribute
	public String getAttributesModuleName() {
		return prop.getProperty("attributesModuleName");
	}
	
	public String getAttributeAuditTrailBreadCrumbs() {
		return prop.getProperty("attributeAuditTrailBreadCrumbs");
	}
	
	public String getAttributeViewPopuplabel() {
		return prop.getProperty("attributeViewPopuplabel");
	}
	
	public String getAttributeAuditTrailPageName() {
		return prop.getProperty("attributeAuditTrailPageName");
	}
	
	public String getEditAttributeCurrentVersion() {
		return prop.getProperty("editAttributeCurrentVersion");
	}
	
	//Create Window
	public String getConfirmMsg() {
		return prop.getProperty("confirmMsg");
	}
	
	public String getWindowNameRequiredMsg() {
		return prop.getProperty("windowNameRequiredMsg");
	}
	
	public String getWindowNameMinimumMsg() {
		return prop.getProperty("windowNameMinimumMsg");
	}
	
	public String getWindowNameMaximumMsg() {
		return prop.getProperty("windowNameMaximumMsg");
	}
	
	public String getWindowNameUniqueMsg() {
		return prop.getProperty("windowNameUniqueMsg");
	}
	
	public String getDescriptionMinimumMsg() {
		return prop.getProperty("descriptionMinimumMsg");
	}
	
	public String getDescriptionMaximumMsg() {
		return prop.getProperty("descriptionMaximumMsg");
	}
	
	public String getModuleDrpDwn() {
		return prop.getProperty("moduleDrpDwn");
	}
	
	public String getAppDrpDwn() {
		return prop.getProperty("appDrpDwn");
	}
	
	public String getModuleDrpDwnAllVal() {
		return prop.getProperty("moduleDrpDwnAllVal");
	}
	
	public String getModuleDrpDwnRequiredMsg() {
		return prop.getProperty("moduleDrpDwnRequiredMsg");
	}
	
	public String getAttributeEntityRequiredMsg() {
		return prop.getProperty("attributeEntityRequiredMsg");
	}
	
	public String getProcedureDrpDwnRequiredMsg() {
		return prop.getProperty("procedureDrpDwnRequiredMsg");
	}
	
	public String getQuestionDrpDwnRequiredMsg() {
		return prop.getProperty("questionDrpDwnRequiredMsg");
	}
	
	public String getWindowTypeRequiredMsg() {
		return prop.getProperty("windowTypeRequiredMsg");
	}
	
	public String getWindowTypeDrpDwn() {
		return prop.getProperty("windowTypeDrpDwn");
	}
	
	public String getWindowPlanRequiredMsg() {
		return prop.getProperty("windowPlanRequiredMsg");
	}
	
	public String getWindowPlanDrpDwn() {
		return prop.getProperty("windowPlanDrpDwn");
	}
	
	public String getSampleSizeRequiredMsg() {
		return prop.getProperty("sampleSizeRequiredMsg");
	}
	
	public String getCollectionDateRequiredMsg() {
		return prop.getProperty("collectionDateRequiredMsg");
	}
	
	public String getMonthDrpDwn() {
		return prop.getProperty("monthDrpDwn");
	}
	
	public String getYearDrpDwn() {
		return prop.getProperty("yearDrpDwn");
	}
	
	public String getMonthYearMsgForCurWindow() {
		return prop.getProperty("monthYearMsgForCurWindow");
	}
	
	public String getMonthYearMsg() {
		return prop.getProperty("monthYearMsg");
	}
	
	public String getInformationIconMsg() {
		return prop.getProperty("informationIconMsg");
	}
	
	public String getSelectionRequiredMsg() {
		return prop.getProperty("selectionRequiredMsg");
	}
	
	public String getClickAllCheckboxMsg() {
		return prop.getProperty("clickAllCheckboxMsg");
	}
	
	public String getClickAllCheckboxMsgForSite() {
		return prop.getProperty("clickAllCheckboxMsgForSite");
	}
	
	public String getPopulationRequiredMsg() {
		return prop.getProperty("populationRequiredMsg");
	}
	
	public String getPopulationMinimumMsg() {
		return prop.getProperty("populationMinimumMsg");
	}
	
	public String getPopulationMaximumMsg() {
		return prop.getProperty("populationMaximumMsg");
	}
	
	public String getSampleSizeMinimumMsg() {
		return prop.getProperty("sampleSizeMinimumMsg");
	}
	
	public String getSampleSizeMaximumMsg() {
		return prop.getProperty("sampleSizeMaximumMsg");
	}
	
	public String getMaximumAllowedProcessFailureRequiredMsg() {
		return prop.getProperty("maximumAllowedProcessFailureRequiredMsg");
	}
	
	public String getMaximumAllowedProcessFailureMinimumMsg() {
		return prop.getProperty("maximumAllowedProcessFailureMinimumMsg");
	}
	
	public String getMaximumAllowedProcessFailureMaximumMsg() {
		return prop.getProperty("maximumAllowedProcessFailureMaximumMsg");
	}
	
	public String getSampleSizeLessThan() {
		return prop.getProperty("sampleSizeLessThan");
	}
	
	public String getHourlyMinimumMsg() {
		return prop.getProperty("hourlyMinimumMsg");
	}
	
	public String getWeeklyRequiredMsg() {
		return prop.getProperty("weeklyRequiredMsg");
	}
	
	public String getEmailDuplicateMsg() {
		return prop.getProperty("emailDuplicateMsg");
	}
	
	public String getEmailInformationMsg() {
		return prop.getProperty("emailInformationMsg");
	}
	
	public String getSaveBtn() {
		return prop.getProperty("saveBtn");
	}
	
	
	public String getCancelBtn() {
		return prop.getProperty("cancelBtn");
	}
	
	
	//Entities
	public String getPageTxtLabelForEditEntity() {
		return prop.getProperty("pageTxtLabelForEditEntity");
	}
	
	public String getPageTxtLabelForEntity() {
		return prop.getProperty("pageTxtLabelForEntity");
	}
	
	public String getCurrVrsonTxtLbl() {
		return prop.getProperty("currVrsonTxtLbl");
	}
	
	public String getEntyNameTxtBoxReqMsg() {
		return prop.getProperty("entyNameTxtBoxReqMsg");
	}
	
	public String getEntyNameTxtBoxMinMsg() {
		return prop.getProperty("entyNameTxtBoxMinMsg");
	}
	
	public String getEntyNameTxtBoxMaxMsg() {
		return prop.getProperty("entyNameTxtBoxMaxMsg");
	}
	
	public String getEntyNameTxtBoxUniqMsg() {
		return prop.getProperty("entyNameTxtBoxUniqMsg");
	}
	
	public String getEntyTagTxtBoxReqMsg() {
		return prop.getProperty("entyTagTxtBoxReqMsg");
	}
	
	public String getEntyTagTxtBoxMinMsg() {
		return prop.getProperty("entyTagTxtBoxMinMsg");
	}
	
	public String getEntyTagTxtBoxMaxMsg() {
		return prop.getProperty("entyTagTxtBoxMaxMsg");
	}
	
	public String getEntyTagTxtBoxUniqMsg() {
		return prop.getProperty("entyTagTxtBoxUniqMsg");
	}
	
	public String getEntyDscipTxtBoxMinMsg() {
		return prop.getProperty("entyDscipTxtBoxMinMsg");
	}
	
	public String getEntyDscipTxtBoxMaxMsg() {
		return prop.getProperty("entyDscipTxtBoxMaxMsg");
	}
	
	public String getInstForUsrTxtBoxRqiMsg() {
		return prop.getProperty("instForUsrTxtBoxRqiMsg");
	}
	
	public String getInstForUsrTxtBoxMinMsg() {
		return prop.getProperty("instForUsrTxtBoxMinMsg");
	}
	
	public String getInstForUsrTxtBoxMaxMsg() {
		return prop.getProperty("instForUsrTxtBoxMaxMsg");
	}
	
	public String getAttriNameTxtBoxRqiMsg() {
		return prop.getProperty("attriNameTxtBoxRqiMsg");
	}
	
	public String getAttriNameTxtBoxMinMsg() {
		return prop.getProperty("attriNameTxtBoxMinMsg");
	}
	
	public String getAttriNameTxtBoxMaxMsg() {
		return prop.getProperty("attriNameTxtBoxMaxMsg");
	}
	
	public String getEntityViewPopuplabel() {
		return prop.getProperty("entityViewPopuplabel");
	}
	
	//Entity Record
	public String getEntityRecdsLabel() {
		return prop.getProperty("entityRecdsLabel");
	}
	
	public String getEntitySearchLabl() {
		return prop.getProperty("entitySearchLabl");
	}
	
	public String getEntityModule() {
		return prop.getProperty("entityModule");
	}
	
	public String getNoEntityFound() {
		return prop.getProperty("noEntityFound");
	}
	
	public String getRemovePopupMyFavoriteLabel() {
		return prop.getProperty("removePopupMyFavoriteLabel");
	}
	
	//General Settings
	public String getGeneralSettingName() {
		return prop.getProperty("generalSettingName");
	}
	
	public String getBreadcrumb() {
		return prop.getProperty("breadcrumb");
	}
	
	public String getValidationRecordTitle() {
		return prop.getProperty("validationRecordTitle");
	}
	
	public String getVerifyMinLengthvalidationMsg() {
		return prop.getProperty("verifyMinLengthvalidationMsg");
	}
	
	public String getVerifyMaxLengthvalidationMsg() {
		return prop.getProperty("verifyMaxLengthvalidationMsg");
	}
	
	public String getStdReportsubTitleRequiredValidation() {
		return prop.getProperty("stdReportsubTitleRequiredValidation");
	}
	
	public String getStdReportsubTitleMinValidation() {
		return prop.getProperty("stdReportsubTitleMinValidation");
	}
	
	public String getStdReportsubTitleMaxValidation() {
		return prop.getProperty("stdReportsubTitleMaxValidation");
	}
	
	public String getTextareaMinValidation() {
		return prop.getProperty("textareaMinValidation");
	}
	
	public String getTextareaMaxValidation() {
		return prop.getProperty("textareaMaxValidation");
	}
	
	public String getVerify0passwordAgingBox() {
		return prop.getProperty("verify0passwordAgingBox");
	}
	
	public String getVerify999passwordAgingBox() {
		return prop.getProperty("verify999passwordAgingBox");
	}
	
	public String getPasswordAgingMsgMinValidation() {
		return prop.getProperty("PasswordAgingMsgMinValidation");
	}
	
	public String getPasswordAgingMsgMaxValidation() {
		return prop.getProperty("PasswordAgingMsgMaxValidation");
	}
	
	public String getEnterZeroValidationPreviousUsedPasswordsBox() {
		return prop.getProperty("enterZeroValidationPreviousUsedPasswordsBox");
	}
	
	public String getLessthan4ValidationPreviousUsedPasswordsBox() {
		return prop.getProperty("lessthan4ValidationPreviousUsedPasswordsBox");
	}
	
	public String getGreaterthan99ValidationPreviousUsedPassBox() {
		return prop.getProperty("greaterthan99ValidationPreviousUsedPassBox");
	}
	
	public String getMinValidationFailLoginAttemptsBox() {
		return prop.getProperty("MinValidationFailLoginAttemptsBox");
	}
	
	public String getMaxValidationFailLoginAttemptsBox() {
		return prop.getProperty("MaxValidationFailLoginAttemptsBox");
	}
	
	public String getValMinMonthlyWindow() {
		return prop.getProperty("ValMinMonthlyWindow");
	}
	
	public String getValMaxMonthlyWindow() {
		return prop.getProperty("ValMaxMonthlyWindow");
	}
	
	//Group Settings
	public String getVerifyGrpSetingsTile() {
		return prop.getProperty("verifyGrpSetingsTile");
	}
	
	public String getAddEditpage() {
		return prop.getProperty("AddEditpage");
	}
	
	public String getValGroupnametxt() {
		return prop.getProperty("ValGroupnametxt");
	}
	
	public String getMinValGroupnametxt() {
		return prop.getProperty("MinValGroupnametxt");
	}
	
	public String getMaxValGroupnametxt() {
		return prop.getProperty("MaxValGroupnametxt");
	}
	
	public String getUniqueValGroupnametxt() {
		return prop.getProperty("UniqueValGroupnametxt");
	}
	
	//Import Data
	public String getImportDataRecdsLbl() {
		return prop.getProperty("importDataRecdsLbl");
	}
	
	public String getBreadcrumImportData() {
		return prop.getProperty("breadcrumImportData");
	}
	
	public String getManualImportLabel() {
		return prop.getProperty("manualImportLabel");
	}
	
	public String getBreadcrumManImp() {
		return prop.getProperty("breadcrumManImp");
	}
	
	public String getUploadCsvFileLabel() {
		return prop.getProperty("uploadCsvFileLabel");
	}
	
	public String getTypeDrpDwnValMsg() {
		return prop.getProperty("typeDrpDwnValMsg");
	}
	
	public String getMappingNameValMsg() {
		return prop.getProperty("mappingNameValMsg");
	}
	
	public String getMappingNameValMsgForDup() {
		return prop.getProperty("mappingNameValMsgForDup");
	}
	
	public String getMappingNameValMsgForMinSize() {
		return prop.getProperty("mappingNameValMsgForMinSize");
	}
	
	public String getMappingNameValMsgForMaxSize() {
		return prop.getProperty("mappingNameValMsgForMaxSize");
	}
	
	public String getViewSavedMappingLabel() {
		return prop.getProperty("viewSavedMappingLabel");
	}
	
	//Login
	public String getLoginlabel() {
		return prop.getProperty("loginlabel");
	}
	
	public String getPasswordMandatoryValidation() {
		return prop.getProperty("passwordMandatoryValidation");
	}
	
	public String getInvalidLoginMessage() {
		return prop.getProperty("invalidLoginMessage");
	}
	
	public String getCommonFnPageObjectColor() {
		return prop.getProperty("commonFnPageObject.Pelorouscolor");
	}
	
	public String getForgotPassLinkGreycolor() {
		return prop.getProperty("forgotPassLink.Greycolor");
	}
	
	public String getForgotPassLink() {
		return prop.getProperty("forgotPassLink");
	}
	
	public String getForgotPassLabel() {
		return prop.getProperty("forgotPassLabel");
	}

	public String getSubmitBtnLabl() {
		return prop.getProperty("submitBtnLabl");
	}
	
	//Master App Settings
	public String getMasterAppSettingsModuleName() {
		return prop.getProperty("masterAppSettingsModuleName");
	}
	
	public String getEditMasterAppBreadCrumbs() {
		return prop.getProperty("editMasterAppBreadCrumbs");
	}
	
	public String getMasterAppReqVal() {
		return prop.getProperty("masterAppReqVal");
	}
	
	public String getMasterAppUniqueVal() {
		return prop.getProperty("masterAppUniqueVal");
	}
	
	public String getMasterAppMiniVal() {
		return prop.getProperty("masterAppMiniVal");
	}
	
	public String getMasterAppMaxVal() {
		return prop.getProperty("masterAppMaxVal");
	}
	
	//My Profile 
	public String getModuleNameMyProfile() {
		return prop.getProperty("moduleNameMyProfile");
	}
	
	public String getUsernameLabel() {
		return prop.getProperty("usernameLabel");
	}
	
	public String getFirstnameLabel() {
		return prop.getProperty("firstnameLabel");
	}
	
	public String getFirstnameRequiredMsg() {
		return prop.getProperty("firstnameRequiredMsg");
	}
	
	public String getFirstnameMinimumMsg() {
		return prop.getProperty("firstnameMinimumMsg");
	}
	
	public String getFirstnameMaximumMsg() {
		return prop.getProperty("firstnameMaximumMsg");
	}
	
	public String getLastnameLabel() {
		return prop.getProperty("lastnameLabel");
	}
	
	public String getLastnameRequiredMsg() {
		return prop.getProperty("lastnameRequiredMsg");
	}
	
	public String getLastnameMinimumMsg() {
		return prop.getProperty("lastnameMinimumMsg");
	}
	
	public String getLastnameMaximumMsg() {
		return prop.getProperty("lastnameMaximumMsg");
	}
	
	public String getEmailLabel() {
		return prop.getProperty("emailLabel");
	}
	
	public String getEmailAddressMaximum() {
		return prop.getProperty("emailAddressMaximum");
	}
	
	public String getEmailAddressUnique() {
		return prop.getProperty("emailAddressUnique");
	}
	
	public String getVerifiedEmailInfo() {
		return prop.getProperty("verifiedEmailInfo");
	}
	
	public String getSecQuesTitle() {
		return prop.getProperty("secQuesTitle");
	}
	
	public String getSecQuesFieldName() {
		return prop.getProperty("secQuesFieldName");
	}
	
	public String getAnswerFieldName() {
		return prop.getProperty("answerFieldName");
	}
	
	public String getSecQuesRequiredMsg() {
		return prop.getProperty("secQuesRequiredMsg");
	}
	
	public String getAnswerRequiredMsg() {
		return prop.getProperty("answerRequiredMsg");
	}
	
	public String getAnswerMinimumMsg() {
		return prop.getProperty("answerMinimumMsg");
	}
	
	public String getAnswerMaximumMsg() {
		return prop.getProperty("answerMaximumMsg");
	}
	
	public String getSaveSuccessMsgcolor() {
		return prop.getProperty("saveSuccessMsg.Greencolor");
	}
	
	public String getSaveSuccessMsg() {
		return prop.getProperty("saveSuccessMsg");
	}
	
	public String getChangePassTitle() {
		return prop.getProperty("changePassTitle");
	}
	
	public String getChangePassLabel() {
		return prop.getProperty("changePassLabel");
	}
	
	public String getNewPassLabel() {
		return prop.getProperty("newPassLabel");
	}
	
	public String getConfirmPassLabel() {
		return prop.getProperty("confirmPassLabel");
	}
	
	public String getInformationIcon() {
		return prop.getProperty("informationIcon");
	}
	
	public String getCurrentPassRequiredMsg() {
		return prop.getProperty("currentPassRequiredMsg");
	}
	
	public String getCurrentPassInvalid() {
		return prop.getProperty("currentPassInvalid");
	}
	
	public String getCurrentPassInvalidColor() {
		return prop.getProperty("currentPassInvalid.NightShadzcolor");
	}
	
	public String getConfirmPassMismatch() {
		return prop.getProperty("confirmPassMismatch");
	}
	
	public String getInvalidMsg() {
		return prop.getProperty("invalidMsg");
	}
	
	public String getNewCurrentSamePass() {
		return prop.getProperty("newCurrentSamePass");
	}
	
	public String getChangeESignPINFirstLabl() {
		return prop.getProperty("changeESignPINFirstLabl");
	}

	public String getCurrentSignInValSignMsg() {
		return prop.getProperty("currentSignInValSignMsg");
	}
	
	public String getChangeESignPINSecondLabl() {
		return prop.getProperty("changeESignPINSecondLabl");
	}
	
	public String getChangeESignPINThirdLabl() {
		return prop.getProperty("changeESignPINThirdLabl");
	}
	
	public String getCurrentSignTextBoxValMsg() {
		return prop.getProperty("currentSignTextBoxValMsg");
	}
	
	public String getNewSignValMsg() {
		return prop.getProperty("newSignValMsg");
	}
	
	public String getConfirmValMsg() {
		return prop.getProperty("confirmValMsg");
	}
	
	public String getNewSignValMsgForDup() {
		return prop.getProperty("newSignValMsgForDup");
	}
	
	public String getConfirmSignValMsgForMisMatch() {
		return prop.getProperty("confirmSignValMsgForMisMatch");
	}
	
	public String getNewSignValMsgForMin() {
		return prop.getProperty("newSignValMsgForMin");
	}
	
	public String getNewSignValMsgForMax() {
		return prop.getProperty("newSignValMsgForMax");
	}
	
	public String getConfirmSignValMsgForMin() {
		return prop.getProperty("confirmSignValMsgForMin");
	}
	
	public String getConfirmSignValMsgForMax() {
		return prop.getProperty("confirmSignValMsgForMax");
	}
	
	public String getGeneralErr() {
		return prop.getProperty("generalErr");
	}
	
	//Procedure
	public String getaddEditProcedure() {
		return prop.getProperty("addEditProcedure");
	}
	
	public String getaddEditProcedurebreadcrumb() {
		return prop.getProperty("addEditProcedurebreadcrumb");
	}
	
	public String getprocedureBreadcrumb() {
		return prop.getProperty("procedureBreadcrumb");
	}
	
	public String getaddEditProcedurebreadcrumbForAuditTrail() {
		return prop.getProperty("addEditProcedurebreadcrumbForAuditTrail");
	}
	
	public String getprocedurenameRequired() {
		return prop.getProperty("procedurenameRequired");
	}
	
	public String getProcedurenameMinvalidation() {
		return prop.getProperty("procedurenameMinvalidation");
	}
	
	public String getProcedurenameMaxvalidation() {
		return prop.getProperty("procedurenameMaxvalidation");
	}
	
	public String getProcedurenameUniquevalidation() {
		return prop.getProperty("procedurenameUniquevalidation");
	}
	
	public String getProceduretagRequired() {
		return prop.getProperty("proceduretagRequired");
	}
	
	public String getProceduretagMinValidation() {
		return prop.getProperty("proceduretagMinValidation");
	}
	
	public String getProceduretagMaxValidation() {
		return prop.getProperty("proceduretagMaxValidation");
	}
	
	
	public String getUrlInputrequired() {
		return prop.getProperty("UrlInputrequired");
	}
	
	public String getInvalidUrlInput() {
		return prop.getProperty("invalidUrlInput");
	}
	
	//Question
	public String getQuestionsModuleName() {
		return prop.getProperty("questionsModuleName");
	}
	
	public String getBreadCrumbsForAddEditQuestion() {
		return prop.getProperty("breadCrumbsForAddEditQuestion");
	}
	
	public String getQuestionsRequiredVal() {
		return prop.getProperty("questionsRequiredVal");
	}
	
	public String getQuestionsMinimumVal() {
		return prop.getProperty("questionsMinimumVal");
	}
	
	public String getQuestionsMaximumVal() {
		return prop.getProperty("questionsMaximumVal");
	}
	
	public String getQuestionsUniqueVal() {
		return prop.getProperty("questionsUniqueVal");
	}
	
	public String getQuestionsTagRequiredVal() {
		return prop.getProperty("questionsTagRequiredVal");
	}
	
	public String getQuestionsTagMinVal() {
		return prop.getProperty("questionsTagMinVal");
	}
	
	public String getQuestionsTagMaxVal() {
		return prop.getProperty("questionsTagMaxVal");
	}
	
	public String getQuestionsTagUniqueVal() {
		return prop.getProperty("questionsTagUniqueVal");
	}
	
	
	public String getEditQuestionBreadCrumbsForAuditTrail() {
		return prop.getProperty("editQuestionBreadCrumbsForAuditTrail");
	}
	
	public String getEditQuestionBreadCrumbs() {
		return prop.getProperty("editQuestionBreadCrumbs");
	}
	
	//Schedule
	public String getScheduleHeading() {
		return prop.getProperty("scheduleHeading");
	}
	
	public String getReviewCompletedAppTab() {
		return prop.getProperty("reviewCompletedAppTab");
	}
	
	public String getStartScheduleApps() {
		return prop.getProperty("startScheduleApps");
	}
	
	//Search Window
	
	public String getSearchWindowLabl() {
		return prop.getProperty("searchWindowLabl");
	}
	
	public String getSearchWindowBreCr() {
		return prop.getProperty("searchWindowBreCr");
	}
	
	public String getDateOpDrpDwn() {
		return prop.getProperty("dateOpDrpDwn");
	}
	
	public String getLastUpdatedBy() {
		return prop.getProperty("lastUpdatedBy");
	}
	
	public String getDeleteFirstEntryConfiPopup() {
		return prop.getProperty("deleteFirstEntryConfiPopup");
	}
	
	public String getDeleteFirstEntryConfiPopupYesBtn() {
		return prop.getProperty("deleteFirstEntryConfiPopupYesBtn");
	}
	
	public String getHeaderType() {
		return prop.getProperty("headerType");
	}
	
	public String getHeaderTypeForEntity() {
		return prop.getProperty("headerTypeForEntity");
	}
	
	public String getHeaderTypeForProcedure() {
		return prop.getProperty("headerTypeForProcedure");
	}

	public String getViewRcdLabl() {
		return prop.getProperty("viewRcdLabl");
	}

	public String getPassedMsg() {
		return prop.getProperty("passedMsg");
	}
	
	public String getFailedMsg() {
		return prop.getProperty("failedMsg");
	}
	
	public String getAssignLabl() {
		return prop.getProperty("assignLabl");
	}
	
	public String getAssignFailureLabl() {
		return prop.getProperty("assignFailureLabl");
	}
	
	public String getPendingFailureLabl() {
		return prop.getProperty("pendingFailureLabl");
	}

	public String getDispositionLablText() {
		return prop.getProperty("dispositionLablText");
	}
	
	public String getCommentTextBoxMinMsg() {
		return prop.getProperty("commentTextBoxMinMsg");
	}
	
	public String getCommentTextBoxMaxMsg() {
		return prop.getProperty("commentTextBoxMaxMsg");
	}
	
	public String getRequiredValMsgForAssignScreen() {
		return prop.getProperty("requiredValMsgForAssignScreen");
	}
	
	public String getNonProcessLabl() {
		return prop.getProperty("nonProcessLabl");
	}
	
	public String getFailuerTypeHead() {
		return prop.getProperty("failuerTypeHead");
	}
	
	public String getFailureTypeNonProcess() {
		return prop.getProperty("failureTypeNonProcess");
	}
	
	public String getFailureTypeProcess() {
		return prop.getProperty("failureTypeProcess");
	}
	
	public String getProcessLabl() {
		return prop.getProperty("processLabl");
	}
	
	public String getPopUpMsgOfStopWindow() {
		return prop.getProperty("popUpMsgOfStopWindow");
	}
	
	public String getWindowStatus() {
		return prop.getProperty("windowStatus");
	}
	
	public String getWindowStatusStopped() {
		return prop.getProperty("windowStatusStopped");
	}
	
	//Set Frequency
	public String getSetFreqRecdsLabl() {
		return prop.getProperty("setFreqRecdsLabl");
	}
	
	public String getAddEditFreLabl() {
		return prop.getProperty("addEditFreLabl");
	}
	
	public String getFreNameTextBoxValMsg() {
		return prop.getProperty("freNameTextBoxValMsg");
	}
	
	public String getFreNameTextBoxValMsgForMinSize() {
		return prop.getProperty("freNameTextBoxValMsgForMinSize");
	}
	
	public String getFreNameTextBoxValMsgForMaxSize() {
		return prop.getProperty("freNameTextBoxValMsgForMaxSize");
	}
	
	public String getFreNameTextBoxValMsgForDup() {
		return prop.getProperty("freNameTextBoxValMsgForDup");
	}
	
	public String getMasterAppDrpDwnValMsg() {
		return prop.getProperty("masterAppDrpDwnValMsg");
	}
	
	public String getHourlyLabl() {
		return prop.getProperty("hourlyLabl");
	}
	
	public String getDailyLabl() {
		return prop.getProperty("dailyLabl");
	}
	
	public String getWeeklyLabl() {
		return prop.getProperty("weeklyLabl");
	}
	
	public String getMonthlyLabl() {
		return prop.getProperty("monthlyLabl");
	}
	
	public String getHourlyTextboxValMsgForMax() {
		return prop.getProperty("hourlyTextboxValMsgForMax");
	}
	
	public String getDailyTextboxValMsg() {
		return prop.getProperty("dailyTextboxValMsg");
	}
	
	public String getEndAfterTextboxValMsg() {
		return prop.getProperty("endAfterTextboxValMsg");
	}
	
	public String getEndAfterTextboxValMsgForMax() {
		return prop.getProperty("endAfterTextboxValMsgForMax");
	}
	
	//Sets
	public String getSetsModuleName() {
		return prop.getProperty("setsModuleName");
	}
	
	public String getVerifyFirstPage() {
		return prop.getProperty("verifyFirstPage");
	}
	
	public String getLinkInformationPopUp() {
		return prop.getProperty("linkInformationPopUp");
	}
	
	public String getCurrentVersion() {
		return prop.getProperty("currentVersion");
	}
	
	public String getRequiredMsg() {
		return prop.getProperty("requiredMsg");
	}
	
	public String getMinimumMsg() {
		return prop.getProperty("minimumMsg");
	}
	
	public String getMaximumMsg() {
		return prop.getProperty("maximumMsg");
	}
	
	public String getSpecialCharMsg() {
		return prop.getProperty("specialCharMsg");
	}
	
	public String getDuplicateMsg() {
		return prop.getProperty("duplicateMsg");
	}
	
	//Share App Link
	public String getVerifyShareLinkTile() {
		return prop.getProperty("verifyShareLinkTile");
	}
	
	public String getVerifybreadcrumb() {
		return prop.getProperty("verifybreadcrumb");
	}
	
	public String getVerifyBreadcrumbForRcd() {
		return prop.getProperty("VerifyBreadcrumbForRcd");
	}
	
	public String getShareAppBtnDirection() {
		return prop.getProperty("ShareAppBtnDirection");
	}
	
	public String getVerifyLableDisplay() {
		return prop.getProperty("verifyLableDisplay");
	}
	
	public String getVerifyLableAppDeails() {
		return prop.getProperty("verifyLableAppDeails");
	}
	
	public String getVerifyLableOtherUserDeails() {
		return prop.getProperty("verifyLableOtherUserDeails");
	}
	
	public String getInvalidEmailMsg() {
		return prop.getProperty("invalidEmailMsg");
	}
	
	public String getTwooptions1InAllowMultipleApp() {
		return prop.getProperty("twooptions1InAllowMultipleApp");
	}
	
	public String getTwooptions2InAllowMultipleApp() {
		return prop.getProperty("twooptions2InAllowMultipleApp");
	}
	
	public String getTotalNumberofAppRadioBtnValidationMsg() {
		return prop.getProperty("totalNumberofAppRadioBtnValidationMsg");
	}
	
	public String getTotalNumberofAppSubmissioZeroValidation() {
		return prop.getProperty("totalNumberofAppSubmissioZeroValidation");
	}
	
	public String getTotalNumberofAppSubmissioMaxValidation() {
		return prop.getProperty("totalNumberofAppSubmissioMaxValidation");
	}
	
	public String getBlankValidationNumberofSunmissionperIndiv() {
		return prop.getProperty("blankValidationNumberofSunmissionperIndiv");
	}
	
	public String getMinValidationNumberofSunmissionperIndivBox() {
		return prop.getProperty("minValidationNumberofSunmissionperIndivBox");
	}
	
	public String getMaxValidationNumberofSunmissionperIndivBox() {
		return prop.getProperty("maxValidationNumberofSunmissionperIndivBox");
	}
	
	public String getRequiredsetLinkexpiryDate() {
		return prop.getProperty("requiredsetLinkexpiryDate");
	}
	
	public String getInvalidsetLinkexpiryDate() {
		return prop.getProperty("invalidsetLinkexpiryDate");
	}
	
	public String getMinvalidationSpecialMsg() {
		return prop.getProperty("minvalidationSpecialMsg");
	}
	
	//Sites
	public String getVerifySitesTile() {
		return prop.getProperty("verifySitesTile");
	}
	
	public String getVerifySiteLevelNameTab() {
		return prop.getProperty("verifySiteLevelNameTab");
	}
	
	public String getAddEditSitesofSiteListing() {
		return prop.getProperty("addEditSitesofSiteListing");
	}
	
	public String getRedirectedNameLevelSite() {
		return prop.getProperty("redirectedNameLevelSite");
	}
	
	
	public String getVerifysearchboxSiteListingScreen() {
		return prop.getProperty("verifysearchboxSiteListingScreen");
	}
	
	//Smart Alert
	public String getSmartAlertsLableName() {
		return prop.getProperty("smartAlertsLableName");
	}
	
	public String getAddEditSmartAlrtLabel() {
		return prop.getProperty("addEditSmartAlrtLabel");
	}
	
	public String getCurrVersion() {
		return prop.getProperty("CurrVersion");
	}
	
	public String getAlertNameTxtBoxRquiMsg() {
		return prop.getProperty("alertNameTxtBoxRquiMsg");
	}
	
	public String getAlertNameTxtBoxMinMsg() {
		return prop.getProperty("alertNameTxtBoxMinMsg");
	}
	
	public String getAlertNameTxtBoxMaxMsg() {
		return prop.getProperty("alertNameTxtBoxMaxMsg");
	}
	
	public String getAlertNameTxtBoxUniqMsg() {
		return prop.getProperty("alertNameTxtBoxUniqMsg");
	}
	
	public String getAlertDescpTxtBoxMinMsg() {
		return prop.getProperty("alertDescpTxtBoxMinMsg");
	}
	
	public String getAlertDescpTxtBoxMaxMsg() {
		return prop.getProperty("alertDescpTxtBoxMaxMsg");
	}
	
	public String getMsgAndInstToEmailTxtBoxRquiMsg() {
		return prop.getProperty("msgAndInstToEmailTxtBoxRquiMsg");
	}
	
	public String getMsgAndInstToEmailTxtBoxMinMsg() {
		return prop.getProperty("msgAndInstToEmailTxtBoxMinMsg");
	}
	
	public String getMsgAndInstToEmailTxtBoxMaxMsg() {
		return prop.getProperty("msgAndInstToEmailTxtBoxMaxMsg");
	}
	
	public String getEmailAddTxtBoxReqMsg() {
		return prop.getProperty("emailAddTxtBoxReqMsg");
	}
	
	public String getDailyTxtBoxLimitMsg() {
		return prop.getProperty("dailyTxtBoxLimitMsg");
	}
	
	public String getWeeklyTxtBoxRqiMsg() {
		return prop.getProperty("weeklyTxtBoxRqiMsg");
	}
	
	public String getWeeklyTxtBoxLimitMsg() {
		return prop.getProperty("weeklyTxtBoxLimitMsg");
	}
	
	//User Activity
	public String getVerifyUserActivityTile() {
		return prop.getProperty("verifyUserActivityTile");
	}
	
	public String getVerifysearchboxresult() {
		return prop.getProperty("verifysearchboxresult");
	}
	
	public String getVerifyUserActTileForLoggedUser() {
		return prop.getProperty("verifyUserActTileForLoggedUser");
	}
	
	//User
	public String getUsersBreadcrumbtxt() {
		return prop.getProperty("usersBreadcrumbtxt");
	}
	
	public String getUsersListingBreadCrumbs() {
		return prop.getProperty("usersListingBreadCrumbs");
	}
	
	public String getUserNameDuplicatevalMsg() {
		return prop.getProperty("userNameDuplicatevalMsg");
	}
	
	public String getUniqueEmailAdd() {
		return prop.getProperty("uniqueEmailAdd");
	}
	
	public String getAtleastMinCharMsg() {
		return prop.getProperty("atleastMinCharMsg");
	}
	
	public String getAtleastMaxCharMsg() {
		return prop.getProperty("atleastMaxCharMsg");
	}
	
	public String getAtleastSmallCaseMsg() {
		return prop.getProperty("atleastSmallCaseMsg");
	}
	
	public String getAtleastCapitalCaseMsg() {
		return prop.getProperty("atleastCapitalCaseMsg");
	}
	
	public String getAtleastSpecialMsg() {
		return prop.getProperty("atleastSpecialMsg");
	}
	
	public String getDigitCase() {
		return prop.getProperty("DigitCase");
	}

	public String getActiveUserResetAccessMsg() {
		return prop.getProperty("activeUserResetAccessMsg");
	}
	
	public String getBreadcrumbtxtForAction() {
		return prop.getProperty("breadcrumbtxtForAction");
	}
	
	public String getBreadcrumbtxtForUser() {
		return prop.getProperty("breadcrumbtxtForUser");
	}

	public String getInactiveUserResetAccessMsgOne() {
		return prop.getProperty("inactiveUserResetAccessMsgOne");
	}

	public String getInactiveUserResetAccessMsgTwo() {
		return prop.getProperty("inactiveUserResetAccessMsgTwo");
	}
	
	public String getNewPasswordManValMsg() {
		return prop.getProperty("newPasswordManValMsg");
	}
	
	public String getResetPageConfirmPwdMandValmsg() {
		return prop.getProperty("resetPageConfirmPwdMandValmsg");
	}
	
	public String getConfirmPasswordMsg() {
		return prop.getProperty("confirmPasswordMsg");
	}
	
	public String getLicenseAggStatus() {
		return prop.getProperty("licenseAggStatus");
	}

}
