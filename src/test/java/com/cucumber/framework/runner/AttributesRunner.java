/**
 * @author vishal bhut
 *	
 *	20-May-2019
 */
package com.cucumber.framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = { "classpath:featurefile/Attributes.feature" }, glue = {
		"classpath:com.cucumber.framework.stepdefinition",
		"classpath:com.cucumber.framework.helper" }, plugin = {"pretty","html:target/cucumber-html-reports",
		        "json:target/cucumber.json"
		},tags = {"@demo"}, 
		monochrome = true)
public class AttributesRunner extends AbstractTestNGCucumberTests
{
	
}										
/* "junit:target/cucum																																																																																																																																																																																																																																											ber.xml","return:target/turn.txt" */