/**
 * @author Chirag Karia
 *	
 *	04-06-2019
 */
package com.cucumber.framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = {
//		"classpath:featurefile/AdvancedSearch.feature",
		"classpath:featurefile/Login.feature",
//		"classpath:featurefile/Attributes.feature",
		//"classpath:featurefile/AppBuilder.feature",
		//"classpath:featurefile/CreateWindow.feature",
		//"classpath:featurefile/Entities.feature",
		//"classpath:featurefile/EntityRecords.feature",
		//"classpath:featurefile/GeneralSettings.feature",
		//"classpath:featurefile/GroupSettings.feature",
		//"classpath:featurefile/ImportData.feature",
		//"classpath:featurefile/MasterAppSetting.feature",
		//"classpath:featurefile/MyProfile.feature",
		//"classpath:featurefile/Procedure.feature",
//		"classpath:featurefile/Questions.feature",
		//"classpath:featurefile/Schedule.feature",
		//"classpath:featurefile/SearchWindow.feature",
		//"classpath:featurefile/SetFrequency.feature",
		//"classpath:featurefile/Sets.feature",
		//"classpath:featurefile/ShareAppLink.feature",
		//"classpath:featurefile/Sites.feature",
		//"classpath:featurefile/SmartAlerts.feature",
		//"classpath:featurefile/UserActivity.feature",
		//"classpath:featurefile/Users.feature",
		}, 
		glue = {
		"classpath:com.cucumber.framework.stepdefinition",
		"classpath:com.cucumber.framework.helper" }, plugin = { "pretty",
		"html:target/cucumber-reports","json:target/LoginRunner.json"},
				 tags = {"@testRun2","~@demo"},
		 monochrome = true
		)
public class AllModulesRunner extends AbstractTestNGCucumberTests 
{
	
}
/* "junit:target/cucumber.xml","return:target/turn.txt" */










