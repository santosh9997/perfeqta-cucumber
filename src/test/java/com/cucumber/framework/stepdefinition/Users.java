package com.cucumber.framework.stepdefinition;
import org.openqa.selenium.Keys;

import com.cucumber.framework.helper.PageObject.PaginationPageObject;
import com.cucumber.framework.helper.PageObject.UsersPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class Users 
{
	public ExcelUtils excel;
	private UsersPageObject usersPageObj;
	private PaginationPageObject paginationObject;
	public static String  userNameForReset;
	public Users()
	{
		usersPageObj = new UsersPageObject(ObjectRepo.driver);
		paginationObject = new PaginationPageObject(ObjectRepo.driver);
	}
	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Users");
		System.err.println();
		return excel.readXLSFile("Users", rowVal, colVal);
	}

	public String excelDataLogin(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Login");
		System.err.println();
		return excel.readXLSFile("Users", rowVal, colVal);
	}
	@Given("^: Click on Users Tile$")
	public void click_on_Users_Tile() throws Throwable
	{
		usersPageObj.clickToUsersicon();
	}
	@Then("^: Verify that Users listing page should be displayed when a user clicks on the Users Tile$")
	public void verify_that_Users_listing_page_should_be_displayed_when_a_user_clicks_on_the_Users_Tile() throws Throwable {
		usersPageObj.verifyUsersModuleName();
	}
	@Given("^: Click on the first record of the Username column$")
	public void click_on_the_first_record_of_the_Username_column() throws Throwable 
	{
						usersPageObj.clickToFirstRecordUnm();
	}
	@Then("^: Verify that system should be redirect to the Add / Edit User screen when a user click on First record of the Username column$")
	public void verify_that_system_should_be_redirect_to_the_Add_Edit_User_screen_when_a_user_click_on_First_record_of_the_Username_column() throws Throwable {
		usersPageObj.verifyUsersEditBreadCrumbs();
	}
	@Given("^: Click on Save button of Users module$")
	public void click_on_Save_button_of_Users_module() throws Throwable {
		usersPageObj.verifyUsersEditBreadCrumbs();
		usersPageObj.clickonSaveBtnForSimple();
	}
	@Then("^: Verify that system should redirect to the Users listing screen$")
	public void verify_that_system_should_redirect_to_the_Users_listing_screen() throws Throwable {
				usersPageObj.verifyUsersModuleName();
	}
	@Given("^: Click on Cancel button of the Users screen$")
	public void click_on_Cancel_button_of_the_Users_screen() throws Throwable {
		usersPageObj.clickonCancelBtn();
	}
	@Then("^: Verify that system should be redirect to the Users listing screen when a user click on the Cancel button$")
	public void verify_that_system_should_be_redirect_to_the_Users_listing_screen_when_a_user_click_on_the_Cancel_button() throws Throwable {

		usersPageObj.verifyUsersModuleName();
	}
	@Then("^: Verify the Save button color should be green for the Users screen$")
	public void verify_the_Save_button_color_should_be_green_for_the_Users() throws Throwable {
		usersPageObj.verifySaveButtonColor();
	}
	@Then("^: Verify that color of Cancel button should be black for the Users screen$")
	public void verify_that_color_of_Cancel_button_should_be_black_for_the_Users_screen() throws Throwable {
		usersPageObj.verifyCanceButtonColor();
	}
	@Given("^: Enter the User name into search box of Users listing screen$")
	public void enter_the_User_name_into_search_box_of_Users_listing_screen() throws Throwable {
		usersPageObj.userSendSearchData();
	}
	@Then("^: Verify the search result$")
	public void verify_the_search_result() throws Throwable {
		String searchData = excelData(2,1);
		usersPageObj.verifyusersSearchData(searchData);
	}
	@Given("^: Click on Sorting icon of the Username column for ascending order sorting$")
	public void click_on_Sorting_icon_of_the_Username_column_for_ascending_order_sorting() throws Throwable {
		System.out.println("test");
	}
	@Then("^: Verify that all the records of Username column display in ascending order$")
	public void verify_that_all_the_records_of_Username_column_display_in_ascending_order() throws Throwable {
		usersPageObj.verifyAscendingorder();
	}
	@Then("^: Verify that color of Back button should be black from the Audit Trail screen under Users module$")
	public void verify_that_color_of_Back_button_should_be_black_from_the_Audit_Trail_screen_under_Users_module() throws Throwable {
		usersPageObj.verifyBackbuttonColor();
	}
	@Given("^: Click on Back button of Users$")
	public void click_on_Back_button() throws Throwable {
		usersPageObj.clickonBackButton();
	}
	@Then("^: Verify that a system should be redirect to the Users listing screen when a user click on the Back button$")
	public void verify_that_a_system_should_be_redirect_to_the_Users_listing_screen_when_a_user_click_on_the_Back_button() throws Throwable {
		usersPageObj.verifyUserlistingBreadcrumbs();
	}
	@Given("^: Click on Add New button of Users listing screen$")
	public void click_on_Add_New_button_of_Users_listing_screen() throws Throwable {
		usersPageObj.clickonAddNewbtn();
	}
	@Given("^: Click on Username textbox and Press the \"([^\"]*)\" key$")
	public void click_on_Username_textbox_and_Press_the_key(String arg1) throws Throwable {
		Thread.sleep(2000);
		usersPageObj.usersTxtUserName.sendKeys(Keys.TAB);
	}
	@Then("^: Verify the Mandatory validation message for Username field as \"([^\"]*)\"$")
	public void verify_the_Mandatory_validation_message_for_Username_field_as(String arg1) throws Throwable {
		usersPageObj.verifyUserNameMandatoryValMsg();
	}
	@Given("^: Click on Username textbox and Enter Username as \"([^\"]*)\"$")
	public void click_on_Username_textbox_and_Enter_Username_as(String arg1) throws Throwable {
		String userName= excelData(3,2);
		usersPageObj.userNameForMinMaxSendkeys(userName);
	}
	@Then("^: Verify Minimum validation message of Username textbox as \"([^\"]*)\"$")
	public void verify_Minimum_validation_message_of_Username_textbox_as(String arg1) throws Throwable {
		usersPageObj.userNamemMinimumValMsg();
	}
	@Given("^: Click on Username textbox and Enter Username value more than (\\d+) characters$")
	public void click_on_Username_textbox_and_Enter_Username_value_more_than_characters(int arg1) throws Throwable {
		String userName= excelData(4,2);
		usersPageObj.userNameForMinMaxSendkeys(userName);
	}
	@Then("^: Verify Maximum validation message of Username textbox as \"([^\"]*)\"$")
	public void verify_Maximum_validation_message_of_Username_textbox_as(String arg1) throws Throwable {
		usersPageObj.userNamemMaximumValMsg();
	}
	@Given("^: Click on Username textbox and enter Duplicate Username$")
	public void click_on_Username_textbox_and_enter_Duplicate_Username() throws Throwable {
		String userName= excelData(5,2);
		usersPageObj.userNameForMinMaxSendkeys(userName);
	}
	@Then("^: Verify Unique validation message of Username textbox as \"([^\"]*)\"$")
	public void verify_Unique_validation_message_of_Username_textbox_as(String arg1) throws Throwable {
		usersPageObj.verifyDuplicateUserName();
	}
	@Given("^: Click on First Name textbox and Click on the \"([^\"]*)\" Key$")
	public void click_on_First_Name_textbox_and_Click_on_the_Key(String arg1) throws Throwable {
		Thread.sleep(1000);
		usersPageObj.txtFirstName.sendKeys(Keys.TAB);
	}
	@Then("^: Verify the Mandatory validation message for First Name field as \"([^\"]*)\"$")
	public void verify_the_Mandatory_validation_message_for_First_Name_field_as(String arg1) throws Throwable {
		usersPageObj.verifyFirstNameValMsg();
	}
	@Given("^: Click on First Name textbox and enter First Name as \"([^\"]*)\"$")
	public void click_on_First_Name_textbox_and_enter_First_Name_as(String arg1) throws Throwable {
		String firstName= excelData(6,3);
		usersPageObj.firstNameSendkeys(firstName);
	}
	@Then("^: Verify Minimum validation message of First Name textbox as \"([^\"]*)\"$")
	public void verify_Minimum_validation_message_of_First_Name_textbox_as(String arg1) throws Throwable {
		usersPageObj.verifyMinimumValMsg();
	}
	@Given("^: Click on First Name textbox and enter First Name value more than (\\d+) characterss$")
	public void click_on_First_Name_textbox_and_enter_First_Name_value_more_than_characterss(int arg1) throws Throwable {
		String firstName= excelData(7,3);
		usersPageObj.firstNameSendkeys(firstName);
	}
	@Then("^: Verify Maximum validation message of First Name textbox as \"([^\"]*)\"$")
	public void verify_Maximum_validation_message_of_First_Name_textbox_as(String arg1) throws Throwable {
		usersPageObj.verifyFirstMaxValMsg();
	}
	@Given("^: Click on Last Name textbox and click on the \"([^\"]*)\" Key$")
	public void click_on_Last_Name_textbox_and_click_on_the_Key(String arg1) throws Throwable {
		usersPageObj.txtLastName.sendKeys(Keys.TAB);
	}
	@Then("^: Verify the Mandatory validation message for Last Name field as \"([^\"]*)\"$")
	public void verify_the_Mandatory_validation_message_for_Last_Name_field_as(String arg1) throws Throwable {
		usersPageObj.verifyLastNmMandatoryValMsg();
	}
	@Given("^: Click on Last Name textbox and enter Last Name as \"([^\"]*)\"$")
	public void click_on_Last_Name_textbox_and_enter_Last_Name_as(String arg1) throws Throwable {
		String lastName= excelData(8,4);
		usersPageObj.lastNameSendkeys(lastName);
	}
	@Then("^: Verify Minimum validation message of Last Name textbox as \"([^\"]*)\"$")
	public void verify_Minimum_validation_message_of_Last_Name_textbox_as(String arg1) throws Throwable {
		usersPageObj.verifyLastNmMinVaMsg();
	}
	@Given("^: Click on Last Name textbox and enter Last Name value more than (\\d+) characters$")
	public void click_on_Last_Name_textbox_and_enter_Last_Name_value_more_than_characters(int arg1) throws Throwable {
		String lastName= excelData(9,4);
		usersPageObj.lastNameSendkeys(lastName);
	}
	@Then("^: Verify Maximum validation message of Last Name textbox as \"([^\"]*)\"$")
	public void verify_Maximum_validation_message_of_Last_Name_textbox_as(String arg1) throws Throwable {
		usersPageObj.verifyLastNmMaxVakMsg();
	}
	@Given("^: Click on Email Address textbox and click on the \"([^\"]*)\" Key$")
	public void click_on_Email_Address_textbox_and_click_on_the_Key(String arg1) throws Throwable {
		usersPageObj.txtEmailAddress.sendKeys(Keys.TAB);
	}
	@Then("^: Verify the Mandatory validation message for Email Address textbox as \"([^\"]*)\"$")
	public void verify_the_Mandatory_validation_message_for_Email_Address_textbox_as(String arg1) throws Throwable {
		usersPageObj.verifyInvalidEmailAddManValMsg();
	}
	@Given("^: Click on Email Address textbox and enter Email Address as \"([^\"]*)\"$")
	public void click_on_Email_Address_textbox_and_enter_Email_Address_as(String arg1) throws Throwable {
		String email= excelData(10,5);
		usersPageObj.txtEmailSendkeys(email);
	}
	@Then("^: Verify the validation message for Email Address textbox as \"([^\"]*)\"$")
	public void verify_the_validation_message_for_Email_Address_textbox_as(String arg1) throws Throwable {
		usersPageObj.verifyInValidEmailAdd();
	}
	@Given("^: Click on Email Address textbox and enter Duplicate Email Address$")
	public void click_on_Email_Address_textbox_and_enter_Duplicate_Email_Address() throws Throwable {
		String email= excelData(11,5);
		usersPageObj.txtEmailSendkeys(email);
	}
	@Then("^: Verify Unique validation message of Email Address textbox as \"([^\"]*)\"$")
	public void verify_Unique_validation_message_of_Email_Address_textbox_as(String arg1) throws Throwable {
		usersPageObj.verifyDuplicateValMsg();
	}
	@Given("^: Enter valid Username into Username textbox$")
	public void enter_valid_Username_into_Username_textbox() throws Throwable {
		String userName= excelData(12,2);
		usersPageObj.userNameSendkeys(userName);
	}
	@Given("^: Enter valid First Name into First Name textbox$")
	public void enter_valid_First_Name_into_First_Name_textbox() throws Throwable {
		String firstName = excelData(12, 3);
		usersPageObj.firstNameSendkeys(firstName);
	}
	@Given("^: Enter valid Last Name into Last Name textbox$")
	public void enter_valid_Last_Name_into_Last_Name_textbox() throws Throwable {
		String lastName = excelData(12,4);
		usersPageObj.lastNameSendkeys(lastName);
	}
	@Given("^: Enter valid Email Address into Email Address textbox$")
	public void enter_valid_Email_Address_into_Email_Address_textbox() throws Throwable {
		String emailAdd = excelData(12,5);
		usersPageObj.txtEmailSendkeys(emailAdd);
	}
	@Given("^: Enter valid Department into Department textbox$")
	public void enter_valid_Department_into_Department_textbox() throws Throwable {
		String department = excelData(12,10);
		usersPageObj.departmentSendKeys(department);
	}
	@Given("^: Select Generate Password as \"([^\"]*)\"$")
	public void select_Generate_Password_as(String arg1) throws Throwable {   
		String gegnerateBy = excelData(12,11);
		usersPageObj.selectGeneratePassword(gegnerateBy);
	}
	@Given("^: Enter valid new password$")
	public void enter_valid_new_password() throws Throwable {
		String newPassword = excelData(12,12);
		usersPageObj.sendNewPassword(newPassword); 
	}
	@Given("^: Enter valid confirm password$")
	public void enter_valid_confirm_password() throws Throwable {
		String confirmPassword = excelData(12,13);
		usersPageObj.sendConfirmPassword(confirmPassword);
	}
	@Given("^: Select Roles to Create User$")
	public void select_Roles_to_Create_User() throws Throwable {
		String roleName = excelData(12,14);
		usersPageObj.selectRole(roleName);
	}
	@Given("^: Select Sites to Create User$")
	public void select_Sites_to_Create_User() throws Throwable {
		usersPageObj.selectSite();
	}
	
	@Given("^: Select Sites to Create User for correct data$")
	public void select_Sites_to_Create_User_for_correct_data() throws Throwable {
		usersPageObj.selectSiteForCorrectData(excelData(23, 23));
	}
	
	@Given("^: Click on Save button of Users$")
	public void click_on_Save_button() throws Throwable {
		usersPageObj.clickonSaveBtn();
	}
	@Then("^: Verify that Add User functionality should be working properly when a user click on Save button$")
	public void verify_that_Add_User_functionality_should_be_working_properly_when_a_user_click_on_Save_button() throws Throwable {
		String newUserName = excelData(12,2);
		String newPassword = excelData(12,12);
		usersPageObj.verifyUesrCreation(newUserName,newPassword);
	}
	@Given("^: Edit User Detail and click on save button$")
	public void edit_User_Detail_and_click_on_save_button() throws Throwable {
		usersPageObj.clickonSaveBtn();
	}
	
	@Given("^: Edit User Detail and click on save button for simple function$")
	public void edit_User_Detail_and_click_on_save_button_for_simple_function() throws Throwable {
		usersPageObj.clickonSaveBtnForSimple();
	}
	
	@Then("^: Verify that record should be updated and redirected to the Users listing screen$")
	public void verify_that_record_should_be_updated_and_redirected_to_the_Users_listing_screen() throws Throwable {
		usersPageObj.verifyUserlistingBreadcrumbs();
	}
	@Given("^: Click on Reset link of first record from the Action column$")
	public void click_on_Reset_link_of_first_record_from_the_Action_column() throws Throwable {
		usersPageObj.clickonResetLink();
	}
	@Then("^: Verify that Reset link should be clickable$")
	public void verify_that_Reset_link_should_be_clickable() throws Throwable {
		usersPageObj.verifyActionpgbreadcrumb();
	}
	@Then("^: Verify that a loggedin user is allowed to reset access of active users$")
	public void verify_that_a_loggedin_user_is_allowed_to_reset_access_of_active_users() throws Throwable {
		usersPageObj.verifyResetFunctionality();
	}
	@Given("^: Click on Reset link of Active User record from the Action column$")
	public void click_on_Reset_link_of_Active_User_record_from_the_Action_column() throws Throwable {
		usersPageObj.clickonResetLink();
	}
	@Given("^: Select By Admin option from Generate Password dropdown$")
	public void select_By_Admin_option_from_Generate_Password_dropdown() throws Throwable {
		usersPageObj.selectGeneratePassword();
	}
	@Given("^: Click on the New Password textbox and enter the new password value$")
	public void click_on_the_New_Password_textbox_and_enter_the_new_password_value() throws Throwable {
		usersPageObj.resetPageNewPwd.sendKeys(Keys.TAB);
	}
	@Then("^: Verify the Mandatory validation message for password field as \"([^\"]*)\"$")
	public void verify_the_Mandatory_validation_message_for_password_field_as(String arg1) throws Throwable {
		usersPageObj.verifyNewPassMandatoryValMsg();
	}
	@Given("^: Click on the New Password textbox and eneter enter New Password as \"([^\"]*)\"$")
	public void click_on_the_New_Password_textbox_and_eneter_enter_New_Password_as(String arg1) throws Throwable {
		usersPageObj.enterNewPassword();
	}
	@Then("^: Verify that Invalid vaildation message for New Password should be displayed as \"([^\"]*)\" \\+$")
	public void verify_that_Invalid_vaildation_message_for_New_Password_should_be_displayed_as(String arg1) throws Throwable {
		usersPageObj.verifyInvalidNewPassword();
	}
	@Given("^: Click on the Confirm Password textbox and click on \"([^\"]*)\" Key$")
	public void click_on_the_Confirm_Password_textbox_and_click_on_Key(String arg1) throws Throwable {
		usersPageObj.resetPageConfirmPwd.sendKeys(Keys.TAB);
	}
	@Then("^: Verify the Mandatory validation message for confirm password field as \"([^\"]*)\"$")
	public void verify_the_Mandatory_validation_message_for_confirm_password_field_as(String arg1) throws Throwable {
		usersPageObj.verifyNewConfirmPassMandatoryValMsg();
	}
	@Given("^: Click on New Password textbox and enter New Password as \"([^\"]*)\"$")
	public void click_on_New_Password_textbox_and_enter_New_Password_as(String arg1) throws Throwable {
		usersPageObj.enterNewPassword();
	}
	
	@Given("^: Click on Confirm Password textbox and enter Confirm Password as \"([^\"]*)\"$")
	public void click_on_Confirm_Password_textbox_and_enter_Confirm_Password_as(String arg1) throws Throwable {
		usersPageObj.enterConfirmPassword();
	}
	@Then("^: Verify that validation message should be displayed as Confirm Password and New Password must be exactly same\\.$")
	public void verify_that_validation_message_should_be_displayed_as_Confirm_Password_and_New_Password_must_be_exactly_same() throws Throwable {
		usersPageObj.verifyConfirmPasswordMsg();
	}
	@Given("^: Click on New Password textbox and enter value into New Password textbox$")
	public void click_on_New_Password_textbox_and_enter_value_into_New_Password_textbox() throws Throwable {
		usersPageObj.enterNewPasssword(excelData(15,18));
	}
	@Given("^: Click on Confirm Password textbox and enter value into Confirm Password textbox$")
	public void click_on_Confirm_Password_textbox_and_enter_value_into_Confirm_Password_textbox() throws Throwable {
		usersPageObj.enterConfirmPasssword();
	}
	@Given("^: Click on Submit button$")
	public void click_on_Submit_button() throws Throwable {
		usersPageObj.clickonSubmitBtn();
	}
	@Then("^: Verify that a user is allowed to reset password by selecting By Admin option$")
	public void verify_that_a_user_is_allowed_to_reset_password_by_selecting_By_Admin_option() throws Throwable 
	{
		usersPageObj.verifyPasswordReset(userNameForReset);
	}
	@Given("^: Select Auto Generate option from Generate Password dropdown$")
	public void select_Auto_Generate_option_from_Generate_Password_dropdown() throws Throwable {
		System.out.println("Test");
	}
	@Then("^: Verify that a user is allowed to reset password by selecting Auto Generate option$")
	public void verify_that_a_user_is_allowed_to_reset_password_by_selecting_Auto_Generate_option() throws Throwable {
		usersPageObj.autoGeneratePassword();
	}
	@Given("^: Click on Sorting icon of the Username column for descending order sorting$")
	public void click_on_Sorting_icon_of_the_Username_column_for_descending_order_sorting() throws Throwable {
		System.out.println("test");
	}

	@Given("^: Enter valid User name into search box of Users listing screen$")
	public void enter_valid_User_name_into_search_box_of_Users_listing_screen() throws Throwable {
		usersPageObj.enterValidUsername(excelDataLogin(2, 1));
	}
	
	@Given("^: Enter valid User name into search box of Users listing screen for reset function$")
	public void enter_valid_User_name_into_search_box_of_Users_listing_screen_for_reset_function() throws Throwable {
		userNameForReset = excelData(22, 1);
		usersPageObj.enterValidUsername(excelData(22, 1));
	}

	@Given("^: Click on Cross Button of User Screen$")
	public void click_on_Cross_Button_of_User_Screen() throws Throwable {
		usersPageObj.clickOnCrossBtnOfSearch();
	}

	@Then("^: Verify entered User Name in Search box should be cleared$")
	public void verify_entered_User_Name_in_Search_box_should_be_cleared() throws Throwable {
		usersPageObj.verifyCrossBtnFun();
	}

	@Given("^: Select page size Ten in User Screen$")
	public void select_page_size_Ten_in_User_Screen() throws Throwable {
		usersPageObj.selectPageSizeDrpDwn("10");
	}

	@Then("^: Verify every page grid should be display Ten records$")
	public void verify_every_page_grid_should_be_display_Ten_records() throws Throwable {
		usersPageObj.verifyPageSizeDrpDwn(10);
	}

	@Given("^: Select page size Twenty in User Screen$")
	public void select_page_size_Twenty_in_User_Screen() throws Throwable {
		usersPageObj.selectPageSizeDrpDwn("20");
	}

	@Then("^: Verify every page grid should be display Twenty records$")
	public void verify_every_page_grid_should_be_display_Twenty_records() throws Throwable {
		usersPageObj.verifyPageSizeDrpDwn(20);
	}

	@Given("^: Select page size Fifty in User Screen$")
	public void select_page_size_Fifty_in_User_Screen() throws Throwable {
		usersPageObj.selectPageSizeDrpDwn("50");
	}

	@Then("^: Verify every page grid should be display Fifty records$")
	public void verify_every_page_grid_should_be_display_Fifty_records() throws Throwable {
		usersPageObj.verifyPageSizeDrpDwn(50);
	}

	@Then("^: Verify Pagination with Ten records$")
	public void verify_Pagination_with_Ten_records() throws Throwable {
		usersPageObj.verifyPaginationWithRecds(10);
	}

	@Then("^: Verify Pagination with Twenty records$")
	public void verify_Pagination_with_Twenty_records() throws Throwable {
		usersPageObj.verifyPaginationWithRecds(20);
	}

	@Then("^: Verify Pagination with Fifty records$")
	public void verify_Pagination_with_Fifty_records() throws Throwable {
		usersPageObj.verifyPaginationWithRecds(50);
	}

	@Then("^: Verify Audit Trail link should display in all users$")
	public void verify_Audit_Trail_link_should_display_in_all_users() throws Throwable {
		usersPageObj.verifyAuditTrailLinkForAllUsers();
	}

	@Given("^: Enter valid User Name into search box of Users listing screen$")
	public void enter_valid_User_Name_into_search_box_of_Users_listing_screen() throws Throwable {
		usersPageObj.enterValidUsername(excelData(17, 2));
	}

	@Then("^: Verify search result for User Name$")
	public void verify_search_result_for_User_Name() throws Throwable {
		usersPageObj.verifySearchForUserName(excelData(17, 2));
	}

	@Given("^: Enter valid User role into search box of Users listing screen$")
	public void enter_valid_User_role_into_search_box_of_Users_listing_screen() throws Throwable {
		usersPageObj.enterValidUsername(excelData(18, 8));
	}

	@Then("^: Verify search result for User role$")
	public void verify_search_result_for_User_role() throws Throwable {
		usersPageObj.verifySearchForUserRole(excelData(18, 8));
	}

	@Given("^: Enter valid User Full Name into search box of Users listing screen$")
	public void enter_valid_User_Full_Name_into_search_box_of_Users_listing_screen() throws Throwable {
		usersPageObj.enterValidUsername(excelData(19, 21));
	}

	@Then("^: Verify search result for User Full Name$")
	public void verify_search_result_for_User_Full_Name() throws Throwable {
		usersPageObj.verifySearchForUserFullName(excelData(19, 21));
	}

	@Given("^: Enter valid Department into search box of Users listing screen$")
	public void enter_valid_Department_into_search_box_of_Users_listing_screen() throws Throwable {
		usersPageObj.enterValidUsername(excelData(20, 10));
	}

	@Then("^: Verify search result for Department$")
	public void verify_search_result_for_Department() throws Throwable {
		usersPageObj.verifySearchForDepartment(excelData(20, 10));
	}

	@Given("^: Click on User Name of Search Result$")
	public void click_on_User_Name_of_Search_Result() throws Throwable {
		usersPageObj.clickOnFirstUserNameOf();
	}

	@Given("^: Click on every field of user and Edit$")
	public void click_on_every_field_of_user_and_Edit() throws Throwable {
		usersPageObj.clickOnEveryFieldOfUser(excelData(21, 3),excelData(21, 22),excelData(21, 4),excelData(21, 10),excelData(21, 5));
	}


	@Then("^: Verify Old value and New value field in Audit Trail Screen$")
	public void verify_Old_value_and_New_value_field_in_Audit_Trail_Screen() throws Throwable {
		usersPageObj.verifyOldAndNewFirstName(excelData(23, 23));
	}

	@Given("^: Enter random valid Email Address into Email Address$")
	public void enter_random_valid_Email_Address_into_Email_Address_textbox() throws Throwable {
		usersPageObj.enterEmailAddress();
	}

	@Then("^: Verify The System should display correct data in Audit Trail Screen$")
	public void Verify_The_System_should_display_correct_data_in_Audit_Trail_Screen() throws Throwable {
		usersPageObj.verifyCorrectDataofAuditTrail(excelData(23, 23));
	}

	@Given("^: Get First User Name in user screen$")
	public void get_First_User_Name_in_user_screen() throws Throwable {
		usersPageObj.getFirstUsername();
	}

	@Then("^: Verify Audit Trail should display correct user name$")
	public void verify_Audit_Trail_should_display_correct_user_name() throws Throwable {
		usersPageObj.verifyAuditTrailUserName();
	}

	@Given("^: Get the version of that User$")
	public void get_the_version_of_that_User() throws Throwable {
		usersPageObj.getFirstVersionNo();
	}

	@Then("^: Verify Version should be updated in Audit Trail Page$")
	public void verify_Version_should_be_updated_in_Audit_Trail_Page() throws Throwable {
		usersPageObj.verifyUpdatedVersionNo();
	}

	@Then("^: Verify Modified by field should Display the correct value$")
	public void verify_Modified_by_field_should_Display_the_correct_value() throws Throwable {
		usersPageObj.verifyModifiedByNameField();
	}

	@Then("^: Verify Date and Time should be Correct in Audit Trail Page$")
	public void verify_Date_and_Time_should_be_Correct_in_Audit_Trail_Page() throws Throwable {
		usersPageObj.verifyDateAndTime();
	}

	@Given("^: Click on License Agreements button$")
	public void click_on_License_Agreements_button() throws Throwable {
		usersPageObj.clickOnLicenseAggBtn();
	}

	@Then("^: Verify License Agreements Screen should be opened$")
	public void verify_License_Agreements_Screen_should_be_opened() throws Throwable {
		usersPageObj.verifyLicenseAggBtnFunction();
	}

	@Given("^: Click on User Name of Search Result for old and new value$")
	public void click_on_User_Name_of_Search_Result_for_old_and_new_value() throws Throwable {
		usersPageObj.clickOnFirstUserNameForOldAndNew();
	}

	@Given("^: Get Audit Trail Entries in Audit Trail Screen$")
	public void get_Audit_Trail_Entries_in_Audit_Trail_Screen() throws Throwable {
		usersPageObj.getAuditTrailEntries();
	}


}