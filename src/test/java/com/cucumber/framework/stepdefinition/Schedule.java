package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import com.cucumber.framework.helper.PageObject.CommonFunctionPageObject;
import com.cucumber.framework.helper.PageObject.EntityRecordsPageObject;
import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.SchedulePageObject;
import com.cucumber.framework.helper.PageObject.SmartAlertsPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Schedule {
	public ExcelUtils excel;
	public SchedulePageObject schedulePageObject;

	public Schedule() {
		schedulePageObject = new SchedulePageObject(ObjectRepo.driver);
	}
	
	public String excelData(int rowVal, int colVal) throws Exception {
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Schedule");
		System.err.println();
		return excel.readXLSFile("Schedule", rowVal, colVal);
	}
	
	@Given("^: Click on Schedule Tile$")
	public void click_on_Schedule_Tile() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.clickScheduleTile();
	}

	@Then("^: Verify that the system should be redirected to the Schedule load$")
	public void verify_that_the_system_should_be_redirected_to_the_Schedule_load() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyScheduleHeading();
	}

	@Given("^: Click on dropdown of App listing$")
	public void click_on_dropdown_of_App_listing() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.clickAppDrpDwn();
	}

	@Then("^: Verify that App dropdown should be clickable when user click on dropdown$")
	public void verify_that_App_dropdown_should_be_clickable_when_user_click_on_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyAppDrpDwnClickable();
	}

	@Then("^: Verify that Name of Second Tab in Schedule screen should be displayed as Review Completed Apps$")
	public void verify_that_Name_of_Second_Tab_in_Schedule_screen_should_be_displayed_as_Review_Completed_Apps() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyReviewCompletedAppLabel();
	}

	@Then("^: Verify that Name of First Tab in Schedule screen should be displayed as Start Scheduled Apps$")
	public void verify_that_Name_of_First_Tab_in_Schedule_screen_should_be_displayed_as_Start_Scheduled_Apps() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyStartScheduleAppsLabel();
	}

	@Given("^: Select a specific App$")
	public void select_a_specific_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.searchSpecificApp(excelData(1, 1));
	}

	@Given("^: Click on Date dropdown$")
	public void click_on_Date_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.clickDateDrpDwn();
	}

	@Given("^: Select \"([^\"]*)\" option from the dropdown of the Date field$")
	public void select_option_from_the_dropdown_of_the_Date_field(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.selectThisYearOptionDateDrpDwn();
	}

	@Given("^: Click on Filter by Status dropdown$")
	public void click_on_Filter_by_Status_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.clickFilterByStatusDrpDwn();
	}

	@Given("^: Select Completed option from the dropdown of Filter by Status field$")
	public void select_option_from_the_dropdown_of_Filter_by_Status_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.selectCompletedOptionFilterByStatusDrpDwn();
	}

	@Then("^: Verify that System should display all records of Hourly Frequency with completed status for specific App$")
	public void verify_that_System_should_display_all_records_of_Hourly_Frequency_with_completed_status_for_specific_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyHourlyAndCompletedData();
	}
	
	@Given("^: Select Upcoming option from the dropdown of Filter by Status field$")
	public void select_Upcoming_option_from_the_dropdown_of_Filter_by_Status_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.selectUpcomingOptionFilterByStatusDrpDwn();
	}

	@Then("^: Verify that System should display all Upcoming records in Start Schedule Apps listing when all apps are selected$")
	public void verify_that_System_should_display_all_Upcoming_records_in_Start_Schedule_Apps_listing_when_all_apps_are_selected() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyHourlyAndUpcomingData();
	}
	
	@Given("^: Select Overdue option from the dropdown of Filter by Status field$")
	public void select_Overdue_option_from_the_dropdown_of_Filter_by_Status_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.selectOverdueOptionFilterByStatusDrpDwn();
	}

	@Then("^: Verify that System should display all Overdue records in Start Schedule Apps listing when all apps are selected$")
	public void verify_that_System_should_display_all_Overdue_records_in_Start_Schedule_Apps_listing_when_all_apps_are_selected() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyHourlyAndOverdueData();
	}
	
	@Given("^: Select Incomplete option from the dropdown of Filter by Status field$")
	public void select_Incomplete_option_from_the_dropdown_of_Filter_by_Status_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.selectIncompleteOptionFilterByStatusDrpDwn();
	}

	@Then("^: Verify that System should display all Incomplete records in Start Schedule Apps listing when all apps are selected$")
	public void verify_that_System_should_display_all_Incomplete_records_in_Start_Schedule_Apps_listing_when_all_apps_are_selected() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyHourlyAndIncompleteData();
	}

	@Given("^: Select Ready to Perform option from the dropdown of Filter by Status field$")
	public void select_Ready_to_Perform_option_from_the_dropdown_of_Filter_by_Status_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.selectReadyToPerformOptionFilterByStatusDrpDwn();
	}

	@Then("^: Verify that System should be displayed all Ready to Perform records in Start Schedule Apps listing when all apps are selected$")
	public void verify_that_System_should_be_displayed_all_Ready_to_Perform_records_in_Start_Schedule_Apps_listing_when_all_apps_are_selected() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyHourlyAndReadyToPerformData();
	}
	
	@Given("^: Click on Search by dropdown$")
	public void click_on_Search_by_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.clickSearchByDrpDwn();
	}

	@Given("^: Select Master App option$")
	public void select_Master_App_option() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.selectMasterAppsFromSearchByDrpDwn();
	}

	@Then("^: Verify that System should display all Upcoming records in Start Schedule Apps listing when Master apps are selected$")
	public void verify_that_System_should_display_all_Upcoming_records_in_Start_Schedule_Apps_listing_when_Master_apps_are_selected() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyDisplayUpcomingMasterAppData();
	}
	
	@Then("^: Verify that System should display all Overdue records in Start Schedule Apps listing when Master apps are selected$")
	public void verify_that_System_should_display_all_Overdue_records_in_Start_Schedule_Apps_listing_when_Master_apps_are_selected() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyDisplayOverdueMasterAppData();
	}

	@Then("^: Verify that System should display all Incomplete records in Start Schedule Apps listing when Master apps are selected$")
	public void verify_that_System_should_display_all_Incomplete_records_in_Start_Schedule_Apps_listing_when_Master_apps_are_selected() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyDisplayIncompleteMasterAppData();
	}

	@Then("^: Verify that System should display all Completed records in Start Schedule Apps listing when all Master apps selected$")
	public void verify_that_System_should_display_all_Completed_records_in_Start_Schedule_Apps_listing_when_all_Master_apps_selected() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyDisplayCompleteMasterAppData();
	}

	@Then("^: Verify that system should be displayed all Ready to Perform records in Start Schedule Apps listing when Master apps are selected$")
	public void verify_that_system_should_be_displayed_all_Ready_to_Perform_records_in_Start_Schedule_Apps_listing_when_Master_apps_are_selected() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyDisplayReadyToPerformMasterAppData();
	}
	
	@Given("^: Click on Collapse Button$")
	public void click_on_Collapse_Button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.clickCollapseLink();
	}

	@Given("^: Click on Expand Button$")
	public void click_on_Expand_Button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.clickExpandLink();
	}

	@Then("^: Verify system should expand Filter By section when user click on Expand Button$")
	public void verify_system_should_expand_Filter_By_section_when_user_click_on_Expand_Button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyExpandFunctionality();
	}

	@Then("^: Verify system should Hide Filter By section when user click on Collape Button$")
	public void verify_system_should_Hide_Filter_By_section_when_user_click_on_Collape_Button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyCollapseFunctionality();
	}

	@Then("^: Verify that system should display all the records of Current Week when user select This Week option$")
	public void verify_that_system_should_display_all_the_records_of_Current_Week_when_user_select_This_Week_option() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyThisWeekData();
	}

	@Then("^: Verify that system should display all the records of Current Month when user select This Month option$")
	public void verify_that_system_should_display_all_the_records_of_Current_Month_when_user_select_This_Month_option() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyThisMonthData();
	}

	@Then("^: Verify that system should display all the records of Current Year when user select This Year option$")
	public void verify_that_system_should_display_all_the_records_of_Current_Year_when_user_select_This_Year_option() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyThisYearData();
	}

	@Given("^: Click on the first record of App/Master App column in Start Schedule Apps listing$")
	public void click_on_the_first_record_of_App_Master_App_column_in_Start_Schedule_Apps_listing() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.selectFirstAppLink();
	}

	@Then("^: Verify that system should be redirected to the particular App when user click on record of App/Master App column in Start Schedule Apps listing$")
	public void verify_that_system_should_be_redirected_to_the_particular_App_when_user_click_on_record_of_App_Master_App_column_in_Start_Schedule_Apps_listing() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.verifyAppName();
	}
	
	@Given("^: Select This Week option from the dropdown of the Date field$")
	public void select_This_Week_option_from_the_dropdown_of_the_Date_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.selectThisWeekOption();
	}

	@Given("^: Select This Month option from the dropdown of the Date field$")
	public void select_This_Month_option_from_the_dropdown_of_the_Date_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.selectThisMonthOption();
	}

	@Given("^: Select This Year option from the dropdown of the Date field$")
	public void select_This_Year_option_from_the_dropdown_of_the_Date_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    schedulePageObject.selectThisYearOption();
	}
	
}
