package com.cucumber.framework.stepdefinition;

import java.awt.RenderingHints.Key;

import org.openqa.selenium.Keys;

import com.cucumber.framework.helper.PageObject.QuestionsPageObject;

import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class Questions
{
	private QuestionsPageObject questionPageObj;
	public ExcelUtils excel;

	public Questions()
	{
		questionPageObj = new QuestionsPageObject(ObjectRepo.driver);
	}
	public String excelData(int rowVal, int colVal) throws Exception {
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Questions");
		System.err.println();
		return excel.readXLSFile("Questions", rowVal, colVal);
	}
	
	@Given("^: Click on Questions Tile$")
	public void click_on_Questions_Tile() throws Throwable {
		questionPageObj.clickOnQuestionsTile();
	}
	@Then("^: Verify the module name as Questions$")
	public void verify_the_module_name_as_Questions() throws Throwable {

		questionPageObj.verifyQuestionModuleName();
	}
	@Given("^: Click on Filter By dropdown$")
	public void click_on_Filter_By_dropdown() throws Throwable {
		questionPageObj.verifyQuestionsDrpFilter();

	}
	@Then("^: Verify the Filter By dropdown should be clickable$")
	public void verify_the_Filter_By_dropdown_should_be_clickable() throws Throwable {
		questionPageObj.verifyQuestionsDrpClickable();
	}
	@Given("^: Click on Filter By dropdown and select Question Type option$")
	public void click_on_Filter_By_dropdown_and_select_Question_Type_option() throws Throwable {
		questionPageObj.selectQuestionTypeOption();
	}
	@Given("^: Click on Question Type dropdown$")
	public void click_on_Question_Type_dropdown() throws Throwable {
		questionPageObj.verifyQuestionsTypeDrpFilter();
	}
	@Then("^: Verify the Question Type dropdown should be clickable$")
	public void verify_the_Question_Type_dropdown_should_be_clickable() throws Throwable {
		questionPageObj.verifyQuestionsTypeClickable();
	}
	@Given("^: Click on Sorting icon of the Questions column$")
	public void click_on_Sorting_icon_of_the_Questions_column() throws Throwable {
		questionPageObj.verifyAscendingOrder();
	}
	@Then("^: Verify that all the records of Questions column display in ascending order when click on sorting icon$")
	public void verify_that_all_the_records_of_Questions_column_display_in_ascending_order_when_click_on_sorting_icon() throws Throwable {		
		String sortingColName = excelData(1, 1);
		questionPageObj.verifyAscendingOrderResult(sortingColName);
	}
	@Then("^: Verify the search results of Questions listing page$")
	public void verify_the_search_results_of_Questions_listing_page() throws Throwable {
		questionPageObj.verifysearch(excelData(2, 2));
	}
	@Given("^: Click on the first record of Question listing grid$")
	public void click_on_the_first_record_of_Question_listing_grid() throws Throwable {
		questionPageObj.clickToFirstQuestion();
	}
	@Then("^: Verify the Breadcrumb of Edit Question as \"([^\"]*)\"$")
	public void verify_the_Breadcrumb_of_Edit_Question_as(String arg1) throws Throwable {
		questionPageObj.verifyEditQuestionBreadCrumbs();
	}
	@Given("^: Click on Save button of Questions screen$")
	public void click_on_Save_button_of_Questions_screen() throws Throwable {
		questionPageObj.clickToSaveButton();
	}
	@Then("^: Verify the Save button should be clickable$")
	public void verify_the_Save_button_should_be_clickable() throws Throwable {
		questionPageObj.verifyQuestionsSaveButton();
	}
	@Given("^: Click on Cancel button of Questions screen$")
	public void click_on_Cancel_button_of_Questions_screen() throws Throwable {
		questionPageObj.clickToCancelButton();
	}
	@Then("^: Verify the Cancel button should be clickable and redirect to Questions listing grid$")
	public void verify_the_Cancel_button_should_be_clickable() throws Throwable {
		questionPageObj.verifyQuestionPageRedirection();
	}
	@Then("^: Verify the Color of Save button$")
	public void verify_the_Color_of_Save_button() throws Throwable {
		questionPageObj.verifySaveButtonColor();
	}
	@Then("^: Verify the Color of Cancel button$")
	public void verify_the_Color_of_Cancel_button() throws Throwable {
		questionPageObj.verifyCancelButtonColor();
	}
	@Given("^: Click on the Create Copy Icon of first record of Question listing grid$")
	public void click_on_the_Create_Copy_Icon_of_first_record_of_Question_listing_grid() throws Throwable {
	}
	@Then("^: Verify the Label of Copy pop-up should be as \"([^\"]*)\"$")
	public void verify_the_Label_of_Copy_pop_up_should_be_as(String arg1) throws Throwable {
		questionPageObj.CopyClick("Questions Name *");
	}
	@Then("^: Verify the Default Value of Copy pop-up should be appended with \"([^\"]*)\"$")
	public void verify_the_Default_Value_of_Copy_pop_up_should_be_appended_with(String arg1) throws Throwable {
		questionPageObj.CopyDefaultValue();
	}
	@Given("^: Click on OK button of the Copy popup$")
	public void click_on_OK_button_of_the_Copy_popup() throws Throwable {
	}
	@Then("^: Verify that the system should Create Copy of Question$")
	public void verify_that_the_system_should_Create_Copy_of_Question() throws Throwable {
		questionPageObj.CreateCopy();
	}
	@Given("^: Click on \"([^\"]*)\" button of Questions listing screen$")
	public void click_on_button_of_Questions_listing_screen(String arg1) throws Throwable {
		questionPageObj.clickonAddNewBtn();
	}
	@Given("^: Click on Question Name textbox and press the \"([^\"]*)\" Key$")
	public void click_on_Question_Name_textbox_and_press_the_Key(String arg1) throws Throwable {
		Thread.sleep(1000);
		questionPageObj.questionsNameTitle.sendKeys(Keys.TAB);
	}
	@Then("^: Verify the validation message as Question Name is required\\.$")
	public void verify_the_validation_message_as_Question_Name_is_required() throws Throwable {
		questionPageObj.verifyQuestionRequiredVal();
	}
	@Given("^: Enter Question Name value less than (\\d+) chracters$")
	public void enter_Question_Name_value_less_than_chracters(int arg1) throws Throwable {
		String questionminival = excelData(5, 2);
		questionPageObj.questionsNamesendkeys(questionminival);
	}
	@Then("^: Verify the validation message as Question Name must be at least (\\d+) characters\\.$")
	public void verify_the_validation_message_as_Question_Name_must_be_at_least_characters(int arg1) throws Throwable {
		questionPageObj.verifyQuestionMinimumVal();
	}
	@Given("^: Enter Question Name value more than (\\d+) characters$")
	public void enter_Question_Name_value_more_than_characters(int arg1) throws Throwable {
		String questionmaxval = excelData(6, 2);
		questionPageObj.questionsNamesendkeys(questionmaxval);
	}
	@Then("^: Verify validation message Question Name cannot be more than (\\d+) characters\\.$")
	public void verify_validation_message_Question_Name_cannot_be_more_than_characters(int arg1) throws Throwable {
		questionPageObj.verifyQuestionMaximumVal();
	}
	@Given("^: Enter a valid Question Name$")
	public void enter_a_valid_Question_Name() throws Throwable {
		String questionnoval = excelData(7, 2);
		questionPageObj.questionsNamesendkeys(questionnoval);
	}
	@Then("^: Verify system should accept the Question name value without any validation message$")
	public void verify_system_should_accept_the_Question_name_value_without_any_validation_message() throws Throwable {
		questionPageObj.verifyQuestionNoVal();   
	}
	@Given("^: Enter duplicate Question Name$")
	public void enter_duplicate_Question_Name() throws Throwable {
		String questionuniqueval = excelData(8, 2);
		questionPageObj.questionsNamesendkeys(questionuniqueval);
	}
	@Then("^: Verify the Unique validation message for the Question Name Textbox as Question Name must be unique\\.$")
	public void verify_the_Unique_validation_message_for_the_Question_Name_Textbox_as_Question_Name_must_be_unique() throws Throwable {
		questionPageObj.verifyQuestionUniqueVal();
	}
	@Given("^: Click on the Question Tag text box and press the \"([^\"]*)\" Key$")
	public void click_on_the_Question_Tag_text_box_and_press_the_Key(String arg1) throws Throwable {
		questionPageObj.questionsTag.sendKeys(Keys.TAB);
	}
	@Then("^: Verify validation message as Question Tag is required\\.$")
	public void verify_validation_message_as_Question_Tag_is_required() throws Throwable {
		questionPageObj.verifyQuestionTagRequiredVal();
	}
	@Given("^: Enter Question Tag value less than (\\d+) characters$")
	public void enter_Question_Tag_value_less_than_characters(int arg1) throws Throwable {
		String questiontagminival = excelData(9, 2);
		questionPageObj.questionsTagsendkeys(questiontagminival);
	}
	@Then("^: Verify validation message as Question Tag must be at least (\\d+) characters\\.$")
	public void verify_validation_message_as_Question_Tag_must_be_at_least_characters(int arg1) throws Throwable {
		questionPageObj.verifyQuestionTagMinimumVal();
	}
	@Given("^: Enter Question Tag value more than (\\d+) characters$")
	public void enter_Question_Tag_value_more_than_characters(int arg1) throws Throwable {
		String questiontagmaxval = excelData(10, 2);
		questionPageObj.questionsTagsendkeys(questiontagmaxval);
	}
	@Then("^: Verify validation message as Question Tag cannot be more than (\\d+) characters\\.$")
	public void verify_validation_message_as_Question_Tag_cannot_be_more_than_characters(int arg1) throws Throwable {
	    questionPageObj.verifyQuestionTagMaximumVal();
	}
	@Given("^: Enter the duplicate value into the Question Tag text box$")
	public void enter_the_duplicate_value_into_the_Question_Tag_text_box() throws Throwable {
		String questionuniqueval = excelData(11, 2);
		questionPageObj.questionsTagsendkeys(questionuniqueval);
		questionPageObj.clickonAddTagBtn();
		questionPageObj.questionsTagsendkeys(questionuniqueval);
		questionPageObj.clickonAddTagBtn();
	}
	@Then("^: Verify message as Question Tag already exists$")
	public void verify_message_as_Question_Tag_already_exists() throws Throwable {
	    questionPageObj.verifyQuestionTagUniqueVal();
	}
	@Given("^: Enter valid data into the Question Tag text box$")
	public void enter_valid_data_into_the_Question_Tag_text_box() throws Throwable {
		String questiontagnoval = excelData(12, 2);
		questionPageObj.questionsTagsendkeys(questiontagnoval);
		questionPageObj.clickonAddTagBtn();
	}
	@Then("^: Verify the system should accept the Question tag value without any validation message$")
	public void verify_the_system_should_accept_the_Question_tag_value_without_any_validation_message() throws Throwable {
	    questionPageObj.verifyQuestionTagNoVal();
	}
	@Given("^: Click on Instructions for user checkbox$")
	public void click_on_Instructions_for_user_checkbox() throws Throwable {
	    questionPageObj.selectInstructionForUser();
	}
	@Then("^: Verify that Instructions for user checkbox appears on the screen$")
	public void verify_that_Instructions_for_user_checkbox_appears_on_the_screen() throws Throwable {
	    questionPageObj.verifyInstructionForUser();
	}
	@Given("^: Click on Instructions for user text box and press the \"([^\"]*)\" Key$")
	public void click_on_Instructions_for_user_text_box_and_press_the_Key(String arg1) throws Throwable {
		questionPageObj.questionsInstForUserTextbox.sendKeys(Keys.TAB);
	}
	@Then("^: Verify mandatory validation message as User Instructions are required\\.$")
	public void verify_mandatory_validation_message_as_User_Instructions_are_required() throws Throwable {
		questionPageObj.verifyQuestionInstructionForUserRequiredVal();
	}
	@Given("^: Enter Instructions For User textbox value less than (\\d+) characters$")
	public void enter_Instructions_For_User_textbox_value_less_than_characters(int arg1) throws Throwable {
		String questioninstforuserminival = excelData(13, 2);
		questionPageObj.questionsInstructionForUsersendkeys(questioninstforuserminival);
	}
	@Then("^: Verify that validation message as Instructions for user must be at least (\\d+) characters\\.$")
	public void verify_that_validation_message_as_Instructions_for_user_must_be_at_least_characters(int arg1) throws Throwable {
		questionPageObj.verifyQuestionInstForUserMinimumVal();
	}
	@Given("^: Enter Instructions for user textbox value more than (\\d+) characters$")
	public void enter_Instructions_for_user_textbox_value_more_than_characters(int arg1) throws Throwable {
		String questioninstforusermaxval = excelData(14, 2);
		questionPageObj.questionsInstructionForUsersendkeys(questioninstforusermaxval);
	}

	@Then("^: Verify that validation message as Instructions for user cannot be more than (\\d+) characters\\.$")
	public void verify_that_validation_message_as_Instructions_for_user_cannot_be_more_than_characters(int arg1) throws Throwable {
		questionPageObj.verifyQuestionInstForUserMaximumVal();
	}
	@Given("^: Enter valid data into Instructions for user textbox$")
	public void enter_valid_data_into_Instructions_for_user_textbox() throws Throwable {
		String questioninstforusernoval = excelData(15, 2);
		questionPageObj.questionsInstructionForUsersendkeys(questioninstforusernoval);
	}
	@Then("^: Veirfy the system should accept the Instructions for user textbox value wihtout any validation message$")
	public void veirfy_the_system_should_accept_the_Instructions_for_user_textbox_value_wihtout_any_validation_message() throws Throwable {
		questionPageObj.verifyQuestionInstForUserNoVal();
	}
	@Given("^: Add valid data for all fields and click on the save button$")
	public void add_valid_data_for_all_fields_and_click_on_the_save_button() throws Throwable {
		String questionnoval = excelData(7, 2);
		questionPageObj.questionsNamesendkeys(questionnoval);
		String questiontagnoval = excelData(12, 2);
		questionPageObj.questionsTagsendkeys(questiontagnoval);
		questionPageObj.clickonAddTagBtn();
		questionPageObj.selectTypeOption();
		questionPageObj.questionsSaveButton();
	}
	@Then("^: Verify that the newly added Question should appear on the Questions listing screen and the page should be redirected to Questions listing screen$")
	public void verify_that_the_newly_added_Question_should_appear_on_the_Questions_listing_screen_and_the_page_should_be_redirected_to_Questions_listing_screen() throws Throwable {
		//questionPageObj.verifysearch(excelData(2, 2));
	}
	@Then("^: Verify that system should display Audit Trail page and Breadcrumb as \"([^\"]*)\" when user click on \"([^\"]*)\" link of the Questions listing screen$")
	public void verify_that_system_should_display_Audit_Trail_page_and_Breadcrumb_as_when_user_click_on_link_of_the_Questions_listing_screen(String arg1, String arg2) throws Throwable {
		questionPageObj.verifyAuditTrailPage();
		questionPageObj.verifyAuditTrailBreadCrumbs();
	}
	@Then("^: Verify the Color of Back button$")
	public void verify_the_Color_of_Back_button() throws Throwable {
		questionPageObj.verifyBackButtonColor();
	}
	@Given("^: Click on Back button of Question$")
	public void click_on_Back_button() throws Throwable {
	    questionPageObj.clickBackButton();
	}

	@Then("^: Verify that the system should redirect to Questions listing screen and Breadcrumb display as \"([^\"]*)\"$")
	public void verify_that_the_system_should_redirect_to_Questions_listing_screen_and_Breadcrumb_display_as(String arg1) throws Throwable {
	    questionPageObj.verifyQuestionsBreadCrumbs();
	}
	
	@Given("^: Select filter by option as Question Type$")
	public void select_filter_by_option_as_Question_Type() throws Throwable {
		
		String filterType = excelData(16, 3);
	    questionPageObj.selectQuestionTypeFilter(filterType);
	}
	@Given("^: Select Question Type as Auto Calculated$")
	public void select_Question_Type_as_Auto_Calculated() throws Throwable {
		
		String questionType = excelData(16,4);
	    questionPageObj.selectQuestionType(questionType);
	}

	@Then("^: Verify that the Question listing screen grid should display only Auto Calculated type of data$")
	public void verify_that_the_Question_listing_screen_grid_should_display_only_Auto_Calculated_type_of_data() throws Throwable {
	    questionPageObj.verifyAutoCalculatedFilter();
	}
	@Given("^: Select Question Type as Textbox$")
	public void select_Question_Type_as_Textbox() throws Throwable {
		String questionType = excelData(17,4);
	    questionPageObj.selectQuestionType(questionType);
	}

	@Then("^: Verify that the Question listing screen grid should display only Textbox type of data$")
	public void verify_that_the_Question_listing_screen_grid_should_display_only_Textbox_type_of_data() throws Throwable {
	    questionPageObj.verifyTextboxFilter();
	}
	@Given("^: Select Question Type as Checkbox$")
	public void select_Question_Type_as_Checkbox() throws Throwable {
		String questionType = excelData(18,4);
	    questionPageObj.selectQuestionType(questionType);
	}

	@Then("^: Verify that the Question listing screen grid should display only Checkbox type of data$")
	public void verify_that_the_Question_listing_screen_grid_should_display_only_Checkbox_type_of_data() throws Throwable {
	    questionPageObj.verifyCheckboxFilter();
	}
	@Given("^: Select Question Type as Textarea$")
	public void select_Question_Type_as_Textarea() throws Throwable {
		String questionType = excelData(19,4);
	    questionPageObj.selectQuestionType(questionType);
	}

	@Then("^: Verify that the Question listing screen grid should display only Textarea type of data$")
	public void verify_that_the_Question_listing_screen_grid_should_display_only_Textarea_type_of_data() throws Throwable {
	    questionPageObj.verifyTextAreaFilter();
	}
	@Given("^: Select Question Type as Dropdown$")
	public void select_Question_Type_as_Dropdown() throws Throwable {
		String questionType = excelData(20,4);
	    questionPageObj.selectQuestionType(questionType);
	}

	@Then("^: Verify that the Question listing screen grid should display only Dropdown type of data$")
	public void verify_that_the_Question_listing_screen_grid_should_display_only_Dropdown_type_of_data() throws Throwable {
	    questionPageObj.verifyDropdownFilter();
	}
	@Given("^: Select Question Type as File Attachment$")
	public void select_Question_Type_as_File_Attachment() throws Throwable {
		String questionType = excelData(21,4);
	    questionPageObj.selectQuestionType(questionType);
	}
	@Then("^: Verify that the Question listing screen grid should display only File Attachment type of data$")
	public void verify_that_the_Question_listing_screen_grid_should_display_only_File_Attachment_type_of_data() throws Throwable {
	    questionPageObj.verifyFileAttahcmentFilter();
	}
	@Given("^: Select filter by option as Question Title$")
	public void select_filter_by_option_as_Question_Title() throws Throwable {
	    System.out.println("test selected");
	}
	@Given("^: Enter Question Name into the search box$")
	public void enter_Question_Name_into_the_search_box() throws Throwable {
		String searchText = excelData(22,5);
		questionPageObj.EnterTexttoSearch(searchText); 
	}
	@Then("^: Search result should be appear according to Searched Question Name and Question Title$")
	public void search_result_should_be_appear_according_to_Searched_Question_Name_and_Question_Title() throws Throwable {
		String questionName = excelData(22,5);
		questionPageObj.verifyQuestionSearchResult(questionName);
	}
	@Given("^: Enter Auto Calculated Type Question Name into the search box$")
	public void enter_Auto_Calculated_Type_Question_Name_into_the_search_box() throws Throwable {
		String searchText = excelData(23,5);
		questionPageObj.EnterTexttoSearch(searchText); 
	}

	@Then("^: Search result should be appear according to Searched Question Name with Question Type as Auto Calculated$")
	public void search_result_should_be_appear_according_to_Searched_Question_Name_with_Question_Type_as_Auto_Calculated() throws Throwable {
		String questionName = excelData(23,5);
		String fileType=excelData(23,4);
		questionPageObj.verifyAutoCalculatedSearchResult(questionName,fileType);
		
	}
	@Given("^: Enter Textbox type Question Name into the search box$")
	public void enter_Textbox_type_Question_Name_into_the_search_box() throws Throwable {
		String searchText = excelData(24,5);
		questionPageObj.EnterTexttoSearch(searchText); 
	}
	@Then("^: Search result should be appear according to Searched Question Name with Question Type as Textbox$")
	public void search_result_should_be_appear_according_to_Searched_Question_Name_with_Question_Type_as_Textbox() throws Throwable {
		String questionName = excelData(24,5);
		String fileType=excelData(24,4);
		questionPageObj.verifyTextBoxSearchResult(questionName,fileType);
	}
	@Given("^: Enter Checkbox type Question Name into the search box$")
	public void enter_Checkbox_type_Question_Name_into_the_search_box() throws Throwable {
		String searchText = excelData(25,5);
		questionPageObj.EnterTexttoSearch(searchText); 
	}
	@Then("^: Search result should be appear according to Searched Question Name with Question Type as Checkbox$")
	public void search_result_should_be_appear_according_to_Searched_Question_Name_with_Question_Type_as_Checkbox() throws Throwable {
		String questionName = excelData(25,5);
		String fileType=excelData(25,4);
		questionPageObj.verifyCheckBoxSearchResult(questionName,fileType);
	}
	@Given("^: Enter Textarea type Question Name into the search box$")
	public void enter_Textarea_type_Question_Name_into_the_search_box() throws Throwable {
		String searchText = excelData(26,5);
		questionPageObj.EnterTexttoSearch(searchText); 
	}

	@Then("^: Search result should be appear according to Searched Question Name with Question Type as Textarea$")
	public void search_result_should_be_appear_according_to_Searched_Question_Name_with_Question_Type_as_Textarea() throws Throwable {
		String questionName = excelData(26,5);
		String fileType=excelData(26,4);
		questionPageObj.verifyTextAreaSearchResult(questionName,fileType);
	}

	@Given("^: Enter Dropdown type Question Name into the search box$")
	public void enter_Dropdown_type_Question_Name_into_the_search_box() throws Throwable {
		String searchText = excelData(27,5);
		questionPageObj.EnterTexttoSearch(searchText); 
	}

	@Then("^: Search result should be appear according to Searched Question Name with Question Type as Dropdown$")
	public void search_result_should_be_appear_according_to_Searched_Question_Name_with_Question_Type_as_Dropdown() throws Throwable {
		String questionName = excelData(27,5);
		String fileType=excelData(27,4);
		questionPageObj.verifyDropdownSearchResult(questionName,fileType);
	}
	@Given("^: Enter File Attachment type Question Name into the search box$")
	public void enter_File_Attachment_type_Question_Name_into_the_search_box() throws Throwable {
		String searchText = excelData(28,5);
		questionPageObj.EnterTexttoSearch(searchText); 
	}

	@Then("^: Search result should be appear according to Searched Question Name with Question Type as File Attachment$")
	public void search_result_should_be_appear_according_to_Searched_Question_Name_with_Question_Type_as_File_Attachment() throws Throwable {
		String questionName = excelData(28,5);
		String fileType=excelData(28,4);
		questionPageObj.verifyFileAttachmentSearchResult(questionName,fileType);
	}
	@Given("^: Enter Question Tag into the search box$")
	public void enter_Question_Tag_into_the_search_box() throws Throwable {
		String searchText = excelData(29,5);
		questionPageObj.EnterTexttoSearch(searchText); 
	}

	@Then("^: Search result should be appear according to Searched Question Tag and Question Title$")
	public void search_result_should_be_appear_according_to_Searched_Question_Tag_and_Question_Title() throws Throwable {
		String searchText = excelData(29,5);
		String questionName=excelData(29,2);
		questionPageObj.verifyQuestionTagSearchVal(searchText,questionName);
	}
	@Given("^: Enter Auto Calculated Type Question Tag into the search box$")
	public void enter_Auto_Calculated_Type_Question_Tag_into_the_search_box() throws Throwable {
		String searchText = excelData(30,5);
		questionPageObj.EnterTexttoSearch(searchText); 
	}

	@Then("^: Search result should be appear according to Searched Question Tag with Question Type as Auto Calculated$")
	public void search_result_should_be_appear_according_to_Searched_Question_Tag_with_Question_Type_as_Auto_Calculated() throws Throwable {
		String searchText = excelData(30,5);
		String questionName=excelData(30,2);
		questionPageObj.verifyAutocalQuestionTagSearchVal(searchText, questionName);
	}
	@Given("^: Enter Textbox type Question Tag into the search box$")
	public void enter_Textbox_type_Question_Tag_into_the_search_box() throws Throwable {
		String searchText = excelData(31,5);
		questionPageObj.EnterTexttoSearch(searchText); 
	}

	@Then("^: Search result should be appear according to Searched Question Tag with Question Type as Textbox$")
	public void search_result_should_be_appear_according_to_Searched_Question_Tag_with_Question_Type_as_Textbox() throws Throwable {
		String searchText = excelData(31,5);
		String questionName=excelData(31,2);
		questionPageObj.verifyTextBoxlQuestionTagSearchVal(searchText, questionName);
		
	}
	@Given("^: Enter Checkbox type Question Tag into the search box$")
	public void enter_Checkbox_type_Question_Tag_into_the_search_box() throws Throwable {
		String searchText = excelData(32,5);
		questionPageObj.EnterTexttoSearch(searchText); 
	}

	@Then("^: Search result should be appear according to Searched Question Tag with Question Type as Checkbox$")
	public void search_result_should_be_appear_according_to_Searched_Question_Tag_with_Question_Type_as_Checkbox() throws Throwable {
		String searchText = excelData(32,5);
		String questionName=excelData(32,2);
		questionPageObj.verifyCheckboxlQuestionTagSearchVal(searchText, questionName);
	}
	@Given("^: Enter Textarea type Question Tag into the search box$")
	public void enter_Textarea_type_Question_Tag_into_the_search_box() throws Throwable {
		String searchText = excelData(33,5);
		questionPageObj.EnterTexttoSearch(searchText); 
	}

	@Then("^: Search result should be appear according to Searched Question Tag with Question Type as Textarea$")
	public void search_result_should_be_appear_according_to_Searched_Question_Tag_with_Question_Type_as_Textarea() throws Throwable {
		String searchText = excelData(33,5);
		String questionName=excelData(33,2);
		questionPageObj.verifyTextareaQuestionTagSearchVal(searchText, questionName);
	}
	@Given("^: Enter Dropdown type Question Tag into the search box$")
	public void enter_Dropdown_type_Question_Tag_into_the_search_box() throws Throwable {
		String searchText = excelData(35,5);
		questionPageObj.EnterTexttoSearch(searchText);
	}

	@Then("^: Search result should be appear according to Searched Question Tag with Question Type as Dropdown$")
	public void search_result_should_be_appear_according_to_Searched_Question_Tag_with_Question_Type_as_Dropdown() throws Throwable {
		String searchText = excelData(35,5);
		String questionName=excelData(35,2);
		questionPageObj.verifyDropdownQuestionTagSearchVal(searchText, questionName);
	}
	@Given("^: Enter File Attachment type Question Tag into the search box$")
	public void enter_File_Attachment_type_Question_Tag_into_the_search_box() throws Throwable {
		String searchText = excelData(34,5);
		questionPageObj.EnterTexttoSearch(searchText); 
	}

	@Then("^: Search result should be appear according to Searched Question Tag with Question Type as File Attachment$")
	public void search_result_should_be_appear_according_to_Searched_Question_Tag_with_Question_Type_as_File_Attachment() throws Throwable {
		String searchText = excelData(34,5);
		String questionName=excelData(34,2);
		questionPageObj.verifyFileAttachmentQuestionTagSearchVal(searchText, questionName);
	}
	@Given("^: Enter Question Name into the search box to check Search remove functionality$")
	public void enter_Question_Name_into_the_search_box_to_check_Search_remove_functionality() throws Throwable {
		String searchText = excelData(22,5);
		questionPageObj.enterSearchValtoRemove(searchText);
	}
	@Given("^: Click on remove icon of search textbox$")
	public void click_on_remove_icon_of_search_textbox() throws Throwable {
	    questionPageObj.clickonRemoveIcon();
	}
	@Then("^: Verify that search result should be disappear from the Question listing screen$")
	public void verify_that_search_result_should_be_disappear_from_the_Question_listing_screen() throws Throwable {
	    questionPageObj.verifyRemoveSearch();
	}
	@Given("^: Select page size as (\\d+)$")
	public void select_page_size_as(int arg1) throws Throwable {
	    questionPageObj.selectPageSizeTen();
	}
	@Then("^: Verify the page size label, no of records per page, and total no of pages$")
	public void verify_the_page_size_label_no_of_records_per_page_and_total_no_of_pages() throws Throwable {
	    questionPageObj.verifyPagesizeten();
	}
	@Given("^: Select page size as twenty number$")
	public void select_page_size_as_twenty_number() throws Throwable {
	  questionPageObj.selectPageSizeTwenty();
	}

	@Then("^: Verify the page size label, no of records per page, and total no of pages should be related to twenty$")
	public void verify_the_page_size_label_no_of_records_per_page_and_total_no_of_pages_should_be_related_to_twenty() throws Throwable {
	    questionPageObj.verifyPagesizetwenty();
	}
	@Given("^: Select page size as fifty number$")
	public void select_page_size_as_fifty_number() throws Throwable {
	    questionPageObj.selectPageSizeFifty();
	}

	@Then("^: Verify the page size label, no of records per page, and total no of pages should be related to fifty$")
	public void verify_the_page_size_label_no_of_records_per_page_and_total_no_of_pages_should_be_related_to_fifty() throws Throwable {
	    questionPageObj.verifyPagesizeFifty();
	}
}

	
