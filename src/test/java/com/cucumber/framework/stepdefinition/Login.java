package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import com.cucumber.framework.helper.PageObject.CommonFunctionPageObject;
import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Login {
	private LoginPageObject lpage;
	private CommonFunctionPageObject commonFnPageObject;
	public ExcelUtils excel;
	private WaitHelper waitObj;
	private TextBoxHelper textBoxHelper;
	
	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excel Calling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Login");
		System.err.println();
		return excel.readXLSFile("Login", rowVal, colVal);
	}
	
	public Login() {
		lpage = new LoginPageObject(ObjectRepo.driver);
		commonFnPageObject = new CommonFunctionPageObject(ObjectRepo.driver);
		waitObj = new WaitHelper(ObjectRepo.driver, ObjectRepo.reader);
		textBoxHelper = new TextBoxHelper(ObjectRepo.driver);
	}

	@When("^: Click to Login Button$")
	public void click_to_Login_Button() throws Throwable {
			commonFnPageObject.clicktologin();

		if (ObjectRepo.driver.findElements(By.xpath("//*[@ng-click='vm.Yes({id: vm.adminId})']")).size() != 0) {
			WebElement yesBtn;
			yesBtn = ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.Yes({id: vm.adminId})']"));
			yesBtn.click();
			Thread.sleep(2000);
			waitObj.waitForElementVisible(ObjectRepo.driver.findElement(By.xpath("//span[@id='wootric-x']")));
		} 
		if (ObjectRepo.driver.findElements(By.xpath("//span[@id='wootric-x']")).size() != 0) {
			commonFnPageObject.clickDismissIcon();
		}	
	}

	@Then("^: Verify the label as \"([^\"]*)\"$")
	public void verify_the_label_as(String arg1) throws Throwable {
		lpage.loginlabelverification();
	}

	
	
	
	@Given("^: Enter username like \"([^\"]*)\" value$")
	public void enter_username_like_value(String arg1) throws Throwable {

		if (excelData(1, 1).equalsIgnoreCase("empty")) {
				commonFnPageObject.username.sendKeys(Keys.TAB);
		} else {
				System.out.println("something went wrog in excel read");
			}
		} 

	@Given("^: Enter valid Password like \"([^\"]*)\"$")
	public void enter_valid_Password_like(String arg1) throws Throwable {
		commonFnPageObject.password.sendKeys(excelData(1, 2));
	}

	@Then("^: Verify the username mandatory validation message as \"([^\"]*)\"$")
	public void verify_the_username_mandatory_validation_message_as(String arg1) throws Throwable {
		lpage.validateUsernamemsg("");
	}

	@Given("^: Without enter username press the \"([^\"]*)\" key$")
	public void without_enter_username_press_the_key(String arg1) throws Throwable {
		commonFnPageObject.username.sendKeys(Keys.TAB);
	}

	@Then("^: Verify the required username validation color$")
	public void verify_the_required_username_validation_color() throws Throwable {
		lpage.verifyUserNameReqMsgColor();
	}

	@Given("^: Enter valid username like \"([^\"]*)\"$")
	public void enter_valid_username_like(String arg1) throws Throwable {
		textBoxHelper.sendKeys(commonFnPageObject.username, excelData(2, 1));
	}

	@Given("^: Enter Password like \"([^\"]*)\" value$")
	public void enter_Password_like_value(String arg1) throws Throwable {
		if (excelData(2, 2).equalsIgnoreCase("empty")) {
			commonFnPageObject.password.sendKeys(Keys.TAB);
		} else {
			System.out.println("something went wrog in excel read");
		}
	}

	@Then("^: Verify the Password mandatory validation message \"([^\"]*)\"$")
	public void verify_the_Password_mandatory_validation_message(String arg1) throws Throwable {
		
	}

	@Given("^: Without enter Password press the \"([^\"]*)\" key$")
	public void without_enter_Password_press_the_key(String arg1) throws Throwable { 
		commonFnPageObject.password.sendKeys(Keys.TAB);
	}

	@Then("^: Verify the required password validation color$")
	public void verify_the_required_password_validation_color() throws Throwable {
		lpage.verifyPasswordReqMsgColor();
	}

	@Given("^: Enter invalid username like \"([^\"]*)\"$")
	public void enter_invalid_username_like(String arg1) throws Throwable {
		textBoxHelper.sendKeys(commonFnPageObject.username, excelData(3, 1));
	}

	@Then("^: Verify the invalid login message as \"([^\"]*)\"$")
	public void verify_the_invalid_login_message_as(String arg1) throws Throwable {
	
		lpage.verifyInvalidLoginMsg();
	}

	@Then("^: Verify the invalid validation color$")
	public void verify_the_invalid_validation_color() throws Throwable {
		lpage.verifyInvalidLoginMsg();
	}

	@Then("^: Verify the Login button color$")
	public void verify_the_Login_button_color() throws Throwable {
		lpage.verifyBtnLoginColor();
	}

	@Given("^: Navigate to the URL in Incongnito mode$")
	public void navigate_to_the_URL_in_Incongnito_mode() throws Throwable {
		
	}

	@When("^: close the Incongnito mode browser\\.$")
	public void close_the_Incongnito_mode_browser() throws Throwable {
		
	}

	@Then("^: Verify the already loged in user validation message like \"([^\"]*)\"$")
	public void verify_the_already_loged_in_user_validation_message_like(String arg1) throws Throwable {
		
	}

	@Given("^: Display already loged in user validation message$")
	public void display_already_loged_in_user_validation_message() throws Throwable {
		
	}

	@Then("^: Verify the confirmation \"([^\"]*)\" button color$")
	public void verify_the_confirmation_button_color_No(String arg1) throws Throwable {
		
	}

	@Then("^: Verify the confirmation \"([^\"]*)\" button color\\.$")
	public void verify_the_confirmation_button_color_yes(String arg1) throws Throwable {
		
	}

	@Then("^: Verify the confirmation \"([^\"]*)\" text label$")
	public void verify_the_confirmation_text_label_No(String arg1) throws Throwable {
		
	}

	@Then("^: Verify the confirmation \"([^\"]*)\" text label\\.$")
	public void verify_the_confirmation_text_label_Yes(String arg1) throws Throwable {
		
	}

	@Then("^: Verify the Forgot Password link color$")
	public void verify_the_Forgot_Password_link_color() throws Throwable {
		lpage.verifyforgotPassLinkColor();
	}

	@Then("^: verify the Forgot Password label as \"([^\"]*)\"$")
	public void verify_the_Forgot_Password_label_as(String arg1) throws Throwable {
		lpage.verifyforgotPassLinkText();
	}

	@Then("^: verify the Forgot Password label as \"([^\"]*)\"\\.$")
	public void verify_the_Forgot_Password_label_as_new(String arg1) throws Throwable {
		lpage.verifyforgotPassLableText();
	}

	@Given("^: Click to \"([^\"]*)\" link$")
	public void click_to_link(String arg1) throws Throwable {
		lpage.forgotPassLinkClick();
	}

	@Then("^: Verify the username mandatory validation message like \"([^\"]*)\"$")
	public void verify_the_username_mandatory_validation_message_like(String arg1) throws Throwable {
		lpage.verifyforgotPassUsrNameReqValiMsg();
	}

	@Given("^: Without enter username press the \"([^\"]*)\" key on Forgot Password screen$")
	public void without_enter_username_press_the_key_on_Forgot_Password_screen(String arg1) throws Throwable {
		lpage.forgotPassUserName.sendKeys(Keys.TAB);
	}

	@Given("^:  Enter Username as one character and press the \"([^\"]*)\" key$")
	public void enter_Username_as_one_character_and_press_the_key(String arg1) throws Throwable {
		textBoxHelper.sendKeys(lpage.forgotPassUserName, excelData(4, 1));
	}

	@Given("^:  Enter Username as  than (\\d+) characters and press the \"([^\"]*)\" key$")
	public void enter_Username_as_than_characters_and_press_the_key(int arg1, String arg2) throws Throwable {
		textBoxHelper.sendKeys(lpage.forgotPassUserName, excelData(5, 1));
	}

	@Then("^: Verify the username mandatory validation message color$")
	public void verify_the_username_mandatory_validation_message_color() throws Throwable {
		lpage.verifyforgotPassUsrNameReqValiMsgColor();
	}

	@Given("^:  Enter Username as \"([^\"]*)\" and press the \"([^\"]*)\" key$")
	public void enter_Username_as_and_press_the_key(String arg1, String arg2) throws Throwable {
		
	}

	@Then("^: Verify the username minimum character validation message like \"([^\"]*)\"$")
	public void verify_the_username_minimum_character_validation_message_like(String arg1) throws Throwable {
		lpage.verifyforgotPassUsrNameMinValiMsg();
	}

	@Then("^: Verify the username minimum validation message color$")
	public void verify_the_username_minimum_validation_message_color() throws Throwable {
		lpage.verifyforgotPassUsrNameMinValiMsgColor();
	}

	@Then("^: Verify the username maximum validation message like \"([^\"]*)\"$")
	public void verify_the_username_maximum_validation_message_like(String arg1) throws Throwable {
		lpage.verifyforgotPassUsrNameMaxValiMsg();
	}
	@Then("^: Verify the username maximum validation message color$")
	public void verify_the_username_maximum_validation_message_color() throws Throwable {
		lpage.verifyforgotPassUsrNameMaxValiMsgColor();
	}
	
	@Then("^: verify the submit button background color$")
	public void verify_the_submit_button_background_color() throws Throwable {
		lpage.verifyForgotPassSubmitBtnColor();
	}

	@Then("^: Verify the submit button text$")
	public void verify_the_submit_button_text() throws Throwable {
		lpage.verifyForgotPassSubmitBtnLabel();
	}

	@Then("^: Verify the Cancel button background color$")
	public void verify_the_Cancel_button_background_color() throws Throwable {
		lpage.verifyForgotPassCancelBtnColor();
	}

	@Then("^: Verify the Cancel button Text$")
	public void verify_the_Cancel_button_Text() throws Throwable {
		lpage.verifyForgotPassCancelBtnLabel();
	}

	@Given("^: Click to \"([^\"]*)\" button$")
	public void click_to_button(String arg1) throws Throwable {
		lpage.verifyForgotPassCancelBtnClick();
	}

	@Then("^: Verify that when click on cancel button user redirected to Login screen$")
	public void verify_that_when_click_on_cancel_button_user_redirected_to_Login_screen() throws Throwable {
		lpage.loginlabelverification();
	}

	@Then("^: Verify the user is login successfully into the system$")
	public void verify_the_user_is_login_successfully_into_the_system() throws Throwable {
		lpage.verifyHomeLabel();
	}

}
