package com.cucumber.framework.stepdefinition;

import com.cucumber.framework.configreader.PropertyFileReader;
import com.cucumber.framework.helper.InitializeWebDrive;
import com.cucumber.framework.helper.PageObject.AdvancedSearchPageObject;
import com.cucumber.framework.helper.PageObject.AppBuilderPageObject;
import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.helper.PageObject.CommonFunctionPageObject;
import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.RolesAndPermissionsPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class RolesAndPermissions {

	private LoginPageObject lpage;
	public ExcelUtils excel;
	private AttributesPageObject attributePageObj;
	private AdvancedSearchPageObject advSearch;
	AppBuilder appBuild;
	Apps apps;
	RolesAndPermissionsPageObject rolePermObj;


	public RolesAndPermissions() {
		attributePageObj = new AttributesPageObject(ObjectRepo.driver);
		lpage = new LoginPageObject(ObjectRepo.driver);
		advSearch = new AdvancedSearchPageObject(ObjectRepo.driver);
		appBuild = new AppBuilder();
		apps = new Apps();
		rolePermObj = new RolesAndPermissionsPageObject(ObjectRepo.driver);
	}

	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Roles Permissions");
		System.err.println();
		return excel.readXLSFile("Roles Permissions", rowVal, colVal);
	}

//	@Given("^: Click on Save button of without permission tab without$")
//	public void click_on_Save_button_of_without_permission_tab_without() throws Throwable {
//	    
//	}

	@Given("^: Click on Roles and Permissions Tile$")
	public void click_on_Roles_and_Permissions_Tile() throws Throwable {
		rolePermObj.clickRolesPermissionsTile();
	}

	@Given("^: Click on roles permissions Button$")
	public void click_on_roles_permissions_Button() throws Throwable {
		rolePermObj.clickRolesPermsBtn();
	}

	@Given("^: Select Role from role dropdown$")
	public void select_Role_from_role_dropdown() throws Throwable {
		rolePermObj.selectRoleDrpDwn("Admin Full Control");
	}

	@Given("^: Click on Edit button$")
	public void click_on_Edit_button() throws Throwable {
	    rolePermObj.clickEditBtn();
	}

	@Given("^: Select Module from module dropdown$")
	public void select_Module_from_module_dropdown() throws Throwable {
	    rolePermObj.selectModuleDrpDwn(new AppBuilder().mdlName);
	}

	@Given("^: Uncheck permission checkbox of App$")
	public void uncheck_permission_checkbox_of_App() throws Throwable {
	    rolePermObj.uncheckAppAllPermissions(new AppBuilderPageObject(ObjectRepo.driver).getAppTitle);
	}

	@Given("^: Click on save button and popup$")
	public void click_on_save_button_and_popup() throws Throwable {
	    rolePermObj.clickSaveAndPopup();
	}
    
	
}//end
