package com.cucumber.framework.stepdefinition;

import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.SetsPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class Sets {

	public ExcelUtils excel;
	private SetsPageObject setPage;

	public Sets() {
		new LoginPageObject(ObjectRepo.driver);
		setPage = new SetsPageObject(ObjectRepo.driver);
		
	}

	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Sets");
		System.err.println();
		return excel.readXLSFile("Sets", rowVal, colVal);
	}

	@Then("^: Verify the module name as Sets$")
	public void verify_the_module_name_as_Sets() throws Throwable {
		setPage.verifySetsModuleName();
	}

	@Given("^: Click on Sets Tile$")
	public void click_on_Sets_Tile() throws Throwable {
		setPage.clicktoSetTile();
	}
	
	@Given("^: Click on View Link in Link Information Column of the Sets listing screen$")
	public void click_on_View_Link_in_Link_Information_Column_of_the_Sets_listing_screen() throws Throwable {
	    setPage.clickLinkInformationLink();
	}

	@Then("^: Verify that system should displayed Popup page Link Information Column when user click on View link of Sets listing screen$")
	public void verify_that_system_should_displayed_Popup_page_Link_Information_Column_when_user_click_on_View_link_of_Sets_listing_screen() throws Throwable {
	    setPage.verifyLinkInformationPopUp();
	}

	@Given("^: Click on \"([^\"]*)\" link of the first record of the Sets listing screen$")
	public void click_on_link_of_the_first_record_of_the_Sets_listing_screen(String arg1) throws Throwable {
	    setPage.clickViewAuditTrailLink();
	}

	@Then("^: Verify that system should displayed Audit Trail page when user click on \"([^\"]*)\" link of the Set listing screen$")
	public void verify_that_system_should_displayed_Audit_Trail_page_when_user_click_on_link_of_the_Set_listing_screen(String arg1) throws Throwable {
	     setPage.verifyAuditTrailPage();
	}

	@Given("^: Click on Sorting icon of the Sets column for ascending$")
	public void click_on_Sorting_icon_of_the_Sets_column_for_ascending() throws Throwable {
	    
	}

	@Then("^: Verify that all the records of Sets column display in ascending order$")
	public void verify_that_all_the_records_of_Sets_column_display_in_ascending_order() throws Throwable {
	   
	    
	}

	@Given("^: Click on Sorting icon of the Sets column for descending$")
	public void click_on_Sorting_icon_of_the_Sets_column_for_descending() throws Throwable {
	   
	    
	}

	@Then("^: Verify that all the records of Sets column display in descending order$")
	public void verify_that_all_the_records_of_Sets_column_display_in_descending_order() throws Throwable {
	   
	    
	}

	@Given("^: Enter the Sets name into search box of Sets listing screen$")
	public void enter_the_Sets_name_into_search_box_of_Sets_listing_screen() throws Throwable {
		System.out.println("Searching will be processed shortly ...");
	}

	@Then("^: Verify the search result of sets$")
    public void verify_the_search_result_of_sets() throws Throwable {
       	setPage.verifySearchTextBoxInput(excelData(1, 1));
    }

	@Given("^: Click on \"([^\"]*)\" button of Sets listing screen$")
	public void click_on_button_of_Sets_listing_screen(String arg1) throws Throwable {
	    setPage.clickAddBtnSets();
	}

	@Then("^: Verify the Current version in Add/Sets screen as \"([^\"]*)\"$")
	public void verify_the_Current_version_in_Add_Sets_screen_as(String arg1) throws Throwable {
	    setPage.verifyCurrentVersion();
	}

	@Given("^: Click on Set Name textbox and press \"([^\"]*)\" key$")
	public void click_on_Set_Name_textbox_and_press_key(String arg1) throws Throwable {
	    setPage.enterSetTextbox(excelData(2, 2));
	}

	@Then("^: Verify that system should displayed required validation message for Set Name textbox as \"([^\"]*)\"$")
	public void verify_that_system_should_displayed_required_validation_message_for_Set_Name_textbox_as(String arg1) throws Throwable {
	    setPage.verifySetRequiredValidationMsg();
	}

	@Given("^: Enter Set Name value less than (\\d+) characters$")
	public void enter_Set_Name_value_less_than_characters(int arg1) throws Throwable {
	    setPage.enterOneChar(excelData(3, 2));
	}

	@Then("^: Verify that system should displayed Minimum validation message of Set Name textbox as \"([^\"]*)\"$")
	public void verify_that_system_should_displayed_Minimum_validation_message_of_Set_Name_textbox_as(String arg1) throws Throwable {
	    setPage.verifySetMinimumValidationMsg();
	}

	@Given("^: Enter Set Name value more than (\\d+) characters$")
	public void enter_Set_Name_value_more_than_characters(int arg1) throws Throwable {
	    setPage.enterFiftyOneChar(excelData(4, 2));
	}

	@Then("^: Verify that system should displayed Maximum validation message of Set Name textbox as \"([^\"]*)\"$")
	public void verify_that_system_should_displayed_Maximum_validation_message_of_Set_Name_textbox_as(String arg1) throws Throwable {
	    setPage.verifySetMaximumValidationMsg();
	}

	@Given("^: Enter Set Name as special characters$")
	public void enter_Set_Name_as_special_characters() throws Throwable {
	    setPage.enterSpecialChar(excelData(5, 2));
	}

	@Then("^: Verify that system should displayed Alphabet validation message of Set Name textbox as \"([^\"]*)\"$")
	public void verify_that_system_should_displayed_Alphabet_validation_message_of_Set_Name_textbox_as(String arg1) throws Throwable {
	    setPage.verifySetSpecialCharValidationMsg();
	}

	@Given("^: Enter Set Name as valid data$")
	public void enter_Set_Name_as_valid_data() throws Throwable {
	    setPage.enterValidSets(excelData(6, 2));
	}

	@Then("^: Verify that system is not displayed any validation message when user enter valid data in Set Name textbox of Sets module$")
	public void verify_that_system_is_not_displayed_any_validation_message_when_user_enter_valid_data_in_Set_Name_textbox_of_Sets_module() throws Throwable {
	    setPage.verifyValidSetsNoMsg();
	}

	@Given("^: Add Duplicate Set Name$")
	public void add_Duplicate_Set_Name() throws Throwable {
	    setPage.enterDuplicateSetsName(excelData(7, 2));
	}

	@Then("^: Verify that system should displayed validation message for Duplicate Set Name as \"([^\"]*)\"$")
	public void verify_that_system_should_displayed_validation_message_for_Duplicate_Set_Name_as(String arg1) throws Throwable {
	    setPage.verifyDuplicateValidationMsg();
	}

	@Given("^: Add name in Set Name textbox$")
	public void add_name_in_Set_Name_textbox() throws Throwable {
	   
	    
	}

	@Given("^: Add value of Set in Enter Set Value textbox$")
	public void add_value_of_Set_in_Enter_Set_Value_textbox() throws Throwable {
	    setPage.enterSetValueTextBox(excelData(1, 3));
	}

	@Given("^: Click on \"([^\"]*)\" button$")
	public void click_on_button(String arg1) throws Throwable {
	    setPage.clickAddBtnSetValue();
	}

	@Then("^: Verify that system should add the Value of set when user click on Add button of Add set screen$")
	public void verify_that_system_should_add_the_Value_of_set_when_user_click_on_Add_button_of_Add_set_screen() throws Throwable {
	    setPage.verifySetValueTextBoxValue(excelData(1, 3));
	}

	@Then("^: Verify that Toggle button should be Active by default$")
	public void verify_that_Toggle_button_should_be_Active_by_default() throws Throwable {
	    setPage.verifyToggleBtnDefaultVal();
	}

	@Given("^: Click on Toggle button and make it as Inactive$")
	public void click_on_Toggle_button_and_make_it_as_Inactive() throws Throwable {
	    setPage.clickToggleBtnInactive();
	}
	
	@Then("^: Verify that Toggle button should be Inactive in Set screen$")
	public void verify_that_Toggle_button_should_be_Inactive_in_Set_screen() throws Throwable {
		setPage.verifyToggleBtnInactiveVal();
	}

	@Given("^: Click on Cancel button of Add Set Screen$")
	public void click_on_Cancel_button_of_Add_Set_Screen() throws Throwable {
		setPage.clickCancelBtnAddSet();
	}

	@Then("^: Verify that system should be redirect to the Sets listing screen when user click on Cancel button$")
	public void verify_that_system_should_be_redirect_to_the_Sets_listing_screen_when_user_click_on_Cancel_button() throws Throwable {
	   
	    
	}

	@Given("^: Checked and Unchecked by clicking checkbox of added set value$")
	public void checked_and_Unchecked_by_clicking_checkbox_of_added_set_value() throws Throwable {		
	   	setPage.checkedDefaultValue();
	}

	@Then("^: Verify that Checkbox should be clickable properly when user Checked and Unchecked$")
	public void verify_that_Checkbox_should_be_clickable_properly_when_user_Checked_and_Unchecked() throws Throwable {
		setPage.verifyCheckboxClickable();
	}

	@Given("^: Click on Edit link of Action field$")
	public void click_on_Edit_link_of_Action_field() throws Throwable {
		setPage.clickonEdit();
	}

	@Given("^: Edit the set value$")
	public void edit_the_set_value() throws Throwable {
		setPage.editSetValue(excelData(6, 2));
	}

	@Given("^: Click on Add button$")
	public void click_on_Add_button() throws Throwable {
	   
	}

	@Then("^: Verify that Edit functionality should be working properly when user Edit set value$")
	public void verify_that_Edit_functionality_should_be_working_properly_when_user_Edit_set_value() throws Throwable {
	    setPage.verifyEditFunctionality(excelData(6, 2));
	}

	@Given("^: click on Delete link to delete set value$")
	public void click_on_Delete_link_to_delete_set_value() throws Throwable {
	    setPage.deleteSetValue();
	}

	@Then("^: Verify that Delete functionality should be working properly when user Delete set value$")
	public void verify_that_Delete_functionality_should_be_working_properly_when_user_Delete_set_value() throws Throwable {
		setPage.deleteSetVerify();
	}

	@Given("^: Enter Set Name in textbox of Set Name$")
	public void enter_Set_Name_in_textbox_of_Set_Name() throws Throwable {
	   
	}

	@Given("^: Add multiple set values$")
	public void add_multiple_set_values() throws Throwable {
		setPage.addMultipleSet(excelData(1, 3));
	    
	}

	@Given("^: Click on Ascending icon$")
	public void click_on_Ascending_icon() throws Throwable {
		setPage.clickAscendingIcon();
	    
	}

	@Then("^: Verify that all the records of Set value column should be displayed in ascending order$")
	public void verify_that_all_the_records_of_Set_value_column_should_be_displayed_in_ascending_order() throws Throwable {
	     setPage.verifyAscendingOrder(excelData(1, 3));
	}

	@Given("^: Click on Descending icon$")
	public void click_on_Descending_icon() throws Throwable {
	    setPage.clickDescendingIcon();
	}

	@Then("^: Verify that all the records of Set value column should be displayed in descending order$")
	public void verify_that_all_the_records_of_Set_value_column_should_be_displayed_in_descending_order() throws Throwable {
	    setPage.verifyDescendingOrder();
	}

	@Given("^: Click on Move Down button$")
	public void click_on_Move_Down_button() throws Throwable {		
        setPage.clickMoveDown();
	    
	}

	@Then("^: Verify that record of set value should be move to the Next row when user click on Move Down Button$")
	public void verify_that_record_of_set_value_should_be_move_to_the_Next_row_when_user_click_on_Move_Down_Button() throws Throwable {
	    setPage.verifyMoveDown(excelData(1, 3));
	}

	@Given("^: Click on Move Up button$")
	public void click_on_Move_Up_button() throws Throwable {
	    setPage.clickMoveUp();
	}

	@Then("^: Verify that record of set value should be move to the Previous row when user click on Move Up Button$")
	public void verify_that_record_of_set_value_should_be_move_to_the_Previous_row_when_user_click_on_Move_Up_Button() throws Throwable {
		setPage.verifyMoveUp(excelData(1, 3));
	}

	@Given("^: Select Radio button of Add from existing set$")
	public void select_Radio_button_of_Add_from_existing_set() throws Throwable {
	   	setPage.radiobtnAddfromExistingSet();
	}

	@Given("^: Select value in Existing set from dropdown list$")
	public void select_value_in_Existing_set_from_dropdown_list() throws Throwable {
	   setPage.selectValExistingSetfromDropdown();
	}

	@Given("^: Click on Move arrow$")
	public void click_on_Move_arrow() throws Throwable {
	   setPage.moveArrow();
	}

	@Then("^: Verify that record of existing set value should be move to the Next row when user click on Move Down Button$")
	public void verify_that_record_of_existing_set_value_should_be_move_to_the_Next_row_when_user_click_on_Move_Down_Button() throws Throwable {
	   setPage.verifyRecordofExistingSetvalueMoveDown();
	}

	@Then("^: Verify that record of existing set value should be move to the Previous row when user click on Move Up Button$")
	public void verify_that_record_of_existing_set_value_should_be_move_to_the_Previous_row_when_user_click_on_Move_Up_Button() throws Throwable {
		setPage.verifyRecordofExistingSetvalueMoveUp();
	}

}
