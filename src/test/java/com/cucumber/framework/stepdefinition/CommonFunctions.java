package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.cucumber.framework.helper.PageObject.CommonFunctionPageObject;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;

public class CommonFunctions
{
	public ExcelUtils excel;
	private CommonFunctionPageObject commonFnPageObject;
	private WaitHelper waitObj;
	public static String username=null, password=null;
	
	public CommonFunctions()
	{
		commonFnPageObject = new CommonFunctionPageObject(ObjectRepo.driver);
		waitObj = new WaitHelper(ObjectRepo.driver, ObjectRepo.reader);

	}
	
	@Given("^: Navigate to the URL$")
	public void navigate_to_the_URL() throws Throwable {
		try {
			ObjectRepo.driver.get(ObjectRepo.reader.getWebsite());
			
			Thread.sleep(2000);
			if (commonFnPageObject.btn_login.getText().equalsIgnoreCase("Login Anyway")) {
				System.err.println("Found Non-Chrome Browser: "+commonFnPageObject.btn_login.getText());
				commonFnPageObject.clicktologin();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("failed to navigate");
		}
	}
	
	@Given("^: Enter valid credentials and Click on Login Button$")
    public void enter_valid_credentials_and_Click_on_Login_Button() throws Throwable {
        ExcelUtils loginExcel= new ExcelUtils();
        loginExcel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Login");
        commonFnPageObject.username.sendKeys(ExcelUtils.readXLSFile("Login", 2, 1));
        commonFnPageObject.password.sendKeys(ExcelUtils.readXLSFile("Login", 1, 2));
        username = ExcelUtils.readXLSFile("Login", 2, 1);
        password = ExcelUtils.readXLSFile("Login", 1, 2);
        commonFnPageObject.clicktologin();
        Thread.sleep(2000);
        if (ObjectRepo.driver.findElements(By.xpath("//*[@ng-click='vm.Yes({id: vm.adminId})']")).size() != 0) 
        {
            WebElement yesBtn;
            yesBtn = ObjectRepo.driver.findElement(By.xpath("//*[@ng-click='vm.Yes({id: vm.adminId})']"));
            yesBtn.click();
            Thread.sleep(2000);
            waitObj.waitForElementVisible(ObjectRepo.driver.findElement(By.xpath("//span[@id='wootric-x']")));
        } 
		
		  if
		  (ObjectRepo.driver.findElements(By.xpath("//span[@id='wootric-x']")).size()
		  != 0) { commonFnPageObject.clickDismissIcon(); }
		 
    }
		
	/* And : Click on Administration Tile */
	@Given("^: Click on Administration Tile$")
	public void click_on_Administration_Tile() throws Throwable {		
		commonFnPageObject.clickAdministrationTile();
	}
	
	@Given("^: Go Back to Home$")
	public void go_Back_to_Home() throws Throwable {
	   commonFnPageObject.clickHomeLink();
	}
	
	@Given("^: Logout from system$")
	public void logout_from_system() throws Throwable {
		commonFnPageObject.tearDownLogout();
	}
	
	@Given("^: Login in system$")
	public void login_in_system() throws Throwable {
	   enter_valid_credentials_and_Click_on_Login_Button();
	}
}
