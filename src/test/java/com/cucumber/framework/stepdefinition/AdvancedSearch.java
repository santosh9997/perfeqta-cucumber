package com.cucumber.framework.stepdefinition;

import com.cucumber.framework.helper.PageObject.AdvancedSearchPageObject;
import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.UsersPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class AdvancedSearch {

	private LoginPageObject lpage;
	public ExcelUtils excel;
	private AttributesPageObject attributePageObj;
	public AdvancedSearchPageObject advSearch;
	public AppBuilder appBuild = new AppBuilder();
	Apps apps;
	
	public AdvancedSearch() {
		attributePageObj = new AttributesPageObject(ObjectRepo.driver);
		lpage = new LoginPageObject(ObjectRepo.driver);
		advSearch = new AdvancedSearchPageObject(ObjectRepo.driver);
//		appBuild = new AppBuilder();
		apps = new Apps();
	}

	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Advanced Search");
		System.err.println();
		return excel.readXLSFile("Advanced Search", rowVal, colVal);
	}

	@Given("^: Click on Advanced Search Tile$")
	public void click_on_Advanced_Search_Tile() throws Throwable {
		
		advSearch.clickAdvancedSearchTile();
	}

	@Then("^: Verify module name as \"([^\"]*)\"$")
	public void verify_module_name_as(String arg1) throws Throwable {
		
		advSearch.verifyAdvSearchModuleName();
	}

	@Then("^: Verify that system should display Breadcrumb for Advanced Search module as Home > App Record Search$")
	public void verify_that_system_should_display_Breadcrumb_for_Advanced_Search_module_as_Home_App_Record_Search() throws Throwable {
		
		advSearch.verifyAdvSearchBreadcrumbs();
	}

	@Then("^: Verify that system should display Record Search type label as App Record Search$")
	public void verify_that_system_should_display_Record_Search_type_label_as_App_Record_Search() throws Throwable {
		
		advSearch.verifyAdvSearchLabel();
	}

	@Then("^: Verify that Color of Record Search type label should be blue$")
	public void verify_that_Color_of_Record_Search_type_label_should_be_blue() throws Throwable {
		
		advSearch.verifyAdvSearchLabelColor();
	}

	@Then("^: Verify that system should display all module name in Advanced Search$")
	public void verify_that_system_should_display_all_module_name_in_Advanced_Search() throws Throwable {
			    
		advSearch.allModuleExpected = excelData(1, 1);
		advSearch.verifyAllModuleIsDisplayed();
	}

	@Then("^: Verify that all modules should be selected by default in Advanced Search$")
	public void verify_that_all_modules_should_be_selected_by_default_in_Advanced_Search() throws Throwable {
		
		advSearch.verifyAllModuleSelected();
	}

	@Given("^: Uncheck all module name$")
	public void uncheck_all_module_name() throws Throwable {
		
		advSearch.clickUncheckAllModule();
	}

	@Given("^: Checked all module name$")
	public void checked_all_module_name() throws Throwable {
		
		advSearch.clickCheckAllModule();
	}

	@Then("^: Verify that a user is allowed to Check all the module name from Advanced Search screen$")
	public void verify_that_a_user_is_allowed_to_Check_all_the_module_name_from_Advanced_Search_screen() throws Throwable {
		
		advSearch.verifyCheckAllModule();
	}

	@Then("^: Verify that a user is allowed to Uncheck all the module name from Advanced Search screen$")
	public void verify_that_a_user_is_allowed_to_Uncheck_all_the_module_name_from_Advanced_Search_screen() throws Throwable {
		
		advSearch.verifyUncheckAllModule();
	}

	@Given("^: Click on Search for Apps by Name Search box$")
	public void click_on_Search_for_Apps_by_Name_Search_box() throws Throwable {
		
		advSearch.clickSearchAppTextField();
	}

	@Given("^: Search and Add App from Search for Apps by Name Search box$")
	public void search_and_Add_App_from_Search_for_Apps_by_Name_Search_box() throws Throwable {
		
		advSearch.searchAppValidAppName(excelData(2, 2));
	}

	@Given("^: Click on Clear button$")
	public void click_on_Clear_button() throws Throwable {
		
		advSearch.clickClearLink();
	}

	@Then("^: Verify that system should remove the App Name from Search box when a user click on the Clear button$")
	public void verify_that_system_should_remove_the_App_Name_from_Search_box_when_a_user_click_on_the_Clear_button() throws Throwable {
		
		advSearch.verifyClearFunctionality();
	}

	@Given("^: Enter invalid App Name into Search for Apps by Name Search box$")
	public void enter_invalid_App_Name_into_Search_for_Apps_by_Name_Search_box() throws Throwable {
		
		advSearch.searchAppInvalidAppName(excelData(3, 2));
	}

	@Then("^: Verify that system should display \"([^\"]*)\" message when a user enters invalid app name$")
	public void verify_that_system_should_display_message_when_a_user_enters_invalid_app_name(String arg1) throws Throwable {
		
		advSearch.verifyNoAppsFoundMsg();
	}

	@Given("^: Enter App name into Search for Apps by Name Search box$")
	public void enter_App_name_into_Search_for_Apps_by_Name_Search_box() throws Throwable {
		
		advSearch.searchAppValidAppName(excelData(4, 2));
	}

	@Then("^: Verify the Search result$")
	public void verify_the_Search_result() throws Throwable {
		
		advSearch.expectedAppName = excelData(4, 2);
		advSearch.selectAppsFromDrpDwn();
	}

	@Given("^: Search and Select multiple App Name into Search for Apps by Name Search box$")
	public void search_and_Select_multiple_App_Name_into_Search_for_Apps_by_Name_Search_box() throws Throwable {
		
		advSearch.selectMultipleAppsFromDrpDwn(excelData(6, 2));
	}

	@Then("^: Verify search Multiple apps functionality$")
	public void verify_search_Multiple_apps_functionality() throws Throwable {
		
		advSearch.expectedAppName = excelData(6, 2);
		advSearch.verifyMultipleApps(excelData(6, 2));
	}

	@Given("^: Enter App Name having (\\d+) record in Search for Apps by Name Search box$")
	public void enter_App_Name_having_record_in_Search_for_Apps_by_Name_Search_box(int arg1) throws Throwable {
		
		advSearch.enterAppsHavingNoRecord(excelData(7, 2));
	}

	@Given("^: Select the App$")
	public void select_the_App() throws Throwable {
		
		advSearch.clickGoBtn();
	}

	@Then("^: Verify that system should display \"([^\"]*)\" message when a user selects App having (\\d+) record$")
	public void verify_that_system_should_display_message_when_a_user_selects_App_having_record(String arg1, int arg2) throws Throwable {
		
		advSearch.verifyNoRecordsFoundMsgInListings();
	}

	@Then("^: Verify that Records You Created Today text should be display under Quick Access$")
	public void verify_that_Records_You_Created_Today_text_should_be_display_under_Quick_Access() throws Throwable {
		
		advSearch.verifyRecordsYouCreatedToday();
	}

	@Then("^: Verify that Records Pending Your Review text should display under Quick Access$")
	public void verify_that_Records_Pending_Your_Review_text_should_display_under_Quick_Access() throws Throwable {
		
		advSearch.verifyRecordsPendingYourReview();
	}

	@Then("^: Verify that Records You Accessed Today text should display under Quick Access$")
	public void verify_that_Records_You_Accessed_Today_text_should_display_under_Quick_Access() throws Throwable {
		
		advSearch.verifyRecordsYouAccessedToday();
	}

	@Then("^: Verify that Records Assigned To You text should display under Quick Access$")
	public void verify_that_Records_Assigned_To_You_text_should_display_under_Quick_Access() throws Throwable {
		
		advSearch.verifyRecordsAssignedToYou();
	}

	@Then("^: Verify that Color of Records You Created Today text should be blue$")
	public void verify_that_Color_of_Records_You_Created_Today_text_should_be_blue() throws Throwable {
		
		advSearch.verifyRecordsYouCreatedTodayColor();
	}

	@Then("^: Verify that Color of Records Pending Your Review text should be blue$")
	public void verify_that_Color_of_Records_Pending_Your_Review_text_should_be_blue() throws Throwable {
		
		advSearch.verifyRecordsPendingYourReviewColor();
	}

	@Then("^: Verify that Color of Records You Accessed Today text should be blue$")
	public void verify_that_Color_of_Records_You_Accessed_Today_text_should_be_blue() throws Throwable {
		
		advSearch.verifyRecordsYouAccessedTodayColor();
	}

	@Then("^: Verify that Color of Records Assigned To You text should be blue$")
	public void verify_that_Color_of_Records_Assigned_To_You_text_should_be_blue() throws Throwable {
		
		advSearch.verifyRecordsAssignedToYouColor();
	}

	@Given("^: Click on Expand button of Filter By section$")
	public void click_on_Expand_button_of_Filter_By_section() throws Throwable {
		
		advSearch.clickFilterByExpand();
	}

	@Given("^: Click on Collapes button$")
	public void click_on_Collapes_button() throws Throwable {
		
		advSearch.clickFilterByCollapse();
	}

	@Then("^: Verify that system should Collapes the Filter By options when a user click on the Collapes button$")
	public void verify_that_system_should_Collapes_the_Filter_By_options_when_a_user_click_on_the_Collapes_button() throws Throwable {
		
		advSearch.verifyCollapseFilterByOption();
	}

	@Given("^: Select App from Search for Apps by Names search box$")
	public void select_App_from_Search_for_Apps_by_Names_search_box() {
		
		try {
			advSearch.searchForApp(excelData(8, 2));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Given("^: Select App from Search for Apps by Names search box for bacth review$")
	public void select_App_from_Search_for_Apps_by_Names_search_box_for_bacth_review() throws Throwable {
		
		advSearch.searchForApp(excelData(23, 2));
	}

	@Then("^: Verify that Color of Go button should be green$")
	public void verify_that_Color_of_Go_button_should_be_green() throws Throwable {
		
		advSearch.verifyGoBtnColor();
	}

	@Given("^: Click on Go button$")
	public void click_on_Go_button() throws Throwable {
		
		advSearch.clickGoBtn();
	}

	@Then("^: Verify that system should display app record in grid when a user search an app$")
	public void verify_that_system_should_display_app_record_in_grid_when_a_user_search_an_app() throws Throwable {
		
		advSearch.verifyBatchReviewLink();
	}

	@Given("^: Click on Clear All button$")
	public void click_on_Clear_All_button() throws Throwable {
		
		advSearch.clickClearAllBtn();
	}

	@Then("^: Verify that Clear All button is clickable or not$")
	public void verify_that_Clear_All_button_is_clickable_or_not() throws Throwable {
		
		advSearch.verifyClearAllFunctionality();
	}

	@Then("^: Verify that Color of Clear All button should be black$")
	public void verify_that_Color_of_Clear_All_button_should_be_black() throws Throwable {
		
		advSearch.clickClearAllBtnColor();
	}

	@Then("^: Verify that system should remove all selection from Advanced Search screen when a user click on the Clear All button$")
	public void verify_that_system_should_remove_all_selection_from_Advanced_Search_screen_when_a_user_click_on_the_Clear_All_button() throws Throwable {
		
		advSearch.verifyAllFieldGetCleared();
	}
	@Given("^: Click on Collapes button of Grid$")
	public void click_on_Collapes_button_of_Grid() throws Throwable {
		
		advSearch.expectedAppName = excelData(8, 2);
		advSearch.clickCollapseBtnGrid();
	}

	@Then("^: Verify that system should Collapes the grid record when a user click on the Collapes button$")
	public void verify_that_system_should_Collapes_the_grid_record_when_a_user_click_on_the_Collapes_button() throws Throwable {
		
		advSearch.verifyCollapseBtnGrid();
	}

	@Then("^: Verify that system should display CSV icon at right side of the Grid$")
	public void verify_that_system_should_display_CSV_icon_at_right_side_of_the_Grid() throws Throwable {
		
		advSearch.verifyCSVIconIsVisible();
	}

	@Then("^: Verify that system should display Excel icon at right side of the Grid$")
	public void verify_that_system_should_display_Excel_icon_at_right_side_of_the_Grid() throws Throwable {
		
		advSearch.verifyExcelIconIsVisible();
	}

	@Then("^: Verify that system should display PDF icon at right side of the Grid$")
	public void verify_that_system_should_display_PDF_icon_at_right_side_of_the_Grid() throws Throwable {
		
		advSearch.verifyPDFIconIsVisible();
	}

	@Then("^: Verify that Color of Add To Favorite button should be blue$")
	public void verify_that_Color_of_Add_To_Favorite_button_should_be_blue() throws Throwable {
		
		advSearch.verifyAddToFavoriteBtn();
	}

	@Given("^: Click on Add To Favorite button of Advanced Search$")
	public void click_on_Add_To_Favorite_button() throws Throwable {
		
		advSearch.clickAddToFavoriteBtn();
	}

	@Given("^: Click on Save button of Add To Favorite pop-up$")
	public void click_on_Save_button_of_Add_To_Favorite_pop_up() throws Throwable {
		
		advSearch.clickAddToFavoriteSaveBtn();
	}

	@Then("^: Verify that system should display Require Validation message for Favorite Name as \"([^\"]*)\"$")
	public void verify_that_system_should_display_Require_Validation_message_for_Favorite_Name_as(String arg1) throws Throwable {
		
		advSearch.verifyAddToFavoriteRequiredMsg();
	}

	@Then("^: Verify that Color of Require validation message color should be red$")
	public void verify_that_Color_of_Require_validation_message_color_should_be_red() throws Throwable {
		
		advSearch.verifyAddToFavoriteRequiredMsgColor();
	}

	@Given("^: Click on Enter Favorite Name textbox$")
	public void click_on_Enter_Favorite_Name_textbox() throws Throwable {
		
		advSearch.clickAddToFavoriteTextField();
	}

	@Given("^: Enter value in Favorite Name as \"([^\"]*)\"$")
	public void enter_value_in_Favorite_Name_as(String arg1) throws Throwable {
		
		advSearch.enterAddToFavoriteMinimumChar(excelData(9, 2));
	}

	@Then("^: Verify that system should display Minimum validation message for Favorite Name as \"([^\"]*)\"$")
	public void verify_that_system_should_display_Minimum_validation_message_for_Favorite_Name_as(String arg1) throws Throwable {
		
		advSearch.verifyAddToFavoriteMinimumMsg();
	}

	@Then("^: Verify that Color of Minimum validation message should be red$")
	public void verify_that_Color_of_Minimum_validation_message_should_be_red() throws Throwable {
		
		advSearch.verifyAddToFavoriteMinimumMsgColor();
	}

	@Given("^: Enter Favorite Name value more than (\\d+) characters$")
	public void enter_Favorite_Name_value_more_than_characters(int arg1) throws Throwable {
		
		advSearch.enterAddToFavoriteMaximumChar(excelData(10, 2));
	}

	@Then("^: Verify Maximum validation message of Favorite Name textbox as \"([^\"]*)\"$")
	public void verify_Maximum_validation_message_of_Favorite_Name_textbox_as(String arg1) throws Throwable {
		
		advSearch.verifyAddToFavoriteMaximumMsg();
	}

	@Then("^: Verify the Color of Maximum validation message for Favorite Name should be red$")
	public void verify_the_Color_of_Maximum_validation_message_for_Favorite_Name_should_be_red() throws Throwable {
		
		advSearch.verifyAddToFavoriteMaximumMsgColor();
	}

	@Given("^: Enter Valid Favorite Name value$")
	public void enter_Valid_Favorite_Name_value() throws Throwable {
		
		advSearch.enterAddToFavoriteValidChar(excelData(11, 2));
	}

	@Then("^: Verify that system should not display validation message when a user enters valid Favorite Name$")
	public void verify_that_system_should_not_display_validation_message_when_a_user_enters_valid_Favorite_Name() throws Throwable {
		
		advSearch.verifyAddToFavoriteNoValidationMsg();
	}

	@Then("^: Verify that system should display Add To Favorite label on pop-up$")
	public void verify_that_system_should_display_Add_To_Favorite_label_on_pop_up() throws Throwable {
		
		advSearch.verifyAddToFavoriteLabel();
	}

	@Given("^: Click on close icon of Add To Favorite pop-up$")
	public void click_on_close_icon_of_Add_To_Favorite_pop_up() throws Throwable {
		
		advSearch.clickCloseIconAddToFavorite();
	}

	@Then("^: Verify that system should close the Add To Favorites pop-up$")
	public void verify_that_system_should_close_the_Add_To_Favorites_pop_up() throws Throwable {
		
		advSearch.verifyCloseIconFunctionalityOfAddToFavorite();
	}

	@Given("^: Click on Cancel button of Advanced Search$")
	public void click_on_Cancel_button() throws Throwable {
		
		advSearch.clickCancelBtnAddToFavorite();
	}

	@Then("^: Verify that system should remove the Add to Favorite pop-up when a user click on the Cancel button$")
	public void verify_that_system_should_remove_the_Add_to_Favorite_pop_up_when_a_user_click_on_the_Cancel_button() throws Throwable {
		
		advSearch.verifyCancelBtnFunctionalityOfAddToFavorite();
	}

	@Then("^: Verify that by default My Favorites radio button is selected in add to favorite pop-up$")
	public void verify_that_by_default_My_Favorites_radio_button_is_selected_in_add_to_favorite_pop_up() throws Throwable {
		
		advSearch.verifyMyFavoriteRadioSelectedByDefault();
	}

	@Then("^: Verify that Save button Color should be green$")
	public void verify_that_Save_button_Color_should_be_green() throws Throwable {
		
		advSearch.verifyMyFavoriteSaveBtnColor();
	}

	@Then("^: Verify that Cancel button Color should be black$")
	public void verify_that_Cancel_button_Color_should_be_black() throws Throwable {
		
		advSearch.verifyMyFavoriteCancelBtnColor();
	}

	@Given("^: Click on Save button of Advanced Search$")
	public void click_on_Save_button() throws Throwable {
		
		advSearch.clickMyFavoriteSaveBtn();
		
	}

	@Then("^: Verify that system should Save the App in My Favorites list when a user click on the Save button$")
	public void verify_that_system_should_Save_the_App_in_My_Favorites_list_when_a_user_click_on_the_Save_button() throws Throwable {
		
		advSearch.verifySavedFavoriteDisplayInListings(excelData(11, 2));
	}

	@Given("^: Enter Already Exist Name in Favorite Name$")
	public void enter_Already_Exist_Name_in_Favorite_Name() throws Throwable {
		
		advSearch.enterAddToFavoriteDuplicate(excelData(12, 2));
	}

	@Then("^: Verify that system should display Unique validation message as \"([^\"]*)\"$")
	public void verify_that_system_should_display_Unique_validation_message_as(String arg1) throws Throwable {
		
		advSearch.verifyMyFavoriteUniqueMsg();
	}

	@Given("^: Now Click on the Remove button for Favorite App Name$")
	public void now_Click_on_the_Remove_button_for_Favorite_App_Name() throws Throwable {
		
		advSearch.clickRemoveIconMyFavorites();
	}

	@Then("^: Verify that system should display My Favorite Remove pop-up label as \"([^\"]*)\"$")
	public void verify_that_system_should_display_My_Favorite_Remove_pop_up_label_as(String arg1) throws Throwable {
		
		advSearch.verifyRemoveMsgMyFavorites(excelData(11, 2));
	}

	@Then("^: Verify that Color of Yes button in My Favorite Remove pop-up should be green$")
	public void verify_that_Color_of_Yes_button_in_My_Favorite_Remove_pop_up_should_be_green() throws Throwable {
		
		advSearch.verifyMyFavoriteRemoveYesBtnColor();
	}

	@Then("^: Verify that Color of No button in My Favorite Remove pop-up should be red$")
	public void verify_that_Color_of_No_button_in_My_Favorite_Remove_pop_up_should_be_red() throws Throwable {
		
		advSearch.verifyMyFavoriteRemoveNoBtnColor();
	}

	@Given("^: Click on the No button$")
	public void click_on_the_No_button() throws Throwable {
		
		advSearch.clickMyFavoriteRemoveNoBtn();
	}

	@Then("^: Verify that system should not remove Favorite Name from My Favorites list when a user click on the No button$")
	public void verify_that_system_should_not_remove_Favorite_Name_from_My_Favorites_list_when_a_user_click_on_the_No_button() throws Throwable {
		
		advSearch.verifyMyFavoriteRemoveNoBtnClick(excelData(11, 2));
	}

	@Then("^: Verify that Favorite Name should be added to the My Favorites list when a user click on the Save button$")
	public void verify_that_Favorite_Name_should_be_added_to_the_My_Favorites_list_when_a_user_click_on_the_Save_button() throws Throwable {
		
		advSearch.verifySavedDataInMyFavorite(excelData(11, 2));
	}

	@Then("^: Verify that system should display Filter By label below App Name Search box$")
	public void verify_that_system_should_display_Filter_By_label_below_App_Name_Search_box() throws Throwable {
		
		advSearch.verifyFilterByLabel();
	}

	@Then("^: Verify that All Filter icons are visiable when a user expand the Filter By Section$")
	public void verify_that_All_Filter_icons_are_visiable_when_a_user_expand_the_Filter_By_Section() throws Throwable {
		
		advSearch.verifyFilterByIconsVisible();
	}

	@Then("^: Verify that spelling of Date range icon should be Correct$")
	public void verify_that_spelling_of_Date_range_icon_should_be_Correct() throws Throwable {
		
		advSearch.verifyDateRangeSpelling();
	}

	@Then("^: Verify that spelling of Attributes/Entity icon should be Correct$")
	public void verify_that_spelling_of_Attributes_Entity_icon_should_be_Correct() throws Throwable {
		
		advSearch.verifyAttributesEntitiesSpelling();
	}

	@Then("^: Verify that spelling of Sites icon should be Correct$")
	public void verify_that_spelling_of_Sites_icon_should_be_Correct() throws Throwable {
		
		advSearch.verifySitesSpelling();
	}

	@Then("^: Verify that spelling of Batch Entries icon should be Correct$")
	public void verify_that_spelling_of_Batch_Entries_icon_should_be_Correct() throws Throwable {
		
		advSearch.verifyBatchEntriesSpelling();
	}

	@Then("^: Verify that spelling of Assignee icon should be Correct$")
	public void verify_that_spelling_of_Assignee_icon_should_be_Correct() throws Throwable {
		
		advSearch.verifyAssigneeSpelling();
	}

	@Then("^: Verify that spelling of Record Status icon should be Correct$")
	public void verify_that_spelling_of_Record_Status_icon_should_be_Correct() throws Throwable {
		
		advSearch.verifyRecordStatusSpelling();
	}

	@Given("^: Select Today option from Date dropdown for Date Range Filter$")
	public void select_Today_option_from_Date_dropdown_for_Date_Range_Filter() throws Throwable {
		
		advSearch.selectTodaysDateFilterBy();
	}

	@Then("^: Verify that system should display Current Date when a user selects Today option from Date dropdown$")
	public void verify_that_system_should_display_Current_Date_when_a_user_selects_Today_option_from_Date_dropdown() throws Throwable {
		
		advSearch.verifyTodaysDateFilterBy();
	}

	@Given("^: Select This week option from Date dropdown for Date Range Filter$")
	public void select_This_week_option_from_Date_dropdown_for_Date_Range_Filter() throws Throwable {
		
		advSearch.selectThisWeekDateFilterBy();
	}

	@Then("^: Verify that system should display Correct Dates when a user selects This week option from Date dropdown$")
	public void verify_that_system_should_display_Correct_Dates_when_a_user_selects_This_week_option_from_Date_dropdown() throws Throwable {
		
		advSearch.verifyThisWeekFilterBy();
	}

	@Then("^: Verify that system should display By Default Date range for This Week option$")
	public void verify_that_system_should_display_By_Default_Date_range_for_This_Week_option() throws Throwable {
		
		advSearch.verifyThisWeekFilterByByDefault();
	}

	@Given("^: Select This Month option from Date dropdown for Date Range Filter$")
	public void select_This_Month_option_from_Date_dropdown_for_Date_Range_Filter() throws Throwable {
		
		advSearch.selectThisMonthFilterBy();
	}

	@Then("^: Verify that system should display Correct Dates when a user selects This Month option from Date dropdown$")
	public void verify_that_system_should_display_Correct_Dates_when_a_user_selects_This_Month_option_from_Date_dropdown() throws Throwable {
		
		advSearch.verifyThisMonthFilterBy();
	}

	@Then("^: Verify that system should display By Default Date range for This Month option$")
	public void verify_that_system_should_display_By_Default_Date_range_for_This_Month_option() throws Throwable {
		
		advSearch.verifyThisMonthFilterByByDefault();
	}

	@Then("^: Verify that Clear link is visible to the user or not for Date Range Filter$")
	public void verify_that_Clear_link_is_visible_to_the_user_or_not_for_Date_Range_Filter() throws Throwable {
		
		advSearch.verifyClearLinkVisibleInDateRange();
	}

	@Then("^: Verify that Color of Clear link for Date Range Filter should be blue$")
	public void verify_that_Color_of_Clear_link_for_Date_Range_Filter_should_be_blue() throws Throwable {
		
		advSearch.verifyClearLinkVisibleInDateRangeColor();
	}

	@Given("^: Click on Clear link of Date Range$")
	public void click_on_Clear_link_of_Date_Range() throws Throwable {
		
		advSearch.clickClearLinkFilterBy();
	}

	@Then("^: Verify that system should Clear all the Selection for Date Range Filter$")
	public void verify_that_system_should_Clear_all_the_Selection_for_Date_Range_Filter() throws Throwable {
		
		advSearch.verifyAllFieldIsCleared();
	}

	@Then("^: Verify that system should display Remove Icon for Date Range Filter$")
	public void verify_that_system_should_display_Remove_Icon_for_Date_Range_Filter() throws Throwable {
		
		advSearch.verifyRemoveIconIsVisible();
	}

	@Given("^: Select Month-Year radio button$")
	public void select_Month_Year_radio_button() throws Throwable {
		
		advSearch.clickMonthYearDrpdwn();
	}

	@Then("^: Verify that system should display Select Month and Select Year dropdown when a user selects Month-Year radio button$")
	public void verify_that_system_should_display_Select_Month_and_Select_Year_dropdown_when_a_user_selects_Month_Year_radio_button() throws Throwable {
		
		advSearch.verifyMonthYearDrpdwnIsVisible();
	}

	@Then("^: Verify that remove icon Color for Date Range Filter should be red$")
	public void verify_that_remove_icon_Color_for_Date_Range_Filter_should_be_red() throws Throwable {
		
		advSearch.verifyRemoveIconColor();
	}

	@Given("^: Select App having this week record from Search for Apps by Names search box$")
	public void select_App_having_this_week_record_from_Search_for_Apps_by_Names_search_box() throws Throwable {
		
		advSearch.searchForThisWeekApp(excelData(13, 2));
	}

	@Then("^: Verify that system should display only this week record in Grid for selected App when user select This week Filter$")
	public void verify_that_system_should_display_only_this_week_record_in_Grid_for_selected_App_when_user_select_This_week_Filter() throws Throwable {
		
		advSearch.VerifySearchForThisWeekApp();
	}

	@Given("^: Select App having Today record from Search for Apps by Names search box$")
	public void select_App_having_Today_record_from_Search_for_Apps_by_Names_search_box() throws Throwable {
		
		advSearch.searchForTodayApp(excelData(14, 2));
	}

	@Then("^: Verify that system should display only Today record in Grid for selected App when user select Today Filter$")
	public void verify_that_system_should_display_only_Today_record_in_Grid_for_selected_App_when_user_select_Today_Filter() throws Throwable {
		
		advSearch.verifySearchForTodayApp();
	}

	@Given("^: Select App having This Month record from Search for Apps by Names search box$")
	public void select_App_having_This_Month_record_from_Search_for_Apps_by_Names_search_box() throws Throwable {
		
		advSearch.searchForThisMonthApp(excelData(15, 2));
	}

	@Then("^: Verify that system should display only This Month record for selected App when user select This Month Filter$")
	public void verify_that_system_should_display_only_This_Month_record_for_selected_App_when_user_select_This_Month_Filter() throws Throwable {
		
		advSearch.verifySearchForThisMonthApp();
	}

	@Given("^: Select Specific Date option from Date dropdown for Date Range Filter$")
	public void select_Specific_Date_option_from_Date_dropdown_for_Date_Range_Filter() throws Throwable {
		
		advSearch.selectSpecificDateFromDrpDwn(excelData(30,5));
	}

	@Given("^: Select Specific Date from Date Picker$")
	public void select_Specific_Date_from_Date_Picker() throws Throwable {
		
		advSearch.selectSpecificDate();
	}

	@Then("^: Verify that system should display record for selected Date for the Selected app when a user selects Specific Date option$")
	public void verify_that_system_should_display_record_for_selected_Date_for_the_Selected_app_when_a_user_selects_Specific_Date_option() throws Throwable {
		
		advSearch.verifyBySelectSpecificDate();
	}

	@Given("^: Select Month from Select Month dropdown$")
	public void select_Month_from_Select_Month_dropdown() throws Throwable {
		
		advSearch.selectMonthFromDrpDwn(excelData(16, 2));
	}

	@Then("^: Verify that Required Validation message should be display as \"([^\"]*)\"$")
	public void verify_that_Required_Validation_message_should_be_display_as(String arg1) throws Throwable {
		
		advSearch.verifyYearRequiredMsg();
	}

	@Then("^: Verify that Color of Required Validation for Date Range should be red$")
	public void verify_that_Color_of_Required_Validation_for_Date_Range_should_be_red() throws Throwable {
		
		advSearch.verifyYearRequiredMsgColor();
	}

	@Given("^: Select Year from Select Year dropdown$")
	public void select_Year_from_Select_Year_dropdown() throws Throwable {
		
		advSearch.selectYearFromDrpDwn(excelData(17, 2));
	}

	@Then("^: Verify that system should display Records of selected month and year for the Selected App$")
	public void verify_that_system_should_display_Records_of_selected_month_and_year_for_the_Selected_App() throws Throwable {
		
		advSearch.verifyMonthYearProperData(excelData(16, 2), excelData(17, 2));
	}

	@Then("^: Verify that system should display \"([^\"]*)\" label for Attributes/Entity Filter$")
	public void verify_that_system_should_display_label_for_Attributes_Entity_Filter(String arg1) throws Throwable {
		
		advSearch.attributesLabel();
	}

	@Given("^: Click on Attributes/Entity icon$")
	public void click_on_Attributes_Entity_icon() throws Throwable {
		
		advSearch.clickAttributesIcon();
	}

	@Given("^: Select Attributes/Entity Key Attributes from dropdown$")
	public void select_Attributes_Entity_Key_Attributes_from_dropdown() throws Throwable {
		
		advSearch.selectKeyAttributesDrpDwn(excelData(18, 2));
	}

	@Then("^: Verify that system display \"([^\"]*)\" validation message when a user doesn't select value of Selected Attributes/Entity Key Attributes$")
	public void verify_that_system_display_validation_message_when_a_user_doesn_t_select_value_of_Selected_Attributes_Entity_Key_Attributes(String arg1) throws Throwable {
		
		advSearch.verifyValueRequiredMsg();
	}

	@Then("^: Verify that Color of Required validation message should be red$")
	public void verify_that_Color_of_Required_validation_message_should_be_red() throws Throwable {
		
		advSearch.verifyValueRequiredMsgColor();
	}

	@Then("^: Verify that system should display Add New Attribute Button$")
	public void verify_that_system_should_display_Add_New_Attribute_Button() throws Throwable {
		
		advSearch.verifyAddAttributeIcon();
	}

	@Then("^: Verify that Color of Add New Attribute button should be green$")
	public void verify_that_Color_of_Add_New_Attribute_button_should_be_green() throws Throwable {
		
		advSearch.verifyAddAttributeIconColor();
	}

	@Given("^: Enter Value for the Selected Attribute/Entity$")
	public void enter_Value_for_the_Selected_Attribute_Entity() throws Throwable {
		
		advSearch.selectAttributeValue(excelData(19, 2));
	}

	@Given("^: Click on Add New Attribute button$")
	public void click_on_Add_New_Attribute_button() throws Throwable {
		
		advSearch.clickAddAttributes();
	}

	@Then("^: Verify that system should display the app record as per Attribute/Entity Filter$")
	public void verify_that_system_should_display_the_app_record_as_per_Attribute_Entity_Filter() throws Throwable {
		
		advSearch.verifyAttributeFilterData();
	}

	@Then("^: Verify that system should display Remove Attributes/Entity button when a user adds Attributes/Entity$")
	public void verify_that_system_should_display_Remove_Attributes_Entity_button_when_a_user_adds_Attributes_Entity() throws Throwable {
		
		advSearch.verifyRemoveIconVisible();
	}

	@Then("^: Verify that Color of Attributes/Entity Remove should be red$")
	public void verify_that_Color_of_Attributes_Entity_Remove_should_be_red() throws Throwable {
		
		advSearch.removeIconColor();
	}

	@Given("^: Click on Remove Attributes/Entity button$")
	public void click_on_Remove_Attributes_Entity_button() throws Throwable {
		
		advSearch.clickRemoveIcon();
	}

	@Then("^: Verify that system should remove the Added Attribute/Entity when a user click on the Remove button$")
	public void verify_that_system_should_remove_the_Added_Attribute_Entity_when_a_user_click_on_the_Remove_button() throws Throwable {
		
		advSearch.verifyRemoveIconFunctionality();
	}

	@Then("^: Verify that system should display Correct data in grid as per applied filter for Key Attribute$")
	public void verify_that_system_should_display_Correct_data_in_grid_as_per_applied_filter_for_Key_Attribute() throws Throwable {
		
		advSearch.verifyDataAccordingly(excelData(18, 2), excelData(19, 2));
	}

	@Then("^: Verify that system should display Clear link for Attributes/Entity Filter$")
	public void verify_that_system_should_display_Clear_link_for_Attributes_Entity_Filter() throws Throwable {
		
		advSearch.verifyClearLinkVisible();
	}

	@Then("^: Verify that Color of Clear link for Attributes/Entity Filter should be blue$")
	public void verify_that_Color_of_Clear_link_for_Attributes_Entity_Filter_should_be_blue() throws Throwable {
		
		advSearch.verifyClearLinkVisibleColor();
	}

	@Given("^: Click on Clear link$")
	public void click_on_Clear_link() throws Throwable {
		
		advSearch.clickAttributesClearLink();
	}

	@Then("^: Verify that system should Clear all Selection for Attribute/Entity filter when a user click on the Clear link$")
	public void verify_that_system_should_Clear_all_Selection_for_Attribute_Entity_filter_when_a_user_click_on_the_Clear_link() throws Throwable {
		
		advSearch.verifyAttributesClearLinkFunctionality();
	}

	@Then("^: Verify that system should display Remove icon for Attributes/Entity Filter$")
	public void verify_that_system_should_display_Remove_icon_for_Attributes_Entity_Filter() throws Throwable {
		
		advSearch.verifyAttributeRemoveIconVisible();
	}

	@Then("^: Verify that Color of Remove icon for Attributes/Entity filter should be red$")
	public void verify_that_Color_of_Remove_icon_for_Attributes_Entity_filter_should_be_red() throws Throwable {
		
		advSearch.attributeRemoveIconColor();
	}

	@Given("^: Click on Remove icon$")
	public void click_on_Remove_icon() throws Throwable {
		
		advSearch.clickRemoveIcon();
	}

	@Then("^: Verify that system should Remove the Attributes/Entity Key Attributes section when a user click on Remove icon$")
	public void verify_that_system_should_Remove_the_Attributes_Entity_Key_Attributes_section_when_a_user_click_on_Remove_icon() throws Throwable {
		
		advSearch.verifyRemoveIconFunctionality();
	}

	@Then("^: Verify that system should display \"([^\"]*)\" label for Sites Filter$")
	public void verify_that_system_should_display_label_for_Sites_Filter(String arg1) throws Throwable {
		
		advSearch.verifySitesLabel();
	}

	@Given("^: Click on Sites icon$")
	public void click_on_Sites_icon() throws Throwable {
		
		advSearch.clickOnSites();
	}

	@Then("^: Verify that system should display Clear link for Sites Filter$")
	public void verify_that_system_should_display_Clear_link_for_Sites_Filter() throws Throwable {
		
		advSearch.verifySitesClearLink();
	}

	@Then("^: Verify Color of Clear link for Sites Filter should be blue$")
	public void verify_Color_of_Clear_link_for_Sites_Filter_should_be_blue() throws Throwable {
		
		advSearch.verifySitesClearLinkColor();
	}

	@Given("^: Select value form the dropdown for Sites$")
	public void select_value_form_the_dropdown_for_Sites() throws Throwable {
		
		advSearch.selectSitesDrpDwn();
	}

	@Given("^: Click on Clear link of Sites$")
	public void click_on_Clear_link_of_Sites() throws Throwable {
		
		advSearch.clickSitesClearLink();
	}

	@Then("^: Verify that system should Clear selection for Sites when a user click on the Clear link$")
	public void verify_that_system_should_Clear_selection_for_Sites_when_a_user_click_on_the_Clear_link() throws Throwable {
		
		advSearch.verifySitesClearLinkFunctionality();
	}

	@Then("^: Verify that system should display Remove icon for Sites filter$")
	public void verify_that_system_should_display_Remove_icon_for_Sites_filter() throws Throwable {
		
		advSearch.verifySitesRemoveIconVisible();
	}

	@Then("^: Verify that Color of Remove icon should be red for Sites filter$")
	public void verify_that_Color_of_Remove_icon_should_be_red_for_Sites_filter() throws Throwable {
		
		advSearch.verifySitesRemoveIconColor();
	}

	@Given("^: Click on Remove icon of Sites$")
	public void click_on_Remove_icon_of_Sites() throws Throwable {
		
		advSearch.clickSitesRemoveIcon();
	}

	@Then("^: Verify that system should Remove Sites section from Filter when a user click on Remove icon$")
	public void verify_that_system_should_Remove_Sites_section_from_Filter_when_a_user_click_on_Remove_icon() throws Throwable {
		
		advSearch.verifySitesRemoveIconFunctionality();
	}

	@Then("^: Verify that system should display Batch Entries label for Batch Entries filter$")
	public void verify_that_system_should_display_Batch_Entries_label_for_Batch_Entries_filter() throws Throwable {
		
		advSearch.verifyBatchEntryLabel();
	}

	@Given("^: Click on Batch Entries filter icon$")
	public void click_on_Batch_Entries_filter_icon() throws Throwable {
		
		advSearch.clickBatchEntries();
	}

	@Given("^: Enter more than (\\d+) characters into Batch Entry ID textbox$")
	public void enter_more_than_characters_into_Batch_Entry_ID_textbox(int arg1) throws Throwable {
		
		advSearch.enterTwentyCharBatchEntriesField(excelData(20, 2));
	}

	@Then("^: Verify that system should display Maximum validation message as \"([^\"]*)\"$")
	public void verify_that_system_should_display_Maximum_validation_message_as(String arg1) throws Throwable {
		
		advSearch.verifyBatchEntiesMaximimMsg();
	}

	@Then("^: Verify that Color of Maximum validation message should be red$")
	public void verify_that_Color_of_Maximum_validation_message_should_be_red() throws Throwable {
		
		advSearch.verifyBatchEntiesMaximimMsgColor();
	}

	@Given("^: Click on Batch Entries ID textbox$")
	public void click_on_Batch_Entries_ID_textbox() throws Throwable {
		
		advSearch.clickBatchEntiesField();
	}

	@Given("^: Enter Valid Batch Entry ID into Batch Entry ID textbox$")
	public void enter_Valid_Batch_Entry_ID_into_Batch_Entry_ID_textbox() throws Throwable {
		
		advSearch.enterBatchEntiesValidChar(excelData(21, 2));
	}

	@Then("^: Verify that system should display correct records in Grid as per applied filter for Batch Entry ID$")
	public void verify_that_system_should_display_correct_records_in_Grid_as_per_applied_filter_for_Batch_Entry_ID() throws Throwable {
		
		advSearch.verifyBatchEntitiesSearching(excelData(21, 2));
	}

	@Then("^: Verify that system should display Clear link for Batch Entry ID Filter$")
	public void verify_that_system_should_display_Clear_link_for_Batch_Entry_ID_Filter() throws Throwable {
		
		advSearch.verifyBatchEntryClearLinkVisible();
	}

	@Then("^: Verify that Color of Clear link for Batch Entries Filter should be blue$")
	public void verify_that_Color_of_Clear_link_for_Batch_Entries_Filter_should_be_blue() throws Throwable {
		
		advSearch.verifyBatchEntryClearLinkColor();
	}

	@Given("^: Click on Clear link of Batch Entries$")
	public void click_on_Clear_link_of_Batch_Entries() throws Throwable {
		
		advSearch.clickBatchEntryClearLink();
	}

	@Then("^: Verify that system should Clear Batch Entry ID from the Batch Entry ID textbox$")
	public void verify_that_system_should_Clear_Batch_Entry_ID_from_the_Batch_Entry_ID_textbox() throws Throwable {
		
		advSearch.verifyBatchEntryClearLinkFunctionality();
	}

	@Then("^: Verify that system should display Remove icon for Batch Entries filter$")
	public void verify_that_system_should_display_Remove_icon_for_Batch_Entries_filter() throws Throwable {
		
		advSearch.verifyBatchEntryRemoveIconVisible();
	}

	@Then("^: Verify that Color of Remove icon should be red for Batch Entries filter$")
	public void verify_that_Color_of_Remove_icon_should_be_red_for_Batch_Entries_filter() throws Throwable {
		
		advSearch.verifyBatchEntryRemoveIconColor();
	}

	@Given("^: Click on Remove icon of Batch Entry$")
	public void click_on_Remove_icon_of_Batch_Entry() throws Throwable {
		
		advSearch.clickBatchEntryRemoveIcon();
	}

	@Then("^: Verify that system should remove Batch Entries Section when a user click on the Remove icon$")
	public void verify_that_system_should_remove_Batch_Entries_Section_when_a_user_click_on_the_Remove_icon() throws Throwable {
		
		advSearch.verifyBatchEntryRemoveIconFunctionality();
	}

	@Given("^: Click on Assignee filter icon$")
	public void click_on_Assignee_filter_icon() throws Throwable {
		
		advSearch.clickAssigneeIcon();
	}

	@Then("^: Verify that system should display Assignee label for (\\d+)st radio button$")
	public void verify_that_system_should_display_Assignee_label_for_st_radio_button(int arg1) throws Throwable {
		
		advSearch.verifyAssigneeRadioBtnVisible();
	}

	@Then("^: Verify that system should display Assignee To Me label for (\\d+)nd radio button$")
	public void verify_that_system_should_display_Assignee_To_Me_label_for_nd_radio_button(int arg1) throws Throwable {
		
		advSearch.verifyAssigneeToMeRadioBtnVisible();
	}

	@Then("^: Verify that Assignee radio button should be selected by default$")
	public void verify_that_Assignee_radio_button_should_be_selected_by_default() throws Throwable {
		
		advSearch.verifyAssigneeRadioBtnByDefaultSelected();
	}

	@Given("^: Click on Please Select Role\\(s\\) Dropdown$")
	public void click_on_Please_Select_Role_s_Dropdown() throws Throwable {
		
		advSearch.clickRoleDrpDwnVal();
	}

	@Given("^: Click on Checked All option$")
	public void click_on_Checked_All_option() throws Throwable {
		
		advSearch.clickCheckAllRoleDrpDwnVal();
	}

	@Then("^: Verify that system should select All the option of Select Role\\(s\\) dropdown$")
	public void verify_that_system_should_select_All_the_option_of_Select_Role_s_dropdown() throws Throwable {
		
		advSearch.verifyCheckAllRoleDrpDwnValFunctionality();
	}

	@Given("^: Click on Uncheck All option of Advanced Search$")
	public void click_on_Uncheck_All_option() throws Throwable {
		
		advSearch.clickUnCheckAllRoleDrpDwnVal();
	}

	@Then("^: Verify that system should deselect All the option of Select Role\\(s\\) dropdown$")
	public void verify_that_system_should_deselect_All_the_option_of_Select_Role_s_dropdown() throws Throwable {
		
		advSearch.verifyUnCheckAllRoleDrpDwnValFunctionality();
	}

	@Then("^: Verify that system should display Record Status label$")
	public void verify_that_system_should_display_Record_Status_label() throws Throwable {
		
		advSearch.verifyRecordStatusLabel();
	}

	@Given("^: Click on Record Status icon$")
	public void click_on_Record_Status_icon() throws Throwable {
		
		advSearch.clickRecordStatusLabel();
	}

	@Then("^: Verify that system should display six Checkbox for Record Status$")
	public void verify_that_system_should_display_six_Checkbox_for_Record_Status() throws Throwable {
		
		advSearch.verifyRecordStatusCount(excelData(22, 2));
	}

	@Then("^: Verify that system should display label as Passed for checkbox of Passed Record Status$")
	public void verify_that_system_should_display_label_as_Passed_for_checkbox_of_Passed_Record_Status() throws Throwable {
		
		advSearch.verifyPassedRecordStatus();
	}

	@Then("^: Verify that system should display label as Failed for checkbox of Failed Record Status$")
	public void verify_that_system_should_display_label_as_Failed_for_checkbox_of_Failed_Record_Status() throws Throwable {
		
		advSearch.verifyFailedRecordStatus();
	}

	@Then("^: Verify that system should display label as Review Pending for checkbox of Review Pending Record Status$")
	public void verify_that_system_should_display_label_as_Review_Pending_for_checkbox_of_Review_Pending_Record_Status() throws Throwable {
		
		advSearch.verifyReviewPendingRecordStatus();
	}

	@Then("^: Verify that system should display label as Void for checkbox of Void Record Status$")
	public void verify_that_system_should_display_label_as_Void_for_checkbox_of_Void_Record_Status() throws Throwable {
		
		advSearch.verifyVoidRecordStatus();
	}

	@Then("^: Verify that system should display label as Draft for checkbox of Draft Record Status$")
	public void verify_that_system_should_display_label_as_Draft_for_checkbox_of_Draft_Record_Status() throws Throwable {
		
		advSearch.verifyDraftRecordStatus();
	}

	@Then("^: Verify that system should display label as Rejected for checkbox of Rejected Record Status$")
	public void verify_that_system_should_display_label_as_Rejected_for_checkbox_of_Rejected_Record_Status() throws Throwable {
		
		advSearch.verifyRejectedRecordStatus();
	}

	@Given("^: Click on Passed checkbox$")
	public void click_on_Passed_checkbox() throws Throwable {
		
		advSearch.clickPassedRecordCheckbox();
	}

	@Then("^: Verify that record status Passed Checkbox is clickable or not$")
	public void verify_that_record_status_Passed_Checkbox_is_clickable_or_not() throws Throwable {
		
		advSearch.verifyPassedRecordCheckboxFunctionality();
	}

	@Given("^: Click on Failed checkbox$")
	public void click_on_Failed_checkbox() throws Throwable {
		
		advSearch.clickFailedRecordCheckbox();
	}

	@Then("^: Verify that record status Failed Checkbox is clickable or not$")
	public void verify_that_record_status_Failed_Checkbox_is_clickable_or_not() throws Throwable {
		
		advSearch.verifyFailedRecordCheckboxFunctionality();
	}

	@Given("^: Click on Review Pending checkbox$")
	public void click_on_Review_Pending_checkbox() throws Throwable {
		
		advSearch.clickReviewPendingRecordCheckbox();
	}

	@Then("^: Verify that record status Review Pending Checkbox is clickable or not$")
	public void verify_that_record_status_Review_Pending_Checkbox_is_clickable_or_not() throws Throwable {
		
		advSearch.verifyReviewPendingRecordCheckboxFunctionality();
	}

	@Given("^: Click on Void checkbox$")
	public void click_on_Void_checkbox() throws Throwable {
		
		advSearch.clickVoidRecordCheckbox();
	}

	@Then("^: Verify that record status Void Checkbox is clickable or not$")
	public void verify_that_record_status_Void_Checkbox_is_clickable_or_not() throws Throwable {
		
		advSearch.verifyVoidRecordCheckboxFunctionality();
	}

	@Given("^: Click on Draft checkbox$")
	public void click_on_Draft_checkbox() throws Throwable {
		
		advSearch.clickDraftRecordCheckbox();
	}

	@Then("^: Verify that record status Draft Checkbox is clickable or not$")
	public void verify_that_record_status_Draft_Checkbox_is_clickable_or_not() throws Throwable {
		
		advSearch.verifyDraftRecordCheckboxFunctionality();
	}

	@Given("^: Click on Rejected checkbox$")
	public void click_on_Rejected_checkbox() throws Throwable {
		
		advSearch.clickRejectedRecordCheckbox();
	}

	@Then("^: Verify that record status Rejected Checkbox is clickable or not$")
	public void verify_that_record_status_Rejected_Checkbox_is_clickable_or_not() throws Throwable {
		
		advSearch.verifyRejectedRecordCheckboxFunctionality();
	}

	@Then("^: Verify that system should display Remove Icon for Record Status Filter$")
	public void verify_that_system_should_display_Remove_Icon_for_Record_Status_Filter() throws Throwable {
		
		advSearch.verifyRecordStatusRemoveIconVisible();
	}

	@Then("^: Verify that Color of Remove Icon for Record Status filter should be red$")
	public void verify_that_Color_of_Remove_Icon_for_Record_Status_filter_should_be_red() throws Throwable {
		
		advSearch.verifyRecordStatusRemoveIconColor();
	}

	@Given("^: Click on Remove icon of Record Status$")
	public void click_on_Remove_icon_of_Record_Status() throws Throwable {
		
		advSearch.clickRecordStatusRemoveIcon();
	}

	@Then("^: Verify that system should Remove the Record Status section when a user click on Remove icon$")
	public void verify_that_system_should_Remove_the_Record_Status_section_when_a_user_click_on_Remove_icon() throws Throwable {
		
		advSearch.verifyRecordStatusRemoveIconFunctionality();
	}

	@Given("^: Click on View link of App Record$")
	public void click_on_View_link_of_App_Record() throws Throwable {
		
		advSearch.clickViewLinkOfApp();
	}

	@Then("^: Verify that system should redirect to the Entry of the Selected App Record when a user click on View link$")
	public void verify_that_system_should_redirect_to_the_Entry_of_the_Selected_App_Record_when_a_user_click_on_View_link() throws Throwable {
		
		advSearch.verifyViewLinkOfApp(excelData(8, 2));
	}

	@Then("^: Verify that total number of record should be matched with Pagination$")
	public void verify_that_total_number_of_record_should_be_matched_with_Pagination() throws Throwable {
		
		advSearch.verifyAdvancedSearchPagination();
	}
	
	//santosh
	
	@Then("^: Verify that Records You Created Today should be clickable$")
	public void verify_that_Records_You_Created_Today_should_be_clickable() throws Throwable {
	    advSearch.verifyQuickAccessLinkClickable();
	}

	@Then("^: Verify that Records Pending Your Review should be clickable$")
	public void verify_that_Records_Pending_Your_Review_should_be_clickable() throws Throwable {
	    advSearch.verifyQuickAccessLinkClickable();
	}

	@Then("^: Verify that Records You Accessed Today should be clickable$")
	public void verify_that_Records_You_Accessed_Today_should_be_clickable() throws Throwable {
	    advSearch.verifyQuickAccessLinkClickable();
	}

	@Then("^: Verify that Records Assigned To You text should be is clickable$")
	public void verify_that_Records_Assigned_To_You_text_should_be_is_clickable() throws Throwable {
		advSearch.verifyQuickAccessLinkClickable();   
	}

	@Given("^: Click on Records You Created Today link$")
	public void click_on_Records_You_Created_Today_link() throws Throwable {
		advSearch.clickRecordsYouCreatedToday();
	}

	@Given("^: Click on Records Pending Your Review link$")
	public void click_on_Records_Pending_Your_Review_link() throws Throwable {
		advSearch.clickRecordsPendingYourReview();
	}
	
	@Given("^: Click on Records You Accessed Today link$")
	public void click_on_Records_You_Accessed_Today_link() throws Throwable {
		advSearch.clickRecordsYouAccessedToday();
	}

	@Given("^: Click on Records Assigned To You link$")
	public void click_on_Records_Assigned_To_You_link() throws Throwable {
		advSearch.clickRecordsAssignedToYou();
	}

	@Then("^: Verify that all the link's under Quick Access is in proper order$")
	public void verify_that_all_the_link_under_Quick_Access_is_in_proper_order() throws Throwable {
		advSearch.verifyOrderOfQuickAccess();
		
	}

	@Then("^: Verify that when click on Records You Created Today then all today's records should display$")
	public void verify_that_when_click_on_Records_You_Created_Today_then_all_today_s_records_should_display() throws Throwable {
		advSearch.verifyTodaysAllRecordsOfGrid();
	}

	@Given("^: Click on View link of Created App$")
	public void click_on_View_link_of_Created_App() throws Throwable {
		advSearch.clickOnViewLinkOfCreatedApp();
	}

	@Given("^: Click on Back button of App which is Viewed$")
	public void click_on_Back_button_of_App_which_is_Viewed() throws Throwable {
		advSearch.clickOnBackBtnOfRecord();
	}

	@Given("^: Create app and enter data accordingly$")
	public void create_app_and_enter_data_accordingly() throws Throwable {
		
		new CommonFunctions().click_on_Administration_Tile();		
		appBuild.click_on_App_Builder_Tile();
		appBuild.click_on_Add_New_button_of_App_Builder();
		appBuild.add_All_Required_App_Details_in_Basic_Tab();
		appBuild.select_Checkbox_of_Peer_or_Second_Review();
		appBuild.select_Checkbox_of_Allow_the_Record_Creator_to_Approve_app();
		appBuild.select_Role_from_the_Role_dropdown();
		appBuild.select_Add_button_All_User_should_be_display_as_selected_by_default();
		appBuild.click_on_Continue_button();
		appBuild.add_All_Required_Attribute_Details_in_Attribute_tab();
		appBuild.select_Mark_Set_Value_Inactive_when_App_Submission_status_is_failed_checkbox();
		appBuild.click_on_Continue_button_of_attribute_tab();
		appBuild.add_all_Required_Workflow_Details_in_Workflow_tab();
//		appBuild.add_filter_in_Entity_of_Workflow();
		appBuild.click_on_Continue_button_of_workflow_tab();
		appBuild.add_all_required_App_Acceptance_Criteria_in_Acceptance_tab();
		appBuild.click_on_Continue_button_of_acceptance_tab();
		appBuild.click_on_Publish_Continue_button();
		appBuild.click_on_Save_button_of_prmission_tab();
		appBuild.verify_that_user_is_allowed_to_Add_New_app();
		new CommonFunctions().go_Back_to_Home();
		apps.click_on_Apps_Tile();
		apps.click_on_Search_App_of_Apps_Module();
		apps.enter_valid_app_name_of_App_Module_search_field();
		apps.click_on_Other_Apps_tile();
		apps.click_on_Searched_app();
		appBuild.enter_data_in_all_Mandatory_fields();
		apps.click_on_save_button_of_Apps_screen();
		new CommonFunctions().go_Back_to_Home();	
	}
	@Then("^: Verify that Records You Created Today option should display records according to permission$")
	public void verify_that_Records_You_Created_Today_option_should_display_records_according_to_permission() throws Throwable {
		advSearch.verifyAppWithoutPermission();
	}
	@Then("^: Verify that when click on Records You Accessed Today then all today's records should display$")
	public void verify_that_when_click_on_Records_You_Accessed_Today_then_all_today_s_records_should_display() throws Throwable {
		advSearch.verifyTodaysAllRecordsOfGrid();
	}
	
	@Then("^: Verify that Records You Accessed Today option should display records according to permission$")
	public void verify_that_Records_You_Accessed_Today_option_should_display_records_according_to_permission() throws Throwable {
		advSearch.verifyAppWithoutPermission();
	}
	
	@Given("^: Create app and enter data accordingly with Assignee$")
	public void create_app_and_enter_data_accordingly_with_Assignee() throws Throwable {
		new CommonFunctions().click_on_Administration_Tile();		
		appBuild.click_on_App_Builder_Tile();
		appBuild.click_on_Add_New_button_of_App_Builder();
		appBuild.add_All_Required_App_Details_in_Basic_Tab();
		appBuild.select_Checkbox_of_Peer_or_Second_Review();
		appBuild.select_Checkbox_of_Allow_the_Record_Creator_to_Approve_app();
		appBuild.select_Role_from_the_Role_dropdown();
		appBuild.select_Add_button_All_User_should_be_display_as_selected_by_default();
		appBuild.click_on_Continue_button();
		appBuild.add_All_Required_Attribute_Details_in_Attribute_tab();
		appBuild.select_Mark_Set_Value_Inactive_when_App_Submission_status_is_failed_checkbox();
		appBuild.click_on_Continue_button_of_attribute_tab();
		appBuild.add_all_Required_Workflow_Details_in_Workflow_tab();
//		appBuild.add_filter_in_Entity_of_Workflow();
		appBuild.click_on_Continue_button_of_workflow_tab();
		appBuild.add_all_required_App_Acceptance_Criteria_in_Acceptance_tab();
		appBuild.click_on_Continue_button_of_acceptance_tab();
		appBuild.click_on_Publish_Continue_button();
		appBuild.click_on_Save_button_of_prmission_tab();
		appBuild.verify_that_user_is_allowed_to_Add_New_app();
		new CommonFunctions().go_Back_to_Home();
		apps.click_on_Apps_Tile();
		apps.click_on_Search_App_of_Apps_Module();
		apps.enter_valid_app_name_of_App_Module_search_field();
		apps.click_on_Other_Apps_tile();
		apps.click_on_Searched_app();
		apps.enter_current_user_as_Assignee();
		appBuild.enter_data_in_all_Mandatory_fields();
		apps.click_on_save_button_of_Apps_screen();
		new CommonFunctions().go_Back_to_Home();
	}

	@Then("^: Verify that when click on Records You Assigned Today then all today's records should display$")
	public void verify_that_when_click_on_Records_You_Assigned_Today_then_all_today_s_records_should_display() throws Throwable {
		advSearch.verifyAssigneeRecordsOfGrid();
	}
	@Given("^: Get Current user details\\(firstName, lastName\\)$")
	public void get_Current_user_details_firstName_lastName_and_department() throws Throwable {
		new UsersPageObject(ObjectRepo.driver).getCurrnUserDetails();
	}
	
	@Then("^: Verify that Records You Assigned Today option should display records according to permission$")
	public void verify_that_Records_You_Assigned_Today_option_should_display_records_according_to_permission() throws Throwable {
		advSearch.verifyAppWithoutPermission();
	}
	
	@Then("^: Verify that when click on Records Pending Your Review then all today's records should display$")
	public void verify_that_when_click_on_Records_Pending_Your_Review_then_all_today_s_records_should_display() throws Throwable {
		advSearch.verifyPendingRecordsOfGrid();
	}
	@Then("^: Verify that Records Pending Your Review option should display records according to permission$")
	public void verify_that_Records_Pending_Your_Review_option_should_display_records_according_to_permission() throws Throwable {
		advSearch.verifyAppWithoutPermission();
	}
	
	@Given("^: Click on My Favorites tab$")
	public void click_on_My_Favorites_tab() throws Throwable {
		advSearch.clickMyFavTab();
	}

	@Then("^: Verify that My Favorites tab should display three columns as Search Name, Last Updated, and Action$")
	public void verify_that_My_Favorites_tab_should_display_three_columns_as_Search_Name_Last_Updated_and_Action() throws Throwable {
		advSearch.verify_SearchName_LastUpdated_Action();
	}
	
	@Given("^: Save app in My Favorites if not existing Already$")
	public void save_app_in_My_Favorites_if_not_existing_Already() throws Throwable {
		advSearch.addAppMyFav();
	}
	
	@Then("^: Verify that Search Name column's all created Favorites should clickable$")
	public void verify_that_Search_Name_column_s_all_created_Favorites_should_clickable() throws Throwable {
		advSearch.findAppInAdvSeachGrid(excelData(8, 2));
		
	}
	
	@Given("^: Remove all saved My Favorites apps$")
	public void remove_all_saved_My_Favorites_apps() throws Throwable {
		advSearch.removeAllFromAppMyFav();
	}

	@Then("^: Verify that Last Updated column should display last modified date and time$")
	public void verify_that_Last_Updated_column_should_display_last_modified_date_and_time() throws Throwable {
		advSearch.verifySavedMyFavLastUpdatedTime();
	}

}
