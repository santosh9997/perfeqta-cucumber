package com.cucumber.framework.stepdefinition;


import java.awt.RenderingHints.Key;


import org.openqa.selenium.Keys;

import com.cucumber.framework.helper.PageObject.MasterAppSettingPageObject;
import com.cucumber.framework.helper.PageObject.QuestionsPageObject;

import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class MasterAppSetting
{
	private MasterAppSettingPageObject masterappsettingPageObj;
	public ExcelUtils excel;

	public MasterAppSetting()
	{
		masterappsettingPageObj = new MasterAppSettingPageObject(ObjectRepo.driver);
	}
	public String excelData(int rowVal, int colVal) throws Exception {
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Questions");
		System.err.println();
		return excel.readXLSFile("Master App Settings", rowVal, colVal);
	}

	@Given("^: Click on Master App Settings Tile$")
	public void click_on_Master_App_Settings_Tile() throws Throwable {
		masterappsettingPageObj.clickOnMasterAppSettingsTile();
	}

	@Then("^: Verify the module name as Master App Settings$")
	public void verify_the_module_name_as_Master_App_Settings() throws Throwable {
		masterappsettingPageObj.verifyMasterAppSettingsModuleName();
	}

	@Given("^: Click on the first record of Master App listing grid$")
	public void click_on_the_first_record_of_Master_App_listing_grid() throws Throwable {
		//String firstMasterAppName = excelData(1, 1);
		masterappsettingPageObj.clickToFirstMasterApp();
	}

	@Then("^: Verify the Breadcrumb of Edit Master App Settings as \"([^\"]*)\"$")
	public void verify_the_Breadcrumb_of_Edit_Master_App_Settings_as(String arg1) throws Throwable {
		masterappsettingPageObj.verifyEditMasterAppBreadCrumbs();
	}
	@Given("^: Click on Save button of Master Settings$")
	public void click_on_Save_button() throws Throwable {
		masterappsettingPageObj.clickOnSaveBtnMasterApp();
	}

	@Then("^: Verify the Save button of Master App Setting should be clickable$")
	public void verify_the_Save_button_of_Master_App_Setting_should_be_clickable() throws Throwable {
		masterappsettingPageObj.verifyMasterAppSaveBtn();
	}

	@Given("^: Click on Cancel button of Master Settings$")
	public void click_on_Cancel_button() throws Throwable {
		masterappsettingPageObj.clickOnCancelBtnMasterApp();
	}

	@Then("^: Verify the Cancel button should be clickable and redirect to Master App Setting listing grid cancel button$")
	public void verify_the_Cancel_button_should_be_clickable_and_redirect_to_Master_App_Setting_listing_grid_cancel_button() throws Throwable {
		masterappsettingPageObj.verifyMasterAppCancelBtn();
	}

	@Then("^: Verify that Color of Save button should be green$")
	public void verify_that_Color_of_Save_button_should_be_green() throws Throwable {
		masterappsettingPageObj.verifySaveBtnColor();
	}

	@Then("^: Verify that Color of Cancel button should be black$")
	public void verify_that_Color_of_Cancel_button_should_be_black() throws Throwable {
		masterappsettingPageObj.verifyCancelBtnColor();
	}

	@Then("^: Verify the search results of Master App Settings screen$")
	public void verify_the_search_results_of_Master_App_Settings_screen() throws Throwable {
		masterappsettingPageObj.verifysearch(excelData(2, 1));
	}

	@Given("^: Click on Sorting icon of the Master Apps column$")
	public void click_on_Sorting_icon_of_the_Master_Apps_column() throws Throwable {
		masterappsettingPageObj.verifyAscendingOrder();
	}

	@Then("^: Verify that all the records of Master Apps column display in ascending order when click on sorting icon$")
	public void verify_that_all_the_records_of_Master_Apps_column_display_in_ascending_order_when_click_on_sorting_icon() throws Throwable {
		String sortingColName = excelData(3, 1);
		masterappsettingPageObj.verifyAscendingOrderResult(sortingColName);
	}

	@Given("^: Click on \"([^\"]*)\" button of Master App listing Screen$")
	public void click_on_button_of_Master_App_listing_Screen(String arg1) throws Throwable {
		masterappsettingPageObj.clickOnMasterAppAddNewBtn();
	}

	@Given("^: Click on Master App Name textbox and press the \"([^\"]*)\" Key$")
	public void click_on_Master_App_Name_textbox_and_press_the_Key(String arg1) throws Throwable {
		Thread.sleep(1000);
		masterappsettingPageObj.masterAppTitle.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that system should display required validation message for Master App field as \"([^\"]*)\"$")
	public void verify_that_system_should_display_required_validation_message_for_Master_App_field_as(String arg1) throws Throwable {
		masterappsettingPageObj.verifyMasterAppNameReqVal();
	}

	@Given("^: Enter duplicate Master App Name$")
	public void enter_duplicate_Master_App_Name() throws Throwable {
		String masterappnameuniqueval = excelData(4, 1);
		masterappsettingPageObj.masterAppNamesendkeys(masterappnameuniqueval);
	}

	@Then("^: Verify that system should display unique validation message for Master App field as \"([^\"]*)\"$")
	public void verify_that_system_should_display_unique_validation_message_for_Master_App_field_as(String arg1) throws Throwable {
		masterappsettingPageObj.verifyMasterAppNameUniqueVal();
	}
	
	@Given("^: Enter Master App Name value less than (\\d+) chracters$")
	public void enter_Master_App_Name_value_less_than_chracters(int arg1) throws Throwable {
		String masterappnameminival = excelData(5, 1);
		masterappsettingPageObj.masterAppNamesendkeys(masterappnameminival);
	}

	@Then("^: Verify that system should display minimum validation message for Master App field as \"([^\"]*)\"$")
	public void verify_that_system_should_display_minimum_validation_message_for_Master_App_field_as(String arg1) throws Throwable {
		masterappsettingPageObj.verifyMasterAppNameMiniVal();
	}

	@Given("^: Enter Master App Name value more than (\\d+) characters$")
	public void enter_Master_App_Name_value_more_than_characters(int arg1) throws Throwable {
		String masterappnamemaxval = excelData(6, 1);
		masterappsettingPageObj.masterAppNamesendkeys(masterappnamemaxval);
	}

	@Then("^: Verify that system should display maximum validation message for Master App field as \"([^\"]*)\"$")
	public void verify_that_system_should_display_maximum_validation_message_for_Master_App_field_as(String arg1) throws Throwable {
	   masterappsettingPageObj.verifyMasterAppNameMaxVal();
	}
	
	@Given("^: Enter a valid Master App Name$")
	public void enter_a_valid_Master_App_Name() throws Throwable {
		String masterappnamenoval = excelData(7, 1);
		masterappsettingPageObj.masterAppNamesendkeys(masterappnamenoval);
	}

	@Then("^: Verify system should accept the Master App name value without any validation message\\.$")
	public void verify_system_should_accept_the_Master_App_name_value_without_any_validation_message() throws Throwable {
		masterappsettingPageObj.verifyMasterAppNameNoVal();
	}
	
	@Given("^: Add valid Master App data for all fields and click on the save button$")
	public void add_valid_Master_App_data_for_all_fields_and_click_on_the_save_button() throws Throwable {
		String masterappnamenoval = excelData(7, 1);
		masterappsettingPageObj.masterAppNamesendkeys(masterappnamenoval);
		masterappsettingPageObj.selectMasterModule(excelData(8, 2));
		masterappsettingPageObj.selectMasterApps();
		masterappsettingPageObj.clickOnselectMasterAppAddBtn();
		masterappsettingPageObj.clickOnSaveBtn();
	}

	@Then("^: Verify that user is able to Add New master app$")
	public void verify_that_user_is_able_to_Add_New_master_app() throws Throwable {
	    
	}
}
