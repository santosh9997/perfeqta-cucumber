package com.cucumber.framework.stepdefinition;

import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.GeneralSettingsPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class GeneralSettings {

	public ExcelUtils excel;
	private Login login;
	private GeneralSettingsPageObject genPage;

	public GeneralSettings() {
		 new LoginPageObject(ObjectRepo.driver);
		 genPage = new GeneralSettingsPageObject(ObjectRepo.driver);
	}
	 
	
	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "General Settings");
		System.err.println();
		return excel.readXLSFile("General Settings", rowVal, colVal);
	}

@Given("^: Click on General Settings Tile$")
public void click_on_General_Settings_Tile() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
	genPage.clickGeneralSetting();
    
}

@Then("^: Verify the module name as General Settings$")
public void verify_the_module_name_as_General_Settings() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyGeneralSetting();
}

@Given("^: Click on General Settings$")
public void click_on_General_Settings() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    
}

@Given("^: Click on breadcrumb of General Settings screen$")
public void click_on_breadcrumb_of_General_Settings_screen() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
	genPage.clickBreadcrumb();
}

@Then("^: Verify that the page should be redirected to previous page$")
public void verify_that_the_page_should_be_redirected_to_previous_page() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyRedirectPreviousPage();
}

@Given("^: Click on Standard Report Title textbox$")
public void click_on_Standard_Report_Title_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.clickStandardReport();
}

@Given("^: Remove Previous data from the textbox$")
public void remove_Previous_data_from_the_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.removePreviousData();
}

@Then("^: Verify that mandatory validation message for Report Tile textbox should be displayed as \"([^\"]*)\"$")
public void verify_that_mandatory_validation_message_for_Report_Tile_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyMandatoryValidationMessage();
}

@Given("^: Enter less than (\\d+) characters in Standard Report Title textbox$")
public void enter_less_than_characters(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.lessThanCharactersMsg(excelData(2,1));
}

@Then("^: Verify that Minimum validation message for Report Tile textbox should be displayed as \"([^\"]*)\"$")
public void verify_that_Minimum_validation_message_for_Report_Tile_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyMinimumValidationMsg();
}

@Given("^: Enter more than (\\d+) characters in Standard Report Title textbox$")
public void enter_more_than_characters(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.moreThanCharacters(excelData(3,1));
}
@Given("^: Enter less than (\\d+) characters for textarea$")
public void enter_less_than_characters_for_textarea(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.enterLessforTextarea(excelData(2, 3));
}

@Then("^: Verify that Maximum validation message for Report Tile textbox should be displayed as \"([^\"]*)\"$")
public void verify_that_Maximum_validation_message_for_Report_Tile_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
	 genPage.verifyMaximumValidationMsg();
}

@Given("^: Click on Standard Report Tile textbox$")
public void click_on_Standard_Report_Tile_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    
}

@Given("^: Enter valid data$")
public void enter_valid_data() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.enterValidData(excelData(5,1));
}

@Then("^: Verify that system should allow valid data and no validation should be displayed for Standard Report Tile textbox$")
public void verify_that_system_should_allow_valid_data_and_no_validation_should_be_displayed_for_Standard_Report_Tile_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyValidDataAllowed();
}

@Given("^: Click on Standard Report Sub Tile textbox$")
public void click_on_Standard_Report_Sub_Tile_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.clickStandardReportSubtitle();
}

@Given("^: Remove Previous data from the subtitle textbox$")
public void remove_Previous_data_from_the_subtitle_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.removePreviousSubtitletext();
}

@Given("^: Enter less than (\\d+) characters for subtitle textbox$")
public void enter_less_than_characters_for_subtitle_textbox(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
   genPage.enterlessCharactersforSubtitlebox(excelData(2, 2));
}

@Given("^: Enter more than (\\d+) characters for subtitle textbox$")
public void enter_more_than_characters_for_subtitle_textbox(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.enterMorethanCharactersSubtitle(excelData(3, 2));
}


@Given("^: Enter valid data in subtitle textbox$")
public void enter_valid_data_in_subtitle_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.validDataallowedSubtitle(excelData(5, 2));
}

@Then("^: Verify that mandatory validation message for Standard Report Sub Tile textbox should be displayed as \"([^\"]*)\"$")
public void verify_that_mandatory_validation_message_for_Standard_Report_Sub_Tile_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifySubTitleRequiredMsg();
}

@Then("^: Verify that Minimum validation message for Standard Report Sub Tile textbox should be displayed as \"([^\"]*)\"$")
public void verify_that_Minimum_validation_message_for_Standard_Report_Sub_Tile_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
	genPage.verifyMinlengthValidationMsg();
}

@Then("^: Verify that Maximum validation message for Standard Report Sub Tile textbox should be displayed as \"([^\"]*)\"$")
public void verify_that_Maximum_validation_message_for_Standard_Report_Sub_Tile_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
	genPage.verifyMaxlengthValidationMsgSubtitle();
}

@Then("^: Verify that system should allow valid data and no validation should be displayed for Standard Report Sub Tile textbox$")
public void verify_that_system_should_allow_valid_data_and_no_validation_should_be_displayed_for_Standard_Report_Sub_Tile_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyvalidDataSubtitle();
}

@Given("^: Click on Support Contact Details textarea$")
public void click_on_Support_Contact_Details_textarea() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.clickContactdetail();
}

@Given("^: Remove Previous data from the textarea$")
public void remove_Previous_data_from_the_textarea() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.removePreviousFromTextarea();
}
@Given("^: Enter more than (\\d+) characters in textarea$")
public void enter_more_than_characters_in_textarea(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.enterMorethanCharactersinTextarea(excelData(3, 3));
}

@Then("^: Verify that Minimum validation message for Support Contact Details textarea should be displayed as \"([^\"]*)\"$")
public void verify_that_Minimum_validation_message_for_Support_Contact_Details_textarea_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    
}

@Then("^: Verify that Maximum validation message for Support Contact Details textarea should be displayed as \"([^\"]*)\"$")
public void verify_that_Maximum_validation_message_for_Support_Contact_Details_textarea_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyMaxValidationTextArea();
}

@Then("^: Verify that system should allow valid data and no validation should be displayed for Support Contact Details textarea$")
public void verify_that_system_should_allow_valid_data_and_no_validation_should_be_displayed_for_Support_Contact_Details_textarea() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyValidDataForTextarea();
}

@Given("^: Click on Password Aging Limit textbox$")
public void click_on_Password_Aging_Limit_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.clickPasswordAgingLimit();
}

@Given("^: Enter valid data for textarea$")
public void enter_valid_data_for_textarea() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.enterValidforTextArea(excelData(5, 3));
}

@Given("^: Enter value as (\\d+) in textbox$")
public void enter_value_as_in_textbox(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.enterZeroinTextbox(excelData(6, 4));
}

@Given("^: Remove Previous data from password Aging$")
public void remove_Previous_data_from_password_Aging() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.removePreviousDatapasswordAging();
}

@Then("^: Verify that Minimum validation message for Password Aging Limit textbox should be displayed as \"([^\"]*)\"$")
public void verify_that_Minimum_validation_message_for_Password_Aging_Limit_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyMinValidationforPasswordAging();
}

@Given("^: Enter more than (\\d+) numeric value$")
public void enter_more_than_numeric_value(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.entermorethan999Numeric(excelData(7, 4));
}

@Then("^: Verify that Maximum validation message for Password Aging Limit textbox should be displayed as \"([^\"]*)\"$")
public void verify_that_Maximum_validation_message_for_Password_Aging_Limit_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyMaxValidationPasswordAging();
}

@Then("^: Verify that system should allow valid data and no validation should be displayed for Password Aging Limit textbox$")
public void verify_that_system_should_allow_valid_data_and_no_validation_should_be_displayed_for_Password_Aging_Limit_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifypasswordAgingBoxallowValidData();
}

@Given("^: Click on Password Aging Limit Alert Message textbox$")
public void click_on_Password_Aging_Limit_Alert_Message_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.clickPasswordAgingLimitAlertMsg();
}

@Given("^: Remove Previous data from password Aging msg$")
public void remove_Previous_data_from_password_Aging_msg() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.removepreviousPasswordAgingMsg();
}

@Given("^: Enter less than (\\d+) characters password Aging msg$")
public void enter_less_than_characters_password_Aging_msg(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.enterlessCharactersPasswordAgingMsg(excelData(9, 5));
}


@Given("^: Enter less than (\\d+) characters for password Aging$")
public void enter_less_than_characters_for_password_Aging(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.enterlessPasswordAging(excelData(1, 2));
}

@Given("^: Enter valid data in password Aging$")
public void enter_valid_data_in_password_Aging() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.enterValidinPasswordAging(excelData(8, 4));
}

@Then("^: Verify that Minimum validation message for Password Aging Limit Alert Message textbox should be displayed  as \"([^\"]*)\"$")
public void verify_that_Minimum_validation_message_for_Password_Aging_Limit_Alert_Message_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyMinValidationPasswordAgingMsg();
}

@Given("^: Enter more than (\\d+) characters password Aging msg$")
public void enter_more_than_characters_password_Aging_msg(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.enterMoreCharPasswordAgingMsg(excelData(10, 5));
}

@Given("^: Enter valid data in password Aging msg box$")
public void enter_valid_data_in_password_Aging_msg_box() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.enterValidDataPasswordAgingMsgBox(excelData(11, 5));
}

@Then("^: Verify that Maximum validation message for Password Aging Limit Alert Message textbox should be displayed  as  \"([^\"]*)\"$")
public void verify_that_Maximum_validation_message_for_Password_Aging_Limit_Alert_Message_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyMaxValidationPasswordAgingMsgalert();
}

@Then("^: Verify that system should allow valid data and no validation should be displayed for Password Aging Limit Alert Message textbox$")
public void verify_that_system_should_allow_valid_data_and_no_validation_should_be_displayed_for_Password_Aging_Limit_Alert_Message_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
	genPage.verifyValidDataAlertMsg();
    
}

//@And("^: Click on General Settings Tile$")
//public void click_on_General_Settings_Tile() throws Throwable {
//    // Write code here that turns the phrase above into concrete actions
//    
//}
//
//@And("^: Click on Number of Previous Used Passwords to Disallow textbox$")
//public void click_on_Number_of_Previous_Used_Passwords_to_Disallow_textbox() throws Throwable {
//    // Write code here that turns the phrase above into concrete actions
//    
//}
//
//@And("^: Enter (\\d+) in textbox$")
//public void enter_in_textbox(int arg1) throws Throwable {
//    // Write code here that turns the phrase above into concrete actions
//    
//}

@Then("^: Verify Minimum validation message for Number of Previous Used Passwords to Disallow textbox should be displayed as \"([^\"]*)\"$")
public void verify_Minimum_validation_message_for_Number_of_Previous_Used_Passwords_to_Disallow_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyMinValidationMsgPreviousAllowedBox();
}

@Given("^: Click on Number of Previous Used Passwords to Disallow textbox$")
public void click_on_Number_of_Previous_Used_Passwords_to_Disallow_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.clickOnNumberofPreviousUsedPassword();
}

@Given("^: Enter less than (\\d+) number$")
public void enter_less_than_number(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    
}

@Given("^: Enter less than (\\d+) number in Number of Previous Used Passwords to Disallow textbox$")
public void enter_less_than_number_in_Number_of_Previous_Used_Passwords_to_Disallow_textbox(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.enterLess4inPreviousUsedTextBox(excelData(13, 6));
}
@Given("^: Remove Number of Previous Used Passwords to Disallow textbox$")
public void remove_Number_of_Previous_Used_Passwords_to_Disallow_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.removePreviousUseddatainTextbox();
}
@Given("^: Enter (\\d+) in textbox in Previous Used Passwords field$")
public void enter_in_textbox_in_Previous_Used_Passwords_field(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
   genPage.enterZeroinPreviousPasswordTextbox(excelData(12, 6));
}

@Then("^: Verify Minimum Regulation validation message for Number of Previous Used Passwords to Disallow should be displayed as \"([^\"]*)\"$")
public void verify_Minimum_Regulation_validation_message_for_Number_of_Previous_Used_Passwords_to_Disallow_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyValidationMsglessThan4PreviousPasswordUsed();
}

@Given("^: Enter more than (\\d+) in Number of Previous Used Passwords to Disallow textbox$")
public void enter_more_than_in_Number_of_Previous_Used_Passwords_to_Disallow_textbox(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.enterMorethan99inNumberofPreviousUsedPasswords(excelData(14, 6));
}

@Given("^: Enter more than (\\d+)$")
public void enter_more_than(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    
}


@Given("^: Enter valid data Previous Used Passwords to Disallow textbox$")
public void enter_valid_data_Previous_Used_Passwords_to_Disallow_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.entervaliddatainPreviousUsed(excelData(15, 6));
}

@Then("^: Verify Maximum validation message for Number of Previous Used Passwords to Disallow should be displayed as \"([^\"]*)\"$")
public void verify_Maximum_validation_message_for_Number_of_Previous_Used_Passwords_to_Disallow_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyMorethan99inNumberofPreviousUsed();
}

@Then("^: Verify that system should allow valid data and no validation should be displayed for Number of Previous Used Passwords to Disallow textbox$")
public void verify_that_system_should_allow_valid_data_and_no_validation_should_be_displayed_for_Number_of_Previous_Used_Passwords_to_Disallow_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyValidDatainPreviousUsedTextBox();
}

@Given("^: Click on Number of Previous Used Passwords to Disallow Alert Message textbox$")
public void click_on_Number_of_Previous_Used_Passwords_to_Disallow_Alert_Message_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.clickOnNumberofPreviousUsedPasswordMsgBox();
}


@Given("^: Remove previous data of  Number of Previous Used Passwords to Disallow Alert Message textbox$")
public void remove_previous_data_of_Number_of_Previous_Used_Passwords_to_Disallow_Alert_Message_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
   genPage.removePreviousDataofPreviousUsedPasswordAlertMsgBox();
}

@Given("^: Enter less than (\\d+) characters in Previous Used Passwords to Disallow Alert Message textbox$")
public void enter_less_than_characters_in_Previous_Used_Passwords_to_Disallow_Alert_Message_textbox(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.enterLessthan2CharinAmlertMsgBox(excelData(16, 7));
}

@Given("^: Enter more than (\\d+) characters in Previous Used Passwords to Disallow Alert Message textbox$")
public void enter_more_than_characters_in_Previous_Used_Passwords_to_Disallow_Alert_Message_textbox(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.enterMorethan200ValidationMsg(excelData(17, 7));
}

@Given("^: Enter valid data in Previous Used Passwords to Disallow Alert Message textbox$")
public void enter_valid_data_in_Previous_Used_Passwords_to_Disallow_Alert_Message_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.enterValidDatainPreviouslyUsedAlertMsgBox(excelData(18, 7));
}

@Then("^: Verify Minimum validation message for Number of Previous Used Passwords to Disallow Alert Message should be displayed as \"([^\"]*)\"$")
public void verify_Minimum_validation_message_for_Number_of_Previous_Used_Passwords_to_Disallow_Alert_Message_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyMinimumValidationMsgforAlertBox();
}

@Then("^: Verify Maximum validation message for Number of Previous Used Passwords to Disallow Alert Message should be displayed as \"([^\"]*)\"$")
public void verify_Maximum_validation_message_for_Number_of_Previous_Used_Passwords_to_Disallow_Alert_Message_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyMaxValidationMsgforAlertMsgBox();
}

@Then("^: Verify that system should allow valid data and no validation mssage should be displayed for Previous Used Passwords to Disallow Alert Message textbox$")
public void verify_that_system_should_allow_valid_data_and_no_validation_mssage_should_be_displayed_for_Previous_Used_Passwords_to_Disallow_Alert_Message_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyValidDatainPreviousUsedAlertMsgBox();
}

@Given("^: Remove previous data Maximum Number of Failed Login Attempts textbox$")
public void remove_previous_data_Maximum_Number_of_Failed_Login_Attempts_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.removePreviousDataofFailedLoginAttemps();
}

@Given("^: Enter (\\d+) in textbox Maximum Number of Failed Login Attempts textbox$")
public void enter_in_textbox_Maximum_Number_of_Failed_Login_Attempts_textbox(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.enterZeroinFailedLoginAttemptsTextbox(excelData(19, 8));
}

@Given("^: Enter more than (\\d+) numeric value Maximum Number of Failed Login Attempts textbox$")
public void enter_more_than_numeric_value_Maximum_Number_of_Failed_Login_Attempts_textbox(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.enterMorethan999FailedLoginAttemptsTextbox(excelData(20, 8));
}

@Given("^: Enter valid data Maximum Number of Failed Login Attempts textbox$")
public void enter_valid_data_Maximum_Number_of_Failed_Login_Attempts_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.enterValidDataforFailedLoginAttempts(excelData(21, 8));
}

@Given("^: Click on Maximum Number of Failed Login Attempts textbox$")
public void click_on_Maximum_Number_of_Failed_Login_Attempts_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.clickFailedLoginAttemptsTextbox();
}

@Given("^: Enter (\\d+) in textbox$")
public void enter_in_textbox(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions

}

@Then("^: Verify Minimum validation message for Maximum Number of Failed Login Attempts textbox should be displayed as \"([^\"]*)\"$")
public void verify_Minimum_validation_message_for_Maximum_Number_of_Failed_Login_Attempts_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyMinforFailedLoginAttemptsTextbox();
}

@Then("^: Verify Maximum validation message for Maximum Number of Failed Login Attempts textbox should be displayed as \"([^\"]*)\"$")
public void verify_Maximum_validation_message_for_Maximum_Number_of_Failed_Login_Attempts_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
	genPage.verifyMaxforFailedLoginAttemptsTextbox();
}

@Then("^: Verify that system should allow valid data and no validation should be displayed for Maximum Number of Failed Login Attempts textbox$")
public void verify_that_system_should_allow_valid_data_and_no_validation_should_be_displayed_for_Maximum_Number_of_Failed_Login_Attempts_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyValidDataforFailedLoginAttemptsTextbox();
}

@Given("^: Click on Maximum Number of Failed Login Attempts Alert Message textbox$")
public void click_on_Maximum_Number_of_Failed_Login_Attempts_Alert_Message_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.clickFailedLoginAttemptsMsgTextbox();
}

@Given("^: Remove data of Failed Login Attempts Alert Message textbox$")
public void remove_data_of_Failed_Login_Attempts_Alert_Message_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.removeDatafromAlertMsgBox();
}

@Given("^: Enter less than (\\d+) characters in Failed Login Attempts Alert Message textbox$")
public void enter_less_than_characters_in_Failed_Login_Attempts_Alert_Message_textbox(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.enterLessthan2CharinFailedAlertMsgBox(excelData(22, 9));
}

@Then("^: Verify Minimum validation message for Maximum Number of Failed Login Attempts Alert Message textbox should be displayed as \"([^\"]*)\"$")
public void verify_Minimum_validation_message_for_Maximum_Number_of_Failed_Login_Attempts_Alert_Message_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyMinCharValidationMsgFailedLoginAlertBox();
}


@Given("^: Enter more than (\\d+) characters in Failed Login Attempts Alert Message textbox$")
public void enter_more_than_characters_in_Failed_Login_Attempts_Alert_Message_textbox(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.enterMorethan200CharinMsgAlertBox(excelData(23, 9));
}

@Then("^: Verify Maximum validation message for Maximum Number of Failed Login Attempts Alert Message textbox should be displayed as \"([^\"]*)\"$")
public void verify_Maximum_validation_message_for_Maximum_Number_of_Failed_Login_Attempts_Alert_Message_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyMaxCharFailedAlertMsg();
}


@Given("^: Enter valid data in Failed Login Attempts Alert Message textbox$")
public void enter_valid_data_in_Failed_Login_Attempts_Alert_Message_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.enterValidDatainFailedAlertMsgbox(excelData(24, 9));
}
@Then("^: Verify that system should allow valid data and no validation should be displayed for Maximum Number of Failed Login Attempts Alert Message textbox$")
public void verify_that_system_should_allow_valid_data_and_no_validation_should_be_displayed_for_Maximum_Number_of_Failed_Login_Attempts_Alert_Message_textbox() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyValidCharFailedAlertMsg();
}

@Given("^: Click on textbox of Day to auto-close monthly windows$")
public void click_on_textbox_of_Day_to_auto_close_monthly_windows() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.clickmonthlywindowsbox();
}

@Given("^: Remove data from textbox of Day to auto-close monthly windows$")
public void remove_data_from_textbox_of_Day_to_auto_close_monthly_windows() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.removedataFrommontlyWindows();
}

@Given("^: Enter (\\d+) in textbox auto-close monthly windows$")
public void enter_in_textbox_auto_close_monthly_windows(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.enterZeroInMonthlyWindows(excelData(25, 10));
}

@Then("^: Verify Minimum validation message for Day to auto-close monthly windows textbox should be displayed as \"([^\"]*)\"$")
public void verify_Minimum_validation_message_for_Day_to_auto_close_monthly_windows_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
	genPage.verifyZeroinMonthlyWindows();
}


@Given("^: Enter more than (\\d+) number auto-close monthly windows$")
public void enter_more_than_number_auto_close_monthly_windows(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.enterMaxinMonthlyWindows(excelData(26, 10));
}
@Given("^: Enter valid data auto-close monthly windows$")
public void enter_valid_data_auto_close_monthly_windows() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
     genPage.enterValidDatainMonthlyWindow(excelData(27, 10));
}
@Given("^: Enter more than (\\d+) number$")
public void enter_more_than_number(int arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    
}

@Then("^: Verify Maximum validation message for Day to auto-close monthly windows textbox should be displayed as \"([^\"]*)\"$")
public void verify_Maximum_validation_message_for_Day_to_auto_close_monthly_windows_textbox_should_be_displayed_as(String arg1) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    
}

@Then("^: Verify that system should allow valid data and no validation should be displayed for Day to auto-close monthly windows$")
public void verify_that_system_should_allow_valid_data_and_no_validation_should_be_displayed_for_Day_to_auto_close_monthly_windows() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyValidDataMonthlyWindows();
}

@Given("^: Click on checkbox of Show Sequence number of Entity, Procedure and Questions$")
public void click_on_checkbox_of_Show_Sequence_number_of_Entity_Procedure_and_Questions() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.clickCheckboxofShowSequence();
}

@Then("^: Verify that checkbox of Show Sequence number of Entity, Procedure and Questions should be selectable$")
public void verify_that_checkbox_of_Show_Sequence_number_of_Entity_Procedure_and_Questions_should_be_selectable() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyChecboxofShowSequence();
}

@Given("^: Click on checkbox of Show Acceptance Criteria Icon in the App$")
public void click_on_checkbox_of_Show_Acceptance_Criteria_Icon_in_the_App() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.clickShowAcceptanceCheckbox();
}

@Then("^: Verify that checkbox of Show Acceptance Criteria Icon in the App$")
public void verify_that_checkbox_of_Show_Acceptance_Criteria_Icon_in_the_App() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyChecboxofAcceptanceCriteria();
}

@Given("^: Click on checkbox of Do not allow to print blank fields$")
public void click_on_checkbox_of_Do_not_allow_to_print_blank_fields() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.clickonDoNoTallowCheckbox();
}

@Then("^: Verify that checkbox of Do not allow to print blank fields$")
public void verify_that_checkbox_of_Do_not_allow_to_print_blank_fields() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.verifyCheckboxofDoNotAllow();
}

@Given("^: Click on checkbox of  Allow Sites to Print$")
public void click_on_checkbox_of_Allow_Sites_to_Print() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.clickAllowAllSiteCheckbox();
}

@Then("^: Verify that checkbox of Allow Sites to Print$")
public void verify_that_checkbox_of_Allow_Sites_to_Print() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
	genPage.verifyCheckboxofAllowAllSite();
}

@Given("^: Click on Save Button of General Setting$")
public void click_on_Save_Button_of_General_Setting() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    genPage.ClickonSaveButton();
}

@Then("^: Verify that System should allow to save the data on General Settings screen$")
public void verify_that_System_should_allow_to_save_the_data_on_General_Settings_screen() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
	genPage.verifySavebutton();
}
}
