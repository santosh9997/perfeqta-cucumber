package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import com.cucumber.framework.helper.DatePicker.Datepicker;
import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.helper.PageObject.EntitiesPageObject;
import com.cucumber.framework.helper.PageObject.SetsPageObject;
import com.cucumber.framework.helper.PageObject.SmartAlertsPageObject;
import com.cucumber.framework.helper.TextBox.TextBoxHelper;
import com.cucumber.framework.helper.Wait.WaitHelper;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class SmartAlerts {

	public ExcelUtils excel;
	public SmartAlertsPageObject smartAlrtObj;
	private WaitHelper waitObj;
	private TextBoxHelper textBoxHelper;

	public SmartAlerts() {
		smartAlrtObj = new SmartAlertsPageObject(ObjectRepo.driver);
		waitObj = new WaitHelper(ObjectRepo.driver, ObjectRepo.reader);
		textBoxHelper = new TextBoxHelper(ObjectRepo.driver);
	}

	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Smart Alerts");
		System.err.println();
		return excel.readXLSFile("Smart Alerts", rowVal, colVal);
	}

	@Given("^: Click on Smart Alerts Tile$")
	public void click_on_Smart_Alerts_Tile() throws Throwable {
		smartAlrtObj.smartAlertsTileClick();
	}

	@Then("^: Verify module name as Smart Alerts$")
	public void verify_module_name_as_Smart_Alerts() throws Throwable {
		smartAlrtObj.verifySmartAlertsLableName();
	}

	@Given("^: Click on Add New button of Smart Alert$")
	public void click_on_Add_New_button() throws Throwable {
		smartAlrtObj.addNewBtnClick();
	}

	@Then("^: Verify that system should redirect to the Add / Edit Smart Alert Screen when a user click on the Add New button$")
	public void verify_that_system_should_redirect_to_the_Add_Edit_Smart_Alert_Screen_when_a_user_click_on_the_Add_New_button()
			throws Throwable {
		smartAlrtObj.verifyAddEditSmartAlrtLabel();
	}

	@Then("^: Verify that a system should display the Current Version of the Smart Alert$")
	public void verify_that_a_system_should_display_the_Current_Version_of_the_Smart_Alert() throws Throwable {
		smartAlrtObj.verifyCurrVersion();
	}

	@Then("^: Verify that system should display required validation message as Smart Alert Name is required\\.$")
	public void verify_that_system_should_display_required_validation_message_as_Smart_Alert_Name_is_required()
			throws Throwable {
		smartAlrtObj.verifyAlertNameTxtBoxRquiMsg();
	}

	@Given("^: Click on Smart Alert Name textbox and Press \"([^\"]*)\" Key$")
	public void click_on_Smart_Alert_Name_textbox_and_Press_Key(String arg1) throws Throwable {
		smartAlrtObj.alertNameTxtBox.sendKeys(Keys.TAB);
	}

	@Given("^: Enter Existing Smart Alert Name into Smart Alert Name textbox$")
	public void enter_Existing_Smart_Alert_Name_into_Smart_Alert_Name_textbox() throws Throwable {
		smartAlrtObj.alertNameTxtBoxSendkeys(excelData(2, 1));
	}

	@Then("^: Verify that system should display unique validation message as Smart Alert Name must be unique\\.$")
	public void verify_that_system_should_display_unique_validation_message_as_Smart_Alert_Name_must_be_unique()
			throws Throwable {
		smartAlrtObj.verifyAlertNameTxtBoxUniqMsg();
	}

	@Given("^: Enter less than (\\d+) characters in Smart Alert Name textbox$")
	public void enter_less_than_characters_in_Smart_Alert_Name_textbox(int arg1) throws Throwable {
		smartAlrtObj.alertNameTxtBoxSendkeys(excelData(3, 1));
//		textBoxHelper.sendKeys(locator, value);
	}

	@Then("^: Verify that system should display minimum validation message as Smart Alert Name must be at least (\\d+) characters\\.$")
	public void verify_that_system_should_display_minimum_validation_message_as_Smart_Alert_Name_must_be_at_least_characters(
			int arg1) throws Throwable {
		smartAlrtObj.verifyAlertNameTxtBoxMinMsg();
	}

	@Given("^: Enter more than (\\d+) characters in Smart Alert Name textbox$")
	public void enter_more_than_characters_in_Smart_Alert_Name_textbox(int arg1) throws Throwable {
		smartAlrtObj.alertNameTxtBoxSendkeys(excelData(4, 1));
	}

	@Then("^: Verify that system should display maximum validation message as Smart Alert Name cannot be more than (\\d+) characters long\\.$")
	public void verify_that_system_should_display_maximum_validation_message_as_Smart_Alert_Name_cannot_be_more_than_characters_long(
			int arg1) throws Throwable {
		smartAlrtObj.verifyAlertNameTxtBoxMaxMsg();
	}

	////////////
	@Given("^: Enter less than (\\d+) characters in Smart Alert Description textbox$")
	public void enter_less_than_characters_in_Smart_Alert_Description_textbox(int arg1) throws Throwable {
		smartAlrtObj.alertDescpTxtBoxSendkeys(excelData(7, 2));
	}

	@Then("^: Verify that system should display Minimum validation message as Smart Alert Description must be at least (\\d+) characters\\.$")
	public void verify_that_system_should_display_Minimum_validation_message_as_Smart_Alert_Description_must_be_at_least_characters(
			int arg1) throws Throwable {
		smartAlrtObj.verifyAlertDescpTxtBoxMinMsg();
	}

	@Given("^: Enter more than (\\d+) characters in Smart Alert Description textbox$")
	public void enter_more_than_characters_in_Smart_Alert_Description_textbox(int arg1) throws Throwable {
		smartAlrtObj.alertDescpTxtBoxSendkeys(excelData(8, 2));
	}

	@Then("^: Verify that system should display Maximum validation message as Smart Alert Description cannot be more than (\\d+) characters\\.$")
	public void verify_that_system_should_display_Maximum_validation_message_as_Smart_Alert_Description_cannot_be_more_than_characters_long(
			int arg1) throws Throwable {
		smartAlrtObj.verifyAlertDescpTxtBoxMaxMsg();
	}

	@Given("^: Enter valid data in Smart Alert Description textbox$")
	public void enter_valid_data_in_Smart_Alert_Description_textbox() throws Throwable {
		smartAlrtObj.alertDescpTxtBoxSendkeys(excelData(9, 2));
	}

	@Then("^: Verify that system should not display any validation message when user do not enter value in Smart Alert Description Field$")
	public void verify_that_system_should_not_display_any_validation_message_when_user_do_not_enter_value_in_Smart_Alert_Description_Field()
			throws Throwable {
		smartAlrtObj.verifyAlertDescpTxtBoxNoValid();
	}

	@Given("^: Click on Message and Instruction to send in Email textarea Press \"([^\"]*)\" Key$")
	public void click_on_Message_and_Instruction_to_send_in_Email_textarea_Press_Key(String arg1) throws Throwable {
		smartAlrtObj.msgAndInstToEmailTxtBox.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that system should display required validation message as Message and Instructions to send in Email is required\\.$")
	public void verify_that_system_should_display_required_validation_message_as_Message_and_Instructions_to_send_in_Email_is_required()
			throws Throwable {
		smartAlrtObj.verifyMsgAndInstToEmailTxtBoxRquiMsg();
	}

	@Given("^: Enter less than (\\d+) characters in Message and Instruction to send in Email textarea$")
	public void enter_less_than_characters_in_Message_and_Instruction_to_send_in_Email_textarea(int arg1)
			throws Throwable {
		smartAlrtObj.msgAndInstToEmailTxtBoxSendkeys(excelData(11, 3));
	}

	@Then("^: Verify that system should display Minimum validation message as Message and Instruction to send in Email should be at least (\\d+) characters long\\.$")
	public void verify_that_system_should_display_Minimum_validation_message_as_Message_and_Instruction_to_send_in_Email_should_be_at_least_characters_long(
			int arg1) throws Throwable {
		smartAlrtObj.verifyMsgAndInstToEmailTxtBoxMinMsg();
	}

	@Given("^: Enter more than (\\d+) characters in Message and Instruction to send in Email textareax$")
	public void enter_more_than_characters_in_Message_and_Instruction_to_send_in_Email_textareax(int arg1)
			throws Throwable {
		smartAlrtObj.msgAndInstToEmailTxtBoxSendkeys(excelData(12, 3));
	}

	@Then("^: Verify that system should display Maximum validation message as Message and Instructions to send in Email cannot be more than (\\d+) characters\\.$")
	public void verify_that_system_should_display_Maximum_validation_message_as_Message_and_Instructions_to_send_in_Email_cannot_be_more_than_characters(
			int arg1) throws Throwable {
		smartAlrtObj.verifyMsgAndInstToEmailTxtBoxMaxMsg();
	}

	@Given("^: Enter valid data in Message and Instruction to send in Email$")
	public void enter_valid_data_in_Message_and_Instruction_to_send_in_Email() throws Throwable {
		smartAlrtObj.msgAndInstToEmailTxtBoxSendkeys(excelData(13, 3));
	}

	@Then("^: Verify that system should not display any validation message when a user enter valid value$")
	public void verify_that_system_should_not_display_any_validation_message_when_a_user_enter_valid_value()
			throws Throwable {
		smartAlrtObj.verifyMsgAndInstToEmailTxtNoValidMsg();
	}

	@Given("^: Click on Email Address\\(es\\) textbox Press \"([^\"]*)\" Key$")
	public void click_on_Email_Address_es_textbox_Press_Key(String arg1) throws Throwable {
		smartAlrtObj.emailAddTxtBox.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that system should display required validation message as Please enter at least one Email Address\\.$")
	public void verify_that_system_should_display_required_validation_message_as_Please_enter_at_least_one_Email_Address()
			throws Throwable {
		smartAlrtObj.verifyEmailAddTxtBoxReqMsg();
	}

	@Given("^: Enter less than (\\d+) characters in Email Address\\(es\\) textbox$")
	public void enter_less_than_characters_in_Email_Address_es_textbox(int arg1) throws Throwable {
		smartAlrtObj.emailAddTxtBoxSendkeys(excelData(15, 4));
	}

	@Then("^: Verify that system should display validation message as Invalid Email Address\\.$")
	public void verify_that_system_should_display_validation_message_as_Invalid_Email_Address() throws Throwable {
		smartAlrtObj.verifyEmailAddTxtBoxInvalidMsg();
	}

	@Given("^: Select Module Name from the dropdown$")
	public void select_Module_Name_from_the_dropdown() throws Throwable {
		smartAlrtObj.selectMdulDrpDwn(excelData(16, 5));
	}

	@Then("^: Verify that a user is allowed to select any module from Module dropdown$")
	public void verify_that_a_user_is_allowed_to_select_any_module_from_Module_dropdown() throws Throwable {
		smartAlrtObj.verifySelectMdulDrpDwn(excelData(16, 5));
	}

	@Given("^: Select Module Name from the dropdown then deselet module$")
	public void select_Module_Name_from_the_dropdown_then_deselet_module() throws Throwable {
		smartAlrtObj.selectMdulDrpDwn(excelData(16, 5));
	}

	@Then("^: Verify that system should display required validation message as \"([^\"]*)\"$")
	public void verify_that_system_should_display_required_validation_message_as(String arg1) throws Throwable {
		smartAlrtObj.verifyMdulDrpDwnReqMsg();
	}

	@Given("^: Click on App dropdown$")
	public void click_on_App_dropdown() throws Throwable {
		smartAlrtObj.appDrpdwnClick();
	}

	@Given("^: Click on Uncheck All option$")
	public void click_on_Uncheck_All_option() throws Throwable {
		smartAlrtObj.appDrpdwnUnchckAllOptClick();

	}

	@Then("^: Verify that system should display required validation message as App is required\\.$")
	public void verify_that_system_should_display_required_validation_message_as_App_is_required() throws Throwable {
		smartAlrtObj.mdulDrpdwn.click();
		smartAlrtObj.verifyAppDrpDwnReqMsg();
	}

	@Given("^: Click on Check All option$")
	public void click_on_Check_All_option() throws Throwable {
		smartAlrtObj.appDrpdwnChckAllOptClick();
	}

	@Then("^: Verify that system should select All the listed App from App dropdown$")
	public void verify_that_system_should_select_All_the_listed_App_from_App_dropdown() throws Throwable {
		smartAlrtObj.verifyAppDrpdwnChckAllOpt();
	}

	@Then("^: Verify that system should deselect All the listed App from App dropdown$")
	public void verify_that_system_should_deselect_All_the_listed_App_from_App_dropdown() throws Throwable {
		smartAlrtObj.verifyAppDrpdwnUnchckAllOpt();
	}

	@Given("^: Select Daily radio button of Smart Alert Frequencey Type$")
	public void select_Daily_radio_button_of_Smart_Alert_Frequencey_Type() throws Throwable {
		smartAlrtObj.dailyRdoBtnClick();
	}

	@Given("^: Click on Every day textbox and Press \"([^\"]*)\" Key$")
	public void click_on_Every_day_textbox_and_Press_Key(String arg1) throws Throwable {
		smartAlrtObj.dailyTxtBox.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that system should display required message as Every Day\\(s\\) is required\\.$")
	public void verify_that_system_should_display_required_message_as_Every_Day_s_is_required() throws Throwable {
		smartAlrtObj.verifyDailyTxtBoxReqMsg();
	}

	@Given("^: Click on Every textbox and Enter Every day textbox Value as (\\d+)$")
	public void click_on_Every_textbox_and_Enter_Every_day_textbox_Value_as(int arg1) throws Throwable {
		smartAlrtObj.dailyTxtBoxSendkeys(excelData(17, 7));
	}

	@Then("^: Verify that system should display maximum validation message for Every day textbox as Invalid Number\\. You cannot add more than (\\d+) days\\.$")
	public void verify_that_system_should_display_maximum_validation_message_for_Every_day_textbox_as_Invalid_Number_You_cannot_add_more_than_days(
			int arg1) throws Throwable {
		smartAlrtObj.verifyDailyTxtBoxLimitMsg();
	}

	@Given("^: Click on Every textbox and Enter Every day textbox Value as e$")
	public void click_on_Every_textbox_and_Enter_Every_day_textbox_Value_as_e() throws Throwable {
		smartAlrtObj.dailyTxtBoxSendkeys(excelData(18, 7));
	}

	@Then("^: Verify that system should display invalid validation message for Every day textbox as Invalid Number\\.$")
	public void verify_that_system_should_display_invalid_validation_message_for_Every_day_textbox_as_Invalid_Number()
			throws Throwable {
		smartAlrtObj.verifyDailyTxtBoxInvalidMsg();
	}

	@Given("^: Click on Every textbox and Enter Valid Value in Every day textbox$")
	public void click_on_Every_textbox_and_Enter_Valid_Value_in_Every_day_textbox() throws Throwable {
		smartAlrtObj.dailyTxtBoxSendkeys(excelData(19, 7));
	}

	@Then("^: Verify that system should not display any validation message when a user enter Valid value into Every day textbox$")
	public void verify_that_system_should_not_display_any_validation_message_when_a_user_enter_Valid_value_into_Every_day_textbox()
			throws Throwable {
		smartAlrtObj.verifyDailyTxtBoxNoValidMsg();
	}

	@Given("^: Select Weekly radio button of Smart Alert Frequencey Type$")
	public void select_Weekly_radio_button_of_Smart_Alert_Frequencey_Type() throws Throwable {
		smartAlrtObj.weeklyRdoBtnClick();
	}

	@Given("^: Click on Every weekly textbox and Press \"([^\"]*)\" Key$")
	public void click_on_Every_weekly_textbox_and_Press_Key(String arg1) throws Throwable {
		smartAlrtObj.weeklyTxtBox.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that system should display required message as Every Week\\(s\\) is required\\.$")
	public void verify_that_system_should_display_required_message_as_Every_Week_s_is_required() throws Throwable {
		smartAlrtObj.verifyWeeklyTxtBoxReqMsg();
	}

	@Given("^: Click on Every textbox and Enter Every weekly textbox Value as (\\d+)$")
	public void click_on_Every_textbox_Enter_Every_weekly_textbox_Value_as(int arg1) throws Throwable {
		smartAlrtObj.weeklyTxtBoxSendkeys(excelData(20, 8));
	}

	@Then("^: Verify that system should display maximum validation message as Invalid Number\\. You cannot add more than (\\d+) weeks\\.$")
	public void verify_that_system_should_display_maximum_validation_message_as_Invalid_Number_You_cannot_add_more_than_weeks(
			int arg1) throws Throwable {
		smartAlrtObj.verifyWeeklyTxtBoxLimitMsg();
	}

	@Given("^: Click on Every textbox and Enter Every weekly textbox Value as e$")
	public void click_on_Every_textbox_and_Enter_Every_weekly_textbox_Value_as_e() throws Throwable {
		smartAlrtObj.weeklyTxtBoxSendkeys(excelData(21, 8));
	}

	@Then("^: Verify that system should display invalid validation message of every weekly textbox as Invalid Number\\.$")
	public void verify_that_system_should_display_invalid_validation_message_of_every_weekly_textbox_as_Invalid_Number()
			throws Throwable {
		smartAlrtObj.verifyWeeklyTxtBoxInvalidMsg();
	}

	@Given("^: Click on Every textbox and Enter Valid Value in Every weekly textbox$")
	public void click_on_Every_textbox_and_Enter_Valid_Value_in_Every_weekly_textbox() throws Throwable {
		smartAlrtObj.weeklyTxtBoxSendkeys(excelData(22, 8));
	}

	@Then("^: Verify that system should not display any validation message when a user enter Valid value into weekly textbox$")
	public void verify_that_system_should_not_display_any_validation_message_when_a_user_enter_Valid_value_into_weekly_textbox()
			throws Throwable {
		smartAlrtObj.verifyWeeklyTxtBoxNoValidMsg();
	}

	@Given("^: Select the Date from the houlry Start Date Picker$")
	public void select_the_Date_from_the_houlry_Start_Date_Picker() throws Throwable {
		System.err.println("this is blank method");
	}

	@Then("^: Verify that user should able to select Date for Start Date of hourly$")
	public void verify_that_user_should_able_to_select_Date_for_Start_Date_of_hourly() throws Throwable {

		String toSplit = excelData(23, 9);
		String[] splitted = toSplit.split("[-/]");
		Thread.sleep(1000);
		WebElement CalPath = ObjectRepo.driver.findElement(By.xpath("//div[1]/div/div[2]/p[1]/span/button/i"));

		Datepicker.Dateselection(splitted[0], Integer.parseInt(splitted[1]), Integer.parseInt(splitted[2]), CalPath);

	}

	@Given("^: Select the Date from the End Date Picker$")
	public void select_the_Date_from_the_End_Date_Picker() throws Throwable {
		System.err.println("this is blank method");
	}

	@Then("^: Verify that user should able to select Date for End Date$")
	public void verify_that_user_should_able_to_select_Date_for_End_Date() throws Throwable {
		String toSplit = excelData(23, 9);
		String[] splitted = toSplit.split("[-/]");
		Thread.sleep(1000);
		WebElement CalPath = ObjectRepo.driver.findElement(By.xpath("//div[2]/div/div[2]/p/span/button/i"));

		Datepicker.Dateselection(splitted[0], Integer.parseInt(splitted[1]), Integer.parseInt(splitted[2]), CalPath);

	}

	@Given("^: Click on \"([^\"]*)\" link of the first record of the Smart Alerts listing screen$")
	public void click_on_link_of_the_first_record_of_the_Smart_Alerts_listing_screen(String arg1) throws Throwable {
		new SetsPageObject(ObjectRepo.driver).clickViewAuditTrailLink();
	}

	@Then("^: Verify that system should displayed Audit Trail page when user click on \"([^\"]*)\" link of the Smart Alert listing screen$")
	public void verify_that_system_should_displayed_Audit_Trail_page_when_user_click_on_link_of_the_Smart_Alert_listing_screen(
			String arg1) throws Throwable {
		new SetsPageObject(ObjectRepo.driver).verifyAuditTrailPage();
	}

	@Then("^: Verify the search results of Smart alert listing page$")
	public void verify_the_search_results_of_Smart_alert_listing_page() throws Throwable {
		String searchItem = excelData(1, 2);

		new EntitiesPageObject(ObjectRepo.driver).verifysearch("Week Alerts");
	}
	@Then("^: Verify that all the records of Smart alert column display in ascending order when click on sorting icon$")
	public void verify_that_all_the_records_of_Smart_alert_column_display_in_ascending_order_when_click_on_sorting_icon() throws Throwable {
		new AttributesPageObject(ObjectRepo.driver).verifyAscendingOrder("Smart Alert");

	}
	
	@Given("^: Click on Sorting icon of the Smart Alert column$")
	public void click_on_Sorting_icon_of_the_Smart_Alert_column() throws Throwable {
	}

	@Then("^: Verify that all the records of Smart Alert column display in descending order when you click on smart alert$")
	public void verify_that_all_the_records_of_Smart_Alert_column_display_in_descending_order_when_you_click_on_smart_alert() throws Throwable {
	}
}// end
