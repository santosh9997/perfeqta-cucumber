Feature: Module Name: Procedure
Executed script on "https://test1.beperfeqta.com/mdav33"    

@chrome
Scenario: Verify breadcrumb functionality of Procedure Screen
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Procedure Tile 
    And : Click on bread crumb of Procedure screen
    Then : Verify that the page should be redirected to the Administration Page

@chrome
Scenario: Verify pagination of Procedure listing screen when user click on "First"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on the Last button of Pagination 
	  And : Click on the First button of Pagination 
    Then : Verify that system should display the First page of the Procedure listing screen
    
@chrome
Scenario: Verify pagination of Procedure listing screen when user click on "Last"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on the Last button of Pagination
    Then : Verify that system should display the Last page of the Procedure listing screen

@chrome
Scenario: Verify pagination of Procedure listing screen when user click on "Next"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Next button of Pagination
    Then : Verify that system should display the Next page of the Procedure listing screen    
    

@chrome
Scenario: Verify pagination of Procedure listing screen when user click on "Previous"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Next button of Pagination
    And : Click on the Previous button of Pagination 
    Then : Verify that system should display the Previous page of the Procedure listing screen    

@chrome
Scenario: Verify Edit Procedure Page load 
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on First record from Procedure listing screen
    Then : Verify that system should be redirected to the Edit Procedure page 
    
@chrome 
Scenario: Verify Breadcrumb functionality of Edit Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on First record from Procedure listing screen
    Then : Verify that Breadcrumb for Edit Procedure screen should be displayed as Home > Administration > Procedures > Add / Edit Procedure
    

@chrome
Scenario: Verify functionality of Save Button for Edit Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on First record from Procedure listing screen
    And : Click on Save Button of Edit Procedure screen
    Then : Verify that system should save the record and redirected to the Procedure listing screen when user click on Save Button

@chrome
Scenario: Verify functionality of Cancel Button for Edit Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on First record from Procedure listing screen
    And : Click on Cancel Button Edit Procedure
    Then : Verify that system should be redirected to the Procedure listing screen when user click on Cancel Button
    
@chrome
Scenario: Verify the color of Save button of Edit Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on First record from Procedure listing screen
    Then : Verify the color of Save Button of Edit Procedure screen should be Green
    
@chrome
Scenario: Verify the color of Cancel button of Edit Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on First record from Procedure listing screen
    Then : Verify the color of Cancel Button of Edit Procedure screen should be Black
    
@chrome @ignore
Scenario: Verify the Search functionality of Procedure listing screen
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Enter the Procedure name into the search box of Procedure listing screen
    Then : Verify the search result of Procedure Listin Screen

@chrome @ignore
Scenario: Verify the Ascending order for the Procedure column under Procedure listing screen
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Procedure Tile
    Then : Verify that all the records of Procedure column display in ascending order
    
@chrome @ignore
Scenario: Verify the Descending order for the Procedure column under Procedure listing screen
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Sorting icon of the Procedures column
    Then : Verify that all the records of Procedure column display in descending order
    
@chrome
Scenario: Verify View Audit Trail Page redirection and Breadcrumbs of Site Level tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on View Audit Trail link of first record in Procedure listing screen
    Then : Verify that system should be redirected to the Audit Trail page of the first record when user click on View Audit Trail link
    
@chrome
Scenario: Verify Breadcrumb functionality of Audit Trail screen 
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
     And : Click on View Audit Trail link of first record in Procedure listing screen
    Then : Verify that Breadcrumb for Edit Procedure screen should be displayed as Home > Administration > Procedures > Audit Trail

@chrome @ignore
Scenario: Verify Pagination of Audit Trail screen 
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on View Audit Trail link of first record in Procedure listing screen
    Then : Verify that total number of entries should be matched with pagination of Audit Trail screen
    
@chrome
Scenario: Verify the color of Back Button of Audit Trail screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on View Audit Trail link of first record in Procedure listing screen
    And : Verify that the color of Back Button of Audit Trail screen should be Black

@chrome
Scenario: Verify functionality of Back Button of Audit Trail screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on View Audit Trail link of first record in Procedure listing screen
    And : Click on Back Button of Audit Trail screen
    Then : Verify that system should be redirected to the Procedure listing screen when user click on Back Button
    
@chrome
Scenario: Verify Required validation message for Procedure Name in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Press "TAB" key in add procedure name input box
    Then : Verify that Required validation message for Procedure Name should be displayed as Procedure Name is required.
    
@chrome 
Scenario: Verify Minimum validation message for Procedure Name in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter less than 2 characters in the textbox of Procedure Name
    Then : Verify that Minimum validation message for Procedure Name should be displayed as Procedure name must be at least 2 characters.
    
@chrome
Scenario: Verify Maximum validation message for Procedure Name in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter more than 200 characters in the textbox of Procedure Name
    Then : Verify that Maximum validation message for Procedure Name should be displayed as Procedure name cannot be more than 200 characters.
  
@chrome
Scenario: Verify No validation message for Procedure Name in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter valid data in the textbox of Procedure Name
    Then : Verify that system should not display any validation message when user enter valid data in the textbox of Procedure Name
    
@chrome
Scenario: Verify Unique validation message for Procedure Name in Add Procedure screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on Add New Button procedure
    And : Enter Procedure Name of textbox which is already added 
    Then : Verify that Unique validation message for Procedure Name should be displayed as Procedure Name must be unique.
    
 @chrome 
Scenario: Verify functionality of Save Button for Add Procedure screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
	And : Click on Add New Button procedure
	And : Enter valid data in the textbox of Procedure Name
	And : Enter valid data in the textbox of Procedure Tag
  And : Enter valid data of all mandatory field in Add Procedure screen
  And : Click on Save Button in Add Procedure screen
  Then : Verify that New Procedure should be added into the system when user click on Save Button in Add Procedure screen
    
@chrome 
Scenario: Verify functionality of Edit Existing record of Procedure module
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Procedure Tile
    And : Click on the first record of the Procedures column
    And : Click on Procedure Name textbox
    And : Edit the Procedure Name
    And : Click on Save Button procedure record
    Then : Verify that system should save the Editable data into the Existing record of Procedure module
    
@chrome
Scenario: Verify text of Label of Copy Procedure pop-up in Procedure Screen    
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Copy icon of first record of Action column
  Then : Verify that Label of Copy Procedure pop-up should be displayed as Procedure Name *
    
@chrome
Scenario: Verify default value '- Copy' is displayed with Procedure name in Copy Procedure pop-up 
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Copy icon of first record of Action column for checking -copy 
  Then : Verify that system should display '- Copy' value by default with Procedure Name in Copy Procedure pop-up
    
@chrome
Scenario: Verify functionality of Create Copy of Procedure
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Copy icon of first record of Action column
  And : Click on Ok button of pop up window 
  Then : Verify that system should Create Copy of Procedure when user click on OK Button
    
@chrome @ignore
Scenario: Verify the Ascending order for the Last Updated column under Procedure listing screen
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Sorting icon of the Last Updated column
  Then : Verify that all the records should be displayed in ascending order
    
@chrome @ignore
Scenario: Verify the Descending order for the Last Updated column under Procedure listing screen
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Sorting icon of the Last Updated column
  Then : Verify that all the records  should be displayed in descending order Verify  functionality of Save Button for Add Procedure screen
    
    @chrome
Scenario: Verify Required validation message for Procedure Tag in Add Procedure screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Press "TAB" key in add Procedure Tag input box
  Then : Verify that Required validation message for Procedure Tag should be displayed as Procedure Name is required.
    
@chrome
Scenario: Verify Minimum validation message for Procedure Tag in Add Procedure screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter less than 2 characters in the textbox of Procedure Tag
  Then : Verify that Minimum validation message for Procedure Tag should be displayed as Procedure name must be at least 2 characters.
    
@chrome
Scenario: Verify Maximum validation message for Procedure Tag in Add Procedure screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter more than 200 characters in the textbox of Procedure Tag
  Then : Verify that Maximum validation message for Procedure Tag should be displayed as Procedure name cannot be more than 200 characters.
   
@chrome 
Scenario: Verify No validation message for Procedure Name in Add Procedure screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter valid data in the textbox of Procedure Tag
  Then : Verify that system should not display any validation message when user enter valid data in the textbox of Procedure Tag
  
  @chrome
Scenario: Verify checkbox of Instruction for user in Add Procedure screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter valid data in the textbox of Procedure Name
  And : Click on Instruction for user checkbox
  Then : Verify that system should allow toclick on checkbox  
    
 @chrome
Scenario: Verify Required validation message for Instructions in Add Procedure screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter valid data in the textbox of Procedure Name
  And : Click on Instruction for user checkbox
  And : Click on Instructions input box
  And : Press Tab key Instruction Input box
  Then : Verify that Required validation message for instructions should be displayed as Instructions is required.  
    
    @chrome
Scenario: Verify Minimum validation message for Instructions in Add Procedure screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter valid data in the textbox of Procedure Name
  And : Click on Instruction for user checkbox
  And : Click on Instructions input box
  And : Enter less than 2 characters in the textbox of Instruction Input box
  Then : Verify that Minimum validation message for instructions should be displayed as Instructions for user must be at least 2 characters. 
    
    @chrome
Scenario: Verify Maximum validation message for Instructions in Add Procedure screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter valid data in the textbox of Procedure Name
  And : Click on Instruction for user checkbox
  And : Click on Instructions input box
  And : Enter more than 200 characters in the textbox of Instruction Input box
  Then : Verify that Maximum validation message for instructions should be displayed as Instructions for user must be at least 2 characters. 
    
    
    @chrome 
Scenario: Verify valid data for Instructions in Add Procedure screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter valid data in the textbox of Procedure Name
  And : Click on Instruction for user checkbox
  And : Click on Instructions input box
  And : Enter Valid data Instruction Input box
  Then : Verify that no validation message for instructions   
    
    @chrome
Scenario: Verify Url checkbox in Add Procedure screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter valid data in the textbox of Procedure Name
  And : Click on Instruction for user checkbox
  And : Click on Instructions input box
  And : Enter Valid data Instruction Input box
  And : Click on Url checkbox
  Then : Verify Url checkbox Should be clickable. 
    
   @chrome
Scenario: Verify Required Url checkbox in Add Procedure screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter valid data in the textbox of Procedure Name
  And : Click on Instruction for user checkbox
  And : Click on Instructions input box
  And : Enter Valid data Instruction Input box
  And : Click on Url checkbox
  And : press Tab in URl box
  Then : Verify Required msg Should be displayed.   
    
@chrome
Scenario: Verify Invalid Url checkbox in Add Procedure screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter valid data in the textbox of Procedure Name
  And : Click on Instruction for user checkbox
  And : Click on Instructions input box
  And : Enter Valid data Instruction Input box
  And : Click on Url checkbox
  And : Enter invalid data url
  Then : Verify invalid  msg Should be displayed as Not a valid URL, don't forget to use http:// or https:// 
      
  @chrome
Scenario: Verify default status of Toggle button in Add Procedure screen  
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
 	And : Click on Procedure Tile
  And : Click on Add New Button procedure
 	Then : Verify that Toggle button should be Active by default new Procedure screen
 	
 #	================================================================================
 	
@chrome @done @3.4
Scenario: Verify for the View Previous Version link is having proper functionality
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on View Previous Version Link
  Then : Verify that View Previous Version link is having proper functionality
  
@chrome @done @3.4
Scenario: Verify for the View Linked Information link is having proper functionality
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on View Linked Information Link
  Then : Verify for the View Linked Information link is having proper functionality
  
@chrome @done @3.4
Scenario: Verify that Procedure link is having proper functionality
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Procedure Name link
  Then : Verify that Procedure Name link is having proper functionality
  
@chrome @done @3.4
Scenario: Verify that Filter Procedure by Tag functionality 
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter Procedure Name in Procedure Name textfield
  And : Enter tag in Procedure Tag field
  And : Add Question in Procedure
  And : Click on Save button of Add/Edit Procedure screen
  And : Click on Add New Button procedure
  And : Enter tag in Procedure Tag field
  Then : Verify that Procedure Tag is having proper functionality
  
@chrome @done @3.4
Scenario: Verify for the Optional Question functionality
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter Procedure Name in Procedure Name textfield
  And : Enter tag in Procedure Tag field
  And : Add Question in Procedure
  And : Click on Optional Question check-box
  And : Click on Save button of Add/Edit Procedure screen
  Then : Verify that Optional Question is having proper functionality
  
@chrome @done @3.4
Scenario: Verify the Standard Acceptance Criteria functionality of Add/Edit Procedure screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter Procedure Name in Procedure Name textfield
  And : Enter tag in Procedure Tag field
  And : Add Question in Procedure
  And : Click on Acceptance Criteria button of selected Question
  And : Select Condition from the Select Condidtion drop-down
  And : Select Value from Select Value drop-down
  And : Click on Save button of Acceptance Criteria screen of Procedure module 
  Then : Verify that Acceptance criteria is added successfully
  
@chrome @done @3.4
Scenario: Verify that remove functionality of Question from Add/Edit Procedure screen is proper
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter Procedure Name in Procedure Name textfield
  And : Enter tag in Procedure Tag field
  And : Add Question in Procedure
  And : Click on cross Icon
  Then : Verify that the question which is selected should remove
  
@chrome @done @3.4
Scenario: Verify the sorting functionality of Add/Edit Procedure screen 
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  And : Enter Procedure Name in Procedure Name textfield
  And : Enter tag in Procedure Tag field
  And : Add Question in Procedure
  And : Select multiple questions in Add/Edit Procedure
  Then : Verify that sorting functionality should proper
  
@chrome @done @3.4
Scenario: Verify the searching functionality of 'No Records Found' of Tag in Procedure module 
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Search by Tag which is not having any Procedure
  Then : Verify that No Records Found message should display in Procedure Listing screen

@chrome @done @3.4
Scenario: Verify the Page Size functionality for 50 records in Procedure module
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Select 50 in Page Size Drop-down
  Then : Verify that Procedure listing screen should display only 50 records
  
@chrome @done @3.4
Scenario: Verify the Page Size functionality for 20 records in Procedure module
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Select twenty(20) in Page Size Drop-down
  Then : Verify that Procedure listing screen should display only twenty(20) records
  
@chrome @done @3.4
Scenario: Verify the Page Size functionality for 10 records in Procedure module
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  Then : Verify that Procedure listing screen should display only ten(10) records
  
@chrome @done @3.4
Scenario: Verify that Previous Version screen is disable to edit/update the Procedure
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on View Previous Version Link
  And : Click on any version from the list of Version
  Then : Verify that Previous Version screen is not having Save button
  
@chrome @done @3.4 
Scenario: Verify that Back button is having proper functionality of Previous Version screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on View Previous Version Link
  And : Click on any version from the list of Version
  Then : Verify that Back button of Previous Version screen is having proper functionality
  
@chrome @done @3.4
Scenario: Verify for the View Linked Information link is displaying App name which is used
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on View Linked Information Link of any record
  Then : Verify for the View Linked Information link App name verificaiton
  
@chrome @done @3.4
Scenario: Verify that Add/Edit Procedure Screen is displaying current version
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  Then : Verify system is displaying current version of the Add/Edit Procedure screen
  
@chrome @done @3.4
Scenario: Verify that Add/Edit screen is displaying Filter by Question Tag and Select Question field 
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Click on Add New Button procedure
  Then : Verify that Filter By Question Tag and Select Question field is visible
  
@chrome @done @3.4
Scenario: Verify that Tooltip of copy procedure icon
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  Then : Verify the tooltip of copy icon
  
@chrome @done @3.4
Scenario: Verify the Updated Status in to the Procedure Listing screen
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
  And : Get Status of first Procedure from Listing
  And : Click on first Procedure Name link
  And : Click on Status toggle button
  And : Click on Save button of Add/Edit Procedure screen
  Then : Verify that Status Functionality should update in listing screen
  
@chrome @done @3.4
Scenario: Verify the functionality of Acceptance Criteria button of Add/Edit Procedure screen 
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
	And : Click on Add New Button procedure
  And : Enter Procedure Name in Procedure Name textfield
  And : Enter tag in Procedure Tag field
  And : Add Question in Procedure
  And : Click on Acceptance Criteria button of selected Question
  Then : Verify that Acceptance Criteria button is clickable
  
@chrome @done @3.4 
Scenario: Verify the functionality of Acceptance Criteria button of Add/Edit Procedure screen 
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Procedure Tile
	And : Click on Add New Button procedure
  And : Enter Procedure Name in Procedure Name textfield
  And : Enter tag in Procedure Tag field
  And : Add Question in Procedure
  And : Select multiple questions in Add/Edit Procedure
  And : Click on Acceptance Criteria button of selected Question
  And : Click on Advanced Acceptance Criteria radio button
  And : Click on Add Condition icon of Advanced Acceptance Criteria
  And : Click on Select Question drop-down
  And : Select the option from the Question drop-down
  And : Select Equal To Operator from Select Operator drop-down
  And : Enter the proper Value in Value field
  And : Click on Add button of Advanced Acceptance Criteria screen
  Then : Verify that Advanced Acceptance Criteria should added
  

    
 
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       