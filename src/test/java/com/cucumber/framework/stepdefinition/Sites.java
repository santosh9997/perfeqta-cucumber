package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.WebDriver;

import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.helper.PageObject.EntitiesPageObject;
import com.cucumber.framework.helper.PageObject.GeneralSettingsPageObject;
import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.SitesPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import mx4j.log.Log;

public class Sites {

	public ExcelUtils excel;
	private Login login;
	private SitesPageObject sitePage;


	public Sites() {
//		lpage = new LoginPageObject(ObjectRepo.driver);
		 new LoginPageObject(ObjectRepo.driver);
		 sitePage = new SitesPageObject(ObjectRepo.driver);
		
	}
	
	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Sites");
		System.err.println();
		return excel.readXLSFile("Sites", rowVal, colVal);
			}

	@Given("^: Click on Sites Tile$")
	public void click_on_Sites_Tile() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    sitePage.ClickonSitesTile();
	}

	@Then("^: Verify the module name as Sites$")
	public void verify_the_module_name_as_Sites() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		sitePage.verifySitesTile();
	}
	
	@Given("^: Click on Site Level Name tab$")
	public void click_on_Site_Level_Name_tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    sitePage.clickonSiteLevelName();
	}

	@Then("^: Verify that system should display information message as Only Sites defined through bottom level will display in App\\.$")
	public void verify_that_system_should_display_information_message_as_Only_Sites_defined_through_bottom_level_will_display_in_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     sitePage.verifySiteLevelname();
	}
	@Given("^: Enter the Sites name into search box of Sites listing screen$")
	public void enter_the_Sites_name_into_search_box_of_Sites_listing_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    sitePage.enterSiteNameInSearchbox(excelData(1, 1));
	}

	@Given("^: Click on remove button of search textbox$")
	public void click_on_remove_button_of_search_textbox() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     sitePage.clickonRemoveBtn();
	}

	@Then("^: Verify that Sites name should be removed from search box when user press remove button$")
	public void verify_that_Sites_name_should_be_removed_from_search_box_when_user_press_remove_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	  sitePage.verifysearchboxRemovebtn();
	}

	@Given("^: Click on First Site from Site listing page$")
	public void click_on_First_Site_from_Site_listing_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    sitePage.clickonFirstSiteofSitelisting();
	}
	@Given("^: Click on Site listing page$")
	public void click_on_Site_listing_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Thread.sleep(2000);
		sitePage.clickonSitelisting();
	}
	@Then("^: Verify that Add / Edit Site page should be displayed when a user click on Site name$")
	public void verify_that_Add_Edit_Site_page_should_be_displayed_when_a_user_click_on_Site_name() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   sitePage.verifyaddEditofSitelisting();
	}

	@Given("^: Click on \"([^\"]*)\" button of site$")
	public void click_on_button_of_site(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
	}
	@Given("^: Click on \"([^\"]*)\" button of site Level Name$")
	public void click_on_button_of_site_Level_Name(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	  sitePage.clickonAddnewofsiteLevelName();
	}

	@Then("^: Verify that color of Save button should be green$")
	public void verify_that_color_of_Save_button_should_be_green() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    sitePage.verifyColorofSaveBtn();
	}

	@Then("^: Verify that color of Cancel button should be black$")
	public void verify_that_color_of_Cancel_button_should_be_black() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		sitePage.verifyColorofcancelBtn();
	}

	@Then("^: Verify that color of Cancel button should be black in View Audit screen$")
	public void verify_that_color_of_Cancel_button_should_be_black_in_View_Audit_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		sitePage.verifyColorofcancelBtnViewAuditScreen();
	}
	@Given("^: Click on First Site name$")
	public void click_on_First_Site_name() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}
	
	@Given("^: Click on First Site name of site level name$")
	public void click_on_First_Site_name_of_site_level_name() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     sitePage.clickonFirstSiteofsitelevelName();
	}
	@Then("^: Verify that system should be redirect to the Site Level Name screen when a user click on the Cancel button$")
	public void verify_that_system_should_be_redirect_to_the_Site_Level_Name_screen_when_a_user_click_on_the_Cancel_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	      sitePage.verifycancelBtnofSiteLevelName();
	}
	@Given("^: Click on cancel btn of First Site name of site level name$")
	public void click_on_cancel_btn_of_First_Site_name_of_site_level_name() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     sitePage.clickcancelBtnofSiteLevelName();
	}
	@Then("^: Verify that system should be redirect to the Site Listing screen when a user click on the Cancel button$")
	public void verify_that_system_should_be_redirect_to_the_Site_Listing_screen_when_a_user_click_on_the_Cancel_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		 sitePage.verifycancelBtnofSiteLevelName();
	}

@Then("^: Verify that system should be redirect to the Site Listing screen when a user click on the Cancel button in View Audit Screen$")
public void verify_that_system_should_be_redirect_to_the_Site_Listing_screen_when_a_user_click_on_the_Cancel_button_in_View_Audit_Screen() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
	sitePage.verifycancelBtnofviewAuditredirection();
}

	 
	@Given("^: Click on View Audit Trail link of the first record in Site Level Name$")
	public void click_on_View_Audit_Trail_link_of_the_first_record_in_Site_Level_Name() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     sitePage.clickonViewAuditSiteLevename();
	}
	@Given("^: Click on View Audit Trail link of the first record Site Listing Screen$")
	public void click_on_View_Audit_Trail_link_of_the_first_record_Site_Listing_Screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     sitePage.clickonViewAuditSitelistingScreen();
	}
	@Given("^: Click on the Cancel button in View Audit Screen$")
	public void click_on_the_Cancel_button_in_View_Audit_Screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   sitePage.clickonCancelBtnViewAuditScreen();
	}

	@Then("^: Verify that system should be redirect to the Site Level Name Screen when a user click on the Site Level tab$")
	public void verify_that_system_should_be_redirect_to_the_Site_Level_Name_Screen_when_a_user_click_on_the_Site_Level_tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     sitePage.verifySiteLevelNamescreen();
	}

	@Then("^: Verify that system should be redirect to the Audit Trail screen when a user click on the View Audit Trail link of the first record$")
	public void verify_that_system_should_be_redirect_to_the_Audit_Trail_screen_when_a_user_click_on_the_View_Audit_Trail_link_of_the_first_record() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     sitePage.verifyredirectionofAudittrailScreen();
	}
	
	@Then("^: Verify that color of Cancel button should be black in Audit Trail of site level name$")
	public void verify_that_color_of_Cancel_button_should_be_black_in_Audit_Trail_of_site_level_name() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		 sitePage.verifyColorofCancelButtonAudittrailinSiteLevelName();
	}

	@Given("^: Click on View Audit Trail link of the first record$")
	public void click_on_View_Audit_Trail_link_of_the_first_record() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    sitePage.clickonViewAuditSitelistingScreen();
	}
	@Given("^: Click on View Audit Trail link of the first record site level name$")
	public void click_on_View_Audit_Trail_link_of_the_first_record_site_level_name() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     sitePage.clickonViewAuditSiteLevename();
	}

	@Then("^: Verify total number of entries should be match with pagination of Audit Trail Page for Site Level Name tab$")
	public void verify_total_number_of_entries_should_be_match_with_pagination_of_Audit_Trail_Page_for_Site_Level_Name_tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    sitePage.verifytotalEntriesSiteListing();
	}


	@Given("^: Click on the Cancel button in View Audit of site level name$")
	public void click_on_the_Cancel_button_in_View_Audit_of_site_level_name() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    sitePage.clickonCancelButtonViewAuditofSitelevel();
	}

	@Given("^: Click on the Site Level Name tab$")
	public void click_on_the_Site_Level_Name_tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
	}
	
	@Then("^: Verify total number of entries should be match with Pagination count of Site Listing tab$")
	public void verify_total_number_of_entries_should_be_match_with_Pagination_count_of_Site_Listing_tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		sitePage.verifytotalEntriesSiteListing();
	}

	@Then("^: Verify that system should be redirect to the Listing screen of Site Level Name tab when a user click on the Cancel button$")
	public void verify_that_system_should_be_redirect_to_the_Listing_screen_of_Site_Level_Name_tab_when_a_user_click_on_the_Cancel_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		sitePage.verifycancelBtnofviewAuditredirectionSiteLevel();
	}

	
	@Then("^: Verify the search result in site listing screen$")
	public void verify_the_search_result_in_site_listing_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		new EntitiesPageObject(ObjectRepo.driver).verifysearch(excelData(1, 1));
	}
	
 

	@Then("^: Verify that system should display the Last page of the Sites listing screen$")
	public void verify_that_system_should_display_the_Last_page_of_the_Sites_listing_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}
	@Given("^: Click on Last button of Pagination of site listing screen$")
	public void click_on_Last_button_of_Pagination_of_site_listing_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    sitePage.clickLastbtnofSiteListing();
	}

	@Given("^: Click on First button of Pagination of site listing screen$")
	public void click_on_First_button_of_Pagination_of_site_listing_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
	}

	@Then("^: Verify that system should display the First page of the Sites listing screen$")
	public void verify_that_system_should_display_the_First_page_of_the_Sites_listing_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
	}
	@Then("^: Verify total number of entries should be match with pagination count of Site Level Name tab$")
	public void verify_total_number_of_entries_should_be_match_with_pagination_count_of_Site_Level_Name_tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     sitePage.verifyNumberofEntries();
	}
	@Given("^: Click on Sorting icon of the Site Name column  ascending order$")
	public void click_on_Sorting_icon_of_the_Site_Name_column_ascending_order() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    sitePage.clickonSortingIconOfSitelevel();
	}

	@Then("^: Verify that all the records of Site Name column display in ascending order$")
	public void verify_that_all_the_records_of_Site_Name_column_display_in_ascending_order() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		new AttributesPageObject(ObjectRepo.driver).verifyAscendingOrder("Site Name");
	}

	@Given("^: Click on Sorting icon of the Site Name column for descending order$")
	public void click_on_Sorting_icon_of_the_Site_Name_column_for_descending_order() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
	}
	
	@Then("^: Verify that all the records of Site Name column display in descending order$")
	public void verify_that_all_the_records_of_Site_Name_column_display_in_descending_order() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}

} //end
