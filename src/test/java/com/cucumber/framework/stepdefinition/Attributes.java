package com.cucumber.framework.stepdefinition;

import java.io.File;

import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class Attributes {

	public ExcelUtils excel;
	private AttributesPageObject attributePageObj;

	public Attributes()
	{
		attributePageObj = new AttributesPageObject(ObjectRepo.driver);
	}

	public String excelData(int rowVal, int colVal) throws Exception {
		excel = new ExcelUtils();

		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Attributes");

		System.err.println();
		return excel.readXLSFile("Attributes", rowVal, colVal);
	}

	@Given("^: Click on Attributes Tile$")
	public void click_on_Attributes_Tile() throws Throwable {
		attributePageObj.clicktoAttributesTile();
	}

	@Then("^: Verify the module name as \"([^\"]*)\"$")
	public void verify_the_module_name_as(String arg1) throws Throwable {
		attributePageObj.verifyAttributesModuleName();
	}

	@Then("^: Verify the default \"([^\"]*)\" values of Attributes module$")
	public void verify_the_default_values_of_Attributes_module(String arg1) throws Throwable {
		attributePageObj.verifyFirstModulValue(excelData(1, 1));
	}

	@Then("^: Click on \"([^\"]*)\" and verify that the drop down is enabled or not$")
	public void click_on_and_verify_that_the_drop_down_is_enabled_or_not(String arg1) throws Throwable {
		attributePageObj.verifyModuledropdownenable();
	}

	@Given("^: Select Module from Attribute listing Screen$")
	public void select_Module_from_Attribute_listing_Screen() throws Throwable {
		String moduleName = excelData(1, 1);
		attributePageObj.selectModuleName(moduleName);
	}

	@Given("^: Enter the data into search box, which user want to search$")
	public void enter_the_data_into_search_box_which_user_want_to_search() throws Throwable {
		System.out.println("Test");
	}

	@Then("^: Verify the search results of Attribute listing page$")
	public void verify_the_search_results_of_Attribute_listing_page() throws Throwable {
		String moduelName = excelData(1, 1);
		String searchItem = excelData(2, 2);
		attributePageObj.verifysearch(searchItem, moduelName);
	}

	/* not used */
	@Given("^: Enter the data into the search box, which user want to search$")
	public void enter_the_data_into_the_search_box_which_user_want_to_search() throws Throwable {

		System.err.println("9");
	}

	@Given("^: Click on remove icon of the search box$")
	public void click_on_remove_icon_of_the_search_box() throws Throwable {
		/* PENDING */
	}

	@Then("^: Verify the searched record is removed$")
	public void verify_the_searched_record_is_removed() throws Throwable {
		/* PENDING */
	}

	@Given("^: Click on Sorting icon of the Attribute column$")
	public void click_on_Sorting_icon_of_the_Attribute_column() throws Throwable {
		String sortingColName = excelData(3, 3);
		attributePageObj.verifyAscendingOrder(sortingColName);
	}

	@Then("^: Verify that all the records of Attribute column display in ascending order$")
	public void verify_that_all_the_records_of_Attribute_column_display_in_ascending_order() throws Throwable {
		attributePageObj.verifyAscendingOrderResult();
	}

	@Then("^: Verify that all the records of Attribute column display in descending order$")
	public void verify_that_all_the_records_of_Attribute_column_display_in_descending_order() throws Throwable {
		/* PENDING */
	}

	@Then("^: Verify the user redirecting to Audit trail of record$")
	public void verify_the_user_redirecting_to_Audit_trail_of_record() throws Throwable {
		attributePageObj.verifyAuditTrailNavigation();
	}

	@Then("^: Verify the Audit trail breadcrumbs as \"([^\"]*)\"$")
	public void verify_the_Audit_trail_breadcrumbs_as(String arg1) throws Throwable {
		attributePageObj.verifyAttributeAuditTrailBreadCrumbs();
	}

	@Given("^: Click on Last option$")
	public void click_on_Last_option() throws Throwable {
		System.err.println("18");
	}

	@Given("^: Click on View link of the first record of Attribute listing screen$")
	public void click_on_View_link_of_the_first_record_of_Attribute_listing_screen() throws Throwable {
		attributePageObj.clicktoAttributesViewLink();
	}

	@Then("^: Verify that popup should appear on the screen with the label \"([^\"]*)\"$")
	public void verify_that_popup_should_appear_on_the_screen_with_the_label(String arg1) throws Throwable {
		attributePageObj.verifyViewPopupLabel();
	}

	@Given("^: Click on the first record of \"([^\"]*)\" column$")
	public void click_on_the_first_record_of_column(String arg1) throws Throwable {
		String moduleName = excelData(4, 1);
		String firstAttributeName = excelData(4, 2);
		attributePageObj.clickToFirstAttribute(moduleName, firstAttributeName);
	}

	@Then("^: Verify the name of the page as \"([^\"]*)\" and the current version$")
	public void verify_the_name_of_the_page_as_and_the_current_version(String arg1) throws Throwable {
		attributePageObj.verifyEditAttribute();
	}

	@Given("^: Update the data$")
	public void update_the_data() throws Throwable {
		System.out.println("UPdate code not implemented");
	}

	@Given("^: Click on save button$")
	public void click_on_save_button() throws Throwable {
		attributePageObj.clickToSaveButton();
	}

	@Then("^: Verify that the updated field should be saved and appear on the screen$")
	public void verify_that_the_updated_field_should_be_saved_and_appear_on_the_screen() throws Throwable {
		attributePageObj.verifyAttributeUpdate();
	}

	@Given("^: Click on cancel button$")
	public void click_on_cancel_button() throws Throwable {
		attributePageObj.clickToCancelButton();
	}

	@Then("^: Verify that page should be redirected back to the Attribute listing screen$")
	public void verify_that_page_should_be_redirected_back_to_the_Attribute_listing_screen() throws Throwable {
		attributePageObj.verifyAttributePageRedirection();
	}

	@Then("^: Verify the cancel button color$")
	public void verify_the_cancel_button_color() throws Throwable {
		attributePageObj.verifyCancelButtonColor();
	}

	@Then("^: Verify the save button color$")
	public void verify_the_save_button_color() throws Throwable {
		attributePageObj.verifySaveButtonColor();
	}

}
