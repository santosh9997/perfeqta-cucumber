package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.helper.PageObject.CommonFunctionPageObject;
import com.cucumber.framework.helper.PageObject.ImportDataPageObject;
import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.SmartAlertsPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;

import cucumber.api.java.ca.I;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ImportData {
	public ExcelUtils excel;

	public ImportDataPageObject importDataObj;

	public ImportData() {
		importDataObj = new ImportDataPageObject(ObjectRepo.driver);
	}

	/*public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Entity Records");
		System.err.println();
		return excel.readXLSFile("Entity Records", rowVal, colVal);
	}*/

	public String excelData(int rowVal, int colVal) throws Exception {
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Import Data");
		System.err.println();
		return excel.readXLSFile("Import Data", rowVal, colVal);
	}

	@Given("^: Click on Import Data Tile$")
	public void click_on_Import_Data_Tile() throws Throwable {
		importDataObj.importDataRecdsTileClick();
	}

	@Then("^: Verify module name as Import Data$")
	public void verify_module_name_as_Import_Data() throws Throwable {
		importDataObj.verifyImportDaLabel();
	}

	@Then("^: Verify that system should display BreadCrumb as \"([^\"]*)\"$")
	public void verify_that_system_should_display_BreadCrumb_as(String arg1) throws Throwable {
		importDataObj.verifyImportDaBreadCrum();
	}

	@Given("^: Click on Manual Import Tile$")
	public void click_on_Manual_Import_Tile() throws Throwable {
		importDataObj.manualImpoDataRecdsTileClick();
	}

	@Then("^: Verify that system should load the Manual Import page Properly$")
	public void verify_that_system_should_load_the_Manual_Import_page_Properly() throws Throwable {
		importDataObj.verifyManualImLbl();
	}

	@Then("^: Verify that system should display BreadCrumb after click on manual import as \"([^\"]*)\"$")
	public void verify_that_system_should_display_BreadCrumb_after_click_on_manual_import_as(String arg1) throws Throwable {
		importDataObj.verifyManImpBreadCrum();
	}

	@Then("^: Verify that system should display label as \"([^\"]*)\" on Manual Import Page$")
	public void verify_that_system_should_display_label_as_on_Manual_Import_Page(String arg1) throws Throwable {
		importDataObj.verifyuploadCsFilebl();
	}

	@Given("^: Select dropdown of Module and Press \"([^\"]*)\" Key$")
	public void select_dropdown_of_Module_and_Press_Key(String arg1) throws Throwable {
		importDataObj.selectDrpDwnModule();
		importDataObj.moduleDrpDwn.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that Color of required validation message for module field should be red$")
	public void verify_that_Color_of_required_validation_message_for_module_field_should_be_red() throws Throwable {
		importDataObj.verifyModuleDrpDwnValMsgColor();
	}

	@Given("^: Select dropdown of Type and Press \"([^\"]*)\" Key$")
	public void select_dropdown_of_Type_and_Press_Key(String arg1) throws Throwable {
		importDataObj.selectDrpDwnType();
		importDataObj.typeDrpDwn.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that system should display required validation message as \"([^\"]*)\" in Type Drop Down$")
	public void verify_that_system_should_display_required_validation_message_as_in_Type_Drop_Down(String arg1) throws Throwable {
		importDataObj.verifyTypeDrpDwnValMsg();
	}

	@Then("^: Verify that Color of required validation message for type field should be red$")
	public void verify_that_Color_of_required_validation_message_for_type_field_should_be_red() throws Throwable {
		importDataObj.verifyTypeDrpDwnValMsgColor();
	}

	@Given("^: Click on Mapping Name textbox$")
	public void click_on_Mapping_Name_textbox() throws Throwable {
		importDataObj.mappingNameClick();
	}

	@Given("^: Press \"([^\"]*)\" Key of Import Data$")
	public void press_Key_of_Import_Data(String arg1) throws Throwable {
		importDataObj.mappingName.sendKeys(Keys.TAB);
	}

	@Then("^: Verify that system should display required validation message as \"([^\"]*)\" in Mapping Name textbox$")
	public void verify_that_system_should_display_required_validation_message_as_in_Mapping_Name_textbox(String arg1) throws Throwable {
		importDataObj.verifyMappingNameValMsg();
	}

	@Then("^: Verify that Color of required validation message for Mapping Name field should be red$")
	public void verify_that_Color_of_required_validation_message_for_Mapping_Name_field_should_be_red() throws Throwable {
		importDataObj.verifyMappingNameValMsgColor();
	}

	@Given("^: Enter Existing Mapping Name into textbox$")
	public void enter_Existing_Mapping_Name_into_textbox() throws Throwable {
		importDataObj.enterDupMapName(excelData(1, 1));
	}

	@Then("^: Verify that system should display unique validation message as \"([^\"]*)\"$")
	public void verify_that_system_should_display_unique_validation_message_as(String arg1) throws Throwable {
		importDataObj.verifyMappingNameValMsgForDup();
	}

	@Then("^: Verify that Color of unique validation message for Mapping Name field should be red$")
	public void verify_that_Color_of_unique_validation_message_for_Mapping_Name_field_should_be_red() throws Throwable {
		importDataObj.verifyMappingNameValMsgColorForDup();
	}

	@Given("^: Enter Mapping Name as \"([^\"]*)\"$")
	public void enter_Mapping_Name_as(String arg1) throws Throwable {
		importDataObj.enterMinSizeMapName(excelData(2, 1));
	}

	@Then("^: Verify that system should display minimum validation message as \"([^\"]*)\"$")
	public void verify_that_system_should_display_minimum_validation_message_as(String arg1) throws Throwable {
		importDataObj.verifyMappingNameValMsgForMinSize();
	}

	@Then("^: Verify that Color of minimum validation message for Mapping Name field should be red$")
	public void verify_that_Color_of_minimum_validation_message_for_Mapping_Name_field_should_be_red() throws Throwable {
		importDataObj.verifyMappingNameValMsgColorForMinSize();
	}

	@Given("^: Enter Mapping Name value more than (\\d+) characters$")
	public void enter_Mapping_Name_value_more_than_characters(int arg1) throws Throwable {
		importDataObj.enterMaxSizeMapName(excelData(3, 1));
	}

	@Then("^: Verify that system should display maximum validation message as \"([^\"]*)\"$")
	public void verify_that_system_should_display_maximum_validation_message_as(String arg1) throws Throwable {
		importDataObj.verifyMappingNameValMsgForMaxSize();
	}

	@Then("^: Verify that Color of maximum validation message for Mapping Name field should be red$")
	public void verify_that_Color_of_maximum_validation_message_for_Mapping_Name_field_should_be_red() throws Throwable {
		importDataObj.verifyMappingNameValMsgColorForMaxSize();
	}

	@Then("^: Verfiy that Color of Upload CSV button should be green$")
	public void verfiy_that_Color_of_Upload_CSV_button_should_be_green() throws Throwable {
		importDataObj.verifyUploadCsvBtnColor();
	}

	@Then("^: Verfiy that Color of Cancel button should be black$")
	public void verfiy_that_Color_of_Cancel_button_should_be_black() throws Throwable {
		importDataObj.verifyCancelManImBtnColor();
	}

	@Given("^: Click on Cancel button in Manual Import$")
	public void click_on_Cancel_button_in_Manual_Import() throws Throwable {
		importDataObj.clickManImCancelBtn();
	}

	@Then("^: Verfiy that system should redirect to the Home Screen of Import Data Page when a user click on Cancel button$")
	public void verfiy_that_system_should_redirect_to_the_Home_Screen_of_Import_Data_Page_when_a_user_click_on_Cancel_button() throws Throwable {
		importDataObj.verifyImportDaLabel();
	}

	@Given("^: Click on View Saved Mapping Tile$")
	public void click_on_View_Saved_Mapping_Tile() throws Throwable {
		importDataObj.viewSavedMapRecdsTileClick();
	}

	@Then("^: Verify that system should load the View Saved Mapping page Properly$")
	public void verify_that_system_should_load_the_View_Saved_Mapping_page_Properly() throws Throwable {
		importDataObj.verifyViewSavMaLbl();
	}
	
	@Given("^: Click on View Audit Trail link of first record from the Audit Trail column$")
	public void click_on_View_Audit_Trail_link_of_first_record_from_the_Audit_Trail_column() throws Throwable {
	    importDataObj.clickViewAuditLink();
	}
	
	@Then("^: Verify that all the records of Mapping Name column display in ascending order when click on sorting icon$")
	public void verify_that_all_the_records_of_Mapping_Name_column_display_in_ascending_order_when_click_on_sorting_icon() throws Throwable {
		new AttributesPageObject(ObjectRepo.driver).verifyAscendingOrder("Mapping Name");
	}
	
	@Given("^: Enter the Mapping Name into search box of View Saved Mapping screen$")
	public void enter_the_Mapping_Name_into_search_box_of_View_Saved_Mapping_screen() throws Throwable {
		String searchItem = excelData(1, 1);
		importDataObj.verifysearch(searchItem);
	}
	
	@Given("^: Enter the data into search box$")
	public void enter_the_data_into_search_box() throws Throwable {
	    importDataObj.viewMapSrchBoxSendkeys(excelData(1, 1));
	}
	
}
