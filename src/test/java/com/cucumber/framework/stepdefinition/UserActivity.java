package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.WebDriver;

import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.helper.PageObject.EntitiesPageObject;
import com.cucumber.framework.helper.PageObject.GeneralSettingsPageObject;
import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.SitesPageObject;
import com.cucumber.framework.helper.PageObject.UserActivityPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import mx4j.log.Log;

public class UserActivity {

	public ExcelUtils excel;
	private Login login;
	private UserActivityPageObject userActPage;


	public UserActivity() {
//		lpage = new LoginPageObject(ObjectRepo.driver);
		 new LoginPageObject(ObjectRepo.driver);
		 userActPage = new UserActivityPageObject(ObjectRepo.driver);
		
	}
	
	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "UserActivity");
		System.err.println();
		return excel.readXLSFile("UserActivity", rowVal, colVal);
			}

	@Given("^: Click on User Activity Tile$")
	public void click_on_User_Activity_Tile() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		userActPage.clickonuserActivityTile();
	}
	

	@Then("^: Verify the module name user activity as \"([^\"]*)\"$")
	public void verify_the_module_name_user_activity_as(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     userActPage.verifyUserActivityTile();
	}
	
	@Then("^: Verify that \"([^\"]*)\" Tab should be displayed bydefault when user click on User Activity Tile$")
	public void verify_that_Tab_should_be_displayed_bydefault_when_user_click_on_User_Activity_Tile(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		userActPage.verifyUserActivityTile();
	}
	@Given("^: Click on Logged in Users tab$")
	public void click_on_Logged_in_Users_tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		userActPage.clickonLoggedin();
	}
	@Given("^: Click on \"([^\"]*)\" Button in loggedin user$")
	public void click_on_Button_in_loggedin_user(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     userActPage.clickonLoggedCancelBtn();
	}

	@Then("^: Verify that system should be redirect to the Administration Screen when a user click on the Cancel button$")
	public void verify_that_system_should_be_redirect_to_the_Administration_Screen_when_a_user_click_on_the_Cancel_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     userActPage.redirectAdministration();
	}

	@Given("^: Enter the Username into search box of User Logging Activity listing screen$")
	public void enter_the_Username_into_search_box_of_User_Logging_Activity_listing_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     userActPage.enterUsernameinSearchBox(excelData(1, 1));
	}
	
	@Then("^: Verify the search result of User Logging Activity listing Screen$")
	public void verify_the_search_result_of_User_Logging_Activity_listing_Screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		new EntitiesPageObject(ObjectRepo.driver).verifysearch(excelData(1, 1));
	}
	@Given("^: Click on Sorting icon of the Username column  ascending order$")
	public void click_on_Sorting_icon_of_the_Username_column_ascending_order() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     userActPage.clickonSortingIcon();
	}
	@Then("^: Verify that all the records of Site Name column display in ascending order  User Logging Activity$")
	public void verify_that_all_the_records_of_Site_Name_column_display_in_ascending_order_User_Logging_Activity() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		new AttributesPageObject(ObjectRepo.driver).verifyAscendingOrder("Username");
	}
	


	@Then("^: Verify that system should be redirect to the Logged in User listing screen when user click on Logged in User Tab$")
	public void verify_that_system_should_be_redirect_to_the_Logged_in_User_listing_screen_when_user_click_on_Logged_in_User_Tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     userActPage.verifyredirctLoggedInUserTile();
	}
	
	@Given("^: Click on Logout link for perticuler Logged in User$")
	public void click_on_Logout_link_for_perticuler_Logged_in_User() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     userActPage.clickonLogout();
	}
	
	@Then("^: Verify that Logged in User should be Logout and record of that User should be removed from the Logged in User listing$")
	public void verify_that_Logged_in_User_should_be_Logout_and_record_of_that_User_should_be_removed_from_the_Logged_in_User_listing() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     userActPage.verifyLoggedOutUser();
	}
	@Given("^: Click on Sorting icon of the Username column for descending order Logging Activity$")
	public void click_on_Sorting_icon_of_the_Username_column_for_descending_order_Logging_Activity() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		 userActPage.clickonSortingIconDscending();
	}

	@Then("^: Verify that all the records of Username column display in descending order$")
	public void verify_that_all_the_records_of_Username_column_display_in_descending_order() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		new AttributesPageObject(ObjectRepo.driver).verifyAscendingOrder("Username");
	}
	
} //end
