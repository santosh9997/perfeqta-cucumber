package com.cucumber.framework.stepdefinition;

import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.BatchScenarioPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class BatchScenario {

	public ExcelUtils excel;
	 
	private BatchScenarioPageObject BatchScenarioPage;

	public BatchScenario() {
		new LoginPageObject(ObjectRepo.driver);
		BatchScenarioPage = new BatchScenarioPageObject(ObjectRepo.driver);
	}


	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Batch Scenario");
		System.err.println();
		return excel.readXLSFile("Batch Scenario", rowVal, colVal);
	}
	@Given("^: Click on Batch Entry Settings Tile$")
	public void click_on_Batch_Entry_Settings_Tile() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		BatchScenarioPage.clickOnBatchEntryTile();
	}


	@Given("^: Select Module$")
	public void select_Module() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		 BatchScenarioPage.selectModule();
	}

	@Given("^: Select App$")
	public void select_App() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		 BatchScenarioPage.selectApp();
	}

	@Given("^: Clicking on Enable App for Batch Entry$")
	public void clicking_on_Enable_App_for_Batch_Entry() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
          BatchScenarioPage.clickonEnableAppBatch();
	}
	
	@Given("^: Checked Copy Site Selections option$")
	public void checked_Copy_Site_Selections_option() throws Throwable {
	     BatchScenarioPage.clickonSiteselectionChkBox();
	}

	@Given("^: Save Batch Entry$")
	public void save_Batch_Entry() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     	BatchScenarioPage.clickonSaveBtn();
	}

	@Given("^: Search App in Apps$")
	public void search_App_in_Apps() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		BatchScenarioPage.enterSearchAppdata();
	}
	 
	 

	@Then("^: Verify Site Selection should be Copied when fill second record$")
	public void verify_Site_Selection_should_be_Copied_when_fill_second_record() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
            BatchScenarioPage.verifySiteCopied();
	}
	@Given("^: Checked Copy Non Key Attributes option and Save Batch Entry$")
	public void checked_Copy_Non_Key_Attributes_option_and_Save_Batch_Entry() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    BatchScenarioPage.clickonCopynonKeyAtrributes();
	}

	@Then("^: Non Key Attributes should be Copied when fill second record\\.$")
	public void non_Key_Attributes_should_be_Copied_when_fill_second_record() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		
	    BatchScenarioPage.verifynoKeyAttributesCopied(excelData(1, 3));
	}
	@Given("^: Get first app name from App builder list$")
	public void get_first_app_name_from_App_builder_list() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    BatchScenarioPage.getFirstAppname();
	}
	//issue
//	@Given("^: Enter valid data to necessary fields$")
//	public void enter_valid_data_to_necessary_fields() throws Throwable {
//	    // Write code here that turns the phrase above into concrete actions
//		String din = excelData(1, 3);
//		String Lotnumber = excelData(1, 4);
//	     BatchScenarioPage.enterValidDataNecessaryFields(din, Lotnumber);
//	     
//	}
	@Given("^: Copy Site name from Site$")
	public void copy_Site_name_from_Site() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    BatchScenarioPage.copytheSiteName();
	}
	@Given("^: Copy nonkey Attributes$")
	public void copy_nonkey_Attributes() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
	}
	@Given("^: Select App which have unchecked key attribute$")
	public void select_App_which_have_unchecked_key_attribute() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    BatchScenarioPage.selectAppwithuncheckedKeyAttribute();
	}
	@Given("^: Search App which have unchecked key attribute$")
	public void search_App_which_have_unchecked_key_attribute() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    BatchScenarioPage.searchAppuncheckedKeyAttribute();
	}

	@Given("^: Click on checkbox of Copy Non Key Entities Attributes option$")
	public void click_on_checkbox_of_Copy_Non_Key_Entities_Attributes_option() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.clickonNonKeyEntities();
	}
	
	@Then("^: Verify Non Key Entities Attributes should be Copied when fill second record\\.$")
	public void verify_Non_Key_Entities_Attributes_should_be_Copied_when_fill_second_record() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.verifyNonKeyEntitiesCopied(excelData(1, 4));
	}
	@Given("^: Click on Start App in Default Batch mode Checkbox$")
	public void click_on_Start_App_in_Default_Batch_mode_Checkbox() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    BatchScenarioPage.clickonStartAppDefaultBatchCheckbox();
	}

	@Then("^: By default records are fill in batch mode\\.$")
	public void by_default_records_are_fill_in_batch_mode() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.verifyDefaultrecordinBatchmode();
	     
	}
	@Given("^: Select YYYY/MM/DD - Auto Increment format$")
	public void select_YYYY_MM_DD_Auto_Increment_format() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.selectYYMMDDautoincrementFormat();
	     
	}

	@Then("^: verify Batch Entry should be display \"([^\"]*)\" Auto Increment format$")
	public void verify_Batch_Entry_should_be_display_Auto_Increment_format(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.verifyBatchEntryYYMMDDformat();
	}

	@Given("^: Select MM/DD/YYYY - Auto Increment format checkbox$")
	public void select_MM_DD_YYYY_Auto_Increment_format_checkbox() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.selectMMDDYYAutoincrementFormat();
	}
	
	@Then("^: verify Batch Entry should be display \"([^\"]*)\" Auto Increment format MM/DD/YYYY$")
	public void verify_Batch_Entry_should_be_display_Auto_Increment_format_MM_DD_YYYY(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.verifyBatchEntryMMDDYYformat();
	}
	@Given("^: Select custom Auto Increment format checkbox$")
	public void select_custom_Auto_Increment_format_checkbox() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    BatchScenarioPage.selectCustomIncrement();
	}

	@Then("^: Batch Entry should be display Custom - Auto Increment format\\.$")
	public void batch_Entry_should_be_display_Custom_Auto_Increment_format() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.verifyCustomIncrementFormat();
	}
	@Given("^: Get value Custom value from Custom Input$")
	public void get_value_Custom_value_from_Custom_Input() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		BatchScenarioPage.getCustomValue();
	}
	@Given("^: Select Auto Increment format checkbox$")
	public void select_Auto_Increment_format_checkbox() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.selectAutoIncrement();
	}

	@Given("^: Get increment value from increment Input$")
	public void get_increment_value_from_increment_Input() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.getIncrementVAlue(excelData(2,5));
	}

	@Then("^: Batch Entry should be display Auto Increment format\\.$")
	public void batch_Entry_should_be_display_Auto_Increment_format() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    BatchScenarioPage.verifyBatchEntryAutoIncrementFormat();
	}
	@Given("^: Select Manual checkbox$")
	public void select_Manual_checkbox() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    BatchScenarioPage.selectManualChkBox();
	}

	@Then("^: Batch Entry should be display Manual format\\.$")
	public void batch_Entry_should_be_display_Manual_format() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.verifyManualFormat(excelData(3, 6));
	}
	@Given("^: Enter value in the Batch Entry ID$")
	public void enter_value_in_the_Batch_Entry_ID() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    BatchScenarioPage.enterValueofBatchEntryID(excelData(3, 6));
	}
	@Then("^: Batch Table should be displayed\\.$")
	public void batch_Table_should_be_displayed() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.verifybatchTableDisplay();
	}
	@Given("^: Click on batch table$")
	public void click_on_batch_table() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.clickonExapndBatchTable();
	}
	@Given("^: Click on Master app radio Button$")
	public void click_on_Master_app_radio_Button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.clickonMasterAppRadioBtn();
	}

	@Then("^: Site Selection should be Copied when fill second record\\.$")
	public void site_Selection_should_be_Copied_when_fill_second_record() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.verifySiteSelectionDataCopied();
	}

	@Given("^: Select Master App From DropDown$")
	public void select_Master_App_From_DropDown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.selectMasterApp();
	}
	
	@Given("^: Search Master App$")
	public void search_Master_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.searchMasterApp();
	}
	@Given("^: Enter Data in First App$")
	public void enter_Data_in_First_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		String din = excelData(1, 3);
		String Lotnumber = excelData(1, 4);
	    BatchScenarioPage.addAllnecessaryFieldFirstApp(din, Lotnumber);
	}

	@Given("^: Save First App Entry$")
	public void save_First_App_Entry() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.saveFirstApp();
	}

	@Given("^: Enter Data in second App$")
	public void enter_Data_in_second_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		String din = excelData(1, 3);
		String Lotnumber = excelData(1, 4);
		BatchScenarioPage.addAllnecessaryFieldSecondApp(din, Lotnumber);
	}

	@Given("^: Save second App Entry$")
	public void save_second_App_Entry() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.saveFirstApp();
	}
	@Given("^: Click on Second App Icon$")
	public void click_on_Second_App_Icon() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.clickonSecondAppIcon();
	}
	@Given("^: Copy Site selection value$")
	public void copy_Site_selection_value() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.copyAllSiteSelectionValues();
	}
	@Then("^: Non Key Attributes should be Copied when fill second record\\. For Master App$")
	public void non_Key_Attributes_should_be_Copied_when_fill_second_record_For_Master_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.verifynonKeyAttributeForMasterApp(excelData(1, 3));
	}
	@Given("^: Click on First App Icon$")
	public void click_on_First_App_Icon() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.clickonFirstAppIcon();
	}
	@Then("^: Non Key Entities Attributes should be Copied when fill second record\\. For Master App$")
	public void non_Key_Entities_Attributes_should_be_Copied_when_fill_second_record_For_Master_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		   BatchScenarioPage.verifyNonKeyEntitiesCopiedMasterApp(excelData(1, 4));
	}

	@Then("^: verify By default records are fill in batch mode for master App$")
	public void verify_By_default_records_are_fill_in_batch_mode_for_master_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.verifyDefaultrecordinBatchmodeforMaster();
	}

	@Then("^: verify Batch Entry should be display YYYY/MM/DD - Auto Increment format\\.Master App$")
	public void verify_Batch_Entry_should_be_display_YYYY_MM_DD_Auto_Increment_format_Master_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.verifyBatchEntryYYMMDDformatMasterApp();
	}

	@Then("^: verify Batch Entry should be display MM/DD/YYYY - Auto Increment format for Master App$")
	public void verify_Batch_Entry_should_be_display_MM_DD_YYYY_Auto_Increment_format_for_Master_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    BatchScenarioPage.verifyBatchEntryMMDDYYformatMasterApp();
	    
	}
	
	@Then("^: verify Batch Entry should be display Custom - Auto Increment format for Master App$")
	public void verify_Batch_Entry_should_be_display_Custom_Auto_Increment_format_for_Master_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   BatchScenarioPage.verifyCustomIncrementFormatMasterApp();
	}
	@Then("^: verify Batch Entry should be display Auto Increment format for Master App$")
	public void verify_Batch_Entry_should_be_display_Auto_Increment_format_for_Master_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    BatchScenarioPage.verifyBatchEntryAutoIncrementFormatMasterApp();
	}
	@Then("^: verify Batch Entry should be display Manual format for Master App$")
	public void verify_Batch_Entry_should_be_display_Manual_format_for_Master_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     BatchScenarioPage.verifyManualFormatMasterApp(excelData(3, 6));
	}

	@Then("^: verify Batch Table should be displayed for Master App$")
	public void verify_Batch_Table_should_be_displayed_for_Master_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    BatchScenarioPage.verifybatchTableDisplayMasterApp();
	}


}
