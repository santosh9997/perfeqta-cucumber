package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.WebDriver;

import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.helper.PageObject.EntitiesPageObject;
import com.cucumber.framework.helper.PageObject.GeneralSettingsPageObject;
import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.PaginationPageObject;
import com.cucumber.framework.helper.PageObject.ShareAppLinkPageObject;
import com.cucumber.framework.helper.PageObject.SitesPageObject;
import com.cucumber.framework.helper.PageObject.UserActivityPageObject;
import com.cucumber.framework.helper.PageObject.GroupSettingsPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import mx4j.log.Log;

public class ShareAppLink {

	public ExcelUtils excel;
	private Login login;
	private ShareAppLinkPageObject shareAppPage;


	public ShareAppLink() {
//		lpage = new LoginPageObject(ObjectRepo.driver);
		 new LoginPageObject(ObjectRepo.driver);
		 shareAppPage = new ShareAppLinkPageObject(ObjectRepo.driver);
		
	}
	
	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "ShareAppLink");
		System.err.println();
		return excel.readXLSFile("ShareAppLink", rowVal, colVal);
			}

	@Given("^: Click on Share App Link Tile$")
	public void click_on_Share_App_Link_Tile() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    shareAppPage.clickShareAppTile();
	}

	@Then("^: Verify the module name as Share App Link$")
	public void verify_the_module_name_as_Share_App_Link() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    shareAppPage.verifyShareAppTile();
	}
	@Then("^: Verify that system should displayed Breadcrumb for Share App Link module as \"([^\"]*)\"$")
	public void verify_that_system_should_displayed_Breadcrumb_for_Share_App_Link_module_as(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    shareAppPage.verifyBreadcrumb();
	}
	@Then("^: Verify total number of entries should be match with pagination of Share App Link page$")
	public void verify_total_number_of_entries_should_be_match_with_pagination_of_Share_App_Link_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		   new SitesPageObject(ObjectRepo.driver).verifytotalEntriesSiteListing();
		  
	}
	@Then("^: Verify that the system should displayed the last page of the listing screen$")
	public void verify_that_the_system_should_displayed_the_last_page_of_the_listing_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		 new PaginationPageObject(ObjectRepo.driver).verifyLastBtnPagination();
	}
	@Then("^: Verify that the system should displayed the first page of the listing screen$")
	public void verify_that_the_system_should_displayed_the_first_page_of_the_listing_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		new PaginationPageObject(ObjectRepo.driver).verifyFirstPagePagination();
	}
	@Given("^: Click on \"([^\"]*)\" link of the first record of the Share App Link page$")
	public void click_on_link_of_the_first_record_of_the_Share_App_Link_page(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		new SitesPageObject(ObjectRepo.driver).clickonViewAuditSitelistingScreen();
	}

	@Then("^: Verify that system should displayed Audit Trail page when user click on \"([^\"]*)\" link of the Share App Link page$")
	public void verify_that_system_should_displayed_Audit_Trail_page_when_user_click_on_link_of_the_Share_App_Link_page(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		new SitesPageObject(ObjectRepo.driver).verifyredirectionofAudittrailScreen();
	}
	@Given("^: Click on Apps$")
	public void click_on_Apps() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
	}

	@Then("^: Verify that all the records of screen should be display in ascending order as per App Name$")
	public void verify_that_all_the_records_of_screen_should_be_display_in_ascending_order_as_per_App_Name() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		new AttributesPageObject(ObjectRepo.driver).verifyAscendingOrder("Apps");
	}
	@Given("^: Enter the data into search box, which user want to search Share app link$")
	public void enter_the_data_into_search_box_which_user_want_to_search_Share_app_link() throws Throwable {
	    shareAppPage.enterSearchBox(excelData(1, 1));	     
	}

	@Then("^: Verify the search results of Share App Link page$")
	public void verify_the_search_results_of_Share_App_Link_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		String moduelName = "";
		String searchItem = excelData(1, 1);
		new AttributesPageObject(ObjectRepo.driver).verifysearch(searchItem, moduelName);
	}
	@Given("^: Click on \"([^\"]*)\" link of the first record from Submitted Records column$")
	public void click_on_link_of_the_first_record_from_Submitted_Records_column(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.clickFirstRecord();
	}

	@Then("^: Verify that system should display Breadcrumb as \"([^\"]*)\"$")
	public void verify_that_system_should_display_Breadcrumb_as(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.verifyBreadcrumbRecods();
	}
	@Given("^: Click on \"([^\"]*)\" button of Share App Link page$")
	public void click_on_button_of_Share_App_Link_page(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    shareAppPage.clickonShareAppBtn();
	}

	@Then("^: Verify that system should redirect to Add/Edit Share App Link page$")
	public void verify_that_system_should_redirect_to_Add_Edit_Share_App_Link_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.verifyShareAppBtnRedirection();
	}
	@Then("^: Verify that system should display label as \"([^\"]*)\" for Add/Edit share App Link page$")
	public void verify_that_system_should_display_label_as_for_Add_Edit_share_App_Link_page(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.VerifyLabelDisplay();
	}
	@Then("^: Verify App Details label should be display as \"([^\"]*)\" for Add/Edit Share App Link page$")
	public void verify_App_Details_label_should_be_display_as_for_Add_Edit_Share_App_Link_page(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.verifyAppdetailsLable();
	}
	@Then("^: Verify Outside User Details label should be display as \"([^\"]*)\" for Add/Edit Share App Link page$")
	public void verify_Outside_User_Details_label_should_be_display_as_for_Add_Edit_Share_App_Link_page(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	  shareAppPage.verifyLableOutsideUserDetail();
	}
	@Given("^: Press \"([^\"]*)\" Key$")
	public void press_Key(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.pressTab();
	}
	@Given("^: Click on Module dropdown Share App$")
	public void click_on_Module_dropdown_Share_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.clickmoduleDropDown();
	}

	@Then("^: Verify required validation message for Module dropdown as \"([^\"]*)\"$")
	public void verify_required_validation_message_for_Module_dropdown_as(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    shareAppPage.verifyModuleDropdownReqValidation();
	}
	@Given("^: Click on App dropdown of Share App Link$")
	public void click_on_App_dropdown_of_Share_App_Link() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.clickAppDropdown();
	}

	@Then("^: Verify required validation message for App dropdown as \"([^\"]*)\"$")
	public void verify_required_validation_message_for_App_dropdown_as(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		shareAppPage.verifyAppDropdownReqValidation();
	}
	@Given("^: Select Module from Module dropdown$")
	public void select_Module_from_Module_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.selectModulefromModuleDropdown();
	}

	@Given("^: Select App from App dropdown$")
	public void select_App_from_App_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.selectAppFromAppDropdown();
	}

	@Then("^: Verify that system should display link as \"([^\"]*)\"$")
	public void verify_that_system_should_display_link_as(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.verifyDisplayedLink();
	}
	
	 

	@Then("^: Verify invalid validation message for Email Address field as \"([^\"]*)\"$")
	public void verify_invalid_validation_message_for_Email_Address_field_as(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.verifyInvalidEmail();
	}
	@Given("^: Enter Email Address value as \"([^\"]*)\"$")
	public void enter_Email_Address_value_as(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.enterEmailAddr(excelData(2, 2));
	}
	@Given("^: Enter a valid Email Address$")
	public void enter_a_valid_Email_Address() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.entervalidEmail(excelData(3, 2));
	}

	@Then("^: Verify system should accept the email address without any validation message$")
	public void verify_system_should_accept_the_email_address_without_any_validation_message() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.verifyValidEmail();
	}
	@Given("^: Enter (\\d+)st valid Email Address and Click on Add button$")
	public void enter_st_valid_Email_Address_and_Click_on_Add_button(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    shareAppPage.enter1stValidEmail(excelData(3, 2));
	}

	@Given("^: Enter (\\d+)nd valid Email Address and Click on Add button$")
	public void enter_nd_valid_Email_Address_and_Click_on_Add_button(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		shareAppPage.enter2ndValidEmail(excelData(4, 2));
	}

	@Then("^: Verify that system should display Added email addresses into list$")
	public void verify_that_system_should_display_Added_email_addresses_into_list() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    shareAppPage.verifyAddedEmail();
	}
	@Given("^: Enter valid Email Address and Click on Add button$")
	public void enter_valid_Email_Address_and_Click_on_Add_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		shareAppPage.enter2ndValidEmail(excelData(4, 2));
	}

	@Given("^: Click on Remove button of Email Address$")
	public void click_on_Remove_button_of_Email_Address() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		shareAppPage.clickonRemoveEmail();
	}

	@Then("^: Verify that system should remove the email address from the list when user click on Remove button$")
	public void verify_that_system_should_remove_the_email_address_from_the_list_when_user_click_on_Remove_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.verifyRemovedEmail();
	}
	@Then("^: Verify that color of information message for email address field should be blue$")
	public void verify_that_color_of_information_message_for_email_address_field_should_be_blue() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    shareAppPage.verifyColorInfoMsgEmail();
	}
	@Given("^: Checked \"([^\"]*)\" checkbox$")
	public void checked_checkbox(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.allowMultipleApp();
	}

	@Then("^: Verify that system should display (\\d+)st option as \"([^\"]*)\" and (\\d+)nd option as \"([^\"]*)\"$")
	public void verify_that_system_should_display_st_option_as_and_nd_option_as(int arg1, String arg2, int arg3, String arg4) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.systemDisplay2options();
	}
	@Given("^: Select \"([^\"]*)\" radio button$")
	public void select_radio_button(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.selectBtnofTotalNumberofApp();
	}

	@Given("^: Click on \"([^\"]*)\" button of Add/Edit Share App Link page$")
	public void click_on_button_of_Add_Edit_Share_App_Link_page(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.clickOnShareAppBtn();
	}

	@Then("^: Verify validation message as \"([^\"]*)\"$")
	public void verify_validation_message_as(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.verifyValidationMsg();
	}
	@Given("^: Enter value into textbox as \"([^\"]*)\"$")
	public void enter_value_into_textbox_as(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    shareAppPage.enter0inTotalnuberofAppSubmission(excelData(5, 3));
	}
	@Then("^: Verify validation message as \"([^\"]*)\" in Number of app submission$")
	public void verify_validation_message_as_in_Number_of_app_submission(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.verifyZeroValidationNuberOfAppSubmission();
	}
	@Given("^: Enter value into textbox as \"([^\"]*)\"  Number of app submission$")
	public void enter_value_into_textbox_as_Number_of_app_submission(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.entermorethan10000inNumberofApp(excelData(6, 3));
	}

	@Then("^: Verify validation message as \"([^\"]*)\"  Number of app submission$")
	public void verify_validation_message_as_Number_of_app_submission(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		shareAppPage.verifyMorethan10000valueinNumberofAppSub();
	}

	@Given("^: Enter valid value into textbox Number of app submission$")
	public void enter_valid_value_into_textbox_Number_of_app_submission() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		shareAppPage.entervalidvalueInNumberofAppSub(excelData(7, 3));
	}
	
	@Then("^: Verify the system should accept the Total Number of App Submissions value without any validation message$")
	public void verify_the_system_should_accept_the_Total_Number_of_App_Submissions_value_without_any_validation_message() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		shareAppPage.verifyValidvalueinNumberofAppSubmission();
	}
	@Given("^: Select \"([^\"]*)\" radio button of Number of Submissions per Individual Email Address$")
	public void select_radio_button_of_Number_of_Submissions_per_Individual_Email_Address(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.clickradioBtnofNumberofSubmissionIndividuals();
	}

	@Then("^: Verify validation message as \"([^\"]*)\" Number of Submissions per Individual Email Address$")
	public void verify_validation_message_as_Number_of_Submissions_per_Individual_Email_Address(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.verifyBlankValidation();
	}
	@Given("^: Enter value into textbox as \"([^\"]*)\" Number of Submissions per Individual Email Address$")
	public void enter_value_into_textbox_as_Number_of_Submissions_per_Individual_Email_Address(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.enterValueZeroinNumberofSubmissionper(excelData(8, 4));
	}
	@Then("^: Verify zero validation message as \"([^\"]*)\" Number of Submissions per Individual Email Address$")
	public void verify_zero_validation_message_as_Number_of_Submissions_per_Individual_Email_Address(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.verifyZeroValidationNumberofSubmission();
	}
	@Given("^: Enter max value into textbox as \"([^\"]*)\" Number of Submissions per Individual Email Address$")
	public void enter_max_value_into_textbox_as_Number_of_Submissions_per_Individual_Email_Address(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    shareAppPage.enterMaxvalueNuberofSubmissionIndividuals(excelData(9, 4));
	}

	@Then("^: Verify max validation message as \"([^\"]*)\"  Number of Submissions per Individual Email Address$")
	public void verify_max_validation_message_as_Number_of_Submissions_per_Individual_Email_Address(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    shareAppPage.verifyMaxValidationInNumberofSubmission();
	}

	@Given("^: Enter valid value into textbox Number of Submissions per Individual Email Address$")
	public void enter_valid_value_into_textbox_Number_of_Submissions_per_Individual_Email_Address() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.entervaliddataNumberofSubmission(excelData(10, 4));
	}
	
	@Then("^: Verify the system should accept the Number of Submissions per Individual Email Address value without any validation message$")
	public void verify_the_system_should_accept_the_Number_of_Submissions_per_Individual_Email_Address_value_without_any_validation_message() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    shareAppPage.verifyValidvalueinNumberofSubmission();
	}

	@Given("^: Checked \"([^\"]*)\" checkbox set link expiry date$")
	public void checked_checkbox_set_link_expiry_date(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   shareAppPage.clickonCheckboxofSetLinkExpiryDate();
	}
	
	@Given("^: Press \"([^\"]*)\" Key set link expiry date$")
	public void press_Key_set_link_expiry_date(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    shareAppPage.pressTabsetLinkExpiry();
	}
	
	@Then("^: Verify that system should display required validation message as \"([^\"]*)\" Expiry date required validation$")
	public void verify_that_system_should_display_required_validation_message_as_Expiry_date_required_validation(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    shareAppPage.verifyExpiryDateValidation();
	}
	@Given("^: Enter Date in invalid format set link expiry date$")
	public void enter_Date_in_invalid_format_set_link_expiry_date() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.enterInvalidDate(excelData(11, 5));
	     
	}

	@Then("^: Verify validation message as \"([^\"]*)\" set link expiry date$")
	public void verify_validation_message_as_set_link_expiry_date(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.verifyValidationMsgSetlinkExpiry();
	}
	@Given("^: Enter Special Message value less than (\\d+) characters$")
	public void enter_Special_Message_value_less_than_characters(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.enterlessthan2InSpecialMsg(excelData(12, 6));
	}

	@Then("^: Verify validation message as \"([^\"]*)\" in Special Message$")
	public void verify_validation_message_as_in_Special_Message(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.verifyMinValidationSpecialMsg();
	}
	@Given("^: Enter valid data into Special Message text box$")
	public void enter_valid_data_into_Special_Message_text_box() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.enterValidSpecialMsg(excelData(13, 6));
	}

	@Then("^: Verify the system should accept the special message value without any validation message$")
	public void verify_the_system_should_accept_the_special_message_value_without_any_validation_message() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.verifyValidDataAcceptSpecialMsg();
	}
	@Then("^: Verify that the newly added Share App should appear on the listing screen and the page should be redirected to listing screen of Share App Link$")
	public void verify_that_the_newly_added_Share_App_should_appear_on_the_listing_screen_and_the_page_should_be_redirected_to_listing_screen_of_Share_App_Link() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		shareAppPage.verifyredirectiontoListingScreen();
	}
	@Given("^: Press \"([^\"]*)\" Key App dropdown$")
	public void press_Key_App_dropdown(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     shareAppPage.pressTabAppDropdown();
	}
	
} //end
