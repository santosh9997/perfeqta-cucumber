package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.cucumber.framework.helper.PageObject.AppBuilderPageObject;
import com.cucumber.framework.helper.PageObject.AppsPageObject;
import com.cucumber.framework.helper.PageObject.CommonFunctionPageObject;
import com.cucumber.framework.helper.PageObject.EntityRecordsPageObject;
import com.cucumber.framework.helper.PageObject.LoginPageObject;
import com.cucumber.framework.helper.PageObject.SearchWindowPageObject;
import com.cucumber.framework.helper.PageObject.SmartAlertsPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Apps {
	public ExcelUtils excel;

	public AppsPageObject appsPagOb;
	public AppBuilderPageObject appBuildPageObj = new AppBuilderPageObject(ObjectRepo.driver);
	public SearchWindowPageObject searchWinPageObj;
	public Apps() {
		appsPagOb = new AppsPageObject(ObjectRepo.driver);
	}

	public String excelData(int rowVal, int colVal) throws Exception {
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Apps");
		System.err.println();
		return excel.readXLSFile("Apps", rowVal, colVal);
	}

	@Given("^: Click on Apps Tile$")
	public void click_on_Apps_Tile() throws Throwable {
		appsPagOb.clickOnAppTile();
	}

	@Given("^: Click on Search App of Apps Module$")
	public void click_on_Search_App_of_Apps_Module() throws Throwable {
		appsPagOb.clickOnSearcgAppTextBox();
	}

	@Given("^: Enter valid app name of App Module search field$")
	public void enter_valid_app_name_of_App_Module_search_field() throws Throwable {
		appsPagOb.enterAppName(appBuildPageObj.getAppTitle);
		//appsPagOb.enterAppName("994874");
	}
	
	@Given("^: Enter valid app name of App Module search field for Search Window$")
	public void enter_valid_app_name_of_App_Module_search_field_for_Search_Window() throws Throwable {
	    appsPagOb.enterAppName(searchWinPageObj.appNameSearchWindow);
	}

	@Given("^: Click on Other Apps tile$")
	public void click_on_Other_Apps_tile() throws Throwable {
		appsPagOb.clickOtherApp();
	}

	@Given("^: Click on Searched app$")
	public void click_on_Searched_app() throws Throwable {
		appsPagOb.clickOnFirstApp();
	}

	@Given("^: Enter valid data to necessary fields$")
	public void enter_valid_data_to_neccasary_fields() throws Throwable {
		String din1 = excelData(1, 1);
		String lotNo = excelData(1, 2);
		String name = excelData(1, 3);
		String phoneNo = excelData(1, 4);
		appsPagOb.enterNecessaryDatatoApp(din1, lotNo, name, phoneNo);
	}

	@Given("^: Click on save button of Apps screen$")
	public void click_on_save_button_of_Apps_screen() throws Throwable {
		appsPagOb.clickOnSaveAndAcceptBtn();
	}

	@Then("^: Verify app data is stored on app$")
	public void verify_app_data_is_stored_on_app() throws Throwable {
		appsPagOb.verifyAppPassedMsg();
	}
	
	@Given("^: Enter invalid data to necessary fields$")
	public void enter_invalid_data_to_neccasary_fields() throws Throwable {
		String din = excelData(2, 1);
		String lotNumber = excelData(2, 2);
		String FirstName = excelData(2, 3);
		String PhoneNo = excelData(2, 4);
	    appsPagOb.enterNecessaryInValDatatoApp(din, lotNumber, FirstName, PhoneNo);
	}

	@Given("^: Enter current user name in Assignee textbox$")
	public void enter_current_user_as_Assignee() throws Throwable {
		appsPagOb.selectCurnUserAsAssignee();
	}
}
