package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.Keys;

import com.cucumber.framework.helper.PageObject.AppBuilderPageObject;
import com.cucumber.framework.helper.PageObject.AttributesPageObject;
import com.cucumber.framework.helper.PageObject.CreateWindowPageObject;
import com.cucumber.framework.helper.PageObject.EntitiesPageObject;
import com.cucumber.framework.helper.PageObject.SearchWindowPageObject;
import com.cucumber.framework.settings.ObjectRepo;
import com.cucumber.framework.utility.Constant;
import com.cucumber.framework.utility.ExcelUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class CreateWindow {

	public ExcelUtils excel;
	public EntitiesPageObject entitiesObj;
	public CreateWindowPageObject createWindowObject;
	public AppBuilderPageObject appPageObj;
	public SearchWindowPageObject searchWinPageObj;

	public CreateWindow() {
		createWindowObject = new CreateWindowPageObject(ObjectRepo.driver);
		//appPageObj = new AppBuilderPageObject(ObjectRepo.driver);
	}

	public String excelData(int rowVal, int colVal) throws Exception {
		System.out.println("excelCalling.........");
		excel = new ExcelUtils();
		excel.setExcelFile(Constant.Path_TestData + Constant.File_TestData, "Create Window");
		System.err.println();
		return excel.readXLSFile("Create Window", rowVal, colVal);
	}

	@Given("^: Click on Qualification Performance Tile of Create window$")
	public void click_on_Qualification_Performance_Tile_of_Create_window() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		createWindowObject.clickQualificationPerformanceTile();
	}

	@Given("^: Click on Create Window Tile$")
	public void click_on_Create_Window_Tile() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickCreateWindowTile();
	}
	
	@Given("^: Enter valid data in Create Window screen for QP$")
	public void enter_valid_data_in_Create_Window_screen_for_QP() throws Throwable {
		createWindowObject.enterValidData(excelData(27, 1), excelData(27, 2), appPageObj.getAppTitle , excelData(27, 5), excelData(27, 6), excelData(27, 7), excelData(27, 9), excelData(27, 10), excelData(27, 12));
	//	createWindowObject.enterValidData(excelData(27, 1), excelData(27, 2), excelData(27, 3) , excelData(27, 5), excelData(27, 6), excelData(27, 7), excelData(27, 9), excelData(27, 10), excelData(27, 12));
	}
	
	@Given("^: Enter valid data in Create Window screen for QP of failure Window$")
	public void enter_valid_data_in_Create_Window_screen_for_QP_of_failure_Window() throws Throwable {
		createWindowObject.enterValidDataForQPFail(excelData(28, 1), excelData(28, 2),searchWinPageObj.appNameSearchWindow, excelData(28, 5), excelData(28, 6), excelData(28, 7), excelData(28, 9), excelData(28, 10), excelData(28, 12));
	}
	
	@Given("^: Enter valid data in Create Window screen for QP of failure Window for pending link should be visible$")
	public void enter_valid_data_in_Create_Window_screen_for_QP_of_failure_Window_for_pending_link_should_be_visible() throws Throwable {
		createWindowObject.enterValidDataForQPFail(excelData(28, 1), excelData(28, 2),appPageObj.getAppTitle, excelData(28, 5), excelData(28, 6), excelData(28, 7), excelData(28, 9), excelData(28, 10), excelData(28, 12));
	}

	@Given("^: Enter valid data in Create Window screen$")
	public void enter_valid_data_in_Create_Window_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterValidData(excelData(1, 1), excelData(1, 2), excelData(1, 3), excelData(1, 5), excelData(1, 6), excelData(1, 7), excelData(1, 9), excelData(1, 10), excelData(1, 12));
	}

	@Given("^: Click on Save Button of Create Window$")
	public void click_on_Save_Button_of_Create_Window() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickSaveBtn();
	}

	@Then("^: Verify that Window should be created$")
	public void verify_that_Window_should_be_created() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    String windowName= excelData(1, 1);
	    createWindowObject.verifySaveFunctionality(windowName);
	}
	
	@Given("^: Click on textbox of Window Name field$")
	public void click_on_textbox_of_Window_Name_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.emptyWindowNameField(excelData(2, 1));
	}

	@Given("^: Press \"([^\"]*)\" key of Create Window$")
	public void press_key_of_Create_Window(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickTabKey();
	}
	
	@Then("^: Verify that Required validation message should be displayed as Window Name is required\\.$")
	public void verify_that_Required_validation_message_should_be_displayed_as_Window_Name_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWindowNameRequiredMsg();
	}
	
	@Then("^: Verify that color of Required validation message should be Red$")
	public void verify_that_color_of_Required_validation_message_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWindowNameRequiredMsgColor();
	}
	
	@Given("^: Enter less than (\\d+) character in the textbox of Window Name$")
	public void enter_less_than_character_in_the_textbox_of_Window_Name(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		createWindowObject.minWindowNameField(excelData(3, 1));
	}

	@Then("^: Verify that Minimum character validation message for Window Name should be displayed as Window Name must be at least (\\d+) characters\\.$")
	public void verify_that_Minimum_character_validation_message_for_Window_Name_should_be_displayed_as_Window_Name_must_be_at_least_characters(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWindowNameMinimumMsg();
	}
	
	@Then("^: Verify that color of Minimum validation message should be Red$")
	public void verify_that_color_of_Minimum_validation_message_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWindowNameMinimumMsgColor();
	}

	@Given("^: Enter more than (\\d+) character in the textbox of Window Name$")
	public void enter_more_than_character_in_the_textbox_of_Window_Name(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.maxWindowNameField(excelData(4, 1));
	}

	@Then("^: Verify that Maximum character validation message for Window Name should be displayed as Window Name cannot be more than (\\d+) characters\\.$")
	public void verify_that_Maximum_character_validation_message_for_Window_Name_should_be_displayed_as_Window_Name_cannot_be_more_than_characters(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWindowNameMaximumMsg();
	}

	@Then("^: Verify that color of Maximum validation message should be Red$")
	public void verify_that_color_of_Maximum_validation_message_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWindowNameMaximumMsgColor();
	}
	
	@Given("^: Enter Window Name which is already added$")
	public void enter_Window_Name_which_is_already_added() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.uniqueWindowNameField(excelData(5, 1));
	}

	@Then("^: Verify that validation message for Duplicate Window Name should be displayed as Window Name must be unique\\.$")
	public void verify_that_validation_message_for_Duplicate_Window_Name_should_be_displayed_as_Window_Name_must_be_unique() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWindowNameUniqueMsg();
	}

	@Then("^: Verify that color of validation message for Duplicate Window Name should be Red$")
	public void verify_that_color_of_validation_message_for_Duplicate_Window_Name_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWindowNameUniqueMsgColor();
	}
	
	@Given("^: Click on textbox of Description field$")
	public void click_on_textbox_of_Description_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickDescriptionTextarea();
	}

	@Given("^: Enter less than (\\d+) character in the Description textbox$")
	public void enter_less_than_character_in_the_Description_textbox(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.minDescriptionTextarea(excelData(3, 1));
	}

	@Then("^: Verify that Minimum character validation message for Window Name should be displayed as Description must be at least (\\d+) characters\\.$")
	public void verify_that_Minimum_character_validation_message_for_Window_Name_should_be_displayed_as_Description_must_be_at_least_characters(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyDescriptionMinimumMsg();
	}

	@Then("^: Verify that color of Minimum validation message for Description should be Red$")
	public void verify_that_color_of_Minimum_validation_message_for_Description_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@Given("^: Enter more than (\\d+) character in the Description textbox$")
	public void enter_more_than_character_in_the_Description_textbox(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.maxDescriptionTextarea(excelData(4, 1));
	}

	@Then("^: Verify that Maximum character validation message for Window Name should be displayed as Description cannot be more than (\\d+) characters\\.$")
	public void verify_that_Maximum_character_validation_message_for_Window_Name_should_be_displayed_as_Description_cannot_be_more_than_characters(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyDescriptionMaximumMsg();
	}

	@Then("^: Verify that color of Maximum validation for Description message should be Red$")
	public void verify_that_color_of_Maximum_validation_for_Description_message_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyDescriptionMaximumMsgColor();
	}
	
	@Then("^: Verify that label should be displayed as App Details$")
	public void verify_that_label_should_be_displayed_as_App_Details() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyAppDetailsHeading();
	}

	@Then("^: Verify that label should be displayed as Test Type and Sampling Plan$")
	public void verify_that_label_should_be_displayed_as_Test_Type_and_Sampling_Plan() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyTestTypeSamplePlanHeading();
	}

	@Then("^: Verify that label should be displayed as Frequency Settings$")
	public void verify_that_label_should_be_displayed_as_Frequency_Settings() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyFrequencySettingHeading();
	}
	
	@Then("^: Verify that Default Selected Value of Module Dropdown should be displayed as -- Please Select --$")
	public void verify_that_Default_Selected_Value_of_Module_Dropdown_should_be_displayed_as_Please_Select() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		createWindowObject.verifyModuleDefaultValue();
	}

	@Then("^: Verify that Default Selected Value of App Dropdown should be displayed as -- Please Select --$")
	public void verify_that_Default_Selected_Value_of_App_Dropdown_should_be_displayed_as_Please_Select() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyAppDefaultValue();
	}
	
	@Given("^: Click on Dropdown of Module$")
	public void click_on_Dropdown_of_Module() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickModuleDrpDwn();
	}
	
	@Then("^: Verify that all the values of Module should be displayed as \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\"$")
	public void verify_that_all_the_values_of_Module_should_be_displayed_as(String arg1, String arg2, String arg3, String arg4) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		createWindowObject.verifyclickModuleDrpDwn();
	}

	@Then("^: Verify that Required validation message should be displayed as Module is required\\.$")
	public void verify_that_Required_validation_message_should_be_displayed_as_Module_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyModuledrpDwnRequiredMsg();
	}

	@Given("^: Click on dropdown of Module$")
	public void click_on_dropdown_of_Module() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		createWindowObject.clickModuleDrpDwn();
	}

	@Then("^: Verify that color of Required validation message for Module should be Red$")
	public void verify_that_color_of_Required_validation_message_for_Module_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyModuledrpDwnRequiredMsgColor();
	}

	@Given("^: Click on Dropdown of App$")
	public void click_on_Dropdown_of_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickAppDrpDwn();
	}

	@Then("^: Verify that Required validation message should be displayed as App is required\\.$")
	public void verify_that_Required_validation_message_should_be_displayed_as_App_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyAppDrpDwnRequiredMsg();
	}

	@Given("^: Click on dropdown of App$")
	public void click_on_dropdown_of_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		createWindowObject.clickAppDrpDwn();
	}

	@Then("^: Verify that color of Required validation message for App should be Red$")
	public void verify_that_color_of_Required_validation_message_for_App_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyAppDrpDwnRequiredMsgColor();
	}
	
	@Given("^: Select value into the dropdown of Module$")
	public void select_value_into_the_dropdown_of_Module() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectModuleDrpDwn();
	}

	@Given("^: Select value into the dropdown of App$")
	public void select_value_into_the_dropdown_of_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectAppDrpDwn(excelData(25, 2));
	}
	
	@Given("^: Click on Dropdown of Attributes/Entity Key Attributes$")
	public void click_on_Dropdown_of_Attributes_Entity_Key_Attributes() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickAttributeEntityDrpDwn(excelData(18, 2));
	}

	@Given("^: Select value from Attributes/Entity dropdown$")
	public void select_value_from_Attributes_Entity_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectAttributeEntityDrpDwn(excelData(19, 2));
	}

	@Then("^: Verify that Required validation message should be displayed as Attributes/Entity Key Attributes is required\\.$")
	public void verify_that_Required_validation_message_should_be_displayed_as_Attributes_Entity_Key_Attributes_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyAttributeEntityDrpDwnRequiredMsg();
	}

	@Then("^: Verify that color of Required validation message for Attributes/Entity should be Red$")
	public void verify_that_color_of_Required_validation_message_for_Attributes_Entity_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyAttributeEntityDrpDwnRequiredMsgColor();
	}
	
	@Then("^: Verify that input dropdown should be disable when Attributes/Entity Key Attributes is not selected$")
	public void verify_that_input_dropdown_should_be_disable_when_Attributes_Entity_Key_Attributes_is_not_selected() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyAttributeEntityValueTextfield();
	}
	
	@Given("^: Click on Add button in Create Window screen$")
	public void click_on_Add_button_in_Create_Window_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickAddIconAttributeEntity();
	}

	@Then("^: Verify that sytem should allow to add multiple Attributes/Entity Key Attributes when user click on Add button$")
	public void verify_that_sytem_should_allow_to_add_multiple_Attributes_Entity_Key_Attributes_when_user_click_on_Add_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMultipleAddAttributeEntity();
	}

	@Given("^: Click on Remove Button in Create Window screen$")
	public void click_on_Remove_Button_in_Create_Window_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickRemoveIconAttributeEntity();
	}

	@Then("^: Verify that sytem should allow to remove value of Attributes/Entity Key Attributes when user click on Remove button$")
	public void verify_that_sytem_should_allow_to_remove_value_of_Attributes_Entity_Key_Attributes_when_user_click_on_Remove_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyRemoveAttributeEntity();
	}
	
	@Then("^: Verify that radio button of Procedure should be selected by default in Create Window screen$")
	public void verify_that_radio_button_of_Procedure_should_be_selected_by_default_in_Create_Window_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyProcedureRadioBtnByDefault();
	}
	
	@Given("^: Click on radio button of Question in Test Type$")
	public void click_on_radio_button_of_Question_in_Test_Type() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickQuestionRadioBtn();
	}

	@Given("^: Click on radio button of Procedure in Test Type$")
	public void click_on_radio_button_of_Procedure_in_Test_Type() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickProcedureRadioBtn();
	}

	@Then("^: Verify that at time only one radio button should be selectable when user select radio button$")
	public void verify_that_at_time_only_one_radio_button_should_be_selectable_when_user_select_radio_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyOneRadioBtnIsSelected();
	}
	
	@Given("^: Click on dropdown of Procedure Test Type$")
	public void click_on_dropdown_of_Procedure_Test_Type() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickProcedureTestTypeDrpDwn();
	}

	@Given("^: Select value into the dropdown of Apps$")
	public void select_value_into_the_dropdown_of_Apps() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectAppDrpDwn(excelData(25, 2));
	}
	
	@Then("^: Verify that Required validation message for dropdown of Procedure Test Type should be displayed as Procedure is required\\.$")
	public void verify_that_Required_validation_message_for_dropdown_of_Procedure_Test_Type_should_be_displayed_as_Procedure_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyProcedureTestTypeDrpDwnRequiredMsg();
	}

	@Then("^: Verify that the color of Required validation message for dropdown of Procedure Test Type should be Red$")
	public void verify_that_the_color_of_Required_validation_message_for_dropdown_of_Procedure_Test_Type_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyProcedureTestTypeDrpDwnRequiredMsgColor();
	}

	@Given("^: Click on dropdown of Question Test Type$")
	public void click_on_dropdown_of_Question_Test_Type() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectQuestionDrpDwn();
	}

	@Then("^: Verify that Required validation message for dropdown of Question Test Type should be displayed as Question is required\\.$")
	public void verify_that_Required_validation_message_for_dropdown_of_Question_Test_Type_should_be_displayed_as_Procedure_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyQuestionTestTypeDrpDwnRequiredMsg();
	}

	@Then("^: Verify that the color of Required validation message for dropdown of Question Test Type should be Red$")
	public void verify_that_the_color_of_Required_validation_message_for_dropdown_of_Question_Test_Type_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyQuestionTestTypeDrpDwnRequiredMsgColor();
	}
	
	@Given("^: Click on dropdown of Window Type field$")
	public void click_on_dropdown_of_Window_Type_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectWindowTypeDrpDwn();
	}

	@Then("^: Verify that Required validation message for Window Type dropdown should be displayed as Window Type is required\\.$")
	public void verify_that_Required_validation_message_for_Window_Type_dropdown_should_be_displayed_as_Window_Type_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWindowTypeDrpDwnRequiredMsg();
	}

	@Then("^: Verify that color of Required validation message for Window Type should be Red$")
	public void verify_that_color_of_Required_validation_message_for_Window_Type_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWindowTypeDrpDwnRequiredMsgColor();
	}
	
	@Then("^: Verify that system should displayed all the values in dropdown of Window Type when user click on dropdown of Window Type field$")
	public void verify_that_system_should_displayed_all_the_values_in_dropdown_of_Window_Type_when_user_click_on_dropdown_of_Window_Type_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWindowTypeDrpDwnOptions();
	}

	@Given("^: Select First record in the dropdown of Window Type field$")
	public void select_First_record_in_the_dropdown_of_Window_Type_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectWindowTypeDrpDwnFirstOption();
	}

	@Given("^: Click on dropdown of Window Plan$")
	public void click_on_dropdown_of_Window_Plan() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickWindowPlanDrpDwnFirst();
	}

	@Given("^: Select first record in the dropdown of Window Plan$")
	public void select_first_record_in_the_dropdown_of_Window_Plan() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectWindowPlanDrpDwnFirstOption();
	}

	@Given("^: Select \"([^\"]*)\" in the dropdown of Window Plan$")
	public void select_in_the_dropdown_of_Window_Plan(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectWindowPlanDrpDwnDefaultOption();
	}

	@Then("^:  Verify that Required validation message for Window Plan Dropdown should be displayed as Window Plan is required\\.$")
	public void verify_that_Required_validation_message_for_Window_Plan_Dropdown_should_be_displayed_as_Window_Plan_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWindowPlanDrpDwnRequiredMsg();
	}
	
	@Then("^: Verify that color of Required validation message for Window Plan should be Red$")
	public void verify_that_color_of_Required_validation_message_for_Window_Plan_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWindowPlanDrpDwnRequiredMsgColor();
	}

	@Then("^: Verify that All types of Window Plan dropdown should be displayed as \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\"$")
	public void verify_that_All_types_of_Window_Plan_dropdown_should_be_displayed_as(String arg1, String arg2, String arg3, String arg4) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWindowPlanDrpDwnOptions();
	}

	@Given("^: Select Binomial Distribution type in the dropdown of Window Type field$")
	public void select_Binomial_Distribution_type_in_the_dropdown_of_Window_Type_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectBinomialWindowPlanDrpDwnOption(excelData(26, 3));
	}

	@Then("^: Verify that system should displayed all related fields when user select Binomial Distribution window type$")
	public void verify_that_system_should_displayed_all_related_fields_when_user_select_Binomial_Distribution_window_type() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyAfterSelectBinomialOptionDisplayOtherDrpDwn();
	}
	
	@Given("^: Select Hypergeometric Distribution type in the dropdown of Window Type field$")
	public void select_Hypergeometric_Distribution_type_in_the_dropdown_of_Window_Type_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectHypergeometricWindowPlanDrpDwnOption(excelData(27, 3));
	}

	@Then("^: Verify that system should displayed all related fields when user select Hypergeometric Distribution window type$")
	public void verify_that_system_should_displayed_all_related_fields_when_user_select_Hypergeometric_Distribution_window_type() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyAfterSelectHypergeometricOptionDisplayOtherDrpDwn();
	}

	@Given("^: Select Customized Windows type in the dropdown of Window Type field$")
	public void select_Customized_Windows_type_in_the_dropdown_of_Window_Type_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectCustomizedWindowPlanDrpDwnOption(excelData(28, 3));
	}

	@Then("^: Verify that system should displayed all related fields when user select Customized Windows window type$")
	public void verify_that_system_should_displayed_all_related_fields_when_user_select_Customized_Windows_window_type() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyAfterSelectCustomizedOptionDisplayOtherDrpDwn();
	}
	
	@Then("^: Verify that Sample Size dropdown should be disable by default in Create Window screen$")
	public void verify_that_Sample_Size_dropdown_should_be_disable_by_default_in_Create_Window_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifySampleSizeDrpDwnIsDisabled();
	}

	@Given("^: Click on dropdown of Sample Size$")
	public void click_on_dropdown_of_Sample_Size() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickSampleSizeDrpDwn();
	}

	@Then("^: Verify Required validation message for Sample Size dropdown should be displayed as Sample Size is required\\.$")
	public void verify_Required_validation_message_for_Sample_Size_dropdown_should_be_displayed_as_Sample_Size_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifySampleSizeDrpDwnRequiredMsg();
	}

	@Then("^: Verify that color of Required validation message for Sample Size should be Red$")
	public void verify_that_color_of_Required_validation_message_for_Sample_Size_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifySampleSizeDrpDwnRequiredMsgColor();
	}
	
	@Then("^: Verify that Maximum Allowed Process Failure in Period dropdown is disable by default in Create Window screen$")
	public void verify_that_Maximum_Allowed_Process_Failure_in_Period_dropdown_is_disable_by_default_in_Create_Window_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMaximumAllowedProcessFailurePeriodIsDisabled();
	}
	
	@Given("^: Select record in the dropdown of Window Plan$")
	public void select_record_in_the_dropdown_of_Window_Plan() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectWindowPlanDrpDwnValue();
	}

	@Given("^: Select record in the dropdown of Sample Size field$")
	public void select_record_in_the_dropdown_of_Sample_Size_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectSampleSizeDrpDwnValue();
	}

	@Given("^: Click on checkbox of  Allow Second Stage field$")
	public void click_on_checkbox_of_Allow_Second_Stage_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.checkMaxAllowedProcessFailureIsDisabled();
	}

	@Then("^: Verify that checkbox of Allow Second Stage field should be clickable when user checked or unchecked the checkbox$")
	public void verify_that_checkbox_of_Allow_Second_Stage_field_should_be_clickable_when_user_checked_or_unchecked_the_checkbox() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMaxAllowedProcessFailureClickable();
	}

	@Given("^: Click on Attributes/Entity key Attributes dropdown$")
	public void click_on_Attributes_Entity_key_Attributes_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickAttributeEntityDrpDwn(excelData(18, 2));
	}

	@Given("^: select Collection Date into the dropdown$")
	public void select_Collection_Date_into_the_dropdown() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //createWindowObject.selectCollectedDateAttributeEntityDrpDwn(excelData(29, 4));
	}

	@Then("^: Verify that Required validation message for Attribute dropdown should be displayed as Collection Date is required\\.$")
	public void verify_that_Required_validation_message_for_Attribute_dropdown_should_be_displayed_as_Collection_Date_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyCollectionRequiredMsg();
	}

	@Then("^: Verify that the color of Required validation message for Attribute dropdown should be Red$")
	public void verify_that_the_color_of_Required_validation_message_for_Attribute_dropdown_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyCollectionRequiredMsgColor();
	}
	
	@Given("^: Click on dropdown of Month field$")
	public void click_on_dropdown_of_Month_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickMonthDrpDwn();
	}

	@Then("^: Verify that values of dropdown of Month field should be displayed as \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\" \\+ \"([^\"]*)\"$")
	public void verify_that_values_of_dropdown_of_Month_field_should_be_displayed_as(String arg1, String arg2, String arg3, String arg4, String arg5, String arg6, String arg7, String arg8, String arg9, String arg10, String arg11, String arg12, String arg13) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMonthDrpDwnValues();
	}

	@Then("^: Verify Required validation message for Month dropdown should be displayed as Month is required\\.$")
	public void verify_Required_validation_message_for_Month_dropdown_should_be_displayed_as_Month_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMonthDrpDwnRequiredMsg();
	}

	@Then("^: Verify that color of Required validation message for Month should be Red$")
	public void verify_that_color_of_Required_validation_message_for_Month_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMonthDrpDwnRequiredMsgColor();
	}
	
	@Given("^: Click on dropdown of Year field$")
	public void click_on_dropdown_of_Year_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickYearDrpDwn();
	}

	@Then("^: Verify that values of dropdown of Year field should be displayed as \"([^\"]*)\"$")
	public void verify_that_values_of_dropdown_of_Year_field_should_be_displayed_as(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyYearDrpDwnDefaultValues();
	}

	@Then("^: Verify Required validation message for Year dropdown should be displayed as Year is required\\.$")
	public void verify_Required_validation_message_for_Year_dropdown_should_be_displayed_as_Year_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyYearDrpDwnRequiredMsg();
	}

	@Then("^: Verify that color of Required validation message for Year should be Red$")
	public void verify_that_color_of_Required_validation_message_for_Year_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyYearDrpDwnRequiredMsgColor();
	}
	
	@Given("^: Click on dropdown of Month field in Month-Year Section$")
	public void click_on_dropdown_of_Month_field_in_Month_Year_Section() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickMonthDrpDwn();
	}

	@Given("^: Select Current Month$")
	public void select_Current_Month() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectCurrentMonthInDrpDwn(excelData(6, 9));
	}

	@Given("^: Click on dropdown of Year field in Month-Year Section$")
	public void click_on_dropdown_of_Year_field_in_Month_Year_Section() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickYearDrpDwn();
	}

	@Given("^: Select Current Year$")
	public void select_Current_Year() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectCurrentYearInDrpDwn(excelData(7, 10));
	}

	@Then("^: Verify that system should displayed information message as Start date will be Today for the current window\\.$")
	public void verify_that_system_should_displayed_information_message_as_Start_date_will_be_Today_for_the_current_window() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMonthYearInformationMsg();
	}
	
	@Then("^: Verify that color of Month-Year information message of ?start from today? should be Red$")
	public void verify_that_color_of_Month_Year_information_message_of_start_from_today_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		createWindowObject.verifyMonthYearInformationMsgColor();
	}

	@Given("^: Select Month$")
	public void select_Month() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectNextMonthInDrpDwn(excelData(8, 9));
	}

	@Then("^: Verify that system should displayed information message as Start date will be (\\d+)st Day of the month for the current window\\.$")
	public void verify_that_system_should_displayed_information_message_as_Start_date_will_be_st_Day_of_the_month_for_the_current_window(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifySecondMonthYearInformationMsg();
	}
	
	@Then("^: Verify that color of Month-Year information message of  ?start from (\\d+)st day?  should be blue$")
	public void verify_that_color_of_Month_Year_information_message_of_start_from_st_day_should_be_blue(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		createWindowObject.verifyMonthYearInformationMsgColor();
	}
	
	@Given("^: Click on checkbox of  Repeat Window field$")
	public void click_on_checkbox_of_Repeat_Window_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickRepeatWindowCheckbox();
	}

	@Then("^: Verify that Repeat Window checkbox field should be clickable when user checked or unchecked the checkbox$")
	public void verify_that_Repeat_Window_checkbox_field_should_be_clickable_when_user_checked_or_unchecked_the_checkbox() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyClickableRepeatWindowCheckbox();
	}

	@Given("^: Move the mouse over the information icon$")
	public void move_the_mouse_over_the_information_icon() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.mouseHoverInformation();
	}

	@Then("^: Verify that Tooltip message of Repeat Window checkbox should be displayed as Select this check box to automatically create the window for the next month at the end of the current month\\.$")
	public void verify_that_Tooltip_message_of_Repeat_Window_checkbox_should_be_displayed_as_Select_this_check_box_to_automatically_create_the_window_for_the_next_month_at_the_end_of_the_current_month() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMouseHoverInformationMsg();
	}
	
	@Then("^: Verify that radio button of All option should be selected by default$")
	public void verify_that_radio_button_of_All_option_should_be_selected_by_default() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifySiteAllIsSelectedByDrfault();
	}

	@Given("^: Click on radio button of All option in Site selection$")
	public void click_on_radio_button_of_All_option_in_Site_selection() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickSitesAllRadioBtn();
	}

	@Given("^: Click on radio button of Selection option in Site selection$")
	public void click_on_radio_button_of_Selection_option_in_Site_selection() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickSitesSelectionRadioBtn();
	}

	@Then("^: Verify that Required validation message should be desplayed as At least one site is required from each level\\.$")
	public void verify_that_Required_validation_message_should_be_desplayed_as_At_least_one_site_is_required_from_each_level() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifySiteSelectionRequiredMsg();
	}
	
	@Then("^: Verify that color of Required validation message for Site Radio should be Red$")
	public void verify_that_color_of_Required_validation_message_for_Site_Radio_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifySiteSelectionRequiredMsgColor();
	}

	@Given("^: Click on Module dropdown of Create Window$")
	public void select_value_from_dropdown_of_Module() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectValueInModuleDrpDwn(excelData(9, 2));
	}

	@Given("^: Click on App dropdown of Create Window$")
	public void select_value_from_dropdown_of_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectValueInAppDrpDwn(excelData(9, 3));
	}

	@Given("^: Select check box of All option of Region section$")
	public void select_check_box_of_All_option_of_Region_section() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickAllCheckboxOfSelection();
	}

	@Then("^: Verify that Site required validation message should be desplayed as At least one site from Department is required for Regin MD Anderson Cancer Center - Updated\\.$")
	public void verify_that_Site_required_validation_message_should_be_desplayed_as_At_least_one_site_from_Department_is_required_for_Regin_MD_Anderson_Cancer_Center_Updated() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyAllSelectionOfRegion();
	}

	@Then("^: Verify that color of Site required message should be Red$")
	public void verify_that_color_of_Site_required_message_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		createWindowObject.verifyAllSelectionOfRegionColor();
	}
	
	@Given("^: Select First checkbox of Department section$")
	public void select_First_checkbox_of_Department_section() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickAllCheckboxOfDepartment();
	}

	@Then("^: Verify that Required validation message should be desplayed as At least one site from Site is required for Department Donor Operations\\.$")
	public void verify_that_Required_validation_message_should_be_desplayed_as_At_least_one_site_from_Site_is_required_for_Department_Donor_Operations() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyAllSelectionOfDepatment();
	}

	@Given("^: Select First checkbox of Site section$")
	public void select_First_checkbox_of_Site_section() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickAllCheckboxOfSites();
	}

	@Then("^: Verify that system should not displayed any validation message when user select checkbox of all the sections$")
	public void verify_that_system_should_not_displayed_any_validation_message_when_user_select_checkbox_of_all_the_sections() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyAllSelectionOfSitesNoMsg();
	}

	@Given("^: Select Hypergeometric Distribution option$")
	public void select_Hypergeometric_Distribution_option() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		createWindowObject.selectHypergeometricOption();
	}

	@Given("^: Click on Population Size textbox$")
	public void click_on_Population_Size_textbox() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickPopulationTextbox();
	}

	@Then("^: Verify that Required validation message for Population Size textbox should be displayed as Population Size is required\\.$")
	public void verify_that_Required_validation_message_for_Population_Size_textbox_should_be_displayed_as_Population_Size_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyPopulationRequiredMsg();
	}

	@Then("^: Verify that color of Required validation message for Population Size textbox should be Red$")
	public void verify_that_color_of_Required_validation_message_for_Population_Size_textbox_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyPopulationRequiredMsgColor();
	}
	
	@Given("^: Enter numeric value (\\d+) in the textbox of Population Size$")
	public void enter_numeric_value_in_the_textbox_of_Population_Size(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterPopulationMinimumTextbox(excelData(10, 13));
	}

	@Then("^: Verify that Minimum validation message for Population Size textbox should be displayed as The Population Size should be greater than (\\d+)\\.$")
	public void verify_that_Minimum_validation_message_for_Population_Size_textbox_should_be_displayed_as_The_Population_Size_should_be_greater_than(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyPopulationMinimumMsg();
	}

	@Then("^: Verify that color of Minimum validation message for Population Size textbox should be Red$")
	public void verify_that_color_of_Minimum_validation_message_for_Population_Size_textbox_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyPopulationMinimumMsgColor();
	}
	
	@Given("^: Enter numeric value which is greater than (\\d+)$")
	public void enter_numeric_value_which_is_greater_than(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterPopulationMaximumTextbox(excelData(11, 13));
	}

	@Then("^: Verify that Maximum validation message for Population Size textbox should be displayed as Population Size must not be more than (\\d+)\\.$")
	public void verify_that_Maximum_validation_message_for_Population_Size_textbox_should_be_displayed_as_Population_Size_must_not_be_more_than(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyPopulationMaximumMsg();
	}

	@Then("^: Verify that color of Maximum validation message for Population Size textbox should be Red$")
	public void verify_that_color_of_Maximum_validation_message_for_Population_Size_textbox_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyPopulationMaximumMsgColor();
	}
	
	@Given("^: Enter valid data in textbox of Population Size$")
	public void enter_valid_data_in_textbox_of_Population_Size() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterPopulationValidTextbox(excelData(12, 13));
	}

	@Then("^: Verify that system should not displayed any validation message when user enter valid data in textbox of Population Size$")
	public void verify_that_system_should_not_displayed_any_validation_message_when_user_enter_valid_data_in_textbox_of_Population_Size() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyPopulationNoValidation();
	}

	@Given("^: Select Customized Windows in the dropdown of Window Type field$")
	public void select_Customized_Windows_in_the_dropdown_of_Window_Type_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.selectCustomizedInWindowTypeDrpDwn();
	}

	@Given("^: Click on textbox of Sample Size field$")
	public void click_on_textbox_of_Sample_Size_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickSampleSizeTextbox();
	}

	@Given("^: Enter numeric value (\\d+) in the textbox of Sample Size$")
	public void enter_numeric_value_in_the_textbox_of_Sample_Size(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterSampleSizeMinimumValue(excelData(13, 7));
	}

	@Then("^: Verify that Minimum validation message for Sample Size textbox should be displayed as Sample Size should be at least (\\d+)\\.$")
	public void verify_that_Minimum_validation_message_for_Sample_Size_textbox_should_be_displayed_as_Sample_Size_should_be_at_least(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifySampleSizeMinimumMsg();
	}

	@Then("^: Verify that color of Minimum validation message for Sample Size textbox should be Red$")
	public void verify_that_color_of_Minimum_validation_message_for_Sample_Size_textbox_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifySampleSizeMinimumMsgColor();
	}
	
	@Then("^: Verify that Maximum validation message for Sample Size textbox should be displayed as Sample Size must not be more than (\\d+)\\.$")
	public void verify_that_Maximum_validation_message_for_Sample_Size_textbox_should_be_displayed_as_Sample_Size_must_not_be_more_than(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifySampleSizeMaximumMsg();
	}

	@Then("^: Verify that color of Maximum validation message for Sample Size textbox should be Red$")
	public void verify_that_color_of_Maximum_validation_message_for_Sample_Size_textbox_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifySampleSizeMaximumMsgColor();
	}
	
	@Given("^: Enter numeric value in Sample Size which is greater than (\\d+)$")
	public void enter_numeric_value_in_Sample_Size_which_is_greater_than(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterSampleSizeMaximumValue(excelData(15, 7));
	}

	@Given("^: Enter valid data in textbox of Sample Size$")
	public void enter_valid_data_in_textbox_of_Sample_Size() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterSampleSizeValidValue(excelData(14, 7));
	}

	@Then("^: Verify that system should not displayed any validation message when user enter valid data in textbox of Sample Size$")
	public void verify_that_system_should_not_displayed_any_validation_message_when_user_enter_valid_data_in_textbox_of_Sample_Size() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifySampleSizeNoValidation();
	}
	
	@Given("^: Click on textbox of Maximum Allowed Process Failure in Period$")
	public void click_on_textbox_of_Maximum_Allowed_Process_Failure_in_Period() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickMaximumAllowedProcessFailureTextfield();
	}

	@Then("^: Verify that Required validation message for Maximum Allowed Process Failure in Period textbox should be displayed as Maximum Allowed Process Failure in Period is required\\.$")
	public void verify_that_Required_validation_message_for_Maximum_Allowed_Process_Failure_in_Period_textbox_should_be_displayed_as_Maximum_Allowed_Process_Failure_in_Period_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMaximumAllowedProcessFailureRequiredMsg();
	}

	@Then("^: Verify that the colorof  Required validation message for Maximum Allowed Process Failure in Period textbox should be Red$")
	public void verify_that_the_colorof_Required_validation_message_for_Maximum_Allowed_Process_Failure_in_Period_textbox_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMaximumAllowedProcessFailureRequiredMsgColor();
	}
	
	@Given("^: Enter numeric value (\\d+) in the textbox of Maximum Allowed Process Failure in Period$")
	public void enter_numeric_value_in_the_textbox_of_Maximum_Allowed_Process_Failure_in_Period(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterMaximumAllowedProcessFailureMinimum(excelData(16, 14));
	}

	@Then("^: Verify that Minimum validation message for Population Size textbox should be displayed as Maximum Allowed Process Failure in Period should be at least (\\d+)\\.$")
	public void verify_that_Minimum_validation_message_for_Population_Size_textbox_should_be_displayed_as_Maximum_Allowed_Process_Failure_in_Period_should_be_at_least(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMaximumAllowedProcessFailureMinimumMsg();
	}

	@Then("^: Verify that the color of Minimum validation message for Population Size textbox should be Red$")
	public void verify_that_the_color_of_Minimum_validation_message_for_Population_Size_textbox_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMaximumAllowedProcessFailureMinimumMsgColor();
	}

	@Given("^: Enter numeric value which is greater than (\\d+) in the textbox of Maximum Allowed Process Failure in Period$")
	public void enter_numeric_value_which_is_greater_than_in_the_textbox_of_Maximum_Allowed_Process_Failure_in_Period(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterMaximumAllowedProcessFailureMaximum(excelData(17, 14));
	}

	@Then("^: Verify that Maximum validation message for Population Size textbox should be displayed as Maximum Allowed Process Failure in Period must not be more than (\\d+)\\.$")
	public void verify_that_Maximum_validation_message_for_Population_Size_textbox_should_be_displayed_as_Maximum_Allowed_Process_Failure_in_Period_must_not_be_more_than(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMaximumAllowedProcessFailureMaximumMsg();
	}

	@Then("^: Verify that the color of Maximum validation message for Population Size textbox should be Red$")
	public void verify_that_the_color_of_Maximum_validation_message_for_Population_Size_textbox_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMaximumAllowedProcessFailureMaximumMsgColor();
	}
	
	@Given("^: Enter numeric value in the textbox of Sample Size$")
	public void enter_numeric_value_in_the_textbox_of_Sample_Size() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterNumericValueSampleSizeTextfield(excelData(18, 7));
	}

	@Given("^: Enter numeric value which is greater than value of Sample Size$")
	public void enter_numeric_value_which_is_greater_than_value_of_Sample_Size() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterNumericValueMaximumAllowedProcessFailureTextfield(excelData(19, 14));
	}

	@Then("^: Verify that Validation message for invalid data of Maximum Allowed Process Failure in Period textbox should be displayed as Maximum Allowed Process Failure in Period must not be more than Sample Size\\.$")
	public void verify_that_Validation_message_for_invalid_data_of_Maximum_Allowed_Process_Failure_in_Period_textbox_should_be_displayed_as_Maximum_Allowed_Process_Failure_in_Period_must_not_be_more_than_Sample_Size() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifySampleSizeMinimumAllowedProcessFailureMaximumMsg();
	}

	@Then("^: Verify that the color of Validation message for invalid data of Maximum Allowed Process Failure in Period textbox should be Red$")
	public void verify_that_the_color_of_Validation_message_for_invalid_data_of_Maximum_Allowed_Process_Failure_in_Period_textbox_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifySampleSizeMinimumAllowedProcessFailureMaximumMsgColor();
	}

	@Given("^: Enter valid numeric value in the textbox$")
	public void enter_valid_numeric_value_in_the_textbox() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterValidNumericValueMaximumAllowedProcessFailureTextfield(excelData(20, 14));
	}

	@Then("^: Verify that system should not displayed any validation message when user enter valid data in textbox of Maximum Allowed Process Failure in Period$")
	public void verify_that_system_should_not_displayed_any_validation_message_when_user_enter_valid_data_in_textbox_of_Maximum_Allowed_Process_Failure_in_Period() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyValidNumericValueMaximumAllowedProcessFailureTextfield();
	}
	
	@Given("^: Click on radio button of Hourly option$")
	public void click_on_radio_button_of_Hourly_option() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickHourlyRadioBtn();
	}

	@Then("^: Verify that radio button of Hourly option should be clickable when user click on Hourly radio button$")
	public void verify_that_radio_button_of_Hourly_option_should_be_clickable_when_user_click_on_Hourly_radio_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyHourlyRadioBtnClickable();
	}

	@Given("^: Click on radio button of Weekly option$")
	public void click_on_radio_button_of_Weekly_option() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickWeeklyRadioBtn();
	}

	@Then("^: Verify that radio button of Hourly option should be clickable when user click on Weekly radio button$")
	public void verify_that_radio_button_of_Hourly_option_should_be_clickable_when_user_click_on_Weekly_radio_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWeeklyRadioBtnClickable();
	}

	@Given("^: Click on radio button of Monthly option$")
	public void click_on_radio_button_of_Monthly_option() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickMonthlyRadioBtn();
	}

	@Then("^: Verify that radio button of Hourly option should be clickable when user click on Monthly radio button$")
	public void verify_that_radio_button_of_Hourly_option_should_be_clickable_when_user_click_on_Monthly_radio_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMontlyRadioBtnClickable();
	}

	@Then("^: Verify that the color of Clear link should be Blue$")
	public void verify_that_the_color_of_Clear_link_should_be_Blue() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWindowFrequencyClearLinkColor();
	}
	
	@Given("^: Click on link of Clear in the Create Window screen$")
	public void click_on_link_of_Clear_in_the_Create_Window_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickWindowFrequencyClearLink();
	}

	@Then("^: Verify that radio button of Hourly option should be deselected when user click on Clear link$")
	public void verify_that_radio_button_of_Hourly_option_should_be_deselected_when_user_click_on_Clear_link() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyHourlyRadioBtnAfterClear();
	}

	@Then("^: Verify that radio button of Weekly option should be deselected when user click on Clear link$")
	public void verify_that_radio_button_of_Weekly_option_should_be_deselected_when_user_click_on_Clear_link() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWeeklyRadioBtnAfterClear();
	}

	@Then("^: Verify that radio button of Monthly option should be deselected when user click on Clear link$")
	public void verify_that_radio_button_of_Monthly_option_should_be_deselected_when_user_click_on_Clear_link() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyMontlyRadioBtnAfterClear();
	}
	
	@Then("^: Verify that label of Hourly textbox should be displayed as \"([^\"]*)\" \\+ \"([^\"]*)\"$")
	public void verify_that_label_of_Hourly_textbox_should_be_displayed_as(String arg1, String arg2) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyHourlyColumnLabel();
	}

	@Then("^: Verify that Required validation message for Hourly textbox should be displayed as Every Hour\\(s\\) is required\\.$")
	public void verify_that_Required_validation_message_for_Hourly_textbox_should_be_displayed_as_Every_Hour_s_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyHourlyRequiredMsg();
	}

	@Then("^: Verify that the color of Required validation message for Hourly textbox should be Red$")
	public void verify_that_the_color_of_Required_validation_message_for_Hourly_textbox_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyHourlyRequiredMsgColor();
	}
	
	@Given("^: Click on textbox of Hourly$")
	public void click_on_textbox_of_Hourly() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickHourlyTextfield();
	}

	@Given("^: Enter numeric value (\\d+) in the textbox of Hourly$")
	public void enter_numeric_value_in_the_textbox_of_Hourly(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterMinimumHourlyTextfield(excelData(21, 15));
	}

	@Then("^: Verify that Minimum validation message for Hourly textbox should be displayed as Invalid Number\\.It must be at least (\\d+)\\.$")
	public void verify_that_Minimum_validation_message_for_Hourly_textbox_should_be_displayed_as_Invalid_Number_It_must_be_at_least(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyHourlyMinimumMsg();
	}

	@Then("^: Verify that the color of Minimum validation message for Hourly textbox should be Red$")
	public void verify_that_the_color_of_Minimum_validation_message_for_Hourly_textbox_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		createWindowObject.verifyHourlyMinimumMsgColor();
	}

	@Given("^: Enter numeric value which is greater than (\\d+) hours$")
	public void enter_numeric_value_which_is_greater_than_hours(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterMaximumHourlyTextfield(excelData(22, 15));
	}

	@Then("^: Verify that Maximum validation message for Hourly textbox should be displayed as Invalid Number\\. You cannot add more than (\\d+) Hours\\.$")
	public void verify_that_Maximum_validation_message_for_Hourly_textbox_should_be_displayed_as_Invalid_Number_You_cannot_add_more_than_Hours(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyHourlyMaximumMsg();
	}

	@Then("^: Verify that the color of Maximum validation message for Hourly textbox should be Red$")
	public void verify_that_the_color_of_Maximum_validation_message_for_Hourly_textbox_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyHourlyMaximumMsgColor();
	}
	
	@Given("^: Enter valid data in the textbox of Hourly$")
	public void enter_valid_data_in_the_textbox_of_Hourly() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterValidHourlyTextfield(excelData(23, 15));
	}

	@Then("^: Verify that system should not displayed any validation message when user enter valid data in the textbox of Hourly$")
	public void verify_that_system_should_not_displayed_any_validation_message_when_user_enter_valid_data_in_the_textbox_of_Hourly() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyHourlyValidValue();
	}

	@Then("^: Verify that all checkboxs should be selected by default when user click on Weekly radio button$")
	public void verify_that_all_checkboxs_should_be_selected_bydefault_when_user_click_on_Weekly_radio_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWeeklyCheckboxAllSelectedByDefault();
	}

	@Given("^: Click on checkboxs of weekly radio button$")
	public void click_on_checkboxs_of_weekly_radio_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickAllWeeklyCheckbox();
	}

	@Then("^: Verify that all checkboxs should be clickable when user checked or unchecked the checkboxs$")
	public void verify_that_all_checkboxs_should_be_clickable_when_user_checked_or_unchecked_the_checkboxs() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWeeklyCheckboxAllUnselected();
	}
	
	@Given("^: Unchecked all the checkbox of Weekly$")
	public void unchecked_all_the_checkbox_of_Weekly() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.uncheckAllWeeklyCheckbox();
	}

	@Then("^: Verify that Required validation message for Weekly checkbox should be displayed as Please select at least one day\\.$")
	public void verify_that_Required_validation_message_for_Weekly_checkbox_should_be_displayed_as_Please_select_at_least_one_day() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWeeklyRequiredMsg();
	}

	@Then("^: Verify that the color of Required validation message for Weekly checkbox should be Red$")
	public void verify_that_the_color_of_Required_validation_message_for_Weekly_checkbox_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWeeklyRequiredMsgColor();
	}

	@Then("^: Verify that system should not displayed any validation message when user check at least one day$")
	public void verify_that_system_should_not_displayed_any_validation_message_when_user_check_at_least_one_day() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyWeeklyNoValidationMsg();
	}
	
	@Given("^: Click on textbox of Email Address$")
	public void click_on_textbox_of_Email_Address() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickEmailAddressTextfield();
	}

	@Then("^: Verify that Required validation message for Email Address textbox should be displayed as Email Address is required\\.$")
	public void verify_that_Required_validation_message_for_Email_Address_textbox_should_be_displayed_as_Email_Address_is_required() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyEmailAddressRequiredMsg();
	}

	@Then("^: Verify that the color of Required validation message for Email Address textbox should be Red$")
	public void verify_that_the_color_of_Required_validation_message_for_Email_Address_textbox_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyEmailAddressRequiredMsgColor();
	}

	@Given("^: Enter invalid format of Email Address$")
	public void enter_invalid_format_of_Email_Address() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterInvalidEmailAddress(excelData(24, 18));
	}

	@Then("^: Verify that validation message for invalid Email Address should be displayed as Invalid Email Address\\.$")
	public void verify_that_validation_message_for_invalid_Email_Address_should_be_displayed_as_Invalid_Email_Address() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyInvalidEmailAddressMsg();
	}
	
	@Then("^: Verify that the color of validation message for invalid Email Address should be Red$")
	public void verify_that_the_color_of_validation_message_for_invalid_Email_Address_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyInvalidEmailAddressMsgColor();
	}
	
	@Given("^: Enter Email Address which is already added in Create Window$")
	public void enter_Email_Address_which_is_already_added_in_Create_Window() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterDuplicateEmailAddress(excelData(25, 18));
	}

	@Then("^: Verify that validation message for Duplicate Email Address should be displayed as Duplicate Email Address\\(es\\) found\\.$")
	public void verify_that_validation_message_for_Duplicate_Email_Address_should_be_displayed_as_Duplicate_Email_Address_es_found() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyDuplicateEmailAddressMsg();
	}

	@Then("^: Verify that the color of  validation message for Duplicate Email Address should be Red$")
	public void verify_that_the_color_of_validation_message_for_Duplicate_Email_Address_should_be_Red() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyDuplicateEmailAddressMsgColor();
	}

	@Given("^: Enter valid Emai Address in the textbox$")
	public void enter_valid_Emai_Address_in_the_textbox() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.enterValidEmailAddress(excelData(26, 18));
	}

	@Then("^: Verify that system should not displayed any validation message when user enter valid Email Address in the textbox of Email Address$")
	public void verify_that_system_should_not_displayed_any_validation_message_when_user_enter_valid_Email_Address_in_the_textbox_of_Email_Address() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyValidEmailAddress();
	}
	
	@Given("^: Verify that Information message for Email Address should be displayed as Separate multiple Email Addresses with a comma\\(,\\)\\.$")
	public void verify_that_Information_message_for_Email_Address_should_be_displayed_as_Separate_multiple_Email_Addresses_with_a_comma() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyInformationEmailAddressMsg();
	}

	@Given("^: Verify that the color of Information message for Email Address should be Blue$")
	public void verify_that_the_color_of_Information_message_for_Email_Address_should_be_Blue() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyInformationEmailAddressMsgColor();
	}

	@Given("^: Verify that text of save button should be displayed as Save$")
	public void verify_that_text_of_save_button_should_be_displayed_as_Save() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyTextOfSaveBtn();
	}

	@Given("^: Verify that the color of save button should be Green$")
	public void verify_that_the_color_of_save_button_should_be_Green() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyTextOfSaveBtnColor();
	}

	@Given("^: Verify that text of cancel button should be displayed as Cancel$")
	public void verify_that_text_of_cancel_button_should_be_displayed_as_Cancel() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyTextOfCancelBtn();
	}

	@Given("^: Verify that the color of cancel button should be Black$")
	public void verify_that_the_color_of_cancel_button_should_be_Black() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.verifyTextOfCancelBtnColor();
	}
	
	@Given("^: Click on dropdown of Month field for checking values$")
	public void click_on_dropdown_of_Month_field_for_checking_values() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    createWindowObject.clickonMonthDrpDwn();
	}
	
}
