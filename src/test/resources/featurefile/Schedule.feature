Feature: Module Name: Schedule
Executed script on "https://test1.beperfeqta.com/mdav33" 

@chrome
Scenario: Verify Schedule Page load 
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    Then : Verify that the system should be redirected to the Schedule load

@chrome 
Scenario: Verify functionality of App dropdown of Schedule screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on dropdown of App listing
    Then : Verify that App dropdown should be clickable when user click on dropdown

@chrome 
Scenario: Verify Name of Second Tab in the Schedule screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    Then : Verify that Name of Second Tab in Schedule screen should be displayed as Review Completed Apps
    
@chrome 
Scenario: Verify Name of First Tab in the Schedule screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    Then : Verify that Name of First Tab in Schedule screen should be displayed as Start Scheduled Apps
        
@chrome 
Scenario: Verify functionality of Hourly Frequency with a completed status of Specific App
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on dropdown of App listing
    And : Select a specific App
    And : Click on Date dropdown 
    And : Select "This Year" option from the dropdown of the Date field
    And : Click on Filter by Status dropdown
    And : Select Completed option from the dropdown of Filter by Status field
    And : Click on Filter by Status dropdown
    Then : Verify that System should display all records of Hourly Frequency with completed status for specific App
   
@chrome 
Scenario: Verify functionality of Upcoming filter when all apps are selected
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on dropdown of App listing
    And : Select a specific App
    And : Click on Date dropdown 
    And : Select "This Year" option from the dropdown of the Date field
    And : Click on Filter by Status dropdown
    And : Select Upcoming option from the dropdown of Filter by Status field
    And : Click on Filter by Status dropdown
    Then : Verify that System should display all Upcoming records in Start Schedule Apps listing when all apps are selected

@chrome 
Scenario: Verify functionality of Overdue filter when all apps are selected
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on dropdown of App listing
    And : Select a specific App
    And : Click on Date dropdown 
    And : Select "This Year" option from the dropdown of the Date field
    And : Click on Filter by Status dropdown
    And : Select Overdue option from the dropdown of Filter by Status field
    And : Click on Filter by Status dropdown
    Then : Verify that System should display all Overdue records in Start Schedule Apps listing when all apps are selected
    
@chrome 
Scenario: Verify functionality of Incomplete filter when all apps are selected
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on dropdown of App listing
    And : Select a specific App
    And : Click on Date dropdown 
    And : Select "This Year" option from the dropdown of the Date field
    And : Click on Filter by Status dropdown
    And : Select Incomplete option from the dropdown of Filter by Status field
    And : Click on Filter by Status dropdown
    Then : Verify that System should display all Incomplete records in Start Schedule Apps listing when all apps are selected
    
@chrome @ignore
Scenario: Verify functionality of Completed filter when all apps are selected
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on Filter by Status dropdown
    And : Select "Completed" option from the dropdown of Filter by Status field
    Then : Verify that System should display all Completed records in Start Schedule Apps listing when all apps are selected
      
@chrome 
Scenario: Verify functionality of Ready to Perform filter when all apps are selected
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on dropdown of App listing
    And : Select a specific App
    And : Click on Date dropdown 
    And : Select "This Year" option from the dropdown of the Date field
    And : Click on Filter by Status dropdown
    And : Select Ready to Perform option from the dropdown of Filter by Status field
    And : Click on Filter by Status dropdown
    Then : Verify that System should be displayed all Ready to Perform records in Start Schedule Apps listing when all apps are selected   
    
#------Master App -------    
    
@chrome 
Scenario: Verify functionality of Upcoming filter when all Master apps are selected
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on Search by dropdown
    And : Select Master App option
    And : Click on Filter by Status dropdown
    And : Select Upcoming option from the dropdown of Filter by Status field
    And : Click on Filter by Status dropdown
    Then : Verify that System should display all Upcoming records in Start Schedule Apps listing when Master apps are selected
    
@chrome 
Scenario: Verify functionality of Overdue filter when Master apps are selected
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on Search by dropdown
    And : Select Master App option
    And : Click on Filter by Status dropdown
    And : Select Overdue option from the dropdown of Filter by Status field
    And : Click on Filter by Status dropdown
    Then : Verify that System should display all Overdue records in Start Schedule Apps listing when Master apps are selected
    
@chrome 
Scenario: Verify functionality of Incomplete filter when Master apps are selected
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on Search by dropdown
    And : Select Master App option
    And : Click on Filter by Status dropdown
    And : Select Incomplete option from the dropdown of Filter by Status field
    And : Click on Filter by Status dropdown
    Then : Verify that System should display all Incomplete records in Start Schedule Apps listing when Master apps are selected
    
@chrome 
Scenario: Verify functionality of Completed filter when Master apps are selected
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on Search by dropdown
    And : Select Master App option
    And : Click on Filter by Status dropdown
    And : Select Completed option from the dropdown of Filter by Status field
    And : Click on Filter by Status dropdown
    Then : Verify that System should display all Completed records in Start Schedule Apps listing when all Master apps selected
    
@chrome 
Scenario: Verify functionality of Ready to Perform filter when Master apps are selected
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on Search by dropdown
    And : Select Master App option
    And : Click on Filter by Status dropdown
    And : Select Ready to Perform option from the dropdown of Filter by Status field
    And : Click on Filter by Status dropdown
    Then : Verify that system should be displayed all Ready to Perform records in Start Schedule Apps listing when Master apps are selected
    
@chrome 
Scenario: Verify functionality of Expand Button in the Schedule screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on Collapse Button
    And : Click on Expand Button
    Then : Verify system should expand Filter By section when user click on Expand Button

@chrome 
Scenario: Verify functionality of Collapse Button in the Schedule screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on Collapse Button
    Then : Verify system should Hide Filter By section when user click on Collape Button    

@chrome 
Scenario: Verify records of Schedule when Date filter is "This Week"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on dropdown of App listing
    And : Select a specific App
    And : Select This Week option from the dropdown of the Date field
    Then : Verify that system should display all the records of Current Week when user select This Week option
    
@chrome 
Scenario: Verify records of Schedule when Date filter is "This Month"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on dropdown of App listing
    And : Select a specific App  
    And : Select This Month option from the dropdown of the Date field
    Then : Verify that system should display all the records of Current Month when user select This Month option
    
@chrome 
Scenario: Verify records of Schedule when Date filter is "This Year"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on dropdown of App listing
    And : Select a specific App
    And : Select This Year option from the dropdown of the Date field
    Then : Verify that system should display all the records of Current Year when user select This Year option
    
@chrome 
Scenario: Verify redirection of the app when user click on App/Master App in Start Schedule Apps listing
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Schedule Tile
    And : Click on the first record of App/Master App column in Start Schedule Apps listing
    Then : Verify that system should be redirected to the particular App when user click on record of App/Master App column in Start Schedule Apps listing
    