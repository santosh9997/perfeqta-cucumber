Feature: Module Name: Share App Link
Executed script on "https://test1.beperfeqta.com/mdav33" 
	
@chrome
Scenario: Verify the module title when user click on "Share App Link" module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
    Then : Verify the module name as Share App Link
	
@chrome
Scenario: Verify Breadcrumb of Share App Link module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
    Then : Verify that system should displayed Breadcrumb for Share App Link module as "Home > Administration > Share App Link"

@chrome @ignore
Scenario: Verify Pagination of Share App Link page
    Given : Navigate to the URL
	  And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	  Then : Verify total number of entries should be match with pagination of Share App Link page
	
@chrome 
Scenario: Verify Pagination of Share App Link page when user click on "Last"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
    And : Click on the Last button of Pagination 
    Then : Verify that the system should displayed the last page of the listing screen
   
@chrome
Scenario: Verify pagination of Share App Link page when user click on "First"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
    And : Click on the Last button of Pagination 
	And : Click on the First button of Pagination 
    Then : Verify that the system should displayed the first page of the listing screen
	
@chrome 
Scenario: Verify the navigation of Audit trail page of Share App Link page
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	  And : Click on Share App Link Tile
	  And : Click on "View Audit Trail" link of the first record of the Share App Link page
    Then : Verify that system should displayed Audit Trail page when user click on "View Audit Trail" link of the Share App Link page	
	
@chrome @ignore
Scenario: Verify the Ascending order for the Apps column under Share App Link page
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
    And : Click on Apps
    Then : Verify that all the records of screen should be display in ascending order as per App Name 
	
@chrome @ignore
Scenario: Verify the Descending order for the Apps column under Share App Link page
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
    And : Click on Sorting icon of the Apps column
    Then : Verify that all the records of screen should be display in descending order as per App Name 
	
@chrome @ignore
Scenario: Verify Search functionality for Share App Link page
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Share App Link Tile
    And : Enter the data into search box, which user want to search Share app link
	Then : Verify the search results of Share App Link page
	
@chrome
Scenario: Verify Breadcrumb of View Records link
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	  And : Click on "View Records" link of the first record from Submitted Records column 
    Then : Verify that system should display Breadcrumb as "Home > Administration > Share App Link > View Submitted Records"
	
@chrome
Scenario: Verify click on Share App button
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	  And : Click on "Share App" button of Share App Link page
	  Then : Verify that system should redirect to Add/Edit Share App Link page	
	
@chrome
Scenario: Verify Share App Link Details label of Add/Edit share App Link page
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	Then : Verify that system should display label as "Share App Link Details" for Add/Edit share App Link page 
	
@chrome
Scenario: Verify App Details label of Add/Edit Share App Link page
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	Then : Verify App Details label should be display as "App Details" for Add/Edit Share App Link page

@chrome
Scenario: Verify Outside User Details label of Add/Edit Share App Link page
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	Then : Verify Outside User Details label should be display as "Outside User Details label" for Add/Edit Share App Link page
	
@chrome
Scenario: Verify Required validation message for Module field
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Click on Module dropdown Share App
	And : Press "TAB" Key
	Then : Verify required validation message for Module dropdown as "Module is required."
	
@chrome
Scenario: Verify Required validation message for App field
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Click on App dropdown of Share App Link
	And : Press "TAB" Key App dropdown
	Then : Verify required validation message for App dropdown as "App is required."
	
@chrome 
Scenario: Verify Copy Link is visible or not
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Click on Module dropdown Share App
	And : Select Module from Module dropdown
	And : Click on App dropdown of Share App Link
	And : Select App from App dropdown
	Then : Verify that system should display link as "Copy Link"
	
@chrome
Scenario: Verify Invalid validation message for Email Address(es) field
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Enter Email Address value as "s@@"
	Then : Verify invalid validation message for Email Address field as "Invalid Email Address(es)."
	
@chrome
Scenario: Verify Email Address(es) text box with valid data
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Enter a valid Email Address
	Then : Verify system should accept the email address without any validation message
	
@chrome
Scenario: Verify Email address list functionality for Email Address field 
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Enter 1st valid Email Address and Click on Add button
	And : Enter 2nd valid Email Address and Click on Add button
	Then : Verify that system should display Added email addresses into list

@chrome
Scenario: Verify Remove email addresses functionality for Email Address field  
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Enter valid Email Address and Click on Add button
	And : Click on Remove button of Email Address
	Then : Verify that system should remove the email address from the list when user click on Remove button

@chrome
Scenario: Verify Color of Information message for Email Address(es) field
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	Then : Verify that color of information message for email address field should be blue
	
@chrome
Scenario: Verify Options after Checked Allow Multiple App Submissions checkbox
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Checked "Allow Multiple App Submissions" checkbox
	Then : Verify that system should display 1st option as "Total Number of App Submissions" and 2nd option as "Number of Submissions per Individual Email Address"

@chrome
Scenario: Verify Required validation message for Total Number of App Submissions radio button
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Checked "Allow Multiple App Submissions" checkbox
	And : Select "Total Number of App Submissions" radio button
	And : Click on "Share App" button of Add/Edit Share App Link page
	Then : Verify validation message as "Total Number of App Submissions is required."

@chrome
Scenario: Verify Minimum validation message for Total Number of App Submissions radio button
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Checked "Allow Multiple App Submissions" checkbox
	And : Select "Total Number of App Submissions" radio button
	And : Enter value into textbox as "0"
	Then : Verify validation message as "Total Number of App Submissions should be at least 1." in Number of app submission
	
@chrome
Scenario: Verify Maximum validation message for Total Number of App Submissions radio button
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Checked "Allow Multiple App Submissions" checkbox
	And : Select "Total Number of App Submissions" radio button
	And : Enter value into textbox as "10000"  Number of app submission
	Then : Verify validation message as "Total Number of App Submissions must not be more than 9999."  Number of app submission
	
@chrome
Scenario: Verify Total Number of App Submissions radio button with valid data 
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Checked "Allow Multiple App Submissions" checkbox
	And : Select "Total Number of App Submissions" radio button
	And : Enter valid value into textbox Number of app submission
	Then : Verify the system should accept the Total Number of App Submissions value without any validation message
	
@chrome
Scenario: Verify Required validation message for Number of Submissions per Individual Email Address radio button
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Checked "Allow Multiple App Submissions" checkbox
	And : Select "Number of Submissions per Individual Email Address" radio button of Number of Submissions per Individual Email Address
	And : Click on "Share App" button of Add/Edit Share App Link page
	Then : Verify validation message as "Number of Submissions per Individual Email Address is required." Number of Submissions per Individual Email Address
	
@chrome
Scenario: Verify Minimum validation message for Number of Submissions per Individual Email Address radio button
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Checked "Allow Multiple App Submissions" checkbox
	And : Select "Number of Submissions per Individual Email Address" radio button of Number of Submissions per Individual Email Address
	And : Enter value into textbox as "0" Number of Submissions per Individual Email Address
	Then : Verify zero validation message as "Number of Submissions per Individual Email Address should be at least 1." Number of Submissions per Individual Email Address
	
	
@chrome
Scenario: Verify Maximum validation message for Number of Submissions per Individual Email Address radio button
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Checked "Allow Multiple App Submissions" checkbox
	And : Select "Number of Submissions per Individual Email Address" radio button of Number of Submissions per Individual Email Address
	And : Enter max value into textbox as "10000" Number of Submissions per Individual Email Address
	Then : Verify max validation message as "Number of Submissions per Individual Email Address must not be more than 9999."  Number of Submissions per Individual Email Address
	

@chrome
Scenario: Verify Number of Submissions per Individual Email Address radio button with valid data 
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Checked "Allow Multiple App Submissions" checkbox
	And : Select "Number of Submissions per Individual Email Address" radio button of Number of Submissions per Individual Email Address
	And : Enter valid value into textbox Number of Submissions per Individual Email Address
	Then : Verify the system should accept the Number of Submissions per Individual Email Address value without any validation message

@chrome
Scenario: Verify required validation message for Set Link Expiry Date field
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Checked "Set Link Expiry Date" checkbox set link expiry date
	And : Press "TAB" Key set link expiry date
	Then : Verify that system should display required validation message as "Expiry Date is required." Expiry date required validation
	
@chrome 
Scenario: Verify invalid validation message for Set Link Expiry Date field
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Checked "Set Link Expiry Date" checkbox set link expiry date
	And : Enter Date in invalid format set link expiry date
	Then : Verify validation message as "Expiry Date must be MM/DD/YYYY." set link expiry date
	
@chrome
Scenario: Verify Invalid Validation message for Special Message field
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Enter Special Message value less than 2 characters
	Then : Verify validation message as "Special Message should be between 2 to 800 characters." in Special Message

@chrome
Scenario: Verify Special Message textbox with valid data
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Enter valid data into Special Message text box
	Then : Verify the system should accept the special message value without any validation message
	
@chrome 
Scenario: Verify Create Share App Link functionality
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Share App Link Tile
	And : Click on "Share App" button of Share App Link page
	And : Click on Module dropdown Share App
	And : Select Module from Module dropdown
	And : Click on App dropdown of Share App Link
	And : Select App from App dropdown
	And : Enter a valid Email Address
	And : Checked "Allow Multiple App Submissions" checkbox
	And : Select "Total Number of App Submissions" radio button
	And : Enter valid value into textbox Number of app submission
	And : Enter valid data into Special Message text box
	And : Click on "Share App" button of Add/Edit Share App Link page
	Then : Verify that the newly added Share App should appear on the listing screen and the page should be redirected to listing screen of Share App Link





 


	





	



	
