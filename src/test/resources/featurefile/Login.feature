Feature: Module Name: Login  
Executed script on "https://test1.beperfeqta.com/mdav33"
	
@chrome @regression @login1 @vishal
Scenario: Verify the Login Label Exists on the Login page 
	Given : Navigate to the URL 
	When : Click to Login Button
	Then : Verify the label as "Login"

@chrome @sanity @login1
Scenario: Verify username field mandatory validation message 
	Given : Navigate to the URL 
	And : Enter username like "blank" value 
	And : Enter valid Password like "@Santosh9997" 
	When : Click to Login Button
	Then : Verify the username mandatory validation message as "username is required." 
		
@chrome @regression 
Scenario: Verify the color of required username Validation message 
	Given : Navigate to the URL 
	And : Without enter username press the "Tab" key 
	Then : Verify the required username validation color 

@chrome @sanity 
Scenario: Verify Password field mandatory validation message 
	Given : Navigate to the URL 
	And : Enter valid username like "jane" 
	And : Enter Password like "blank" value 
	When : Click to Login Button 
	Then : Verify the Password mandatory validation message "Password is required."
	
@chrome @regression 
Scenario: Verify the color of required password Validation Message 
	Given : Navigate to the URL 
	And : Without enter Password press the "Tab" key 
	Then : Verify the required password validation color 
	
@chrome @smoke @sanity 
Scenario: Verify the message for invalid login 
	Given : Navigate to the URL 
	And : Enter invalid username like "Invalid" 
	And : Enter valid Password like "@Santosh9997" 
	When : Click to Login Button 
	Then : Verify the invalid login message as "The combination of the username and password doesn't match our records. Please check your entries. If you have forgotten your password, please click the Forgot Password? link to recover it." 
	
@chrome @regression @Test1
Scenario: Verify the color of invalid login validation message 
	Given : Navigate to the URL 
	And : Enter invalid username like "Invalid" 
	And : Enter valid Password like "@Santosh9997" 
	When : Click to Login Button 
	Then : Verify the invalid validation color 
	
@chrome @regression 
Scenario: Verify the Back ground color of Login Button 
	Given : Navigate to the URL 
	Then : Verify the Login button color 
		
@chrome @regression 
Scenario: Verify Validation message for the already loged in user 
	Given : Navigate to the URL in Incongnito mode 
	And : Enter valid username like "jane" 
	And : Enter valid Password like "@Santosh9997" 
	When : Click to Login Button 
	And : close the Incongnito mode browser. 
	Given : Navigate to the URL
	And : Enter valid username like "jane" 
	And : Enter valid Password like "@Santosh9997" 
	When : Click to Login Button 
	Then : Verify the already loged in user validation message like "Oops! User is currently logged in with this account. Do you want to log off current user? Yes No" 
	
@chrome @regression 
Scenario: Verify the confirmation "No" button color. 
	Given : Display already loged in user validation message 
	Then : Verify the confirmation "No" button color 
	
@chrome @regression 
Scenario: Verify the confirmation "Yes" button color 
	Given : Display already loged in user validation message 
	Then : Verify the confirmation "Yes" button color. 
	
@chrome @regression 
Scenario: Verify the confirmation "No" button text label. 
	Given : Display already loged in user validation message 
	Then : Verify the confirmation "No" text label 
	
@chrome @regression 
Scenario: Verify the confirmation "Yes" button text label 
	Given : Display already loged in user validation message 
	Then : Verify the confirmation "Yes" text label. 
	
@chrome @regression 
Scenario: Verify the Forgot Password link color before click 
	Given : Navigate to the URL 
	Then : Verify the Forgot Password link color 
	
@chrome @regression 
Scenario: Verify the Forgot password label on Login screen
	Given : Navigate to the URL 
	Then : verify the Forgot Password label as "Forgot Password?" 
	
@chrome @regression
Scenario: Verify the Forgot password label on Forgot Password screen
	Given : Navigate to the URL 
	And : Click to "Forgot Password?" link 
	Then : verify the Forgot Password label as "Forgot Password". 

@chrome @regression 
Scenario: Verify the username mandatory validation message on Forgot Password screen 
	Given : Navigate to the URL 
	And : Click to "Forgot Password?" link 
	And : Without enter username press the "Tab" key on Forgot Password screen 
	Then : Verify the username mandatory validation message like "Username is required." 
	
@Chrome @regression 
Scenario: Verifyt the username mandatory validation message color on Forgot Password screen 
	Given : Navigate to the URL 
	And : Click to "Forgot Password?" link 
	And : Without enter username press the "Tab" key on Forgot Password screen 
	Then : Verify the username mandatory validation message color 
	
@chrome @regression 
Scenario: Verify the username minimum character validation message on Forgot Password screen 
	Given : Navigate to the URL 
	And : Click to "Forgot Password?" link 
	And :  Enter Username as one character and press the "Tab" key 
	Then : Verify the username minimum character validation message like "Username must be at least 2 characters." 
	
@chrome @regression 
Scenario: Verify the username minimum character validation message color on Forgot Password screen 
	Given : Navigate to the URL 
	And : Click to "Forgot Password?" link 
	And :  Enter Username as one character and press the "Tab" key 
	Then : Verify the username minimum validation message color 
	
	
@chrome @regression 
Scenario: Verify the username maximum character validation message on Forgot Password screen 
	Given : Navigate to the URL 
	And : Click to "Forgot Password?" link 
	And :  Enter Username as  than 20 characters and press the "Tab" key 
	Then : Verify the username maximum validation message like "Username cannot be more than 20 characters."
	
@chrome @regression 
Scenario: Verify the username maximum character validation message color on Forgot Password screen 
	Given : Navigate to the URL 
	And : Click to "Forgot Password?" link 
	And :  Enter Username as  than 20 characters and press the "Tab" key 
	Then : Verify the username maximum validation message color 
	
@chrome @regression 
Scenario: Verify the Submit button background color on Forgot Password screen 
	Given : Navigate to the URL 
	And : Click to "Forgot Password?" link 
	Then : verify the submit button background color 
	
@chrome @regression 
Scenario: Verify the Submit button text on Forgot Password screen 
	Given : Navigate to the URL 
	And : Click to "Forgot Password?" link 
	Then : Verify the submit button text 
	
@chrome @regression 
Scenario: Verify the Cancel button background color on the Forgot Password screen 
	Given : Navigate to the URL 
	And : Click to "Forgot Password?" link 
	Then : Verify the Cancel button background color 
	
@chrome @regression 
Scenario: Verify the Cancel buton Text on the Forgot Password screen 
	Given : Navigate to the URL 
	And : Click to "Forgot Password?" link 
	Then : Verify the Cancel button Text 
		
@chrome @smoke @regression 
Scenario: Verify the Cancel button functionality on the Forgot Password screen 
	Given : Navigate to the URL 
	And : Click to "Forgot Password?" link 
	And : Click to "Cancel" button 
	Then : Verify that when click on cancel button user redirected to Login screen 	

@chrome @smoke @regression @login
Scenario: Verify if a user will be able to login with a valid credentials 
	Given : Navigate to the URL 
	And : Enter valid username like "jane" 
	And : Enter valid Password like "@Santosh9997" 
	When : Click to Login Button 
	Then : Verify the user is login successfully into the system 