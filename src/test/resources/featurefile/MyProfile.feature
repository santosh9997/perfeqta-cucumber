Feature: Module Name: My Profile
Executed script on "https://test1.beperfeqta.com/mdav33"    

@chrome 
Scenario: Verify the module title when user click on "My Profile" module
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
    And : Click on My Profile link
    Then : Verify the module name as My Profile

@chrome
Scenario: Verify breadcrumbs functionality of My Profile Screen
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
    And : Click on My Profile link
    And : Click on breadcrumbs of My Profile screen
    Then : Verify that the page should be redirected to the previous page

@chrome
Scenario: Verify the Username label of the My Profile Screen
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
    And : Click on My Profile link
    Then : Verify the Username label as "Username *"
 
@chrome
Scenario: Verify that the Username field of the My Profile screen is disable
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	Then : Verify that the Username field of the My Profile screen should be disable

@chrome
Scenario: Verify First Name label of the My Profile Screen
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	Then : Verify the First Name label as "First Name *"

@chrome
Scenario: Verify the Firsname textbox value when user click on My Profile page
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	Then : Verify that the FirstName value should be same as Logged in username
   	
@chrome
Scenario: Verify Mandatory validationa message for the First Name textbox of the My Profifle screen
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Remove the First name value and Press the "TAB" key
   	Then : Verify the Mandatory validation message as First Name is required.

@chrome
Scenario: Verify the First name mandatory validation message color
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Remove the First name value and Press the "TAB" key
	Then : Verify the Mandatory validation message color as "Red"

@chrome
Scenario: Verify the Minimum Character validation message for the First Name of My Profile screen
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Enter First Name as "T"
   	Then : Verify validation message as Firstname must be at least 2 characters.

@chrome
Scenario: Verify the Firstname minimum character validation message color.
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Enter First Name as "T"
   	Then : Verify minimum charcter validation message color as "Red"

@chrome
Scenario: Verify Maximum character validtion message for the FirstName
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Enter First Name value as more than 50 characters
   	Then : Verify the Maximum character validation message as First Name cannot be more than 50 characters.

@chrome
Scenario: Verify the Firstname Maximum character validation message color.
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Enter First Name value as more than 50 characters
   	Then : Verify maximum charcter validation message color as "Red"

@chrome
Scenario: Verify Last Name label of the My Profile Screen
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	Then : Verify the Last Name label as "Last Name *"

@chrome
Scenario: Verify the LastName textbox value when user click on My Profile page
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	Then : Verify that the Last Name value should be proper

@chrome
Scenario: Verify Mandatory validationa message for the Last Name textbox of the My Profifle screen
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Remove the Last name value and Press the "TAB" key
   	Then : Verify the Mandatory validation message as Last Name is required.

@chrome
Scenario: Verify the Last name mandatory validation message color
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Remove the Last name value and Press the "TAB" key
	Then : Verify the Lastname Mandatory validation message color as "Red"

@chrome
Scenario: Verify the Minimum Character validation message for the Last Name of My Profile screen
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Enter Last Name as "T"
   	Then : Verify validation message as Last Name must be at least 2 characters.

@chrome
Scenario: Verify the Last Name minimum character validation message color.
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Enter Last Name as "T"
   	Then : Verify Lastname minimum character validation message color as "Red"

@chrome
Scenario: Verify Maximum character validtion message for the Last Name
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Enter Last Name value as more than 50 charcters
   	Then : Verify the Maximum character validation message as Last Name cannot be more than 50 characters.

@chrome
Scenario: Verify the Last Name minimum character validation message color.
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Enter Last Name value as more than 50 charcters
   	Then : Verify maximum characters validation message color as "Red"

@chrome
Scenario: Verify Email Address label of the My Profile Screen
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	Then : Verify the Email Address label as "Email Address *"

@chrome
Scenario: Verify Mandatory validationa message for the Email textbox of the My Profifle screen
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Remove the Email value and Press the "TAB" key
   	Then : Verify the Mandatory validation message as Email Address is required.

@chrome
Scenario: Verify the Email Address mandatory validation message color
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Remove the Email value and Press the "TAB" key
	Then : Verify the Email Mandatory validation message color as "Red"

@chrome 
Scenario: Verify the validation message for Invalid Email Address of My Profile Screen
	 Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Enter InCorrect Email Address
	Then : Verify the Incorrect Email validation message as Invalid Email Address.

@chrome 
Scenario: Verify the Incorrect Email Address mandatory validation message color
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Enter InCorrect Email Address
	Then : Verify the Incorrect Email Address validation message color as "Red"

@chrome
Scenario: Verify Maximum character validtion message for the Email Address
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Enter Emai Address value as more than 50 charcters
   	Then : Verify the Maximum character validation message as Email Address cannot be more than 50 characters long.

@chrome
Scenario: Verify the Maximum character Email Address validation message color
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Enter Emai Address value as more than 50 charcters
		Then : Verify the Maximum character Email Address validation message color as "Red"

@chrome 
Scenario: Verify the First Name textbox with valid data
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
		And : Enter valid name in First Name as "Automation"
		Then : Verify that system should allow First Name with valid data
	
@chrome 
Scenario: Verify the Last Name textbox with valid data
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
		And : Enter valid name in Last Name as "Cucumber"
		Then : Verify that system should allow Last Name with valid data

@chrome
Scenario: Verify Unique validation message for Entered Alredy added Email Address
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Enter Email Address which is already added
		Then : Verify system should be displayed validation message as "Email Address must be unique." 

@chrome 
Scenario: Verify for the Unique validation message when edit Email address
Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Edit Email address
	And : Click on Save Buttton
	Then : Verify that system should displayed validation message as "Last verified email is cka@yopmail.com\n" + "( Cancel verification | Resend verification email )"

@chrome 
Scenario: Verify Security Question button is clickable 
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Security Question Button
	Then : Verify that Security Question button should be clickable when user click on button

@chrome 
Scenario: Verify the Label of Security Question when click on Security Question Button 
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Security Question Button
	Then : Verify that Label of Security Question should be displayed as "Security Question *"

@chrome 
Scenario: Verify the Label of Answer in Security Question when user Click on Security Question Button
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Security Question Button
	Then : Verify that Label of Answer should be displayed as "Answer *"

@chrome
Scenario: Verify Required vaildation message for Security Question textbox for Security Question
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Security Question Button
		And : select "--Select Security Question--"
		Then : Verify that system should displayed Required vaildation message as "Security Question is required."

@chrome 
Scenario: Verify the color of Required vaildation message for Security Question textbox 
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Security Question Button
	And : select "--Select Security Question--"
	Then : Verify that color of Required vaildation message should be "Red"

@chrome 
Scenario: Verify Required vaildation message for Answer in Security Question
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Security Question Button
		And : Click on Answer textbox
		And : Press "TAB" key
		Then : Verify that system should displayed Required vaildation message for Answer as "Answer is required."

@chrome 
Scenario: Verify the color of Required vaildation message for Answer in Security Question
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Security Question Button
	And : Click on Answer textbox
	And : Press "TAB" key
	Then : Verify that color of Required vaildation message of Answer should be "Red"

@chrome 
Scenario: Verify Minimum vaildation message for Answer of Security Question
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Security Question Button
		And : Click on Answer textbox
		And : Enter less than 2 characters
		Then : Verify that Minimum vaildation message for Answer should be displayed as "Answer must be at least 2 characters."

@chrome 
Scenario: Verify the color of Minimum vaildation message for Answer of Security Question
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Security Question Button
	And : Click on Answer textbox
	And : Enter less than 2 characters
	Then : Verify that color of Minimum vaildation message for Answer should be "Red"

@chrome 
Scenario: Verify Maximum vaildation message for Answer of Security Question
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Security Question Button
	And : Click on Answer textbox
	And : Enter more than 50 characters
	Then : Verify that Maximum vaildation message for Answer should be displayed as "Answer cannot be more than 50 characters."

@chrome 
Scenario: Verify the color of Maximum validation message for Answer of Security Question
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Security Question Button
	And : Click on Answer textbox
	And : Enter more than 50 characters
	Then : Verify that color of Maximum validation message for Answer should be "Red" 

@chrome 
Scenario: Verify the functionality of Answer textbox with Valid data
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Security Question Button
	And : Click on Answer textbox
	And : Enter valid data as "Ahmedabad"
	Then : Verify that system should allow to enter Valid data into Answer textbox

@chrome 
Scenario: Verify validation message when saving the Data
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Security Question Button
	And : Click on Answer textbox
	And : Enter valid data as "abc"
	And : Click on "Save" Button
	Then : Verify that validation message should be displayed as "Security Question has changed successfully."

@chrome 
Scenario: Verify the color of validation message when saving the data
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Security Question Button
	And : Click on Answer textbox
	And : Enter valid data as "abc"
	And : Click on "Save" Button
	Then : Verify that color of validation message should be displayed "Green"

@chrome
Scenario: Verify Change Password button is clickable
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	Then : Verify that Change Password should be clickable when user click on Button

@chrome
Scenario: Verify the First Label of Change Password 
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	Then : Verify that First Label of Change Password should be displayed as "Current Password *"

@chrome
Scenario: Verify the Second Label of Change Password
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	Then : Verify that Second Label of Change Password should be displayed as "New Password *"

@chrome
Scenario: Verify the Third Label of Change Password
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	Then : Verify that Third Label of Change Password should be displayed as "Confirm Password *"

@chrome
Scenario: Verify Tooltip message of Change Password
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Move mouse on the information icon
	Then : Verify that system should displayed Tooltip message as "You cannot reuse your recent 4 passwords."

@chrome
Scenario: Verify the Required vaildation message for Current Password textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Click on Current Password textbox
	Then : Verify that system should displayed Required vaildation message of Current Password as "Current Password is required."

@chrome
Scenario: Verify the color of Required vaildation message for Current Password textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Click on Current Password textbox
	Then : Verify that color of Required validation message should be "Red"

@chrome
Scenario: Verify the Invalid vaildation message for Current Password
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Click on Current Password textbox
	And : Enter Invalid Password
	And : Click on New Password textbox
	And : Enter New Password as "Test@123"
	And : Click on Confirm Password textbox
	And : Enter Confirm Password as "Test@123"
	And : Click on Change Password "Save" Button
	Then : Verify that Invalid vaildation message should be displayed as "Current Password is incorrect."

@chrome
Scenario: Verify the color of Invalid vaildation message for Current Password
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Click on Current Password textbox
	And : Enter Invalid Password
	And : Click on New Password textbox
	And : Enter New Password as "Test@123"
	And : Click on Confirm Password textbox
	And : Enter Confirm Password as "Test@123"
	And : Click on Change Password "Save" Button
	Then : Verify that color of Invalid vaildation message should be "Red" 

@chrome
Scenario: Verify the Required vaildation message for New Password textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Click on New Password textbox
	Then : Verify that system should displayed Required vaildation message of New Password as "New Password is required."

@chrome
Scenario: Verify the color of Required vaildation message for New Password textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Click on New Password textbox
	Then : Verify that color of Required vaildation message of New Password should be "Red"

@chrome 
Scenario: Verify the validation message for Confirm Password
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Click on Current Password textbox
	And : Enter Current Password
	And : Click on New Password textbox
	And : Enter New Password as "Test@123"
	And : Click on Confirm Password textbox
	And : Enter Incorrect Confirm Password as "Text@12345"
	Then : Verify that validation message for Confirm Password should be displayed as "Confirm Password and New Password must be exactly same."

@chrome
Scenario: Verify the color of validation message for Confirm Password
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Click on Current Password textbox
	And : Enter Current Password
	And : Click on New Password textbox
	And : Enter New Password as "Test@123"
	And : Click on Confirm Password textbox
	And : Enter Incorrect Confirm Password as "Text@12345"
	Then : Verify that color of validation message for Confirm Password should be "Red"

@chrome
Scenario: Verify the Invalid vaildation message for New Password
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Click on Current Password textbox
	And : Enter Current Password
	And : Click on New Password textbox
	And : Enter New Password as alphabets like "abc"
	Then : Verify that Invalid vaildation message should display properly

@chrome
Scenario: Verify the color of Invalid vaildation message for New Password
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Click on Current Password textbox
	And : Enter Current Password
	And : Click on New Password textbox
	And : Enter New Password as alphabets like "abc"
	Then : Verify that color of Invalid vaildation message for New Password should be "Red"

@chrome
Scenario: Verify the Required vaildation message for Confirm Password
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Click on Confirm Password textbox
	Then : Verify that system should displayed Required vaildation message of Confirm Password as "Confirm Password is required."

@chrome
Scenario: Verify the color of Required vaildation message for Confirm Password
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
  	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Click on Confirm Password textbox
Then : Verify that color of Required vaildation message for Confirm Password should be "Red"

@chrome 
Scenario: Verify validation message when New Password is same as Current Password
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Click on Current Password textbox
	And : Enter Current Password as Test@123
	And : Click on New Password textbox
	And : Enter New Password as Test@123
	Then : Verify that validation message for New Password is same as Current Password should be displayed as "New Password cannot be the same as your Old Password."

@chrome
Scenario: Veriry the color of  validation message when  Current Password and New Password is same
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Click on Current Password textbox
	And : Enter Current Password as Test@123
	And : Click on New Password textbox
	And : Enter New Password as Test@123
	Then : Verify that color of vaildation message should be "Red" when Current Password and New Password is same

@chrome
Scenario: Verify that Cancel Button is clickable for Change Password
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change Password Button
	And : Click on Cancel Button
	Then : Verify that Cancel button should be clickable when user click on Cancel Button
	
	
# ------------------------------------- E-Signature PIN -------------------------------------
@chrome 
Scenario: Verify that Change E-Signature PIN button is clickable
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	Then : Verify that Change E-Signature PIN button should be clickable when user click on Change E-Signature PIN Button

@chrome 
Scenario: Verify the First Label for Change E-Signature PIN
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	Then : Verify that First Label of Change E-Signature PIN should be displayed as "Current eSignature PIN *(Forgot eSignature PIN?)"
	
@chrome 
Scenario: Verify the Second Label for Change E-Signature PIN
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	Then : Verify that Second Label of Change E-Signature PIN should be displayed as "New eSignature PIN *"
	
@chrome 
Scenario: Verify the Last Label for Change E-Signature PIN
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	Then : Verify that Last Label of Change E-Signature PIN should be displayed as "Confirm New eSignature PIN *"
	
@chrome 
Scenario: Verify the Required validation message for Current E-Signature PIN
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on Current E-Signature PIN textbox
	Then : Verify that Required vaildation message for Current E-Signature PIN should be displayed as "Current eSignature PIN is required."
	
@chrome 
Scenario: Verify the color of Required validation message for Current E-Signature PIN textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on Current E-Signature PIN textbox
	Then : Verify that color of Required vaildation message for Current E-Signature PIN should be "Red"
	
@chrome 
Scenario: Verify Invalid vaildation message for Current e-Signature PIN textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on Current E-Signature PIN textbox
	And : Enter Invalid E-Signature PIN
	And : Click on New E-Signature PIN
	And : Enter E-Signature PIN as "1410" in New E-Signature PIN
	And : Click on Confirm New eSignature PIN textbox
	And : Enter E-Signature PIN as "1410" in Confirm E-Signature PIN
	And : Click on "Save" Button of Change E-Signature PIN
	Then : Verify that system should displayed vaildation message as Current eSignature PIN is incorrect
	
@chrome 
Scenario: Verify the color of Invalid vaildation message for Current e-Signature PIN textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on Current E-Signature PIN textbox
	And : Enter Invalid E-Signature PIN
	And : Click on New E-Signature PIN
	And : Enter E-Signature PIN as "1410" in New E-Signature PIN
	And : Click on Confirm New eSignature PIN textbox
	And : Enter E-Signature PIN as "1410" in Confirm E-Signature PIN
	And : Click on "Save" Button of Change E-Signature PIN
	Then : Verify that color of vaildation message should be Red
	
@chrome  
Scenario: Verify Required vaildation message for New E-Signature PIN textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on New E-Signature PIN
	Then : Verify that Required vaildation message shoudl be displayed as "New eSignature PIN is required."
	
@chrome 
Scenario: Verify the color of Required vaildation message for New E-Signature PIN textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on New E-Signature PIN
	Then : Verify that color of Required vaildation message should be "Red" in New eSignature PIN
	
@chrome  
Scenario: Verify validation message when New E-Signature PIN is same as Current E-Signature PIN
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on Current E-Signature PIN textbox
	And : Enter E-Signature PIN as "1410" in current E-Signature PIN
	And : Click on New E-Signature PIN
	And : Enter E-Signature PIN as "1410" in New E-Signature PIN
	Then : Verify that validation message should be displayed as Current eSignature PIN and New eSignature PIN cannot be same.
	
@chrome 
Scenario: Verify the color of validation message when New E-Signature PIN is same as Current E-Signature PIN
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on Current E-Signature PIN textbox
	And : Enter E-Signature PIN as "1410" in current E-Signature PIN
	And : Click on New E-Signature PIN
	And : Enter E-Signature PIN as "1410" in New E-Signature PIN
	Then : Verify that color of validation message should be "Red" in same Current and New eSignature
	
@chrome
Scenario: Verify Confirm PIN Mismatch validation message when New E-Signature PIN and Confirm E-Signature PIN is not same
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on New E-Signature PIN
	And : Enter E-Signature PIN as "1410" in New E-Signature PIN
	And : Click on Confirm New eSignature PIN textbox
	And : Enter E-Signature PIN as 1411 in Confirm E-Signature PIN
	Then : Verify that Confirm PIN Mismatch vaildation message should be displayed as "eSignature PIN mismatch."
	
@chrome 
Scenario: Verify the color of Confirm PIN Mismatch validation message
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on New E-Signature PIN
	And : Enter E-Signature PIN as "1410" in New E-Signature PIN
	And : Click on Confirm New eSignature PIN textbox
	And : Enter E-Signature PIN as 1411 in Confirm E-Signature PIN
	Then : Verify that color of Confirm PIN Mismatch vaildation message should be "Red"
	 
@chrome 
Scenario: Verify Required validation message for Confirm E-Signature PIN textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on Confirm New eSignature PIN textbox
	
	Then : Verify that Required vaildation message for Confirm E-Signature PIN should be displayed as "Confirm New eSignature PIN is required."
	
@chrome 
Scenario: Verify the color of Required validation message for Confirm E-Signature PIN textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on Confirm New eSignature PIN textbox
	Then : Verify that color of Required vaildation message for Confirm E-Signature PIN should be "Red"
	 
@chrome 
Scenario: Verify the Minimum vaildation message for New E-Signature PIN textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on New E-Signature PIN
	And : Enter less than 2 characters in New eSignature
	Then : Verify that Minimum vaildation message should be displayed as "New eSignature PIN must be at least 2 characters."
	
@chrome 
Scenario: Verify the color of Minimum vaildation message for New E-Signature PIN textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on New E-Signature PIN
	And : Enter less than 2 characters in New eSignature
	Then : Verify that color of Minimum vaildation message for New eSignature should be "Red"

@chrome 
Scenario: Verify the Maximum vaildation message for New E-Signature PIN textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on New E-Signature PIN
	And : Enter more than 50 characters in New eSignature
	Then : Verify that Maximum vaildation message should be displayed for New eSignature as "New eSignature PIN cannot be more than 50 characters."
	
@chrome 
Scenario: Verify the color of Maximum vaildation message for New E-Signature PIN textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on New E-Signature PIN
	And : Enter more than 50 characters in New eSignature
	Then : Verify that color of Maximum vaildation message for New eSignature should be "Red" 
	
@chrome 
Scenario: Verify the Minimum vaildation message for Confirm New E-Signature PIN textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on Confirm New eSignature PIN textbox
	And : Enter less than 2 characters in Confirm eSignature
	Then : Verify that Minimum vaildation message for Confirm eSignature should be displayed as "Confirm eSignature PIN must be at least 2 characters."
	
@chrome  
Scenario: Verify the color of Minimum vaildation message for Confirm New E-Signature PIN textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on Confirm New eSignature PIN textbox
	And : Enter less than 2 characters in Confirm eSignature 
	Then : Verify that color of Minimum vaildation message should for Confirm eSignature be "Red"
	
@chrome  
Scenario: Verify the Maximum vaildation message for Confirm New E-Signature PIN textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on Confirm New eSignature PIN textbox
	And : Enter more than 50 characters in Confirm eSignature 
	Then : Verify that Maximum vaildation message for Confirm New E-Signature PIN should be displayed as "Confirm New eSignature PIN cannot be more than 50 characters."
	
@chrome  
Scenario: Verify the color of Maximum vaildation message for Confirm New E-Signature PIN textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on Confirm New eSignature PIN textbox
	And : Enter more than 50 characters in Confirm eSignature
	Then : Verify that color of Maximum vaildation message should for Confirm eSignature be "Red"

@chrome 
Scenario: Verify the General error validation message for Change E-Signature PIN when user click on Save button without add any record
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on "Save" Button of Change E-Signature PIN
	Then : Verify that General error vaildation message should be displayed as "You must complete all required fields marked with (*)."

@chrome 
Scenario: Verify the color of General error validation message for Change E-Signature PIN 
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
	And : Click on "Save" Button of Change E-Signature PIN
	Then : Verify that color of General error vaildation message should be "Red"

@chrome
Scenario: Verify that Cancel button is clickable for Change E-Signature PIN
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Username from Home Page top right corner
   	And : Click on My Profile link
   	And : Click on Change E-Signature PIN Button
   	And : Click on Cancel Button of Change E-Signature PIN
	Then : Verify that Cancel Button should be clickable when user click on Cancel button