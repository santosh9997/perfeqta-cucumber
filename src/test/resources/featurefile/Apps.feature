Feature: Module Name: Apps
Executed script on "https://test1.beperfeqta.com/mdav33"
		
@chrome 
Scenario: Verify the data should be passed to created app
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Apps Tile
    And : Click on Search App of Apps Module
    And : Enter valid app name of App Module search field
    And : Click on Other Apps tile 
    And : Click on Searched app
    And : Enter valid data to necessary fields
    And : Click on save button of Apps screen
    Then : Verify app data is stored on app
    
@chrome
Scenario: Verify App is Searched after getting App Name from Search Window
 	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Apps Tile
    And : Click on Search App of Apps Module
    And : Enter valid app name of App Module search field for Search Window
		And : Click on Other Apps tile 
    And : Click on Searched app
    And : Enter valid data to necessary fields
    And : Click on save button of Apps screen
    Then : Verify app data is stored on app
    
    
@chrome 
Scenario: Verify the data should be failed to created app
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Apps Tile
    And : Click on Search App of Apps Module
    And : Enter valid app name of App Module search field
    And : Click on Other Apps tile 
    And : Click on Searched app
    And : Enter invalid data to necessary fields
    And : Click on save button of Apps screen
    Then : Verify app data is stored on app
