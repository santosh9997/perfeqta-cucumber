Feature: Module Name: Import Data
Executed script on "https://test1.beperfeqta.com/mdav33"
	
@chrome 
Scenario: Verify the module title when user click on "Import Data" module
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
    Then : Verify module name as Import Data
	
@chrome 
Scenario: Verify BreadCrumb for Import Data Page
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	Then : Verify that system should display BreadCrumb as "Home > Import Data"
 
@chrome 
Scenario: Verify Manual Import Page is Load functionality
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	Then : Verify that system should load the Manual Import page Properly
	
@chrome 
Scenario: Verify BreadCrumb for Manual Import Page
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	Then : Verify that system should display BreadCrumb after click on manual import as "Home > Import Data > Manual Import"
	
@chrome 
Scenario: Verify Upload CSV File Label in Manual Import Page
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	Then : Verify that system should display label as "Upload CSV File" on Manual Import Page
	
@chrome 
Scenario: Verify Required Validation message for Module Field
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	And : Select dropdown of Module and Press "TAB" Key
	Then : Verify that system should display required validation message as "Module is required."
	
@chrome 
Scenario: Verify Color of Required Validation message for Module Field
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	And : Select dropdown of Module and Press "TAB" Key
	Then : Verify that Color of required validation message for module field should be red
	
@chrome
Scenario: Verify Required Validation message for Type Field
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	And : Select dropdown of Type and Press "TAB" Key
	Then : Verify that system should display required validation message as "Type is required." in Type Drop Down
	
@chrome
Scenario: Verify Color of Required Validation message for Type Field
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	And : Select dropdown of Type and Press "TAB" Key
	Then : Verify that Color of required validation message for type field should be red
	
@chrome 
Scenario: Verify Required Validation message for Mapping Name Field
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	And : Click on Mapping Name textbox
	And : Press "TAB" Key of Import Data 
	Then : Verify that system should display required validation message as "Mapping Name is required." in Mapping Name textbox
	
@chrome 
Scenario: Verify Color of Required Validation message for Mapping Name Field
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	And : Click on Mapping Name textbox
	And : Press "TAB" Key of Import Data
	Then : Verify that Color of required validation message for Mapping Name field should be red
	
@chrome 
Scenario: Verify Unique Validation message for Mapping Name Field
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	And : Click on Mapping Name textbox
	And : Enter Existing Mapping Name into textbox
	Then : Verify that system should display unique validation message as "Mapping Name must be unique."
	
@chrome 
Scenario: Verify Color of Unique Validation message for Mapping Name Field
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	And : Click on Mapping Name textbox
	And : Enter Existing Mapping Name into textbox
	Then : Verify that Color of unique validation message for Mapping Name field should be red
	
@chrome 
Scenario: Verify Minimum Validation message for Mapping Name Field
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	And : Click on Mapping Name textbox
	And : Enter Mapping Name as "T"
	Then : Verify that system should display minimum validation message as "Mapping Name should be at least 2 characters long."

@chrome 
Scenario: Verify Color of Minimum Validation message for Mapping Name Field
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	And : Click on Mapping Name textbox
	And : Enter Mapping Name as "T"
	Then : Verify that Color of minimum validation message for Mapping Name field should be red
	
@chrome 
Scenario: Verify Maximum Validation message for Mapping Name Field
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	And : Click on Mapping Name textbox
	And : Enter Mapping Name value more than 50 characters
	Then : Verify that system should display maximum validation message as "Mapping Name cannot be more than 50 characters long."

@chrome 
Scenario: Verify Color of Maximum Validation message for Mapping Name Field
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	And : Click on Mapping Name textbox
	And : Enter Mapping Name value more than 50 characters
	Then : Verify that Color of maximum validation message for Mapping Name field should be red
	
@chrome 
Scenario: Verfiy Color of Upload CSV Button
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	Then : Verfiy that Color of Upload CSV button should be green
	
@chrome 
Scenario: Verfiy Color of Cancel Button
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	Then : Verfiy that Color of Cancel button should be black
	
#No implimentation
@chrome @ignore
Scenario: Verify Upload CSV File functionality
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	And : Click on Upload Document from Local Drive Icon
	And : Selet CSV file
	And : Click on Open button
	Then : Verfiy that system is allowed to upload CSV file by click on Upload Document from Local Drive Icon
	
@chrome 
Scenario: Verfiy Cancel button functionality
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on Manual Import Tile
	And : Click on Cancel button in Manual Import
	Then : Verfiy that system should redirect to the Home Screen of Import Data Page when a user click on Cancel button
	
@chrome 
Scenario: Verify View Saved Mapping Page is Load functionality
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on View Saved Mapping Tile
	Then : Verify that system should load the View Saved Mapping page Properly
	
@chrome @ignore
Scenario: Verify Pagination for View Saved Mapping Screen
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on View Saved Mapping Tile
	Then : Verify the record count
	
@chrome 
Scenario: Verify pagination when user click on First
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on View Saved Mapping Tile	
    And : Click on the First button of Pagination
    Then : Verify that the system should display the first page of the listing screen	

@chrome 
Scenario: Verify pagination when user click on Last
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on View Saved Mapping Tile	
    And : Click on the Last button of Pagination
    Then : Verify that the system should display the last page of the listing screen
	
@chrome 
Scenario: Verify Pagination for Audit Trail Page
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on View Saved Mapping Tile	
	And : Click on View Audit Trail link of first record from the Audit Trail column
	Then : Verify the record count


@chrome  @ignore
Scenario: Verify the Ascending order functionality for Mapping Name column of View Saved Mapping screen
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
  	And : Click on View Saved Mapping Tile	
   Then : Verify that all the records of Mapping Name column display in ascending order when click on sorting icon

		
#yet to be implemented
@chrome  @ignore
Scenario: Verify the Descending order functionality for Mapping Name column of View Saved Mapping screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Import Data Tile
	And : Click on View Saved Mapping Tile	
	And : Click on Sorting icon of the Mapping Name column for descending order sorting
    Then : Verify that all the records of Mapping Name column display in descending order
	
@chrome 
Scenario: Verify Search Functionality for View Saved Mapping screen
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
  And : Click on Import Data Tile
	And : Click on View Saved Mapping Tile
	And : Enter the data into search box
    Then : Enter the Mapping Name into search box of View Saved Mapping screen
	

	

	

	

	
	


	

	
