Feature: Module Name: BatchScenario
Executed script on "https://test1.beperfeqta.com/mdav33" 
	
@chrome   
Scenario: Verify Copy Site Selections functionality for Apps.
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile
  And : Select Module 
  And : Select App
  And : Clicking on Enable App for Batch Entry 
  And : Checked Copy Site Selections option 
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search App in Apps
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Enter valid data to necessary fields
  And : Copy Site name from Site
  And : Click on save button of Apps screen
  Then : Verify Site Selection should be Copied when fill second record
  
  @chrome 
  Scenario: Verify Copy Non Key Attributes functionality for Apps.
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Go Back to Home
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile 
  And : Select Module 
  And : Select App which have unchecked key attribute
  And : Clicking on Enable App for Batch Entry 
  And : Checked Copy Non Key Attributes option and Save Batch Entry 
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search App which have unchecked key attribute
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Enter valid data to necessary fields
  And : Click on save button of Apps screen
  Then : Non Key Attributes should be Copied when fill second record.
  
  @chrome 
  Scenario: Verify Copy Non Key Entities Attributes functionality for Apps
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Go Back to Home
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile 
  And : Select Module 
  And : Select App which have unchecked key attribute
  And : Clicking on Enable App for Batch Entry
  And : Click on checkbox of Copy Non Key Entities Attributes option
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search App in Apps
  And : Click on Other Apps tile
  And : Search App which have unchecked key attribute
  And : Enter valid data to necessary fields
  And : Click on save button of Apps screen
  Then : Verify Non Key Entities Attributes should be Copied when fill second record.
   
   @chrome   
  Scenario: Verify Start App in Default Batch Mode functionality for Apps.
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile 
  And : Select Module 
  And : Select App
  And : Clicking on Enable App for Batch Entry
  And : Click on Start App in Default Batch mode Checkbox  
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search App in Apps
  And : Click on Other Apps tile
  And : Click on Searched app
  Then : By default records are fill in batch mode.
  
  @chrome   
  Scenario: Verify YYYY/MM/DD - Auto Increment Batch Entry ID Format.
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile 
  And : Select YYYY/MM/DD - Auto Increment format
  And : Select Module 
  And : Select App
  And : Clicking on Enable App for Batch Entry
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search App in Apps
  And : Click on Other Apps tile
  And : Click on Searched app
  Then : verify Batch Entry should be display "YYYY/MM/DD" Auto Increment format
  
  @chrome   
 Scenario: Verify MM/DD/YYYY - Auto Increment Batch Entry ID Format.
 Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile
  And : Select MM/DD/YYYY - Auto Increment format checkbox
  And : Select Module 
  And : Select App
  And : Clicking on Enable App for Batch Entry
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search App in Apps
  And : Click on Other Apps tile
  And : Click on Searched app
  Then : verify Batch Entry should be display "MM/DD/YYYY" Auto Increment format MM/DD/YYYY
  
  @chrome   
  Scenario: Verify Custom - Auto Increment Batch Entry ID Format.
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile
  And : Select custom Auto Increment format checkbox
  And : Get value Custom value from Custom Input 
  And : Select Module 
  And : Select App
  And : Clicking on Enable App for Batch Entry
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search App in Apps
  And : Click on Other Apps tile
  And : Click on Searched app
  Then : Batch Entry should be display Custom - Auto Increment format.
 
 @chrome   
  Scenario: Verify Auto Increment Batch Entry ID Format.
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile
  And : Select Auto Increment format checkbox
  And : Get increment value from increment Input 
  And : Select Module 
  And : Select App
  And : Clicking on Enable App for Batch Entry
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search App in Apps
  And : Click on Other Apps tile
  And : Click on Searched app
 Then : Batch Entry should be display Auto Increment format.
 
 
 @chrome   
  Scenario: Verify Manual Batch Entry ID Format.
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile
  And : Select Manual checkbox
  And : Select Module 
  And : Select App
  And : Clicking on Enable App for Batch Entry
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search App in Apps
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Enter value in the Batch Entry ID
  Then : Batch Entry should be display Manual format.
 
 @chrome   
 Scenario: Verify the Batch table when do entry in Batch.
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Go Back to Home
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile 
  And : Select Module 
  And : Select App which have unchecked key attribute
  And : Clicking on Enable App for Batch Entry
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search App which have unchecked key attribute
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Click on batch table
  Then : Batch Table should be displayed.
  
   @chrome   
   Scenario: Verify Copy Site Selections functionality for Master Apps.
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
	   And : Click on Add New button of App Builder
	  And : Add All Required App Details in Basic Tab
	  And : Select Checkbox of Peer or Second Review.
	  And : Select Checkbox of Allow the Record Creator to Approve app.
	  And : Select Role from the Role dropdown.
	  And : Select Add button (All User should be display as selected by default).
	  And : Click on Continue button of basic tab
	  And : Add All Required Attribute Details in Attribute tab
	  And : Click on Continue button of attribute tab
	  And : Add all Required Workflow Details in Workflow tab
	  And : Click on Continue button of workflow tab
	  And : Add all required App Acceptance Criteria in Acceptance tab
	  And : Click on Continue button of acceptance tab
	  And : Click on Publish & Continue button
	  And : Click on Save button of permission tab
    And : Go Back to Home
	  And : Click on Administration Tile
	  And : Click on Master App Settings Tile
	  And : Click on "Add New" button of Master App listing Screen
	  And : Enter master Name
		And : Select Module for this Master App
		And : Select Multiple Apps for this Master App
		And : Click on Use in Master App Only Checkbox
		And : Click on Save Button of Master app
  And : Go Back to Home
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile 
  And : Select Module 
  And : Click on Master app radio Button
  And : Select Master App From DropDown
  And : Clicking on Enable App for Batch Entry
  And : Checked Copy Site Selections option
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search Master App  
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Copy Site selection value
  And : Enter Data in First App 
  And : Save First App Entry
  And : Click on Second App Icon
  And : Enter Data in second App
  And : Save second App Entry
  Then : Site Selection should be Copied when fill second record.
   
    @chrome    
   Scenario: Verify Copy Non Key Attributes functionality for Master Apps.
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
	   And : Click on Add New button of App Builder
	  And : Add All Required App Details in Basic Tab
	  And : Select Checkbox of Peer or Second Review.
	  And : Select Checkbox of Allow the Record Creator to Approve app.
	  And : Select Role from the Role dropdown.
	  And : Select Add button (All User should be display as selected by default).
	  And : Click on Continue button of basic tab
	  And : Add All Required Attribute Details in Attribute tab
	  And : Click on Continue button of attribute tab
	  And : Add all Required Workflow Details in Workflow tab
	  And : Click on Continue button of workflow tab
	  And : Add all required App Acceptance Criteria in Acceptance tab
	  And : Click on Continue button of acceptance tab
	  And : Click on Publish & Continue button
	  And : Click on Save button of permission tab
    And : Go Back to Home
	  And : Click on Administration Tile
	  And : Click on Master App Settings Tile
	  And : Click on "Add New" button of Master App listing Screen
	  And : Enter master Name
		And : Select Module for this Master App
		And : Select Multiple Apps for this Master App
		And : Click on Use in Master App Only Checkbox
		And : Click on Save Button of Master app
  And : Go Back to Home
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile 
  And : Select Module 
  And : Click on Master app radio Button
  And : Select Master App From DropDown
  And : Clicking on Enable App for Batch Entry
  And : Checked Copy Non Key Attributes option and Save Batch Entry 
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search Master App  
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Copy Site selection value
  And : Enter Data in First App 
  And : Save First App Entry
  And : Click on Second App Icon
  And : Enter Data in second App
  And : Save second App Entry
  And : Click on First App Icon
  Then : Non Key Attributes should be Copied when fill second record. For Master App
  
      @chrome    
   Scenario: Verify Copy Non Key Entities Attributes functionality for Master Apps.
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
	   And : Click on Add New button of App Builder
	  And : Add All Required App Details in Basic Tab
	  And : Select Checkbox of Peer or Second Review.
	  And : Select Checkbox of Allow the Record Creator to Approve app.
	  And : Select Role from the Role dropdown.
	  And : Select Add button (All User should be display as selected by default).
	  And : Click on Continue button of basic tab
	  And : Add All Required Attribute Details in Attribute tab
	  And : Click on Continue button of attribute tab
	  And : Add all Required Workflow Details in Workflow tab
	  And : Click on Continue button of workflow tab
	  And : Add all required App Acceptance Criteria in Acceptance tab
	  And : Click on Continue button of acceptance tab
	  And : Click on Publish & Continue button
	  And : Click on Save button of permission tab
    And : Go Back to Home
	  And : Click on Administration Tile
	  And : Click on Master App Settings Tile
	  And : Click on "Add New" button of Master App listing Screen
	  And : Enter master Name
		And : Select Module for this Master App
		And : Select Multiple Apps for this Master App
		And : Click on Use in Master App Only Checkbox
		And : Click on Save Button of Master app
  And : Go Back to Home
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile 
  And : Select Module 
  And : Click on Master app radio Button
  And : Select Master App From DropDown
  And : Clicking on Enable App for Batch Entry
  And : Click on checkbox of Copy Non Key Entities Attributes option
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search Master App  
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Copy Site selection value
  And : Enter Data in First App 
  And : Save First App Entry
  And : Click on Second App Icon
  And : Enter Data in second App
  And : Save second App Entry
  And : Click on First App Icon
  Then : Non Key Entities Attributes should be Copied when fill second record. For Master App
  
  
   @chrome    
   Scenario: Verify Copy Non Key Entities Attributes functionality for Master Apps.
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
	   And : Click on Add New button of App Builder
	  And : Add All Required App Details in Basic Tab
	  And : Select Checkbox of Peer or Second Review.
	  And : Select Checkbox of Allow the Record Creator to Approve app.
	  And : Select Role from the Role dropdown.
	  And : Select Add button (All User should be display as selected by default).
	  And : Click on Continue button of basic tab
	  And : Add All Required Attribute Details in Attribute tab
	  And : Click on Continue button of attribute tab
	  And : Add all Required Workflow Details in Workflow tab
	  And : Click on Continue button of workflow tab
	  And : Add all required App Acceptance Criteria in Acceptance tab
	  And : Click on Continue button of acceptance tab
	  And : Click on Publish & Continue button
	  And : Click on Save button of permission tab
    And : Go Back to Home
	  And : Click on Administration Tile
	  And : Click on Master App Settings Tile
	  And : Click on "Add New" button of Master App listing Screen
	  And : Enter master Name
		And : Select Module for this Master App
		And : Select Multiple Apps for this Master App
		And : Click on Use in Master App Only Checkbox
		And : Click on Save Button of Master app
  And : Go Back to Home
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile 
  And : Select Module 
  And : Click on Master app radio Button
  And : Select Master App From DropDown
  And : Clicking on Enable App for Batch Entry
  And : Click on Start App in Default Batch mode Checkbox
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search Master App  
  And : Click on Other Apps tile
  And : Click on Searched app
  Then : verify By default records are fill in batch mode for master App
  
  
    @chrome    
   Scenario: Verify YYYY/MM/DD - Auto Increment Batch Entry ID Format. For Master App
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
	   And : Click on Add New button of App Builder
	  And : Add All Required App Details in Basic Tab
	  And : Select Checkbox of Peer or Second Review.
	  And : Select Checkbox of Allow the Record Creator to Approve app.
	  And : Select Role from the Role dropdown.
	  And : Select Add button (All User should be display as selected by default).
	  And : Click on Continue button of basic tab
	  And : Add All Required Attribute Details in Attribute tab
	  And : Click on Continue button of attribute tab
	  And : Add all Required Workflow Details in Workflow tab
	  And : Click on Continue button of workflow tab
	  And : Add all required App Acceptance Criteria in Acceptance tab
	  And : Click on Continue button of acceptance tab
	  And : Click on Publish & Continue button
	  And : Click on Save button of permission tab
    And : Go Back to Home
	  And : Click on Administration Tile
	  And : Click on Master App Settings Tile
	  And : Click on "Add New" button of Master App listing Screen
	  And : Enter master Name
		And : Select Module for this Master App
		And : Select Multiple Apps for this Master App
		And : Click on Use in Master App Only Checkbox
		And : Click on Save Button of Master app
  And : Go Back to Home
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile
   And : Select YYYY/MM/DD - Auto Increment format 
  And : Select Module 
  And : Click on Master app radio Button
  And : Select Master App From DropDown
  And : Clicking on Enable App for Batch Entry
  And : Click on Start App in Default Batch mode Checkbox
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search Master App  
  And : Click on Other Apps tile
  And : Click on Searched app
  Then : verify Batch Entry should be display YYYY/MM/DD - Auto Increment format.Master App
  
      @chrome   
   Scenario: Verify MM/DD/YYYY - Auto Increment Batch Entry ID Format. for Master App
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
	   And : Click on Add New button of App Builder
	  And : Add All Required App Details in Basic Tab
	  And : Select Checkbox of Peer or Second Review.
	  And : Select Checkbox of Allow the Record Creator to Approve app.
	  And : Select Role from the Role dropdown.
	  And : Select Add button (All User should be display as selected by default).
	  And : Click on Continue button of basic tab
	  And : Add All Required Attribute Details in Attribute tab
	  And : Click on Continue button of attribute tab
	  And : Add all Required Workflow Details in Workflow tab
	  And : Click on Continue button of workflow tab
	  And : Add all required App Acceptance Criteria in Acceptance tab
	  And : Click on Continue button of acceptance tab
	  And : Click on Publish & Continue button
	  And : Click on Save button of permission tab
    And : Go Back to Home
	  And : Click on Administration Tile
	  And : Click on Master App Settings Tile
	  And : Click on "Add New" button of Master App listing Screen
	  And : Enter master Name
		And : Select Module for this Master App
		And : Select Multiple Apps for this Master App
		And : Click on Use in Master App Only Checkbox
		And : Click on Save Button of Master app
  And : Go Back to Home
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile
    And : Select MM/DD/YYYY - Auto Increment format checkbox
  And : Select Module 
  And : Click on Master app radio Button
  And : Select Master App From DropDown
  And : Clicking on Enable App for Batch Entry
  And : Click on Start App in Default Batch mode Checkbox
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search Master App  
  And : Click on Other Apps tile
  And : Click on Searched app
  Then : verify Batch Entry should be display MM/DD/YYYY - Auto Increment format for Master App
  
  @chrome  
  Scenario: Verify Custom - Auto Increment Batch Entry ID Format for master app
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
	   And : Click on Add New button of App Builder
	  And : Add All Required App Details in Basic Tab
	  And : Select Checkbox of Peer or Second Review.
	  And : Select Checkbox of Allow the Record Creator to Approve app.
	  And : Select Role from the Role dropdown.
	  And : Select Add button (All User should be display as selected by default).
	  And : Click on Continue button of basic tab
	  And : Add All Required Attribute Details in Attribute tab
	  And : Click on Continue button of attribute tab
	  And : Add all Required Workflow Details in Workflow tab
	  And : Click on Continue button of workflow tab
	  And : Add all required App Acceptance Criteria in Acceptance tab
	  And : Click on Continue button of acceptance tab
	  And : Click on Publish & Continue button
	  And : Click on Save button of permission tab
    And : Go Back to Home
	  And : Click on Administration Tile
	  And : Click on Master App Settings Tile
	  And : Click on "Add New" button of Master App listing Screen
	  And : Enter master Name
		And : Select Module for this Master App
		And : Select Multiple Apps for this Master App
		And : Click on Use in Master App Only Checkbox
		And : Click on Save Button of Master app
  And : Go Back to Home
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile
    And : Select custom Auto Increment format checkbox
   And : Get value Custom value from Custom Input
  And : Select Module 
  And : Click on Master app radio Button
  And : Select Master App From DropDown
  And : Clicking on Enable App for Batch Entry
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search Master App  
  And : Click on Other Apps tile
  And : Click on Searched app
  Then : verify Batch Entry should be display Custom - Auto Increment format for Master App
  
  @chrome   
  Scenario: Verify Auto Increment Batch Entry ID Format for master app
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
	   And : Click on Add New button of App Builder
	  And : Add All Required App Details in Basic Tab
	  And : Select Checkbox of Peer or Second Review.
	  And : Select Checkbox of Allow the Record Creator to Approve app.
	  And : Select Role from the Role dropdown.
	  And : Select Add button (All User should be display as selected by default).
	  And : Click on Continue button of basic tab
	  And : Add All Required Attribute Details in Attribute tab
	  And : Click on Continue button of attribute tab
	  And : Add all Required Workflow Details in Workflow tab
	  And : Click on Continue button of workflow tab
	  And : Add all required App Acceptance Criteria in Acceptance tab
	  And : Click on Continue button of acceptance tab
	  And : Click on Publish & Continue button
	  And : Click on Save button of permission tab
    And : Go Back to Home
	  And : Click on Administration Tile
	  And : Click on Master App Settings Tile
	  And : Click on "Add New" button of Master App listing Screen
	  And : Enter master Name
		And : Select Module for this Master App
		And : Select Multiple Apps for this Master App
		And : Click on Use in Master App Only Checkbox
		And : Click on Save Button of Master app
  And : Go Back to Home
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile
 And : Select Auto Increment format checkbox
  And : Get increment value from increment Input
  And : Select Module 
  And : Click on Master app radio Button
  And : Select Master App From DropDown
  And : Clicking on Enable App for Batch Entry
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search Master App  
  And : Click on Other Apps tile
  And : Click on Searched app
  Then : verify Batch Entry should be display Auto Increment format for Master App
  
  
   @chrome  
  Scenario: Verify Manual Batch Entry ID Format for Master App
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
	   And : Click on Add New button of App Builder
	  And : Add All Required App Details in Basic Tab
	  And : Select Checkbox of Peer or Second Review.
	  And : Select Checkbox of Allow the Record Creator to Approve app.
	  And : Select Role from the Role dropdown.
	  And : Select Add button (All User should be display as selected by default).
	  And : Click on Continue button of basic tab
	  And : Add All Required Attribute Details in Attribute tab
	  And : Click on Continue button of attribute tab
	  And : Add all Required Workflow Details in Workflow tab
	  And : Click on Continue button of workflow tab
	  And : Add all required App Acceptance Criteria in Acceptance tab
	  And : Click on Continue button of acceptance tab
	  And : Click on Publish & Continue button
	  And : Click on Save button of permission tab
    And : Go Back to Home
	  And : Click on Administration Tile
	  And : Click on Master App Settings Tile
	  And : Click on "Add New" button of Master App listing Screen
	  And : Enter master Name
		And : Select Module for this Master App
		And : Select Multiple Apps for this Master App
		And : Click on Use in Master App Only Checkbox
		And : Click on Save Button of Master app
  And : Go Back to Home
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile
  And : Select Manual checkbox
  And : Select Module 
  And : Click on Master app radio Button
  And : Select Master App From DropDown
  And : Clicking on Enable App for Batch Entry
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search Master App  
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Enter value in the Batch Entry ID
  Then : verify Batch Entry should be display Manual format for Master App
  
   @chrome 
  Scenario: Verify the Batch table when do entry in Batch for Master App
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
	   And : Click on Add New button of App Builder
	  And : Add All Required App Details in Basic Tab
	  And : Select Checkbox of Peer or Second Review.
	  And : Select Checkbox of Allow the Record Creator to Approve app.
	  And : Select Role from the Role dropdown.
	  And : Select Add button (All User should be display as selected by default).
	  And : Click on Continue button of basic tab
	  And : Add All Required Attribute Details in Attribute tab
	  And : Click on Continue button of attribute tab
	  And : Add all Required Workflow Details in Workflow tab
	  And : Click on Continue button of workflow tab
	  And : Add all required App Acceptance Criteria in Acceptance tab
	  And : Click on Continue button of acceptance tab
	  And : Click on Publish & Continue button
	  And : Click on Save button of permission tab
    And : Go Back to Home
	  And : Click on Administration Tile
	  And : Click on Master App Settings Tile
	  And : Click on "Add New" button of Master App listing Screen
	  And : Enter master Name
		And : Select Module for this Master App
		And : Select Multiple Apps for this Master App
		And : Click on Use in Master App Only Checkbox
		And : Click on Save Button of Master app
  And : Go Back to Home
  And : Click on Administration Tile
  And : Click on Batch Entry Settings Tile
  And : Select Module 
  And : Click on Master app radio Button
  And : Select Master App From DropDown
  And : Clicking on Enable App for Batch Entry
  And : Save Batch Entry
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Search Master App  
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Click on batch table
  Then : verify Batch Table should be displayed for Master App
  
