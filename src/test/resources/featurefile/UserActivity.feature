Feature: Module Name: User Activity
Executed script on "https://test1.beperfeqta.com/mdav33" 
	
@chrome
Scenario: Verify the module title when user click on "User Activity" module
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on User Activity Tile
    Then : Verify the module name user activity as "User Activity"
	
@chrome
Scenario: verify Default Tab of "User Activity" module
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on User Activity Tile
    Then : Verify that "User Logging Activity" Tab should be displayed bydefault when user click on User Activity Tile

 
 @chrome
  Scenario: Verify pagination of "User Logging Activity" listing page when user click on "Last"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on User Activity Tile
    And : Click on the Last button of Pagination
    Then : Verify that the system should display the last page of the listing screen

@chrome
  Scenario: Verify pagination of "User Logging Activity" listing page when user click on "First"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on User Activity Tile
     And : Click on the Last button of Pagination
    And : Click on the First button of Pagination
    Then : Verify that the system should display the first page of the listing screen 
@chrome @ignore
  Scenario: Verify total number of entries with pagination count of User Logging Activity tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on User Activity Tile
    Then : Verify total number of entries should be match with Pagination count of Site Listing tab
	
	
 

@chrome
  Scenario: Verify Cancel button functionality of Logged in Users tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on User Activity Tile
    And : Click on Logged in Users tab
    And : Click on "Cancel" Button in loggedin user
    Then : Verify that system should be redirect to the Administration Screen when a user click on the Cancel button


@chrome @ignore
  Scenario: Verify the Search functionality of "User Logging Activity" listing screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on User Activity Tile
    And : Enter the Username into search box of User Logging Activity listing screen
    Then : Verify the search result of User Logging Activity listing Screen
	
@chrome @ignore
  Scenario: Verify the Ascending order for the Username column of User Logging Activity tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on User Activity Tile
    And : Click on Sorting icon of the Username column  ascending order
     Then : Verify that all the records of Site Name column display in ascending order  User Logging Activity

@chrome @ignore
  Scenario: Verify the Descending order for the Username  column of User Logging Activity tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on User Activity Tile
    And : Click on Sorting icon of the Username column for descending order Logging Activity 
    Then : Verify that all the records of Username column display in descending order

	
@chrome
  Scenario: Verify Navigation functionality of Logged in User
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on User Activity Tile
    And : Click on Logged in Users tab
    Then : Verify that system should be redirect to the Logged in User listing screen when user click on Logged in User Tab
@chrome
Scenario: Verify Logout functionality from Logged in User 
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on User Activity Tile
     And : Click on Logged in Users tab
	   And : Click on Logout link for perticuler Logged in User
    Then : Verify that Logged in User should be Logout and record of that User should be removed from the Logged in User listing 