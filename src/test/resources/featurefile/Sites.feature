Feature: Module Name: Sites
Executed script on "https://test1.beperfeqta.com/mdav33" 

  @chrome
  Scenario: Verify the module title when user click on "Sites" module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    Then : Verify the module name as Sites

  @chrome
  Scenario: Verify the Information message of site level page when user click on "Sites" module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on Site Level Name tab
    Then : Verify that system should display information message as Only Sites defined through bottom level will display in App.

  @chrome 
  Scenario: Verify the Search functionality of "Sites" module listing screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Enter the Sites name into search box of Sites listing screen
    Then : Verify the search result in site listing screen

  @chrome
  Scenario: Verify the Remove functionality of "Sites" module listing screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Enter the Sites name into search box of Sites listing screen
    And : Click on remove button of search textbox
    Then : Verify that Sites name should be removed from search box when user press remove button

  @chrome @demo
  Scenario: Verify pagination of "Sites" listing page when user click on "Last"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on the Last button of Pagination
    Then : Verify that the system should display the last page of the listing screen

  @chrome 
  Scenario: Verify pagination of "Sites" listing page when user click on "First"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on the First button of Pagination
    Then : Verify that the system should display the first page of the listing screen

  @chrome 
  Scenario: Verify total number of entries with pagination count of Site Level Name tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on Site Level Name tab
    Then : Verify total number of entries should be match with pagination count of Site Level Name tab

  @chrome @ignore
  Scenario: Verify the Ascending order for the Site Name column of Site Level Name tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on Site Level Name tab
    And : Click on Sorting icon of the Site Name column ascending order
    Then : Verify that all the records of Site Name column display in ascending order

#yet to write code
  @chrome @ignore 
  Scenario: Verify the Descending order for the Site Name column of Site Level Name tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on Site Level Name tab
    And : Click on Sorting icon of the Site Name column for descending order
    Then : Verify that all the records of Site Name column display in descending order

  @chrome
  Scenario: Verify Add / Edit label on Site Page
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on Site listing page
    And : Click on First Site from Site listing page
    Then : Verify that Add / Edit Site page should be displayed when a user click on Site name

  @chrome
  Scenario: Verify Save button Color of Site Level Name tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on Site Level Name tab
    And : Click on "Add New" button of site Level Name
    Then : Verify that color of Save button should be green

  @chrome
  Scenario: Verify Cancel button Color of Site Level Name tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on Site Level Name tab
    And : Click on "Add New" button of site Level Name
    Then : Verify that color of Cancel button should be black

  @chrome
  Scenario: Verify Cancel button functionality of Site Level Name tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on Site Level Name tab
    And : Click on First Site name of site level name
     And : Click on cancel btn of First Site name of site level name
    Then : Verify that system should be redirect to the Site Level Name screen when a user click on the Cancel button

  @chrome  
  Scenario: Verify Audit Trail Page of Site Listing tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on View Audit Trail link of the first record
    Then : Verify that system should be redirect to the Audit Trail screen when a user click on the View Audit Trail link of the first record

  @chrome
  Scenario: Verify Audit Trail Pagination of Site Listing tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on View Audit Trail link of the first record
    Then : Verify total number of entries should be match with Pagination count of Site Listing tab

  @chrome
  Scenario: Verify Audit Trail Back button color for Site Listing Screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
     And : Click on View Audit Trail link of the first record Site Listing Screen
    Then : Verify that color of Cancel button should be black in View Audit screen

  @chrome
  Scenario: Verify Audit Trail back button functionality for Site Listing Screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on View Audit Trail link of the first record Site Listing Screen
    And : Click on the Cancel button in View Audit Screen
    Then : Verify that system should be redirect to the Site Listing screen when a user click on the Cancel button in View Audit Screen

  @chrome
  Scenario: Verify Site Level Name Tab Navigation
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on Site Level Name tab
    Then : Verify that system should be redirect to the Site Level Name Screen when a user click on the Site Level tab

  @chrome
  Scenario: Verify View Audit Trail Page of Site Level Name tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on Site Level Name tab
    And : Click on View Audit Trail link of the first record in Site Level Name
    Then : Verify that system should be redirect to the Audit Trail screen when a user click on the View Audit Trail link of the first record

  @chrome @common
  Scenario: Verify Audit Trail Pagination of Site Level Name tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on Site Level Name tab
    And : Click on View Audit Trail link of the first record site level name
    Then : Verify total number of entries should be match with pagination of Audit Trail Page for Site Level Name tab

  @chrome
  Scenario: Verify Audit Trail Back button color for Audi Trail Page of Site Level Name tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on Site Level Name tab
     And : Click on View Audit Trail link of the first record in Site Level Name
    Then : Verify that color of Cancel button should be black in Audit Trail of site level name

  @chrome
  Scenario: Verify Audit Trail back button functionality for Site Level Name tab
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Sites Tile
    And : Click on Site Level Name tab
    And : Click on View Audit Trail link of the first record in Site Level Name
    And : Click on the Cancel button in View Audit of site level name
    Then : Verify that system should be redirect to the Listing screen of Site Level Name tab when a user click on the Cancel button
