Feature: Module Name: Reports
Executed script on "https://test1.beperfeqta.com/mdav33" 
@chrome
Scenario: Verify Generate Report functionality in Report 
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen 
	And : Select App of Reports screen 
	And : Click on Date option Dropdown of Reports screen of Reports screen
	And : Select Date Type of Reports screen
	And : Click on Generate Report Button
	Then : Verify that Report should be generated in Reort screen
	
@chrome
Scenario: Verify Reports shall be generated as per the selected Test Type (Procedure)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Dropdown of Procedure Test Type
	And : Select Procedure into the Dropdown list
	And : Click on Date option Dropdown of Reports screen
	And : Select Date Type  of Reports screen
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per the selected Test Type (Procedure)
	
@chrome
Scenario: Verify Reports shall be generated as per the selected Test Type (Questions)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Question Radio button
	And : Click on Dropdown of Question
	And : Select Questionin Dropdown list
	And : Click on Date option Dropdown of Reports screen
	And : Select Date Type of Reports screen
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per the selected Test Type (Question)	
	
@chrome
Scenario: Verify Reports shall be generated as per the selected Window Type (Binomial)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on  Window Type Dropdown of Reports screen 
	And : Click on Checkbox of Binomial Window Type in Dropdown list of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select Date Type of Reports screen
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per the Binomial Window Type
	
@chrome
Scenario: Verify Reports shall be generated as per the selected Window Type (Hypergeomatric)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on  Window Type Dropdown of Reports screen 
	And : Click on Checkbox of Hypergeomatric Window Type in Dropdown list of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select Date Type of Reports screen
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per the Hypergeomatric Window Type
	
@chrome
Scenario: Verify Reports shall be generated as per the selected Window Type (Customized)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on  Window Type Dropdown of Reports screen 
	And : Click on Checkbox of Customized Window Type in Dropdown list of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select Date Type of Reports screen
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per the Customized Window Type
@chrome		
Scenario: Verify Reports shall be generated as per the selected Date Options (Today)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select Today option in Date dropdown list of Reports screen
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per the Today Date Options
	
@chrome		
Scenario: Verify Reports shall be generated as per the selected Date Options (This Month)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Generate Report Button
	Then : Verify Reports shoul be generated as per the This Month Date Options 
	
@chrome
Scenario: Verify Reports shall be generated as per the selected Date Options (This Week)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Week option in Date dropdown list of Reports screen
	And : Click on Generate Report Button
	Then : Verify Reports shoul be generated as per the This Week Date Options 
	
@chrome
Scenario: Verify Reports shall be generated as per the selected Date Options (Specific Date)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select Specific Date option in Date dropdown list of Reports screen
	And : Click on Date Picker of Reports screen
	And : Select Date into the Date Picker of Reports screen
	And : Click on Generate Report Button
	Then : Verify Reports shoul be generated as per the Specific Date Options
	
	
@chrome
Scenario: Verify Reports shall be generated as per the selected Date Options (Month-Year)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Select Month-Year Radio Button of Reports screen
	And : Click on select Month Dropdown of of Reports screen
	And : Select Month in Dropdown list of Reports screen
	And : Click on Select Year Dropdown of Reports screen
	And : Select Year in Dropdown list of Reports screen
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per the Month-Year Date Options
	
@chrome
Scenario: Verify Reports shall be generated as per the selected Date Options (Date Range)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Select Date Range Radio Button of Reports screen
	And : Click on Date Picker of Starting Date of Reports screen
	And : Select Date into the Date Picker of Reports screen
	And : Clcik on Date Picker of Ending Date of Reports screen
	And : Select Date into the Date Picker of Reports screen
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per the Date Range Date Options
	
@chrome
Scenario: Verify Reports shall be generated as per the selected Attributes/Entity Key Attributes Filter (Collection Date)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Filters option of Reports screen
	And : Click on Attributes/Entity Key Attributes Dropdown of Reports screen
	And : Select Collection Date in Dropdown list of Reports screen
	And : Click on Date Picker of Collection Date of Reports screen
	And : Select Date into the Date Picker of Reports screen
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per the Collection Date Attributes/Entity Key Attributes Filter  

@chrome
Scenario: Verify Reports shall be generated as per the selected Attributes/Entity Key Attributes Filter (DIN)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Filters option of Reports screen
	And : Click on Attributes/Entity Key Attributes Dropdown of Reports screen
	And : Select DIN in Dropdown list of Reports screen
	And : Click on Textbox of DIN of Reports screen
	And : Enter Numeric Value in Textbox of DIN of Reports screen
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per the DIN Attributes/Entity Key Attributes Filter 
	
@chrome
Scenario: Verify Reports shall be generated as per the selected Attributes/Entity Key Attributes Filter (DIN)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Filters option of Reports screen
	And : Click on Attributes/Entity Key Attributes Dropdown of Reports screen
	And : Select DIN in Dropdown list of Reports screen
	And : Click on Textbox of DIN of Reports screen
	And : Enter Numeric Value in Textbox of DIN of Reports screen
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per the DIN Attributes/Entity Key Attributes Filter
	
@chrome
Scenario: Verify Reports shall be generated as per the selected Attributes/Entity Key Attributes Filter (Reagent Master-Lot Number)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Filters option of Reports screen
	And : Click on Attributes/Entity Key Attributes Dropdown of Reports screen
	And : Select Reagent Master-Lot Number in Dropdown list of Reports screen
	And : Click on Dropdown of Reagent Master-Lot Number of Reports screen
	And : Select Value into the Reagent Master-Lot Number dropdown liat of Reports screen
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per the DIN Attributes/Entity Key Attributes Filter
	
	
@chrome
Scenario: Verify Reports shall be generated as per Transfusion Service option 
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Filters option of Reports screen
	And : Click on Site Selections option into the Filter of Reports screen
	And : Click on Department Dropdown in Site Selections of Reports screen
	And : Select Transfusion Service option into the Dropdown list of Department Dropdown
	And : Click on Site Dropdown into the Site Selections of Reports screen
	And : Select First option into the Dropdown list of Site Dropdown
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per the Transfusion Service option 	
	
@chrome
Scenario: Verify Reports shall be generated as per Donor Operations option 
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Filters option of Reports screen
	And : Click on Site Selections option into the Filter of Reports screen
	And : Click on Department Dropdown in Site Selections of Reports screen
	And : Select Donor Operations option into the Dropdown list of Department Dropdown
	And : Click on Site Dropdown into the Site Selections of Reports screen
	And : Select First option into the Dropdown list of Site Dropdown
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per the Donor Operations option
	


@chrome
Scenario: Verify Reports shall be generated as per the selected Window Status Filter (Current)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Filters option of Reports screen
	And : Select Current checkbox of Window Status
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per Current Window Status Filter 
	
@chrome
Scenario: Verify Reports shall be generated as per the selected Window Status Filter (Stopped)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Filters option of Reports screen
	And : Select Stopped checkbox of Window Status
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per Stopped Window Status Filter 

@chrome
Scenario: Verify Reports shall be generated as per the selected Window Status Filter (Failed)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Filters option of Reports screen
	And : Select Failed checkbox of Window Status
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per Failed Window Status Filter 
	

@chrome
Scenario: Verify Reports shall be generated as per the selected Window Status Filter (Closed)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Filters option of Reports screen
	And : Select Closed checkbox of Window Status
	And : Click on Generate Report Button
	Then : Verify Reports should be generated as per Closed Window Status Filter 
	
@chrome
Scenario: Verify the Save Report Functionality of Reports screen
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Textbox of Report Name of Reports screen
	And : Enter the name in Textbox of Reports Name
	And : Click on Save Report Button
	Then : Verify that Report should be Saved when Click on Save Report Button
	
@chrome
Scenario: Verify the Report Name field functionalty with the less than 2 characters
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Textbox of Report Name of Reports screen
	And : Enter One character in Textbox of Reports Name
	Then : Verify that "Report Name must be at least 2 characters." Validation message should be displayed in Reports screen

@chrome
Scenario: Verify the Report Name field functionalty with the More than 50 characters
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Textbox of Report Name of Reports screen
	And : Enter more than 50 characters in Textbox of Reports Name
	Then : Verify that "Report Name cannot be more than 50 characters." Validation message should be displayed in Reports screen
	
@chrome
Scenario: Verify the Report Name field functionalty with the 50 characters
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Textbox of Report Name of Reports screen
	And : Enter 50 characters in Textbox of Reports Name
	And : Click on Save Report Button
	Then : Verify that System is allow to Save the Report in Reports screen
	

@chrome
Scenario: Verify the Saved Search section from the QP Reports
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen
	And : Select App of Reports screen
	And : Click on Date option Dropdown of Reports screen
	And : Select This Month option in Date dropdown list of Reports screen
	And : Click on Textbox of Report Name of Reports screen
	And : Enter the name in Textbox of Reports Name
	And : Click on Save Report Button
	Then : Verify that Report should be displayed with Entered Name in Saved Search section from QP Reports in Reports screen
	
@chrome
Scenario: Verify the search functionality of the saved search reports
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on serchbox of saved reports in Reports screen
	And : Enter report name in Reports screen
	Then : Verify that functionality of saved search reports for Reports screen should be working properly
	
	

@chrome
Scenario: Verify the Ascending order for the Window Reports column under Saved Reports listing
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Sorting icon of the Window Reports column of Reports screen
    Then : Verify that all the records of Window Reports column should be displayed in ascending order
	
@chrome
Scenario:  Verify the Descending order for the Window Reports column under Saved Reports listing
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Reports Tile
	And : Click on Sorting icon of the Window Reports column of Reports screen
  Then : Verify that all the records of Window Reports column should be displayed in descending order
	

@chrome
Scenario: Verify the remove functionality from the saved search reports in Reports screen
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Remove button from saved Window Reports listing in Reports screen
	And : Click on Yes Button from the Popup of  Remove report in Reports screen
	Then : Verify that Report should be removed when click on Yes button from Popup in Report screen
	
@chrome
Scenario: Verify the clear all button functionality of Reports screen
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen 
	And : Select App of Reports screen 
	And : Click on Date option Dropdown of Reports screen of Reports screen
	And : elect This Month option in Date dropdown list of Reports screen
	And : Click on Generate Report Button
	And : Click on Clear All Button of Reports screen
	Then : Verify the functionality of clear all button in Reports screen should be working properly

	

@chrome
Scenario: Verify the cancel button functionality of Reports screen
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Cancel Button of Reports screen
	Then : Verify that System should redirected to the Previous page when Click on Cancel button of Reports screen
	
	
@chrome
Scenario: Verify the Generated Reports data  in Reports screen
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen 
	And : Select App of Reports screen 
	And : Click on Date option Dropdown of Reports screen of Reports screen
	And : elect This Month option in Date dropdown list of Reports screen
	And : Click on Generate Report Button
	Then : Verify that data of Generated Reports should be displayd properly in Reports screen


@chrome
Scenario: Verify the show record details link functionality from the generated report in Reports screen
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen 
	And : Select App of Reports screen 
	And : Click on Date option Dropdown of Reports screen of Reports screen
	And : elect This Month option in Date dropdown list of Reports screen
	And : Click on Generate Report Button
	And : Click on Show record details link from the generated report in Reports screen
	Then : Verify the show record details link should be working properly in Reports screen
	
@chrome
Scenario: Verify the hide record details link functionality from the generated report in Reports screen
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen 
	And : Select App of Reports screen 
	And : Click on Date option Dropdown of Reports screen of Reports screen
	And : elect This Month option in Date dropdown list of Reports screen
	And : Click on Generate Report Button
	And : Click on Show record details link from the generated report in Reports screen
	And : Click on Hide record details link from the generated report in Reports screen
	Then : Verify the Hide records details link should be working properly in Reports screen
	
@chrome
Scenario: Verify the Load more record  link functionality from the generated report in Reports screen
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen 
	And : Select App of Reports screen 
	And : Click on Date option Dropdown of Reports screen of Reports screen
	And : elect This Month option in Date dropdown list of Reports screen
	And : Click on Generate Report Button
	And : Click on Load more record link from the generated report in Reports screen	
	Then : Verify the Load more record link should be working properly in Reports screen
	
	

@chrome
Scenario: Verify the Window status
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Search Window Tile
	And : Click on Module Dropdown of Search Window screen
	And : Select Module of Search Window screen
	And : Click on App Dropdown of Search Window screen 
	And : Select App of Search Window screen 
	And : Click on Search Button of Search Window screen
	And : Click on Perticuler ID into the ID column of Searched Window listing of Search Window screen
	And : Get the Window Status in Window detail screen
	And : Click on Qualification Performance from the Bread crumb
	And : Click on Reports Tile
	And : And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen 
	And : Select App of Reports screen 
	And : Click on Date option Dropdown of Reports screen of Reports screen
	And : elect This Month option in Date dropdown list of Reports screen
	And : Click on Generate Report Button
	Then : Verify the Window status into Reports screen
	
	
@chrome
Scenario: Verify the Export to PDF functionality
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen 
	And : Select App of Reports screen 
	And : Click on Date option Dropdown of Reports screen of Reports screen
	And : elect This Month option in Date dropdown list of Reports screen
	And : Click on Generate Report Button
	And : Click on Export PDF icon in Reports screen
	And : Click on Ok button for Export to PDF in Reports screen
	Then : Verify that Export to PDF functionality should be working properly in Reports screen
	

@chrome
Scenario: Verify the Export to Excel functionality
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen 
	And : Select App of Reports screen 
	And : Click on Date option Dropdown of Reports screen of Reports screen
	And : elect This Month option in Date dropdown list of Reports screen
	And : Click on Generate Report Button
	And : Click on Export to Excel icon in Reports screen
	And : Click on Ok button for Export to Excel in Reports screen
	Then : Verify that Export to Excel functionality should be working properly in Reports screen
	

@chrome
Scenario: Verify Requested Files link functionality in Reports screen
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen 
	And : Select App of Reports screen 
	And : Click on Date option Dropdown of Reports screen of Reports screen
	And : elect This Month option in Date dropdown list of Reports screen
	And : Click on Generate Report Button
	And : Click on Requested Files link in Reports screen
	Then : Verify that system should redirected to the Files listing screen when clcik on Requested Files link from the Reports screen
	
@chrome
Scenario: Verify Download link functionality from the Requested Files screen
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Qualification Performance Tile
	And : Click on Reports Tile
	And : Click on Module Dropdown of Reports screen
	And : Select Module of Reports screen
	And : Click on App Dropdown of Reports screen 
	And : Select App of Reports screen 
	And : Click on Date option Dropdown of Reports screen of Reports screen
	And : elect This Month option in Date dropdown list of Reports screen
	And : Click on Generate Report Button
	And : Click on Requested Files link in Reports screen
	And : Click on Download link into the Action coumn from Files listing screen
	Then : Verify that Download functionality should be working properly from the Requested Files screen