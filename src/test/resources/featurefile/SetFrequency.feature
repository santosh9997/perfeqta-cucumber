Feature: Module Name: Set Frequency
Executed script on "https://test1.beperfeqta.com/mdav33" 
	
@chrome 
Scenario: Verify the module title when user click on "Set Frequency" module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Set Frequency Tile
    Then : Verify the module name as Set Frequency
	
@chrome @ignore
Scenario: Verify Pagination of Set Frequency Screen
	  Given : Navigate to the URL
  	And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Set Frequency Tile
   Then : Verify total number of entries should be match with pagination of Set Frequency screen
	
@chrome 
Scenario: Verify pagination of Set Frequency page when user click on "Last"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Set Frequency Tile
    And : Click on the Last button of Pagination 
    Then : Verify that the system should displayed the last page of the listing screen
   
@chrome 
Scenario: Verify pagination of Set Frequency page when user click on "First"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Set Frequency Tile
    And : Click on the Last button of Pagination 
	And : Click on the First button of Pagination 
    Then : Verify that the system should displayed the first page of the listing screen

@chrome 
Scenario: Verify the navigation of Audit trail page of Set Frequency screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
	And : Click on "View Audit Trail" link of the first record of the Sets listing screen
    Then : Verify that system should displayed Audit Trail page when user click on "View Audit Trail" link of the Set listing screen	

@chrome @ignore
Scenario: Verify the Ascending order for the Frequency Name column under Set Frequency page
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Set Frequency Tile
    Then : Verify that all the records of Screen should be display in ascending order as per Frequency Name
	
@chrome @ignore
Scenario: Verify the Descending order for the Frequency Name column under Set Frequency page
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Set Frequency Tile
    And : Click on Sorting icon of the Frequency Name column
    Then : Verify that all the records of Screen should be display in descending order as per Frequency Name

@chrome 
Scenario: Verify the search functionality for Set Frequency screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Enter the data into search box of Set Frequency Screen 
	Then : Verify the search results of Set Frequency screen
	
@chrome 
Scenario: Verify Click of Add New button 
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	Then : Verify that system should redirect to the Add/Edit Frequency screen when user click on Add New button
	
@chrome 
Scenario: Verify Required validation message for Frequency Name Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
    And : Click on Frequency Name textbox 
	And : Press the "TAB" Key in Frequency Name textbox
    Then : Verify the validation message as Frequency Name is required.

@chrome 
Scenario: Verify Minimum validation message of Frequency Name Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
    And : Click on Frequency Name textbox 
    And : Enter Frequency Name value Less than 2 chracters
    Then : Verify the validation message as Frequency Name must be at least 2 characters.
	
@chrome  
Scenario: Verify Maximum validation message of Frequency Name Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
    And : Click on Frequency Name textbox 
    And : Enter Frequency Name value more than 50 characters
    Then : Verify validation message Frequency Name cannot be more than 50 characters long.
	
@chrome 
Scenario: Verify Unique validation message of Frequency Name Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
    And : Click on Frequency Name textbox 
    And : Enter Duplicate Frequency Name
    Then : Verify Unique validation message Frequency Name must be unique.
	
@chrome 
Scenario: Verify Frequency Name text box with valid data
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
    And : Click on Frequency Name textbox 
    And : Enter a valid Frequency Name
    Then : Verify system should accept the Frequency name value without any validation message
	
@chrome 
Scenario: Verify Required Validaion message for App radio button
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	And : Select dropdown of Apps and Press "TAB" Key
	Then : Verify the validation message as App is required.
	
@chrome 
Scenario: Verify Required Validaion message for Master App radio button
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	And : Select Master App radio button
	And : Select dropdown of Master App and Press "TAB" Key
	Then : Verify the validation message as Master App is required.
	
@chrome 
Scenario: Verify Hourly label text for Frequency type
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	Then : Verify that label of hourly frequency type should be displayed as "Hourly"
	
@chrome 
Scenario: Verify Daily label text for Frequency type
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	Then : Verify that label of daily frequency type should be displayed as "Daily"
	
@chrome 
Scenario: Verify Weekly label text for Frequency type
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	Then : Verify that label of weekly frequency type should be displayed as "Weekly"
	
@chrome 
Scenario: Verify Monthly label text for Frequency type
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	Then : Verify that label of Monthly frequency type should be displayed as "Monthly"
	
@chrome 
Scenario: Verify Start Date Picker functionality 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	And : Select the Date from the houlry Start Date Picker
	Then : Verify that user should able to select Date for Start Date
	
@chrome 
Scenario: Verify Required Validation message for Every Textbox of Hourly Frequency Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen 
	And : Click on Every textbox for Hourly type
	And : Press "TAB" Key on Every text box for Hourly type 
	Then : Verify that system should display required message as "Every Hour(s) is required."
	
@chrome 
Scenario: Verify Maximum Validation message for Every Textbox of Hourly Frequency Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen 
	And : Click on Every textbox for Hourly type
	And : Enter Every textbox Value as "32"
	Then : Verify that system should display maximum Hour validation message as "Invalid Number. You cannot add more than 24 Hours."

@chrome 
Scenario: Verify Invalid Validation message for Every Textbox of Hourly Frequency Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen 
	And : Click on Every textbox for Hourly type
	And : Enter Every textbox Value as string
	Then : Verify that system should display invalid validation message as "Invalid Number."
	
	 
@chrome 
Scenario: Verify No Validation message for Every Textbox of Hourly Frequency Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	And : Click on Every textbox for Hourly type
	And : Enter Valid Value in Every textbox 
	Then : Verify that system should not display any validation message when a user enter Valid value into textbox

@chrome 
Scenario: Verify Required Validation message for Every Textbox of Weekly Frequency Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen 
	And : Select Weekly radio button for Weekly Frequency type 
	And : Click on Every textbox of weekly type
	And : Press "TAB" Key on Weekly Frequency type
	Then : Verify that system should display required message for No input data as "Every Week(s) is required."
	
@chrome 
Scenario: Verify Maximum Validation message for Every Textbox of Weekly Frequency Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen 
	And : Select Weekly radio button for Weekly Frequency type
	And : Click on Every textbox of weekly type 
	And : Enter Every textbox Value for more than 10 week as "32"
	Then : Verify that system should display maximum validation message for weekly type as "Invalid Number. You cannot add more than 10 weeks."
	
@chrome
Scenario: Verify Invalid Validation message for Every Textbox of Weekly Frequency Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen 
	And : Select Weekly radio button for Weekly Frequency type
	And : Click on Every textbox of weekly type
	And : Enter Every textbox Value for string as "e"
	Then : Verify that system should display invalid validation message for weekly type as "Invalid Number."
	
@chrome 
Scenario: Verify No Validation message for Every Textbox of Weekly Frequency Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	And : Select Weekly radio button for Weekly Frequency type
	And : Enter Valid Value in Every textbox for weekly type
	Then : Verify that system should not display any validation message when a user enter Valid value into textbox of Weekly type
	
@chrome 
Scenario: Verify Required Validation message for Every Textbox of Daily Frequency Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen 
	And : Select Daily radio button for Daily Frequency type 
	And : Click on Every textbox of Daily type
	And : Press "TAB" Key on Daily Type
	Then : Verify that system should display required message for Daily Type as "Every Day(s) is required."
	
@chrome
Scenario: Verify Maximum Validation message for Every Textbox of Daily Frequency Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen 
	And : Select Daily radio button for Daily Frequency type
	And : Click on Every textbox of Daily type
	And : Enter Every textbox Value for days as "32"
	Then : Verify that system should display maximum validation message for days as "Invalid Number. You cannot add more than 31 days."

@chrome 
Scenario: Verify Invalid Validation message for Every Textbox of Daily Frequency Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen 
	And : Select Daily radio button for Daily Frequency type
	And : Enter Every textbox Value for days in string as "e"
	Then : Verify that system should display invalid validation message for days as "Invalid Number."
	
	
@chrome 
Scenario: Verify No Validation message for Every Textbox of Daily Frequency Type
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	And : Select Daily radio button for Daily Frequency type
	And : Enter Valid Value in Every textbox for days
	Then : Verify that system should not display any validation message when a user enter Valid value into textbox for Daily type
	
@chrome 
Scenario: Verify End date picker functionality 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	And : Select Date from End the Date Picker 
	Then : Verify that user should able to select Date for End Date in Set Frequency Add new button
	
@chrome 
Scenario: Verify Required validation message for End After(Occurrences) textbox
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	And : Select End After radio button
	And : Click on End After(Occurrences) textbox
	And : Press "TAB" Key on End After(Occurrences) textbox
	Then : Verify that system should display required message for no data as "End After(Occurrences) is required."
	
@chrome 
Scenario: Verify Maximum Validation message for End After(Occurrences) textbox
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen 
	And : Select End After radio button
	And : Click on End After(Occurrences) textbox
	And : Enter End textbox Value as "10000"
	Then : Verify that system should display maximum validation message for End After as "Invalid Number. Cannot add more than 9999 End After(Occurrences)."

@chrome 
Scenario: Verify Invalid Validation message for End After(Occurrences) textbox
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
   And : Click on Administration Tile
	And : Click on Set Frequency Tile
	And : Click on "Add New" button of Set Frequency screen
   And : Select End After radio button
	And : Click on End After(Occurrences) textbox
	And : Enter End After textbox Value as "0"
	Then : Verify that system should display invalid validation message for End After as "Invalid Number."
	
@chrome 
Scenario: Verify No Validation message for End After(Occurrences) textbox
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	And : Select End After radio button
	And : Click on End After(Occurrences) textbox
	And : Enter Valid Value in End After(Occurrences) textbox
	Then : Verify that system should not display any validation message when a user enter Valid value into textbox in End After Text Box
	
@chrome 
Scenario: Verify Current time is display for Frequency Time field
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	Then : Verify that system should display current time in frequency time field
	
@chrome 
Scenario: Verify Required validation message for Email Address textbox
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
	And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	And : Checked Send Email Reminder checkbox
	And : Click on Email Address textbox
	And : Press "TAB" Key on Email textbox
	Then : Verify that system should display required validation message for Email Address texbox as "Email Address is required."

@chromebox 
Scenario: Verify Invalid validation message for Email Address textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	And : Checked Send Email Reminder checkbox
	And : Click on Email Address textbox
	And : Enter Email Address as "abc12"
	Then : Verify that system should display invalid validation message for Invaild email Address as "Invalid Email Address."

@chrome 
Scenario: Verify No validation message for Email Address textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	And : Checked Send Email Reminder checkbox
	And : Click on Email Address textbox
	And : Enter Valid Email Address 
	Then : Verify that system should not display any validation message when user enter Valid Email Address

@chrome @demo
Scenario: Verify Create New Set Frequency functionality
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Set Frequency Tile
    And : Click on "Add New" button of Set Frequency screen
	And : Add Frequency Name for Frequency Name field
	And : Select App Name from App dropdown
	And : Select Frequency Type and Add required data
	And : Select Date from Start Date Picker
	And : Select Date from End Date Picker
	And : Select Role(s) and Click on Add button
	And : Click on Save button
	Then : Verify that system should create new set frequency by Click on Add New button

	