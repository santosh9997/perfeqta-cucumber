Feature: Module Name: Master App Settings
Executed script on "https://test1.beperfeqta.com/mdav33"
	
@chrome 
Scenario: Verify the module title when user click on "Master App Settings" module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
    Then : Verify the module name as Master App Settings
	
@chrome  
Scenario: Verify pagination of Master App Settings page when user click on "Last"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
    And : Click on the Last button of Pagination 
   Then : Verify that the system should display the last page of the listing screen

@chrome 
Scenario: Verify pagination of Master App Setting page when user click on "First"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
    And : Click on the Last button of Pagination 
	And : Click on the First button of Pagination 
   Then : Verify that the system should display the first page of the listing screen
   
@chrome 
Scenario: Verify pagination of Master App Setting page when user click on "Next" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Master App Settings Tile
	And : Click on Next button of Pagination 
	Then : Verify that the system should display the second page of the listing screen 
	
@chrome
Scenario: Verify pagination of Master App Setting page when user click on "Previous" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Master App Settings Tile
	And : Click on the Previous button of Pagination 
	Then : Verify that the system should display the first page of the listing screen  
	
@chrome @ignore
Scenario: Verify pagination of Master App Setting page
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
	Then : Verify the record count 
	
@chrome 
Scenario: Verify Edit Master App Setting Breadcrumb Functionality
	Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Master App Settings Tile
	And : Click on the first record of Master App listing grid 
	Then : Verify the Breadcrumb of Edit Master App Settings as "Home > Administration > Questions > Add / Edit Master App Settings" 
	
@chrome 
Scenario: Verify Save button Functionality
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
	And : Click on the first record of Master App listing grid 
		And : Click on Save button of Master Settings
	Then : Verify the Save button of Master App Setting should be clickable
	
@chrome 
Scenario: Verify Cancel button Functionality
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
	And : Click on the first record of Master App listing grid 
		And : Click on Cancel button of Master Settings
	Then : Verify the Cancel button should be clickable and redirect to Master App Setting listing grid cancel button
	
@chrome 
Scenario: Verify Color of Save button 
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
	And : Click on the first record of Master App listing grid 
	Then : Verify that Color of Save button should be green
	
@chrome 
Scenario: Verify Color of Cancel button 
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
	And : Click on the first record of Master App listing grid 
	Then : Verify that Color of Cancel button should be black
	
@chrome @ignore 
  Scenario: Verify the Search functionality of Master App Settings screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
    And : Enter the data into search box, which user want to search 
	Then : Verify the search results of Master App Settings screen

@chrome	@ignore
Scenario: Verify the Ascending order for the Master Apps column under Master App Settings screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
    And : Click on Sorting icon of the Master Apps column
    Then : Verify that all the records of Master Apps column display in ascending order when click on sorting icon
	
		#not implement
		
@chrome @ignore
 Scenario: Verify the Descending order for the Master Apps column under Master App Settings screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
    And : Click on Sorting icon of the Master Apps column
    Then : Verify that all the records of Master Apps column display in descending order
	
@chrome @ignore
Scenario: Verify pagination of Audit Trail 
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
	And : Click on "View Audit Trail" link of the first record of the listing page 
	Then : Verify the record count 
	
@chrome  
Scenario: Verify pagination of Master App Settings page when user click on "Last"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
    And : Click on "View Audit Trail" link of the first record of the listing page 
    And : Click on the Last button of Pagination 
   Then : Verify that the system should display the last page of the listing screen
   
@chrome  
Scenario: Verify pagination of Audit Trail when user click on "First"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile 
    And : Click on "View Audit Trail" link of the first record of the listing page 
    And : Click on the Last button of Pagination 
	  And : Click on the First button of Pagination 
   Then : Verify that the system should display the first page of the listing screen
   
      
@chrome  
Scenario: Verify pagination of Audit Trail when user click on "Next"
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Master App Settings Tile
	And : Click on "View Audit Trail" link of the first record of the listing page 
	And : Click on Next button of Pagination 
	Then : Verify that the system should display the second page of the listing screen 
	
@chrome  
Scenario: Verify pagination of Audit Trail when user click on "Previous"
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Master App Settings Tile
	And : Click on "View Audit Trail" link of the first record of the listing page
	And : Click on Next button of Pagination 
	And : Click on the Previous button of Pagination 
	Then : Verify that the system should display the first page of the listing screen  
   
@chrome  
Scenario: Verify Required validation message for Master App Field
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
	And : Click on "Add New" button of Master App listing Screen
	And : Click on Master App Name textbox and press the "TAB" Key 
	Then : Verify that system should display required validation message for Master App field as "Master App is required."
	
@chrome  
Scenario: Verify Unique validation message for Master App Field
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
	And : Click on "Add New" button of Master App listing Screen
	And : Enter duplicate Master App Name 
	Then : Verify that system should display unique validation message for Master App field as "Master App must be unique."
	
@chrome  
Scenario: Verify Mininum validation message for Master App Field
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
	And : Click on "Add New" button of Master App listing Screen
	And : Enter Master App Name value less than 2 chracters 
	Then : Verify that system should display minimum validation message for Master App field as "Master App must be at least 2 characters."
	
@chrome  
Scenario: Verify Maximum validation message for Master Add Field
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
	And : Click on "Add New" button of Master App listing Screen
	And : Enter Master App Name value more than 50 characters
	Then : Verify that system should display maximum validation message for Master App field as "Master App cannot be more than 50 characters."

@chrome  
Scenario: Verify Master App Name text box with valid data 
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
	And : Click on "Add New" button of Master App listing Screen
	And : Enter a valid Master App Name 
	Then : Verify system should accept the Master App name value without any validation message. 

@chrome  
Scenario: Verify Add New Master App Functionality
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Master App Settings Tile
	And : Click on "Add New" button of Master App listing Screen
		And : Add valid Master App data for all fields and click on the save button
	Then : Verify that user is able to Add New master app 



	

 


	
	



