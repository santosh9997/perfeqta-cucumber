Feature: Module Name: Sets
Executed script on "https://test1.beperfeqta.com/mdav33" 
    
@chrome
Scenario: Verify the module title when user click on "Sets" module
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    Then : Verify the module name as Sets
@chrome
Scenario: Verify pagination of Sets listing page when user click on "First"
  Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
   	And : Click on the Last button of Pagination 
	And : Click on the First button of Pagination 
   Then : Verify that the system should display the first page of the listing screen 

@chrome
Scenario: Verify pagination of Sets listing page when user click on "Next"
  Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on Next button of Pagination
    Then : Verify that the system should display the second page of the listing screen

@chrome
Scenario: Verify pagination of Sets listing page when user click on "Previous"
  Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
	And : Click on Next button of Pagination 
	And : Click on the Previous button of Pagination 
   Then : Verify that the system should display the first page of the listing screen 
@chrome
Scenario: Verify pagination of Sets listing page when user click on "Last"
  Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on the Last button of Pagination 
   Then : Verify that the system should display the last page of the listing screen 
@chrome
Scenario: Verify the records count of Sets listing screen
  Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
   Then : Verify the record count 
@chrome 
Scenario: Verify pagination of Audit trail screen of Sets module when user click on "First" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile
	And : Click on Sets Tile
	And : Click on "View Audit Trail" link of the first record of the listing page 
	And : Click on the Last button of Pagination 
	And : Click on the First button of Pagination 
   Then : Verify that the system should display the first page of the Audit trail screen 
@chrome 
Scenario: Verify pagination of Audit trail screen of Sets module when user click on "Next" 
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Sets Tile
	And : Click on "View Audit Trail" link of the first record of the listing page 
	And : Click on Next button of Pagination 
   Then : Verify that the system should display the second page of the Audit trail screen 
@chrome 
Scenario: Verify pagination of Audit trail screen of Sets module when user click on "Previous" 
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Sets Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
	And : Click on Next button of Pagination 
	And : Click on the Previous button of Pagination 
   Then : Verify that the system should display the previous page of the Audit trail screen 
@chrome 
Scenario: Verify pagination of Audit trail screen of Sets module when user click on "Last" 
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Sets Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
	And : Click on the Last button of Pagination 
   Then : Verify that the system should display the last page of the Audit trail screen 
@chrome @ignore
Scenario: Verify the record count of Audit trail screen of Attribute module 
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Sets Tile
	And : Click on "View Audit Trail" link of the first record of the listing page 
   Then : Verify the record count 
@chrome
Scenario: Verify Link Information Column when user click on View link of the Sets listing screen
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
		And : Click on View Link in Link Information Column of the Sets listing screen
		Then : Verify that system should displayed Popup page Link Information Column when user click on View link of Sets listing screen

@chrome
Scenario: Verify the navigation of Audit trail page of Sets screen
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
	  And : Click on "View Audit Trail" link of the first record of the Sets listing screen
    Then : Verify that system should displayed Audit Trail page when user click on "View Audit Trail" link of the Set listing screen

@chrome @ignore
Scenario: Verify the Ascending order for the Sets column under Sets listing page
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on Sorting icon of the Sets column for ascending
    Then : Verify that all the records of Sets column display in ascending order

@chrome @ignore
Scenario: Verify the Descending order for the Sets column under Sets listing page
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on Sorting icon of the Sets column for descending
    Then : Verify that all the records of Sets column display in descending order

@chrome
Scenario: Verify Link Information Popup when user click on View link of the Sets listing screen
    Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
	And : Click on View Link in Link Information Column of the Sets listing screen
	Then : Verify that system should displayed Popup page Link Information Column when user click on View link of Sets listing screen

@chrome
Scenario: Verify the Search functionality of Sets module listing screen
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Enter the Sets name into search box of Sets listing screen
    Then : Verify the search result of sets

@chrome
Scenario: Verify the current version of Sets when user trying to add first Sets
	Given : Navigate to the URL  
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
	Then : Verify the Current version in Add/Sets screen as "Current Version 1.00"

@chrome
Scenario: Verify Required validation message for Set Name textbox of Sets module
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
	And : Click on Set Name textbox and press "TAB" key
   Then : Verify that system should displayed required validation message for Set Name textbox as "Set Name is required."

@chrome
Scenario: Verify Minimum validation message for Set Name textbox of Sets module
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
    And : Enter Set Name value less than 2 characters
    Then : Verify that system should displayed Minimum validation message of Set Name textbox as "Set Name must be at least 2 characters."

@chrome
Scenario: Verify Maximum validation message for Set Name textbox of Sets module
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
    And : Enter Set Name value more than 50 characters
    Then : Verify that system should displayed Maximum validation message of Set Name textbox as "Set Value cannot be more than 50 characters long."

@chrome
Scenario: Verify validation message for starts with Alphabet characters of Set Name textbox of Sets module
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
    And : Enter Set Name as special characters
    Then : Verify that system should displayed Alphabet validation message of Set Name textbox as "Set Name should start with alphabets only."

@chrome
Scenario: Verify Validation for valid data in Set Name textbox of Sets module
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
    And : Enter Set Name as valid data
	Then : Verify that system is not displayed any validation message when user enter valid data in Set Name textbox of Sets module

@chrome
Scenario: Verify Unique validation message of Set Name Textbox
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile 
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
    And : Add Duplicate Set Name
    Then : Verify that system should displayed validation message for Duplicate Set Name as "Set Name must be unique."

@chrome
Scenario: Verify Add Set value functionality for Adding Set of Add/Edit Set Screen
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
    And : Enter Set Name as valid data
		And : Add value of Set in Enter Set Value textbox
		And : Click on "Add" button
   Then : Verify that system should add the Value of set when user click on Add button of Add set screen

@chrome
Scenario: Verify default status of Toggle button in Add Set screen  
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
   Then : Verify that Toggle button should be Active by default

@chrome 
Scenario: Verify Inactive Toggle button functionality of Set screen  
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
		And : Click on Toggle button and make it as Inactive
   Then : Verify that Toggle button should be Inactive in Set screen

@chrome
Scenario: Verify functionality of Cancel button of Add Set screen of Sets module
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
    And : Click on Cancel button of Add Set Screen
	 Then : Verify that system should be redirect to the Sets listing screen when user click on Cancel button

@chrome
Scenario: Verify checkbox is clickable of Add Set Screen
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
		And : Enter Set Name as valid data
		And : Add value of Set in Enter Set Value textbox
		And : Click on "Add" button
		And : Checked and Unchecked by clicking checkbox of added set value
	 Then : Verify that Checkbox should be clickable properly when user Checked and Unchecked 

@chrome
Scenario: Verify Edit functionality of Set Value of Add Set Screen 
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button 
    And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
		And : Enter Set Name as valid data 
		And : Add value of Set in Enter Set Value textbox
		And : Click on "Add" button
		And : Click on Edit link of Action field
		And : Edit the set value
		And : Click on "Add" button
	 Then : Verify that Edit functionality should be working properly when user Edit set value

@chrome 
Scenario:  Verify Delete functionality of Set Value of Add Set Screen 
	Given : Navigate to the URL 
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile 
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
		And : Enter Set Name as valid data
		And : Add value of Set in Enter Set Value textbox
		And : Click on "Add" button
		And : click on Delete link to delete set value
	Then : Verify that Delete functionality should be working properly when user Delete set value

@chrome @ignore
Scenario: Verify the Ascending order for the Set value column under Add Set screen
    Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
    And : Enter Set Name as valid data
		And : Add multiple set values
		And : Click on Ascending icon
    Then : Verify that all the records of Set value column should be displayed in ascending order

@chrome @ignore
Scenario: Verify the Descending order for the Set value column under Add Set screen
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
    And : Enter Set Name as valid data
		And : Add multiple set values
		And : Click on Descending icon 
    Then : Verify that all the records of Set value column should be displayed in descending order

@chrome
Scenario: Verify functionality of Move Down button for records of set value in Add set Screen
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
     And : Enter Set Name as valid data
		And : Add multiple set values
		And : Click on Move Down button
    Then : Verify that record of set value should be move to the Next row when user click on Move Down Button

@chrome
Scenario: Verify functionality of Move Up button for records of set value in Add set Screen
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
    And : Enter Set Name as valid data
		And : Add multiple set values
		And : Click on Move Up button
    Then : Verify that record of set value should be move to the Previous row when user click on Move Up Button


	 
@chrome @ignore
Scenario: Verify the Ascending order for Existing Set in Add Set screen
    Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button 
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
    And : Enter Set Name in textbox of Set Name
	And : Select Radio button of Add from existing set
	And : Select value in Existing set from dropdown list
	And : Click on Move arrow 
	And : Click on Ascending icon 
    Then : Verify that all the records of Existing Set value column should be displayed in ascending order

	
@chrome @ignore
Scenario: Verify the Descending order for Existing Set in Add Set screen
    Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button 
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
    And : Enter Set Name in textbox of Set Name
		And : Select Radio button of Add from existing set
		And : Select value in Existing set from dropdown list
		And : Click on Move arrow 
		And : Click on Descending icon 
    Then : Verify that all the records of Existing Set value column should be displayed in descending order

@chrome 
Scenario: Verify functionality of Move Down button for Existing Set in Add Set Screen
    Given : Navigate to the URL
		And : Enter valid credentials and Click on Login Button
		And : Click on Administration Tile 
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
    And : Enter Set Name as valid data
		And : Select Radio button of Add from existing set
		And : Select value in Existing set from dropdown list
		And : Click on Move arrow
	  Then : Verify that record of existing set value should be move to the Next row when user click on Move Down Button

@chrome @demo
Scenario: Verify functionality of Move UP button for Existing Set in Add Set Screen
    Given : Navigate to the URL
		And : Enter valid credentials and Click on Login Button
		And : Click on Administration Tile 
    And : Click on Sets Tile
    And : Click on "Add New" button of Sets listing screen
    And : Enter Set Name as valid data
		And : Select Radio button of Add from existing set
		And : Select value in Existing set from dropdown list
		And : Click on Move arrow 
    Then : Verify that record of existing set value should be move to the Previous row when user click on Move Up Button