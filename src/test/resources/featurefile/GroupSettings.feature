Feature: Module Name: Group Settings
Executed script on "https://test1.beperfeqta.com/mdav33"

@chrome 
Scenario: Verify the module title when user click on "Group Settings" module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Group Settings Tile
    Then : Verify the module name as Group Settings
	
@chrome @ignore
Scenario: Verify pagination of Group Settings Screen
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Group Settings Tile
	Then : Verify total number of entries should be match with Pagination count of Site Listing tab
	
@chrome 
Scenario: Verify pagination of Group Settings page when user click on "Last"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Group Settings Tile
    And : Click on the Last button of Pagination 
   Then : Verify that the system should display the last page of the listing screen
   
@chrome
Scenario: Verify pagination of Group Setting page when user click on "First"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Group Settings Tile
    And : Click on the Last button of Pagination 
	And : Click on the First button of Pagination 
   Then : Verify that the system should display the first page of the listing screen
   
@chrome 
Scenario: Verify the navigation of Audit trail page of Group Settings screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Group Settings Tile
	And : Click on "View Audit Trail" link of the first record of the Group Settings screen
    Then : Verify that system should be redirect to the Audit Trail screen when a user click on the View Audit Trail link of the first record
   
@chrome @ignore
Scenario: Verify the Ascending order for the Group Name column under Group Settings page
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Group Settings Tile
    And : Click on Group Name column
    Then : Verify that all the records of Site Name column display in ascending order Grup setting
	
@chrome @ignore
Scenario: Verify the Descending order for the Group Name column under Group Settings page
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Group Settings Tile
    And : Click on Sorting icon of the Group Name column
    Then : Verify that all the records of Group Name column display in descending order
	
@chrome @ignore
Scenario: Verify the Search functionality of Group Settings screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Group Settings Tile
    And : Enter the data into search box, which user want to search 
	Then : Verify the search results of Group Settings screen
	
@chrome
Scenario: Verify Click of Add New button 
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Group Settings Tile
	  And : Click on Add New button of Group Settings
	  Then : Verify that system should redirect to the Add / Edit Group Settings screen when user click on Add New button
	
@chrome
Scenario: Verify Required validation message for Group Name Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Group Settings Tile
    And : Click on Add New button of Group Settings
    And : Click on Group Name textbox 
	  And : Press the "TAB" Key
    Then : Verify the validation message as Group Name is required.

@chrome
Scenario: Verify Minimum validation message of Group Name Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Group Settings Tile
    And : Click on Add New button of Group Settings
    And : Click on Group Name textbox 
    And : Enter Group Name value Less than 2 chracters
    Then : Verify the validation message as Group Name must be at least 2 characters.

@chrome
Scenario: Verify Maximum validation message of Group Name Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Group Settings Tile
    And : Click on Add New button of Group Settings
    And : Click on Group Name textbox 
    And : Enter Group Name value more than 50 characters
    Then : Verify validation message Group Name cannot be more than 50 characters long.
	
@chrome
Scenario: Verify Unique validation message of Group Name Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Group Settings Tile
    And : Click on Add New button of Group Settings
    And : Click on Group Name textbox 
    And : Enter Duplicate Group Name
    Then : Verify Unique validation message Group Name must be unique.

@chrome
Scenario: Verify Group Name text box with valid data
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Group Settings Tile
     And : Click on Add New button of Group Settings
    And : Click on Group Name textbox 
    And : Enter a valid Group Name
    Then : Verify system should accept the Group name value without any validation message
	
@chrome
Scenario: Verify CheckAll option functionality for Module field
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Group Settings Tile
   And : Click on Add New button of Group Settings
	And : Enter a valid Group Name
	And : Click on Module dropdown
	And : Click to Check All option
	Then : Verify that all the module name should be selected
	
@chrome
Scenario: Verify UncheckAll option functionality for Module field
	Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
     And : Click on Administration Tile
	And : Click on Group Settings Tile
    And : Click on Add New button of Group Settings
	And : Enter a valid Group Name
	And : Click on Module dropdown
	And : Click to Check All option
	And : Click on UnCheck All option
	Then : Verify that all the module name should not be selected
	
@chrome
Scenario: Verify Add Apps functionality
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
	And : Click on Group Settings Tile
  And : Click on Add New button of Group Settings
	And : Enter a valid Group Name
	And : Click on Module dropdown
	And : select Module from dropdown
	And : Select App from list
	And : Click on Move button
	Then : Verify that by click on Move button selected App should be added to the Group
	
@chrome @ignore
Scenario: Verify Ascending order Sorting functionality for Apps
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
     And : Click on Administration Tile
	And : Click on Group Settings Tile
    And : Click on Add New button of Group Settings
	And : Enter a valid Group Name
	And : Click on Module dropdown
	And : select Module from dropdown
	And : Select App from list
	And : Click on Move button
	And : Click on Ascending icon
	Then : Verify that All added Apps should be displayed in ascending order

@chrome @ignore	
Scenario: Verify Descending order Sorting functionality for Added App(s)
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Group Settings Tile
   And : Click on Add New button of Group Settings
	And : Enter a valid Group Name
	And : Click on Module dropdown
	And : select Module from dropdown
	And : Select App from list
	And : Click on Move button
	And : Click on Descending icon 
	Then : Verify that All added Apps should be displayed in descending order
	And : Select multiple Apps from list
	And : Click on Move Down button
	
@chrome
Scenario: Verify functionality of Move Down button for Added App(s) 
    Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Group Settings Tile
    And : Click on Add New button of Group Settings
	And : Enter a valid Group Name
	And : Click on Module dropdown
	And : select Module from dropdown
	And : Select multiple Apps from list
	And : Click on Move Down button of added grp
    Then : Verify that Selected App should be move to the Next row when user click on Move Down Button
	
@chrome
Scenario: Verify functionality of Move Up button for Added App(s) 
    Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Group Settings Tile
    And : Click on Add New button of Group Settings
	And : Enter a valid Group Name
	And : Click on Module dropdown
	And : select Module from dropdown
	And : Select multiple Apps from list
	And : Click on Move Up button of added grp
    Then : Verify that Selected App should be move to the Previous row when user click on Move Up Button

@chrome
Scenario: Verify Color of Save Button
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Group Settings Tile
    And : Click on Add New button of Group Settings
	Then : Verify that color of Save button should be green group setting screen
	
@chrome 
Scenario: Verify Color of Cancel Button
 Given : Navigate to the URL
   And : Enter valid credentials and Click on Login Button
   And : Click on Administration Tile
   And : Click on Group Settings Tile
   And : Click on Add New button of Group Settings
  Then : Verify that color of Cancel button should be black group setting screen
	
@chrome
Scenario: Verify Cancel button functionality 
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
	And : Click on Group Settings Tile
    And : Click on Add New button of Group Settings
    And : Enter a valid Group Name
	And : Click on Cancel button of Group Settings
   Then : Verify that system should redirect to the Group Name listing screen when user click on the Cancel button

	



	
	

	



	