Feature: Module Name: Questions

@chrome 
Scenario: Verify the module title when user click on "Questions" module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	Then : Verify the module name as Questions 
	
@chrome  
Scenario: Verify the "Filter By" dropdown functionality of Questions module listing screen 
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on Filter By dropdown 
	Then : Verify the Filter By dropdown should be clickable 
	
@chrome  
Scenario: Verify the "Question Type" dropdown  functionality of Questions module listing screen 
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on Filter By dropdown and select Question Type option 
	And : Click on Question Type dropdown 
	Then : Verify the Question Type dropdown should be clickable 
	
@chrome  
Scenario: Verify pagination of Questions listing page when user click on "Last"
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the Last button of Pagination 
	Then : Verify that the system should display the last page of the listing screen 
	
@chrome  
Scenario: Verify pagination of Questions listing page when user click on "First" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the Last button of Pagination 
	And : Click on the First button of Pagination 
	Then : Verify that the system should display the first page of the listing screen 
	
@chrome  
Scenario: Verify pagination of Questions listing page when user click on "Next" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on Next button of Pagination 
	Then : Verify that the system should display the second page of the listing screen 
	
@chrome  
Scenario: Verify pagination of Questions listing page when user click on "Previous" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on Next button of Pagination 
	And : Click on the Previous button of Pagination 
	Then : Verify that the system should display the first page of the listing screen 
	
@chrome @ignore
Scenario: Verify the record count of Questions listing screen 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	Then : Verify the record count 
	
@chrome  @ignore
Scenario: Verify the Ascending order for the Questions column under Questions listing page 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on Sorting icon of the Questions column
	Then : Verify that all the records of Questions column display in ascending order when click on sorting icon 
	
	#not implement
@chrome  @ignore
Scenario: Verify the Descending order for the Questions column under Questions listing page 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on Sorting icon of the Questions column 
	Then : Verify that all the records of Questions column display in descending order 
	
@chrome @ignore 
Scenario: Verify the Search functionality of Questions module listing screen 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Enter the data into search box, which user want to search 
	Then : Verify the search results of Questions listing page 
	
@chrome 
Scenario: Verify the filter functionality with Auto Calculated Question Type from Question listing screen
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as Auto Calculated
   Then : Verify that the Question listing screen grid should display only Auto Calculated type of data
    
@chrome  
Scenario: Verify the filter functionality with Textbox Question Type from Question listing screen
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as Textbox
   Then : Verify that the Question listing screen grid should display only Textbox type of data 
   
@chrome 
Scenario: Verify the filter functionality with Checkbox Question Type from Question listing screen
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as Checkbox
   Then : Verify that the Question listing screen grid should display only Checkbox type of data
	
@chrome 
Scenario: Verify the filter functionality with Textarea Question Type from Question listing screen
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as Textarea
   Then : Verify that the Question listing screen grid should display only Textarea type of data
   
@chrome 
Scenario: Verify the filter functionality with Dropdown Question Type from Question listing screen
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as Dropdown
   Then : Verify that the Question listing screen grid should display only Dropdown type of data
   
@chrome 
Scenario: Verify the filter functionality with File Attachment Question Type from Question listing screen
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as File Attachment
   Then : Verify that the Question listing screen grid should display only File Attachment type of data

@chrome 
Scenario: Verify the Search functionality with Question Name when Filter by option is Question Title
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Title
	And : Enter Question Name into the search box
   Then : Search result should be appear according to Searched Question Name and Question Title 
   
@chrome
Scenario: Verify the Search functionality with Question Name when Question Type is Auto Calculated
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as Auto Calculated
	And : Enter Auto Calculated Type Question Name into the search box
   Then : Search result should be appear according to Searched Question Name with Question Type as Auto Calculated
   
@chrome 
Scenario: Verify the Search functionality with Question Name when Question Type is Textbox
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as Textbox
	And : Enter Textbox type Question Name into the search box
   Then : Search result should be appear according to Searched Question Name with Question Type as Textbox
     
@chrome 
Scenario: Verify the Search functionality with Question Name when Question Type is Checkbox
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as Checkbox
	And : Enter Checkbox type Question Name into the search box
   Then : Search result should be appear according to Searched Question Name with Question Type as Checkbox 
   
@chrome 
Scenario: Verify the Search functionality with Question Name when Question Type is Textarea
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as Textarea
	And : Enter Textarea type Question Name into the search box
   Then : Search result should be appear according to Searched Question Name with Question Type as Textarea 
 
@chrome 
Scenario: Verify the Search functionality with Question Name when Question Type is Dropdown
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as Dropdown
	And : Enter Dropdown type Question Name into the search box
   Then : Search result should be appear according to Searched Question Name with Question Type as Dropdown 
   
@chrome 
Scenario: Verify the Search functionality with Question Name when Question Type is File Attachment
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as File Attachment
	And : Enter File Attachment type Question Name into the search box
   Then : Search result should be appear according to Searched Question Name with Question Type as File Attachment

@chrome  
Scenario: Verify the Search functionality with Question Tag when Filter by option is Question Title
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Title
	And : Enter Question Tag into the search box
   Then : Search result should be appear according to Searched Question Tag and Question Title 
   
@chrome  
Scenario: Verify the Search functionality with Question Tag when Question Type is Auto Calculated
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as Auto Calculated
	And : Enter Auto Calculated Type Question Tag into the search box
   Then : Search result should be appear according to Searched Question Tag with Question Type as Auto Calculated
   
@chrome  
Scenario: Verify the Search functionality with Question Tag when Question Type is Textbox
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as Textbox
	And : Enter Textbox type Question Tag into the search box
   Then : Search result should be appear according to Searched Question Tag with Question Type as Textbox
     
@chrome  
Scenario: Verify the Search functionality with Question Tag when Question Type is Checkbox
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as Checkbox
	And : Enter Checkbox type Question Tag into the search box
   Then : Search result should be appear according to Searched Question Tag with Question Type as Checkbox 
   
@chrome 
Scenario: Verify the Search functionality with Question Tag when Question Type is Textarea
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as Textarea
	And : Enter Textarea type Question Tag into the search box
   Then : Search result should be appear according to Searched Question Tag with Question Type as Textarea 
 
@chrome 
Scenario: Verify the Search functionality with Question Tag when Question Type is Dropdown
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as Dropdown
	And : Enter Dropdown type Question Tag into the search box
   Then : Search result should be appear according to Searched Question Tag with Question Type as Dropdown 
   
@chrome  
Scenario: Verify the Search functionality with Question Tag when Question Type is File Attachment
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Type
	And : Select Question Type as File Attachment
	And : Enter File Attachment type Question Tag into the search box
   Then : Search result should be appear according to Searched Question Tag with Question Type as File Attachment
   
@chrome 
Scenario: Verify the Remove Search functionality with Question name when Filter by is Question Title
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select filter by option as Question Title
	And : Enter Question Name into the search box to check Search remove functionality
	And : Click on remove icon of search textbox
   Then : Verify that search result should be disappear from the Question listing screen
 
@chrome  
Scenario: Verify Edit Questions Breadcrumb functionality 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the first record of Question listing grid 
	Then : Verify the Breadcrumb of Edit Question as "Home > Administration > Questions > Add / Edit Question" 
	
@chrome 
Scenario: Verify the "Save" button functionality 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the first record of Question listing grid
	And : Click on Save button of Questions screen
	Then : Verify the Save button should be clickable 
	
@chrome  
Scenario: Verify the "Cancel" button functionality 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the first record of Question listing grid
	And : Click on Cancel button of Questions screen
	Then : Verify the Cancel button should be clickable and redirect to Questions listing grid
	
@chrome 
Scenario: Verify the Color of Save button of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the first record of Question listing grid 
	Then : Verify the Color of Save button 
	
@chrome  
Scenario: Verify the Color of Cancel button of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the first record of Question listing grid 
	Then : Verify the Color of Cancel button 
	
@chrome  
Scenario: Verify the Copy pop-up of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the Create Copy Icon of first record of Question listing grid 
	Then : Verify the Label of Copy pop-up should be as "Questions Name" 
	
		
@chrome  
Scenario: Verify the Default Value of Copy pop-up while Creating Copy 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the Create Copy Icon of first record of Question listing grid 
	Then : Verify the Default Value of Copy pop-up should be appended with "- Copy" 
	
@chrome @demo
Scenario: Verify the Copy functionality of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on the Create Copy Icon of first record of Question listing grid 
	And : Click on OK button of the Copy popup 
	Then : Verify that the system should Create Copy of Question 
	
	
@chrome   
Scenario: Verify Mandatory validation message for the Question name field of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Click on Question Name textbox and press the "TAB" Key 
	Then : Verify the validation message as Question Name is required. 
	
@chrome   
Scenario: Verify Minimum validation message of Question Name Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter Question Name value less than 2 chracters 
	Then : Verify the validation message as Question Name must be at least 2 characters. 
	
@chrome 
Scenario: Verify Maximum validation message of Question Name Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter Question Name value more than 200 characters 
	Then : Verify validation message Question Name cannot be more than 200 characters. 
	
@chrome  
Scenario: Verify Question Name text box with valid data 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter a valid Question Name 
	Then : Verify system should accept the Question name value without any validation message 
	
@chrome 
Scenario: Verify Unique validation message of Question Name Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter duplicate Question Name 
	Then : Verify the Unique validation message for the Question Name Textbox as Question Name must be unique.
	
@chrome  
Scenario: Verify Mandatory validation message of Question Tag Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Click on the Question Tag text box and press the "TAB" Key 
	Then : Verify validation message as Question Tag is required. 
	
@chrome 
Scenario: Verify Minimum validation message of Question Tag Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter Question Tag value less than 2 characters	
	Then : Verify validation message as Question Tag must be at least 2 characters. 
	
@chrome  
Scenario: Verify Maximum validation message of Question Tag Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter Question Tag value more than 20 characters 
	Then : Verify validation message as Question Tag cannot be more than 20 characters. 
	
@chrome  
Scenario: Verify Unique validation message of Question Tag Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter the duplicate value into the Question Tag text box 
	Then : Verify message as Question Tag already exists 
	
@chrome  
Scenario: Verify Question Tag text box with valid data 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Enter valid data into the Question Tag text box 
	Then : Verify the system should accept the Question tag value without any validation message 
	
@chrome  
Scenario: Verify Instructions for user TextBox is appear after checking "Instructions for user" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Click on Instructions for user checkbox 
	Then : Verify that Instructions for user checkbox appears on the screen 
	
@chrome  
Scenario: Verify Mandatory validation message of Instructions for user Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Click on Instructions for user checkbox 
	And : Click on Instructions for user text box and press the "TAB" Key 
	Then : Verify mandatory validation message as User Instructions are required. 
	
@chorme  
Scenario: Verify Minimum validation message of Instructions for user Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Click on Instructions for user checkbox 
	And : Enter Instructions For User textbox value less than 2 characters 
	Then : Verify that validation message as Instructions for user must be at least 2 characters. 
	
@chorme  
Scenario: Verify Maximum validation message of Instructions for user Textbox 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Click on Instructions for user checkbox 
	And : Enter Instructions for user textbox value more than 800 characters 
	Then : Verify that validation message as Instructions for user cannot be more than 800 characters. 
	
@chrome  
Scenario: Verify Instructions for user Textbox with valid data 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Click on Instructions for user checkbox 
	And : Enter valid data into Instructions for user textbox 
	Then : Veirfy the system should accept the Instructions for user textbox value wihtout any validation message 
	
@chrome 
Scenario: Verify the Add Question functionality 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "Add New" button of Questions listing screen 
	And : Add valid data for all fields and click on the save button 
	Then : Verify that the newly added Question should appear on the Questions listing screen and the page should be redirected to Questions listing screen 
	
@chrome  
Scenario: Verify the Navigation and Breadcrumb of Audit trail page of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
	Then : Verify that system should display Audit Trail page and Breadcrumb as "Home > Administration > Questions > Audit Trail" when user click on "View Audit Trail" link of the Questions listing screen 
	
@chrome    
Scenario: Verify pagination of Audit Trail when user click on "Last" for Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page  
	And : Click on the Last button of Pagination
	Then : Verify that the system should display the last page of the Audit trail screen
	
@chrome  
Scenario: Verify pagination of Audit Trail when user click on "First" for Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
	And : Click on the Last button of Pagination 
	And : Click on the First button of Pagination 
   Then : Verify that the system should display the first page of the Audit trail screen	
   
@chrome  
Scenario: Verify pagination of Audit Trail when user click on "Next" for Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page
	And : Click on Next button of Pagination 
   Then : Verify that the system should display the second page of the Audit trail screen 
   
@chrome  
Scenario: Verify pagination of Audit Trail when user click on "Previous" for Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
	And : Click on the Previous button of Pagination 
	Then : Verify that the system should display the previous page of the Audit trail screen
	
@chrome  
Scenario: Verify Color of Back button in Audit Trail of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
	Then : Verify the Color of Back button 
	
@chrome  
Scenario: Verify Back button functionality in Audit Trail of Questions module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page
	And : Click on Back button of Question
	Then : Verify that the system should redirect to Questions listing screen and Breadcrumb display as "Questions
	
@chrome 
Scenario: Verify the Count of record and pagination with 10 page size. 
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select page size as 10
   Then : Verify the page size label, no of records per page, and total no of pages
   
@chrome
Scenario: Verify the Count of record and pagination with 20 page size. 
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select page size as twenty number 
   Then : Verify the page size label, no of records per page, and total no of pages should be related to twenty

@chrome @demo
Scenario: Verify the Count of record and pagination with 50 page size. 
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Questions Tile 
	And : Select page size as fifty number 
   Then : Verify the page size label, no of records per page, and total no of pages should be related to fifty
   