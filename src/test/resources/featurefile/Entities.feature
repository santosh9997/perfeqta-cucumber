Feature: Module Name: Entities
Executed script on "https://test1.beperfeqta.com/mdav33"

  @chrome  
  Scenario: Verify the module title when user click on "Entities" module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    Then : Verify the module name as Entities

  @chrome @ignore
  Scenario: Verify the Search functionality of Entities module listing screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Enter the data into search box, which user want to search 
	 Then : Verify the search results of Entities listing page
	
#this scenario will have AFTER issue due to pop up is not close so element null exception
  @chrome 
  Scenario: Verify the "View" link under linked information column of Entities listing screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
	  And : Click on View link of the first record of the Entity listing screen
    Then : Verify that popup should appear on the entity screen with the label "Your current Entity is used in these Apps:"

  # Pagination test cases for the Attribute Listing screen
  @chrome 
  Scenario: Verify pagination of Entities listing page when user click on "First"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    	And : Click on the Last button of Pagination 
	And : Click on the First button of Pagination 
   Then : Verify that the system should display the first page of the listing screen

  @chrome 
  Scenario: Verify pagination of Entities listing page when user click on "Next"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
      And : Click on Next button of Pagination
    Then : Verify that the system should display the second page of the listing screen

  @chrome 
  Scenario: Verify pagination of Entities listing page when user click on "Previous"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on the Previous button of Pagination 
   Then : Verify that the system should display the first page of the listing screen

  @chrome 
  Scenario: Verify pagination of Entities listing page when user click on "Last"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on the Last button of Pagination 
   Then : Verify that the system should display the last page of the listing screen

  @chrome @ignore
  Scenario: Verify the record count of Entities listing screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
   Then : Verify the record count 

  @chrome 
  Scenario: Verify the navigation of Audit trail page of Entity screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
		And : Click on "View Audit Trail" link of the first record of the Sets listing screen
    Then : Verify that system should displayed Audit Trail page when user click on "View Audit Trail" link of the Set listing screen

  @chrome @ignore
  Scenario: Verify the Ascending order for the Entities column under Entities listing page
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    Then : Verify that all the records of Entities column display in ascending order when click on sorting icon

#yet to be implemented
  @chrome @ignore
  Scenario: Verify the Descending order for the Entities column under Entities listing page
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on Sorting icon of the Entities column
    Then : Verify that all the records of Entities column display in descending order

  @chrome @ignore
  Scenario: Verify the Add Entity functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Add valid data for all the fields and click on the save button
    Then : Verify that the newly added entity should appear on the Entity listing screen and the page should be redirected to entity listing screen

  @chrome 	
  Scenario: Verify the current version of Entity when user trying to add first entity
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    Then : Verify the Current version in Add/Entity screen as "Entity Details Current Version 1.00"

  @chrome 
  Scenario: Verify module dropdown Check all functionality of Entities module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Click on Module dropdown 
    And : Click to Check All option
    Then : Verify that all the module name should be selected

  @chrome  
  Scenario: Verify module drop-down UnCheck All functionality of Entities module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Click on Module dropdown 
    And : Click to Check All option
    And : Click on UnCheck All option
    Then : Verify that all the module name should not be selected

  @chrome 
  Scenario: Verify the search functionality of Module drop-down
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Click on Module dropdown 
    And : Enter the module name for in search box 
		Then : Verify that searched module should appear in the Module drop-down

  @chrome 
  Scenario: Verify Mandatory validation message for the Entity name field of Entity module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Click on Entity Name textbox and press the "TAB" Key
    Then : Verify the validation message as Entity Name is required.

  @chrome 
  Scenario: Verify Minimum validation message of Entity Name Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Enter Entity Name value Less than 2 chracters
    Then : Verify the validation message as Entity Name must be at least 2 characters.

  @chrome 
  Scenario: Verify Maximum validation message of Entity Name Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Enter Entity Name value more than 200 characters
    Then : Verify validation message Entity Name cannot be more than 200 characters.

  @chrome 
  Scenario: Verify Entity Name text box with valid data
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Enter a valid Entity Name
    Then : Veridy system should accept the entity name value without any validation message

  @chrome 
  Scenario: Verify Unique validation message of Entity Name Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Enter Duplicate Entity Name
    Then : Verify the Unique validation message for the Entity Name Textbox

  @chrome 
  Scenario: Verify Mandatory validation message of Entity Tag Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Click on the Entity Tag text box and press the "TAB" Key
    Then : Verify validation message as Entity Name is required.

  @chrome 
  Scenario: Verify Minimum validation message of Entity Tag Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Enter Entity Tag value less than 2 
    Then : Verify validation message as Entity Tag must be at least 2 characters.

  @chrome 
  Scenario: Verify Maximum validation message of Entity Tag Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Enter Entity Tag value more than 20 characters
    Then : Verify validation message as Entity Tag cannot be more than 20 characters.

  @chrome 
  Scenario: Verify Unique validation message of Entity Tag Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Enter the duplicate value into the Entity Tag text box
    Then : Verify message as Entity Tag already exists

  @chrome 
  Scenario: Verify Entity Tag text box with valid data
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Enter valid data into the Entity Tag text box
    Then : Verify the system should accept the entity tag value without any validation message

  @chrome 
  Scenario: Verify Minimum validation message of Entity Description Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Enter Entity Description value less than 2 characters
    Then : Verify the validation message as Description must be at least 2 characters.

  @chrome 
  Scenario: Verify Maximum validation message of Entity Description Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Enter Entity Description value more than 800 characters
    Then : Verify the validation message as Description cannot be more than 800 characters.

  @chrome 
  Scenario: Verify Entity Description with valid data
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Enter valid data into Entity Description text box
    Then : Verify the system should accept the entity description value without any validation message

  @chrome 
  Scenario: Verify Instructions for user TextBox is appear after checking "Instruction for user"
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Click on Instruction for user checkbox
    Then : Verify that Instruction for user checkbox appears on the screen

  @chrome 
  Scenario: Verify Mandatory validation message of Instructions for user Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Click on Instruction for user checkbox
    And : Click on Instruction for user text box and press the "TAB" Key
    Then : Verify the mandatory validation message as User Instructions are required.

  @chorme 
  Scenario: Verify Minimum validation message of Instructions for user Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Click on Instruction for user checkbox
    And : Enter Instruction for user textbox value less than 2 characters
    Then : Verify the validation message as Instructions for user must be at least 2 characters.

  @chorme 
  Scenario: Verify Maximum validation message of Instructions for user Textbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Click on Instruction for user checkbox
    And : Enter Instruction for user textbox value more than 800 characters
    Then : Verify the validation message as Instructions for user cannot be more than 800 characters.

  @chrome 
  Scenario: Verify Instruction for user Textbox with valid data
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Click on Instruction for user checkbox
    And : Enter valid data into Instruction for user textbox
    Then : Veirfy the system should accept the Instruction for user textbox value wihtout any validation message

  @chrome 
  Scenario: Verify Instructions for user UploadDocument is Appear after check Instruction for user checkbox
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Click on Instruction for user checkbox
    And : Click on Upload Document radio button
    Then : Verify Upload document option appears on the screen after clicking Instruction for user checkbox'

  @chrome 
  Scenario: Verify Mandatory validation message of Attribute Name Textbox of Entites screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Click on Attribute name textbox and then Press the "Tab" key in Entities screen
    Then : Verify the validation message as Attribute Name is required.

  @chrome 
  Scenario: Verify Minimum validation message of Attribute Name Textbox of Entities screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Enter Attribute name textbox value less than 2 characters in Entities screen
    Then : Verify the validation message as Attribute Name must be at least 2 characters.

  @chrome 
  Scenario: Verify Maximum validation message of Attribute Name Textbox of Entities screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Enter Attribute Name textbox value more than 200 characters in Entities screen
    Then : Verify the validation message as  Attribute Name cannot be more than 200 characters.

  @chrome 
  Scenario: Verify Attribute name textbox value with valid data in Entities screen
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Enter valid Attribute Name Textbox value in Entities screen
    Then : System should accept the Attribute Name Textbox value

  @chrome 
  Scenario: Verify Link to Entity is not displaying after checked an Attribute is Key Identifier option
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Select Attribute is Key Identifier option
    Then : Verify that the Link to Entity option does not appear on the screen

  @chrome 
  Scenario: Verify the page name
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    Then : Verify the page name as Add / Edit Entity

  @chrome 
  Scenario: Verify navigation using bread crumb value
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on "Add New" button of Entities listing screen
    And : Click on bread crumb link
    Then : Verify that the page should be redirected

  @chrome @ignore
  Scenario: Verify Edit Entity functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on Entities Tile
    And : Click on the first record of entity listing grid
    And : Change the valid data to any of the fields and Click to save button
    Then : Verify that the newly added record updated and the page should be redirected to the Entities listing screen
