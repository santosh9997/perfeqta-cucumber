Feature: Module Name: Attribute 
Executed script on "https://test1.beperfeqta.com/mdav33"

@chrome @RunIssue @vishal
Scenario: Verify the "Attribute" module title 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	Then : Verify the module name as "Attributes" 
	
@chrome
Scenario: Verify the default "Module drop-down" values of Attributes module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	Then : Verify the default "Module drop-down" values of Attributes module 
	
@chrome 
Scenario: Verify the default module drop-down is enable or not 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	Then : Click on "Module drop-down" and verify that the drop down is enabled or not 
	
@chrome 
Scenario: Verify the search functionality of Attribute listing page 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Select Module from Attribute listing Screen 
	And : Enter the data into search box, which user want to search 
	Then : Verify the search results of Attribute listing page 
	
@chrome 
Scenario: Verify the Ascending order for the Attribute column under Attributes listing page 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on Sorting icon of the Attribute column 
	Then : Verify that all the records of Attribute column display in ascending order 

@chrome 
Scenario: Verify the navigation of Audit trail page of Attributes 
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page
   Then : Verify the user redirecting to Audit trail of record 
	
@chrome 
Scenario: Verify the breadcrumbs of Audit trail page of Attributes module 
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
   Then : Verify the Audit trail breadcrumbs as "Home>Administration>Attributes>Audit Trail" 
	
		
@chrome 
Scenario: Verify pagination of Audit trail screen of Attribute module when user click on "First" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
	And : Click on the Last button of Pagination 
	And : Click on the First button of Pagination 
   Then : Verify that the system should display the first page of the Audit trail screen 
	
@chrome 
Scenario: Verify pagination of Audit trail screen of Attribute module when user click on "Next" 
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
	And : Click on Next button of Pagination 
   Then : Verify that the system should display the second page of the Audit trail screen 
	
@chrome 
Scenario: Verify pagination of Audit trail screen of Attribute module when user click on "Previous" 
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
	And : Click on Next button of Pagination 
	And : Click on the Previous button of Pagination 
   Then : Verify that the system should display the previous page of the Audit trail screen 
	
@chrome 
Scenario: Verify pagination of Audit trail screen of Attribute module when user click on "Last" 
  Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
	And : Click on the Last button of Pagination 
   Then : Verify that the system should display the last page of the Audit trail screen 
@chrome 
Scenario: Verify the record count of Audit trail screen of Attribute module 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on "View Audit Trail" link of the first record of the listing page 
   Then : Verify the record count 
		
@chrome 
Scenario: Verify pagination of Attributes listing page when user click on "First" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on the Last button of Pagination 
	And : Click on the First button of Pagination 
   Then : Verify that the system should display the first page of the listing screen 
	
@chrome 
Scenario: Verify pagination of Attributes listing page when user click on "Next" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on Next button of Pagination 
	Then : Verify that the system should display the second page of the listing screen 
	
@chrome 
Scenario: Verify pagination of Attributes listing page when user click on "Previous" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on Next button of Pagination 
	And : Click on the Previous button of Pagination 
	Then : Verify that the system should display the first page of the listing screen 
@chrome 
Scenario: Verify pagination of Attributes listing page when user click on "Last" 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on the Last button of Pagination 
	Then : Verify that the system should display the last page of the listing screen 
	
@chrome 
Scenario: Verify the record count of Attribute listing screen 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	Then : Verify the record count 
	
	
@chrome 
Scenario: Verify the navigation of "View" link of Linked Information column of Attribute listing screen 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on View link of the first record of Attribute listing screen 
	Then : Verify that popup should appear on the screen with the label "Your current Attribute is used in these Apps:" 
	
@chrome 
Scenario: Verify Edit Attribute functionality 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on the first record of "Attributes" column 
	Then : Verify the name of the page as "Edit Attribute" and the current version 
	
@chrome 
Scenario: Verify Attribute update functionality 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on the first record of "Attributes" column 
	And : Update the data 
	And : Click on save button 
	Then : Verify that the updated field should be saved and appear on the screen 
	
@chrome 
Scenario: Verify the cancel button functionality of Edit Attribute screen 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on the first record of "Attributes" column 
	And : Click on cancel button 
	Then : Verify that page should be redirected back to the Attribute listing screen 
	
@chrome 
Scenario: Verify the Cancel button color of Edit Attribute screen 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on the first record of "Attributes" column 
	Then : Verify the cancel button color 
	
@chrome 
Scenario: Verify the save button color of Edit Attribute screen 
	Given : Navigate to the URL 
	And : Enter valid credentials and Click on Login Button 
	And : Click on Administration Tile 
	And : Click on Attributes Tile 
	And : Click on the first record of "Attributes" column 
	Then : Verify the save button color 
