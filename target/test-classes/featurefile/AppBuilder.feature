﻿Feature: Info: Executed script on "https://test1.beperfeqta.com/mdav33/#/login" link.
  Module Name: App Builder

  @chrome 
  Scenario: Verify the module title when user click on "App Builder" module
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
    Then : Verify module name as App Builder

  @chrome @common @ignore
  Scenario: Verify Last button of App Builder Page Pagination
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Last button of Pagination
    Then : Verify that system should display the Last page of the App Builder listing screen

  @chrome @common @ignore
  Scenario: Verify First button of App Builder Page Pagination
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on First button of Pagination
    Then : Verify that system should display the First page of the App Builder listing screen

  @chrome @common @ignore
  Scenario: Verify Pagination of App Builder Page
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
    Then : Verify total number of entries should be match with pagination of App Builder screen

  @chrome @common @ignore
  Scenario: Verify Search Functionality of App Builder Screen
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Enter the App name into search box of App Builder screen
    Then : Verify the search result

  @chrome @common @ignore
  Scenario: Verify the Ascending order functionality for Apps column of App Builder screen
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Sorting icon of the Apps column for ascending order sorting
    Then : Verify that all the records of Apps column display in ascending order

  @chrome @common @ignore
  Scenario: Verify the Descending order functionality for Apps column of App Builder screen
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Sorting icon of the Apps column for descending order sorting
    Then : Verify that all the records of Apps column display in descending order

  @chrome @common 
  Scenario: Verify View Audit Trail Page redirection of App Builder
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on View Audit Trail link of first record from the Audit Trail column on app builder listing page
    Then : Verify that system should be redirect to the Audit Trail screen when a user click on the View Audit Trail link of the first record

  @chrome @common @ignore
  Scenario: Verify Pagination for Audit Trail Page
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on View Audit Trail link of first record from the Audit Trail column
    Then : Verify total number of entries should be match with pagination of Audit Trail screen

  @chrome @common @ignore
  Scenario: Verify Back Button color for Audit Trail Page
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on View Audit Trail link of first record from the Audit Trail column
    Then : Verify that color of Back button should be black

  @chrome @common @ignore
  Scenario: Verify Audit Trail back button functionality of App Builder
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on View Audit Trail link of first record from the Audit Trail column
    Then : Verify that color of Back button should be black

  @chrome @common @ignore
  Scenario: Verify Audit Trail back button functionality of App Builder
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on View Audit Trail link of first record from the Audit Trail column
  And : Click on Back button
    Then : Verify that system should be redirect to the App Builder screen when a user click on the Back button

  #	In this scenario @After will throw an Exception which is not an issue.
  @chrome @done @ignore
  Scenario: Verify Copy pop-up
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Create Copy icon of first record from the Action column
    Then : Verify that system should display Copy pop-up when a user click on the Create Copy icon

  #	In this scenario @After will throw an Exception which is not an issue.
  @chrome @done
  Scenario: Verify Default value while creating Copy of App
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
    Then : Verify that system should display Default App Title into Copy pop-up when a user create copy of a App

  @chrome @ignore
  Scenario: Verify creating copy of App functionality
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Create Copy icon of first record from the Action column
  And : Click on OK button of Copy pop-up
    Then : Verify that a user is allow to create copy of app by click on Create Copy icon

  @chrome @done
  Scenario: Verify Color of Add New Button
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
    Then : Verify that color of Add New button should be blue

  @chrome @done
  Scenario: Verify Add New Button functionality
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
    Then : Verify that Build App page should be display when a user Click on Add New button of App Builder

  @chrome @done
  Scenario: Verify '1  BASIC' text in Basic tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
    Then : Verify that 1  BASIC text should be displayed in Basic tab

  @chrome @done
  Scenario: Verify color of Basic tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
    Then : Verify that Color of the Basic tab should be green

  @chrome @done
  Scenario: Verify App Details label in Basic Tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
    Then : Verify that App Details label should be displayed in top left side of Basic Tab

  @chrome @done
  Scenario: Verify Current Version of App in Basic Tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
    Then : Verify that a system should display the Current Version of the App in top right side of Basic Tab

  @chrome @done
  Scenario: Verify Current Version message color in Basic tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
    Then : Verify that Color of Current Version message should blue

  @chrome @done
  Scenario: Verify Site Selections Section label in Basic tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
    Then : Verify that Site Selections label should be displayed after App Details section in Basic Tab

  @chrome @done
  Scenario: Verify other tabs are disabled when User access Basic Tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
    Then : Verify that all other tabs should be displayed in disable mode when a user access Basic Tab

  @chrome @done
  Scenario: Verify Required field validation for App Module
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Press "TAB" Key on Select App Module
    Then : Verify that system should display Required validation message as App Module is required.

  @chrome @done
  Scenario: Verify Color of Save Draft Button
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
    Then : Verify that Color of Save Draft button should be blue

  @chrome @done
  Scenario: Verify Color of Continue Button
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
    Then : Verify that Color of Continue button should be green

  @chrome @done
  Scenario: Verify Color of Cancel Button
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
    Then : Verify that Color of App Builder Cancel button should be black

  #	In this scenario @After will throw an Exception which is not an issue.
  @chrome @done
  Scenario: Verify Alertbox appear on Click on Cancel button of App Builder
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Cancel button of App Builder
    Then : Verify that system should display Alertbox when a user click on the Cancel button

  #	In this scenario @After will throw an Exception which is not an issue.
  @chrome @done
  Scenario: Verify Alertbox Message that appear on Click on Cancel button of App Builder
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Cancel button of App Builder
    Then : Verify that system should display Alertbox message as Whoa! Are you sure? All changes will be lost.

  #	In this scenario @After will throw an Exception which is not an issue.
  @chrome @done
  Scenario: Verify Color of No Button in Alertbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Cancel button of App Builder
    Then : Verify that color of No button in Alertbox should be red

  #	In this scenario @After will throw an Exception which is not an issue.
  @chrome @done
  Scenario: Verify Color of Yes Button in Alertbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Cancel button of App Builder
    Then : Verify that color of Yes button in Alertbox should be green

  @chrome @done
  Scenario: Verify Alertbox disappear on Click on No Button
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Cancel button of App Builder
  And : Click on No button of Alert Popup
    Then : Verify that Alertbox should be disappear when a user click on the No button

  @chrome @done
  Scenario: Verify system is redirected to Listing Page on click of Yes Button of Alertbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Cancel button of App Builder
  And : Click on Yes button of Alert Popup
    Then : Verify that system should redirect to the App Listing page when a user click on the Yes button of Alertbox

  @chrome @done
  Scenario: Verify Required validations message for App Title field
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on the App Title textbox and Press "TAB" Key
    Then : Verify the Required validation message for App Title field as "App Title is required."

  @chrome @done
  Scenario: Verify color of App title Required validations message
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on the App Title textbox and Press "TAB" Key
    Then : Verify that color of Required validation for App Title field message should be red

  @chrome @done
  Scenario: Verify Minimum validation message for App title textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on App title textbox and Enter App title as value less than 2 characters
    Then : Verify Minimum validation message of App title textbox asApp title must be at least 2 characters.

  @chrome @done
  Scenario: Verify color of Minimum validation message for App title field
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on App title textbox and Enter App title as value less than 2 characters
    Then : Verify that color of Minimum validation message for App Title field should be red

  @chrome @done
  Scenario: Verify Maximum validation message for App title textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on App title textbox and Enter App title value more than 50 characters
    Then : Verify Maximum validation message of App title textbox as App title cannot be more than 50 characters.

  @chrome @done
  Scenario: Verify color of Maximum validation for App title field
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on App title textbox and Enter App title value more than 50 characters
    Then : Verify that color of Maximum validation message for App Title field should be red

  @chrome @done
  Scenario: Verify no validation message for App title field
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on App title textbox and Enter Valid Value in App title textbox
    Then : Verify that system should not display validation message when a user enter valid App Title

  @chrome @done
  Scenario: Verify Minimum validation for App Description field
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on App Description textbox and Enter App Description as value less than 2 characters
    Then : Verify Minimum validation message of App Description textbox as App Description must be at least 2 characters.

  @chrome @done
  Scenario: Verify Color of Minimum validation message for App Description field
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on App Description textbox and Enter App Description as value less than 2 characters
    Then : Verify that color of App Description textbox Minimum validation message should be red

  @chrome @done 
  Scenario: Verify Maximum validation message for App Description field
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on App Description textbox and Enter App Description as value more than 800 characters
    Then : Verify Maximum validation message of App Description textbox as App Description cannot be more than 800 characters.

  @chrome @done
  Scenario: Verify Color of Maximum validation message for App Description field
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on App Description textbox and Enter App Description as value more than 800 characters
    Then : Verify that color of App Description textbox Maximum validation message should be red

  @chrome @done
  Scenario: Verify no validation message for App Description field
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Enter Valid Value in App Description textbox
    Then : Verify that system should not display validation message when a user enter Valid App Description

  @chrome @done
  Scenario: Verify invalid Validation message for URL
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select URL Radio button
  And : Click on the URL textbox and Enter URL as abc
    Then : Verify that system display invalid validation for URL as Not a valid URL, Don't forget to use http:// or https://

  @chrome @done
  Scenario: Verify Color of Validation message for URL
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select URL Radio button
  And : Click on the URL textbox and Enter URL as abc
    Then : Verify that Color of invalid Validation message color should be red

  @chrome @done
  Scenario: Verify No Validation message for Valid URL
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select URL Radio button
  And : Click on the URL textbox and Enter Valid URL
    Then : Verify that system should not display Validation message when a user enter valid URL

  @chrome @done
  Scenario: verify Remove validation Message for URL
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select URL Radio button
  And : Click on the URL textbox and Enter Valid URL
  And : Select Upload Document Radio button
    Then : Verify that system should display Remove Validation message as Please remove the entered URL.

  @chrome @done
  Scenario: Verify that by selecting "Send Email Alert when Record result is a failure" checkbox Email textbox appears
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select Send Email Alert when Record result is a failure checkbox
    Then : Verify that system should display Email Address textbox when a user select Send Email Alert when Record result is a failure checkbox

  @chrome @done
  Scenario: Verify that by Deselecting "Send Email Alert when Record result is a failure" checkbox Email textbox disappear.
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select Send Email Alert when Record result is a failure checkbox
  And : Deselect Send Email Alert when Record result is a failure checkbox
    Then : Verify that system should not display Email textbox when a user deselect Send Email Alert when Record result is a failure checkbox

  @chrome @done
  Scenario: Verify that by selecting "Send Email Alert when Record result is a failure" checkbox Email Information message appears
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select Send Email Alert when Record result is a failure checkbox
    Then : Verify that system should display information message as "Separate multiple Email Addresses with a comma(,)." when a user select Send Email Alert when Record result is a failure checkbox

  @chrome @done
  Scenario: Verify color of Email Information message
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select Send Email Alert when Record result is a failure checkbox
    Then : Verify that color of Email Information message should be blue

  @chrome @done
  Scenario: Verify Required message for Email Address
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select Send Email Alert when Record result is a failure checkbox
  And : Click on the Email Address textbox and Press "TAB" key
    Then : Verify that system should display required message for Email Address textbox as Please enter at least one Email Address.

  @chrome @done
  Scenario: Verify Invalid validation message for Email Address
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select Send Email Alert when Record result is a failure checkbox
  And : Click on the Email Address textbox and Enter Email Address as abc
    Then : Verify that Invalid email Address validation message should display as Invalid Email Address.

  @chrome @done
  Scenario: Verify No Validation message for Valid Email Address
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select Send Email Alert when Record result is a failure checkbox
  And : Click on the Email Address textbox and Enter Valid Email Address
    Then : Verify that system should not display validation message when a user enter valid email address

  @chrome @done
  Scenario: Verify Information message of Site Selection YES option
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
    Then : Verify that Information message for Site selection should be display as Please select at least one option from each site level." when Yes option is selected

  @chrome @done
  Scenario: Verify Color of Information message for Site Selection option YES
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
    Then : Verify that Color of Information message should be blue for Yes option of Site Selection

  @chrome @done
  Scenario: Verify Information messages of Site Selection option No
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select No option form Does this app require site selections? from dropdown
    Then : Verify that system should display Information Message for NO option as Please select default option from each site level. They will be stored in the database for each app.

  @chrome @done
  Scenario: Verify Color of Information message for Site Selection option No
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select No option form Does this app require site selections? from dropdown
    Then : Verify that Color of Information message should be blue for No option of Site Selection

  @chrome @done
  Scenario: Verify Required Validation Message for Site Selection
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select No option form Does this app require site selections? from dropdown
    Then : Verify that Required validation Message should be display as At least one site is required from each level. when a user select No option

  @chrome @done
  Scenario: Verify Color of Required Validation Message for Site Selection
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select No option form Does this app require site selections? from dropdown
    Then : Verify that Color of Required validation message should be red for No option of Site Selection

  @chrome @done
  Scenario: Verify Minimum validation of Header Message for Print textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Header Message for Print textbox and Enter less than 2 characters
    Then : Verify that system should display Minimum validation of Header Message for Print as "Header Message for Print must be at least 2 characters."

  @chrome @done
  Scenario: Verify Color of Minimum validation of Header Message for Print textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Header Message for Print textbox and Enter less than 2 characters
    Then : Verify that Color of Minimum validation message should be red for Header Message for Print textbox

  @chrome @done
  Scenario: Verify Maximum validation of "Header Message for Print" textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Header Message for Print textbox and Enter more than 200 characters
    Then : Verify that system should display Maximum validation message as Header Message for Print cannot be more than 200 characters.

  @chrome @done
  Scenario: Verify Color of Maximum validation of "Header Message for Print" textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Header Message for Print textbox and Enter more than 200 characters
    Then : Verify that Color of Maximum validation message should be red for Header Message for Print textbox

  @chrome @done
  Scenario: Verify No validation message for "Header Message for Print" textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Header Message for Print textbox and Enter valid data
    Then : Verify that system should not display any validation message when a user enter Valid Value into Header Message for Print textbox

  @chrome @done
  Scenario: Verify Minimum validation of "Header Message for Print When App Status is Passed" textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Header Message for Print When App Status is Passed textbox and Enter less than 2 characters
    Then : Verify that system should display Minimum validation of Header Message for Print When App Status is Passed as "Header Message for Print When App Status is Passed must be at least 2 characters."

  @chrome @done
  Scenario: verify Color of Minimum validation of "Header Message for Print When App Status is Passed" textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Header Message for Print When App Status is Passed textbox and Enter less than 2 characters
    Then : Verify that Color of Minimum validation message should be red for Header Message for Print When App Status is Passed textbox

  @chrome @done
  Scenario: Verify Maximum validation of "Header Message for Print When App Status is Passed" textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Header Message for Print When App Status is Passed textbox and Enter more than 200 characters
    Then : Verify that system should display Maximum validation message as Header Message for Print When App Status is Passed cannot be more than 200 characters.

  @chrome @done
  Scenario: verify Color of Maximum validation of "Header Message for Print When App Status is Passed" textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Header Message for Print When App Status is Passed textbox and Enter more than 200 characters
    Then : Verify that Color of Maximum validation message should be red for Header Message for Print When App Status is Passed textbox

  @chrome @done
  Scenario: Verify No validation message for "Header Message for Print When App Status is Passed" textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Header Message for Print When App Status is Passed textbox and Enter valid data
    Then : Verify that system should not display any validation message when a user enter Valid Value into Header Message for Print When App Status is Passed textbox

  @chrome @done
  Scenario: Verify Minimum validation of "Header Message for Print When App Status is Failed" textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Header Message for Print When App Status is Failed textbox and Enter less than 2 characters
    Then : Verify that system should display Minimum validation of Header Message for Print When App Status is Failed as Header Message for Print when App Status is Failed must be at least 2 characters.

  @chrome @done
  Scenario: verify Color of Minimum validation of "Header Message for Print When App Status is Failed" textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Header Message for Print When App Status is Failed textbox and Enter less than 2 characters
    Then : Verify that Color of Minimum validation message should be red for Header Message for Print When App Status is Failed textbox

  @chrome @done
  Scenario: Verify Maximum validation of "Header Message for Print When App Status is Failed" textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Header Message for Print When App Status is Failed textbox and Enter more than 200 characters
    Then : Verify that system should display Maximum validation message as Header Message for Print When App Status is Failed cannot be more than 200 characters.

  @chrome @done
  Scenario: verify Color of Maximum validation of "Header Message for Print When App Status is Failed" textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Header Message for Print When App Status is Failed textbox and Enter more than 200 characters
    Then : Verify that Color of Maximum validation message should be red for Header Message for Print When App Status is Failed textbox

  @chrome @done
  Scenario: Verify No validation message for "Header Message for Print When App Status is Failed" textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Click on Header Message for Print When App Status is Failed textbox and Enter valid data
    Then : Verify that system should not display any validation message when a user enter Valid Value into Header Message for Print When App Status is Failed textbox

  @chrome @done
  Scenario: Verify that by selecting "Peer or Second Review" checkbox Select Role dropdown is displayed
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select Peer or Second Review checkbox
    Then : Verify that system should display Select Role dropdown when a user select Peer or Second Review checkbox

  @chrome @done
  Scenario: Verify that by unselecting "Peer or Second Review" checkbox Select Role dropdown is disappear
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Select Peer or Second Review checkbox
  And : Deselect Peer or Second Review checkbox
    Then : Verify that system should not display Select Role dropdown when a user unselect the Peer or Second Review checkbox

  @chrome @done @test
  Scenario: Verify App Details in Basic Tab to Proceed in Attribute tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
    Then : Verify that system should redirect to the Attribute tab when a user Adds All required details into Basic Tab

  @chrome @done 
  Scenario: Verify '2  ATTRIBUTES' text in Attribute tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
    Then : Verify that 2 ATTRIBUTES text should be display in Attribute Tab

  @chrome @done 
  Scenario: Verify Color of Attribute tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
    Then : Verify that Color of Attribute tab should be green

  @chrome @done 
  Scenario: Verify Attribute Details label in Attribute tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
    Then : Verify that Attribute Details label should be displayed in top left side of Attribute Tab

  @chrome @done
  Scenario: Verify Current Version of App in Attribute Tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
    Then : Verify that a system should display the Current Version of the App in top right side of Attribute Tab

  @chrome @done
  Scenario: Verify Current Version Message color in Attribute tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
    Then : Verify that Color of Current Version message should be blue in Attribute tab

  @chrome @done 
  Scenario: Verify other tabs are Disabled when User access Attribute Tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
    Then : Verify that all next tabs should displayed in Disable mode when a user access Attribute tab

  @chrome @done 
  Scenario: Verify Information Message for Attribute tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
    Then : Verify that system should display information message in Attribute tab as Define Attributes for App. Minimum 2 Attributes and/or Entities are required.

  @chrome @done
  Scenario: Verify Color of Information Message in Attribute tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
    Then : Verify that Color of Information message in Attribute tab should be blue

  @chrome @done
  Scenario: Verify Add Attribute button is not displayed initially when the page is loaded
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
    Then : Verify that Add Attribute button should not displayed initially to the user when Attribute tab is loaded

  @chrome @done
  Scenario: Verify that Add Attribute button is displayed only when Attribute is selected in Dropdown
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Select Attribute from Select Attribute dropdown
    Then : Verify that system should display Add Attribute button only when a user select any Attribute from dropdown

  @chrome @done
  Scenario: Verify that Add Attribute is clickable
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Select Attribute from Select Attribute dropdown
  And : Click on Add Attribute button
    Then : Verify that Add Attribute button is clickable or not

  @chrome @done
  Scenario: Verify Color of  Add Attribute button
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Select Attribute from Select Attribute dropdown
    Then : Verify that Color of Add Attribute button should be blue

  @chrome @done
  Scenario: Verify that Remove button functionality
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Select Attribute from Select Attribute dropdown
  And : Click on Add Attribute button
  And : Click on Remove button
    Then : Verify that Selected Attribute should be removed when a user click on the Remove button

  @chrome @done 
  Scenario: Verify Attribute Details in Attribute Tab to Proceed in Workflow tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
    Then : Verify that system should redirect to the Workflow tab when a user Adds All required details into Attribute Tab

  @chrome @done
  Scenario: Verify text in '3  WORKFLOW' Workflow tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
    Then : Verify that 3  WORKFLOW text should be display in Workflow tab

  @chrome @done
  Scenario: Verify Color of Workflow tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
    Then : Verify that Color of Workflow tab should be green

  @chrome @done 
  Scenario: Verify Workflow Details label in Workflow tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
    Then : Verify that Workflow Details label should be displayed in top left side of Workflow Tab

  @chrome @done
  Scenario: Verify Current Version of App in Workflow Tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
    Then : Verify that a system should display the Current Version of the App in top right side of Workflow Tab

  @chrome @done
  Scenario: Verify Current Version Message color in Workflow tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
    Then : Verify that Color of Current Version message should be blue in Workflow tab

  @chrome @done
  Scenario: Verify other tabs are disabled when User access Workflow Tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
    Then : Verify that all next tabs should displayed in Disable mode when a user access Workflow tab

  @chrome @done
  Scenario: Verify Entity Workflow label in Workflow tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
    Then : Verify that system should display Entity Workflow label below Workflow Details label

  @chrome @done
  Scenario: Verify Procedure Workflow label in Workflow tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
    Then : Verify that system should display Procedure Workflow label below Entity Workflow section

  @chrome @done
  Scenario: Verify that Add Entity button is not displayed initially when the page is loaded
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
    Then : Verify that system should not display Add Entity button initially when the page is loaded

  @chrome @done 
  Scenario: Verify that Add Entity button is displayed when Entity name is typed in Search and Select Entity textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Type Entity name in Search and Select Entity dropdown
    Then : Verify that system should display Add Entity button when user Type Valid Entity name in Search and Select Entity dropdown

  @chrome @done 
  Scenario: Verify that Add Entity button is clickable
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Type Entity name in Search and Select Entity dropdown
  And : Click on Add Entity button
    Then : Verify that Add Entity button is clickable or not

  @chrome @done 
  Scenario: Verify Color of Add Entity button
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Type Entity name in Search and Select Entity dropdown
    Then : Verify that Color of Add Entity button should be soft cyan

  @chrome @done 
  Scenario: Verify Filter button redirection
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Type Entity name in Search and Select Entity dropdown
  And : Click on Add Entity button
  And : Click on Filter button
    Then : Verify that system should redirect to the Apply Entity Filter section when a user click on the Filter button

  @chrome @done 
  Scenario: Verify Add Rule button redirection for Entity
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Type Entity name in Search and Select Entity dropdown
  And : Click on Add Entity button
  And : Click on Add Rule button
    Then : Verify that system should redirect to the Define Rule section when a user click on the Add Rule button

  @chrome @done 
  Scenario: Verify Show details link functionality
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Type Entity name in Search and Select Entity dropdown
  And : Click on Add Entity button
  And : Click on Show link
    Then : Verify that system should expand the Entity Details when a user click on the Show link

  @chrome @done 
  Scenario: Verify Hide details link functionality
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Type Entity name in Search and Select Entity dropdown
  And : Click on Add Entity button
  And : Click on Show link
  And : Click on Hide link
    Then : Verify that system should collapse the Entity Details when a user click on the Hide link

  @chrome @done
  Scenario: Verify that Add Procedure button is not displayed initially when the page is loaded
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
    Then : Verify that Add Procedure button should not displayed initially when the page is loaded

  @chrome  @ignore
  Scenario: Verify that Add Procedure button is displayed when Procedure name is typed in Search and Select Procedure textbox
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Type Procedure name in Search and Select Procedure dropdown
  And : Click on Add Procedure button
    Then : Verify that system should display Add Procedure button when user Type Valid Procedure name in Search and Select Procedure dropdown

  @chrome  @ignore
  Scenario: Verify that Add Procedure button is clickable
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
		And : Click on Continue button of attribute tab
  And : Type Procedure name in Search and Select Procedure dropdown
  And : Click on Add Procedure button
    Then : Verify that Add Procedure button is clickable or not

  @chrome  @ignore
  Scenario: Verify Color of Add Procedure button
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Type Procedure name in Search and Select Procedure dropdown
    Then : Verify that Color of Add Procedure button should be soft cyan

  @chrome  @ignore
  Scenario: Verify Add Rule button redirection for Procedure
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Type Procedure name in Search and Select Procedure dropdown
  And : Click on Add Procedure button
  And : Click on Add Rule button
    Then : Verify that system should redirect to the Define Rule section when a user click on the Add Rule button

  @chrome  @ignore
  Scenario: Verify Validation Message for Workflow tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Click on Continue button of workflow tab
    Then : Verify that system should display validation message as "Please add at least 2 Attributes and/or Entities."

  @chrome @chirag
  Scenario: Verify Workflow Details in Workflow Tab to Proceed in Acceptance tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
    Then : Verify that system should redirect to the Acceptance tab when a user Adds All required details into Workflow Tab

  @chrome @chirag
  Scenario: Verify text in '3   ACCEPTANCE' Acceptance tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
    Then : Verify that '3   ACCEPTANCE' text should be display in Acceptance tab

  @chrome @chirag
  Scenario: Verify Color of Acceptance tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
    Then : Verify that Color of Acceptance tab should be green

  @chrome @chirag 
  Scenario: Verify  App Acceptance Criteria label in Acceptance tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
    Then : Verify that App Acceptance Criteria label should be displayed in top left side of Acceptance Tab

  @chrome @chirag
  Scenario: Verify Current Version of App in Acceptance Tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
    Then : Verify that a system should display the Current Version of the App in top right side of Acceptance Tab

  @chrome @chirag
  Scenario: Verify Current Version Message color in Acceptance tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
    Then : Verify that Color of Current Version message should be blue in Acceptance tab

  @chrome @chirag
  Scenario: Verify other tabs are disabled when User access Acceptance Tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
    Then : Verify that all next tabs should displayed in Disable mode when a user access Acceptance tab

  @chrome @chirag
  Scenario: Verify How do you want to track Unique Records?? label in Acceptance tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
    Then : Verify that "How do you want to track Unique Records??" label should be display below App Acceptance Criteria label

  @chrome @chirag
  Scenario: Verify Information Message for Acceptance tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
    Then : Verify that system should display information message as "Combination of selected 'Key Attributes' will be checked against any matching entry."

  @chrome @chirag
  Scenario: Verify Color of Information Message for Acceptance tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
    Then : Verify that Color of Acceptance Attribute information message should be blue

  @chrome @chirag
  Scenario: Verify "Define Pass Criteria(s)on App" label in Acceptance tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
    Then : Verify that system should display Define Pass Criteria(s) on App label below How do you want to track Unique Records? section

  @chrome @chirag
  Scenario: Verify Information Message to select Procedure of Acceptance tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
    Then : Verify that system should display Procedure information message as "Add Procedure(s) that will allow the App to Pass."

  @chrome @chirag
  Scenario: Verify  Color of Information Message for Procedure in Acceptance tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
    Then : Verify that Color of Procedure information message should be blue

  @chrome @chirag
  Scenario: Verify that Add Procedure button is displayed only when Procedure is selected in Dropdown
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Select Procedure from Select Procedure dropdown
    Then : Verify that system should display Add Procedure button when a user select Procedure from Select Procedure dropdown

  @chrome @chirag
  Scenario: Verify that Add Procedure button is clickable
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Select Procedure from Select Procedure dropdown
  And : Click on Add Procedure button
    Then : Verify that Add Procedure button is clickable or not

  @chrome @chirag
  Scenario: Verify Color of Add Procedure button
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Select Procedure from Select Procedure dropdown
    Then : Verify that Color of Add Procedure button should be blue

  @chrome @chirag 
  Scenario: Verify that Remove button functionality
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Select Procedure from Select Procedure dropdown
  And : Click on Add Procedure button
  And : Click on Remove button of Acceptance tab
    Then : Verify that system should remove the Procedure when a user click on the Remove button

  @chrome @chirag
  Scenario: Verify App Acceptance Criteria in Acceptance tab to Proceed in Preview tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
    Then : Verify that system should redirect to the Preview tab when a user Adds All required details into Acceptance Tab

 #===================================================================

  @chrome @chirag
  Scenario: Verify text in '6 PREVIEW' Preview tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
    Then : Verify that system should display '6 PREVIEW' in Preview tab

  @chrome @chirag
  Scenario: Verify Color of Preview tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
    Then : Verify that Color of Preview tab should be green

  @chrome @chirag
  Scenario: Verify other tabs are disabled when User access Preview Tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
    Then : Verify that all next tabs should displayed in Disable mode when a user access Preview tab

  @chrome @chirag
  Scenario: Verify Information Message for Preview tab
  	Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
    Then : Verify that system should display Preview tab information message as "Preview the App before publishing."

  @chrome @chirag
  Scenario: Verify Color of Information message for Preview tab
  	Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
    Then : Verify that Color of information message for Preview tab should be blue

  @chrome @chirag 
  Scenario: Verify Current Version of App in Preview Tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
    Then : Verify that system should display the Current Version of the App in top right side of Preview Tab

  @chrome @chirag
  Scenario: Verify Current App name in Preview tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  	And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
    Then : Verify that system should display Correct App name label in Preview tab

  @chrome @chirag 
  Scenario: Verify Preview App in Preview tab to Proceed in Permissions tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
    Then : Verify that system should redirect to the Permissions tab when a user click on Publish & Continue button

  @chrome @chirag
  Scenario: Verify text in '6 Permission' in Permission tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  	And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
    Then : Verify that system should display '6  Permissions' text in Permissions tab

  @chrome @chirag 
  Scenario: Verify Color of Permission tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
    Then : Verify that Color of Permissions tab should be green

  @chrome @chirag
  Scenario: Verify App Name in Permission Tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
    Then : Verify that system should display Correct App Name label in Permissions tab

  @chrome @chirag
  Scenario: Verify Information message 'Define Permissions for App.' in Permission tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
    Then : Verify that system should display information as "Define Permissions for App." in Permissions tab

  @chrome @chirag
  Scenario: Verify Information Message color in Permission tab
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
    Then : Verify that Color of information message in Permissions tab should be blue

  @chrome @chirag
  Scenario: Verify Color of Save Button
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
    Then : Verify that Color of Save button should be green in App Builder Screen

  @chrome @pend @ignore
  Scenario: Verify Save Button Click
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
    Then : Verify that Save button is clickable or not

  @chrome @chirag @ignore
  Scenario: Verify Save Button Redirection
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
    Then : Verify that system should redirect to the Apps Listing Screen when a user click on the Save button

  @chrome @chirag @modify
  Scenario: Verify Add New App functionality
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
    Then : Verify that user is allowed to Add New app
    
@chrome @siddharth 
  Scenario: Verify Add New App functionality with acceptance criteria
    Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab for acceptance criteria
  And : Click on show button of procedure workflow
  And : Click on first procedure acceptance criteria button
  And : Select condition dropdown from standrad Acceptance Criteria
  And : Enter value in value textbox
  And : Click on save button of Acceptance criteria
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
    Then : Verify that user is allowed to Add New app
    
    
@chrome @chirag @scenario 
Scenario: Verify the Workflow scenario for App
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile 
  And : Click on Searched app
  And : Enter data in Mandatory fields
  And : Click on save button of Apps screen
  And : Go Back to Home
  And : Click on Advanced Search Tile
  And : Search for the Respective app in Search Textbox
  And : Select the searched app name
  And : Click on Go button of Advanced Search
  And : Click on View link of App record
  Then : Verify that eSignature PIN table should display
    
@chrome @chirag
Scenario: Verify Mark Set Value 'Inactive' when App Submission status is failed – functionality is working properly
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab for acceptance criteria
  And : Click on Show link of Entity
  And : Select Mark entity record as 'Inactive' when App Submission status is failed checkbox
  And : Click on show button of procedure workflow
  And : Click on first procedure acceptance criteria button
  And : Select condition dropdown from standrad Acceptance Criteria
  And : Enter value in value textbox
  And : Click on save button of Acceptance criteria
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile 
  And : Click on Searched app
  And : Enter data in all Mandatory fields select entity and fail the record
  And : Click on save button of Apps screen
  And : Go Back to Home
  And : Click on Administration Tile
  And : Click on Sets Tile
  And : Search for the Set
  And : Enter Equipment Type in searchbox
  And : Click on first searched result
  Then : Verify that selected option of App should Inactive in Add/Edit Sets
  
@chrome @chirag @scenario 
Scenario: Verify Entity records shall be display as per the set filter from the Entity workflow
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Add filter in Entity of Workflow
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile 
  And : Click on Searched app
  And : Enter data in all Mandatory fields
  And : Click on save button of Apps screen
  And : Go Back to Home
  And : Click on Advanced Search Tile
  And : Search for the Respective app in Search Textbox
  And : Select the searched app name
  And : Click on Go button of Advanced Search
  Then : Verify View link of App record  
 
@chrome @chirag
Scenario: Verify Display pop-up message and accept entry functionality is working properly from the entity  
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Add rule button
  And : Select attribute and Operator options
  And : Select Display Pop up message and Accept entry option in Action drop-down
  And : Enter the Alert Message in textarea
  And : Click on Add Rule button and Save button
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile 
  And : Click on Searched app
  And : Enter data in all Mandatory fields and enter Lot Number as given in Add rules
  And : Verify the pop up Information message
  Then : Verify Lot Number textfield should not clear the field
  
@chrome @chirag 
Scenario: Verify Display pop-up message and Do not accept entry functionality is working properly from the entity  
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Add rule button
  And : Select attribute and Operator options
  And : Select Display Pop up message and Do not accept entry option in Action drop-down
  And : Enter the Alert Message in textarea
  And : Click on Add Rule button and Save button
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile 
  And : Click on Searched app
  And : Enter data in all Mandatory fields and enter Lot Number as given in Add rules
  And : Click the pop up Information message of Display Pop up message and Do not accept entry option
  Then : Verify Lot Number textfield should clear the field
  
@chrome @chirag
Scenario: Verify Display procedure functionality is working properly from the entity  
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Add rule button
  And : Select attribute and Operator options
  And : Select Display procedure option in Action drop-down
  And : Select Procedure from Select Procedure drop-down
  And : Click on Add Rule button and Save button
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile 
  And : Click on Searched app
  And : Enter data in all Mandatory fields and Initiate procedure
  Then : Verify that the selected Procedure initiated
  
@chrome @chirag 
Scenario: Verify Allow inactive records functionality of working properly  
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Show link of Entity
  And : Select Allow Inactive records checkbox
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile 
  And : Click on Searched app
  And : Enter data in all Mandatory fields and select Inactive entity
  Then : Verify that the system is allow to select inactive entity 
  
@chrome @chirag 
Scenario: Verify Mark entity record as 'Inactive' when App Submission status is failed functionality is working properly
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab for acceptance criteria
  And : Click on Show link of Entity
  And : Select Mark entity record as 'Inactive' when App Submission status is failed checkbox
  And : Click on show button of procedure workflow
  And : Click on first procedure acceptance criteria button
  And : Select condition dropdown from standrad Acceptance Criteria
  And : Enter value in value textbox
  And : Click on save button of Acceptance criteria  
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile 
  And : Click on Searched app
  And : Enter data in all Mandatory fields select entity and fail the record
 	And : Click on save button of Apps screen
  And : Go Back to Home
  And : Click on Entity Records Tile	
  And : Click on serchbox of Enity
  And : Enter Entity name which is used in App Entry
  And : Click on Filter drop-down
  And : Select Lot Number and select value
  And : Click on go button
  Then : Verify that the entity should have 'Inactive' status
 
# =======================================================================================================
  
@chrome @chirag @scenario
Scenario: Verify Enable Entity Record in Add/Edit Mode functionality working properly
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Show link of Entity
  And : Select Enable Entity Record in Add/Edit Mode checkbox
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Enter data in all Mandatory fields and entity in enable mode
 	Then : Verify that entity should be in edit mode
 	
@chrome @chirag @scenario
Scenario: Verify initiate procedure functionality is working properly from the procedure
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Add Rules of Procedure
  And : Select Question from Select Question drop-down
  And : Select Value in the textfield
  And : Select 'Initiate Procedure' from Select Action drop-down
  And : Select Procedure from Select Procedure drop-down of Add Rule screen
  And : Click on Add Rule button of Add Rule screen
  And : Click on Add button of Add Rule screen
  And : Click on Continue button of workflow tab
  And : Select Define Pass Criteria(s) on App of Acceptance Tab
  And : Click on Continue button of acceptance tab
  #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Enter data in all Mandatory fields and initiate procedure
 	Then : Verify that proper procedure should initiate which is selected in App Builder
 	
@chrome @chirag @scenario
Scenario: Verify initiate App functionality is working properly from the procedure
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Add Rules of Procedure
  And : Select Question from Select Question drop-down
  And : Select Value in the textfield
  And : Select 'Initiate' App from Select Action drop-down
  And : Click on Add Rule button for Initiate App
  And : Click on Add button of Add Rule screen
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Enter data in all Mandatory fields and initiate App
  And : Click on save button of Apps screen
 	Then : Verify that proper App should initiate which is selected in App Builder
 	
@chrome @chirag @scenario
Scenario: Verify Update Entity records > Modified Entity Records functionality is working properly
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Add Rules of Procedure
  And : Select Question from Select Question drop-down
  And : Select Value in the textfield
  And : Select 'Update Entity Records' from Select Action drop-down
  And : Select Entity from Select Entity drop-down
  And : Select Modify Entity Record option from Select Action drop-down
  And : Click on Add button of Add Rule screen
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Enter data in all Mandatory fields and update entity
  And : Click on save button of Apps screen
  And : Go Back to Home
  And : Click on Entity Records Tile	
  And : Click on serchbox of Enity
  And : Enter Entity name which is used in App Entry
  And : Click on Filter drop-down
  And : Select Lot Number and select value
  And : Click on go button
 	Then : Verify that the entity record should updated properly
 	
@chrome @chirag @scenario
Scenario: Verify Update Entity records > Activate Entity Records functionality is working properly
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Show link of Entity
  And : Select Allow Inactive records checkbox
  And : Click on Add Rules of Procedure
  And : Select Question from Select Question drop-down
  And : Select Value in the textfield
  And : Select 'Update Entity Records' from Select Action drop-down
  And : Select Entity from Select Entity drop-down
  And : Select Activate Entity Record option from Select Action drop-down
  And : Click on Add button of Add Rule screen
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Enter data in all Mandatory fields and active entity
  And : Click on save button of Apps screen
  And : Go Back to Home
  And : Click on Entity Records Tile	
  And : Click on serchbox of Enity
  And : Enter Entity name which is used in App Entry
  And : Click on Filter drop-down
  And : Select Lot Number and select active value
  And : Click on go button
 	Then : Verify that the entity record should active properly
 	
@chrome @chirag @scenario
Scenario: Verify Update Entity records > Deactivate Entity Records functionality is working properly
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Add Rules of Procedure
  And : Select Question from Select Question drop-down
  And : Select Value in the textfield
  And : Select 'Update Entity Records' from Select Action drop-down
  And : Select Entity from Select Entity drop-down
  And : Select Deactivate Entity Record option from Select Action drop-down
  And : Click on Add button of Add Rule screen
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Enter data in all Mandatory fields and Deactivate entity
  And : Click on save button of Apps screen
  And : Go Back to Home
  And : Click on Entity Records Tile	
  And : Click on serchbox of Enity
  And : Enter Entity name which is used in App Entry
  And : Click on Filter drop-down
  And : Select Lot Number and select Deactivated value
  And : Click on go button
 	Then : Verify that the entity record should Deactivate properly
 	
@chrome @chirag @scenario
Scenario: Verify Standard Acceptance criteria functionality
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Show link of Procedure
  And : Click Acceptance Criteria button of Attribute
  And : Click on Add Acceptance Criteria icon
  And : Select Condition from the Condition drop-down
  And : Select Value from Value drop-down
  And : Click on Save button of Standard Acceptance Criteria screen
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Enter data in all Mandatory data in such a way that can display acceptance criteria pop-up
  Then : Verify that Acceptance Criteria pop-up should display
  
@chrome @chirag @scenario
Scenario: Verify Advanced Acceptance criteria functionality
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Show link of Procedure
  And : Click Acceptance Criteria button of Attribute
  And : Click on Advanced Acceptance Criteria radio button of Procedure Workflow
  And : Click on Add Condition icon
  And : Select Question from Select Question drop-down of Advanced Acceptance Criteria
  And : Select Operator from Select Operator drop-down of Advanced Acceptance Criteria
  And : Enter value in field of Advanced Acceptance Criteria
  And : Select Condition from the Condition drop-down
  And : Select Value from Value drop-down
  And : Click on Add button of Advanced Acceptance Criteria
  And : Click on Save button of Advanced Acceptance Criteria screen
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Enter data in all Mandatory data in such a way that can display acceptance criteria pop-up
  Then : Verify that Acceptance Criteria pop-up should display
  
@chrome @chirag @scenario
Scenario: Verify the Optional procedure functionality
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Show link of Procedure
  And : Select Optional Procedure checkbox
  And : Enter the message in textfield
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile
  And : Click on Searched app
  Then : Verify that Optional message should display properly
  
@chrome @chirag @scenario
Scenario: Verify Unique records functionality from the Acceptance tab
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Attribute - Frequency, Key Attribute checkbox of How do you want to track Unique Records?
  And : Click on Continue button of acceptance tab
  #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Enter data in all Mandatory fields and Key Attribute field
  And : Click on save button of Apps screen
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Enter data in Key Attribute field
  Then : Verify that validation should display properly
  
@chrome @chirag @scenario @NA(Add_Rule_button_is_not_in_this_version)
Scenario: Verify App Rule functionality from the Acceptance tab
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Add App Rule button of Acceptance tab
  And : Enter all required fields and click save button
  And : Click on Continue button of acceptance tab
  #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile
  And : Click on Searched app
  And : Enter data in all Mandatory data in such a way that can display the Rules
  Then : Verify that Rules id display accordingly
  
@chrome @chirag @scenario
Scenario: Verify Define Pass Criteria(s) on App functionality from the Acceptance tab
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
 #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Verify that user is allowed to Add New app
  And : Go Back to Home
  And : Click on Apps Tile
  And : Click on Search App of Apps Module
  And : Enter valid app name of App Module search field
  And : Click on Other Apps tile
  And : Click on Searched app
  Then : Verify that Define Pass Criteria(s) on App functionality
  
@chrome @chirag @scenario
Scenario: Verify Active/Inactive functionality from the App Builder
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
 #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  Then : Verify that user is allowed to Active/Inactive the App
  
@chrome @chirag @scenario
Scenario: Verify Save Draft button functionality and that version
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Save Draft button of acceptance tab
  Then : Verify Save Draft button functionality and that version
  
@chrome @chirag @scenario
Scenario: Verify Published version functionality
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Select Checkbox of Peer or Second Review.
  And : Select Checkbox of Allow the Record Creator to Approve app.
  And : Select Role from the Role dropdown.
  And : Select Add button (All User should be display as selected by default).	
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
 #And : Get the current app name and "store it"
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  Then : Verify that Published Version should display when user save App as Draft
 	
 # =========================================================================
 
@chrome @chirag @4.0
Scenario: Verify Private App icon functionality
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile
	And : Click on App Builder Tile
	And : Click on Add New button of App Builder
	And : Add All Required App Details in Basic Tab
	And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
	And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on Publish & Continue button
	And : Select Mark App as Private App checkbox
	And : Select Role and Click on Add button
  And : Click on Save button of permission tab
	Then : Verify that user should able to create a Private App
	
@chrome 
Scenario: Verify Restore Link functionality
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile
	And : Click on App Builder Tile
	And : Click on Add New button of App Builder 
	And : Add All Required App Details in Basic Tab
	And : Enter valid URL into URL textbox
	And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
	And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on Publish & Continue button of Preview tab
	And : Click on Save button of Permission tab
  And : Verify that user is allowed to Add New app
	And : Click on New App name link 
	And : Click on Save Draft button
	Then : Verify that Resotre Link should be displayed beside App version cloumn in App Listing Screen
   
@chrome
Scenario: Verify Restore Link functionality by click on Resotre link
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile
	And : Click on App Builder Tile
	And : Click on Add New button of App Builder 
	And : Add All Required App Details in Basic Tab
	And : Enter valid URL into URL textbox
	And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
	And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on Publish & Continue button of Preview tab
	And : Click on Save button of Permission tab
  And : Verify that user is allowed to Add New app
	And : Click on New App name link 
	And : Click on Save Draft button
	And : Click on the Restore link from the Version column 
	Then : Verify that previous version of app should be stored by click on the Restore link
   
@chrome 
Scenario: Verify URL functionality of Instruction for Users from Preview tab
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile
	And : Click on App Builder Tile
	And : Click on Add New button of App Builder 
	And : Add All Required App Details in Basic Tab
	And : Enter valid URL into URL textbox
	And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
	And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on URL displayed in Related Media of Preview tab
	Then : Verify that user should able to click on the URL from Preview tab
	
@chrome 
Scenario: Verify URL functionality of Instruction for Users from Apps module
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile
	And : Click on App Builder Tile
	And : Click on Add New button of App Builder 
	And : Add All Required App Details in Basic Tab
	And : Enter valid URL into URL textbox
	And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
	And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on Publish & Continue button of Preview tab
	And : Click on Save button of Permission tab
  And : Verify that user is allowed to Add New app
	And : Go Back to Home
	And : Click on Apps Tile
	And : Click on Search App of Apps Module
	And : Enter valid app name of App Module search field
	And : Click on Other Apps tile 
	And : Click on Searched app
	And : Click on URL displayed in Related Media 
	Then : Verify that user should able to click on the URL from Apps module
    
@chrome 
Scenario: Verify Upload Document functionality of Instruction for Users from Preview tab
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile
	And : Click on App Builder Tile
	And : Click on Add New button of App Builder 
	And : Add All Required App Details in Basic Tab
	And : Select Upload Document radio button 
	And : Click on the PC icon 
	And : Upload file 
	And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
	And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on Uploaded Document from Related Media of Preview tab
	Then : Verify that user should able to download the document from Preview tab
    
@chrome 
Scenario: Verify Upload Document functionality of Instruction for Users from Apps
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile
	And : Click on App Builder Tile
	And : Click on Add New button of App Builder 
	And : Add All Required App Details in Basic Tab
	And : Select Upload Document radio button 
	And : Click on the PC icon 
	And : Upload file
	And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
	And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on Publish & Continue button of Preview tab
	And : Click on Save button of Permission tab
  And : Verify that user is allowed to Add New app
	And : Go Back to Home
	And : Click on Apps Tile
	And : Click on Search App of Apps Module
	And : Enter valid app name of App Module search field
	And : Click on Other Apps tile 
	And : Click on Searched app
	And : Click on Uploaded Document from Related Media
	Then : Verify that user should able to download the document from Apps module
	
@chrome 
Scenario: Verify Send Email Alert when Record result is a failure Functionality
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile
	And : Click on App Builder Tile
	And : Click on Add New button of App Builder 
	And : Add All Required App Details in Basic Tab  
	And : Select Send Email Alert when Record result is a failure checkbox
	And : Enter valid Email address into Email Address textbox
	And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
	And : Click on Show link of Procedure
	And : Click Acceptance Criteria button of Attribute
	And : Click on Add Acceptance Criteria icon
	And : Select Condition from the Condition drop-down
	And : Select Value from Value drop-down
	And : Click on Save button of Standard Acceptance Criteria screen
	And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on Publish & Continue button of Preview tab
	And : Click on Save button of Permission tab
  And : Verify that user is allowed to Add New app
	And : Go Back to Home
	And : Click on Apps Tile
	And : Click on Search App of Apps Module
	And : Enter valid app name of App Module search field
	And : Click on Other Apps tile 
	And : Click on Searched app
	And : Enter data in all Mandatory fields in such a way that can fail the app record 
	And : Go to Entered Email Address
	Then : Verify that user should get Email regarding Fail App record on entered email address
  
@chrome
Scenario: Verify Share App Link Setting functionality
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile
	And : Click on App Builder Tile
	And : Click on Add New button of App Builder 
	And : Add All Required App Details in Basic Tab  
	And : Select Allow app sharing to the outside users checkbox
  And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on Publish & Continue button of Preview tab
	And : Click on Save button of Permission tab
  And : Verify that user is allowed to Add New app
	And : Go Back to Home
	And : Click on Administration Tile
  And : Click on Share App Link Tile
	And : Click on Share App button of Share App Link page
	And : Add valid data for all the fields and click on the Share App button
	And : Go to the entered email address
	And : Click on the link given into the email
	And : Enter data in all Mandatory fields
	And : Click on Save & Accept button 
	And : Click on Yes button of the pop-up 
	And : Click on I am Finished button 
	Then : Verify that user should able to add app record from link given into the email
	
@chrome
Scenario: Verify Include Linked App(s) Table functionality of Print Settings 
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile
	And : Click on App Builder Tile
	And : Click on Add New button of App Builder 
	And : Add All Required App Details in Basic Tab
	And : select Include Linked App(s) Table checkbox
	And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
	And : Click on Add Rules of Procedure
	And : Select Question from Select Question drop-down
	And : Select Value in the textfield
	And : Select 'Initiate App' from Select Action drop-down
	And : Select App from Select App drop-down of Add Rule screen
	And : Click on Add Rule button of Add Rule screen
	And : Click on Save button of Add Rule screen
	And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on Publish & Continue button of Preview tab
	And : Click on Save button of Permission tab
  And : Verify that user is allowed to Add New app
	And : Go Back to Home
	And : Click on Apps Tile
	And : Click on Search App of Apps Module
	And : Enter valid app name of App Module search field
	And : Click on Other Apps tile
	And : Click on Searched app
	And : Enter data in all Mandatory fields in such a way that can initiate App
	And : Click on Save & Accept button of app record
	And : Click on App name link from Linked App(s)
	And : Click on Print button 
    Then : Verify that Linked App table should displayed when user print the app record

@chrome
Scenario: Verify Header Message for Print functionality of Print Settings
	Given : Navigate to the URL
	And : Enter valid credentials and Click on Login Button
	And : Click on Administration Tile
	And : Click on App Builder Tile
	And : Click on Add New button of App Builder 
	And : Add All Required App Details in Basic Tab
	And : Enter message into Header Message for Print textbox
	And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on Publish & Continue button of Preview tab
	And : Click on Save button of Permission tab
  And : Verify that user is allowed to Add New app
	And : Go Back to Home
	And : Click on Apps Tile
	And : Click on Search App of Apps Module
	And : Enter valid app name of App Module search field
	And : Click on Other Apps tile
	And : Click on Searched app
	And : Enter data in all Mandatory fields in such a way that can initiate App
	And : Click on Save & Accept button of app record
	And : Click on the Print button 
	Then : Verify that Header Message should displayed when user print the app record
  
 @chrome @modify
  Scenario: Verify Linked Information popup label
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on 'View' link of Linked Information
  Then : Verify that system should display the Linked Information popup
    
@chrome @modify
  Scenario: Verify Linked Information popup No Records Found
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder
  And : Add All Required App Details in Basic Tab
  And : Click on Continue button of basic tab
  And : Add All Required Attribute Details in Attribute tab
  And : Click on Continue button of attribute tab
  And : Add all Required Workflow Details in Workflow tab
  And : Click on Continue button of workflow tab
  And : Add all required App Acceptance Criteria in Acceptance tab
  And : Click on Continue button of acceptance tab
  And : Click on Publish & Continue button
  And : Click on Save button of permission tab
  And : Click on 'View' link of Linked Information
  Then : Verify that system should display No Records Found in Linked Information popup
    
@chrome @modify
  Scenario: Verify Linked Information popup for single master app
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Get app name which is not linked with any master app
  And : Click on Administration link from Breadcrumbs
  And : Click on Master App Settings Tile
  And : Click on "Add New" button of Master App listing Screen
  And : Enter all mandatory field with app name
  And : Click on Save button of Add Edit Master App Settings screen
  And : Click on Administration link from Breadcrumbs
  And : Click on App Builder Tile
  And : Search for the App which is used by master app
  And : Click on 'View' link of Linked Information
  Then : Verify Master App name should display in Link Information Popup
  
@chrome @modify 
  Scenario: Verify Linked Information popup for multiple master app
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Get app name which is not linked with any master app
  And : Click on Administration link from Breadcrumbs
  And : Click on Master App Settings Tile
  And : Add a app in multiple master app
  And : Click on Administration link from Breadcrumbs
  And : Click on App Builder Tile
  And : Search for the App which is used by master app
  And : Click on 'View' link of Linked Information
  Then : Verify multiple Master App name should display in Link Information Popup

@chrome @modify
  Scenario: Verify close functionality of Linked Information popup
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
	And : Click on 'View' link of Linked Information
	And : Click on Cross of icon Linked Information popup
	Then : Verify that Cross icon is having proper functionality
    
@chrome @modify
  Scenario: Verify label of Page Size drop-down
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Verify the label of Page Size drop-down
 
@chrome @modify
  Scenario: Verify Copy functionality is proper
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Action icon of App Builder listing
  And : Click on Ok button of pop up
  And : Select the Roles in permission tab
  And : Click on Save button of Permission tab
  Then : Verify that Copy functionality is working properly
  
@chrome @modify 
  Scenario: Verify Copy popup message is proper
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Action icon of App Builder listing
  And : Click on Ok button of pop up
  Then : Verify that Copy message should be proper
  
@chrome @modify 
  Scenario: Verify Color of OK button
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Action icon of App Builder listing
  And : Click on Ok button of pop up
  Then : Verify the color of OK button of popup
  
@chrome @modify 
  Scenario: Verify Required Validation of App Title
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Action icon of App Builder listing
  And : Clear the App Title textfield
  Then : Verify the Required message of the App Title Textfield
  
@chrome @modify
  Scenario: Verify Minimum Validation of App Title
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Action icon of App Builder listing
  And : Enter One Character in the App Title textfield
  Then : Verify the Minimum message of the App Title Textfield
  
@chrome @modify
  Scenario: Verify Maximum Validation of App Title
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Action icon of App Builder listing
  And : Enter More Than 50 Characters in the App Title textfield
  Then : Verify the Maximum message of the App Title Textfield
  
@chrome @ignore @regression
Scenario: Verify the System will display the correct data in Audit Trail
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on App Builder Tile
	And : Click on Add New button of App Builder 
	And : Add All Required App Details in Basic Tab
	And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
	And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on Publish & Continue button of Preview tab
	And : Click on Save button of Permission tab
	And : Click on "View Audit Trail" link of the first record of the App builder listing page
  Then : Verify that system should display correct data in Audit Trail Screen of App Builder
  
@chrome @ignore @regression
Scenario: Verify the System will display the correct Version information in Audit Trail
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on App Builder Tile
	And : Click on Add New button of App Builder 
	And : Add All Required App Details in Basic Tab
	And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
	And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on Publish & Continue button of Preview tab
	And : Click in App name of the created app
	And : Add the Header Message for Print field
	And : Click on Save As Draft button
	And : Click Audit trail of created app
	Then : Verify the Version column of Audit trail screen
	
@chrome @ignore 
Scenario: Verify the functionality of Restore Link
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on App Builder Tile
	And : Click on Add New button of App Builder 
	And : Add All Required App Details in Basic Tab
	And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
	And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on Publish & Continue button of Preview tab
	And : Click in App name of the created app
	And : Add the Header Message for Print field
	And : Click on Save As Draft button
	And : Click Restore link of edited app
	Then : Verify that Publish Version will changed to Version
	
@chrome @ignore
Scenario: Verify the Information Message of Restore Link
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on App Builder Tile
	And : Click on Add New button of App Builder 
	And : Add All Required App Details in Basic Tab
	And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
	And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on Publish & Continue button of Preview tab
	And : Click in App name of the created app
	And : Add the Header Message for Print field
	And : Click on Save As Draft button
	Then : Verify that Information Message of Restore Link is proper after clicking on it
	
@chrome @ignore @pending
Scenario: Verify the Publish Version after saving Draft App
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on App Builder Tile
  And : Click on Add New button of App Builder 
	And : Add All Required App Details in Basic Tab
	And : Click on Continue button of basic tab
	And : Add All Required Attribute Details in Attribute tab
	And : Click on Continue button of attribute tab
	And : Add all Required Workflow Details in Workflow tab
	And : Click on Continue button of workflow tab
	And : Add all required App Acceptance Criteria in Acceptance tab
	And : Click on Continue button of acceptance tab
	And : Click on Publish & Continue button of Preview tab
	And : Click in App name of the created app
	And : Add the Header Message for Print field
	And : Click on Save As Draft button
	And : Click on the App which is Save as Draft
	And : Click on Continue button of basic tab
	And : Click on Continue button of attribute tab
	And : Click on Continue button of workflow tab
	And : Click on Continue button of acceptance tab
	And : Click on Publish & Continue button of Preview tab
	Then : Verify the Publish Version should display proper after saving Drafted app
    
  
    
    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    