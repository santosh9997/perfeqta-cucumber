Feature: Module Name: Users 
Executed script on "https://test1.beperfeqta.com/mdav33" 
	
@chrome
Scenario: Verify Users Page
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
    Then : Verify that Users listing page should be displayed when a user clicks on the Users Tile
	
@chrome 
Scenario: Verify Last button of Users Page Pagination
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
  And : Click on the Last button of Pagination
    Then : Verify that the system should display the last page of the listing screen
	
@chrome
Scenario: Verify First button of Users Page Pagination
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
  And : Click on the Last button of Pagination 
	And : Click on the First button of Pagination 
    Then : Verify that the system should display the first page of the listing screen
	
@chrome 
Scenario: Verify the record count of Attribute listing screen with pagination
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	Then : Verify the record count
	
@chrome 
Scenario: Verify Edit Users Page
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on the first record of the Username column
	Then : Verify that system should be redirect to the Add / Edit User screen when a user click on First record of the Username column
	
@chrome 
Scenario: Verify Save Button Functionality 
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on the first record of the Username column
	And : Click on Save button of Users module
	Then : Verify that system should redirect to the Users listing screen
	
@chrome 
Scenario: Verify Cancel Button Functionality 
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on the first record of the Username column
	And : Click on Cancel button of the Users screen
	Then : Verify that system should be redirect to the Users listing screen when a user click on the Cancel button

@chrome 
Scenario: Verify Color of Save Button
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on the first record of the Username column
	Then : Verify the Save button color should be green for the Users screen
	
@chrome 
Scenario: Verify Color of Cancel Button
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on the first record of the Username column
	Then : Verify that color of Cancel button should be black for the Users screen
	
@chrome @ignore
Scenario: Verify Search Functionality for Users listing screen
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Enter the User name into search box of Users listing screen
  Then : Verify the search result
	
@chrome @ignore
Scenario: Verify the Ascending order functionality for Username column of Users listing screen
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Sorting icon of the Username column for ascending order sorting
    Then : Verify that all the records of Username column display in ascending order

@chrome @ignore
Scenario: Verify the Descending order functionality for Username column of Users listing screen
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Sorting icon of the Username column for descending order sorting
    Then : Verify that all the records of Username column display in descending order
	
@chrome 
Scenario: Verify View Audit Trail Page redirection of Users
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on "View Audit Trail" link of the first record of the listing page
	Then : Verify the user redirecting to Audit trail of record
	
@chrome 
Scenario: Verify Audit Trail Page Pagination
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on "View Audit Trail" link of the first record of the listing page
	Then : Verify the record count

@chrome	 
Scenario: Verify Audit Trail Page Back Button color
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on "View Audit Trail" link of the first record of the listing page
	Then : Verify that color of Back button should be black from the Audit Trail screen under Users module
	
@chrome 
Scenario: Verify Audit Trail back button functionality of Users
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on "View Audit Trail" link of the first record of the listing page
	And : Click on Back button of Users
	Then : Verify that a system should be redirect to the Users listing screen when a user click on the Back button
	
@chrome  
Scenario: Verify Mandatory validation message for Username textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Add New button of Users listing screen
	And : Click on Username textbox and Press the "TAB" key
	Then : Verify the Mandatory validation message for Username field as "Username is required." 
	
@chrome 
Scenario: Verify Minimum validation message for Username textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Add New button of Users listing screen
	And : Click on Username textbox and Enter Username as "T"	 
	Then : Verify Minimum validation message of Username textbox as "Username must be at least 2 characters."
	
@chrome 
Scenario: Verify Maximum validation message for Username textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Add New button of Users listing screen
	And : Click on Username textbox and Enter Username value more than 20 characters
	Then : Verify Maximum validation message of Username textbox as "Username cannot be more than 20 characters."
	
@chrome 
Scenario: Verify Unique validation message for Username textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Add New button of Users listing screen
	And : Click on Username textbox and enter Duplicate Username
	Then : Verify Unique validation message of Username textbox as "User Name must be unique."
	
@chrome 
Scenario: Verify Mandatory validation message for First Name textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Add New button of Users listing screen
	And : Click on First Name textbox and Click on the "TAB" Key
	Then : Verify the Mandatory validation message for First Name field as "First Name is required."
	
@chrome
Scenario: Verify Minimum validation message for First Name textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Add New button of Users listing screen
	And : Click on First Name textbox and enter First Name as "T"	
	Then : Verify Minimum validation message of First Name textbox as "Firstname must be at least 2 characters."
	
@chrome 
Scenario: Verify Maximum validation message for First Name textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Add New button of Users listing screen
	And : Click on First Name textbox and enter First Name value more than 50 characterss
	Then : Verify Maximum validation message of First Name textbox as "First Name cannot be more than 50 characters."

@chrome  
Scenario: Verify Mandatory validation message for Last Name textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Add New button of Users listing screen
	And : Click on Last Name textbox and click on the "TAB" Key
	Then : Verify the Mandatory validation message for Last Name field as "Last Name is required."
	
@chrome 
Scenario: Verify Minimum validation message for Last Name textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Add New button of Users listing screen
	And : Click on Last Name textbox and enter Last Name as "T"	
	Then : Verify Minimum validation message of Last Name textbox as "Last Name must be at least 2 characters."

@chrome 
Scenario: Verify Maximum validation message for Last Name textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Add New button of Users listing screen
	And : Click on Last Name textbox and enter Last Name value more than 50 characters
	Then : Verify Maximum validation message of Last Name textbox as "Last Name cannot be more than 50 characters."
	
@chrome
Scenario: Verify Mandatory validation message for Email Address textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Add New button of Users listing screen
	And : Click on Email Address textbox and click on the "TAB" Key
	Then : Verify the Mandatory validation message for Email Address textbox as "Email Address is required."

@chrome 
Scenario: Verify Invalid validation message for Email Address textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Add New button of Users listing screen
	And : Click on Email Address textbox and enter Email Address as "abc12"
	Then : Verify the validation message for Email Address textbox as "Invalid Email Address."	
	
@chrome 
Scenario: Verify Unique validation message for Email Address textbox
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Add New button of Users listing screen
	And : Click on Email Address textbox and enter Duplicate Email Address
	Then : Verify Unique validation message of Email Address textbox as "Email must be unique."

@chrome 
Scenario: Verify Add Functionality of Users
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Add New button of Users listing screen
	And : Enter valid Username into Username textbox
	And : Enter valid First Name into First Name textbox
	And : Enter valid Last Name into Last Name textbox
	And : Enter valid Email Address into Email Address textbox
	And : Enter valid Department into Department textbox
	And : Select Generate Password as "By Admin"
	And : Enter valid new password
	And : Enter valid confirm password 
	And : Select Roles to Create User
	And : Select Sites to Create User
	And : Click on Save button of Users
	Then : Verify that Add User functionality should be working properly when a user click on Save button 
	
@chrome 
Scenario: Verify Edit Functionality of Users
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on the first record of the Username column
	And : Edit User Detail and click on save button for simple function
	Then : Verify that record should be updated and redirected to the Users listing screen

@chrome 
Scenario: Verify that Reset Link is Clickable or not
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Reset link of first record from the Action column
	Then : Verify that Reset link should be clickable
	
@chrome 
Scenario: Verify that Current User is able to reset access of Active Users only
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Reset link of first record from the Action column
	Then : Verify that a loggedin user is allowed to reset access of active users 
	
@chrome  
Scenario: Verify Required validation message for New Password Field
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Reset link of Active User record from the Action column
	And : Select By Admin option from Generate Password dropdown
	And : Click on the New Password textbox and enter the new password value
	Then : Verify the Mandatory validation message for password field as "New Password is required."
	
@chrome 
Scenario: Verify the Invalid vaildation message for New Password
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
	And : Click on Users Tile
	And : Click on Reset link of Active User record from the Action column
	And : Select By Admin option from Generate Password dropdown
	And : Click on the New Password textbox and eneter enter New Password as "abc"
	Then : Verify that Invalid vaildation message for New Password should be displayed as "New Password must have the following properties.\n" + 
				
	
@chrome 
Scenario: Verify Required validation message for Confirm Password Field
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Reset link of Active User record from the Action column
	And : Select By Admin option from Generate Password dropdown
	And : Click on the Confirm Password textbox and click on "TAB" Key
	Then : Verify the Mandatory validation message for confirm password field as "Confirm Password is required."
	
@chrome 
Scenario: Verify validation message New Password and Confirm Password is same or not
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Reset link of Active User record from the Action column
	And : Select By Admin option from Generate Password dropdown
	And : Click on New Password textbox and enter New Password as "Test@123"
	And : Click on Confirm Password textbox and enter Confirm Password as "Test@1234"
   Then : Verify that validation message should be displayed as Confirm Password and New Password must be exactly same.
	
@chrome 
Scenario: Verify that Reset Password functionality is working or not by selecting By Admin option from Generate Password dropdown
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
  And : Enter valid User name into search box of Users listing screen for reset function
	And : Click on Reset link of Active User record from the Action column
	And : Select By Admin option from Generate Password dropdown
	And : Click on New Password textbox and enter value into New Password textbox
	And : Click on Confirm Password textbox and enter value into Confirm Password textbox
	And : Click on Submit button
	Then : Verify that a user is allowed to reset password by selecting By Admin option
	
@chrome 
Scenario: Verify that Reset Password functionality is working or not by selecting Auto Generate option from Generate Password dropdown
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
  And : Enter valid User name into search box of Users listing screen for reset function
	And : Click on Reset link of Active User record from the Action column
	And : Select Auto Generate option from Generate Password dropdown
	And : Click on Submit button
	Then : Verify that a user is allowed to reset password by selecting Auto Generate option
	
@chrome 
Scenario: Verify the Cross button functionality in Search field.
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
  And : Enter valid User name into search box of Users listing screen
  And : Click on Cross Button of User Screen
    Then : Verify entered User Name in Search box should be cleared

@chrome
	Scenario: Verify the Page Size functionality for 10 in User Screen
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile    
  And : Select page size Ten in User Screen
    Then : Verify every page grid should be display Ten records
    
@chrome 
	Scenario: Verify the Page Size functionality for 20 in User Screen
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile    
  And : Select page size Twenty in User Screen
    Then : Verify every page grid should be display Twenty records
    
@chrome
	Scenario: Verify the Page Size functionality for 50 in User Screen
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile    
  And : Select page size Fifty in User Screen
    Then : Verify every page grid should be display Fifty records
    
@chrome 
Scenario: Verify the Pagination functionality for 10 in User Screen
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile    
  And : Select page size Ten in User Screen
    Then : Verify Pagination with Ten records
    
@chrome 
Scenario: xPathVerify the Pagination functionality for 20 in User Screen
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile    
  And : Select page size Twenty in User Screen
    Then : Verify Pagination with Twenty records
    
@chrome 
Scenario: Verify the Pagination functionality for 50 in User Screen
	Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile    
  And : Select page size Fifty in User Screen
    Then : Verify Pagination with Fifty records    
    
@chrome
Scenario: Verify the Audit Trail link is displaying for all the Users
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile    
  And : Select page size Fifty in User Screen
 Then : Verify Audit Trail link should display in all users

    
@chrome @ignore
Scenario: Verify Search Functionality of User Name in Users Screen
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
  And : Enter valid User name into search box of Users listing screen
  Then : Verify search result for User Name
  
@chrome @ignore
Scenario: Verify Search Functionality of User Role in Users Screen
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
  And : Enter valid User role into search box of Users listing screen
  Then : Verify search result for User role
  
@chrome @ignore
Scenario: Verify Search Functionality of User Full Name in Users Screen
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
  And : Enter valid User Full Name into search box of Users listing screen
  Then : Verify search result for User Full Name 
  
@chrome @ignore
Scenario: Verify Search Functionality of Department in Users Screen
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
  And : Enter valid Department into search box of Users listing screen
  Then : Verify search result for Department

@chrome @pending @ignore
Scenario: Verify the System will display the correct data in Audit Trail
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
	And : Click on Add New button of Users listing screen
	And : Enter valid Username into Username textbox
	And : Enter valid First Name into First Name textbox
	And : Enter valid Last Name into Last Name textbox
	And : Enter random valid Email Address into Email Address
	And : Enter valid Department into Department textbox
	And : Select Generate Password as "By Admin"
	And : Enter valid new password
	And : Enter valid confirm password 
	And : Select Roles to Create User
	And : Select Sites to Create User for correct data
	And : Click on Save button of Users
  And : Click on "View Audit Trail" link of the first record of the listing page
  Then : Verify The System should display correct data in Audit Trail Screen
  
@chrome @Bug
Scenario: Verify the field display the Old value and New value in Audit Trail
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
  And : Enter valid User name into search box of Users listing screen
  And : Click on "View Audit Trail" link of the first record of the listing page
  And : Get Audit Trail Entries in Audit Trail Screen
  And : Click on Back button of Users
  And : Click on User Name of Search Result for old and new value
  And : Click on every field of user and Edit
  And : Click on Save button of Users
  Then : Verify Old value and New value field in Audit Trail Screen
  
@chrome 
Scenario: Verify the Name of the Audit Trail screen will display the correct User Name
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
  And : Get First User Name in user screen
  And : Click on "View Audit Trail" link of the first record of the listing page
  Then : Verify Audit Trail should display correct user name
  
@chrome 
Scenario: Verify the Version when user update the any data from the user
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button 
  And : Click on Administration Tile
  And : Click on Users Tile
  And : Click on "View Audit Trail" link of the first record of the listing page
  And : Get the version of that User
  And : Click on Back button of Users
  And : Click on the first record of the Username column
  And : Edit User Detail and click on save button
  And : Click on "View Audit Trail" link of the first record of the listing page
  Then : Verify Version should be updated in Audit Trail Page
  
@chrome  
Scenario: Verify the Modified by field display the correct value
  Given : Navigate to the URL 
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Users Tile
  And : Click on the first record of the Username column
  And : Edit User Detail and click on save button
  Then : Verify Modified by field should Display the correct value 

@chrome 
Scenario: Verify the Date & Time field will display the correct time
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Users Tile
  And : Click on the first record of the Username column
  And : Edit User Detail and click on save button
  And : Click on "View Audit Trail" link of the first record of the listing page
  Then : Verify Date and Time should be Correct in Audit Trail Page

@chrome 
Scenario: Verify Functionality of License Agreements button 
  Given : Navigate to the URL
  And : Enter valid credentials and Click on Login Button
  And : Click on Administration Tile
  And : Click on Users Tile
  And : Click on License Agreements button
  Then : Verify License Agreements Screen should be opened 
  
  
  
  
  
  