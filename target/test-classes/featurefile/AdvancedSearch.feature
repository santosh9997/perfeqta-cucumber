Feature: Module Name: Advanced Search
  Executed script on "https://test1.beperfeqta.com/mdav33"

  @chrome
  Scenario: Verify the module title when user click on "Advanced Search" module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify module name as "App Record Search"

  @chrome
  Scenario: Verify Breadcrumb of Advanced Search module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that system should display Breadcrumb for Advanced Search module as Home > App Record Search

  @chrome
  Scenario: Verify Record Search type label of advanced search module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that system should display Record Search type label as App Record Search

  @chrome
  Scenario: Verify Color of Record Search type label for advanced search module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that Color of Record Search type label should be blue

  @chrome
  Scenario: Verify all module name in Advanced Search
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that system should display all module name in Advanced Search

  @chrome
  Scenario: Verify all modules are selected by default in Advanced Search
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that all modules should be selected by default in Advanced Search

  @chrome
  Scenario: Verify check all modules
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Uncheck all module name
    And : Checked all module name
    Then : Verify that a user is allowed to Check all the module name from Advanced Search screen

  @chrome
  Scenario: verify Uncheck All Module
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Uncheck all module name
    Then : Verify that a user is allowed to Uncheck all the module name from Advanced Search screen

  @chrome
  Scenario: Verify Clear button functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Search for Apps by Name Search box
    And : Search and Add App from Search for Apps by Name Search box
    And : Click on Clear button
    Then : Verify that system should remove the App Name from Search box when a user click on the Clear button

  @chrome
  Scenario: Verify No apps found message
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Search for Apps by Name Search box
    And : Enter invalid App Name into Search for Apps by Name Search box
    Then : Verify that system should display "No apps found." message when a user enters invalid app name

  @chrome
  Scenario: Verify search apps functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Search for Apps by Name Search box
    And : Enter App name into Search for Apps by Name Search box
    Then : Verify the Search result

  @chrome
  Scenario: Verify search Multiple apps functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Search for Apps by Name Search box
    And : Search and Select multiple App Name into Search for Apps by Name Search box
    Then : Verify search Multiple apps functionality

  @chrome
  Scenario: Verify No Record Found message
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Search for Apps by Name Search box
    And : Enter App Name having 0 record in Search for Apps by Name Search box
    And : Select the App
    Then : Verify that system should display "No record found." message when a user selects App having 0 record

  @chrome
  Scenario: Verify Records You Created Today text
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that Records You Created Today text should be display under Quick Access

  @chrome
  Scenario: Verify Records Pending Your Review text
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that Records Pending Your Review text should display under Quick Access

  @chrome
  Scenario: Verify Records You Accessed Today text
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that Records You Accessed Today text should display under Quick Access

  @chrome
  Scenario: Verify Records Assigned To You text
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that Records Assigned To You text should display under Quick Access

  @chrome
  Scenario: Verify Records You Created Today Color
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that Color of Records You Created Today text should be blue

  @chrome
  Scenario: Verify Records Pending Your Review Color
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that Color of Records Pending Your Review text should be blue

  @chrome
  Scenario: Verify Records You Accessed Today Color
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that Color of Records You Accessed Today text should be blue

  @chrome
  Scenario: Verify Records Assigned To You Color
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that Color of Records Assigned To You text should be blue

  @chrome
  Scenario: verify Collapes Button Functionality for Filter By Section
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Collapes button
    Then : Verify that system should Collapes the Filter By options when a user click on the Collapes button

  @chrome
  Scenario: Verify Color of Go button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    Then : Verify that Color of Go button should be green

  @chrome @test
  Scenario: Verify Searched App Grid Display when search an app
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box for bacth review
    And : Click on Go button
    Then : Verify that system should display app record in grid when a user search an app

  @chrome
  Scenario: Verify Searched App Grid no record message
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Search for Apps by Name Search box
    And : Enter App Name having 0 record in Search for Apps by Name Search box
    And : Select the App
    Then : Verify that system should display "No record found." message when a user selects App having 0 record

  @chrome
  Scenario: Verify click on Clear All button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Clear All button
    Then : Verify that Clear All button is clickable or not

  @chrome
  Scenario: Verify  Color of Clear All button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that Color of Clear All button should be black

  @chrome
  Scenario: Verify Clear All button functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Clear All button
    Then : Verify that system should remove all selection from Advanced Search screen when a user click on the Clear All button

  @chrome
  Scenario: Verify Collapes button of grid
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Collapes button of Grid
    Then : Verify that system should Collapes the grid record when a user click on the Collapes button

  @chrome
  Scenario: Verify CSV icon is displaying
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    Then : Verify that system should display CSV icon at right side of the Grid

  @chrome
  Scenario: Verify Excel icon is displaying
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    Then : Verify that system should display Excel icon at right side of the Grid

  @chrome
  Scenario: Verify PDF icon is displaying
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    Then : Verify that system should display PDF icon at right side of the Grid

  @chrome
  Scenario: Verify Color of add to Favorite button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    Then : Verify that Color of Add To Favorite button should be blue

  @chrome
  Scenario: Verify add to favorite Require Validation message
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    And : Click on Save button of Add To Favorite pop-up
    Then : Verify that system should display Require Validation message for Favorite Name as "Favorite Name is required."

  @chrome
  Scenario: Verify Color of add to favorite Require Validation
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    And : Click on Save button of Add To Favorite pop-up
    Then : Verify that Color of Require validation message color should be red

  @chrome
  Scenario: Verify add to favorite Minimum Validation
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    And : Click on Enter Favorite Name textbox
    And : Enter value in Favorite Name as "T"
    Then : Verify that system should display Minimum validation message for Favorite Name as "Favorite Name must be at least 2 characters."

  @chrome
  Scenario: Verify Color of add to favorite Minimum Validation
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    And : Click on Enter Favorite Name textbox
    And : Enter value in Favorite Name as "T"
    Then : Verify that Color of Minimum validation message should be red

  @chrome
  Scenario: Verify add to favorite Maximum Validation
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    And : Click on Enter Favorite Name textbox
    And : Enter Favorite Name value more than 50 characters
    Then : Verify Maximum validation message of Favorite Name textbox as "Favorite Name cannot be more than 50 characters."

  @chrome
  Scenario: Verify Color of add to favorite Maximum Validation
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    And : Click on Enter Favorite Name textbox
    And : Enter Favorite Name value more than 50 characters
    Then : Verify the Color of Maximum validation message for Favorite Name should be red

  @chrome
  Scenario: Verify add to favorite No Validation
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    And : Click on Enter Favorite Name textbox
    And : Enter Valid Favorite Name value
    Then : Verify that system should not display validation message when a user enters valid Favorite Name

  @chrome
  Scenario: Verify add to favorite Label text
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    Then : Verify that system should display Add To Favorite label on pop-up

  @chrome
  Scenario: Verify add to favorite Cross button function
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    And : Click on close icon of Add To Favorite pop-up
    Then : Verify that system should close the Add To Favorites pop-up

  @chrome
  Scenario: Verify add to favorite Cancel function
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    And : Click on Cancel button of Advanced Search
    Then : Verify that system should remove the Add to Favorite pop-up when a user click on the Cancel button

  @chrome
  Scenario: Verify My Favorites radio button is selected in add to favorite pop-up
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    Then : Verify that by default My Favorites radio button is selected in add to favorite pop-up

  @chrome
  Scenario: Verify add to favorite save button Color
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    Then : Verify that Save button Color should be green

  @chrome
  Scenario: Verify add to favorite cancel button Color
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    Then : Verify that Cancel button Color should be black

  @chrome
  Scenario: Verify add to favorite save functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    And : Enter Valid Favorite Name value
    And : Click on Save button of Advanced Search
    Then : Verify that system should Save the App in My Favorites list when a user click on the Save button

  @chrome
  Scenario: Verify add to favorite Unique Validation
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    And : Enter Already Exist Name in Favorite Name
    Then : Verify that system should display Unique validation message as "Favorite Name must be unique."

  @chrome
  Scenario: Verify My Favorite Remove pop-up Label Text
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Now Click on the Remove button for Favorite App Name
    Then : Verify that system should display My Favorite Remove pop-up label as "Are you sure you want to remove 'Saved Name'?"

  @chrome
  Scenario: Verify Yes button Color in My Favorite Remove pop-up
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Now Click on the Remove button for Favorite App Name
    Then : Verify that Color of Yes button in My Favorite Remove pop-up should be green

  @chrome
  Scenario: Verify No button Color in My Favorite Remove pop-up
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Now Click on the Remove button for Favorite App Name
    Then : Verify that Color of No button in My Favorite Remove pop-up should be red

  @chrome
  Scenario: Verify No button functionality in My Favorite Remove pop-up
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Now Click on the Remove button for Favorite App Name
    And : Click on the No button
    Then : Verify that system should not remove Favorite Name from My Favorites list when a user click on the No button

  @chrome @ignore
  Scenario: Verify add to favorite save functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    And : Enter Valid Favorite Name value
    Then : Verify that Favorite Name should be added to the My Favorites list when a user click on the Save button

  @chrome
  Scenario: Verify Filter Label
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that system should display Filter By label below App Name Search box

  @chrome
  Scenario: Verify All Filter icons are visiable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    Then : Verify that All Filter icons are visiable when a user expand the Filter By Section

  @chrome
  Scenario: Verify spelling of Date range icon
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    Then : Verify that spelling of Date range icon should be Correct

  @chrome
  Scenario: Verify spelling of Attributes/Entity icon
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    Then : Verify that spelling of Attributes/Entity icon should be Correct

  @chrome
  Scenario: Verify spelling of Sites icon
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    Then : Verify that spelling of Sites icon should be Correct

  @chrome
  Scenario: Verify spelling of Batch Entries icon
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    Then : Verify that spelling of Batch Entries icon should be Correct

  @chrome
  Scenario: Verify spelling of Assignee icon
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    Then : Verify that spelling of Assignee icon should be Correct

  @chrome
  Scenario: Verify spelling of Record Status icon
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    Then : Verify that spelling of Record Status icon should be Correct

  @chrome
  Scenario: Verify Today Date calender is displaying a cuurent date
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Select Today option from Date dropdown for Date Range Filter
    Then : Verify that system should display Current Date when a user selects Today option from Date dropdown

  @chrome
  Scenario: Verify This week is displaying a Correct date
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Select This week option from Date dropdown for Date Range Filter
    Then : Verify that system should display Correct Dates when a user selects This week option from Date dropdown

  @chrome
  Scenario: Verify By Default Date Range is Selected When select This Week option
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Select This week option from Date dropdown for Date Range Filter
    Then : Verify that system should display By Default Date range for This Week option

  @chrome
  Scenario: Verify This Month is displaying a correct date
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Select This Month option from Date dropdown for Date Range Filter
    Then : Verify that system should display Correct Dates when a user selects This Month option from Date dropdown

  @chrome
  Scenario: Verify By Default Date Range is Selected When select This Month option
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Select This Month option from Date dropdown for Date Range Filter
    Then : Verify that system should display By Default Date range for This Month option

  @chrome
  Scenario: Verify Clear link is visible for Date Range Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    Then : Verify that Clear link is visible to the user or not for Date Range Filter

  @chrome
  Scenario: Verify Color of Clear Link for Date Range Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    Then : Verify that Color of Clear link for Date Range Filter should be blue

  @chrome
  Scenario: Verify Date Range Clear Link Functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Clear link of Date Range
    Then : Verify that system should Clear all the Selection for Date Range Filter

  @chrome
  Scenario: Verify Remove Icon is Visible for Date Range Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    Then : Verify that system should display Remove Icon for Date Range Filter

  @chrome
  Scenario: Verify Month-Year radio button functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Select Month-Year radio button
    Then : Verify that system should display Select Month and Select Year dropdown when a user selects Month-Year radio button

  @chrome
  Scenario: verify Remove Icon Color for Date Range Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    Then : Verify that remove icon Color for Date Range Filter should be red

  @chrome
  Scenario: verify Date Range This Week Filter With Grid Functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App having this week record from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Select This week option from Date dropdown for Date Range Filter
    Then : Verify that system should display only this week record in Grid for selected App when user select This week Filter

  @chrome
  Scenario: verify Date Range Today Filter With Grid Functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App having Today record from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Select Today option from Date dropdown for Date Range Filter
    Then : Verify that system should display only Today record in Grid for selected App when user select Today Filter

  @chrome
  Scenario: verify Date Range This Month Filter With Grid Functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App having This Month record from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Select This Month option from Date dropdown for Date Range Filter
    Then : Verify that system should display only This Month record for selected App when user select This Month Filter

  @chrome
  Scenario: verify Date Range Specific Date Filter With Grid Functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Select Specific Date option from Date dropdown for Date Range Filter
    And : Select Specific Date from Date Picker
    Then : Verify that system should display record for selected Date for the Selected app when a user selects Specific Date option

  @chrome
  Scenario: verify Required Validation Message for Select Year in Date Range
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Select Month-Year radio button
    And : Select Month from Select Month dropdown
    Then : Verify that Required Validation message should be display as "Year is required."

  @chrome
  Scenario: verify Color of Required Validation message for Year
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Select Month-Year radio button
    And : Select Month from Select Month dropdown
    Then : Verify that Color of Required Validation for Date Range should be red

  # Pending and will be solved later.
  @chrome @ignore
  Scenario: verify Date Range Search By Month Functionality
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Select Month-Year radio button
    And : Select Month from Select Month dropdown
    And : Select Year from Select Year dropdown
    Then : Verify that system should display Records of selected month and year for the Selected App

  @chrome
  Scenario: Verify Attributes/Entity Filter Label
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    Then : Verify that system should display "Attributes/Entity Key Attributes" label for Attributes/Entity Filter

  @chrome
  Scenario: Verify Required validation message for Attributes/Entity Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Click on Attributes/Entity icon
    And : Select Attributes/Entity Key Attributes from dropdown
    Then : Verify that system display "Value is required" validation message when a user doesn't select value of Selected Attributes/Entity Key Attributes

  @chrome
  Scenario: Verify Color of Required validation message for Attributes/Entity Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Click on Attributes/Entity icon
    And : Select Attributes/Entity Key Attributes from dropdown
    Then : Verify that Color of Required validation message should be red

  @chrome
  Scenario: Verify Add New Attribute Button is Visible for Attributes/Entity Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Attributes/Entity icon
    Then : Verify that system should display Add New Attribute Button

  @chrome
  Scenario: Verify Color of Add New Attribute Button of Attributes/Entity Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Attributes/Entity icon
    Then : Verify that Color of Add New Attribute button should be green

  @chrome
  Scenario: Verify Add New Attribute functionality for Attributes/Entity Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Click on Attributes/Entity icon
    And : Select Attributes/Entity Key Attributes from dropdown
    And : Enter Value for the Selected Attribute/Entity
    And : Click on Add New Attribute button
    Then : Verify that system should display the app record as per Attribute/Entity Filter

  @chrome
  Scenario: Verify Remove Attributes/Entity Button is visible
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Click on Attributes/Entity icon
    And : Select Attributes/Entity Key Attributes from dropdown
    And : Enter Value for the Selected Attribute/Entity
    And : Click on Add New Attribute button
    Then : Verify that system should display Remove Attributes/Entity button when a user adds Attributes/Entity

  @chrome
  Scenario: Verify Color of Attributes/Entity Remove Attribute Button
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Click on Attributes/Entity icon
    And : Select Attributes/Entity Key Attributes from dropdown
    And : Enter Value for the Selected Attribute/Entity
    And : Click on Add New Attribute button
    Then : Verify that Color of Attributes/Entity Remove should be red

  @chrome
  Scenario: Verify Remove Attribute functionality for Attributes/Entity Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Click on Attributes/Entity icon
    And : Select Attributes/Entity Key Attributes from dropdown
    And : Enter Value for the Selected Attribute/Entity
    And : Click on Add New Attribute button
    And : Click on Remove Attributes/Entity button
    Then : Verify that system should remove the Added Attribute/Entity when a user click on the Remove button

  @chrome
  Scenario: Verify Attributes/Entity search by Key Attributes is displaying correct data in grid
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Click on Attributes/Entity icon
    And : Select Attributes/Entity Key Attributes from dropdown
    And : Enter Value for the Selected Attribute/Entity
    Then : Verify that system should display Correct data in grid as per applied filter for Key Attribute

  @chrome
  Scenario: Verify Clear link is visible for Attributes/Entity Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Click on Attributes/Entity icon
    Then : Verify that system should display Clear link for Attributes/Entity Filter

  @chrome
  Scenario: Verify Color of Clear link for Attributes/Entity Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Click on Attributes/Entity icon
    Then : Verify that Color of Clear link for Attributes/Entity Filter should be blue

  @chrome
  Scenario: Verify Clear Link Functionality for Attributes/Entity Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Click on Attributes/Entity icon
    And : Select Attributes/Entity Key Attributes from dropdown
    And : Enter Value for the Selected Attribute/Entity
    And : Click on Clear link
    Then : Verify that system should Clear all Selection for Attribute/Entity filter when a user click on the Clear link

  @chrome
  Scenario: Verify Remove Icon is visible for Attributes/Entity Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Click on Attributes/Entity icon
    And : Select Attributes/Entity Key Attributes from dropdown
    And : Enter Value for the Selected Attribute/Entity
    And : Click on Add New Attribute button
    Then : Verify that system should display Remove icon for Attributes/Entity Filter

  @chrome
  Scenario: Verify Color of Remove Icon for Attributes/Entity Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Click on Attributes/Entity icon
    And : Select Attributes/Entity Key Attributes from dropdown
    And : Enter Value for the Selected Attribute/Entity
    And : Click on Add New Attribute button
    Then : Verify that Color of Remove icon for Attributes/Entity filter should be red

  @chrome
  Scenario: Verify Remove Icon Functionality for Attributes/Entity
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Click on Attributes/Entity icon
    And : Select Attributes/Entity Key Attributes from dropdown
    And : Enter Value for the Selected Attribute/Entity
    And : Click on Add New Attribute button
    And : Click on Remove icon
    Then : Verify that system should Remove the Attributes/Entity Key Attributes section when a user click on Remove icon

  @chrome
  Scenario: Verify Label of Sites Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    Then : Verify that system should display "Sites" label for Sites Filter

  @chrome
  Scenario: Verify Clear link is visible for Sites Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Sites icon
    Then : Verify that system should display Clear link for Sites Filter

  @chrome
  Scenario: Verify Color of Clear link for Sites Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Sites icon
    Then : Verify Color of Clear link for Sites Filter should be blue

  @chrome
  Scenario: verify Clear Link Functionality for Sites
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Sites icon
    And : Select value form the dropdown for Sites
    And : Click on Clear link of Sites
    Then : Verify that system should Clear selection for Sites when a user click on the Clear link

  @chrome
  Scenario: verify Remove Icon is visible for Sites Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Sites icon
    Then : Verify that system should display Remove icon for Sites filter

  @chrome
  Scenario: Verify Color of Remove Icon for Sites Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Sites icon
    Then : Verify that Color of Remove icon should be red for Sites filter

  @chrome
  Scenario: Verify Remove Icon Functionality for Sites Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Sites icon
    And : Click on Remove icon of Sites
    Then : Verify that system should Remove Sites section from Filter when a user click on Remove icon

  @chrome
  Scenario: Verify Label of Batch Entries Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    Then : Verify that system should display Batch Entries label for Batch Entries filter

  @chrome
  Scenario: Verify Maximum validation message for Batch Entries Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Batch Entries filter icon
    And : Enter more than 20 characters into Batch Entry ID textbox
    Then : Verify that system should display Maximum validation message as "Batch Entry ID must not be more than 20 characters."

  @chrome
  Scenario: Verify Color of Maximum validation message for Batch Entries Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Batch Entries filter icon
    And : Enter more than 20 characters into Batch Entry ID textbox
    Then : Verify that Color of Maximum validation message should be red

  @chrome
  Scenario: Verify Batch Entries search is displaying correct data in grid
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Expand button of Filter By section
    And : Click on Batch Entries filter icon
    And : Click on Batch Entries ID textbox
    And : Enter Valid Batch Entry ID into Batch Entry ID textbox
    Then : Verify that system should display correct records in Grid as per applied filter for Batch Entry ID

  @chrome
  Scenario: Verify Clear link is visible for Batch Entry ID filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Batch Entries filter icon
    Then : Verify that system should display Clear link for Batch Entry ID Filter

  @chrome
  Scenario: Verify Color of Clear link for Batch Entries filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Batch Entries filter icon
    Then : Verify that Color of Clear link for Batch Entries Filter should be blue

  @chrome
  Scenario: Verify Clear Link Functionality for Batch Entries filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Batch Entries filter icon
    And : Click on Batch Entries ID textbox
    And : Enter Valid Batch Entry ID into Batch Entry ID textbox
    And : Click on Clear link of Batch Entries
    Then : Verify that system should Clear Batch Entry ID from the Batch Entry ID textbox

  @chrome
  Scenario: Verify Remove Icon is visible for Batch Entries filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Batch Entries filter icon
    Then : Verify that system should display Remove icon for Batch Entries filter

  @chrome
  Scenario: Verify Color of Remove Icon Batch Entries filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Batch Entries filter icon
    Then : Verify that Color of Remove icon should be red for Batch Entries filter

  @chrome
  Scenario: Verify Remove Icon Functionality for Batch Entries filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Batch Entries filter icon
    And : Click on Batch Entries ID textbox
    And : Enter Valid Batch Entry ID into Batch Entry ID textbox
    And : Click on Remove icon of Batch Entry
    Then : Verify that system should remove Batch Entries Section when a user click on the Remove icon

  @chrome
  Scenario: Verify Label of Assignee radio button for Assignee Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Assignee filter icon
    Then : Verify that system should display Assignee label for 1st radio button

  @chrome
  Scenario: Verify Label of Assignee To Me radio button for Assignee Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Assignee filter icon
    Then : Verify that system should display Assignee To Me label for 2nd radio button

  @chrome
  Scenario: Verify default selected Radio button is Assignee
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Assignee filter icon
    Then : Verify that Assignee radio button should be selected by default

  @chrome
  Scenario: Verify Check All Functionality Assignee Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Assignee filter icon
    And : Click on Please Select Role(s) Dropdown
    And : Click on Checked All option
    Then : Verify that system should select All the option of Select Role(s) dropdown

  @chrome
  Scenario: Verify Uncheck All Functionality Assignee Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Assignee filter icon
    And : Click on Please Select Role(s) Dropdown
    And : Click on Checked All option
    And : Click on Uncheck All option of Advanced Search
    Then : Verify that system should deselect All the option of Select Role(s) dropdown

  @chrome
  Scenario: Verify Label of Record Status Filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    Then : Verify that system should display Record Status label

  @chrome
  Scenario: Verify Number of Available Record Status
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    Then : Verify that system should display six Checkbox for Record Status

  @chrome
  Scenario: Verify Label of Passed Record Status
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    Then : Verify that system should display label as Passed for checkbox of Passed Record Status

  @chrome
  Scenario: Verify Label of Failed Record Status
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    Then : Verify that system should display label as Failed for checkbox of Failed Record Status

  @chrome
  Scenario: Verify Label of Review Pending Record Status
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    Then : Verify that system should display label as Review Pending for checkbox of Review Pending Record Status

  @chrome
  Scenario: Verify Label of Void Record Status
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    Then : Verify that system should display label as Void for checkbox of Void Record Status

  @chrome
  Scenario: Verify Label of Draft Record Status
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    Then : Verify that system should display label as Draft for checkbox of Draft Record Status

  @chrome
  Scenario: Verify Label of Rejected Record Status
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    Then : Verify that system should display label as Rejected for checkbox of Rejected Record Status

  @chrome
  Scenario: Verify Record Status Passed is clickable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    And : Click on Passed checkbox
    Then : Verify that record status Passed Checkbox is clickable or not

  @chrome
  Scenario: Verify Record Status Failed is clickable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    And : Click on Failed checkbox
    Then : Verify that record status Failed Checkbox is clickable or not

  @chrome
  Scenario: Verify Record Status Review Pending is clickable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    And : Click on Review Pending checkbox
    Then : Verify that record status Review Pending Checkbox is clickable or not

  @chrome
  Scenario: Verify Record Status Void is clickable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    And : Click on Void checkbox
    Then : Verify that record status Void Checkbox is clickable or not

  @chrome
  Scenario: Verify Record Status Draft is clickable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    And : Click on Draft checkbox
    Then : Verify that record status Draft Checkbox is clickable or not

  @chrome
  Scenario: Verify Record Status Rejected is clickable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    And : Click on Rejected checkbox
    Then : Verify that record status Rejected Checkbox is clickable or not

  @chrome
  Scenario: Verify Remove Icon is visible for Record Status filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    Then : Verify that system should display Remove Icon for Record Status Filter

  @chrome
  Scenario: Verify Color of Remove Icon for Record Status filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    Then : Verify that Color of Remove Icon for Record Status filter should be red

  @chrome
  Scenario: Verify Remove Icon Functionality Record Status filter
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on Expand button of Filter By section
    And : Click on Record Status icon
    And : Click on Remove icon of Record Status
    Then : Verify that system should Remove the Record Status section when a user click on Remove icon

  @chrome
  Scenario: Verify when click on View Link Of Record then it redirects to Entry
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on View link of App Record
    Then : Verify that system should redirect to the Entry of the Selected App Record when a user click on View link

  @chrome
  Scenario: Verify Pagination is match with Present Records
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    Then : Verify that total number of record should be matched with Pagination

  #santosh
  @chrome @done @3.4 @done
  Scenario: Verify Records You Created Today is Clickable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Select Checkbox of Peer or Second Review.
    And : Select Checkbox of Allow the Record Creator to Approve app.
    And : Select Role from the Role dropdown.
    And : Select Add button (All User should be display as selected by default).
    And : Click on Continue button of basic tab
    And : Add All Required Attribute Details in Attribute tab
    And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
    And : Click on Continue button of attribute tab
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button of workflow tab
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button of acceptance tab
    And : Click on Publish & Continue button
    And : Click on Save button of permission tab
    And : Verify that user is allowed to Add New app
    And : Go Back to Home
    And : Click on Apps Tile
    And : Click on Search App of Apps Module
    And : Enter valid app name of App Module search field
    And : Click on Other Apps tile
    And : Click on Searched app
    And : Enter data in Mandatory fields
    And : Click on save button of Apps screen
    And : Go Back to Home
    And : Click on Advanced Search Tile
    And : Click on Records You Created Today link
    Then : Verify that Records You Created Today should be clickable

  @chrome @done @3.4 @done
  Scenario: Verify Records Pending Your Review is Clickable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Select Checkbox of Peer or Second Review.
    And : Select Checkbox of Allow the Record Creator to Approve app.
    And : Select Role from the Role dropdown.
    And : Select Add button (All User should be display as selected by default).
    And : Click on Continue button of basic tab
    And : Add All Required Attribute Details in Attribute tab
    And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
    And : Click on Continue button of attribute tab
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button of workflow tab
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button of acceptance tab
    And : Click on Publish & Continue button
    And : Click on Save button of permission tab
    And : Verify that user is allowed to Add New app
    And : Go Back to Home
    And : Click on Apps Tile
    And : Click on Search App of Apps Module
    And : Enter valid app name of App Module search field
    And : Click on Other Apps tile
    And : Click on Searched app
    And : Enter data in Mandatory fields
    And : Click on save button of Apps screen
    And : Go Back to Home
    And : Click on Advanced Search Tile
    And : Click on Records Pending Your Review link
    Then : Verify that Records Pending Your Review should be clickable

  @chrome @new @3.4 @done
  Scenario: Verify Records You Accessed Today is Clickable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Select Checkbox of Peer or Second Review.
    And : Select Checkbox of Allow the Record Creator to Approve app.
    And : Select Role from the Role dropdown.
    And : Select Add button (All User should be display as selected by default).
    And : Click on Continue button of basic tab
    And : Add All Required Attribute Details in Attribute tab
    And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
    And : Click on Continue button of attribute tab
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button of workflow tab
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button of acceptance tab
    And : Click on Publish & Continue button
    And : Click on Save button of permission tab
    And : Verify that user is allowed to Add New app
    And : Go Back to Home
    And : Click on Apps Tile
    And : Click on Search App of Apps Module
    And : Enter valid app name of App Module search field
    And : Click on Other Apps tile
    And : Click on Searched app
    And : Enter data in Mandatory fields
    And : Click on save button of Apps screen
    And : Go Back to Home
    And : Click on Advanced Search Tile
    And : Click on Records You Created Today link
    And : Click on View link of Created App
    And : Click on Back button of App which is Viewed
    And : Click on Records You Accessed Today link
    Then : Verify that Records You Accessed Today should be clickable

  @chrome @new @3.4 @done
  Scenario: Verify Records Assigned To You is Clickable
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Select Checkbox of Peer or Second Review.
    And : Select Checkbox of Allow the Record Creator to Approve app.
    And : Select Role from the Role dropdown.
    And : Select Add button (All User should be display as selected by default).
    And : Click on Continue button of basic tab
    And : Add All Required Attribute Details in Attribute tab
    And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
    And : Click on Continue button of attribute tab
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button of workflow tab
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button of acceptance tab
    And : Click on Publish & Continue button
    And : Click on Save button of permission tab
    And : Verify that user is allowed to Add New app
    And : Go Back to Home
    And : Click on Apps Tile
    And : Click on Search App of Apps Module
    And : Enter valid app name of App Module search field
    And : Click on Other Apps tile
    And : Click on Searched app
    And : Enter current user name in Assignee textbox
    And : Enter data in all Mandatory fields
    And : Click on save button of Apps screen
    And : Go Back to Home
    And : Click on Advanced Search Tile
    And : Click on Records Assigned To You link
    Then : Verify that Records Assigned To You text should be is clickable

  @chrome @new @3.4 @done
  Scenario: Verify order of Quick Access Link's
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    Then : Verify that all the link's under Quick Access is in proper order

  @chrome @3.4 @done
  Scenario: Verify that if clicking on Records You Created Today option then all today's records are display.
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Create app and enter data accordingly
    And : Create app and enter data accordingly
    And : Go Back to Home
    And : Click on Advanced Search Tile
    And : Click on Records You Created Today link
    Then : Verify that when click on Records You Created Today then all today's records should display

  #check same with draft and void
  @chrome @done
  Scenario: Verify that Records You Created Today option display records according to permission.
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Select Checkbox of Peer or Second Review.
    And : Select Checkbox of Allow the Record Creator to Approve app.
    And : Select Role from the Role dropdown.
    And : Select Add button (All User should be display as selected by default).
    And : Click on Continue button of basic tab
    And : Add All Required Attribute Details in Attribute tab
    And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
    And : Click on Continue button of attribute tab
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button of workflow tab
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button of acceptance tab
    And : Click on Publish & Continue button
    And : Click on Save button of permission tab
    And : Verify that user is allowed to Add New app
    And : Go Back to Home
    And : Click on Apps Tile
    And : Click on Search App of Apps Module
    And : Enter valid app name of App Module search field
    And : Click on Other Apps tile
    And : Click on Searched app
    And : Enter data in all Mandatory fields select entity and fail the record
    And : Click on save button of Apps screen
    And : Go Back to Home
    And : Click on Administration Tile
    And : Click on Roles and Permissions Tile
    And : Click on roles permissions Button
    And : Select Role from role dropdown
    And : Click on Edit button
    And : Select Module from module dropdown
    And : Uncheck permission checkbox of App
    And : Click on save button and popup
    And : Logout from system
    And : Login in system
    And : Click on Advanced Search Tile
    And : Click on Records You Created Today link
    Then : Verify that Records You Created Today option should display records according to permission

  @chrome @done
  Scenario: Verify that Records You Accessed Today option display records according to permission.
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Administration Tile
    And : Click on App Builder Tile
    And : Click on Add New button of App Builder
    And : Add All Required App Details in Basic Tab
    And : Select Checkbox of Peer or Second Review.
    And : Select Checkbox of Allow the Record Creator to Approve app.
    And : Select Role from the Role dropdown.
    And : Select Add button (All User should be display as selected by default).
    And : Click on Continue button of basic tab
    And : Add All Required Attribute Details in Attribute tab
    And : Select Mark Set Value 'Inactive' when App Submission status is failed checkbox
    And : Click on Continue button of attribute tab
    And : Add all Required Workflow Details in Workflow tab
    And : Click on Continue button of workflow tab
    And : Add all required App Acceptance Criteria in Acceptance tab
    And : Click on Continue button of acceptance tab
    And : Click on Publish & Continue button
    And : Click on Save button of permission tab
    And : Verify that user is allowed to Add New app
    And : Go Back to Home
    And : Click on Apps Tile
    And : Click on Search App of Apps Module
    And : Enter valid app name of App Module search field
    And : Click on Other Apps tile
    And : Click on Searched app
    And : Enter data in all Mandatory fields select entity and fail the record
    And : Click on save button of Apps screen
    And : Go Back to Home
    And : Click on Advanced Search Tile
    And : Click on Records You Created Today link
    And : Click on View link of Created App
    And : Click on Back button of App which is Viewed
    And : Go Back to Home
    And : Click on Administration Tile
    And : Click on Roles and Permissions Tile
    And : Click on roles permissions Button
    And : Select Role from role dropdown
    And : Click on Edit button
    And : Select Module from module dropdown
    And : Uncheck permission checkbox of App
    And : Click on save button and popup
    And : Logout from system
    And : Login in system
    And : Click on Advanced Search Tile
    And : Click on Records You Accessed Today link
    Then : Verify that Records You Accessed Today option should display records according to permission

  @chrome @done
  Scenario: Verify that if clicking on Records You Accessed Today option then all today's accessed records are display.
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Create app and enter data accordingly
    And : Click on Advanced Search Tile
    And : Click on Records You Created Today link
    And : Click on View link of Created App
    And : Click on Back button of App which is Viewed
    And : Go Back to Home
    And : Create app and enter data accordingly
    And : Click on Advanced Search Tile
    And : Click on Records You Created Today link
    And : Click on View link of Created App
    And : Click on Back button of App which is Viewed
    And : Go Back to Home
    And : Click on Advanced Search Tile
    And : Click on Records You Accessed Today link
    Then : Verify that when click on Records You Accessed Today then all today's records should display

  @chrome @done
  Scenario: Verify that if clicking on Records You Assigned Today option then all today's accessed records are display.
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Create app and enter data accordingly with Assignee
    And : Create app and enter data accordingly with Assignee
    And : Go Back to Home
    And : Get Current user details(firstName, lastName)
    And : Go Back to Home
    And : Click on Advanced Search Tile
    And : Click on Records Assigned To You link
    Then : Verify that when click on Records You Assigned Today then all today's records should display

  @chrome @done
  Scenario: Verify that Records You Assigned Today option display records according to permission.
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Create app and enter data accordingly with Assignee
    And : Go Back to Home
    And : Click on Administration Tile
    And : Click on Roles and Permissions Tile
    And : Click on roles permissions Button
    And : Select Role from role dropdown
    And : Click on Edit button
    And : Select Module from module dropdown
    And : Uncheck permission checkbox of App
    And : Click on save button and popup
    And : Logout from system
    And : Login in system
    And : Click on Advanced Search Tile
    And : Click on Records Assigned To You link
    Then : Verify that Records You Assigned Today option should display records according to permission

  @chrome @done
  Scenario: Verify that if clicking on Records Pending Your Review option then all today's accessed records are display.
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Create app and enter data accordingly
    And : Create app and enter data accordingly
    And : Go Back to Home
    And : Click on Advanced Search Tile
    And : Click on Records Pending Your Review link
    Then : Verify that when click on Records Pending Your Review then all today's records should display

  @chrome @done
  Scenario: Verify that Records Pending Your Review option display records according to permission.
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Create app and enter data accordingly
    And : Go Back to Home
    And : Click on Administration Tile
    And : Click on Roles and Permissions Tile
    And : Click on roles permissions Button
    And : Select Role from role dropdown
    And : Click on Edit button
    And : Select Module from module dropdown
    And : Uncheck permission checkbox of App
    And : Click on save button and popup
    And : Logout from system
    And : Login in system
    And : Click on Advanced Search Tile
    And : Click on Records Pending Your Review link
    Then : Verify that Records Pending Your Review option should display records according to permission

  @chrome @done
  Scenario: Verify that My Favorites tab displays three columns as Search Name, Last Updated, and Action.
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Click on My Favorites tab
    And : Save app in My Favorites if not existing Already
    Then : Verify that My Favorites tab should display three columns as Search Name, Last Updated, and Action

  @chrome @done
  Scenario: Verify that Search Name column's all created Favorites is clickable.
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    And : Enter Valid Favorite Name value
    And : Click on Save button of Advanced Search
    Then : Verify that Search Name column's all created Favorites should clickable

	@chrome @tempDemo
  Scenario: Verify that Last Updated column displays last modified date and time.
    Given : Navigate to the URL
    And : Enter valid credentials and Click on Login Button
    And : Click on Advanced Search Tile
    And : Remove all saved My Favorites apps
    And : Select App from Search for Apps by Names search box
    And : Click on Go button
    And : Click on Add To Favorite button of Advanced Search
    And : Enter Valid Favorite Name value
    And : Click on Save button of Advanced Search
    Then : Verify that Last Updated column should display last modified date and time

      @chrome @Pending
  Scenario: Verify that My Favorites Search displays according to Login User.
  Given : Navigate to the URL
	And : Enter valid credentials of User 1 and click on Login Button
	And : Click on Advanced Search Tile
	And : Click on Search for Apps by Name textbox
	And : Enter one or multiple App/Master App Name
	And : Click on Add To Favorite button
	And : Click on Enter Favorite Name textbox
	And : Enter name in the textbox
	And : Click on My Favorites checkbox
	And : Click on Save button
	And : Observe the My Favorites tab
	And : Enter valid credentials of User 2 and click on Login Button
	And : Click on Advanced Search Tile
	And : Click on Search for Apps by Name textbox
	And : Enter one or multiple App/Master App Name
	And : Click on Add To Favorite button
	And : Click on Enter Favorite Name textbox
	And : Enter name in the textbox
	And : Click on My Favorites checkbox
	And : Click on Save button
	And : Observe the My Favorites tab
	Then : Verify that My Favorites Search should display according to Login User
	
	